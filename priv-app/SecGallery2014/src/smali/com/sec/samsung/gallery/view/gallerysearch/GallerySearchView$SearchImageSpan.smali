.class Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchImageSpan;
.super Landroid/text/style/ImageSpan;
.source "GallerySearchView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SearchImageSpan"
.end annotation


# instance fields
.field private mDrawableBounds:Landroid/graphics/Rect;

.field private mFontBounds:Landroid/graphics/Rect;

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;Landroid/graphics/drawable/Drawable;I)V
    .locals 2
    .param p2, "d"    # Landroid/graphics/drawable/Drawable;
    .param p3, "size"    # I

    .prologue
    const/4 v1, 0x0

    .line 574
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchImageSpan;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    .line 575
    invoke-direct {p0, p2}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 576
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchImageSpan;->mFontBounds:Landroid/graphics/Rect;

    .line 577
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, p3, p3}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchImageSpan;->mDrawableBounds:Landroid/graphics/Rect;

    .line 578
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "text"    # Ljava/lang/CharSequence;
    .param p3, "start"    # I
    .param p4, "end"    # I
    .param p5, "x"    # F
    .param p6, "top"    # I
    .param p7, "y"    # I
    .param p8, "bottom"    # I
    .param p9, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 582
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchImageSpan;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$1000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0280

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 583
    .local v1, "transX":I
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 584
    neg-int v1, v1

    .line 586
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchImageSpan;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 587
    .local v0, "b":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchImageSpan;->mDrawableBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    sub-int v3, p8, v3

    div-int/lit8 v2, v3, 0x2

    .line 588
    .local v2, "transY":I
    int-to-float v3, v1

    add-float/2addr v3, p5

    int-to-float v4, v2

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 589
    if-nez v0, :cond_1

    .line 595
    :goto_0
    return-void

    .line 591
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchImageSpan;->mDrawableBounds:Landroid/graphics/Rect;

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 592
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 593
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchImageSpan;->mFontBounds:Landroid/graphics/Rect;

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 594
    int-to-float v3, v1

    sub-float v3, p5, v3

    neg-float v3, v3

    neg-int v4, v2

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_0
.end method

.method public getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 1
    .param p1, "paint"    # Landroid/graphics/Paint;
    .param p2, "text"    # Ljava/lang/CharSequence;
    .param p3, "start"    # I
    .param p4, "end"    # I
    .param p5, "fm"    # Landroid/graphics/Paint$FontMetricsInt;

    .prologue
    .line 599
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchImageSpan;->mDrawableBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    return v0
.end method
