.class public Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;
.super Lcom/sec/samsung/gallery/view/ViewObservable;
.source "CheckStorageDialog.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private final mCtx:Landroid/content/Context;

.field private mDialog:Landroid/app/Dialog;

.field private mFromMyFiles:Z

.field private final mMyFilesName:Landroid/content/ComponentName;

.field private final mMyFilesTabletName:Landroid/content/ComponentName;

.field private mShowEmptyDialog:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "c"    # Landroid/content/Context;
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/content/Context;Landroid/content/Intent;Z)V

    .line 47
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/content/Context;Landroid/content/Intent;Z)V
    .locals 4
    .param p1, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "c"    # Landroid/content/Context;
    .param p3, "intent"    # Landroid/content/Intent;
    .param p4, "showEmptyDialog"    # Z

    .prologue
    const/4 v3, 0x0

    .line 49
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/ViewObservable;-><init>()V

    .line 38
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.sec.android.app.myfiles"

    const-string v2, "com.sec.android.app.myfiles.MainActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mMyFilesName:Landroid/content/ComponentName;

    .line 40
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.sec.android.app.myfiles"

    const-string v2, "com.sec.android.app.myfiles.Myfiles"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mMyFilesTabletName:Landroid/content/ComponentName;

    .line 42
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mFromMyFiles:Z

    .line 43
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mShowEmptyDialog:Z

    .line 50
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 51
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mCtx:Landroid/content/Context;

    .line 52
    iput-boolean p4, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mShowEmptyDialog:Z

    .line 53
    invoke-direct {p0, p3}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->checkMyFilesIntent(Landroid/content/Intent;)V

    .line 54
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->createDialog()V

    .line 55
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->callMyFiles()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mCtx:Landroid/content/Context;

    return-object v0
.end method

.method private callMyFiles()V
    .locals 4

    .prologue
    .line 145
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 146
    .local v1, "intent":Landroid/content/Intent;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->getMyFilesName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 147
    const/high16 v2, 0x16000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 150
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mCtx:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    :goto_0
    return-void

    .line 151
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->TAG:Ljava/lang/String;

    const-string v3, "Activity Not Found"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private checkMyFilesIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 58
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mFromMyFiles:Z

    .line 59
    if-eqz p1, :cond_0

    .line 60
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 61
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 62
    const-string v1, "from-myfiles"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mFromMyFiles:Z

    .line 65
    .end local v0    # "extras":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method private checkMyfiles()Z
    .locals 6

    .prologue
    .line 131
    const/4 v2, 0x0

    .line 133
    .local v2, "myFilesExist":Z
    :try_start_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mCtx:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->getMyFilesName()Landroid/content/ComponentName;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 135
    .local v0, "activityInfo":Landroid/content/pm/ActivityInfo;
    if-eqz v0, :cond_0

    .line 136
    const/4 v2, 0x1

    .line 141
    .end local v0    # "activityInfo":Landroid/content/pm/ActivityInfo;
    :cond_0
    :goto_0
    return v2

    .line 138
    :catch_0
    move-exception v1

    .line 139
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private getMyFilesName()Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 158
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMyFilesTabletName:Z

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mMyFilesTabletName:Landroid/content/ComponentName;

    .line 161
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mMyFilesName:Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private setEmptyDialog(Landroid/app/AlertDialog$Builder;J)V
    .locals 6
    .param p1, "builder"    # Landroid/app/AlertDialog$Builder;
    .param p2, "remaining"    # J

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mCtx:Landroid/content/Context;

    const v1, 0x7f0e028b

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 121
    const v0, 0x7f0e00db

    new-instance v1, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog$3;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog$3;-><init>(Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 128
    return-void
.end method

.method private setMyfilesDialog(Landroid/app/AlertDialog$Builder;J)V
    .locals 6
    .param p1, "builder"    # Landroid/app/AlertDialog$Builder;
    .param p2, "remaining"    # J

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mCtx:Landroid/content/Context;

    const v1, 0x7f0e028a

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 102
    const v0, 0x7f0e00db

    new-instance v1, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog$1;-><init>(Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 110
    const v0, 0x7f0e0046

    new-instance v1, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog$2;-><init>(Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 117
    return-void
.end method


# virtual methods
.method public createDialog()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x400

    const/4 v8, 0x0

    .line 80
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mCtx:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 81
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mCtx:Landroid/content/Context;

    const v4, 0x7f0e021b

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 82
    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 85
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getImageCacheService()Lcom/sec/android/gallery3d/data/ImageCacheService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/ImageCacheService;->getImageDiskCacheMaxBytes()J

    move-result-wide v4

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getExternalStorageFreeSize()J

    move-result-wide v6

    sub-long/2addr v4, v6

    div-long/2addr v4, v10

    div-long v2, v4, v10

    .line 89
    .local v2, "requestStorageSize":J
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mFromMyFiles:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->checkMyfiles()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mShowEmptyDialog:Z

    if-nez v1, :cond_0

    .line 90
    invoke-direct {p0, v0, v2, v3}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->setMyfilesDialog(Landroid/app/AlertDialog$Builder;J)V

    .line 95
    :goto_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mDialog:Landroid/app/Dialog;

    .line 96
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v1, v8}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 98
    return-void

    .line 92
    :cond_0
    invoke-direct {p0, v0, v2, v3}, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->setEmptyDialog(Landroid/app/AlertDialog$Builder;J)V

    goto :goto_0
.end method

.method public dismissDialog()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 77
    :cond_0
    return-void
.end method

.method public showDialog()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/CheckStorageDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 71
    :cond_0
    return-void
.end method
