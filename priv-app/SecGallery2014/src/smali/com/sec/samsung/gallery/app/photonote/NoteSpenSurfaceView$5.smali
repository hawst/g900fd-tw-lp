.class Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$5;
.super Ljava/lang/Object;
.source "NoteSpenSurfaceView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mPreButtonState:I

.field final synthetic this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)V
    .locals 1

    .prologue
    .line 525
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$5;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 526
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$5;->mPreButtonState:I

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x2

    .line 530
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v0

    .line 531
    .local v0, "nCurBtnState":I
    iget v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$5;->mPreButtonState:I

    if-nez v1, :cond_1

    if-ne v0, v2, :cond_1

    .line 532
    const-string v1, "NoteSpenSurfaceView"

    const-string v2, "Button Down"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    :cond_0
    :goto_0
    iput v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$5;->mPreButtonState:I

    .line 574
    const/4 v1, 0x0

    return v1

    .line 533
    :cond_1
    iget v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$5;->mPreButtonState:I

    if-ne v1, v2, :cond_2

    if-nez v0, :cond_2

    .line 534
    const-string v1, "NoteSpenSurfaceView"

    const-string v2, "Button Up"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 566
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$5;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mHoverButtonAction:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$800(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 567
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$5;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->isPenMode()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 568
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$5;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$5;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    # invokes: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->toggleVisibility(Landroid/view/View;)V
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$900(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;Landroid/view/View;)V

    goto :goto_0

    .line 569
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$5;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->isRemoverMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 570
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$5;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$5;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # invokes: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->toggleVisibility(Landroid/view/View;)V
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$900(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;Landroid/view/View;)V

    goto :goto_0
.end method
