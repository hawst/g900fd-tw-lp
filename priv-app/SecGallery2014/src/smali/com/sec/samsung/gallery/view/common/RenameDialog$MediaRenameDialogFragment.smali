.class Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;
.super Landroid/app/DialogFragment;
.source "RenameDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/common/RenameDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MediaRenameDialogFragment"
.end annotation


# static fields
.field protected static final TAG:Ljava/lang/String; = "MediaRenameDialogFragment"


# instance fields
.field private final MAX_NAME_LENGTH:I

.field mAlertDialog:Landroid/app/AlertDialog;

.field private mClipVisible:Z

.field private final mCtx:Landroid/content/Context;

.field private mInvalidToast:Landroid/widget/Toast;

.field public mItem:Lcom/sec/android/gallery3d/data/MediaObject;

.field public mOrgFilePath:Ljava/lang/String;

.field private mOrgName:Ljava/lang/String;

.field public mString:Ljava/lang/String;

.field private mTooLongToast:Landroid/widget/Toast;

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/common/RenameDialog;Landroid/content/Context;)V
    .locals 3
    .param p2, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 130
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 111
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mString:Ljava/lang/String;

    .line 114
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    .line 118
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mOrgName:Ljava/lang/String;

    .line 120
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mClipVisible:Z

    .line 125
    const/16 v1, 0x32

    iput v1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->MAX_NAME_LENGTH:I

    .line 131
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mCtx:Landroid/content/Context;

    .line 132
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 133
    .local v0, "args":Landroid/os/Bundle;
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 134
    return-void
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mClipVisible:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mInvalidToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mTooLongToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mOrgName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mCtx:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->startHandleRename()V

    return-void
.end method

.method private startHandleRename()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 484
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mItem:Lcom/sec/android/gallery3d/data/MediaObject;

    if-eqz v1, :cond_0

    .line 485
    new-instance v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;-><init>()V

    .line 486
    .local v0, "progressDialog":Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mCtx:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mCtx:Landroid/content/Context;

    const v4, 0x7f0e02e1

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->showProgressDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 488
    new-instance v6, Lcom/sec/samsung/gallery/util/FileUtil;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mCtx:Landroid/content/Context;

    invoke-direct {v6, v1}, Lcom/sec/samsung/gallery/util/FileUtil;-><init>(Landroid/content/Context;)V

    .line 489
    .local v6, "fileUtils":Lcom/sec/samsung/gallery/util/FileUtil;
    new-instance v1, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$10;

    invoke-direct {v1, p0, v0}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$10;-><init>(Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;)V

    invoke-virtual {v6, v2, v1}, Lcom/sec/samsung/gallery/util/FileUtil;->operateMedias(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/OnProgressListener;)Lcom/sec/samsung/gallery/util/MediaOperations;

    .line 515
    .end local v0    # "progressDialog":Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;
    .end local v6    # "fileUtils":Lcom/sec/samsung/gallery/util/FileUtil;
    :goto_0
    return-void

    .line 512
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_RENAME_MEDIA:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setData(Ljava/lang/Object;)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 465
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 466
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 467
    .local v0, "d":Landroid/app/Dialog;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 468
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 469
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 519
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 522
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 138
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mCtx:Landroid/content/Context;

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f030027

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 140
    .local v0, "alertDialogueView":Landroid/view/View;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mTooLongToast:Landroid/widget/Toast;

    if-nez v6, :cond_0

    .line 141
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mCtx:Landroid/content/Context;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mCtx:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0e01cc

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/16 v11, 0x32

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mTooLongToast:Landroid/widget/Toast;

    .line 145
    :cond_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mInvalidToast:Landroid/widget/Toast;

    if-nez v6, :cond_1

    .line 146
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mCtx:Landroid/content/Context;

    const v7, 0x7f0e0111

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mInvalidToast:Landroid/widget/Toast;

    .line 150
    :cond_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    const v6, 0x7f0f0069

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    # setter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v7, v6}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$102(Lcom/sec/samsung/gallery/view/common/RenameDialog;Landroid/widget/EditText;)Landroid/widget/EditText;

    .line 152
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWritingPanel:Z

    if-eqz v6, :cond_2

    .line 153
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/widget/EditText;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->getInputExtras(Z)Landroid/os/Bundle;

    move-result-object v1

    .line 154
    .local v1, "b":Landroid/os/Bundle;
    const-string v6, "maxLength"

    const/16 v7, 0x32

    invoke-virtual {v1, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 156
    .end local v1    # "b":Landroid/os/Bundle;
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/widget/EditText;

    move-result-object v6

    const v7, 0x84000

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setInputType(I)V

    .line 158
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/widget/EditText;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/EditText;->requestFocus()Z

    .line 160
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$200(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v6

    if-nez v6, :cond_3

    .line 161
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/widget/EditText;

    move-result-object v6

    new-instance v7, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$1;

    invoke-direct {v7, p0}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$1;-><init>(Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;)V

    const-wide/16 v8, 0x12c

    invoke-virtual {v6, v7, v8, v9}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 169
    :cond_3
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mString:Ljava/lang/String;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mString:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    const/16 v7, 0x32

    if-le v6, v7, :cond_4

    .line 170
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/widget/EditText;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mString:Ljava/lang/String;

    const/4 v8, 0x0

    const/16 v9, 0x32

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 171
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/widget/EditText;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mString:Ljava/lang/String;

    const/4 v8, 0x0

    const/16 v9, 0x32

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setSelection(I)V

    .line 172
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/widget/EditText;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/EditText;->selectAll()V

    .line 173
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mTooLongToast:Landroid/widget/Toast;

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 180
    :goto_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/widget/EditText;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 182
    new-instance v2, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$2;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$2;-><init>(Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;)V

    .line 253
    .local v2, "filter":Landroid/text/InputFilter;
    new-instance v3, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$3;

    const/16 v6, 0x32

    invoke-direct {v3, p0, v6}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$3;-><init>(Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;I)V

    .line 267
    .local v3, "lengthFilter":Landroid/text/InputFilter$LengthFilter;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/widget/EditText;

    move-result-object v6

    const-string v7, "inputType=filename;disableEmoticonInput=true"

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 268
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/widget/EditText;

    move-result-object v6

    const/4 v7, 0x2

    new-array v7, v7, [Landroid/text/InputFilter;

    const/4 v8, 0x0

    aput-object v2, v7, v8

    const/4 v8, 0x1

    aput-object v3, v7, v8

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 270
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/widget/EditText;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mOrgName:Ljava/lang/String;

    .line 272
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v7, 0x7f0e010e

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f0e010e

    new-instance v8, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$5;

    invoke-direct {v8, p0}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$5;-><init>(Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const/high16 v7, 0x1040000

    new-instance v8, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$4;

    invoke-direct {v8, p0}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$4;-><init>(Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    .line 287
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    .line 289
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/widget/EditText;

    move-result-object v6

    new-instance v7, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$6;

    invoke-direct {v7, p0}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$6;-><init>(Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;)V

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 301
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    const/4 v7, -0x1

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v4

    .line 302
    .local v4, "positiveButton":Landroid/widget/Button;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mOrgName:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 303
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 308
    :goto_1
    new-instance v6, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$7;

    invoke-direct {v6, p0}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$7;-><init>(Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;)V

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 388
    new-instance v5, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$8;

    invoke-direct {v5, p0}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$8;-><init>(Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;)V

    .line 407
    .local v5, "textWatcher":Landroid/text/TextWatcher;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/widget/EditText;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 408
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/widget/EditText;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 410
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-static {v6}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->hideStatusIcon(Ljava/lang/Object;)V

    .line 411
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    return-object v6

    .line 175
    .end local v2    # "filter":Landroid/text/InputFilter;
    .end local v3    # "lengthFilter":Landroid/text/InputFilter$LengthFilter;
    .end local v4    # "positiveButton":Landroid/widget/Button;
    .end local v5    # "textWatcher":Landroid/text/TextWatcher;
    :cond_4
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/widget/EditText;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mString:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 176
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/widget/EditText;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setSelection(I)V

    .line 177
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/widget/EditText;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/EditText;->selectAll()V

    goto/16 :goto_0

    .line 305
    .restart local v2    # "filter":Landroid/text/InputFilter;
    .restart local v3    # "lengthFilter":Landroid/text/InputFilter$LengthFilter;
    .restart local v4    # "positiveButton":Landroid/widget/Button;
    :cond_5
    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1
.end method

.method public onPause()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 445
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 446
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->mReceiveTime:J
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$1200(Lcom/sec/samsung/gallery/view/common/RenameDialog;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x2ee

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 447
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # setter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->isSipVisible:Z
    invoke-static {v1, v6}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$1002(Lcom/sec/samsung/gallery/view/common/RenameDialog;Z)Z

    .line 449
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$1300(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 450
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mCtx:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$1300(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/content/BroadcastReceiver;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 452
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    const/4 v2, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$1302(Lcom/sec/samsung/gallery/view/common/RenameDialog;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;

    .line 453
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    # invokes: Lcom/sec/samsung/gallery/view/common/RenameDialog;->hideSoftInput(Landroid/os/IBinder;)V
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$000(Lcom/sec/samsung/gallery/view/common/RenameDialog;Landroid/os/IBinder;)V

    .line 455
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mCtx:Landroid/content/Context;

    const-string v2, "clipboardEx"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/sec/clipboard/ClipboardExManager;

    .line 456
    .local v0, "clipEx":Landroid/sec/clipboard/ClipboardExManager;
    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 457
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mClipVisible:Z

    .line 461
    :goto_0
    return-void

    .line 459
    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mClipVisible:Z

    goto :goto_0
.end method

.method public onResume()V
    .locals 6

    .prologue
    .line 417
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 419
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$800(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 420
    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 421
    const v2, 0x7f0f0069

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 422
    .local v1, "editText":Landroid/widget/EditText;
    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 423
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 424
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # invokes: Lcom/sec/samsung/gallery/view/common/RenameDialog;->setSipBroadcastReceiver()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$900(Lcom/sec/samsung/gallery/view/common/RenameDialog;)V

    .line 425
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RenameDialog;->isSipVisible:Z
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->access$1000(Lcom/sec/samsung/gallery/view/common/RenameDialog;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 426
    new-instance v2, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$9;

    invoke-direct {v2, p0, v1}, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment$9;-><init>(Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;Landroid/widget/EditText;)V

    const-wide/16 v4, 0x258

    invoke-virtual {v1, v2, v4, v5}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 441
    .end local v1    # "editText":Landroid/widget/EditText;
    :cond_0
    return-void
.end method

.method public setCurrentFilePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 480
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mOrgFilePath:Ljava/lang/String;

    .line 481
    return-void
.end method

.method public setCurrentItem(Lcom/sec/android/gallery3d/data/MediaObject;)V
    .locals 0
    .param p1, "currentItem"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 476
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mItem:Lcom/sec/android/gallery3d/data/MediaObject;

    .line 477
    return-void
.end method

.method public setCurrentName(Ljava/lang/String;)V
    .locals 0
    .param p1, "currentName"    # Ljava/lang/String;

    .prologue
    .line 472
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/RenameDialog$MediaRenameDialogFragment;->mString:Ljava/lang/String;

    .line 473
    return-void
.end method
