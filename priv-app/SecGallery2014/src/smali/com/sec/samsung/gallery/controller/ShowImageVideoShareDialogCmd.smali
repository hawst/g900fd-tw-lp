.class public Lcom/sec/samsung/gallery/controller/ShowImageVideoShareDialogCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "ShowImageVideoShareDialogCmd.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field type:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method private addNewFile(Ljava/lang/String;)V
    .locals 7
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 58
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoShareDialogCmd;->mContext:Landroid/content/Context;

    check-cast v6, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    .line 59
    .local v4, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/ShowImageVideoShareDialogCmd;->getUriFrom(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 60
    .local v5, "uri":Landroid/net/Uri;
    if-nez v5, :cond_0

    const-string v6, "jpg"

    invoke-virtual {p1, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 61
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    .line 63
    :cond_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoShareDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 64
    .local v0, "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    .line 65
    .local v1, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    .line 66
    .local v3, "path":Lcom/sec/android/gallery3d/data/Path;
    const/4 v6, 0x0

    invoke-virtual {v1, v3, v6}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;Z)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 67
    .local v2, "media":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 68
    return-void
.end method

.method private getUriFrom(Ljava/lang/String;)Landroid/net/Uri;
    .locals 12
    .param p1, "newFilePath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 71
    new-array v2, v3, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    .line 74
    .local v2, "projection":[Ljava/lang/String;
    new-array v4, v3, [Ljava/lang/String;

    aput-object p1, v4, v1

    .line 77
    .local v4, "args":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 78
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 80
    .local v8, "uri":Landroid/net/Uri;
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoShareDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v3, "_data=?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 82
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 84
    .local v7, "id":I
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    int-to-long v10, v7

    invoke-static {v0, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 87
    .end local v7    # "id":I
    :cond_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 89
    return-object v8

    .line 87
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private showImageVideoShareDialog()V
    .locals 4

    .prologue
    .line 40
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoShareDialogCmd;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    .line 41
    .local v1, "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getNewMP4FilePath()Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "newFilePath":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 43
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/app/StateManager;->setMultiFormatShareActive(ZLjava/lang/String;)V

    .line 44
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/controller/ShowImageVideoShareDialogCmd;->updateSelectionModeProxy(Ljava/lang/String;)V

    .line 46
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowImageVideoShareDialogCmd;->showShareDialog()V

    .line 47
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoShareDialogCmd;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "EXIT_SELECTION_MODE"

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method private showShareDialog()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 93
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoShareDialogCmd;->mContext:Landroid/content/Context;

    aput-object v1, v0, v2

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 96
    .local v0, "params":[Ljava/lang/Object;
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoShareDialogCmd;->type:Z

    if-eqz v1, :cond_0

    .line 97
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoShareDialogCmd;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "LAST_SHARE_APP"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 103
    :goto_0
    return-void

    .line 100
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoShareDialogCmd;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "SHOW_SHARE_DIALOG"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private updateSelectionModeProxy(Ljava/lang/String;)V
    .locals 2
    .param p1, "newFilePath"    # Ljava/lang/String;

    .prologue
    .line 52
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoShareDialogCmd;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    .line 53
    .local v0, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 54
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/ShowImageVideoShareDialogCmd;->addNewFile(Ljava/lang/String;)V

    .line 55
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 2
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 33
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    .line 34
    .local v0, "params":[Ljava/lang/Object;
    const/4 v1, 0x0

    aget-object v1, v0, v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoShareDialogCmd;->mContext:Landroid/content/Context;

    .line 35
    const/4 v1, 0x1

    aget-object v1, v0, v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoShareDialogCmd;->type:Z

    .line 36
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowImageVideoShareDialogCmd;->showImageVideoShareDialog()V

    .line 37
    return-void
.end method
