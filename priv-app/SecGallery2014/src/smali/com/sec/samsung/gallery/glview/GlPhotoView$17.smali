.class Lcom/sec/samsung/gallery/glview/GlPhotoView$17;
.super Ljava/lang/Object;
.source "GlPhotoView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 0

    .prologue
    .line 2133
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$17;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHoverEnter(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 5
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/4 v4, 0x1

    .line 2135
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$17;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    .line 2136
    .local v1, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2144
    .end local p1    # "object":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_0
    :goto_0
    return v4

    .line 2139
    .restart local p1    # "object":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_1
    check-cast p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .end local p1    # "object":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget v0, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 2141
    .local v0, "index":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$17;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnHoverListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnHoverListener;

    if-eqz v2, :cond_0

    .line 2142
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$17;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnHoverListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnHoverListener;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$17;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-interface {v2, v3, v0}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnHoverListener;->onHoverEnter(Lcom/sec/samsung/gallery/glview/GlAbsListView;I)V

    goto :goto_0
.end method

.method public onHoverExit(Lcom/sec/android/gallery3d/glcore/GlObject;)Z
    .locals 3
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 2152
    check-cast p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .end local p1    # "object":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget v0, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 2154
    .local v0, "index":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$17;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnHoverListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnHoverListener;

    if-eqz v1, :cond_0

    .line 2155
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$17;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnHoverListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnHoverListener;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$17;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-interface {v1, v2, v0}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnHoverListener;->onHoverExit(Lcom/sec/samsung/gallery/glview/GlAbsListView;I)V

    .line 2157
    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method public onHoverMove(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 1
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "dx"    # I
    .param p3, "dy"    # I

    .prologue
    .line 2148
    const/4 v0, 0x1

    return v0
.end method
