.class public Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "MultiplePickerSelectionCompleted.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field private static final TAG:Ljava/lang/String;

.field public static final WIDGET_MAX_COUNT:I = 0x3e8


# instance fields
.field private mActivity:Landroid/app/Activity;

.field mContext:Landroid/content/Context;

.field mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method private getAlbumPathList(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    .local p1, "selectedItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 129
    .local v0, "albumPathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 130
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v3, v2, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v3, :cond_0

    .line 131
    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 134
    :cond_1
    return-object v0
.end method

.method private getCoverFilePathList(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166
    .local p1, "selectedItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 167
    .local v1, "coverFilePathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 168
    .local v3, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v5, v3, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v5, :cond_0

    .line 169
    check-cast v3, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local v3    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getCoverMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    .line 170
    .local v4, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v5, v4, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v5, :cond_0

    .line 171
    check-cast v4, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    .end local v4    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getFilePath()Ljava/lang/String;

    move-result-object v0

    .line 172
    .local v0, "coverFilePath":Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 176
    .end local v0    # "coverFilePath":Ljava/lang/String;
    :cond_1
    return-object v1
.end method

.method private getFilePathList(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    .local p1, "selectedItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 109
    .local v1, "filePathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 110
    .local v4, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v7, v4, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v7, :cond_3

    move-object v7, v4

    .line 111
    check-cast v7, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    .line 112
    .local v0, "count":I
    check-cast v4, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local v4    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    const/4 v7, 0x0

    invoke-virtual {v4, v7, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v5

    .line 113
    .local v5, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 114
    .local v6, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v7, v6, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v7, :cond_2

    .line 115
    check-cast v6, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    .end local v6    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;

    move-result-object v7

    iget-object v7, v7, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 116
    .restart local v6    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_2
    instance-of v7, v6, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v7, :cond_1

    .line 117
    check-cast v6, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v6    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v7, v6, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 120
    .end local v0    # "count":I
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .restart local v4    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_3
    instance-of v7, v4, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v7, :cond_0

    .line 121
    check-cast v4, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v4    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    iget-object v7, v4, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 124
    :cond_4
    return-object v1
.end method

.method private getPersonIdList(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    .local p1, "selectedItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 139
    .local v4, "personIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 140
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v5, v2, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v5, :cond_0

    .line 141
    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getCoverMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    .line 142
    .local v3, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v5, v3, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v5, :cond_0

    .line 143
    check-cast v3, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    .end local v3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getPersonId()I

    move-result v1

    .line 144
    .local v1, "id":I
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 148
    .end local v1    # "id":I
    :cond_1
    return-object v4
.end method

.method private getPersonImageList(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    .local p1, "selectedItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 153
    .local v4, "personImageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 154
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v5, v2, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v5, :cond_0

    .line 155
    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getCoverMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    .line 156
    .local v3, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v5, v3, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v5, :cond_0

    .line 157
    check-cast v3, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    .end local v3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getFacePath()Ljava/lang/String;

    move-result-object v0

    .line 158
    .local v0, "filePath":Ljava/lang/String;
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 162
    .end local v0    # "filePath":Ljava/lang/String;
    :cond_1
    return-object v4
.end method

.method private getUriList(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    .local p1, "selectedItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 91
    .local v6, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 92
    .local v3, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v3, :cond_1

    .line 93
    sget-object v7, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->TAG:Ljava/lang/String;

    const-string v8, "selected item\'s uri is null, item is ignored"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 94
    :cond_1
    instance-of v7, v3, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v7, :cond_2

    move-object v7, v3

    .line 95
    check-cast v7, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    .line 96
    .local v0, "count":I
    check-cast v3, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local v3    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    const/4 v7, 0x0

    invoke-virtual {v3, v7, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v4

    .line 97
    .local v4, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 98
    .local v5, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 101
    .end local v0    # "count":I
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v5    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v3    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 104
    .end local v3    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_3
    return-object v6
.end method

.method private isFromWidget()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 201
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "photo-pick"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 203
    :goto_0
    return v1

    .line 202
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method private isPersonPick()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 209
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "person-pick"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 211
    :goto_0
    return v1

    .line 210
    :catch_0
    move-exception v0

    .line 211
    .local v0, "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method private isPhotoWall()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 193
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "from-photowall"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 195
    :goto_0
    return v1

    .line 194
    :catch_0
    move-exception v0

    .line 195
    .local v0, "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method private selectedItemCount(Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 180
    .local p1, "selectedItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v2, 0x0

    .line 181
    .local v2, "totalCount":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 182
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v3, v1, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v3, :cond_0

    .line 183
    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    .line 185
    .restart local v1    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 188
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    return v2
.end method

.method private setResultAndFinish(Landroid/app/Activity;)V
    .locals 17
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 49
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->mContext:Landroid/content/Context;

    check-cast v11, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v9

    .line 50
    .local v9, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCloneMediaList()Ljava/util/LinkedList;

    move-result-object v8

    .line 52
    .local v8, "selectedItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->isPhotoWall()Z

    move-result v5

    .line 53
    .local v5, "isFromPhotoWall":Z
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 55
    .local v7, "responseIntent":Landroid/content/Intent;
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->isPersonPick()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 56
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->getPersonIdList(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v6

    .line 57
    .local v6, "personIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v11, "personIds"

    invoke-virtual {v7, v11, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 58
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->getPersonImageList(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v3

    .line 59
    .local v3, "coverPathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v11, "coverPaths"

    invoke-virtual {v7, v11, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 60
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->getCoverFilePathList(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    .line 61
    .local v2, "coverFilePathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v11, "coverFilePaths"

    invoke-virtual {v7, v11, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 64
    .end local v2    # "coverFilePathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3    # "coverPathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v6    # "personIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_0
    if-eqz v5, :cond_1

    .line 65
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->getFilePathList(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v4

    .line 66
    .local v4, "filePathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v11, "android.intent.extra.STREAM"

    invoke-virtual {v7, v11, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 84
    .end local v4    # "filePathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    const/4 v11, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v7}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 85
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->leaveSelectionMode()V

    .line 86
    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->finish()V

    .line 87
    :goto_1
    return-void

    .line 70
    :cond_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v11

    if-nez v11, :cond_2

    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->isFromWidget()Z

    move-result v11

    if-eqz v11, :cond_2

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->selectedItemCount(Ljava/util/List;)I

    move-result v11

    const/16 v12, 0x3e8

    if-le v11, v12, :cond_2

    .line 71
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->mContext:Landroid/content/Context;

    const v13, 0x7f0e0114

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const/16 v16, 0x3e8

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v12, v13, v14}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 75
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->isPersonPick()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 76
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->getAlbumPathList(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v1

    .line 77
    .local v1, "albumPathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v11, "albumPaths"

    invoke-virtual {v7, v11, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_0

    .line 79
    .end local v1    # "albumPathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_3
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->getUriList(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v10

    .line 80
    .local v10, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const-string v11, "selectedItems"

    invoke-virtual {v7, v11, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 81
    const-string v11, "selectedCount"

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v12

    invoke-virtual {v7, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 2
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 41
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    .line 42
    .local v0, "params":[Ljava/lang/Object;
    const/4 v1, 0x0

    aget-object v1, v0, v1

    check-cast v1, Landroid/app/Activity;

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->mActivity:Landroid/app/Activity;

    .line 43
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->mActivity:Landroid/app/Activity;

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->mContext:Landroid/content/Context;

    .line 44
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 45
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->mActivity:Landroid/app/Activity;

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;->setResultAndFinish(Landroid/app/Activity;)V

    .line 46
    return-void
.end method
