.class Lcom/sec/samsung/gallery/view/help/HelpViewState$4;
.super Ljava/lang/Object;
.source "HelpViewState.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/help/HelpViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpViewState;)V
    .locals 0

    .prologue
    .line 403
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 415
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    iget v1, v0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mCurrentPointAnimation:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mCurrentPointAnimation:I

    .line 416
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mCurrentPointAnimation:I

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mPointAnimations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 417
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mTap:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 418
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    iget-object v1, v0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mTap:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mPointAnimations:Ljava/util/List;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    iget v2, v2, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mCurrentPointAnimation:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Animation;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 420
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 411
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 407
    return-void
.end method
