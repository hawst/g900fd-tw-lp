.class public Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
.super Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
.source "PositionControllerBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ThumbObject"
.end annotation


# instance fields
.field protected mAlbumTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field protected mBorderView:Lcom/sec/android/gallery3d/glcore/GlView;

.field protected mCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field protected mDecorObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field protected mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

.field protected mDimState:Z

.field protected mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field protected mPlayIconView:Lcom/sec/android/gallery3d/glcore/GlView;

.field protected mReorderEnable:Z

.field protected mSelectState:Z

.field protected mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field protected mTitleVisible:Z

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 3
    .param p2, "compView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1427
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .line 1428
    invoke-direct {p0, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    .line 1413
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 1414
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mAlbumTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 1415
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mDecorObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 1416
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 1417
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 1418
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mSelectState:Z

    .line 1419
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mDimState:Z

    .line 1420
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mTitleVisible:Z

    .line 1421
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mReorderEnable:Z

    .line 1422
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 1423
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mPlayIconView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 1424
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mBorderView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 1429
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setObjective(I)V

    .line 1431
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-direct {v0, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 1432
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 1434
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-direct {v0, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 1435
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 1437
    invoke-virtual {p1, p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->onCreateThumbObj(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)V

    .line 1438
    return-void
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .prologue
    .line 1412
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getExceptViews()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private getExceptViews()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/glcore/GlView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1512
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1513
    .local v0, "exceptViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/glcore/GlView;>;"
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mPlayIconView:Lcom/sec/android/gallery3d/glcore/GlView;

    if-eqz v1, :cond_0

    .line 1514
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mPlayIconView:Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1516
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mBorderView:Lcom/sec/android/gallery3d/glcore/GlView;

    if-eqz v1, :cond_1

    .line 1517
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mBorderView:Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1519
    :cond_1
    return-object v0
.end method


# virtual methods
.method public isSelected()Z
    .locals 1

    .prologue
    .line 1464
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mSelectState:Z

    return v0
.end method

.method public setDecorState(Lcom/sec/android/gallery3d/glcore/GlView;)V
    .locals 6
    .param p1, "decorView"    # Lcom/sec/android/gallery3d/glcore/GlView;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1468
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    if-nez v2, :cond_1

    if-nez p1, :cond_1

    .line 1505
    :cond_0
    :goto_0
    return-void

    .line 1471
    :cond_1
    if-eqz p1, :cond_8

    .line 1472
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mHideIconMinLevel:Z

    if-nez v2, :cond_2

    .line 1473
    invoke-virtual {p0, v3, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setCanvasSubVisiblity(ZI)V

    .line 1495
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCanvasMgr:Lcom/sec/samsung/gallery/glview/composeView/GlCanvasManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemMaxW:F

    float-to-int v3, v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemMaxH:F

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlCanvasManager;->findInstance(II)Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v0

    .line 1496
    .local v0, "canvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setCanvasSub(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 1497
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setViewSub(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 1498
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    goto :goto_0

    .line 1475
    .end local v0    # "canvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mPlayIconView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 1476
    const/16 v2, 0xd

    invoke-virtual {p1, v2}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mBorderView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 1477
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getExceptViews()Ljava/util/ArrayList;

    move-result-object v1

    .line 1479
    .local v1, "exceptViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/glcore/GlView;>;"
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mIsSplitView:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCurrent:I

    if-ne v2, v3, :cond_4

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1480
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1481
    invoke-virtual {p1, v3, v1}, Lcom/sec/android/gallery3d/glcore/GlView;->setChildVisibility(ILjava/util/ArrayList;)V

    .line 1482
    invoke-virtual {p0, v3, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setCanvasSubVisiblity(ZI)V

    goto :goto_1

    .line 1484
    :cond_3
    invoke-virtual {p0, v4, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setCanvasSubVisiblity(ZI)V

    goto :goto_1

    .line 1486
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCurrent:I

    if-nez v2, :cond_5

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsDecorAlwaysVisible:Z

    if-eqz v2, :cond_6

    .line 1487
    :cond_5
    invoke-virtual {p0, v3, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setCanvasSubVisiblity(ZI)V

    goto :goto_1

    .line 1488
    :cond_6
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 1489
    invoke-virtual {p1, v3, v1}, Lcom/sec/android/gallery3d/glcore/GlView;->setChildVisibility(ILjava/util/ArrayList;)V

    .line 1490
    invoke-virtual {p0, v3, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setCanvasSubVisiblity(ZI)V

    goto :goto_1

    .line 1492
    :cond_7
    invoke-virtual {p0, v4, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setCanvasSubVisiblity(ZI)V

    goto :goto_1

    .line 1499
    .end local v1    # "exceptViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/glcore/GlView;>;"
    :cond_8
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    if-eqz v2, :cond_0

    .line 1500
    invoke-virtual {p0, v4, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setCanvasSubVisiblity(ZI)V

    .line 1501
    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setCanvasSub(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 1502
    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setViewSub(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 1503
    iput-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    goto/16 :goto_0
.end method

.method public setDecorVisibility(Z)V
    .locals 1
    .param p1, "isVisible"    # Z

    .prologue
    .line 1508
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setCanvasSubVisiblity(ZI)V

    .line 1509
    return-void
.end method

.method protected setDimState(Z)V
    .locals 1
    .param p1, "isDim"    # Z

    .prologue
    .line 1453
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mDimState:Z

    if-ne v0, p1, :cond_0

    .line 1461
    :goto_0
    return-void

    .line 1455
    :cond_0
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mDimState:Z

    .line 1456
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mDimState:Z

    if-eqz v0, :cond_1

    .line 1457
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setDim(F)V

    goto :goto_0

    .line 1459
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setDim(F)V

    goto :goto_0
.end method

.method protected setSelected(Z)V
    .locals 2
    .param p1, "isSelected"    # Z

    .prologue
    .line 1441
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mSelectState:Z

    .line 1442
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mExpCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 1444
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setDim(F)V

    .line 1445
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setAlpha(F)V

    .line 1446
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v0, :cond_0

    .line 1447
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mCanvChecked:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 1450
    :cond_0
    return-void

    .line 1447
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mCanvUnchecked:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    goto :goto_0
.end method
