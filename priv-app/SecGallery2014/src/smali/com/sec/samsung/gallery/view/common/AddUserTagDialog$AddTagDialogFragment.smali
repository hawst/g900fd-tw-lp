.class Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;
.super Landroid/app/DialogFragment;
.source "AddUserTagDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AddTagDialogFragment"
.end annotation


# static fields
.field protected static final TAG:Ljava/lang/String; = "AddTagDialogFragment"


# instance fields
.field private final MAX_NAME_LENGTH:I

.field mAlertDialog:Landroid/app/AlertDialog;

.field private mClipVisible:Z

.field private final mContext:Landroid/content/Context;

.field private mInvalidToast:Landroid/widget/Toast;

.field private mTag:Ljava/lang/String;

.field private mTooLongToast:Landroid/widget/Toast;

.field private mUriList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;Landroid/content/Context;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 113
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 103
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    .line 105
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mTag:Ljava/lang/String;

    .line 106
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mClipVisible:Z

    .line 108
    const/16 v1, 0x40

    iput v1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->MAX_NAME_LENGTH:I

    .line 114
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mContext:Landroid/content/Context;

    .line 115
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 116
    .local v0, "args":Landroid/os/Bundle;
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 117
    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mTag:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mClipVisible:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mUriList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mInvalidToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mTooLongToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->startHandleAddTag()V

    return-void
.end method

.method private startHandleAddTag()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 401
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mUriList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 409
    :cond_0
    :goto_0
    return-void

    .line 404
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 405
    .local v0, "addUserTagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mTag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 407
    new-instance v1, Lcom/sec/samsung/gallery/view/common/AddUserTagUpdateTask;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mUriList:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v3, v0}, Lcom/sec/samsung/gallery/view/common/AddUserTagUpdateTask;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    new-array v2, v4, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/common/AddUserTagUpdateTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 408
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "EXIT_SELECTION_MODE"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 390
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 391
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 392
    .local v0, "d":Landroid/app/Dialog;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 393
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 394
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 413
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 416
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 121
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mContext:Landroid/content/Context;

    invoke-static {v8}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    const v9, 0x7f03001a

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 123
    .local v0, "alertDialogueView":Landroid/view/View;
    const/4 v8, 0x0

    invoke-static {v8}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->setSavedUserTag(Z)V

    .line 124
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mTooLongToast:Landroid/widget/Toast;

    if-nez v8, :cond_0

    .line 125
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0e01cc

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const/16 v13, 0x40

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v9, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mTooLongToast:Landroid/widget/Toast;

    .line 129
    :cond_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mInvalidToast:Landroid/widget/Toast;

    if-nez v8, :cond_1

    .line 130
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mContext:Landroid/content/Context;

    const v9, 0x7f0e0111

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mInvalidToast:Landroid/widget/Toast;

    .line 134
    :cond_1
    const v8, 0x7f0f0052

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 137
    .local v1, "alertEditText":Landroid/widget/EditText;
    const v8, 0x7f0f0054

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 138
    .local v6, "tagListButtonView":Landroid/widget/ImageView;
    new-instance v8, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$1;

    invoke-direct {v8, p0, v1}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$1;-><init>(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;Landroid/widget/EditText;)V

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getAllUserTag(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 147
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 148
    const v8, 0x7f020422

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 153
    :goto_0
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 155
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWritingPanel:Z

    if-eqz v8, :cond_2

    .line 156
    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Landroid/widget/EditText;->getInputExtras(Z)Landroid/os/Bundle;

    move-result-object v2

    .line 157
    .local v2, "b":Landroid/os/Bundle;
    const-string v8, "maxLength"

    const/16 v9, 0x40

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 160
    .end local v2    # "b":Landroid/os/Bundle;
    :cond_2
    const v8, 0x84000

    invoke-virtual {v1, v8}, Landroid/widget/EditText;->setInputType(I)V

    .line 162
    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 164
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->access$400(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v8

    if-nez v8, :cond_3

    .line 165
    new-instance v8, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$2;

    invoke-direct {v8, p0, v1}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$2;-><init>(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;Landroid/widget/EditText;)V

    const-wide/16 v10, 0x12c

    invoke-virtual {v1, v8, v10, v11}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 173
    :cond_3
    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 175
    new-instance v3, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$3;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$3;-><init>(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;)V

    .line 239
    .local v3, "filter":Landroid/text/InputFilter;
    new-instance v4, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$4;

    const/16 v8, 0x40

    invoke-direct {v4, p0, v8}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$4;-><init>(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;I)V

    .line 253
    .local v4, "lengthFilter":Landroid/text/InputFilter$LengthFilter;
    const-string v8, "inputType=filename;disableEmoticonInput=true"

    invoke-virtual {v1, v8}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 254
    const/4 v8, 0x2

    new-array v8, v8, [Landroid/text/InputFilter;

    const/4 v9, 0x0

    aput-object v3, v8, v9

    const/4 v9, 0x1

    aput-object v4, v8, v9

    invoke-virtual {v1, v8}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 258
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mTag:Ljava/lang/String;

    .line 260
    new-instance v8, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v9, 0x7f0e0472

    invoke-virtual {v8, v9}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const v9, 0x104000a

    new-instance v10, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$6;

    invoke-direct {v10, p0}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$6;-><init>(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;)V

    invoke-virtual {v8, v9, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const/high16 v9, 0x1040000

    new-instance v10, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$5;

    invoke-direct {v10, p0, v1}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$5;-><init>(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;Landroid/widget/EditText;)V

    invoke-virtual {v8, v9, v10}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    .line 275
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v8}, Landroid/app/AlertDialog;->show()V

    .line 277
    new-instance v8, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$7;

    invoke-direct {v8, p0}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$7;-><init>(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;)V

    invoke-virtual {v1, v8}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 290
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    const/4 v9, -0x1

    invoke-virtual {v8, v9}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v5

    .line 291
    .local v5, "positiveButton":Landroid/widget/Button;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mTag:Ljava/lang/String;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 292
    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Landroid/widget/Button;->setEnabled(Z)V

    .line 297
    :goto_1
    new-instance v8, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$8;

    invoke-direct {v8, p0, v1}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$8;-><init>(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;Landroid/widget/EditText;)V

    invoke-virtual {v5, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 308
    new-instance v7, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$9;

    invoke-direct {v7, p0}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$9;-><init>(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;)V

    .line 327
    .local v7, "textWatcher":Landroid/text/TextWatcher;
    invoke-virtual {v1, v7}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 328
    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 330
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-static {v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->hideStatusIcon(Ljava/lang/Object;)V

    .line 331
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    return-object v8

    .line 150
    .end local v3    # "filter":Landroid/text/InputFilter;
    .end local v4    # "lengthFilter":Landroid/text/InputFilter$LengthFilter;
    .end local v5    # "positiveButton":Landroid/widget/Button;
    .end local v7    # "textWatcher":Landroid/text/TextWatcher;
    :cond_4
    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 151
    const v8, 0x7f020421

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 294
    .restart local v3    # "filter":Landroid/text/InputFilter;
    .restart local v4    # "lengthFilter":Landroid/text/InputFilter$LengthFilter;
    .restart local v5    # "positiveButton":Landroid/widget/Button;
    :cond_5
    const/4 v8, 0x1

    invoke-virtual {v5, v8}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1
.end method

.method public onPause()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 369
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 371
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mReceiveTime:J
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->access$1300(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x96

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 372
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    # setter for: Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->isSipVisible:Z
    invoke-static {v1, v6}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->access$1002(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;Z)Z

    .line 374
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->access$1400(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 375
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->access$1400(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;)Landroid/content/BroadcastReceiver;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 377
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    const/4 v2, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->access$1402(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;

    .line 380
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mContext:Landroid/content/Context;

    const-string v2, "clipboardEx"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/sec/clipboard/ClipboardExManager;

    .line 381
    .local v0, "clipEx":Landroid/sec/clipboard/ClipboardExManager;
    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 382
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mClipVisible:Z

    .line 386
    :goto_0
    return-void

    .line 384
    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mClipVisible:Z

    goto :goto_0
.end method

.method public onResume()V
    .locals 6

    .prologue
    .line 337
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 339
    invoke-static {}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->isSavedUserTag()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 340
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->dismissAddTagDialog()V

    .line 342
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->access$800(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;)Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 343
    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 344
    const v2, 0x7f0f0052

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 345
    .local v1, "editText":Landroid/widget/EditText;
    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 346
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    # invokes: Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->setSipBroadcastReceiver()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->access$900(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;)V

    .line 347
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->isSipVisible:Z
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->access$1000(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 348
    new-instance v2, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$10;

    invoke-direct {v2, p0, v1}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$10;-><init>(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;Landroid/widget/EditText;)V

    const-wide/16 v4, 0x258

    invoke-virtual {v1, v2, v4, v5}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 365
    .end local v1    # "editText":Landroid/widget/EditText;
    :cond_1
    return-void
.end method

.method public setSelectedItemUriList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 397
    .local p1, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mUriList:Ljava/util/ArrayList;

    .line 398
    return-void
.end method
