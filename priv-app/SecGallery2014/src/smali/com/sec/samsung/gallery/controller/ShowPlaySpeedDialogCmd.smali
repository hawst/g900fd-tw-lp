.class public Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "ShowPlaySpeedDialogCmd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;,
        Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItem;
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;

.field private mContext:Landroid/content/Context;

.field private mOnItemClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private mPlaySpeedDialog:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 58
    new-instance v0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->mOnItemClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 81
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;)Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->mAdapter:Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->mPlaySpeedDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->mPlaySpeedDialog:Landroid/app/AlertDialog;

    return-object p1
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 2
    .param p1, "notification"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 31
    const/4 v0, 0x0

    .line 32
    .local v0, "params":[Ljava/lang/Object;
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    .line 33
    const/4 v1, 0x0

    aget-object v1, v0, v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->mContext:Landroid/content/Context;

    .line 34
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->showSubOptions()V

    .line 35
    return-void
.end method

.method protected showSubOptions()V
    .locals 5

    .prologue
    .line 38
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->mPlaySpeedDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->mPlaySpeedDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 40
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 41
    .local v0, "itemPool":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItem;>;"
    new-instance v1, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItem;

    const v2, 0x7f02017b

    const v3, 0x7f0e01b6

    const v4, 0x7f0e01bb

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItem;-><init>(Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;III)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    new-instance v1, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItem;

    const v2, 0x7f02017c

    const v3, 0x7f0e01b7

    const v4, 0x7f0e01bc

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItem;-><init>(Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;III)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    new-instance v1, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItem;

    const v2, 0x7f02017d

    const v3, 0x7f0e01b8

    const v4, 0x7f0e01bd

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItem;-><init>(Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;III)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    new-instance v1, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItem;

    const v2, 0x7f02017e

    const v3, 0x7f0e01b9

    const v4, 0x7f0e01be

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItem;-><init>(Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;III)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    new-instance v1, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->mContext:Landroid/content/Context;

    const v3, 0x7f0300d7

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;-><init>(Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->mAdapter:Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;

    .line 48
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e01b5

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->mAdapter:Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->mOnItemClickListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->mPlaySpeedDialog:Landroid/app/AlertDialog;

    .line 53
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->mPlaySpeedDialog:Landroid/app/AlertDialog;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 54
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->mPlaySpeedDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 56
    .end local v0    # "itemPool":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItem;>;"
    :cond_1
    return-void
.end method
