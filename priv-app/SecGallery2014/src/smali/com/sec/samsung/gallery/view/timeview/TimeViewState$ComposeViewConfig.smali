.class Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;
.super Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;
.source "TimeViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/timeview/TimeViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ComposeViewConfig"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2813
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;-><init>()V

    .line 2814
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mUsePenSelectInPickMode:Z

    .line 2815
    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$500(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    .line 2816
    .local v0, "lauchMode":Lcom/sec/samsung/gallery/core/LaunchModeType;
    sget-object v2, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v2, :cond_0

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mUseEnlargeAnim:Z

    .line 2817
    sget-object v2, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v2, :cond_1

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsEasyMode:Z
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$6500(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Z

    move-result v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mUseLayoutChange:Z

    .line 2819
    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$500(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    .line 2820
    .local v1, "tabTagType":Lcom/sec/samsung/gallery/core/TabTagType;
    sget-object v2, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v2, :cond_2

    move v2, v3

    :goto_2
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mUseGroupSelect:Z

    .line 2821
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mUseGroupTitle:Z

    .line 2822
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mUseItemSelect:Z

    .line 2825
    iput v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mInitialLevel:I

    .line 2826
    iput v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mMinLevel:I

    .line 2827
    iput v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mMaxLevel:I

    .line 2828
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mTopGroupTitle:Z

    .line 2829
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mUseQuickScroll:Z

    .line 2830
    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$500(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mViewTabType:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 2831
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const-class v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;

    aput-object v5, v2, v4

    const-class v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;

    aput-object v4, v2, v3

    const-class v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;

    aput-object v3, v2, v6

    const/4 v3, 0x3

    const-class v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;

    aput-object v4, v2, v3

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mPosCtrl:[Ljava/lang/Object;

    .line 2834
    return-void

    .end local v1    # "tabTagType":Lcom/sec/samsung/gallery/core/TabTagType;
    :cond_0
    move v2, v4

    .line 2816
    goto :goto_0

    :cond_1
    move v2, v4

    .line 2817
    goto :goto_1

    .restart local v1    # "tabTagType":Lcom/sec/samsung/gallery/core/TabTagType;
    :cond_2
    move v2, v4

    .line 2820
    goto :goto_2
.end method

.method static synthetic access$5600(Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;ZI)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;
    .param p1, "x1"    # Z
    .param p2, "x2"    # I

    .prologue
    .line 2811
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->setInitalLevel(ZI)V

    return-void
.end method

.method private setInitalLevel(ZI)V
    .locals 2
    .param p1, "isEasyMode"    # Z
    .param p2, "level"    # I

    .prologue
    const/4 v0, 0x3

    const/4 v1, 0x2

    .line 2837
    if-eqz p1, :cond_0

    .line 2838
    iput v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mInitialLevel:I

    .line 2839
    iput v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mMinLevel:I

    .line 2840
    iput v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mMaxLevel:I

    .line 2850
    :goto_0
    return-void

    .line 2841
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryAppImpl;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$6600(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->isFestivalMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2842
    iput v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mInitialLevel:I

    .line 2843
    iput v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mMinLevel:I

    .line 2844
    iput v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mMaxLevel:I

    goto :goto_0

    .line 2846
    :cond_1
    iput p2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mInitialLevel:I

    .line 2847
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$6700(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c007c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mMinLevel:I

    .line 2848
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$6800(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c007d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mMaxLevel:I

    goto :goto_0
.end method
