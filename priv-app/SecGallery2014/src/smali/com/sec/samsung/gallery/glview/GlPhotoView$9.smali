.class Lcom/sec/samsung/gallery/glview/GlPhotoView$9;
.super Ljava/lang/Object;
.source "GlPhotoView.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GContentObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 0

    .prologue
    .line 1215
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1218
    const-string v0, "GlPhotoView"

    const-string v1, "CMD_REQ_LOAD"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1219
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    .line 1220
    return-void
.end method

.method public onChanged(II)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "parm"    # I

    .prologue
    const/4 v2, 0x0

    .line 1224
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mVisibleFirst:I

    if-gt v0, p1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mVisibleLast:I

    if-gt p1, v0, :cond_0

    .line 1226
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1, v2, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    .line 1228
    :cond_0
    return-void
.end method
