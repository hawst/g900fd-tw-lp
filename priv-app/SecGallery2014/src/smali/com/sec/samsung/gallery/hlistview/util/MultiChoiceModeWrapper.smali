.class public Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;
.super Ljava/lang/Object;
.source "MultiChoiceModeWrapper.java"

# interfaces
.implements Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeListener;


# instance fields
.field private mView:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

.field private mWrapped:Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeListener;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V
    .locals 0
    .param p1, "view"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;->mView:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    .line 33
    return-void
.end method


# virtual methods
.method public hasWrappedCallback()Z
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;->mWrapped:Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;->mWrapped:Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeListener;

    invoke-interface {v0, p1, p2}, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeListener;->onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 2
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v0, 0x0

    .line 45
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;->mWrapped:Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeListener;

    invoke-interface {v1, p1, p2}, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeListener;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;->mView:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setLongClickable(Z)V

    .line 47
    const/4 v0, 0x1

    .line 49
    :cond_0
    return v0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 3
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    const/4 v2, 0x1

    .line 64
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;->mWrapped:Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeListener;

    invoke-interface {v0, p1}, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeListener;->onDestroyActionMode(Landroid/view/ActionMode;)V

    .line 65
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;->mView:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceActionMode:Ljava/lang/Object;

    .line 68
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;->mView:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->clearChoices()V

    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;->mView:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iput-boolean v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataChanged:Z

    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;->mView:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->rememberSyncState()V

    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;->mView:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->requestLayout()V

    .line 72
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;->mView:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setLongClickable(Z)V

    .line 73
    return-void
.end method

.method public onItemCheckedStateChanged(Landroid/view/ActionMode;IJZ)V
    .locals 7
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "position"    # I
    .param p3, "id"    # J
    .param p5, "checked"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 78
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;->mWrapped:Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeListener;

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    move v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeListener;->onItemCheckedStateChanged(Landroid/view/ActionMode;IJZ)V

    .line 81
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;->mView:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getCheckedItemCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 82
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    .line 84
    :cond_0
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;->mWrapped:Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeListener;

    invoke-interface {v0, p1, p2}, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeListener;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public setWrapped(Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeListener;)V
    .locals 0
    .param p1, "wrapped"    # Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeListener;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;->mWrapped:Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeListener;

    .line 37
    return-void
.end method
