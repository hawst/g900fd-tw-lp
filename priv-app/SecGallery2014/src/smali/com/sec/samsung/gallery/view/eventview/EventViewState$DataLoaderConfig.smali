.class Lcom/sec/samsung/gallery/view/eventview/EventViewState$DataLoaderConfig;
.super Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;
.source "EventViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/eventview/EventViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DataLoaderConfig"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1184
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$DataLoaderConfig;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;-><init>()V

    .line 1185
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$DataLoaderConfig;->mScanFirstOnly:Z

    .line 1186
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$DataLoaderConfig;->mCheckSetVersion:Z

    .line 1187
    return-void
.end method


# virtual methods
.method public setAlbumAttribute(Lcom/sec/android/gallery3d/data/MediaSet;I)I
    .locals 2
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "count"    # I

    .prologue
    const/4 v0, 0x0

    .line 1191
    const/16 v1, 0x20

    iput-byte v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$DataLoaderConfig;->mRetThmType:B

    .line 1192
    iput v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$DataLoaderConfig;->mRetLineCount:I

    .line 1193
    if-lez p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
