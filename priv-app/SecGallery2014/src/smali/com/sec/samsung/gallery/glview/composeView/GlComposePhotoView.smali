.class public Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;
.super Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
.source "GlComposePhotoView.java"


# static fields
.field public static final OPTION_NO_SPLIT:I = 0x200

.field public static SCROLL_AREA:F = 0.0f

.field public static SPLIT_RATIO_LAND:F = 0.0f

.field public static SPLIT_RATIO_PORT:F = 0.0f

.field public static final SPREAD_ANIM_ACTIVE:I = 0x4

.field public static final SPREAD_ANIM_COND_OBJ:I = 0x3

.field public static final SPREAD_ANIM_COND_SIZE:I = 0x2

.field public static final SPREAD_ANIM_IDLE:I = 0x0

.field public static final SPREAD_ANIM_READY:I = 0x1

.field public static final STATUS_ADD_TO_NEWALBUM:I = 0xb

.field public static final STATUS_EXPAND_CHANGE:I = 0xa

.field public static final STATUS_ITEM_MOVE:I = 0xc

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mExpandable:Z

.field private mFlags:I

.field private mGatherAnim:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

.field mListenerDragDone:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$OnDragAnimListener;

.field public mNotNeedToResetLayout:Z

.field public mObjForSpreadAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field public mOriginExpanded:Z

.field private mPressX:I

.field private mPressY:I

.field private mPressed:Z

.field public mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

.field private mSpreadAnim:Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;

.field private mSpreadAnimListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

.field private mSpreadAnimState:I

.field private mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field public mSupportExpand:Z

.field private mValidView:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->TAG:Ljava/lang/String;

    .line 34
    const/high16 v0, 0x425c0000    # 55.0f

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->SCROLL_AREA:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/sec/android/gallery3d/data/MediaItem;ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "initCode"    # I
    .param p3, "initItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p4, "option"    # I
    .param p5, "config"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    .prologue
    const/4 v3, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58
    invoke-direct/range {p0 .. p5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;-><init>(Landroid/content/Context;ILcom/sec/android/gallery3d/data/MediaItem;ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;)V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mObjForSpreadAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 48
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSpreadAnimState:I

    .line 49
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mExpandable:Z

    .line 50
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPressed:Z

    .line 51
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSupportExpand:Z

    .line 52
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mFlags:I

    .line 53
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mOriginExpanded:Z

    .line 54
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mNotNeedToResetLayout:Z

    .line 434
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView$1;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mListenerDragDone:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$OnDragAnimListener;

    .line 443
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView$2;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSpreadAnimListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 60
    iget v0, p5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mLandRatio:F

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->SPLIT_RATIO_LAND:F

    .line 61
    iget v0, p5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mPortRatio:F

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->SPLIT_RATIO_PORT:F

    .line 62
    and-int/lit16 v0, p4, 0x200

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSupportExpand:Z

    .line 63
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionAndSplitPinch:Z

    if-eqz v0, :cond_1

    .line 64
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mIsSplitViewExpanded:Z

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    .line 67
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 62
    goto :goto_0

    .line 66
    :cond_1
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mIsEasyMode:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPosCtrlCurrent:I

    if-gt v0, v3, :cond_3

    :cond_2
    :goto_2
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2

    :cond_4
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPosCtrlCurrent:I

    if-ne v0, v3, :cond_2

    move v1, v2

    goto :goto_2
.end method

.method private isEnlargeAnimActive(F)Z
    .locals 4
    .param p1, "scale"    # F

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    .line 248
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, p1, v1

    if-lez v1, :cond_3

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionAndSplitPinch:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mMode:I

    if-eq v1, v0, :cond_3

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mMode:I

    if-eq v1, v3, :cond_3

    :cond_0
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    if-nez v1, :cond_1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPosCtrlCurrent:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    :cond_1
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPosCtrlCurrent:I

    if-ne v1, v3, :cond_3

    :cond_2
    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private processScrollToward(Z)V
    .locals 4
    .param p1, "active"    # Z

    .prologue
    .line 405
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mGatherAnim:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->getScrollIntensity()F

    move-result v0

    .line 407
    .local v0, "intensity":F
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    neg-float v2, v0

    const/high16 v3, 0x41c80000    # 25.0f

    mul-float/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setMoveToward(F)V

    .line 408
    return-void

    .line 405
    .end local v0    # "intensity":F
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private processTrailAnim(II)V
    .locals 4
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 392
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mWidthSpace:F

    int-to-float v3, p1

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mWidth:I

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 393
    .local v0, "sx":F
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mHeightSpace:F

    int-to-float v3, p2

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mHeight:I

    int-to-float v3, v3

    div-float v1, v2, v3

    .line 395
    .local v1, "sy":F
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mGatherAnim:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    invoke-virtual {v2, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->setTargetPos(FF)V

    .line 396
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mMode:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 397
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mGatherAnim:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->findFocused(Landroid/util/SparseArray;)V

    .line 402
    :goto_0
    return-void

    .line 399
    :cond_0
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->processScrollToward(Z)V

    .line 400
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mGatherAnim:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->findFocused(Landroid/util/SparseArray;)V

    goto :goto_0
.end method

.method private setGatherAnimScrollArea()V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 372
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mWideMode:Z

    if-eqz v4, :cond_0

    sget v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->SPLIT_RATIO_LAND:F

    .line 373
    .local v1, "ratio":F
    :goto_0
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mWidthSpace:F

    div-float/2addr v4, v6

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mWidthSpace:F

    mul-float/2addr v5, v1

    sub-float v2, v4, v5

    .line 374
    .local v2, "split":F
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mHeightSpace:F

    div-float/2addr v4, v6

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->getActionBarHeight()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->convY(I)F

    move-result v5

    sub-float/2addr v4, v5

    sget v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->SCROLL_AREA:F

    sub-float v3, v4, v5

    .line 375
    .local v3, "upArea":F
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mHeightSpace:F

    neg-float v4, v4

    div-float/2addr v4, v6

    sget v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->SCROLL_AREA:F

    add-float v0, v4, v5

    .line 377
    .local v0, "downArea":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mGatherAnim:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    invoke-virtual {v4, v2, v3, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->setScrollArea(FFF)V

    .line 378
    return-void

    .line 372
    .end local v0    # "downArea":F
    .end local v1    # "ratio":F
    .end local v2    # "split":F
    .end local v3    # "upArea":F
    :cond_0
    sget v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->SPLIT_RATIO_PORT:F

    goto :goto_0
.end method

.method private setValidView()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 107
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mWideMode:Z

    if-eqz v2, :cond_0

    sget v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->SPLIT_RATIO_LAND:F

    .line 108
    .local v0, "ratio":F
    :goto_0
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mWidth:I

    int-to-float v2, v2

    mul-float/2addr v2, v0

    float-to-int v1, v2

    .line 110
    .local v1, "splitVWidth":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mValidView:Landroid/graphics/Rect;

    if-nez v2, :cond_1

    .line 111
    new-instance v2, Landroid/graphics/Rect;

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mWidth:I

    sub-int/2addr v3, v1

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mWidth:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mHeight:I

    invoke-direct {v2, v3, v6, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mValidView:Landroid/graphics/Rect;

    .line 114
    :goto_1
    return-void

    .line 107
    .end local v0    # "ratio":F
    .end local v1    # "splitVWidth":I
    :cond_0
    sget v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->SPLIT_RATIO_PORT:F

    goto :goto_0

    .line 113
    .restart local v0    # "ratio":F
    .restart local v1    # "splitVWidth":I
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mValidView:Landroid/graphics/Rect;

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mWidth:I

    sub-int/2addr v3, v1

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mWidth:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mHeight:I

    invoke-virtual {v2, v3, v6, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_1
.end method

.method private startTrailAnim(IZII)V
    .locals 6
    .param p1, "index"    # I
    .param p2, "isNewAlbum"    # Z
    .param p3, "pressedX"    # I
    .param p4, "pressedY"    # I

    .prologue
    .line 381
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v2

    .line 382
    .local v2, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->getNewAlbumObject()Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v3

    .line 384
    .local v3, "dstObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :goto_0
    if-nez v2, :cond_1

    .line 389
    :goto_1
    return-void

    .line 382
    .end local v3    # "dstObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->getFirstThumbObject()Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v3

    goto :goto_0

    .line 387
    .restart local v3    # "dstObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mGatherAnim:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->startAnimation(Landroid/util/SparseArray;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;II)V

    .line 388
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mGatherAnim:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->startDragAnimation()V

    goto :goto_1
.end method


# virtual methods
.method public addToUpdateQueue(II)V
    .locals 4
    .param p1, "code"    # I
    .param p2, "type"    # I

    .prologue
    const/4 v3, 0x0

    .line 118
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->addToUpdateQueue(II)V

    .line 119
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbumHeaderFocused:Z

    if-eqz v2, :cond_0

    if-nez p1, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v2, :cond_0

    .line 120
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v2, v3, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    .line 121
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v2, v3, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getItemImage(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 123
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v2, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->updateNewAlbumImage(Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 125
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    return-void
.end method

.method public checkStartSpreadAnimation(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)V
    .locals 5
    .param p1, "object"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .param p2, "sizeUpdated"    # Z

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 315
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSpreadAnimState:I

    if-nez v0, :cond_1

    .line 342
    :cond_0
    :goto_0
    return-void

    .line 318
    :cond_1
    if-eqz p1, :cond_2

    .line 319
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mObjForSpreadAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 322
    :cond_2
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSpreadAnimState:I

    if-ne v0, v2, :cond_4

    .line 323
    if-eqz p2, :cond_3

    .line 324
    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSpreadAnimState:I

    goto :goto_0

    .line 325
    :cond_3
    if-eqz p1, :cond_0

    .line 326
    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSpreadAnimState:I

    goto :goto_0

    .line 330
    :cond_4
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSpreadAnimState:I

    if-ne v0, v3, :cond_5

    if-nez p2, :cond_0

    .line 332
    :cond_5
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSpreadAnimState:I

    if-ne v0, v4, :cond_6

    if-nez p1, :cond_0

    .line 336
    :cond_6
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->resetUpdateQueue()V

    .line 337
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 338
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setScroll(FZ)V

    .line 339
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSpreadAnim:Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mObjForSpreadAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->startAnimation(Landroid/util/SparseArray;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V

    .line 340
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mObjForSpreadAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 341
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSpreadAnimState:I

    goto :goto_0
.end method

.method protected getNextLevel(II)I
    .locals 2
    .param p1, "now"    # I
    .param p2, "toward"    # I

    .prologue
    const/4 v0, 0x2

    .line 235
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionAndSplitPinch:Z

    if-nez v1, :cond_2

    .line 236
    if-lez p2, :cond_1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPosCtrlCurrent:I

    if-ge v1, v0, :cond_1

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    if-nez v1, :cond_1

    .line 238
    const/4 v0, 0x3

    .line 244
    :cond_0
    :goto_0
    return v0

    .line 240
    :cond_1
    if-lez p2, :cond_2

    if-ne p1, v0, :cond_2

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    if-nez v1, :cond_0

    .line 244
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getNextLevel(II)I

    move-result v0

    goto :goto_0
.end method

.method protected handleLongClick(III)Z
    .locals 5
    .param p1, "index"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 291
    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/core/TabTagType;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 292
    .local v0, "isAlbumState":Z
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->handleLongClick(I)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    if-eqz v3, :cond_0

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDragAndDropInExpandSplitView:Z

    if-eqz v3, :cond_3

    :cond_0
    if-eqz v0, :cond_3

    .line 293
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    if-eqz v3, :cond_1

    .line 294
    invoke-virtual {p0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setExpanded(ZZ)V

    .line 295
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mOriginExpanded:Z

    .line 297
    :cond_1
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mMode:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    move v1, v2

    :cond_2
    invoke-direct {p0, p1, v1, p2, p3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->startTrailAnim(IZII)V

    .line 300
    :goto_0
    return v2

    :cond_3
    move v2, v1

    goto :goto_0
.end method

.method public isDragAnimRunning()Z
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mGatherAnim:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    if-nez v0, :cond_0

    .line 129
    const/4 v0, 0x0

    .line 130
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mGatherAnim:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->isActive()Z

    move-result v0

    goto :goto_0
.end method

.method public isExpanded()Z
    .locals 1

    .prologue
    .line 345
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    return v0
.end method

.method public isOriginExpanded()Z
    .locals 1

    .prologue
    .line 304
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mOriginExpanded:Z

    return v0
.end method

.method public markStartSpreadAnimation()V
    .locals 1

    .prologue
    .line 311
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSpreadAnimState:I

    .line 312
    return-void
.end method

.method protected onClickThumbnail(Lcom/sec/android/gallery3d/glcore/GlObject;)Z
    .locals 1
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 256
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSpreadAnim:Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate()V
    .locals 2

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setValidView()V

    .line 72
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onCreate()V

    .line 74
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSpreadAnim:Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;

    .line 75
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSpreadAnim:Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSpreadAnimListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 76
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;-><init>(Landroid/content/Context;Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mGatherAnim:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    .line 77
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mGatherAnim:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mListenerDragDone:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$OnDragAnimListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->setOnDragAnimListener(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$OnDragAnimListener;)V

    .line 78
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setGatherAnimScrollArea()V

    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    iput-boolean v1, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mIsSplitViewExpanded:Z

    .line 80
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mIsSplitView:Z

    .line 82
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    if-eqz v0, :cond_0

    .line 83
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPosCtrlCurrent:I

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 87
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlingInTalkBack:Z

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mGlComposeViewAccessibility:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mComposeViewItemAccessibilityListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->setAccessibilityNodeInfoListener(Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;)V

    .line 90
    :cond_1
    return-void

    .line 80
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 465
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->isSlideShowMode:Z

    if-eqz v3, :cond_1

    .line 491
    :cond_0
    :goto_0
    return v1

    .line 467
    :cond_1
    const/16 v3, 0xa8

    if-eq p1, v3, :cond_2

    const/16 v3, 0xa9

    if-eq p1, v3, :cond_2

    const/16 v3, 0x118

    if-eq p1, v3, :cond_2

    const/16 v3, 0x117

    if-ne p1, v3, :cond_3

    :cond_2
    move v1, v2

    .line 469
    goto :goto_0

    .line 472
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getCount()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->isScreenLocked()Z

    move-result v3

    if-nez v3, :cond_0

    .line 475
    const/16 v3, 0x42

    if-eq p1, v3, :cond_4

    const/16 v3, 0x17

    if-ne p1, v3, :cond_7

    .line 476
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-ltz v3, :cond_6

    .line 477
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    if-eqz v3, :cond_6

    .line 478
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v0

    .line 479
    .local v0, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 480
    if-eqz v0, :cond_0

    .line 481
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mMode:I

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v3, v4, v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->prepareAnimation(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)Z

    move-result v3

    if-eqz v3, :cond_5

    move v1, v2

    .line 483
    goto :goto_0

    .line 485
    :cond_5
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mThis:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-interface {v3, v4, v5, v1, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;->onItemClick(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/android/gallery3d/glcore/GlView;II)Z

    .line 486
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v4, 0x4

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {v3, v4, v5, v1, v1}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .end local v0    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_6
    move v1, v2

    .line 489
    goto :goto_0

    .line 491
    :cond_7
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method protected onMoved(II)Z
    .locals 6
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 148
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPressed:Z

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mOnScaling:Z

    if-eqz v4, :cond_2

    :cond_0
    move v2, v3

    .line 169
    :cond_1
    :goto_0
    return v2

    .line 151
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mGatherAnim:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->isActive()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 152
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->processTrailAnim(II)V

    goto :goto_0

    .line 155
    :cond_3
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSupportExpand:Z

    if-eqz v4, :cond_4

    .line 156
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 157
    .local v0, "absDx":I
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 158
    .local v1, "absDy":I
    const/16 v4, 0x32

    if-le v1, v4, :cond_5

    .line 159
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mExpandable:Z

    .line 169
    .end local v0    # "absDx":I
    .end local v1    # "absDy":I
    :cond_4
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onMoved(II)Z

    move-result v2

    goto :goto_0

    .line 160
    .restart local v0    # "absDx":I
    .restart local v1    # "absDy":I
    :cond_5
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mExpandable:Z

    if-eqz v4, :cond_4

    const/16 v4, 0x64

    if-le v0, v4, :cond_4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mMode:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    .line 161
    if-gez p1, :cond_6

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    if-nez v4, :cond_6

    .line 162
    invoke-virtual {p0, v2, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setExpanded(ZZ)V

    goto :goto_0

    .line 163
    :cond_6
    if-lez p1, :cond_1

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    if-eqz v4, :cond_1

    .line 164
    invoke-virtual {p0, v3, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setExpanded(ZZ)V

    goto :goto_0
.end method

.method protected onPressed(II)Z
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 135
    if-ne p1, v1, :cond_0

    if-eq p2, v1, :cond_1

    :cond_0
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mValidView:Landroid/graphics/Rect;

    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 136
    :cond_1
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPressX:I

    .line 137
    iput p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPressY:I

    .line 138
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPressed:Z

    .line 139
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mExpandable:Z

    .line 140
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onPressed(II)Z

    move-result v0

    .line 143
    :goto_0
    return v0

    .line 142
    :cond_2
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPressed:Z

    goto :goto_0
.end method

.method protected onReleased(IIII)Z
    .locals 2
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "speedX"    # I
    .param p4, "speedY"    # I

    .prologue
    const/4 v0, 0x1

    .line 174
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPressed:Z

    if-nez v1, :cond_0

    .line 175
    const/4 v0, 0x0

    .line 181
    :goto_0
    return v0

    .line 177
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mGatherAnim:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->isActive()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 178
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->stopDragAnim(Z)V

    goto :goto_0

    .line 181
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onReleased(IIII)Z

    move-result v0

    goto :goto_0
.end method

.method protected onScale(Landroid/view/ScaleGestureDetector;)V
    .locals 12
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 186
    iget-boolean v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mUseScaleCtrl:Z

    if-eqz v8, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->checkIfIncorrectMode()Z

    move-result v8

    if-nez v8, :cond_0

    iget-boolean v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mOnScaling:Z

    if-nez v8, :cond_1

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 188
    :cond_1
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v7

    .line 189
    .local v7, "scale":F
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    iget-boolean v8, v8, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mActive:Z

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->isRunning()Z

    move-result v8

    if-nez v8, :cond_2

    .line 190
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    invoke-virtual {v8, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->setScale(F)V

    goto :goto_0

    .line 193
    :cond_2
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mScaleCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

    if-eqz v8, :cond_0

    .line 195
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mScaleCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mScale:F

    mul-float v2, v8, v7

    .line 196
    .local v2, "cScale":F
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->isEnlargeAnimActive(F)Z

    move-result v0

    .line 198
    .local v0, "animActive":Z
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    if-eqz v8, :cond_4

    .line 199
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->isIdle()Z

    move-result v1

    .line 200
    .local v1, "animIdle":Z
    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    .line 202
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mScaleCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-virtual {v8, v9, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->doScale(FF)Z

    .line 203
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mScaleCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mScaleCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPosCtrlCurrent:I

    iput v10, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mToLevel:I

    iput v10, v8, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFromLevel:I

    .line 204
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v8

    float-to-int v3, v8

    .line 205
    .local v3, "focusX":I
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v8

    float-to-int v4, v8

    .line 206
    .local v4, "focusY":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v8, v3, v4, v11}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getFocusedObject(IIZ)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    move-result-object v5

    .line 207
    .local v5, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz v5, :cond_0

    invoke-virtual {p0, p1, v5, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->checkDisablePinchZoom(Landroid/view/ScaleGestureDetector;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;II)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 208
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v8, v9, v5, v11}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->prepareAnimation(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)Z

    goto :goto_0

    .line 211
    .end local v3    # "focusX":I
    .end local v4    # "focusY":I
    .end local v5    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_3
    if-nez v1, :cond_4

    .line 212
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    invoke-virtual {v8, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->setScale(F)I

    move-result v6

    .line 213
    .local v6, "result":I
    if-eqz v6, :cond_0

    .line 218
    .end local v1    # "animIdle":Z
    .end local v6    # "result":I
    :cond_4
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionAndSplitPinch:Z

    if-nez v8, :cond_5

    iget-boolean v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    if-eqz v8, :cond_0

    .line 220
    :cond_5
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mScaleCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

    invoke-virtual {v8, v2, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->doScale(FF)Z

    goto/16 :goto_0
.end method

.method protected onSetMode(IILjava/lang/Object;)V
    .locals 2
    .param p1, "mode"    # I
    .param p2, "parm"    # I
    .param p3, "obj"    # Ljava/lang/Object;

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->setMode(IILjava/lang/Object;)V

    .line 228
    :cond_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 229
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setExpanded(ZZ)V

    .line 231
    :cond_1
    return-void
.end method

.method protected resetLayout()V
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mNotNeedToResetLayout:Z

    if-eqz v0, :cond_0

    .line 104
    :goto_0
    return-void

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSpreadAnim:Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSpreadAnim:Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->stop()V

    .line 101
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setValidView()V

    .line 102
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->resetLayout()V

    .line 103
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setGatherAnimScrollArea()V

    goto :goto_0
.end method

.method public setExpanded(ZZ)V
    .locals 4
    .param p1, "expand"    # Z
    .param p2, "playAnim"    # Z

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 349
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mScaleAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->isFadeAnimRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 351
    :cond_1
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    .line 352
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    iput-boolean v1, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mIsSplitViewExpanded:Z

    .line 353
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mIsSplitView:Z

    .line 354
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mScaleCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPressX:I

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPressY:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->prepareScale(II)Z

    .line 355
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitViewExpanded:Z

    if-eqz v0, :cond_3

    .line 356
    sget v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->SCALE_LOWER:F

    invoke-virtual {p0, v3, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->animateScale(FF)V

    .line 357
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->fadeOut()V

    goto :goto_0

    .line 353
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 359
    :cond_3
    sget v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->SCALE_UPPER:F

    invoke-virtual {p0, v3, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->animateScale(FF)V

    .line 360
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->fadeIn()V

    goto :goto_0
.end method

.method public setFlags(IZ)V
    .locals 2
    .param p1, "flag"    # I
    .param p2, "set"    # Z

    .prologue
    .line 365
    if-eqz p2, :cond_0

    .line 366
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mFlags:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mFlags:I

    .line 369
    :goto_0
    return-void

    .line 368
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mFlags:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mFlags:I

    goto :goto_0
.end method

.method public setOriginExpanded(Z)V
    .locals 0
    .param p1, "expanded"    # Z

    .prologue
    .line 307
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mOriginExpanded:Z

    .line 308
    return-void
.end method

.method protected startEnlargeAnimation()V
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->addFadeOutObjs(Landroid/util/SparseArray;)V

    .line 275
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->addFadeOutObj(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 277
    :cond_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->startEnlargeAnimation()V

    .line 278
    return-void
.end method

.method protected startShrinkAnimation()V
    .locals 2

    .prologue
    .line 282
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->addFadeInObj(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V

    .line 284
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mBgObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->addFadeInObj(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V

    .line 286
    :cond_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->startShrinkAnimation()V

    .line 287
    return-void
.end method

.method public stopDragAnim(Z)V
    .locals 6
    .param p1, "confirmed"    # Z

    .prologue
    const/4 v2, 0x0

    .line 414
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->processScrollToward(Z)V

    .line 415
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mGatherAnim:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->isLastAnimRunning()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 432
    :goto_0
    return-void

    .line 417
    :cond_0
    if-eqz p1, :cond_4

    .line 418
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mGatherAnim:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->isFocused()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 419
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mGatherAnim:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->startMoveToAnimation()V

    .line 420
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mMode:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 421
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mGatherAnim:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->getFocusedObject()Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v1

    .line 422
    .local v1, "toObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-nez v1, :cond_2

    move v0, v2

    .line 423
    .local v0, "albumIndex":I
    :goto_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mThis:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;

    const/16 v5, 0xc

    invoke-interface {v3, v4, v5, v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;->onStatusChange(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;III)V

    .line 431
    .end local v0    # "albumIndex":I
    .end local v1    # "toObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_1
    :goto_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mGatherAnim:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->resetSourceDim(Z)V

    goto :goto_0

    .line 422
    .restart local v1    # "toObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_2
    iget v3, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    shr-int/lit8 v0, v3, 0x10

    goto :goto_1

    .line 426
    .end local v1    # "toObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mGatherAnim:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->startInverseDragAnimation()V

    goto :goto_2

    .line 429
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mGatherAnim:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->resetDrag()V

    goto :goto_2
.end method

.method public updateFolderItem(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 260
    return-void
.end method

.method protected updateSize(I)V
    .locals 4
    .param p1, "size"    # I

    .prologue
    const/4 v1, 0x1

    .line 264
    sget-object v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateSize = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", spreadObj = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mObjForSpreadAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", spreadState = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mSpreadAnimState:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->updateSize(I)V

    .line 268
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->checkStartSpreadAnimation(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)V

    .line 269
    return-void

    .line 264
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
