.class Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$7;
.super Ljava/lang/Object;
.source "GallerySearchView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)V
    .locals 0

    .prologue
    .line 442
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$7;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 444
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$7;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnSuggestionListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnSuggestionListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$1300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnSuggestionListener;

    move-result-object v0

    invoke-interface {v0, p3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnSuggestionListener;->onSuggestionClick(I)Z

    .line 445
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$7;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setImeVisibility(Z)V

    .line 446
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$7;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->dismissDropDown()V

    .line 447
    return-void
.end method
