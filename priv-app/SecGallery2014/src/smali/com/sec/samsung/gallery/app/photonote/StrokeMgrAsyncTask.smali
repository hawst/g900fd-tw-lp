.class public Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;
.super Landroid/os/AsyncTask;
.source "StrokeMgrAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/samsung/android/sdk/pen/document/SpenPageDoc;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/samsung/android/strokemanager/data/CandidateData;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final MSG_PHOTOFRAME_INSERT:I = 0x1


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mDbHelper:Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

.field private mFilePath:Ljava/lang/String;

.field private mInsertHandler:Landroid/os/Handler;

.field private mIsAbleToSave:Z

.field private mStrokeMgr:Lcom/samsung/android/strokemanager/StrokeManager;

.field private mUri:Landroid/net/Uri;

.field private mValues:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "Language"    # Ljava/lang/String;
    .param p3, "filePath"    # Ljava/lang/String;
    .param p4, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 21
    iput-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mContext:Landroid/content/Context;

    .line 22
    iput-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mStrokeMgr:Lcom/samsung/android/strokemanager/StrokeManager;

    .line 23
    iput-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mFilePath:Ljava/lang/String;

    .line 24
    iput-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mUri:Landroid/net/Uri;

    .line 25
    iput-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mDbHelper:Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mIsAbleToSave:Z

    .line 27
    iput-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mInsertHandler:Landroid/os/Handler;

    .line 30
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mValues:[Ljava/lang/String;

    .line 34
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mContext:Landroid/content/Context;

    .line 35
    new-instance v0, Lcom/samsung/android/strokemanager/StrokeManager;

    invoke-direct {v0, p1}, Lcom/samsung/android/strokemanager/StrokeManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mStrokeMgr:Lcom/samsung/android/strokemanager/StrokeManager;

    .line 36
    iput-object p3, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mFilePath:Ljava/lang/String;

    .line 37
    iput-object p4, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mUri:Landroid/net/Uri;

    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 40
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask$1;-><init>(Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mInsertHandler:Landroid/os/Handler;

    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;)Lcom/sec/android/gallery3d/data/DataManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mInsertHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mIsAbleToSave:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->insertPhotoFrame()V

    return-void
.end method

.method private declared-synchronized getDbHelper()Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;
    .locals 1

    .prologue
    .line 55
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mDbHelper:Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    if-nez v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mDbHelper:Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mDbHelper:Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private insertPhotoFrame()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 134
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 135
    .local v3, "values":Landroid/content/ContentValues;
    const/4 v0, 0x0

    .line 136
    .local v0, "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    const/4 v1, -0x1

    .line 137
    .local v1, "mediaId":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 138
    .local v2, "uriPass":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v5, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mUri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaItem(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    check-cast v0, Lcom/sec/android/gallery3d/data/LocalImage;

    .line 139
    .restart local v0    # "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v1

    .line 140
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 142
    const-string/jumbo v4, "word"

    iget-object v5, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mValues:[Ljava/lang/String;

    aget-object v5, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string v4, "region"

    iget-object v5, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mValues:[Ljava/lang/String;

    const/4 v6, 0x1

    aget-object v5, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const-string v4, "media_id"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 145
    const-string v4, "photo"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string/jumbo v4, "type"

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 147
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->getDbHelper()Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->insert(Landroid/content/ContentValues;)J

    .line 148
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 20
    check-cast p1, [Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->doInBackground([Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)Ljava/util/List;
    .locals 14
    .param p1, "spenPageDoc"    # [Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/samsung/android/sdk/pen/document/SpenPageDoc;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/strokemanager/data/CandidateData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 65
    const/4 v0, 0x0

    .line 67
    .local v0, "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/strokemanager/data/CandidateData;>;"
    :try_start_0
    iget-object v9, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mStrokeMgr:Lcom/samsung/android/strokemanager/StrokeManager;

    const/4 v10, 0x0

    aget-object v10, p1, v10

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lcom/samsung/android/strokemanager/StrokeManager;->startRecognize(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 71
    :goto_0
    if-nez v0, :cond_1

    .line 72
    const/4 v0, 0x0

    .line 130
    .end local v0    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/strokemanager/data/CandidateData;>;"
    :cond_0
    :goto_1
    return-object v0

    .line 68
    .restart local v0    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/strokemanager/data/CandidateData;>;"
    :catch_0
    move-exception v1

    .line 69
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 74
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :cond_1
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    .line 75
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 76
    .local v7, "values":Landroid/content/ContentValues;
    const-string v8, ""

    .line 77
    .local v8, "word":Ljava/lang/String;
    const-string v5, ""

    .line 79
    .local v5, "region":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v9

    if-ge v2, v9, :cond_4

    .line 80
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/strokemanager/data/CandidateData;

    invoke-virtual {v9}, Lcom/samsung/android/strokemanager/data/CandidateData;->getCandidateList()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 79
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 83
    :cond_2
    if-eqz v2, :cond_3

    .line 84
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "|"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 85
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "|"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 87
    :cond_3
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/strokemanager/data/CandidateData;

    invoke-virtual {v9}, Lcom/samsung/android/strokemanager/data/CandidateData;->getCandidateList()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 88
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/strokemanager/data/CandidateData;

    invoke-virtual {v9}, Lcom/samsung/android/strokemanager/data/CandidateData;->getRectFString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    .line 91
    :cond_4
    const-string/jumbo v9, "word"

    invoke-virtual {v7, v9, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const-string v9, "region"

    invoke-virtual {v7, v9, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v9, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mUri:Landroid/net/Uri;

    if-eqz v9, :cond_5

    iget-object v9, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v10, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mUri:Landroid/net/Uri;

    invoke-virtual {v10}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaItem(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 95
    iget-object v9, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mValues:[Ljava/lang/String;

    aput-object v8, v9, v12

    .line 96
    iget-object v9, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mValues:[Ljava/lang/String;

    aput-object v5, v9, v13

    .line 97
    iget-object v9, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mInsertHandler:Landroid/os/Handler;

    invoke-virtual {v9, v13}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 100
    :cond_5
    const/4 v3, 0x0

    .line 101
    .local v3, "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    const/4 v4, -0x1

    .line 102
    .local v4, "mediaId":I
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 104
    .local v6, "uriPass":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mUri:Landroid/net/Uri;

    if-nez v9, :cond_7

    iget-object v9, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v10, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mFilePath:Ljava/lang/String;

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaItem(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_7

    iget-object v9, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v10, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mFilePath:Ljava/lang/String;

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaItem(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    instance-of v9, v9, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v9, :cond_7

    .line 105
    iget-object v9, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v10, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mFilePath:Ljava/lang/String;

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaItem(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    check-cast v3, Lcom/sec/android/gallery3d/data/LocalImage;

    .line 106
    .restart local v3    # "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v4

    .line 107
    const-string v9, "media_id"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 108
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 109
    const-string/jumbo v9, "type"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 110
    iput-boolean v13, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mIsAbleToSave:Z

    .line 122
    :cond_6
    :goto_4
    const-string v9, "photo"

    invoke-virtual {v7, v9, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->getDbHelper()Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v9

    invoke-virtual {v9, v4}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->selectMediaId(I)I

    move-result v9

    const/4 v10, -0x1

    if-le v9, v10, :cond_9

    .line 124
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->getDbHelper()Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v9

    invoke-virtual {v9, v7, v4}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->update(Landroid/content/ContentValues;I)Z

    goto/16 :goto_1

    .line 111
    :cond_7
    iget-object v9, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mUri:Landroid/net/Uri;

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v10, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mUri:Landroid/net/Uri;

    invoke-virtual {v10}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaItem(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_6

    .line 112
    iget-object v9, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v10, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mUri:Landroid/net/Uri;

    invoke-virtual {v10}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaItem(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    instance-of v9, v9, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v9, :cond_8

    .line 113
    iget-object v9, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v10, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mUri:Landroid/net/Uri;

    invoke-virtual {v10}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaItem(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    check-cast v3, Lcom/sec/android/gallery3d/data/LocalImage;

    .line 114
    .restart local v3    # "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v4

    .line 115
    const-string v9, "media_id"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 117
    :cond_8
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 118
    const-string/jumbo v9, "type"

    const/4 v10, 0x2

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 119
    iput-boolean v13, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mIsAbleToSave:Z

    goto :goto_4

    .line 125
    :cond_9
    iget-boolean v9, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mIsAbleToSave:Z

    if-eqz v9, :cond_0

    .line 126
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->getDbHelper()Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v9

    invoke-virtual {v9, v7}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->insert(Landroid/content/ContentValues;)J

    goto/16 :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 20
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/strokemanager/data/CandidateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 152
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/strokemanager/data/CandidateData;>;"
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mStrokeMgr:Lcom/samsung/android/strokemanager/StrokeManager;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mStrokeMgr:Lcom/samsung/android/strokemanager/StrokeManager;

    invoke-virtual {v0}, Lcom/samsung/android/strokemanager/StrokeManager;->close()V

    .line 155
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 156
    return-void
.end method
