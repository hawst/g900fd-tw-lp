.class public Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;
.super Ljava/lang/Object;
.source "DimensionConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/DimensionConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ScreenDimension"
.end annotation


# static fields
.field private static sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;


# instance fields
.field public actionbar_height_l:I

.field public actionbar_height_p:I

.field public density:F

.field public densityDpi:I

.field public galleryGlWidthMWOffset:I

.field public selectionModeBar_height:I

.field public spinnerVerticalOffset:I


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 31
    .local v0, "r":Landroid/content/res/Resources;
    const v1, 0x7f0d0374

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;->actionbar_height_p:I

    .line 32
    const v1, 0x7f0d0375

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;->actionbar_height_l:I

    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;->density:F

    .line 34
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;->densityDpi:I

    .line 35
    const v1, 0x7f0d02e0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;->selectionModeBar_height:I

    .line 36
    const v1, 0x7f0d027d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;->galleryGlWidthMWOffset:I

    .line 37
    const v1, 0x7f0c0056

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;->spinnerVerticalOffset:I

    .line 38
    return-void
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    const-class v1, Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;

    if-nez v0, :cond_0

    .line 24
    new-instance v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;

    .line 26
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 23
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
