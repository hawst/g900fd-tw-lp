.class public abstract Lcom/sec/samsung/gallery/glview/GlBaseAdapter;
.super Lcom/sec/samsung/gallery/glview/GContentObservable;
.source "GlBaseAdapter.java"


# static fields
.field public static final LARGE_COVER_THUMB_HEIGHT:I = 0x21c

.field public static final LARGE_COVER_THUMB_WIDTH:I = 0x280

.field public static final STATE_INACTIVE:I = 0x1

.field public static final STATE_NORMAL:I = 0x0

.field public static final THUMB_HEIGHT:I = 0x140

.field public static final THUMB_WIDTH:I = 0x140

.field public static final TYPE_MICROTHUMBNAIL:I = 0x2

.field public static final TYPE_THUMBNAIL:I = 0x1


# instance fields
.field public mAttribute:I

.field protected mLoadContentData:Z

.field protected mSignatureBackground:Landroid/graphics/drawable/Drawable;

.field public mViewResult:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GContentObservable;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->mLoadContentData:Z

    .line 28
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const v1, -0x777778

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->mSignatureBackground:Landroid/graphics/drawable/Drawable;

    return-void
.end method


# virtual methods
.method public getAttribute()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->mAttribute:I

    return v0
.end method

.method public abstract getCount()I
.end method

.method public getFlexibleHeightDP(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 69
    const/4 v0, 0x0

    return v0
.end method

.method public getFlexibleHeightPixel(I)F
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 66
    const/4 v0, 0x0

    return v0
.end method

.method public abstract getItem(I)Ljava/lang/Object;
.end method

.method public abstract getItemImage(II)Landroid/graphics/Bitmap;
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 57
    const/4 v0, 0x0

    return-object v0
.end method

.method public getThumbSize()I
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x2

    return v0
.end method

.method public abstract getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;
.end method

.method public getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p4, "ext"    # I
    .param p5, "obj"    # Ljava/lang/Object;

    .prologue
    .line 63
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    return-object v0
.end method

.method public loadContentData(Z)V
    .locals 1
    .param p1, "reload"    # Z

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->mLoadContentData:Z

    if-nez v0, :cond_0

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->mLoadContentData:Z

    .line 44
    if-eqz p1, :cond_0

    .line 45
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->reloadData()V

    .line 48
    :cond_0
    return-void
.end method

.method public reloadData()V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method public setActiveWindow(IIII)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "visibleStart"    # I
    .param p4, "visibleEnd"    # I

    .prologue
    .line 73
    return-void
.end method

.method public setAlbumViewMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 79
    return-void
.end method

.method public setAttribute(IZ)V
    .locals 2
    .param p1, "attribute"    # I
    .param p2, "set"    # Z

    .prologue
    .line 31
    if-eqz p2, :cond_0

    .line 32
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->mAttribute:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->mAttribute:I

    .line 35
    :goto_0
    return-void

    .line 34
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->mAttribute:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->mAttribute:I

    goto :goto_0
.end method

.method public setReorderAlbum(II)V
    .locals 0
    .param p1, "parm1"    # I
    .param p2, "parm2"    # I

    .prologue
    .line 90
    return-void
.end method

.method public updateVisiableRange(II)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 76
    return-void
.end method
