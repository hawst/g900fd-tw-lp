.class Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$3;
.super Ljava/lang/Object;
.source "GallerySearchFilterGroupViewPager.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;)V
    .locals 0

    .prologue
    .line 570
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$3;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 588
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$3;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->access$200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 589
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$3;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->access$200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    .line 591
    :cond_0
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 584
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 574
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$3;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->access$200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 575
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$3;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->access$200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageSelected(I)V

    .line 578
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$3;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    # invokes: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->setArrowPosition(I)V
    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->access$300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;I)V

    .line 579
    return-void
.end method
