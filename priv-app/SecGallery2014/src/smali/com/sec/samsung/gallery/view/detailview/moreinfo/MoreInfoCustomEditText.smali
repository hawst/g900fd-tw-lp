.class public Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;
.super Landroid/widget/EditText;
.source "MoreInfoCustomEditText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText$onKeyPreImeListener;
    }
.end annotation


# instance fields
.field private mOnKeyPreImeListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText$onKeyPreImeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    return-void
.end method


# virtual methods
.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->mOnKeyPreImeListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText$onKeyPreImeListener;

    if-eqz v0, :cond_0

    .line 23
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->mOnKeyPreImeListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText$onKeyPreImeListener;

    invoke-interface {v0, p1, p2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText$onKeyPreImeListener;->onKeyPreIme(ILandroid/view/KeyEvent;)V

    .line 25
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public setOnKeyPreImeListener(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText$onKeyPreImeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText$onKeyPreImeListener;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->mOnKeyPreImeListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText$onKeyPreImeListener;

    .line 30
    return-void
.end method
