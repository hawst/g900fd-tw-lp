.class public Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;
.super Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;
.source "DMRPhotoDataAdapter.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$ImageBundle;
    }
.end annotation


# static fields
.field private static final MSG_UPDATE_IMAGE:I = 0x1

.field private static final SIZE_BACKUP:I = 0x400

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAGIF:Lcom/quramsoft/agif/QuramAGIF;

.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mHandler:Landroid/os/Handler;

.field private mHasFullImage:Z

.field private mIsCanceled:Z

.field private mItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mLargeListener:Lcom/sec/android/gallery3d/util/FutureListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/FutureListener",
            "<",
            "Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

.field private mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

.field private mQURAMWINKUTIL:Lcom/quramsoft/agif/QURAMWINKUTIL;

.field private mTask:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<*>;"
        }
    .end annotation
.end field

.field private mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

.field private mThumbListener:Lcom/sec/android/gallery3d/util/FutureListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/FutureListener",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 6
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "view"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p3, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/TileImageViewAdapter;-><init>()V

    .line 67
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mIsCanceled:Z

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 126
    new-instance v0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$2;-><init>(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mLargeListener:Lcom/sec/android/gallery3d/util/FutureListener;

    .line 148
    new-instance v0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$3;-><init>(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mThumbListener:Lcom/sec/android/gallery3d/util/FutureListener;

    .line 73
    invoke-static {p3}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 74
    invoke-virtual {p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v2

    const-wide/16 v4, 0x40

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mHasFullImage:Z

    .line 76
    invoke-static {p2}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/ui/PhotoView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    .line 77
    new-instance v0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$1;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$1;-><init>(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;Lcom/sec/android/gallery3d/ui/GLRoot;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mHandler:Landroid/os/Handler;

    .line 103
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    .line 105
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 106
    return-void

    :cond_0
    move v0, v1

    .line 74
    goto :goto_0
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mIsCanceled:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mIsCanceled:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mHasFullImage:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$ImageBundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$ImageBundle;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->onDecodeLargeComplete(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$ImageBundle;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;Lcom/sec/android/gallery3d/util/Future;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/util/Future;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->onDecodeThumbComplete(Lcom/sec/android/gallery3d/util/Future;)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;)Lcom/sec/android/gallery3d/app/LoadingListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private onDecodeLargeComplete(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$ImageBundle;)V
    .locals 4
    .param p1, "bundle"    # Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$ImageBundle;

    .prologue
    .line 166
    :try_start_0
    iget-object v1, p1, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$ImageBundle;->decoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    if-nez v1, :cond_0

    .line 167
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->updateBrokenImage()V

    .line 179
    :goto_0
    return-void

    .line 171
    :cond_0
    new-instance v1, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    iget-object v2, p1, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$ImageBundle;->backupImage:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2}, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;-><init>(Landroid/graphics/Bitmap;)V

    iget-object v2, p1, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$ImageBundle;->decoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->getWidth()I

    move-result v2

    iget-object v3, p1, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$ImageBundle;->decoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->getHeight()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->setScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;II)V

    .line 173
    iget-object v1, p1, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$ImageBundle;->decoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->setRegionDecoder(Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)V

    .line 174
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->setScalePrepared(Z)V

    .line 175
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyCurrentImageInvalidated()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 176
    :catch_0
    move-exception v0

    .line 177
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->TAG:Ljava/lang/String;

    const-string v2, "fail to decode large"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private onDecodeThumbComplete(Lcom/sec/android/gallery3d/util/Future;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 183
    .local p1, "future":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<Landroid/graphics/Bitmap;>;"
    :try_start_0
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 184
    .local v0, "backup":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 185
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->updateBrokenImage()V

    .line 195
    .end local v0    # "backup":Landroid/graphics/Bitmap;
    :goto_0
    return-void

    .line 188
    .restart local v0    # "backup":Landroid/graphics/Bitmap;
    :cond_0
    new-instance v2, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    invoke-direct {v2, v0}, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {p0, v2, v3, v4}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->setScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;II)V

    .line 189
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyOnNewImage()V

    .line 190
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->setScalePrepared(Z)V

    .line 191
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyCurrentImageInvalidated()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 192
    .end local v0    # "backup":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v1

    .line 193
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->TAG:Ljava/lang/String;

    const-string v3, "fail to decode thumb"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public changeSource(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 0
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 293
    return-void
.end method

.method public destroy()V
    .locals 0

    .prologue
    .line 285
    return-void
.end method

.method public getCurrentIndex()I
    .locals 1

    .prologue
    .line 251
    const/4 v0, 0x0

    return v0
.end method

.method public getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method public getImageRotation()I
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v0

    return v0
.end method

.method public getImageRotation(I)I
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 322
    const/4 v0, 0x0

    return v0
.end method

.method public getImageSize(ILcom/sec/android/gallery3d/ui/PhotoView$Size;)V
    .locals 0
    .param p1, "offset"    # I
    .param p2, "size"    # Lcom/sec/android/gallery3d/ui/PhotoView$Size;

    .prologue
    .line 311
    return-void
.end method

.method public getLoadingState(I)I
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 370
    const/4 v0, 0x0

    return v0
.end method

.method public getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 316
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNextImage()Lcom/sec/android/gallery3d/ui/PhotoView$ImageData;
    .locals 1

    .prologue
    .line 227
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPreviousImage()Lcom/sec/android/gallery3d/ui/PhotoView$ImageData;
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x0

    return-object v0
.end method

.method public getScreenNail(I)Lcom/sec/android/gallery3d/ui/ScreenNail;
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 328
    const/4 v0, 0x0

    return-object v0
.end method

.method public is3DTour(I)Z
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 388
    const/4 v0, 0x0

    return v0
.end method

.method public isCamera(I)Z
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 340
    const/4 v0, 0x0

    return v0
.end method

.method public isDeletable(I)Z
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 364
    const/4 v0, 0x0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    return v0
.end method

.method public isGolfShot(I)Z
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 394
    const/4 v0, 0x0

    return v0
.end method

.method public isPanorama(I)Z
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 346
    const/4 v0, 0x0

    return v0
.end method

.method public isStaticCamera(I)Z
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 352
    const/4 v0, 0x0

    return v0
.end method

.method public isVideo(I)Z
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 358
    const/4 v0, 0x0

    return v0
.end method

.method public jumpTo(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 243
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public moveTo(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 299
    return-void
.end method

.method public moveToFastest(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 305
    return-void
.end method

.method public next()V
    .locals 1

    .prologue
    .line 235
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onAgifPlayRequests()V
    .locals 2

    .prologue
    .line 275
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getQuramAGIF()Lcom/quramsoft/agif/QuramAGIF;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mAGIF:Lcom/quramsoft/agif/QuramAGIF;

    .line 276
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mAGIF:Lcom/quramsoft/agif/QuramAGIF;

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mAGIF:Lcom/quramsoft/agif/QuramAGIF;

    invoke-virtual {v0}, Lcom/quramsoft/agif/QuramAGIF;->getQURAMWINKUTIL()Lcom/quramsoft/agif/QURAMWINKUTIL;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mQURAMWINKUTIL:Lcom/quramsoft/agif/QURAMWINKUTIL;

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mQURAMWINKUTIL:Lcom/quramsoft/agif/QURAMWINKUTIL;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0, v1}, Lcom/quramsoft/agif/QURAMWINKUTIL;->updateAGIF(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    .line 281
    return-void
.end method

.method public pause()V
    .locals 3

    .prologue
    .line 213
    sget-object v1, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->TAG:Ljava/lang/String;

    const-string v2, "DMRPhotoDataApdater pause()"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->setScalePrepared(Z)V

    .line 215
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mTask:Lcom/sec/android/gallery3d/util/Future;

    .line 216
    .local v0, "task":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<*>;"
    invoke-interface {v0}, Lcom/sec/android/gallery3d/util/Future;->isDone()Z

    move-result v1

    if-nez v1, :cond_0

    .line 217
    sget-object v1, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->TAG:Ljava/lang/String;

    const-string v2, "cancel start !!!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mIsCanceled:Z

    .line 219
    invoke-interface {v0}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 220
    invoke-interface {v0}, Lcom/sec/android/gallery3d/util/Future;->waitDone()V

    .line 221
    sget-object v1, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->TAG:Ljava/lang/String;

    const-string v2, "cancel end !!!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mTask:Lcom/sec/android/gallery3d/util/Future;

    .line 224
    return-void
.end method

.method public previous()V
    .locals 1

    .prologue
    .line 239
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public resume()V
    .locals 3

    .prologue
    .line 198
    sget-object v0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->TAG:Ljava/lang/String;

    const-string v1, "DMRPhotoDataApdater resume()"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->setScalePrepared(Z)V

    .line 200
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mTask:Lcom/sec/android/gallery3d/util/Future;

    if-nez v0, :cond_0

    .line 201
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mHasFullImage:Z

    if-eqz v0, :cond_1

    .line 202
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->requestLargeImage()Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mLargeListener:Lcom/sec/android/gallery3d/util/FutureListener;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mTask:Lcom/sec/android/gallery3d/util/Future;

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mThumbListener:Lcom/sec/android/gallery3d/util/FutureListener;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mTask:Lcom/sec/android/gallery3d/util/Future;

    goto :goto_0
.end method

.method public setCurrentPhoto(Lcom/sec/android/gallery3d/data/Path;I)V
    .locals 0
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "indexHint"    # I

    .prologue
    .line 256
    return-void
.end method

.method public setFocusHintDirection(I)V
    .locals 0
    .param p1, "direction"    # I

    .prologue
    .line 377
    return-void
.end method

.method public setFocusHintPath(Lcom/sec/android/gallery3d/data/Path;)V
    .locals 0
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 383
    return-void
.end method

.method public setLoadingListener(Lcom/sec/android/gallery3d/app/LoadingListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/app/LoadingListener;

    .prologue
    .line 288
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

    .line 289
    return-void
.end method

.method public setNeedFullImage(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 335
    return-void
.end method

.method public setNewData(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 4
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 109
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v0, :cond_0

    .line 110
    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 111
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v0

    const-wide/16 v2, 0x40

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mHasFullImage:Z

    .line 114
    :cond_0
    return-void

    .line 111
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateBrokenImage()V
    .locals 4

    .prologue
    .line 260
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200a7

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 262
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->updateBrokenImage(Landroid/graphics/Bitmap;)V

    .line 263
    return-void
.end method

.method public updateBrokenImage(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "backup"    # Landroid/graphics/Bitmap;

    .prologue
    .line 266
    new-instance v0, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    invoke-direct {v0, p1}, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->setScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;II)V

    .line 267
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyOnNewImage()V

    .line 268
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->notifyCurrentImageInvalidated()V

    .line 269
    return-void
.end method

.method public useLoadingProgress(I)Z
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 399
    const/4 v0, 0x0

    return v0
.end method
