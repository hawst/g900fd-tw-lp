.class Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;
.super Lorg/puremvc/java/patterns/mediator/Mediator;
.source "DetailViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/Object;

    .prologue
    .line 3095
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-direct {p0, p2, p3}, Lorg/puremvc/java/patterns/mediator/Mediator;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public handleNotification(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 14
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 3117
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getName()Ljava/lang/String;

    move-result-object v3

    .line 3119
    .local v3, "notiName":Ljava/lang/String;
    const-string v9, "PREPARE_SLIDE_SHOW_DATA"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 3120
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->addItemsToSlideshowProxy(Z)V
    invoke-static {v9, v12}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$12000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Z)V

    .line 3202
    :cond_0
    :goto_0
    return-void

    .line 3121
    :cond_1
    const-string v9, "BURSTSHOTLIST_UPDATED"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 3122
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateSaveButtonState()V
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$12100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    goto :goto_0

    .line 3123
    :cond_2
    const-string v9, "EXIT_SELECTION_MODE"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 3124
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v9

    if-eqz v9, :cond_3

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 3125
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$12300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v9

    new-instance v10, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12$1;

    invoke-direct {v10, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12$1;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;)V

    invoke-virtual {v9, v10}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 3132
    :cond_3
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v9

    instance-of v9, v9, Ljava/lang/Integer;

    if-eqz v9, :cond_4

    .line 3133
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 3134
    .local v7, "reset":I
    if-ne v7, v13, :cond_4

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v9

    if-eqz v9, :cond_4

    .line 3135
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v10}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v10

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/data/MediaItem;->setSceretBoxItem(Z)V

    .line 3136
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateUIForCurrentPhoto()V
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$5900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    .line 3139
    .end local v7    # "reset":I
    :cond_4
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseContextualAwareness:Z

    if-eqz v9, :cond_0

    .line 3140
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v10}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isActionbarShowing()Z
    invoke-static {v11}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$12400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Z

    move-result v11

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/gallery3d/ui/PhotoView;->setCurrentPhotoForCA(Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    goto/16 :goto_0

    .line 3142
    :cond_5
    const-string v9, "DOWNLOAD_NEARBY"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 3143
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/Object;

    move-object v6, v9

    check-cast v6, [Ljava/lang/Object;

    .line 3144
    .local v6, "params":[Ljava/lang/Object;
    const-string v8, ""

    .line 3145
    .local v8, "serverName":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    move-result-object v9

    if-eqz v9, :cond_6

    .line 3146
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getName()Ljava/lang/String;

    move-result-object v8

    .line 3148
    :cond_6
    aget-object v5, v6, v12

    check-cast v5, Ljava/util/ArrayList;

    .line 3149
    .local v5, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 3150
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v9

    invoke-virtual {v9, v8, v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->download(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 3151
    .end local v5    # "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    .end local v6    # "params":[Ljava/lang/Object;
    .end local v8    # "serverName":Ljava/lang/String;
    :cond_7
    const-string v9, "DOWNLOAD_CLOUDBY"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 3152
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/Object;

    move-object v6, v9

    check-cast v6, [Ljava/lang/Object;

    .line 3154
    .restart local v6    # "params":[Ljava/lang/Object;
    aget-object v4, v6, v12

    check-cast v4, Ljava/util/ArrayList;

    .line 3155
    .local v4, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$12500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v9

    invoke-static {v9, v4}, Lcom/sec/samsung/gallery/util/DownloadUtil;->downloadEnqueue(Landroid/content/Context;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 3156
    .end local v4    # "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v6    # "params":[Ljava/lang/Object;
    :cond_8
    const-string v9, "UNLOCK_TOUCH"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 3157
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mLockTouchEvent:Z
    invoke-static {v9, v12}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4802(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Z)Z

    .line 3158
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 3159
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v9

    invoke-virtual {v9, v12}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setLockTouchEvent(Z)V

    goto/16 :goto_0

    .line 3160
    :cond_9
    const-string v9, "BUSRT_SHOT_SETTING_CHANGED"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 3161
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$12600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 3162
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
    invoke-static {v10}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$12600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v11}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->updateBurstShotIcon(Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v10

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowPlaySpeed:I
    invoke-static {v9, v10}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$12702(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;I)I

    goto/16 :goto_0

    .line 3163
    :cond_a
    const-string v9, "MEDIA_EJECT"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 3164
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$12800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$12800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v9

    if-lez v9, :cond_0

    .line 3165
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->handleDismissDialogs()V
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$12900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    goto/16 :goto_0

    .line 3167
    :cond_b
    const-string v9, "MOREINFO_EVENT"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 3168
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/Object;

    move-object v6, v9

    check-cast v6, [Ljava/lang/Object;

    .line 3169
    .restart local v6    # "params":[Ljava/lang/Object;
    aget-object v9, v6, v12

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    if-nez v9, :cond_c

    .line 3170
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    iget-object v9, v9, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    if-nez v9, :cond_0

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 3171
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->startMoreInfo()V
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$13000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    goto/16 :goto_0

    .line 3174
    :cond_c
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    iget-object v9, v9, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    iget-object v9, v9, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->isEnabled()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 3175
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    iget-object v9, v9, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    invoke-virtual {v9, v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->changeMode([Ljava/lang/Object;)V

    .line 3176
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->stopSoundScene()V
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$13100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    goto/16 :goto_0

    .line 3179
    .end local v6    # "params":[Ljava/lang/Object;
    :cond_d
    const-string v9, "DEVICE_DISCONNECTED"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 3180
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 3181
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    invoke-static {v10}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOriginalSetPathString:Ljava/lang/String;
    invoke-static {v11}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$13200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->isDeviceRemoved(Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 3182
    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$5000()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "top mediaset is removed"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3183
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/String;

    move-object v2, v9

    check-cast v2, [Ljava/lang/String;

    .line 3184
    .local v2, "deviceInfo":[Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v9

    aget-object v10, v2, v12

    aget-object v11, v2, v13

    invoke-static {v9, v10, v11}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->showDeviceRemovedDialog(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Ljava/lang/String;Ljava/lang/String;)V

    .line 3185
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->finishDetailView()V
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    .line 3186
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$13300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->getLastestActivityState(Landroid/content/Context;)Ljava/lang/Class;

    move-result-object v0

    .line 3187
    .local v0, "activityState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v0, v10}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 3190
    .end local v0    # "activityState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    .end local v2    # "deviceInfo":[Ljava/lang/String;
    :cond_e
    const-string v9, "SECRET_MODE_CHANGED"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 3191
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/Object;

    move-object v6, v9

    check-cast v6, [Ljava/lang/Object;

    .line 3192
    .restart local v6    # "params":[Ljava/lang/Object;
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    aget-object v9, v6, v12

    check-cast v9, Ljava/lang/Boolean;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateSelectionMode(Ljava/lang/Boolean;)V
    invoke-static {v10, v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$13400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 3193
    .end local v6    # "params":[Ljava/lang/Object;
    :cond_f
    const-string v9, "ACTION_DELETE_CONFIRM"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_10

    .line 3194
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v1

    .line 3195
    .local v1, "controller":Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->isPanoramaScrolling()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 3196
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PositionController;->stopAnimation()V

    goto/16 :goto_0

    .line 3198
    .end local v1    # "controller":Lcom/sec/android/gallery3d/ui/PositionController;
    :cond_10
    const-string v9, "SHARE_DIALOG_HIDDEN"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 3199
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsPopupDialogDisplayed:Z
    invoke-static {v9, v12}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$13502(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Z)Z

    .line 3200
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    const/16 v10, 0xdac

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->refreshHidingMessage(I)V
    invoke-static {v9, v10}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$13600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;I)V

    goto/16 :goto_0
.end method

.method public listNotificationInterests()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 3098
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "PREPARE_SLIDE_SHOW_DATA"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "BURSTSHOTLIST_UPDATED"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "EXIT_SELECTION_MODE"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "DOWNLOAD_NEARBY"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "DOWNLOAD_CLOUDBY"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "UNLOCK_TOUCH"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "BUSRT_SHOT_SETTING_CHANGED"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "MEDIA_EJECT"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "MOREINFO_EVENT"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "DEVICE_DISCONNECTED"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "SECRET_MODE_CHANGED"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "ACTION_DELETE_CONFIRM"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "SHARE_DIALOG_HIDDEN"

    aput-object v2, v0, v1

    return-object v0
.end method
