.class public Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;
.super Landroid/widget/AutoCompleteTextView;
.source "GallerySearchView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SearchAutoComplete"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 499
    invoke-direct {p0, p1}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 500
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->init()V

    .line 501
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 504
    invoke-direct {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 505
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->init()V

    .line 506
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 509
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 510
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->init()V

    .line 511
    return-void
.end method


# virtual methods
.method public enoughToFilter()Z
    .locals 1

    .prologue
    .line 523
    const/4 v0, 0x1

    return v0
.end method

.method public forceQuery()V
    .locals 0

    .prologue
    .line 527
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->showDropDown()V

    .line 528
    return-void
.end method

.method public init()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 531
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGallerySearchUISeperatedMode:Z

    if-eqz v3, :cond_0

    .line 532
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 533
    .local v2, "res":Landroid/content/res/Resources;
    const v3, 0x7f0d02a0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 535
    .local v0, "paddingLeft":I
    const v3, 0x7f0d02a1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 538
    .local v1, "paddingRight":I
    invoke-virtual {p0, v0, v4, v1, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setPadding(IIII)V

    .line 540
    .end local v0    # "paddingLeft":I
    .end local v1    # "paddingRight":I
    .end local v2    # "res":Landroid/content/res/Resources;
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 2
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 544
    invoke-super/range {p0 .. p5}, Landroid/widget/AutoCompleteTextView;->onLayout(ZIIII)V

    .line 545
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->requestFocus()Z

    .line 546
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->getWidth()I

    move-result v0

    .line 547
    .local v0, "width":I
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v1, :cond_0

    .line 548
    add-int/lit8 v0, v0, -0x6

    .line 549
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setDropDownWidth(I)V

    .line 556
    :goto_0
    return-void

    .line 551
    :cond_0
    add-int/lit8 v0, v0, 0x6

    .line 552
    const/4 v1, -0x3

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setDropDownHorizontalOffset(I)V

    .line 553
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setDropDownWidth(I)V

    goto :goto_0
.end method

.method protected replaceText(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 519
    return-void
.end method
