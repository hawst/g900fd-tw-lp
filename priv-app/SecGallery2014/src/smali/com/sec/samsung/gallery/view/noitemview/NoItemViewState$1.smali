.class Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$1;
.super Lorg/puremvc/java/patterns/mediator/Mediator;
.source "NoItemViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/Object;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    invoke-direct {p0, p2, p3}, Lorg/puremvc/java/patterns/mediator/Mediator;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public handleNotification(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 8
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 98
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getName()Ljava/lang/String;

    move-result-object v5

    .line 99
    .local v5, "notiName":Ljava/lang/String;
    const-string v6, "SECRET_MODE_CHANGED"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 100
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    # invokes: Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->onTabChanged()V
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->access$000(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)V

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    const-string v6, "VIEW_BY_TYPE_UPDATED"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 102
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    # invokes: Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->handleFilter()V
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->access$100(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)V

    .line 103
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->getActionBarManager()Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v0

    .line 104
    .local v0, "actionBar":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    instance-of v6, v0, Lcom/sec/samsung/gallery/view/AlbumTabBar;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mNoItemActionBarForNormal:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->access$200(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mNoItemActionBarForNormal:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->access$200(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->isPick()Z

    move-result v6

    if-nez v6, :cond_0

    .line 106
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->access$300(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v2

    .line 107
    .local v2, "currentViewByType":I
    check-cast v0, Lcom/sec/samsung/gallery/view/AlbumTabBar;

    .end local v0    # "actionBar":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->changeViewByType(I)V

    goto :goto_0

    .line 109
    .end local v2    # "currentViewByType":I
    :cond_2
    const-string v6, "DEVICE_DISCONNECTED"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 110
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->getNearbyClient()Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v4

    .line 111
    .local v4, "nearbyClient":Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    if-eqz v4, :cond_0

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->access$400(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mTopSetPath:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->access$500(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v4, v7}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->isDeviceRemoved(Lcom/sec/android/gallery3d/data/DataManager;Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 112
    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->access$600()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "top mediaset is removed"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    move-object v3, v6

    check-cast v3, [Ljava/lang/String;

    .line 114
    .local v3, "deviceInfo":[Ljava/lang/String;
    const/4 v6, 0x0

    aget-object v6, v3, v6

    const/4 v7, 0x1

    aget-object v7, v3, v7

    invoke-static {v4, v6, v7}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->showDeviceRemovedDialog(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->access$700(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->getLastestActivityState(Landroid/content/Context;)Ljava/lang/Class;

    move-result-object v1

    .line 116
    .local v1, "activityState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->access$300(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v1, v7}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_0
.end method

.method public listNotificationInterests()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 89
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "VIEW_BY_TYPE_UPDATED"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "SECRET_MODE_CHANGED"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "DEVICE_DISCONNECTED"

    aput-object v2, v0, v1

    return-object v0
.end method
