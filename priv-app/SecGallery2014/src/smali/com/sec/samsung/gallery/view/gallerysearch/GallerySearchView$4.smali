.class Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;
.super Ljava/lang/Object;
.source "GallerySearchView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 286
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mCloseButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Landroid/widget/ImageView;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 287
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # invokes: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->onCloseClicked()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)V

    .line 312
    :cond_0
    :goto_0
    return-void

    .line 288
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mVoiceButton:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$400(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_2

    .line 289
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # invokes: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->startVoiceRecognitionActivity()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$500(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)V

    goto :goto_0

    .line 290
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    move-result-object v0

    if-ne p1, v0, :cond_3

    .line 291
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setCursorVisible(Z)V

    .line 292
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$600(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Landroid/widget/CursorAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$600(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Landroid/widget/CursorAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->forceQuery()V

    goto :goto_0

    .line 297
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mCloseTagListButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$700(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Landroid/widget/ImageView;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnUpdateTagListListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnUpdateTagListListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$800(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnUpdateTagListListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mShowTagList:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$900(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 300
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mCloseTagListButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$700(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f0200f2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 301
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mCloseTagListButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$700(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$1000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e017e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 302
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mShowTagList:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$902(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;Z)Z

    .line 303
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnUpdateTagListListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnUpdateTagListListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$800(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnUpdateTagListListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mShowTagList:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$900(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnUpdateTagListListener;->onUpdateTagList(Z)V

    goto/16 :goto_0

    .line 305
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mCloseTagListButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$700(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f0200f1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 306
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mCloseTagListButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$700(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$1000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e017f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 307
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # setter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mShowTagList:Z
    invoke-static {v0, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$902(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;Z)Z

    .line 308
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnUpdateTagListListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnUpdateTagListListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$800(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnUpdateTagListListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mShowTagList:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$900(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnUpdateTagListListener;->onUpdateTagList(Z)V

    goto/16 :goto_0
.end method
