.class public Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarView;
.source "AlbumActionBarForReorder.java"


# instance fields
.field private isOrderChanged:Z

.field private final mActionBarListener:Landroid/view/View$OnClickListener;

.field private mDoneActionView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Z)V
    .locals 11
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "isChanged"    # Z

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 22
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 19
    iput-boolean v9, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->isOrderChanged:Z

    .line 60
    new-instance v6, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder$1;

    invoke-direct {v6, p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder$1;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;)V

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mActionBarListener:Landroid/view/View$OnClickListener;

    .line 23
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 24
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const/4 v0, 0x0

    .line 25
    .local v0, "actionBarButtons":Landroid/view/View;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 26
    const v6, 0x7f030016

    new-instance v7, Landroid/widget/LinearLayout;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v7, v8}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v6, v7, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 32
    :goto_0
    const v6, 0x7f0f0022

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 33
    .local v2, "cancelActionView":Landroid/view/View;
    const v6, 0x7f0f0023

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 34
    .local v1, "cancel":Landroid/widget/TextView;
    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setAllCaps(Z)V

    .line 35
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mActionBarListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    const v6, 0x7f0f0024

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mDoneActionView:Landroid/view/View;

    .line 37
    const v6, 0x7f0f0025

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 38
    .local v4, "save":Landroid/widget/TextView;
    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setAllCaps(Z)V

    .line 39
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mDoneActionView:Landroid/view/View;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mActionBarListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    if-eqz p2, :cond_2

    .line 41
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mDoneActionView:Landroid/view/View;

    invoke-virtual {v6, v10}, Landroid/view/View;->setEnabled(Z)V

    .line 42
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mDoneActionView:Landroid/view/View;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v6, v7}, Landroid/view/View;->setAlpha(F)V

    .line 48
    :goto_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v6, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 49
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v6, v9}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 50
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v6, v9}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 51
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v6, v9}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 53
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 54
    const v6, 0x7f0f002f

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 55
    .local v5, "tv":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e0362

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 58
    .end local v5    # "tv":Landroid/widget/TextView;
    :cond_0
    return-void

    .line 29
    .end local v1    # "cancel":Landroid/widget/TextView;
    .end local v2    # "cancelActionView":Landroid/view/View;
    .end local v4    # "save":Landroid/widget/TextView;
    :cond_1
    const v6, 0x7f030012

    new-instance v7, Landroid/widget/LinearLayout;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v7, v8}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v6, v7, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 44
    .restart local v1    # "cancel":Landroid/widget/TextView;
    .restart local v2    # "cancelActionView":Landroid/view/View;
    .restart local v4    # "save":Landroid/widget/TextView;
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mDoneActionView:Landroid/view/View;

    invoke-virtual {v6, v9}, Landroid/view/View;->setEnabled(Z)V

    .line 45
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mDoneActionView:Landroid/view/View;

    const/high16 v7, 0x3f000000    # 0.5f

    invoke-virtual {v6, v7}, Landroid/view/View;->setAlpha(F)V

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;
    .param p1, "x1"    # I

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->onActionBarItemSelected(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->isOrderChanged:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mDoneActionView:Landroid/view/View;

    return-object v0
.end method

.method private onActionBarItemSelected(I)Z
    .locals 3
    .param p1, "itemId"    # I

    .prologue
    const/4 v2, 0x0

    .line 68
    const v0, 0x7f0f0024

    if-ne p1, v0, :cond_1

    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "REORDER_DONE"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 75
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 71
    :cond_1
    const v0, 0x7f0f0022

    if-ne p1, v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "REORDER_CANCEL"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public setIsOrderChanged(Z)V
    .locals 0
    .param p1, "isChanged"    # Z

    .prologue
    .line 95
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->isOrderChanged:Z

    .line 96
    return-void
.end method

.method public updateActionbarButton()V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder$2;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 91
    return-void
.end method
