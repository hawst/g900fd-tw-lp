.class Lcom/sec/samsung/gallery/view/eventview/EventViewState$13;
.super Ljava/lang/Object;
.source "EventViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/eventview/EventViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V
    .locals 0

    .prologue
    .line 495
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$13;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isCheckAvailable()V
    .locals 0

    .prologue
    .line 511
    return-void
.end method

.method public onPenSelection(IIIZ)Z
    .locals 2
    .param p1, "albumIdex"    # I
    .param p2, "itemIndex"    # I
    .param p3, "selectedCnt"    # I
    .param p4, "isLast"    # Z

    .prologue
    .line 499
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$13;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->access$000(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 500
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$13;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    const/4 v1, 0x0

    # invokes: Lcom/sec/samsung/gallery/view/eventview/EventViewState;->enterSelectionMode(Z)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->access$2600(Lcom/sec/samsung/gallery/view/eventview/EventViewState;Z)V

    .line 503
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$13;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->access$000(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 504
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$13;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    # invokes: Lcom/sec/samsung/gallery/view/eventview/EventViewState;->selectAlbum(I)V
    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->access$2000(Lcom/sec/samsung/gallery/view/eventview/EventViewState;I)V

    .line 506
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public prePenSelectionCheck(III)Z
    .locals 1
    .param p1, "albumIndex"    # I
    .param p2, "itemIndex"    # I
    .param p3, "selectedCnt"    # I

    .prologue
    .line 517
    const/4 v0, 0x0

    return v0
.end method
