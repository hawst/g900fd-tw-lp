.class Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$5;
.super Ljava/lang/Object;
.source "HiddenPhotoViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$5;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrollStateChanged(Lcom/sec/samsung/gallery/glview/GlAbsListView;I)V
    .locals 7
    .param p1, "view"    # Lcom/sec/samsung/gallery/glview/GlAbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 196
    packed-switch p2, :pswitch_data_0

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 198
    :pswitch_0
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->getFirstVisiblePosition()I

    move-result v1

    .line 199
    .local v1, "firstIndex":I
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->getLastVisiblePosition()I

    move-result v2

    .line 200
    .local v2, "lastIndex":I
    add-int v3, v1, v2

    div-int/lit8 v0, v3, 0x2

    .line 201
    .local v0, "bgIndex":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$5;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mBgImageIndex:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$1000(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)I

    move-result v3

    if-eq v3, v0, :cond_0

    .line 202
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$5;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # setter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mBgImageIndex:I
    invoke-static {v3, v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$1002(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;I)I

    .line 203
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$5;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$700(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/glview/GlPhotoView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$5;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$5;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mBgImageIndex:I
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$1000(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)I

    move-result v5

    const/4 v6, 0x2

    invoke-virtual {v4, v5, v6}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getItemImage(II)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setNextImage(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 209
    .end local v0    # "bgIndex":I
    .end local v1    # "firstIndex":I
    .end local v2    # "lastIndex":I
    :pswitch_1
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->getFirstVisiblePosition()I

    move-result v1

    .line 210
    .restart local v1    # "firstIndex":I
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->getLastVisiblePosition()I

    move-result v2

    .line 211
    .restart local v2    # "lastIndex":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$5;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$100(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setActiveWindow(II)V

    goto :goto_0

    .line 196
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
