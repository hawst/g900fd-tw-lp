.class public Lcom/sec/samsung/gallery/glview/GlDeleteAnim;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlDeleteAnim.java"


# static fields
.field private static final SHRINK_RATIO:F = 0.3f


# instance fields
.field private mObject:Lcom/sec/android/gallery3d/glcore/GlObject;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 3
    .param p1, "ratio"    # F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 21
    const v1, 0x3e99999a    # 0.3f

    mul-float/2addr v1, p1

    sub-float v0, v2, v1

    .line 23
    .local v0, "shrink":F
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlDeleteAnim;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v1, v0, v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->setScale(FF)V

    .line 24
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlDeleteAnim;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    sub-float/2addr v2, p1

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setAlpha(F)V

    .line 25
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 28
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlDeleteAnim;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0, v1, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->setScale(FF)V

    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlDeleteAnim;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlObject;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 30
    return-void
.end method

.method public startAnimation(Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 2
    .param p1, "objects"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 11
    if-nez p1, :cond_0

    .line 18
    :goto_0
    return-void

    .line 14
    :cond_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlDeleteAnim;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 15
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlDeleteAnim;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 16
    const-wide/16 v0, 0xc8

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/GlDeleteAnim;->setDuration(J)V

    .line 17
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlDeleteAnim;->start()V

    goto :goto_0
.end method
