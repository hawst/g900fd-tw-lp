.class Lcom/sec/samsung/gallery/glview/GlBlurObject$foregndDrawer;
.super Ljava/lang/Object;
.source "GlBlurObject.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlRootView$OnForeground;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlBlurObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "foregndDrawer"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlBlurObject;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/glview/GlBlurObject;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject$foregndDrawer;->this$0:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/glview/GlBlurObject;Lcom/sec/samsung/gallery/glview/GlBlurObject$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/glview/GlBlurObject;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/glview/GlBlurObject$1;

    .prologue
    .line 222
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlBlurObject$foregndDrawer;-><init>(Lcom/sec/samsung/gallery/glview/GlBlurObject;)V

    return-void
.end method


# virtual methods
.method public onDraw(Ljavax/microedition/khronos/opengles/GL11;)V
    .locals 2
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL11;

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject$foregndDrawer;->this$0:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    # setter for: Lcom/sec/samsung/gallery/glview/GlBlurObject;->mGL:Ljavax/microedition/khronos/opengles/GL11;
    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->access$102(Lcom/sec/samsung/gallery/glview/GlBlurObject;Ljavax/microedition/khronos/opengles/GL11;)Ljavax/microedition/khronos/opengles/GL11;

    .line 225
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject$foregndDrawer;->this$0:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    # getter for: Lcom/sec/samsung/gallery/glview/GlBlurObject;->mUploaded:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->access$200(Lcom/sec/samsung/gallery/glview/GlBlurObject;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject$foregndDrawer;->this$0:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    # invokes: Lcom/sec/samsung/gallery/glview/GlBlurObject;->setBitmapTexture()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->access$300(Lcom/sec/samsung/gallery/glview/GlBlurObject;)V

    .line 227
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject$foregndDrawer;->this$0:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    const/4 v1, 0x1

    # setter for: Lcom/sec/samsung/gallery/glview/GlBlurObject;->mUploaded:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->access$202(Lcom/sec/samsung/gallery/glview/GlBlurObject;Z)Z

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject$foregndDrawer;->this$0:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    # getter for: Lcom/sec/samsung/gallery/glview/GlBlurObject;->mLevel:F
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->access$400(Lcom/sec/samsung/gallery/glview/GlBlurObject;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    .line 230
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject$foregndDrawer;->this$0:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    # invokes: Lcom/sec/samsung/gallery/glview/GlBlurObject;->draw()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->access$500(Lcom/sec/samsung/gallery/glview/GlBlurObject;)V

    .line 232
    :cond_1
    return-void
.end method
