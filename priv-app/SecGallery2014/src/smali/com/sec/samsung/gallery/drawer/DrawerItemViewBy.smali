.class public Lcom/sec/samsung/gallery/drawer/DrawerItemViewBy;
.super Lcom/sec/samsung/gallery/drawer/DrawerItem;
.source "DrawerItemViewBy.java"


# static fields
.field private static final KEY_ONLY_3GP:Ljava/lang/String; = "only3gp"

.field private static final KEY_ONLY_JPG:Ljava/lang/String; = "onlyJpg"

.field private static final KEY_SENDER_VIDEOCALL:Ljava/lang/String; = "senderIsVideoCall"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContentType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/sec/samsung/gallery/drawer/DrawerItemViewBy;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/drawer/DrawerItemViewBy;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(IIILcom/sec/samsung/gallery/core/TabTagType;)V
    .locals 0
    .param p1, "groupid"    # I
    .param p2, "stringId"    # I
    .param p3, "ttsId"    # I
    .param p4, "tagType"    # Lcom/sec/samsung/gallery/core/TabTagType;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/drawer/DrawerItem;-><init>(IIILcom/sec/samsung/gallery/core/TabTagType;)V

    .line 39
    return-void
.end method

.method public static createViewByItems(IIIILcom/sec/samsung/gallery/core/TabTagType;)Lcom/sec/samsung/gallery/drawer/DrawerItem;
    .locals 1
    .param p0, "groupId"    # I
    .param p1, "stringId"    # I
    .param p2, "ttsId"    # I
    .param p3, "iconRes"    # I
    .param p4, "tagType"    # Lcom/sec/samsung/gallery/core/TabTagType;

    .prologue
    .line 42
    new-instance v0, Lcom/sec/samsung/gallery/drawer/DrawerItemViewBy;

    invoke-direct {v0, p0, p1, p2, p4}, Lcom/sec/samsung/gallery/drawer/DrawerItemViewBy;-><init>(IIILcom/sec/samsung/gallery/core/TabTagType;)V

    .line 43
    .local v0, "item":Lcom/sec/samsung/gallery/drawer/DrawerItem;
    invoke-virtual {v0, p3}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->setIconId(I)V

    .line 45
    return-object v0
.end method

.method private getContentType(Landroid/app/Activity;Landroid/content/Intent;)Ljava/lang/String;
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 194
    invoke-virtual {p2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v1

    .line 195
    .local v1, "type":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 203
    .end local v1    # "type":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 198
    .restart local v1    # "type":Ljava/lang/String;
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 200
    .local v2, "uri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 201
    :catch_0
    move-exception v0

    .line 202
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/sec/samsung/gallery/drawer/DrawerItemViewBy;->TAG:Ljava/lang/String;

    const-string v4, "Failed to get intent content type!"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 203
    const-string v1, ""

    goto :goto_0
.end method

.method private getFilterType(Ljava/lang/String;)Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;
    .locals 1
    .param p1, "contentType"    # Ljava/lang/String;

    .prologue
    .line 208
    const-string/jumbo v0, "video"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    sget-object v0, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    .line 213
    :goto_0
    return-object v0

    .line 210
    :cond_0
    const-string v0, "*/*"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 211
    sget-object v0, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->IMAGE_AND_VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    goto :goto_0

    .line 213
    :cond_1
    sget-object v0, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->IMAGE:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    goto :goto_0
.end method

.method private getFilterTypeToString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemViewBy;->mContentType:Ljava/lang/String;

    const-string/jumbo v1, "video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    const-string/jumbo v0, "video"

    .line 226
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "image"

    goto :goto_0
.end method

.method private getFilteredSetPath(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Landroid/content/Intent;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 10
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "filterType"    # Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;
    .param p4, "filterMimeType"    # Ljava/lang/String;
    .param p5, "onlyLocal"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 150
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    .line 151
    .local v1, "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    const/4 v6, 0x3

    .line 152
    .local v6, "typeBit":I
    if-eqz p5, :cond_2

    .line 153
    const/4 v6, 0x5

    .line 178
    :cond_0
    :goto_0
    const/4 v0, 0x0

    .line 179
    .local v0, "basePath":Ljava/lang/String;
    invoke-virtual {v1, v6}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 180
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_1

    .line 181
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    .line 185
    :cond_1
    if-eqz p4, :cond_9

    .line 186
    invoke-static {p3}, Lcom/sec/android/gallery3d/app/FilterUtils;->toFilterType(Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)I

    move-result v7

    invoke-static {v0, v7, p4}, Lcom/sec/android/gallery3d/app/FilterUtils;->switchFilterPath(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 190
    .local v5, "newPath":Ljava/lang/String;
    :goto_1
    return-object v5

    .line 155
    .end local v0    # "basePath":Ljava/lang/String;
    .end local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v5    # "newPath":Ljava/lang/String;
    :cond_2
    invoke-static {p1, p2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->determineTypeBits(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v6

    .line 156
    const-string v8, "android.intent.action.GET_CONTENT"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    const-string v8, "android.intent.action.MULTIPLE_PICK"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    const-string v8, "android.intent.action.PICK"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 159
    :cond_3
    if-ne v6, v3, :cond_8

    .line 160
    const-string v8, "mode-crop"

    invoke-virtual {p2, v8, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 161
    .local v4, "modeWallpaper":Z
    const-string v8, "crop"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 162
    .local v3, "modeContact":Z
    :goto_2
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCloud:Z

    if-eqz v7, :cond_6

    .line 163
    const/16 v6, 0xd

    .line 164
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSetAsSNSImage:Z

    if-nez v7, :cond_0

    if-nez v4, :cond_4

    if-eqz v3, :cond_0

    .line 165
    :cond_4
    const/16 v6, 0x11

    goto :goto_0

    .end local v3    # "modeContact":Z
    :cond_5
    move v3, v7

    .line 161
    goto :goto_2

    .line 168
    .restart local v3    # "modeContact":Z
    :cond_6
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSetAsSNSImage:Z

    if-nez v7, :cond_0

    if-nez v4, :cond_7

    if-eqz v3, :cond_0

    .line 169
    :cond_7
    const/16 v6, 0x11

    goto :goto_0

    .line 172
    .end local v3    # "modeContact":Z
    .end local v4    # "modeWallpaper":Z
    :cond_8
    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    .line 173
    const/16 v6, 0x14

    goto :goto_0

    .line 188
    .restart local v0    # "basePath":Ljava/lang/String;
    .restart local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_9
    move-object v5, v0

    .restart local v5    # "newPath":Ljava/lang/String;
    goto :goto_1
.end method

.method private getNewPath(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Ljava/lang/String;
    .locals 11
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x0

    .line 119
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 120
    .local v2, "intent":Landroid/content/Intent;
    const-string v0, "senderIsVideoCall"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    .line 121
    .local v10, "videoCall":Z
    const-string v0, "caller_id_pick"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 122
    .local v5, "isContactPickerMode":Z
    const/4 v4, 0x0

    .line 124
    .local v4, "filterMimeType":Ljava/lang/String;
    if-eqz v10, :cond_0

    .line 125
    const-string v0, "onlyJpg"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    .line 126
    .local v8, "onlyJpg":Z
    const-string v0, "only3gp"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    .line 127
    .local v7, "only3gp":Z
    if-eqz v8, :cond_2

    .line 128
    const-string v4, "image/jpeg"

    .line 134
    .end local v7    # "only3gp":Z
    .end local v8    # "onlyJpg":Z
    :cond_0
    :goto_0
    const/4 v9, 0x0

    .line 136
    .local v9, "skipGolf":Z
    if-eqz v9, :cond_1

    .line 137
    const-string/jumbo v4, "skip:image/golf"

    .line 140
    :cond_1
    invoke-direct {p0, p1, v2}, Lcom/sec/samsung/gallery/drawer/DrawerItemViewBy;->getContentType(Landroid/app/Activity;Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemViewBy;->mContentType:Ljava/lang/String;

    .line 142
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemViewBy;->mContentType:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/drawer/DrawerItemViewBy;->getFilterType(Ljava/lang/String;)Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v3

    .line 143
    .local v3, "filterType":Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;
    invoke-direct {p0, p1, v3}, Lcom/sec/samsung/gallery/drawer/DrawerItemViewBy;->setMediaFilterType(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)V

    move-object v0, p0

    move-object v1, p1

    .line 145
    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/drawer/DrawerItemViewBy;->getFilteredSetPath(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Landroid/content/Intent;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 146
    .local v6, "newPath":Ljava/lang/String;
    return-object v6

    .line 129
    .end local v3    # "filterType":Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;
    .end local v6    # "newPath":Ljava/lang/String;
    .end local v9    # "skipGolf":Z
    .restart local v7    # "only3gp":Z
    .restart local v8    # "onlyJpg":Z
    :cond_2
    if-eqz v7, :cond_0

    .line 130
    const-string/jumbo v4, "video/3gp"

    goto :goto_0
.end method

.method private setMediaFilterType(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "filterType"    # Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    .prologue
    .line 217
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    .line 218
    .local v0, "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    if-eqz v0, :cond_0

    .line 219
    invoke-virtual {v0, p2}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentMediaFilterType(Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)V

    .line 220
    :cond_0
    return-void
.end method

.method private startPickMode(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/app/StateManager;IZ)V
    .locals 6
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "statusProxy"    # Lcom/sec/android/gallery3d/app/StateManager;
    .param p3, "index"    # I
    .param p4, "isPickMode"    # Z

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/drawer/DrawerItemViewBy;->getNewPath(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Ljava/lang/String;

    move-result-object v2

    .line 95
    .local v2, "newPath":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v4

    invoke-static {p1, v4, v2}, Lcom/sec/samsung/gallery/view/utils/DataPath;->getViewByTopSetPath(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 97
    .local v3, "viewByTopSetPath":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 98
    move-object v2, v3

    .line 101
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 102
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string v4, "KEY_PICK_MEDIA_TYPE"

    invoke-direct {p0}, Lcom/sec/samsung/gallery/drawer/DrawerItemViewBy;->getFilterTypeToString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    if-nez p3, :cond_1

    .line 106
    const/4 v4, 0x2

    invoke-static {v2, v4}, Lcom/sec/android/gallery3d/app/FilterUtils;->newClusterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 107
    .local v1, "clusterPath":Ljava/lang/String;
    const-string v4, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    sget-object v4, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {p2, v4, p4}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;Z)V

    .line 109
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    const-class v5, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    invoke-virtual {v4, v5, v0}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 117
    .end local v1    # "clusterPath":Ljava/lang/String;
    :goto_0
    return-void

    .line 112
    :cond_1
    sget-object v4, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {p2, v4, p4}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;Z)V

    .line 113
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    const-class v5, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-virtual {v4, v5, v0}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0
.end method


# virtual methods
.method public selectItem(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/app/StateManager;IZ)V
    .locals 8
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "statusProxy"    # Lcom/sec/android/gallery3d/app/StateManager;
    .param p3, "index"    # I
    .param p4, "isPickMode"    # Z

    .prologue
    const/4 v7, 0x1

    .line 50
    sget-object v4, Lcom/sec/samsung/gallery/drawer/DrawerItemViewBy;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Local Selected : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewMode()Ljava/lang/Class;

    move-result-object v2

    .line 52
    .local v2, "currentViewState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 55
    .local v1, "bundle":Landroid/os/Bundle;
    packed-switch p3, :pswitch_data_0

    .line 84
    const-string v4, "KEY_VIEW_REDRAW"

    invoke-virtual {v1, v4, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 85
    move-object v0, v2

    .line 88
    .local v0, "activityState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 89
    .end local v0    # "activityState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    :goto_1
    return-void

    .line 57
    :pswitch_0
    sget-object v4, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {p2, v4, p4}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;Z)V

    .line 58
    if-eqz p4, :cond_0

    .line 59
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/drawer/DrawerItemViewBy;->startPickMode(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/app/StateManager;IZ)V

    goto :goto_1

    .line 62
    :cond_0
    const-class v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .line 63
    .restart local v0    # "activityState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    goto :goto_0

    .line 67
    .end local v0    # "activityState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    :pswitch_1
    const-class v4, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-ne v2, v4, :cond_1

    .line 68
    const-string v4, "KEY_VIEW_REDRAW"

    invoke-virtual {v1, v4, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 70
    :cond_1
    sget-object v4, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {p2, v4, p4}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;Z)V

    .line 71
    if-eqz p4, :cond_3

    .line 72
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v3

    .line 73
    .local v3, "mSelectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 74
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->leaveSelectionMode()V

    .line 76
    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/drawer/DrawerItemViewBy;->startPickMode(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/app/StateManager;IZ)V

    goto :goto_1

    .line 79
    .end local v3    # "mSelectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    :cond_3
    const-class v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .line 80
    .restart local v0    # "activityState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    goto :goto_0

    .line 55
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
