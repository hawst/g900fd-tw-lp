.class Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$7;
.super Ljava/lang/Object;
.source "SlideShowSettingView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->setupVideoView(Landroid/content/Context;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)V
    .locals 0

    .prologue
    .line 482
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$7;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 485
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$7;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mVideoCheckedTextView:Landroid/widget/CheckedTextView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$500(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)Landroid/widget/CheckedTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 486
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$7;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mVideoCheckedTextView:Landroid/widget/CheckedTextView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$500(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)Landroid/widget/CheckedTextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 490
    :goto_0
    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$000()Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$7;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mVideoCheckedTextView:Landroid/widget/CheckedTextView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$500(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)Landroid/widget/CheckedTextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->setVideoPlayEnable(Z)V

    .line 491
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$7;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$100(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)Landroid/content/Context;

    move-result-object v0

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$000()Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->save(Landroid/content/Context;Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;)V

    .line 492
    return-void

    .line 488
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$7;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mVideoCheckedTextView:Landroid/widget/CheckedTextView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$500(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)Landroid/widget/CheckedTextView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_0
.end method
