.class Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$DataLoaderConfig;
.super Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;
.source "AlbumViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DataLoaderConfig"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V
    .locals 1

    .prologue
    .line 2184
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$DataLoaderConfig;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;-><init>()V

    .line 2185
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$DataLoaderConfig;->mScanFirstOnly:Z

    .line 2186
    return-void
.end method


# virtual methods
.method public setAlbumAttribute(Lcom/sec/android/gallery3d/data/MediaSet;I)I
    .locals 2
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "count"    # I

    .prologue
    const/4 v0, 0x0

    .line 2189
    const/16 v1, 0x20

    iput-byte v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$DataLoaderConfig;->mRetThmType:B

    .line 2190
    iput v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$DataLoaderConfig;->mRetLineCount:I

    .line 2191
    if-lez p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
