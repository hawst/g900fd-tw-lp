.class public Lcom/sec/samsung/gallery/controller/ShowDetailsDialogCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "ShowDetailsDialogCmd.java"

# interfaces
.implements Ljava/util/Observer;
.implements Lorg/puremvc/java/interfaces/ICommand;


# instance fields
.field private mDetailsDaialog:Lcom/sec/samsung/gallery/view/common/DetailsDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method private dismissDialog()V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDetailsDialogCmd;->mDetailsDaialog:Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDetailsDialogCmd;->mDetailsDaialog:Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->dismissDialog()V

    .line 71
    :cond_0
    return-void
.end method

.method private showDialog(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contents"    # Ljava/lang/Object;

    .prologue
    .line 52
    if-nez p2, :cond_1

    .line 66
    .end local p2    # "contents":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-void

    .line 55
    .restart local p2    # "contents":Ljava/lang/Object;
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDetailsDialogCmd;->mDetailsDaialog:Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDetailsDialogCmd;->mDetailsDaialog:Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    :cond_2
    instance-of v0, p2, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v0, :cond_3

    .line 60
    new-instance v0, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;

    check-cast p2, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local p2    # "contents":Ljava/lang/Object;
    invoke-direct {v0, p1, p2}, Lcom/sec/samsung/gallery/view/common/DetailsDialogForCamera;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDetailsDialogCmd;->mDetailsDaialog:Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    .line 64
    :goto_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDetailsDialogCmd;->mDetailsDaialog:Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->addObserver(Ljava/util/Observer;)V

    .line 65
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDetailsDialogCmd;->mDetailsDaialog:Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->showDialog()V

    goto :goto_0

    .line 62
    .restart local p2    # "contents":Ljava/lang/Object;
    :cond_3
    new-instance v0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    check-cast p2, Ljava/util/ArrayList;

    .end local p2    # "contents":Ljava/lang/Object;
    invoke-direct {v0, p1, p2}, Lcom/sec/samsung/gallery/view/common/DetailsDialog;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDetailsDialogCmd;->mDetailsDaialog:Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    goto :goto_1
.end method

.method private updateDetailDialogList()V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDetailsDialogCmd;->mDetailsDaialog:Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDetailsDialogCmd;->mDetailsDaialog:Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->updateDetailDialogList()V

    .line 48
    :cond_0
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 4
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 23
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Object;

    move-object v1, v3

    check-cast v1, [Ljava/lang/Object;

    .line 24
    .local v1, "params":[Ljava/lang/Object;
    if-nez v1, :cond_0

    .line 25
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowDetailsDialogCmd;->updateDetailDialogList()V

    .line 37
    :goto_0
    return-void

    .line 29
    :cond_0
    const/4 v3, 0x0

    aget-object v0, v1, v3

    check-cast v0, Landroid/content/Context;

    .line 30
    .local v0, "context":Landroid/content/Context;
    const/4 v3, 0x2

    aget-object v3, v1, v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 31
    .local v2, "show":Z
    if-nez v2, :cond_1

    .line 32
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowDetailsDialogCmd;->dismissDialog()V

    goto :goto_0

    .line 35
    :cond_1
    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-direct {p0, v0, v3}, Lcom/sec/samsung/gallery/controller/ShowDetailsDialogCmd;->showDialog(Landroid/content/Context;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 0
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 42
    return-void
.end method
