.class public Lcom/sec/samsung/gallery/controller/LastShareAppCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "LastShareAppCmd.java"


# static fields
.field public static final LAST_SHARE_ACTIVITY_NAME:Ljava/lang/String; = "ACTIVITY_NAME"

.field private static final LAST_SHARE_APP_NAME:Ljava/lang/String; = "APP_NAME"

.field private static final LAST_SHARE_EXIST:Ljava/lang/String; = "LAST_SHAREAPP_EXIST"

.field public static final LAST_SHARE_PACKAGE_NAME:Ljava/lang/String; = "PACKAGE_NAME"

.field public static final LAST_SHARE_PREFERENCES_NAME:Ljava/lang/String; = "last_shared"


# instance fields
.field protected mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method public static getActivityName(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 236
    const-string v1, "last_shared"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 238
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v1, "ACTIVITY_NAME"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getLastShareAppIcon(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resolveInfo"    # Landroid/content/pm/ResolveInfo;

    .prologue
    const/4 v1, 0x0

    .line 107
    if-eqz p1, :cond_0

    .line 108
    :try_start_0
    iget-object v2, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->icon:I

    if-nez v2, :cond_1

    .line 121
    :cond_0
    :goto_0
    return-object v1

    .line 111
    :cond_1
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v2, :cond_2

    .line 112
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_0

    .line 114
    :cond_2
    invoke-static {p0, p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getApplicationLogo(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 117
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getLastShareAppName(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 210
    if-nez p0, :cond_0

    .line 211
    const-string v1, ""

    .line 219
    :goto_0
    return-object v1

    .line 213
    :cond_0
    const-string v1, "last_shared"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 215
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    if-eqz v0, :cond_1

    .line 216
    const-string v1, "APP_NAME"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 219
    :cond_1
    const-string v1, ""

    goto :goto_0
.end method

.method public static getPackageName(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 242
    const-string v1, "last_shared"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 244
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v1, "PACKAGE_NAME"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getResolveInfo(Landroid/content/Context;)Landroid/content/pm/ResolveInfo;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 249
    :try_start_0
    invoke-static {p0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->getPackageName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 250
    .local v2, "packageName":Ljava/lang/String;
    invoke-static {p0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->getActivityName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 251
    .local v0, "activityName":Ljava/lang/String;
    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 252
    invoke-static {p0, v2, v0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->getResolveInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/pm/ResolveInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 259
    .end local v0    # "activityName":Ljava/lang/String;
    .end local v2    # "packageName":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 255
    :catch_0
    move-exception v1

    .line 256
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 259
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private static getResolveInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/pm/ResolveInfo;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "activityName"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 264
    new-instance v6, Lcom/sec/samsung/gallery/controller/ShareAppUtil;

    invoke-direct {v6, p0}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;-><init>(Landroid/content/Context;)V

    .line 265
    .local v6, "shareAppUtil":Lcom/sec/samsung/gallery/controller/ShareAppUtil;
    invoke-virtual {v6}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->getUriList()Ljava/util/ArrayList;

    move-result-object v7

    .line 266
    .local v7, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_1

    :cond_0
    move-object v4, v8

    .line 281
    :goto_0
    return-object v4

    .line 269
    :cond_1
    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->createIntentForShare(Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v2

    .line 271
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 272
    .local v3, "packageManager":Landroid/content/pm/PackageManager;
    const/4 v9, 0x0

    invoke-virtual {v3, v2, v9}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    .line 274
    .local v5, "shareAppList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    .line 275
    .local v4, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v0, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 276
    .local v0, "activityInfo":Landroid/content/pm/ActivityInfo;
    iget-object v9, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v9, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    iget-object v9, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v9, v9, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v9, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    goto :goto_0

    .end local v0    # "activityInfo":Landroid/content/pm/ActivityInfo;
    .end local v4    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_3
    move-object v4, v8

    .line 281
    goto :goto_0
.end method

.method public static isLastShareAppBluetooth(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 184
    const/4 v1, 0x0

    .line 186
    .local v1, "lastShareApp":Z
    :try_start_0
    invoke-static {p0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->getPackageName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 187
    .local v2, "packageName":Ljava/lang/String;
    if-eqz v2, :cond_0

    const-string v3, "com.android.bluetooth"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    .line 188
    const/4 v1, 0x1

    .line 193
    .end local v1    # "lastShareApp":Z
    .end local v2    # "packageName":Ljava/lang/String;
    :cond_0
    :goto_0
    return v1

    .line 190
    .restart local v1    # "lastShareApp":Z
    :catch_0
    move-exception v0

    .line 191
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static isLastShareAppDropBox(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 158
    const/4 v1, 0x0

    .line 160
    .local v1, "lastShareAppDropBox":Z
    :try_start_0
    invoke-static {p0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->getPackageName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 162
    .local v2, "packageName":Ljava/lang/String;
    if-eqz v2, :cond_0

    const-string v3, "com.dropbox.android"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    .line 163
    const/4 v1, 0x1

    .line 168
    .end local v1    # "lastShareAppDropBox":Z
    .end local v2    # "packageName":Ljava/lang/String;
    :cond_0
    :goto_0
    return v1

    .line 165
    .restart local v1    # "lastShareAppDropBox":Z
    :catch_0
    move-exception v0

    .line 166
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static isLastShareAppExisted(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 223
    if-nez p0, :cond_1

    .line 232
    :cond_0
    :goto_0
    return v1

    .line 226
    :cond_1
    const-string v2, "last_shared"

    invoke-virtual {p0, v2, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 228
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    if-eqz v0, :cond_0

    .line 229
    const-string v2, "LAST_SHAREAPP_EXIST"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_0
.end method

.method public static isLastShareAppFacebook(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 173
    :try_start_0
    invoke-static {p0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->getPackageName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 174
    .local v1, "packageName":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v2, "com.facebook.katana"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    .line 175
    const/4 v2, 0x1

    .line 180
    .end local v1    # "packageName":Ljava/lang/String;
    :goto_0
    return v2

    .line 177
    :catch_0
    move-exception v0

    .line 178
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 180
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isLastShareAppGroupPlay(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 143
    const/4 v2, 0x0

    .line 145
    .local v2, "lastShareAppGroupPlay":Z
    :try_start_0
    invoke-static {p0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->getPackageName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 146
    .local v3, "packageName":Ljava/lang/String;
    invoke-static {p0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->getActivityName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 147
    .local v0, "activityName":Ljava/lang/String;
    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    const-string v4, "com.samsung.groupcast"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_0

    .line 149
    const/4 v2, 0x1

    .line 154
    .end local v0    # "activityName":Ljava/lang/String;
    .end local v2    # "lastShareAppGroupPlay":Z
    .end local v3    # "packageName":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 151
    .restart local v2    # "lastShareAppGroupPlay":Z
    :catch_0
    move-exception v1

    .line 152
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static isLastShareAppWiFiDirect(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 198
    :try_start_0
    invoke-static {p0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->getPackageName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 199
    .local v1, "packageName":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v2, "com.sec.android.app.FileShareClient"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    .line 201
    const/4 v2, 0x1

    .line 206
    .end local v1    # "packageName":Ljava/lang/String;
    :goto_0
    return v2

    .line 203
    :catch_0
    move-exception v0

    .line 204
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 206
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isLastShareAppYouTube(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 127
    const/4 v2, 0x0

    .line 129
    .local v2, "lastShareAppYouTube":Z
    :try_start_0
    invoke-static {p0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->getPackageName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 130
    .local v3, "packageName":Ljava/lang/String;
    invoke-static {p0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->getActivityName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 131
    .local v0, "activityName":Ljava/lang/String;
    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    const-string v4, "com.google.android.youtube"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_0

    .line 133
    const/4 v2, 0x1

    .line 138
    .end local v0    # "activityName":Ljava/lang/String;
    .end local v2    # "lastShareAppYouTube":Z
    .end local v3    # "packageName":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 135
    .restart local v2    # "lastShareAppYouTube":Z
    :catch_0
    move-exception v1

    .line 136
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static launchLastShareApp(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "mimeType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57
    .local p1, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v5, Lcom/sec/samsung/gallery/controller/ShareAppUtil;

    invoke-direct {v5, p0}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;-><init>(Landroid/content/Context;)V

    .line 58
    .local v5, "shareAppUtil":Lcom/sec/samsung/gallery/controller/ShareAppUtil;
    invoke-virtual {v5, p1}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->createIntentForShare(Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v3

    .line 60
    .local v3, "intent":Landroid/content/Intent;
    if-eqz p2, :cond_0

    .line 61
    invoke-virtual {v3, p2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->getPackageName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 66
    .local v4, "packageName":Ljava/lang/String;
    invoke-static {p0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->getActivityName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 67
    .local v1, "activityName":Ljava/lang/String;
    if-eqz v4, :cond_2

    if-eqz v1, :cond_2

    .line 68
    const-string v6, "com.att.android.mobile.attmessages"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 69
    const/high16 v6, 0x80000

    invoke-virtual {v3, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 70
    :cond_1
    new-instance v6, Landroid/content/ComponentName;

    invoke-direct {v6, v4, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 71
    invoke-virtual {p0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 73
    move-object v0, p0

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object v6, v0

    invoke-static {v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v6

    const-string v7, "EXIT_SELECTION_MODE"

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;)V

    .line 75
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->closeCameraLens(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    .end local v1    # "activityName":Ljava/lang/String;
    .end local v4    # "packageName":Ljava/lang/String;
    :cond_2
    :goto_0
    return-void

    .line 77
    :catch_0
    move-exception v2

    .line 78
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static saveLastShareAppInfo(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "info"    # Landroid/content/pm/ResolveInfo;

    .prologue
    .line 84
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 87
    :cond_1
    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 89
    .local v0, "activityInfo":Landroid/content/pm/ActivityInfo;
    if-eqz v0, :cond_0

    .line 92
    const-string v5, "last_shared"

    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 93
    .local v4, "sharedPreferences":Landroid/content/SharedPreferences;
    if-eqz v4, :cond_0

    .line 94
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 95
    .local v2, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v5, "LAST_SHAREAPP_EXIST"

    const/4 v6, 0x1

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 96
    const-string v5, "PACKAGE_NAME"

    iget-object v6, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v6, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 97
    const-string v5, "ACTIVITY_NAME"

    iget-object v6, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 98
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 99
    .local v3, "label":Ljava/lang/CharSequence;
    if-nez v3, :cond_2

    const-string v1, ""

    .line 100
    .local v1, "appName":Ljava/lang/String;
    :goto_1
    const-string v5, "APP_NAME"

    invoke-interface {v2, v5, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 101
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 99
    .end local v1    # "appName":Ljava/lang/String;
    :cond_2
    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public static updateLastShareAppName(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resolveInfo"    # Landroid/content/pm/ResolveInfo;

    .prologue
    .line 285
    if-eqz p1, :cond_0

    .line 286
    invoke-static {p0, p1}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->saveLastShareAppInfo(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)V

    .line 288
    :cond_0
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 5
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 45
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Object;

    move-object v0, v3

    check-cast v0, [Ljava/lang/Object;

    .line 46
    .local v0, "params":[Ljava/lang/Object;
    const/4 v3, 0x0

    aget-object v3, v0, v3

    check-cast v3, Landroid/content/Context;

    iput-object v3, p0, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->mContext:Landroid/content/Context;

    .line 48
    new-instance v1, Lcom/sec/samsung/gallery/controller/ShareAppUtil;

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;-><init>(Landroid/content/Context;)V

    .line 49
    .local v1, "shareAppUtil":Lcom/sec/samsung/gallery/controller/ShareAppUtil;
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->getUriList()Ljava/util/ArrayList;

    move-result-object v2

    .line 50
    .local v2, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->existImageNotCached()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 51
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->mContext:Landroid/content/Context;

    const v4, 0x7f0e04ba

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 53
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->getMimeType()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v2, v4}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->launchLastShareApp(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 54
    return-void
.end method
