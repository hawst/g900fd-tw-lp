.class Lcom/sec/samsung/gallery/glview/GlPhotoView$2;
.super Ljava/lang/Object;
.source "GlPhotoView.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$GlAnimationProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 0

    .prologue
    .line 767
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationProgress(FLcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 11
    .param p1, "ratio"    # F
    .param p2, "obj"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/4 v4, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    .line 771
    if-nez p2, :cond_1

    .line 791
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v5, p2

    .line 773
    check-cast v5, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 774
    .local v5, "baseObj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    check-cast v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    .line 775
    .local v0, "adapter":Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimType:I

    sget v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_TYPE_VIEW:I

    if-ne v1, v2, :cond_0

    iget v1, v5, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$100(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 777
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    move-result-object v1

    iget v10, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemW:F

    .line 778
    .local v10, "splitW":F
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    move-result-object v1

    iget v9, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemH:F

    .line 779
    .local v9, "splitH":F
    invoke-virtual {p2, v4}, Lcom/sec/android/gallery3d/glcore/GlObject;->getWidth(Z)F

    move-result v7

    .line 780
    .local v7, "albumW":F
    invoke-virtual {p2, v4}, Lcom/sec/android/gallery3d/glcore/GlObject;->getHeight(Z)F

    move-result v6

    .line 781
    .local v6, "albumH":F
    div-float v8, v10, v7

    .line 782
    .local v8, "r":F
    const v1, 0x3f666666    # 0.9f

    cmpl-float v1, v8, v1

    if-lez v1, :cond_2

    .line 783
    iput v3, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mRatio:F

    .line 784
    iput v3, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mAspectRatioCorrection:F

    .line 789
    :goto_1
    iget v1, v5, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/glcore/GlObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThisListView:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    goto :goto_0

    .line 786
    :cond_2
    iput v8, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mRatio:F

    .line 787
    div-float v1, v7, v6

    div-float v2, v10, v9

    div-float/2addr v1, v2

    iput v1, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mAspectRatioCorrection:F

    goto :goto_1
.end method
