.class public Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;
.source "PhotoActionBarForEdit.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

.field private mMenu:Landroid/view/Menu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 46
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;)Landroid/view/Menu;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->setSelectAllButtonTitle(ILjava/lang/String;)V

    return-void
.end method

.method private getOptionMenuId()I
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEmergencyMode(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 433
    const v0, 0x7f120020

    .line 439
    :goto_0
    return v0

    .line 434
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 435
    const v0, 0x7f12002d

    goto :goto_0

    .line 436
    :cond_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 437
    const v0, 0x7f12002f

    goto :goto_0

    .line 439
    :cond_2
    const v0, 0x7f12002e

    goto :goto_0
.end method

.method private hideAllMenuItems(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 418
    const v0, 0x7f0f02d1

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 419
    return-void
.end method

.method private isValidSelection()Z
    .locals 8

    .prologue
    .line 449
    const/4 v0, 0x1

    .line 451
    .local v0, "isValid":Z
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    .line 452
    .local v1, "selectedCount":I
    if-nez v1, :cond_1

    .line 453
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v4, 0x7f0e0113

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 454
    const/4 v0, 0x0

    .line 462
    :cond_0
    :goto_0
    return v0

    .line 455
    :cond_1
    sget v3, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    if-le v1, v3, :cond_0

    .line 456
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0114

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    sget v7, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 458
    .local v2, "text":Ljava/lang/CharSequence;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 459
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private makeMotionPicture()V
    .locals 5

    .prologue
    .line 466
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v3

    .line 467
    .local v3, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 477
    :goto_0
    return-void

    .line 470
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 471
    .local v0, "burstShotList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 472
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 473
    .local v2, "media":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 471
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 476
    .end local v2    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v4, v0}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->makeMotionPicture(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private startStoryAlbumApp()V
    .locals 3

    .prologue
    .line 444
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "START_STORY_ALBUM_APP"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 446
    return-void
.end method


# virtual methods
.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 51
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 52
    .local v0, "inflater":Landroid/view/MenuInflater;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->getOptionMenuId()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 53
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 248
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 380
    :cond_0
    :goto_0
    return-void

    .line 250
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->isValidSelection()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 251
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->chooseShareDialog()V

    goto :goto_0

    .line 255
    :sswitch_1
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_SLIDESHOW_SETTING:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 259
    :sswitch_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PHSD"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1, v2, v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 260
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_DELETE_DIALOG:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 263
    :sswitch_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->copyToClipboard()V

    goto :goto_0

    .line 266
    :sswitch_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/16 v2, -0x5a

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->rotate(I)V

    goto :goto_0

    .line 269
    :sswitch_5
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/16 v2, 0x5a

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->rotate(I)V

    goto :goto_0

    .line 272
    :sswitch_6
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showRenameDialog(Z)V

    goto :goto_0

    .line 275
    :sswitch_7
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showDetailsDialog(Z)V

    goto :goto_0

    .line 278
    :sswitch_8
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_COPY_FILES:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 281
    :sswitch_9
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_MOVE_FILES:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 284
    :sswitch_a
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showSetAsDialog(Z)V

    goto :goto_0

    .line 287
    :sswitch_b
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->printImage()V

    goto/16 :goto_0

    .line 290
    :sswitch_c
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->imageEdit()V

    goto/16 :goto_0

    .line 293
    :sswitch_d
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    sget-object v2, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;->CONFIRM_TAG:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    invoke-virtual {v1, v3, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showRemoveConfirmTagDialog(ZLcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;)V

    goto/16 :goto_0

    .line 296
    :sswitch_e
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/16 v2, 0x204

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->startContactActivity(I)V

    goto/16 :goto_0

    .line 299
    :sswitch_f
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    sget-object v2, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;->REMOVE_TAG:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    invoke-virtual {v1, v3, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showRemoveConfirmTagDialog(ZLcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;)V

    goto/16 :goto_0

    .line 302
    :sswitch_10
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_BURST_SHOT_PLAY:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 305
    :sswitch_11
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->download()V

    goto/16 :goto_0

    .line 308
    :sswitch_12
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_IMPORT:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 311
    :sswitch_13
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->isValidSelection()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 312
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAdvancedSoundSceneShare:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isASoundSceneFileSelected(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/golf/GolfMgr;->isGolfFileSelected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 314
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showImageVideoConversionShareDialog(ZZ)V

    goto/16 :goto_0

    .line 316
    :cond_3
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_LAST_SHARE_APP:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 321
    :sswitch_14
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3, v2, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->hideAlbums(ZZZ)V

    goto/16 :goto_0

    .line 324
    :sswitch_15
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->moveToSecretbox(ZZ)V

    goto/16 :goto_0

    .line 327
    :sswitch_16
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_FROM_SECRETBOX:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 330
    :sswitch_17
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->moveToKNOX(ZZ)V

    goto/16 :goto_0

    .line 333
    :sswitch_18
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->moveToKNOX(ZZ)V

    goto/16 :goto_0

    .line 337
    :sswitch_19
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->startStoryAlbumApp()V

    goto/16 :goto_0

    .line 340
    :sswitch_1a
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->makeMotionPicture()V

    goto/16 :goto_0

    .line 343
    :sswitch_1b
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_MAKE_BUSRTSHOT_PICTURE:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 346
    :sswitch_1c
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_CREATE_VIDEO_ALBUM:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 349
    :sswitch_1d
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->CopyToEvent(Z)V

    goto/16 :goto_0

    .line 352
    :sswitch_1e
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v1, v0, v2

    const/4 v1, 0x0

    aput-object v1, v0, v3

    .line 353
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "S_STUDIO"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 356
    .end local v0    # "params":[Ljava/lang/Object;
    :sswitch_1f
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->IsCategoryView(Lcom/sec/samsung/gallery/core/TabTagType;Ljava/lang/Boolean;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 357
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_CATEGORY:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 359
    :cond_4
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_DELETE_DIALOG:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 363
    :sswitch_20
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_EDIT_CATEGORY:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 367
    :sswitch_21
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->IsCategoryView(Lcom/sec/samsung/gallery/core/TabTagType;Ljava/lang/Boolean;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 368
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_CATEGORY:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 370
    :cond_5
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_DELETE_DIALOG:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 374
    :sswitch_22
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/16 v2, 0x205

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAddUserTagDialog(IZ)V

    goto/16 :goto_0

    .line 377
    :sswitch_23
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->startGifMaker()V

    goto/16 :goto_0

    .line 248
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f026c -> :sswitch_0
        0x7f0f026d -> :sswitch_13
        0x7f0f026e -> :sswitch_2
        0x7f0f026f -> :sswitch_1
        0x7f0f0270 -> :sswitch_6
        0x7f0f0271 -> :sswitch_14
        0x7f0f0272 -> :sswitch_d
        0x7f0f0273 -> :sswitch_e
        0x7f0f0274 -> :sswitch_f
        0x7f0f0275 -> :sswitch_19
        0x7f0f0276 -> :sswitch_11
        0x7f0f0277 -> :sswitch_15
        0x7f0f0278 -> :sswitch_16
        0x7f0f0279 -> :sswitch_17
        0x7f0f027a -> :sswitch_18
        0x7f0f027b -> :sswitch_21
        0x7f0f027c -> :sswitch_12
        0x7f0f027d -> :sswitch_8
        0x7f0f027e -> :sswitch_9
        0x7f0f027f -> :sswitch_1d
        0x7f0f0280 -> :sswitch_4
        0x7f0f0281 -> :sswitch_5
        0x7f0f0282 -> :sswitch_22
        0x7f0f0283 -> :sswitch_1e
        0x7f0f0285 -> :sswitch_b
        0x7f0f0297 -> :sswitch_1f
        0x7f0f029a -> :sswitch_a
        0x7f0f029b -> :sswitch_7
        0x7f0f029c -> :sswitch_c
        0x7f0f02a3 -> :sswitch_10
        0x7f0f02a7 -> :sswitch_3
        0x7f0f02b3 -> :sswitch_1a
        0x7f0f02b4 -> :sswitch_1b
        0x7f0f02b6 -> :sswitch_2
        0x7f0f02cb -> :sswitch_20
        0x7f0f02cd -> :sswitch_1c
        0x7f0f02ce -> :sswitch_23
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 12
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    .line 58
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->hideAllMenuItems(Landroid/view/Menu;)V

    .line 59
    const v7, 0x7f0f02cc

    const/4 v8, 0x1

    invoke-interface {p1, v7, v8}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 60
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->getSupportedOperationForSelectedItem()J

    move-result-wide v4

    .line 61
    .local v4, "supportOperation":J
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7, p1, v4, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkLastShareAppSupport(Landroid/content/Context;Landroid/view/Menu;J)J

    move-result-wide v4

    .line 62
    invoke-static {p1, v4, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->updateMenuOperation(Landroid/view/Menu;J)V

    .line 64
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    .line 65
    .local v2, "isNothingSelected":Z
    if-eqz v2, :cond_1

    .line 66
    const v7, 0x7f0f02cc

    const/4 v8, 0x0

    invoke-interface {p1, v7, v8}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isLonglifeModeOn(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 72
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f02b4

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 75
    :cond_2
    const v7, 0x7f0f0275

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 77
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEditBurstShot:Z

    if-eqz v7, :cond_d

    .line 78
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f02b3

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 79
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGroupId:Z

    if-eqz v7, :cond_c

    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCreateAGIF:Z

    if-eqz v7, :cond_c

    .line 80
    const-wide v8, 0x200000000L

    and-long/2addr v8, v4

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v7

    const/4 v8, 0x2

    if-ge v7, v8, :cond_3

    .line 81
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f02b4

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 97
    :cond_3
    :goto_1
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v7, :cond_11

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecretModeOn()Z

    move-result v7

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/samsung/android/privatemode/PrivateModeManager;->isPrivateStorageMounted(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_5

    :cond_4
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isReadyPrivateMode(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 99
    :cond_5
    const-wide/32 v8, 0x40000000

    and-long/2addr v8, v4

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-eqz v7, :cond_f

    .line 100
    const v7, 0x7f0f0277

    const/4 v8, 0x1

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 104
    :goto_2
    const-wide/32 v8, 0x20000000

    and-long/2addr v8, v4

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-eqz v7, :cond_10

    .line 105
    const v7, 0x7f0f0278

    const/4 v8, 0x1

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 106
    const v7, 0x7f0f0271

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 115
    :goto_3
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableMenuHide:Z

    if-eqz v7, :cond_6

    .line 116
    const v7, 0x7f0f0271

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 119
    :cond_6
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsGuestMode:Z

    if-nez v7, :cond_15

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOXInstallMode(Landroid/content/Context;)Z

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_15

    .line 120
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_13

    .line 121
    const v7, 0x7f0f0279

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 122
    const-wide/32 v8, 0x1000000

    and-long/2addr v8, v4

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-eqz v7, :cond_12

    .line 123
    const v7, 0x7f0f027a

    const/4 v8, 0x1

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 141
    :goto_4
    const v7, 0x7f0f026d

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 142
    const v7, 0x7f0f027b

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 143
    const v7, 0x7f0f02ce

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 147
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget v7, v7, Landroid/content/res/Configuration;->orientation:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_16

    .line 148
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v7

    if-nez v7, :cond_7

    .line 149
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f026e

    const/4 v9, 0x2

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 151
    :cond_7
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 152
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f026c

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 153
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f026c

    const v9, 0x7f020030

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIcon(Landroid/view/Menu;II)V

    .line 162
    :cond_8
    :goto_5
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 163
    const v7, 0x7f0f0280

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 164
    const v7, 0x7f0f0281

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 165
    const v7, 0x7f0f029b

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 166
    const v7, 0x7f0f0271

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 167
    const v7, 0x7f0f0283

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 171
    :cond_9
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEventViewMode(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_17

    .line 172
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f026e

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 173
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f02b6

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 174
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f027f

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 175
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f0280

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 176
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f0281

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 177
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f027c

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 178
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f02cd

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 179
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f0275

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 180
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f0271

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 181
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f0297

    const/4 v9, 0x1

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 182
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f02cb

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 184
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v6

    .line 185
    .local v6, "type":Lcom/sec/samsung/gallery/core/TabTagType;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v7, v6}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v8, v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 186
    .local v1, "category_name":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v8, 0x7f0e0463

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v1, v9, v10

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 187
    .local v0, "category_menu":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f0297

    invoke-static {v7, v8, v0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemTitle(Landroid/view/Menu;ILjava/lang/String;)V

    .line 229
    .end local v0    # "category_menu":Ljava/lang/String;
    .end local v1    # "category_name":Ljava/lang/String;
    .end local v6    # "type":Lcom/sec/samsung/gallery/core/TabTagType;
    :goto_6
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOXInstallMode(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_a

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 230
    const v7, 0x7f0f0280

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 231
    const v7, 0x7f0f0281

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 233
    :cond_a
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/samsung/gallery/controller/SStudioCmd;->isSStudioAvailable(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_1e

    .line 234
    const v7, 0x7f0f0283

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 241
    :cond_b
    :goto_7
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-nez v7, :cond_0

    .line 242
    const v7, 0x7f0f0282

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_0

    .line 84
    :cond_c
    const v7, 0x7f0f02b4

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_1

    .line 87
    :cond_d
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f02b4

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 88
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGroupId:Z

    if-eqz v7, :cond_e

    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCreateAGIF:Z

    if-eqz v7, :cond_e

    .line 89
    const-wide v8, 0x200000000L

    and-long/2addr v8, v4

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v7

    const/4 v8, 0x2

    if-ge v7, v8, :cond_3

    .line 90
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f02b3

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_1

    .line 93
    :cond_e
    const v7, 0x7f0f02b3

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_1

    .line 102
    :cond_f
    const v7, 0x7f0f0277

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_2

    .line 108
    :cond_10
    const v7, 0x7f0f0278

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_3

    .line 111
    :cond_11
    const v7, 0x7f0f0277

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 112
    const v7, 0x7f0f0278

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_3

    .line 125
    :cond_12
    const v7, 0x7f0f027a

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_4

    .line 128
    :cond_13
    const v7, 0x7f0f027a

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 129
    const-wide/32 v8, 0x2000000

    and-long/2addr v8, v4

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-eqz v7, :cond_14

    .line 130
    const v7, 0x7f0f0279

    const/4 v8, 0x1

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_4

    .line 132
    :cond_14
    const v7, 0x7f0f0279

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_4

    .line 136
    :cond_15
    const v7, 0x7f0f0279

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 137
    const v7, 0x7f0f027a

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_4

    .line 156
    :cond_16
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v7

    if-nez v7, :cond_8

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v7

    if-nez v7, :cond_8

    .line 157
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f026e

    const/4 v9, 0x2

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    goto/16 :goto_5

    .line 189
    :cond_17
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v7

    sget-object v8, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v7, v8, :cond_18

    .line 190
    const v7, 0x7f0f029b

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 191
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v7

    const-string v8, "UPDATE_CONFIRM_MENU"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 192
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f0297

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 193
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f02cb

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_6

    .line 194
    :cond_18
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/util/GalleryUtils;->IsCategoryView(Lcom/sec/samsung/gallery/core/TabTagType;Ljava/lang/Boolean;)Z

    move-result v7

    if-eqz v7, :cond_1b

    .line 195
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f027f

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 196
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f0280

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 197
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f0281

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 198
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f027c

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 199
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f02cd

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 200
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f0275

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 201
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f0271

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 202
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f02b6

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 203
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f026e

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 204
    if-eqz v2, :cond_19

    .line 205
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f0297

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 206
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f02cb

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 207
    const v7, 0x7f0f029b

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_6

    .line 209
    :cond_19
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v6

    .line 210
    .restart local v6    # "type":Lcom/sec/samsung/gallery/core/TabTagType;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v7, v6}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v8, v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 211
    .restart local v1    # "category_name":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v8, 0x7f0e0463

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v1, v9, v10

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 212
    .restart local v0    # "category_menu":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f0297

    invoke-static {v7, v8, v0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemTitle(Landroid/view/Menu;ILjava/lang/String;)V

    .line 213
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEditIconShowInCategory:Z

    if-eqz v7, :cond_1a

    .line 214
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f0283

    const/4 v9, 0x2

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    goto/16 :goto_6

    .line 216
    :cond_1a
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f0283

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    goto/16 :goto_6

    .line 219
    .end local v0    # "category_menu":Ljava/lang/String;
    .end local v1    # "category_name":Ljava/lang/String;
    .end local v6    # "type":Lcom/sec/samsung/gallery/core/TabTagType;
    :cond_1b
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v7

    sget-object v8, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eq v7, v8, :cond_1c

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v7

    sget-object v8, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v7, v8, :cond_1d

    .line 221
    :cond_1c
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f02b6

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 222
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f0297

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 223
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f02cb

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_6

    .line 225
    :cond_1d
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f0297

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 226
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v8, 0x7f0f02cb

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_6

    .line 235
    :cond_1e
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableStudioShowAsAction:Z

    if-eqz v7, :cond_b

    .line 236
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    .line 237
    .local v3, "locale":Ljava/lang/String;
    const-string/jumbo v7, "vi"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 238
    const v7, 0x7f0f0283

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    goto/16 :goto_7
.end method

.method public setTitle(I)V
    .locals 2
    .param p1, "numberOfItemsSelected"    # I

    .prologue
    .line 384
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->getTotalSelectedItemsCount()I

    move-result v0

    if-ne p1, v0, :cond_1

    if-eqz p1, :cond_1

    .line 415
    :cond_0
    :goto_0
    return-void

    .line 387
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit$1;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public updateConfirm(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "format"    # Ljava/lang/String;

    .prologue
    .line 423
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    if-eqz v1, :cond_0

    .line 424
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v2, 0x7f0f0272

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 425
    .local v0, "confirmAsMenu":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 426
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 429
    .end local v0    # "confirmAsMenu":Landroid/view/MenuItem;
    :cond_0
    return-void
.end method
