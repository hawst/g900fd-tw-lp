.class Lcom/sec/samsung/gallery/view/timeview/TimeViewState$DataLoaderConfig;
.super Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;
.source "TimeViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/timeview/TimeViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DataLoaderConfig"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V
    .locals 1

    .prologue
    .line 2854
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$DataLoaderConfig;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;-><init>()V

    .line 2855
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$DataLoaderConfig;->mFirstRangeOptimization:Z

    .line 2856
    return-void
.end method


# virtual methods
.method public setAlbumAttribute(Lcom/sec/android/gallery3d/data/MediaSet;I)I
    .locals 1
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "count"    # I

    .prologue
    .line 2859
    const/16 v0, 0x20

    iput-byte v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$DataLoaderConfig;->mRetThmType:B

    .line 2860
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$DataLoaderConfig;->mRetLineCount:I

    .line 2861
    return p2
.end method
