.class Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;
.super Ljava/lang/Object;
.source "GlSearchView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlSearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EstimationInfo"
.end annotation


# instance fields
.field firstX:F

.field firstY:F

.field firstZ:F

.field gapH:F

.field gapW:F

.field hCount:I

.field objH:F

.field objW:F

.field scroll:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V
    .locals 0

    .prologue
    .line 732
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/glview/GlSearchView;Lcom/sec/samsung/gallery/glview/GlSearchView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/glview/GlSearchView$1;

    .prologue
    .line 732
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;-><init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V

    return-void
.end method


# virtual methods
.method public getObject(I)Lcom/sec/android/gallery3d/glcore/GlObject;
    .locals 7
    .param p1, "index"    # I

    .prologue
    .line 743
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlSearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->objW:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->objH:F

    invoke-direct {v1, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 745
    .local v1, "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->hCount:I

    rem-int v0, p1, v3

    .line 746
    .local v0, "col":I
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->hCount:I

    div-int v2, p1, v3

    .line 747
    .local v2, "row":I
    int-to-float v3, v0

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->gapW:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->firstX:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->firstY:F

    int-to-float v5, v2

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->gapH:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->scroll:F

    add-float/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->firstZ:F

    invoke-virtual {v1, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPos(FFF)V

    .line 748
    return-object v1
.end method

.method public saveState()V
    .locals 1

    .prologue
    .line 752
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemW:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->objW:F

    .line 753
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemH:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->objH:F

    .line 754
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapW:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->gapW:F

    .line 755
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapH:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->gapH:F

    .line 756
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemStartX:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->firstX:F

    .line 757
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemStartY:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->firstY:F

    .line 758
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemStartZ:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->firstZ:F

    .line 759
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->hCount:I

    .line 760
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;->scroll:F

    .line 761
    return-void
.end method
