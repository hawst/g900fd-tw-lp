.class Lcom/sec/samsung/gallery/drawer/DrawerAdapter$2;
.super Ljava/lang/Object;
.source "DrawerAdapter.java"

# interfaces
.implements Lcom/sec/samsung/gallery/drawer/IDrawerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/drawer/DrawerAdapter;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$2;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdate()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 72
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$2;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    iget-object v1, v1, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupFilterBy:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->getGroup()Lcom/sec/samsung/gallery/drawer/Group;

    move-result-object v0

    .line 73
    .local v0, "groupFilterBy":Lcom/sec/samsung/gallery/drawer/Group;
    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/Group;->getChildrenCount()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$2;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$100(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 75
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$2;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$100(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 76
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$2;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # setter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mHasFilterBy:Z
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$202(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;Z)Z

    .line 81
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$2;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->notifyDataSetChanged()V

    .line 82
    return-void

    .line 77
    :cond_1
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/Group;->getChildrenCount()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$2;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$100(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$2;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$100(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
