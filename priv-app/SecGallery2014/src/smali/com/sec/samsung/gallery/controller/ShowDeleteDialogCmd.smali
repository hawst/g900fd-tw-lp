.class public Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "ShowDeleteDialogCmd.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Ljava/util/Observer;
.implements Lorg/puremvc/java/interfaces/ICommand;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$UpdateProgressTask;
    }
.end annotation


# static fields
.field private static final AVERAGE_DB_OPERATION_TIME:J = 0xfL

.field private static final EXTERNAL_SD_DIRECTORY:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field mContext:Landroid/content/Context;

.field mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

.field mDeleteDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

.field private mElapsedTime:J

.field private mFakeProgress:Z

.field private mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

.field private mIsAirButton:Z

.field private mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

.field mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-class v0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->TAG:Ljava/lang/String;

    .line 81
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->EXTERNAL_SD_DIRECTORY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 65
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 80
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mIsAirButton:Z

    .line 83
    const-wide/16 v0, 0xf

    iput-wide v0, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mElapsedTime:J

    .line 84
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mFakeProgress:Z

    .line 389
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->EXTERNAL_SD_DIRECTORY:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;Lcom/sec/samsung/gallery/util/FileUtil;)Lcom/sec/samsung/gallery/util/FileUtil;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/util/FileUtil;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mFakeProgress:Z

    return v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mElapsedTime:J

    return-wide v0
.end method

.method static synthetic access$602(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;
    .param p1, "x1"    # J

    .prologue
    .line 65
    iput-wide p1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mElapsedTime:J

    return-wide p1
.end method

.method private checkMetaItemExist()Z
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 137
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCloud:Z

    if-nez v9, :cond_0

    move v9, v10

    .line 157
    :goto_0
    return v9

    .line 139
    :cond_0
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v8

    .line 140
    .local v8, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v7

    .line 142
    .local v7, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 143
    .local v5, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v9, v5, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;

    if-eqz v9, :cond_1

    move-object v1, v5

    .line 144
    check-cast v1, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;

    .line 145
    .local v1, "cloud":Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;->getTotalMediaItemCount()I

    move-result v2

    .line 146
    .local v2, "count":I
    invoke-virtual {v1, v10, v2}, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v6

    .line 147
    .local v6, "mediaItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz v6, :cond_1

    .line 148
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 149
    .local v0, "citem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_2

    instance-of v9, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-eqz v9, :cond_2

    .line 150
    check-cast v0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    .end local v0    # "citem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->getCacheStatus()I

    move-result v9

    if-ne v9, v11, :cond_2

    move v9, v11

    .line 151
    goto :goto_0

    .end local v1    # "cloud":Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;
    .end local v2    # "count":I
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v6    # "mediaItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_3
    move v9, v10

    .line 157
    goto :goto_0
.end method

.method private dismissdialog()V
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mDeleteDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mDeleteDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->dismissDialog()V

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    if-eqz v0, :cond_1

    .line 181
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/FileUtil;->cancelOperation()V

    .line 182
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    .line 184
    :cond_1
    return-void
.end method

.method private getNumberOfItemsToDelete()I
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    return v0
.end method

.method private getPopupText(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isEntireAlbumsSelected"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 195
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 200
    .local v0, "currentState":Lcom/sec/android/gallery3d/app/ActivityState;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->getNumberOfItemsToDelete()I

    move-result v1

    if-ne v1, v2, :cond_2

    .line 201
    instance-of v1, v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mIsAirButton:Z

    if-nez v1, :cond_0

    .line 202
    const v1, 0x7f0e01c3

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 218
    :goto_0
    return-object v1

    .line 205
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEventViewMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewMode()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCameraQuickViewOnLockscreen(Landroid/app/Activity;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 206
    const v1, 0x7f0e0044

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 208
    :cond_1
    const v1, 0x7f0e0042

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 211
    :cond_2
    instance-of v1, v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-eqz v1, :cond_3

    .line 212
    const v1, 0x7f0e01c4

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->getNumberOfItemsToDelete()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 214
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEventViewMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCameraQuickViewOnLockscreen(Landroid/app/Activity;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 215
    const v1, 0x7f0e0045

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->getNumberOfItemsToDelete()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 218
    :cond_4
    const v1, 0x7f0e0043

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->getNumberOfItemsToDelete()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method

.method private getPopupTitleStringId()I
    .locals 3

    .prologue
    .line 187
    const v0, 0x7f0e0041

    .line 188
    .local v0, "titleId":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEventViewMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewMode()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCameraQuickViewOnLockscreen(Landroid/app/Activity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 189
    const v0, 0x7f0e0463

    .line 191
    :cond_0
    return v0
.end method

.method private handleDeleteMedias(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 14
    .param p1, "parentMediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 229
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v8

    .line 230
    .local v8, "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEventViewMode(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSupportDeleteFromMapView(Landroid/content/Context;)Z

    move-result v9

    if-nez v9, :cond_2

    instance-of v9, v8, Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    if-nez v9, :cond_0

    instance-of v9, v8, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    if-nez v9, :cond_0

    instance-of v9, v8, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-eqz v9, :cond_2

    :cond_0
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewMode()Ljava/lang/Class;

    move-result-object v9

    const-class v10, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    if-eq v9, v10, :cond_2

    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    check-cast v9, Landroid/app/Activity;

    invoke-static {v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCameraQuickViewOnLockscreen(Landroid/app/Activity;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 233
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    .line 234
    .local v0, "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v0}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/EventAlbumManager;

    move-result-object v2

    .line 235
    .local v2, "eventMgr":Lcom/sec/android/gallery3d/data/EventAlbumManager;
    if-eqz v2, :cond_1

    .line 236
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    iget-object v10, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    const/4 v11, 0x1

    invoke-virtual {v2, v9, v10, v11}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->deleteEvent(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Z)V

    .line 377
    .end local v0    # "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    .end local v2    # "eventMgr":Lcom/sec/android/gallery3d/data/EventAlbumManager;
    :cond_1
    :goto_0
    return-void

    .line 240
    :cond_2
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v3

    .line 241
    .local v3, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    if-eqz v3, :cond_3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 242
    :cond_3
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    const v10, 0x7f0e0113

    invoke-static {v9, v10}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 246
    :cond_4
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->getPopupTitleStringId()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 247
    .local v6, "title":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    .line 249
    .local v1, "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    const-string v10, "AverageDeleteDbOperationTime"

    const-wide/16 v12, 0xf

    invoke-static {v9, v10, v12, v13}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadLongKey(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v10

    iput-wide v10, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mElapsedTime:J

    .line 251
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v9

    const/4 v10, 0x1

    if-gt v9, v10, :cond_5

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_6

    instance-of v9, v8, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-eqz v9, :cond_6

    .line 252
    :cond_5
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDialogWithMessage:Z

    if-eqz v9, :cond_7

    .line 253
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    const v10, 0x7f0e00ca

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 254
    .local v4, "message":Ljava/lang/String;
    new-instance v9, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v10, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {v9, v10, v4}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    .line 255
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v10, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v10

    invoke-virtual {v9, v10}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalCount(I)V

    .line 256
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v10, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v9, v10, v11}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalSize(J)V

    .line 257
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    const/4 v10, 0x1

    invoke-virtual {v9, v6, v10, p0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->showProgressDialog(Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 266
    .end local v4    # "message":Ljava/lang/String;
    :cond_6
    :goto_1
    new-instance v5, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;

    invoke-direct {v5, p0, v1}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;Lcom/sec/android/gallery3d/data/DataManager;)V

    .line 370
    .local v5, "onProgressListener":Lcom/sec/android/gallery3d/data/OnProgressListener;
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v7

    .line 371
    .local v7, "topViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    const/4 v9, 0x0

    invoke-interface {v3, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    instance-of v9, v9, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v9, :cond_8

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v9

    const/4 v10, 0x1

    if-le v9, v10, :cond_8

    instance-of v9, v7, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    if-nez v9, :cond_8

    .line 372
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    invoke-virtual {v9, p1, v5}, Lcom/sec/samsung/gallery/util/FileUtil;->operateMediaList(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/OnProgressListener;)Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;

    .line 373
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mFakeProgress:Z

    goto/16 :goto_0

    .line 259
    .end local v5    # "onProgressListener":Lcom/sec/android/gallery3d/data/OnProgressListener;
    .end local v7    # "topViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_7
    new-instance v9, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v10, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {v9, v10}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    .line 260
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v10, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v10

    invoke-virtual {v9, v10}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalCount(I)V

    .line 261
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v10, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v9, v10, v11}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalSize(J)V

    .line 262
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-virtual {v9, v6, v10, v11, p0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->showProgressDialog(Ljava/lang/String;Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_1

    .line 375
    .restart local v5    # "onProgressListener":Lcom/sec/android/gallery3d/data/OnProgressListener;
    .restart local v7    # "topViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_8
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    invoke-virtual {v9, p1, v5}, Lcom/sec/samsung/gallery/util/FileUtil;->operateMedias(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/OnProgressListener;)Lcom/sec/samsung/gallery/util/MediaOperations;

    goto/16 :goto_0
.end method

.method private showCloudDialog(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "popupText"    # Ljava/lang/String;

    .prologue
    .line 161
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->dismissdialog()V

    .line 162
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELETE_CLOUD_MEDIA:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    .line 163
    .local v0, "event":Lcom/sec/samsung/gallery/core/Event;
    new-instance v1, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    const v2, 0x7f0e0239

    invoke-direct {v1, p1, v2, p2, v0}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;-><init>(Landroid/content/Context;ILjava/lang/String;Lcom/sec/samsung/gallery/core/Event;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mDeleteDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    .line 164
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mDeleteDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    invoke-virtual {v1, p0}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->addObserver(Ljava/util/Observer;)V

    .line 165
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mDeleteDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->showDialog()V

    .line 166
    return-void
.end method

.method private showDialog(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "popupText"    # Ljava/lang/String;

    .prologue
    .line 169
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->dismissdialog()V

    .line 170
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELETE_MEDIA:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    .line 171
    .local v0, "event":Lcom/sec/samsung/gallery/core/Event;
    new-instance v1, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->getPopupTitleStringId()I

    move-result v2

    invoke-direct {v1, p1, v2, p2, v0}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;-><init>(Landroid/content/Context;ILjava/lang/String;Lcom/sec/samsung/gallery/core/Event;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mDeleteDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    .line 172
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mDeleteDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mIsAirButton:Z

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->setIsAirButton(Z)V

    .line 173
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mDeleteDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    invoke-virtual {v1, p0}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->addObserver(Ljava/util/Observer;)V

    .line 174
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mDeleteDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->showDialog()V

    .line 175
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 6
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 88
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    move-object v1, v4

    check-cast v1, [Ljava/lang/Object;

    .line 89
    .local v1, "params":[Ljava/lang/Object;
    const/4 v4, 0x0

    aget-object v4, v1, v4

    check-cast v4, Landroid/content/Context;

    iput-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    .line 90
    const/4 v4, 0x1

    aget-object v4, v1, v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 91
    .local v0, "isEntireAlbumsSelected":Z
    const/4 v4, 0x2

    aget-object v4, v1, v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 92
    .local v3, "show":Z
    const/4 v4, 0x3

    aget-object v4, v1, v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mIsAirButton:Z

    .line 93
    if-nez v3, :cond_0

    .line 94
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->dismissdialog()V

    .line 108
    :goto_0
    return-void

    .line 97
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 98
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 99
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    .line 100
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 101
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->getNumberOfItemsToDelete()I

    move-result v4

    if-nez v4, :cond_1

    .line 102
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    const v5, 0x7f0e0113

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 106
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4, v0}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->getPopupText(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v2

    .line 107
    .local v2, "popupText":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4, v2}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->showDialog(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    .line 381
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/FileUtil;->cancelOperation()V

    .line 383
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    .line 384
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "EXIT_SELECTION_MODE"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 387
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 6
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 112
    move-object v1, p2

    check-cast v1, Lcom/sec/samsung/gallery/core/Event;

    .line 113
    .local v1, "ev":Lcom/sec/samsung/gallery/core/Event;
    new-instance v4, Lcom/sec/samsung/gallery/util/FileUtil;

    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/sec/samsung/gallery/util/FileUtil;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    .line 115
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v4

    sget v5, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELETE_CLOUD_MEDIA:I

    if-ne v4, v5, :cond_1

    .line 116
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/core/Event;->getData()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->handleDeleteMedias(Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v4

    sget v5, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELETE_MEDIA:I

    if-ne v4, v5, :cond_0

    .line 118
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 119
    .local v0, "currentState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-eqz v4, :cond_2

    .line 120
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v4

    const-string v5, "ACTION_DELETE_CONFIRM"

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;)V

    .line 121
    const/4 v4, 0x2

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    aput-object v5, v2, v4

    const/4 v4, 0x1

    sget-object v5, Lcom/sec/samsung/gallery/controller/SoundSceneCmd$SoundSceneCmdType;->STOP_PLAY_SOUND:Lcom/sec/samsung/gallery/controller/SoundSceneCmd$SoundSceneCmdType;

    aput-object v5, v2, v4

    .line 124
    .local v2, "params":[Ljava/lang/Object;
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v4

    const-string v5, "SOUND_SCENE"

    invoke-virtual {v4, v5, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 127
    .end local v2    # "params":[Ljava/lang/Object;
    :cond_2
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->checkMetaItemExist()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 128
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    const v5, 0x7f0e0238

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 129
    .local v3, "popupText":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4, v3}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->showCloudDialog(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 132
    .end local v3    # "popupText":Ljava/lang/String;
    :cond_3
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/core/Event;->getData()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->handleDeleteMedias(Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_0
.end method
