.class public interface abstract Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$ItemInfoAdapter;
.super Ljava/lang/Object;
.source "AppChoiceDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ItemInfoAdapter"
.end annotation


# virtual methods
.method public abstract getItemCount()I
.end method

.method public abstract getItemIcon(I)Landroid/graphics/drawable/Drawable;
.end method

.method public abstract getItemName(I)Ljava/lang/String;
.end method

.method public abstract getItemObject(I)Ljava/lang/Object;
.end method

.method public abstract getItemPackageName(I)Ljava/lang/String;
.end method
