.class public Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarView;
.source "AlbumActionBarForPick.java"


# instance fields
.field private mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

.field private mIsFocusInited:Z

.field private mIsHelpMode:Z


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 36
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mIsFocusInited:Z

    .line 37
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mIsHelpMode:Z

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick$1;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 70
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mIsHelpMode:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method private setTextOnlyButton(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 155
    const v1, 0x7f0f0288

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 156
    .local v0, "cancel":Landroid/view/MenuItem;
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->setTextOnlyButton(Landroid/view/MenuItem;)V

    .line 157
    return-void
.end method


# virtual methods
.method protected isDrawableHideMode()Z
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 160
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 161
    .local v1, "intent":Landroid/content/Intent;
    if-nez v1, :cond_1

    .line 178
    :cond_0
    :goto_0
    return v6

    .line 163
    :cond_1
    const-string v7, "album-pick"

    invoke-virtual {v1, v7, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 164
    .local v2, "isAlbumPick":Z
    const-string v7, "onlyMagic"

    invoke-virtual {v1, v7, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 165
    .local v4, "isMagicPick":Z
    const-string v7, "android.intent.action.PERSON_PICK"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 166
    .local v5, "isPersonPick":Z
    const-string v7, "caller_id_pick"

    invoke-virtual {v1, v7, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 168
    .local v3, "isContactPickerMode":Z
    const/4 v0, 0x0

    .line 169
    .local v0, "clusterType":I
    if-eqz v3, :cond_4

    .line 170
    const/16 v0, 0x4000

    .line 178
    :cond_2
    :goto_1
    if-nez v2, :cond_3

    if-nez v4, :cond_3

    if-nez v5, :cond_3

    if-eqz v0, :cond_0

    :cond_3
    const/4 v6, 0x1

    goto :goto_0

    .line 171
    :cond_4
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v7

    sget-object v8, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v7, v8, :cond_2

    .line 172
    const-string v7, "include-recommend"

    invoke-virtual {v1, v7, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 173
    const/high16 v0, 0x10000

    goto :goto_1

    .line 175
    :cond_5
    const v0, 0x8000

    goto :goto_1
.end method

.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x1

    .line 81
    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2, v3}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    .line 82
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mIsHelpMode:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    if-eqz v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->show(Z)V

    .line 85
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 86
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f12000c

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 88
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->setTextOnlyButton(Landroid/view/Menu;)V

    .line 89
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 128
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 152
    :goto_0
    return-void

    .line 130
    :sswitch_0
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_HIDDEN_ALBUMS:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 133
    :sswitch_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 134
    .local v0, "activity":Landroid/app/Activity;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setResult(I)V

    .line 135
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 138
    .end local v0    # "activity":Landroid/app/Activity;
    :sswitch_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "START_CAMERA"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 141
    :sswitch_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "SHOW_CONTENT_TO_DISPLAY_DIALOG"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 145
    :sswitch_4
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-class v3, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 146
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 149
    .end local v1    # "intent":Landroid/content/Intent;
    :sswitch_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->goToHelpGallery(Landroid/content/Context;)V

    goto :goto_0

    .line 128
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f0251 -> :sswitch_2
        0x7f0f0257 -> :sswitch_4
        0x7f0f0287 -> :sswitch_0
        0x7f0f0288 -> :sswitch_1
        0x7f0f0289 -> :sswitch_3
        0x7f0f028a -> :sswitch_5
    .end sparse-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mIsHelpMode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->hide()V

    .line 97
    :cond_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->onPause()V

    .line 98
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 6
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v5, 0x0

    .line 102
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3, v5}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 103
    const v3, 0x7f0f0286

    const/4 v4, 0x1

    invoke-interface {p1, v3, v4}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 105
    const v3, 0x7f0f0287

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkShowHiddenMenu(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v4

    invoke-static {p1, v3, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 106
    const v3, 0x7f0f0288

    invoke-static {p1, v3, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 108
    const v3, 0x7f0f0257

    invoke-static {p1, v3, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 109
    const v3, 0x7f0f028a

    invoke-static {p1, v3, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 110
    const v3, 0x7f0f0289

    invoke-static {p1, v3, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 112
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mIsFocusInited:Z

    if-nez v3, :cond_0

    .line 113
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 114
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    .line 115
    .local v1, "actionView":Landroid/view/View;
    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 116
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mIsFocusInited:Z

    .line 119
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    .end local v1    # "actionView":Landroid/view/View;
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v2

    .line 120
    .local v2, "currState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v3, v2, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-eqz v3, :cond_1

    check-cast v2, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .end local v2    # "currState":Lcom/sec/android/gallery3d/app/ActivityState;
    iget-object v3, v2, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getCount()I

    move-result v3

    if-eqz v3, :cond_1

    .line 121
    const v3, 0x7f0f0251

    invoke-static {p1, v3, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 124
    :cond_1
    return-void

    .line 114
    .restart local v0    # "actionBar":Landroid/app/ActionBar;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected setupButtons()V
    .locals 1

    .prologue
    .line 74
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->showHomeButtonOnTopLeft()V

    .line 77
    :cond_0
    return-void
.end method
