.class public Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;
.super Lcom/sec/samsung/gallery/drawer/DrawerItem;
.source "DrawerItemSLink.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mIconDrawable:Landroid/graphics/drawable/BitmapDrawable;

.field private mNetworkMode:Ljava/lang/String;

.field private mStorageId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(ILjava/lang/String;Ljava/lang/String;ILcom/sec/samsung/gallery/core/TabTagType;Ljava/lang/String;)V
    .locals 1
    .param p1, "groupid"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "mediaSetPath"    # Ljava/lang/String;
    .param p4, "storageId"    # I
    .param p5, "tagType"    # Lcom/sec/samsung/gallery/core/TabTagType;
    .param p6, "networkMode"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p5}, Lcom/sec/samsung/gallery/drawer/DrawerItem;-><init>(ILjava/lang/CharSequence;Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->mNetworkMode:Ljava/lang/String;

    .line 45
    iput-object p3, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->mMediaSetPath:Ljava/lang/String;

    .line 46
    iput p4, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->mStorageId:I

    .line 47
    iput-object p6, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->mNetworkMode:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public static createSamsungLinkItems(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/sec/samsung/gallery/drawer/DrawerItem;
    .locals 7
    .param p0, "groupId"    # I
    .param p1, "deviceName"    # Ljava/lang/String;
    .param p2, "storagePath"    # Ljava/lang/String;
    .param p3, "storageId"    # I
    .param p4, "networkMode"    # Ljava/lang/String;

    .prologue
    .line 52
    new-instance v0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;

    sget-object v5, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_OTHER_DEVICE:Lcom/sec/samsung/gallery/core/TabTagType;

    move v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/sec/samsung/gallery/core/TabTagType;Ljava/lang/String;)V

    return-object v0
.end method

.method private isSLinkDevice(Ljava/lang/String;)Z
    .locals 2
    .param p1, "mediaSetPath"    # Ljava/lang/String;

    .prologue
    .line 148
    if-nez p1, :cond_0

    .line 149
    sget-object v0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->TAG:Ljava/lang/String;

    const-string v1, "isSLinkDevice : mediaSetPath is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    const/4 v0, 0x0

    .line 152
    :goto_0
    return v0

    :cond_0
    const-string/jumbo v0, "slink"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public getMediaSetPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->mMediaSetPath:Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkMode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->mNetworkMode:Ljava/lang/String;

    return-object v0
.end method

.method public selectItem(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/app/StateManager;IZ)V
    .locals 8
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "statusProxy"    # Lcom/sec/android/gallery3d/app/StateManager;
    .param p3, "index"    # I
    .param p4, "isPickMode"    # Z

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 112
    const v4, 0x7f030025

    invoke-static {p1, v4, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 113
    .local v2, "chargeDailogLayout":Landroid/widget/LinearLayout;
    const v4, 0x7f0f001d

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    .line 114
    .local v3, "checkBox":Landroid/widget/CheckBox;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "allow_mobile_network"

    invoke-static {v4, v5, v6}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    .line 116
    .local v0, "bAllowMobileNet":Z
    invoke-virtual {v3, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 117
    sget-object v4, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sLinkSelected : mediaSetPath = [ "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->mMediaSetPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    iget-object v4, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->mMediaSetPath:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->isSLinkDevice(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    if-nez v0, :cond_0

    .line 120
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f0e02ff

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0e00db

    new-instance v6, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink$1;

    invoke-direct {v6, p0, v3, p1}, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink$1;-><init>(Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;Landroid/widget/CheckBox;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const/high16 v5, 0x1040000

    invoke-virtual {v4, v5, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 145
    :goto_0
    return-void

    .line 141
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 142
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v4, "KEY_MEDIA_SET_PATH"

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->getMediaSetPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    const-class v5, Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-virtual {v4, v5, v1}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public setIcon(Landroid/widget/ImageView;)V
    .locals 10
    .param p1, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 62
    if-nez p1, :cond_0

    .line 85
    :goto_0
    return-void

    .line 67
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->mIconDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-nez v1, :cond_2

    .line 69
    :try_start_0
    sget-object v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;->LIGHT:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;

    .line 70
    .local v5, "iconTheme":Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v1, :cond_1

    .line 71
    sget-object v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;->DARK:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;

    .line 73
    :cond_1
    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->mStorageId:I

    int-to-long v2, v2

    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;->LARGE:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;

    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v8, 0x0

    const v9, 0x101009e

    aput v9, v6, v8

    invoke-static/range {v1 .. v6}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;->getDeviceIcon(Landroid/content/Context;JLcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;[I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 77
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->mIconDrawable:Landroid/graphics/drawable/BitmapDrawable;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v5    # "iconTheme":Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->mIconDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 78
    :catch_0
    move-exception v7

    .line 79
    .local v7, "e":Ljava/io/FileNotFoundException;
    sget-object v1, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Trouble getting device icon for device "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->mStorageId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setNetworkMode(Landroid/widget/ImageView;)V
    .locals 3
    .param p1, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 89
    const/4 v0, -0x1

    .line 90
    .local v0, "imgRsr":I
    if-eqz p1, :cond_0

    .line 91
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->mNetworkMode:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 92
    const-string v1, "WIFI"

    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->mNetworkMode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 93
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v1, :cond_1

    .line 94
    const v0, 0x7f0200b8

    .line 101
    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 104
    :cond_0
    return-void

    .line 96
    :cond_1
    const v0, 0x7f0200b6

    goto :goto_0

    .line 99
    :cond_2
    const v0, 0x7f0200ac

    goto :goto_0
.end method
