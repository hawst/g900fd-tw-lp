.class public Lcom/sec/samsung/gallery/controller/ShowSlideshowMusicListCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "ShowSlideshowMusicListCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDialog:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method private dismissDialog()V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowSlideshowMusicListCmd;->mDialog:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowSlideshowMusicListCmd;->mDialog:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->dismissDialog()V

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowSlideshowMusicListCmd;->mDialog:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    .line 56
    :cond_0
    return-void
.end method

.method private showDialog(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ringtoneUri"    # Landroid/net/Uri;
    .param p3, "extraUri"    # Landroid/net/Uri;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowSlideshowMusicListCmd;->dismissDialog()V

    .line 47
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    invoke-direct {v0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;-><init>(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowSlideshowMusicListCmd;->mDialog:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    .line 48
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowSlideshowMusicListCmd;->mDialog:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->showDialog()V

    .line 49
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 7
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const/4 v6, 0x3

    .line 20
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/Object;

    move-object v2, v5

    check-cast v2, [Ljava/lang/Object;

    .line 21
    .local v2, "params":[Ljava/lang/Object;
    if-nez v2, :cond_1

    .line 43
    :cond_0
    :goto_0
    return-void

    .line 25
    :cond_1
    const/4 v5, 0x0

    aget-object v5, v2, v5

    check-cast v5, Landroid/content/Context;

    iput-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowSlideshowMusicListCmd;->mContext:Landroid/content/Context;

    .line 26
    const/4 v5, 0x1

    aget-object v5, v2, v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 27
    .local v4, "show":Z
    const/4 v5, 0x2

    aget-object v5, v2, v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 28
    .local v3, "ringtoneUri":Landroid/net/Uri;
    aget-object v5, v2, v6

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v0, 0x0

    .line 29
    .local v0, "extraUri":Landroid/net/Uri;
    :goto_1
    const/4 v5, 0x4

    aget-object v5, v2, v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 31
    .local v1, "isStopCurrentMusic":Z
    if-eqz v1, :cond_3

    .line 32
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowSlideshowMusicListCmd;->mDialog:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    if-eqz v5, :cond_0

    .line 33
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowSlideshowMusicListCmd;->mDialog:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->stopMusic()V

    goto :goto_0

    .line 28
    .end local v0    # "extraUri":Landroid/net/Uri;
    .end local v1    # "isStopCurrentMusic":Z
    :cond_2
    aget-object v5, v2, v6

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    .line 36
    .restart local v0    # "extraUri":Landroid/net/Uri;
    .restart local v1    # "isStopCurrentMusic":Z
    :cond_3
    if-eqz v4, :cond_4

    .line 37
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowSlideshowMusicListCmd;->mContext:Landroid/content/Context;

    invoke-direct {p0, v5, v3, v0}, Lcom/sec/samsung/gallery/controller/ShowSlideshowMusicListCmd;->showDialog(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;)V

    goto :goto_0

    .line 39
    :cond_4
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowSlideshowMusicListCmd;->dismissDialog()V

    goto :goto_0
.end method
