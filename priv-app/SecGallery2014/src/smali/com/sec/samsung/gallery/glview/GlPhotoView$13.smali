.class Lcom/sec/samsung/gallery/glview/GlPhotoView$13;
.super Ljava/lang/Object;
.source "GlPhotoView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mThumbDragLeavedGallery:Z

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 1

    .prologue
    .line 1941
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1942
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->mThumbDragLeavedGallery:Z

    return-void
.end method

.method private startDragAndDrop()V
    .locals 5

    .prologue
    .line 1973
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlPos;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glcore/GlPos;-><init>()V

    .line 1974
    .local v0, "pos":Lcom/sec/android/gallery3d/glcore/GlPos;
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlPos2D;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/glcore/GlPos2D;-><init>()V

    .line 1975
    .local v1, "viewPos":Lcom/sec/android/gallery3d/glcore/GlPos2D;
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getY()F

    move-result v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getZ()F

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlPos;->set(FFF)V

    .line 1976
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getGlConfig()Lcom/sec/android/gallery3d/glcore/GlConfig;

    move-result-object v2

    sget v3, Lcom/sec/android/gallery3d/glcore/GlConfig;->CRIT_LEFT_TOP:I

    invoke-virtual {v2, v1, v0, v3}, Lcom/sec/android/gallery3d/glcore/GlConfig;->getViewPos(Lcom/sec/android/gallery3d/glcore/GlPos2D;Lcom/sec/android/gallery3d/glcore/GlPos;I)V

    .line 1983
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->mThumbDragLeavedGallery:Z

    .line 1985
    return-void
.end method


# virtual methods
.method public onMove(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 2
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "dx"    # I
    .param p3, "dy"    # I

    .prologue
    .line 1949
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-eqz v0, :cond_3

    .line 1950
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setMultiWindow(Landroid/content/Context;)V

    .line 1951
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDragNDropInMultiwindow:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isConnetedSideSync(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCallWindow(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->mThumbDragLeavedGallery:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mStartDragListener:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    if-eqz v0, :cond_2

    .line 1954
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->startDragAndDrop()V

    .line 1956
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    const/4 v1, 0x1

    invoke-virtual {v0, p2, p3, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->onDrag(IIZ)Z

    move-result v0

    .line 1958
    :goto_0
    return v0

    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v0, p2, p3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->onMoved(II)Z

    move-result v0

    goto :goto_0
.end method

.method public onPress(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 2
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 1945
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWidth:I

    invoke-virtual {v0, v1, p3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->onPressed(II)Z

    move-result v0

    return v0
.end method

.method public onRelease(Lcom/sec/android/gallery3d/glcore/GlObject;IIII)Z
    .locals 4
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "dx"    # I
    .param p3, "dy"    # I
    .param p4, "speedX"    # I
    .param p5, "speedY"    # I

    .prologue
    const/4 v2, 0x0

    .line 1963
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-eqz v1, :cond_1

    .line 1964
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v1, p2, p3, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->onDrag(IIZ)Z

    move-result v0

    .line 1965
    .local v0, "ret":Z
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->mThumbDragLeavedGallery:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->stopDragAnim(Z)V

    .line 1966
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->mThumbDragLeavedGallery:Z

    .line 1969
    .end local v0    # "ret":Z
    :goto_1
    return v0

    .restart local v0    # "ret":Z
    :cond_0
    move v1, v2

    .line 1965
    goto :goto_0

    .line 1969
    .end local v0    # "ret":Z
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$13;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v1, p2, p3, p4, p5}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->onReleased(IIII)Z

    move-result v0

    goto :goto_1
.end method
