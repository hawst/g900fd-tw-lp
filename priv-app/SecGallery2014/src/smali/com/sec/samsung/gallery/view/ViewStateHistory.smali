.class public Lcom/sec/samsung/gallery/view/ViewStateHistory;
.super Ljava/lang/Object;
.source "ViewStateHistory.java"


# static fields
.field private static final DEFAULT_VIEW_STATE:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;"
        }
    .end annotation
.end field

.field private static final KEY_CLASS:Ljava/lang/String; = "class"

.field private static final KEY_PREFERENCE:Ljava/lang/String; = "lastest_view_state"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/sec/samsung/gallery/view/ViewStateHistory;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/ViewStateHistory;->TAG:Ljava/lang/String;

    .line 23
    const-class v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    sput-object v0, Lcom/sec/samsung/gallery/view/ViewStateHistory;->DEFAULT_VIEW_STATE:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/ViewStateHistory;->mContext:Landroid/content/Context;

    .line 27
    return-void
.end method

.method private toViewState(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p1, "viewStateName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    const-class v2, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    .line 44
    .local v1, "timeViewState":Ljava/lang/String;
    const-class v2, Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "allViewState":Ljava/lang/String;
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 47
    const-class v2, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .line 52
    :goto_0
    return-object v2

    .line 48
    :cond_0
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 49
    const-class v2, Lcom/sec/samsung/gallery/view/allview/AllViewState;

    goto :goto_0

    .line 52
    :cond_1
    sget-object v2, Lcom/sec/samsung/gallery/view/ViewStateHistory;->DEFAULT_VIEW_STATE:Ljava/lang/Class;

    goto :goto_0
.end method


# virtual methods
.method public saveLatestViewState()V
    .locals 6

    .prologue
    .line 30
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/ViewStateHistory;->mContext:Landroid/content/Context;

    const-string v4, "lastest_view_state"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 31
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 33
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/ViewStateHistory;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v2

    .line 34
    .local v2, "vs":Lcom/sec/android/gallery3d/app/ActivityState;
    if-eqz v2, :cond_0

    .line 35
    const-string v3, "class"

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 39
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 40
    return-void

    .line 37
    :cond_0
    sget-object v3, Lcom/sec/samsung/gallery/view/ViewStateHistory;->TAG:Ljava/lang/String;

    const-string v4, "TopViewState is empty."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
