.class public Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;
.super Lcom/sec/samsung/gallery/glview/GlBaseObject;
.source "GlPhotoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GlBgObject"
.end annotation


# instance fields
.field private mImgView:Lcom/sec/android/gallery3d/glcore/GlImageView;

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 3
    .param p2, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 3568
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .line 3569
    sget v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_CANVAS_MIDDLE:I

    invoke-direct {p0, p2, v2, v2, v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFI)V

    .line 3571
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v1, :cond_0

    const v0, 0x7f02020b

    .line 3572
    .local v0, "leftSplitBgId":I
    :goto_0
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v2, p1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->mImgView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 3573
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->mImgView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 3574
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->mImgView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 3575
    return-void

    .line 3571
    .end local v0    # "leftSplitBgId":I
    :cond_0
    const v0, 0x7f020267

    goto :goto_0
.end method
