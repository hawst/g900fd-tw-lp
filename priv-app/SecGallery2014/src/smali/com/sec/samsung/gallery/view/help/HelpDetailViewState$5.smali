.class Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$5;
.super Ljava/lang/Object;
.source "HelpDetailViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)V
    .locals 0

    .prologue
    .line 393
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$5;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongTap(I)V
    .locals 0
    .param p1, "slotIndex"    # I

    .prologue
    .line 405
    return-void
.end method

.method public onSelectionChanged(Lcom/sec/android/gallery3d/data/MediaItem;ZI)V
    .locals 0
    .param p1, "media"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "isSelected"    # Z
    .param p3, "slotIndex"    # I

    .prologue
    .line 409
    return-void
.end method

.method public onSelectionModeChanged(Z)V
    .locals 0
    .param p1, "isSelectionMode"    # Z

    .prologue
    .line 413
    return-void
.end method

.method public onSlotSelected(I)Z
    .locals 2
    .param p1, "slotIndex"    # I

    .prologue
    .line 396
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$5;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHelpMode:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$2700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 397
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$5;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$2800(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    const v1, 0x7f0e02da

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 398
    const/4 v0, 0x0

    .line 400
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$5;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->jumpTo(I)Z

    move-result v0

    goto :goto_0
.end method
