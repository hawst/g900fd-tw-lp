.class public Lcom/sec/samsung/gallery/glview/GlConverseAnimation;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlConverseAnimation.java"


# instance fields
.field private mAlphaInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

.field private mFromAlpha:F

.field mFromObj:Lcom/sec/android/gallery3d/glcore/GlObject;

.field private mFromPitch:F

.field private mFromScaleX:F

.field private mFromScaleY:F

.field private mFromX:F

.field private mFromY:F

.field private mFromYaw:F

.field private mFromZ:F

.field private mPosInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

.field private mScaleInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

.field private mToAlpha:F

.field private mToPitch:F

.field private mToScaleX:F

.field private mToScaleY:F

.field private mToX:F

.field private mToY:F

.field private mToYaw:F

.field private mToZ:F


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 0
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 31
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->setTransition(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlObject;Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 0
    .param p1, "fromObj"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "toObj"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 26
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->setTransition(Lcom/sec/android/gallery3d/glcore/GlObject;Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 27
    return-void
.end method

.method private setTransition(Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 1
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 86
    if-nez p1, :cond_0

    .line 111
    :goto_0
    return-void

    .line 90
    :cond_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 91
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromX:F

    .line 92
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromY:F

    .line 93
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getZ()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromZ:F

    .line 95
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleX:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromScaleX:F

    .line 96
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleY:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromScaleY:F

    .line 97
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlpha:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromAlpha:F

    .line 98
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mYaw:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromYaw:F

    .line 99
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mPitch:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromPitch:F

    .line 101
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromX:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToX:F

    .line 102
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromY:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToY:F

    .line 103
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromZ:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToZ:F

    .line 105
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromScaleX:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToScaleX:F

    .line 106
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromScaleY:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToScaleY:F

    .line 107
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromAlpha:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToAlpha:F

    .line 108
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromYaw:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToYaw:F

    .line 109
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromPitch:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToPitch:F

    .line 110
    invoke-virtual {p1, p0}, Lcom/sec/android/gallery3d/glcore/GlObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    goto :goto_0
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 7
    .param p1, "ratio"    # F

    .prologue
    .line 177
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToX:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromX:F

    cmpl-float v4, v4, v5

    if-nez v4, :cond_0

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToY:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromY:F

    cmpl-float v4, v4, v5

    if-nez v4, :cond_0

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToZ:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromZ:F

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_1

    .line 178
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mPosInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    if-eqz v4, :cond_7

    .line 179
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mPosInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    invoke-virtual {v4, p1}, Lcom/sec/android/gallery3d/glcore/GlInterpolator;->getInterpolation(F)F

    move-result v3

    .line 182
    .local v3, "inRatio":F
    :goto_0
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromX:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToX:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromX:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, v3

    add-float v0, v4, v5

    .line 183
    .local v0, "dx":F
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromY:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToY:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromY:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, v3

    add-float v1, v4, v5

    .line 184
    .local v1, "dy":F
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromZ:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToZ:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromZ:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, v3

    add-float v2, v4, v5

    .line 185
    .local v2, "dz":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v4, v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPos(FFF)V

    .line 187
    .end local v0    # "dx":F
    .end local v1    # "dy":F
    .end local v2    # "dz":F
    .end local v3    # "inRatio":F
    :cond_1
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromScaleX:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToScaleX:F

    invoke-static {v4, v5}, Ljava/lang/Float;->compare(FF)I

    move-result v4

    if-nez v4, :cond_2

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromScaleY:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToScaleY:F

    invoke-static {v4, v5}, Ljava/lang/Float;->compare(FF)I

    move-result v4

    if-eqz v4, :cond_3

    .line 188
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mScaleInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    if-eqz v4, :cond_8

    .line 189
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mScaleInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    invoke-virtual {v4, p1}, Lcom/sec/android/gallery3d/glcore/GlInterpolator;->getInterpolation(F)F

    move-result v3

    .line 192
    .restart local v3    # "inRatio":F
    :goto_1
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromScaleX:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToScaleX:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromScaleX:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, v3

    add-float v0, v4, v5

    .line 193
    .restart local v0    # "dx":F
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromScaleY:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToScaleY:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromScaleY:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, v3

    add-float v1, v4, v5

    .line 194
    .restart local v1    # "dy":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->setScale(FF)V

    .line 196
    .end local v0    # "dx":F
    .end local v1    # "dy":F
    .end local v3    # "inRatio":F
    :cond_3
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromAlpha:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToAlpha:F

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_4

    .line 197
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mAlphaInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    if-eqz v4, :cond_9

    .line 198
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mAlphaInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    invoke-virtual {v4, p1}, Lcom/sec/android/gallery3d/glcore/GlInterpolator;->getInterpolation(F)F

    move-result v3

    .line 201
    .restart local v3    # "inRatio":F
    :goto_2
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromAlpha:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToAlpha:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromAlpha:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, v3

    add-float v0, v4, v5

    .line 202
    .restart local v0    # "dx":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v4, v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->setAlpha(F)V

    .line 204
    .end local v0    # "dx":F
    .end local v3    # "inRatio":F
    :cond_4
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromYaw:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToYaw:F

    invoke-static {v4, v5}, Ljava/lang/Float;->compare(FF)I

    move-result v4

    if-eqz v4, :cond_5

    .line 205
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromYaw:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToYaw:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromYaw:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, p1

    add-float v0, v4, v5

    .line 206
    .restart local v0    # "dx":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v4, v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->setYaw(F)V

    .line 208
    .end local v0    # "dx":F
    :cond_5
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromPitch:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToPitch:F

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_6

    .line 209
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromPitch:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToPitch:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromPitch:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, p1

    add-float v0, v4, v5

    .line 210
    .restart local v0    # "dx":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v4, v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPitch(F)V

    .line 212
    .end local v0    # "dx":F
    :cond_6
    return-void

    .line 181
    :cond_7
    move v3, p1

    .restart local v3    # "inRatio":F
    goto/16 :goto_0

    .line 191
    .end local v3    # "inRatio":F
    :cond_8
    move v3, p1

    .restart local v3    # "inRatio":F
    goto :goto_1

    .line 200
    .end local v3    # "inRatio":F
    :cond_9
    move v3, p1

    .restart local v3    # "inRatio":F
    goto :goto_2
.end method

.method public findShortestRotationPath()V
    .locals 4

    .prologue
    const/high16 v3, 0x43b40000    # 360.0f

    .line 215
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToYaw:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromYaw:F

    sub-float v0, v1, v2

    .line 216
    .local v0, "del":F
    :goto_0
    const/high16 v1, -0x3ccc0000    # -180.0f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    add-float/2addr v0, v3

    goto :goto_0

    .line 217
    :cond_0
    :goto_1
    const/high16 v1, 0x43340000    # 180.0f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    sub-float/2addr v0, v3

    goto :goto_1

    .line 218
    :cond_1
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromYaw:F

    add-float/2addr v1, v0

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToYaw:F

    .line 219
    return-void
.end method

.method protected onCancel()V
    .locals 0

    .prologue
    .line 222
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->onStop()V

    .line 223
    return-void
.end method

.method protected onStop()V
    .locals 4

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToX:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToY:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToZ:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPos(FFF)V

    .line 228
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToScaleX:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToScaleY:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setScale(FF)V

    .line 229
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToAlpha:F

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->setAlpha(F)V

    .line 230
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToYaw:F

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->setYaw(F)V

    .line 231
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToPitch:F

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPitch(F)V

    .line 233
    :cond_0
    return-void
.end method

.method public setAlphaInterpolator(Lcom/sec/android/gallery3d/glcore/GlInterpolator;)V
    .locals 0
    .param p1, "interpolator"    # Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mAlphaInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    .line 169
    return-void
.end method

.method public setAnimAlpha(FZ)V
    .locals 0
    .param p1, "alpha"    # F
    .param p2, "isTo"    # Z

    .prologue
    .line 136
    if-eqz p2, :cond_0

    .line 137
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToAlpha:F

    .line 141
    :goto_0
    return-void

    .line 139
    :cond_0
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromAlpha:F

    goto :goto_0
.end method

.method public setAnimPitch(FZ)V
    .locals 0
    .param p1, "pitch"    # F
    .param p2, "isTo"    # Z

    .prologue
    .line 152
    if-eqz p2, :cond_0

    .line 153
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToPitch:F

    .line 157
    :goto_0
    return-void

    .line 155
    :cond_0
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromPitch:F

    goto :goto_0
.end method

.method public setAnimPos(FFFZ)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F
    .param p4, "isTo"    # Z

    .prologue
    .line 114
    if-eqz p4, :cond_0

    .line 115
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToX:F

    .line 116
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToY:F

    .line 117
    iput p3, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToZ:F

    .line 123
    :goto_0
    return-void

    .line 119
    :cond_0
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromX:F

    .line 120
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromY:F

    .line 121
    iput p3, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromZ:F

    goto :goto_0
.end method

.method public setAnimScale(FFZ)V
    .locals 0
    .param p1, "xScale"    # F
    .param p2, "yScale"    # F
    .param p3, "isTo"    # Z

    .prologue
    .line 126
    if-eqz p3, :cond_0

    .line 127
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToScaleX:F

    .line 128
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToScaleY:F

    .line 133
    :goto_0
    return-void

    .line 130
    :cond_0
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromScaleX:F

    .line 131
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromScaleY:F

    goto :goto_0
.end method

.method public setAnimYaw(FZ)V
    .locals 0
    .param p1, "yaw"    # F
    .param p2, "isTo"    # Z

    .prologue
    .line 144
    if-eqz p2, :cond_0

    .line 145
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToYaw:F

    .line 149
    :goto_0
    return-void

    .line 147
    :cond_0
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromYaw:F

    goto :goto_0
.end method

.method public setPosInterpolator(Lcom/sec/android/gallery3d/glcore/GlInterpolator;)V
    .locals 0
    .param p1, "interpolator"    # Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mPosInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    .line 161
    return-void
.end method

.method public setRefereceObject(Lcom/sec/android/gallery3d/glcore/GlObject;Z)V
    .locals 4
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "isToObject"    # Z

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 35
    if-eqz p2, :cond_2

    .line 36
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToX:F

    .line 37
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToY:F

    .line 38
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getZ()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToZ:F

    .line 40
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleX:F

    iget v2, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mWidth:F

    mul-float/2addr v2, v0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mWidth:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mWidth:F

    :goto_0
    div-float v0, v2, v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToScaleX:F

    .line 41
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleY:F

    iget v2, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mHeight:F

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v2, v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mHeight:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mHeight:F

    :cond_0
    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToScaleY:F

    .line 42
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlpha:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToAlpha:F

    .line 43
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mYaw:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToYaw:F

    .line 44
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mPitch:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToPitch:F

    .line 56
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 40
    goto :goto_0

    .line 46
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromX:F

    .line 47
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromY:F

    .line 48
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getZ()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromZ:F

    .line 50
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleX:F

    iget v2, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mWidth:F

    mul-float/2addr v2, v0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mWidth:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mWidth:F

    :goto_2
    div-float v0, v2, v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromScaleX:F

    .line 51
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleY:F

    iget v2, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mHeight:F

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v2, v2, Lcom/sec/android/gallery3d/glcore/GlObject;->mHeight:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mHeight:F

    :cond_3
    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromScaleY:F

    .line 52
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlpha:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromAlpha:F

    .line 53
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mYaw:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromYaw:F

    .line 54
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mPitch:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromPitch:F

    goto :goto_1

    :cond_4
    move v0, v1

    .line 50
    goto :goto_2
.end method

.method public setScaleInterpolator(Lcom/sec/android/gallery3d/glcore/GlInterpolator;)V
    .locals 0
    .param p1, "interpolator"    # Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mScaleInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    .line 165
    return-void
.end method

.method public setTransition(Lcom/sec/android/gallery3d/glcore/GlObject;Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 4
    .param p1, "fromObj"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "toObj"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 59
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 64
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromX:F

    .line 65
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromY:F

    .line 66
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getZ()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromZ:F

    .line 68
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleX:F

    iget v2, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mWidth:F

    mul-float/2addr v2, v0

    iget v0, p2, Lcom/sec/android/gallery3d/glcore/GlObject;->mWidth:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_3

    iget v0, p2, Lcom/sec/android/gallery3d/glcore/GlObject;->mWidth:F

    :goto_1
    div-float v0, v2, v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromScaleX:F

    .line 69
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleY:F

    iget v2, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mHeight:F

    mul-float/2addr v0, v2

    iget v2, p2, Lcom/sec/android/gallery3d/glcore/GlObject;->mHeight:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_2

    iget v1, p2, Lcom/sec/android/gallery3d/glcore/GlObject;->mHeight:F

    :cond_2
    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromScaleY:F

    .line 70
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlpha:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromAlpha:F

    .line 71
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mYaw:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromYaw:F

    .line 72
    iget v0, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mPitch:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mFromPitch:F

    .line 74
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/glcore/GlObject;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToX:F

    .line 75
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/glcore/GlObject;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToY:F

    .line 76
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/glcore/GlObject;->getZ()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToZ:F

    .line 78
    iget v0, p2, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleX:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToScaleX:F

    .line 79
    iget v0, p2, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleY:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToScaleY:F

    .line 80
    iget v0, p2, Lcom/sec/android/gallery3d/glcore/GlObject;->mAlpha:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToAlpha:F

    .line 81
    iget v0, p2, Lcom/sec/android/gallery3d/glcore/GlObject;->mYaw:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToYaw:F

    .line 82
    iget v0, p2, Lcom/sec/android/gallery3d/glcore/GlObject;->mPitch:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->mToPitch:F

    goto :goto_0

    :cond_3
    move v0, v1

    .line 68
    goto :goto_1
.end method
