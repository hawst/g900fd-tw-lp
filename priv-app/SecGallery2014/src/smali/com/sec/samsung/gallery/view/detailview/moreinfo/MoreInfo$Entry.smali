.class public Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;
.super Ljava/lang/Object;
.source "MoreInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Entry"
.end annotation


# instance fields
.field public icon:I

.field public index:I

.field public layout:I

.field public title:I

.field public type:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 0
    .param p1, "index"    # I
    .param p2, "layout"    # I
    .param p3, "icon"    # I
    .param p4, "title"    # I

    .prologue
    .line 480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 481
    iput p4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;->index:I

    .line 482
    iput p2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;->layout:I

    .line 483
    iput p3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;->icon:I

    .line 484
    iput p4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;->title:I

    .line 485
    return-void
.end method

.method public constructor <init>(IIIII)V
    .locals 0
    .param p1, "index"    # I
    .param p2, "layout"    # I
    .param p3, "icon"    # I
    .param p4, "title"    # I
    .param p5, "type"    # I

    .prologue
    .line 488
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;-><init>(IIII)V

    .line 489
    iput p5, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;->type:I

    .line 490
    return-void
.end method
