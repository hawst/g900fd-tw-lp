.class public Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;
.super Ljava/lang/Object;
.source "ProgressDialogHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$onCancelListener;
    }
.end annotation


# static fields
.field private static mHandler:Landroid/os/Handler;


# instance fields
.field mClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mListener:Landroid/content/DialogInterface$OnCancelListener;

.field private mMaxCount:I

.field private mOnCancelListener:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$onCancelListener;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mProgressStyle:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressStyle:I

    .line 27
    iput v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mMaxCount:I

    .line 125
    new-instance v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$3;-><init>(Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 134
    new-instance v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$4;-><init>(Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 33
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "progressStyle"    # I
    .param p2, "maxCount"    # I

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressStyle:I

    .line 27
    iput v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mMaxCount:I

    .line 125
    new-instance v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$3;-><init>(Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 134
    new-instance v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$4;-><init>(Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 36
    iput p1, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressStyle:I

    .line 37
    iput p2, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mMaxCount:I

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    .prologue
    .line 17
    iget v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressStyle:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    .prologue
    .line 17
    iget v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mMaxCount:I

    return v0
.end method

.method static synthetic access$302(Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;
    .param p1, "x1"    # I

    .prologue
    .line 17
    iput p1, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mMaxCount:I

    return p1
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;)Landroid/content/DialogInterface$OnCancelListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mListener:Landroid/content/DialogInterface$OnCancelListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;)Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$onCancelListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mOnCancelListener:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$onCancelListener;

    return-object v0
.end method


# virtual methods
.method public closeProgressDialog()V
    .locals 1

    .prologue
    .line 81
    const/16 v0, 0x3e8

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->closeProgressDialog(I)V

    .line 82
    return-void
.end method

.method public closeProgressDialog(I)V
    .locals 4
    .param p1, "delayedTime"    # I

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    sget-object v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$1;-><init>(Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;)V

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 100
    :cond_0
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOncancelListener(Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$onCancelListener;)V
    .locals 0
    .param p1, "onCancelListener"    # Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$onCancelListener;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mOnCancelListener:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$onCancelListener;

    .line 161
    return-void
.end method

.method public showProgressDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "enableCancel"    # Z
    .param p5, "cancelListener"    # Landroid/content/DialogInterface$OnCancelListener;

    .prologue
    .line 42
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->showProgressDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 43
    return-void
.end method

.method public showProgressDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLandroid/content/DialogInterface$OnCancelListener;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "enableCancel"    # Z
    .param p5, "touchOutside"    # Z
    .param p6, "cancelListener"    # Landroid/content/DialogInterface$OnCancelListener;

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->closeProgressDialog()V

    .line 49
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mContext:Landroid/content/Context;

    .line 51
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 52
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    iget v1, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressStyle:I

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 54
    if-eqz p2, :cond_0

    .line 55
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p2}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 56
    :cond_0
    if-eqz p3, :cond_1

    .line 57
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 59
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressStyle:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 60
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    iget v1, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mMaxCount:I

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 62
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 63
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p5}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 64
    if-eqz p4, :cond_3

    .line 65
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, -0x2

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mContext:Landroid/content/Context;

    const v3, 0x7f0e0046

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mClickListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 67
    iput-object p6, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 70
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p6}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 72
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-static {v0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->hideStatusIcon(Ljava/lang/Object;)V

    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 74
    return-void
.end method

.method public updateProgress(II)V
    .locals 2
    .param p1, "progress"    # I
    .param p2, "total"    # I

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 104
    sget-object v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper$2;-><init>(Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 123
    :cond_0
    return-void
.end method
