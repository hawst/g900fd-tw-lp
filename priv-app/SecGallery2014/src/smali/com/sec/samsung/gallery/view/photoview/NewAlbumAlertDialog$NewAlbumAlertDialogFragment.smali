.class Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;
.super Landroid/app/DialogFragment;
.source "NewAlbumAlertDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NewAlbumAlertDialogFragment"
.end annotation


# static fields
.field protected static final TAG:Ljava/lang/String; = "NewAlbumAlertDialogFragment"


# instance fields
.field private MAX_FOLDER_NAME_LENGTH:I

.field mAlertDialog:Landroid/app/AlertDialog;

.field private mBucketId:I

.field private mClipVisible:Z

.field private final mCtx:Landroid/content/Context;

.field private mIsNewEvent:Z

.field private mItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private mString:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;Landroid/content/Context;ZLjava/util/ArrayList;ILjava/lang/String;)V
    .locals 2
    .param p2, "ctx"    # Landroid/content/Context;
    .param p3, "isNewEvent"    # Z
    .param p5, "bucketId"    # I
    .param p6, "albumName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Z",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 112
    .local p4, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 100
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    .line 102
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mClipVisible:Z

    .line 107
    const/16 v1, 0x32

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->MAX_FOLDER_NAME_LENGTH:I

    .line 113
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mCtx:Landroid/content/Context;

    .line 114
    iput-boolean p3, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mIsNewEvent:Z

    .line 115
    iput-object p4, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mItemList:Ljava/util/ArrayList;

    .line 116
    iput p5, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mBucketId:I

    .line 117
    iput-object p6, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mString:Ljava/lang/String;

    .line 118
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 119
    .local v0, "args":Landroid/os/Bundle;
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 120
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mIsNewEvent:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mItemList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    .prologue
    .line 91
    iget v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mBucketId:I

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mClipVisible:Z

    return v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    .prologue
    .line 91
    iget v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->MAX_FOLDER_NAME_LENGTH:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mCtx:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mString:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 358
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 360
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 361
    .local v0, "newDialog":Landroid/app/Dialog;
    if-eqz v0, :cond_0

    .line 362
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 363
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 424
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 428
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 18
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 125
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mCtx:Landroid/content/Context;

    invoke-static {v11}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 126
    .local v4, "factory":Landroid/view/LayoutInflater;
    const v11, 0x7f030027

    const/4 v12, 0x0

    invoke-virtual {v4, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 127
    .local v2, "alertDialogueView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    const v11, 0x7f0f0069

    invoke-virtual {v2, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/EditText;

    # setter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v12, v11}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$102(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;Landroid/widget/EditText;)Landroid/widget/EditText;

    .line 129
    sget-boolean v11, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWritingPanel:Z

    if-eqz v11, :cond_0

    .line 130
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v11}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$100(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/widget/EditText;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/widget/EditText;->getInputExtras(Z)Landroid/os/Bundle;

    move-result-object v3

    .line 131
    .local v3, "b":Landroid/os/Bundle;
    const-string v11, "maxLength"

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->MAX_FOLDER_NAME_LENGTH:I

    invoke-virtual {v3, v11, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 133
    .end local v3    # "b":Landroid/os/Bundle;
    :cond_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v11}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$100(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/widget/EditText;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/EditText;->requestFocus()Z

    .line 134
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mTooLongToast:Landroid/widget/Toast;
    invoke-static {v11}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$200(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/widget/Toast;

    move-result-object v11

    if-nez v11, :cond_1

    .line 135
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mCtx:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mCtx:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0e01cc

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->MAX_FOLDER_NAME_LENGTH:I

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v13, v14, v15}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v12

    # setter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mTooLongToast:Landroid/widget/Toast;
    invoke-static {v11, v12}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$202(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 139
    :cond_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mInvalidToast:Landroid/widget/Toast;
    invoke-static {v11}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$300(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/widget/Toast;

    move-result-object v11

    if-nez v11, :cond_2

    .line 140
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mCtx:Landroid/content/Context;

    const v13, 0x7f0e0111

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v12

    # setter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mInvalidToast:Landroid/widget/Toast;
    invoke-static {v11, v12}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$302(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 144
    :cond_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;
    invoke-static {v11}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$400(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v11

    if-nez v11, :cond_3

    .line 145
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v11}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$100(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/widget/EditText;

    move-result-object v11

    new-instance v12, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$1;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$1;-><init>(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;)V

    const-wide/16 v14, 0x12c

    invoke-virtual {v11, v12, v14, v15}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 154
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mIsNewEvent:Z

    if-eqz v11, :cond_7

    .line 155
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mString:Ljava/lang/String;

    .line 159
    .local v8, "newAlbumName":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v11}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$100(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/widget/EditText;

    move-result-object v11

    invoke-virtual {v11, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 160
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v11}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$100(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/widget/EditText;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/EditText;->selectAll()V

    .line 161
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v11}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$100(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/widget/EditText;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 163
    new-instance v5, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$2;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$2;-><init>(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;)V

    .line 235
    .local v5, "filter":Landroid/text/InputFilter;
    new-instance v6, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$3;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->MAX_FOLDER_NAME_LENGTH:I

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v11}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$3;-><init>(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;I)V

    .line 252
    .local v6, "lengthFilter":Landroid/text/InputFilter$LengthFilter;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v11}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$100(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/widget/EditText;

    move-result-object v11

    const-string v12, "inputType=filename;disableEmoticonInput=true"

    invoke-virtual {v11, v12}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 253
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v11}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$100(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/widget/EditText;

    move-result-object v11

    const/4 v12, 0x2

    new-array v12, v12, [Landroid/text/InputFilter;

    const/4 v13, 0x0

    aput-object v5, v12, v13

    const/4 v13, 0x1

    aput-object v6, v12, v13

    invoke-virtual {v11, v12}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 255
    new-instance v9, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$4;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$4;-><init>(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;)V

    .line 274
    .local v9, "textWatcher":Landroid/text/TextWatcher;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v11}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$100(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/widget/EditText;

    move-result-object v11

    invoke-virtual {v11, v9}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 275
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v11}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$100(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/widget/EditText;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 277
    const v10, 0x7f0e01a5

    .line 278
    .local v10, "titleId":I
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mIsNewEvent:Z

    if-eqz v11, :cond_4

    .line 279
    const v10, 0x7f0e0444

    .line 281
    :cond_4
    const v7, 0x7f0e00db

    .line 282
    .local v7, "mPositiveButtonTitle":I
    const v11, 0x7f0e01a5

    if-eq v10, v11, :cond_5

    const v11, 0x7f0e0444

    if-ne v10, v11, :cond_6

    .line 283
    :cond_5
    const v7, 0x7f0e04c4

    .line 285
    :cond_6
    new-instance v11, Landroid/app/AlertDialog$Builder;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v12

    invoke-direct {v11, v12}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v11, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v11

    invoke-virtual {v11, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v7, v12}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v11

    const/high16 v12, 0x1040000

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    .line 292
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    const/4 v12, 0x1

    # setter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mNewAlbumDialogIsReady:Z
    invoke-static {v11, v12}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$702(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;Z)Z

    .line 294
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    new-instance v12, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$5;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$5;-><init>(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;)V

    invoke-virtual {v11, v12}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 336
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v11}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$100(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/widget/EditText;

    move-result-object v11

    new-instance v12, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$6;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$6;-><init>(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;)V

    invoke-virtual {v11, v12}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 348
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-static {v11}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->hideStatusIcon(Ljava/lang/Object;)V

    .line 349
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    return-object v11

    .line 157
    .end local v5    # "filter":Landroid/text/InputFilter;
    .end local v6    # "lengthFilter":Landroid/text/InputFilter$LengthFilter;
    .end local v7    # "mPositiveButtonTitle":I
    .end local v8    # "newAlbumName":Ljava/lang/String;
    .end local v9    # "textWatcher":Landroid/text/TextWatcher;
    .end local v10    # "titleId":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # invokes: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->getSuggestedName()Ljava/lang/String;
    invoke-static {v11}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$500(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Ljava/lang/String;

    move-result-object v8

    .restart local v8    # "newAlbumName":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 418
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 419
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    const/4 v1, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mNewAlbumDialogIsReady:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$702(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;Z)Z

    .line 420
    return-void
.end method

.method public onPause()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 397
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 399
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mReceiveTime:J
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$1700(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x2ee

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 400
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # setter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->isSipVisible:Z
    invoke-static {v1, v6}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$1502(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;Z)Z

    .line 402
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$1800(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 403
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mCtx:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$1800(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/content/BroadcastReceiver;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 405
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    const/4 v2, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$1802(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;

    .line 406
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->alertEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$100(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    # invokes: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->hideSoftInput(Landroid/os/IBinder;)V
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$000(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;Landroid/os/IBinder;)V

    .line 409
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mCtx:Landroid/content/Context;

    const-string v2, "clipboardEx"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/sec/clipboard/ClipboardExManager;

    .line 410
    .local v0, "clipEx":Landroid/sec/clipboard/ClipboardExManager;
    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 411
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mClipVisible:Z

    .line 415
    :goto_0
    return-void

    .line 413
    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mClipVisible:Z

    goto :goto_0
.end method

.method public onResume()V
    .locals 6

    .prologue
    .line 367
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 369
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$1300(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 370
    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 372
    const v2, 0x7f0f0069

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 373
    .local v1, "editText":Landroid/widget/EditText;
    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 374
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 375
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # invokes: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->setSipBroadcastReceiver()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$1400(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)V

    .line 377
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->isSipVisible:Z
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$1500(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 378
    new-instance v2, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$7;

    invoke-direct {v2, p0, v1}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$7;-><init>(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;Landroid/widget/EditText;)V

    const-wide/16 v4, 0x258

    invoke-virtual {v1, v2, v4, v5}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 393
    .end local v1    # "editText":Landroid/widget/EditText;
    :cond_0
    return-void
.end method

.method public setCurrentName(Ljava/lang/String;)V
    .locals 0
    .param p1, "currentName"    # Ljava/lang/String;

    .prologue
    .line 353
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mString:Ljava/lang/String;

    .line 354
    return-void
.end method
