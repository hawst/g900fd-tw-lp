.class public Lcom/sec/samsung/gallery/util/HiddenOperations;
.super Landroid/os/AsyncTask;
.source "HiddenOperations.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/util/HiddenOperations$1;,
        Lcom/sec/samsung/gallery/util/HiddenOperations$MediaObjectComparator;,
        Lcom/sec/samsung/gallery/util/HiddenOperations$OnHiddenListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field public static final OP_DELETE:I = 0x2

.field public static final OP_HIDE:I = 0x0

.field public static final OP_SHOW:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field private static final mComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mOnHiddenListener:Lcom/sec/samsung/gallery/util/HiddenOperations$OnHiddenListener;

.field private mOperationId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    const-class v0, Lcom/sec/samsung/gallery/util/HiddenOperations;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/util/HiddenOperations;->TAG:Ljava/lang/String;

    .line 108
    new-instance v0, Lcom/sec/samsung/gallery/util/HiddenOperations$MediaObjectComparator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/util/HiddenOperations$MediaObjectComparator;-><init>(Lcom/sec/samsung/gallery/util/HiddenOperations$1;)V

    sput-object v0, Lcom/sec/samsung/gallery/util/HiddenOperations;->mComparator:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(ILcom/sec/samsung/gallery/util/HiddenOperations$OnHiddenListener;Landroid/content/Context;)V
    .locals 1
    .param p1, "operationId"    # I
    .param p2, "onHiddenListener"    # Lcom/sec/samsung/gallery/util/HiddenOperations$OnHiddenListener;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 38
    iput p1, p0, Lcom/sec/samsung/gallery/util/HiddenOperations;->mOperationId:I

    .line 39
    iput-object p2, p0, Lcom/sec/samsung/gallery/util/HiddenOperations;->mOnHiddenListener:Lcom/sec/samsung/gallery/util/HiddenOperations$OnHiddenListener;

    .line 40
    iput-object p3, p0, Lcom/sec/samsung/gallery/util/HiddenOperations;->mContext:Landroid/content/Context;

    .line 41
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/HiddenOperations;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/HiddenOperations;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 42
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 9
    .param p1, "args"    # [Ljava/lang/Void;

    .prologue
    .line 52
    const/4 v5, 0x0

    .line 53
    .local v5, "result":Z
    const/4 v0, 0x0

    .line 54
    .local v0, "failedCount":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/util/HiddenOperations;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v6

    .line 55
    .local v6, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 56
    .local v2, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    sget-object v7, Lcom/sec/samsung/gallery/util/HiddenOperations;->mComparator:Ljava/util/Comparator;

    invoke-static {v2, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 58
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 59
    .local v3, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/HiddenOperations;->isCancelled()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 87
    .end local v3    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    return-object v7

    .line 61
    .restart local v3    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    iget v7, p0, Lcom/sec/samsung/gallery/util/HiddenOperations;->mOperationId:I

    if-nez v7, :cond_6

    .line 62
    instance-of v7, v3, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v7, :cond_5

    move-object v4, v3

    .line 63
    check-cast v4, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 65
    .local v4, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    instance-of v7, v4, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v7, :cond_4

    move-object v7, v4

    check-cast v7, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->isCameraAlbum()Z

    move-result v7

    if-nez v7, :cond_4

    .line 67
    iget-object v7, p0, Lcom/sec/samsung/gallery/util/HiddenOperations;->mOnHiddenListener:Lcom/sec/samsung/gallery/util/HiddenOperations$OnHiddenListener;

    invoke-interface {v7, v4}, Lcom/sec/samsung/gallery/util/HiddenOperations$OnHiddenListener;->handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v5

    .line 68
    if-nez v5, :cond_3

    .line 69
    sget-object v7, Lcom/sec/samsung/gallery/util/HiddenOperations;->TAG:Ljava/lang/String;

    const-string v8, "doInBackground() failed because: handleOperation(mediaSet) failed"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    .end local v4    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_3
    :goto_1
    if-nez v5, :cond_0

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 71
    .restart local v4    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_4
    const/4 v5, 0x0

    .line 72
    sget-object v7, Lcom/sec/samsung/gallery/util/HiddenOperations;->TAG:Ljava/lang/String;

    const-string v8, "doInBackground() failed because: mediaSet isn\'t LocalMergeAsbum or is CameraAlbum "

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 74
    .end local v4    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_5
    instance-of v7, v3, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v7, :cond_3

    .line 75
    iget-object v7, p0, Lcom/sec/samsung/gallery/util/HiddenOperations;->mOnHiddenListener:Lcom/sec/samsung/gallery/util/HiddenOperations$OnHiddenListener;

    invoke-interface {v7, v3}, Lcom/sec/samsung/gallery/util/HiddenOperations$OnHiddenListener;->handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v5

    .line 76
    if-nez v5, :cond_3

    .line 77
    sget-object v7, Lcom/sec/samsung/gallery/util/HiddenOperations;->TAG:Ljava/lang/String;

    const-string v8, "doInBackground() failed because: handleOperation(mediaObject) failed"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 79
    :cond_6
    iget v7, p0, Lcom/sec/samsung/gallery/util/HiddenOperations;->mOperationId:I

    const/4 v8, 0x1

    if-eq v7, v8, :cond_7

    iget v7, p0, Lcom/sec/samsung/gallery/util/HiddenOperations;->mOperationId:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_3

    .line 80
    :cond_7
    iget-object v7, p0, Lcom/sec/samsung/gallery/util/HiddenOperations;->mOnHiddenListener:Lcom/sec/samsung/gallery/util/HiddenOperations$OnHiddenListener;

    invoke-interface {v7, v3}, Lcom/sec/samsung/gallery/util/HiddenOperations$OnHiddenListener;->handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v5

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 18
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/HiddenOperations;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/HiddenOperations;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->setChangeNotifierActive(Z)V

    .line 102
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/HiddenOperations;->mOnHiddenListener:Lcom/sec/samsung/gallery/util/HiddenOperations$OnHiddenListener;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/HiddenOperations;->mOnHiddenListener:Lcom/sec/samsung/gallery/util/HiddenOperations$OnHiddenListener;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/util/HiddenOperations$OnHiddenListener;->onCancelled()V

    .line 105
    :cond_0
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 106
    return-void
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/HiddenOperations;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->setChangeNotifierActive(Z)V

    .line 93
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/HiddenOperations;->mOnHiddenListener:Lcom/sec/samsung/gallery/util/HiddenOperations$OnHiddenListener;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/HiddenOperations;->mOnHiddenListener:Lcom/sec/samsung/gallery/util/HiddenOperations$OnHiddenListener;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/util/HiddenOperations$OnHiddenListener;->onCompleted(I)V

    .line 96
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 97
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 18
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/HiddenOperations;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/HiddenOperations;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->setChangeNotifierActive(Z)V

    .line 47
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 48
    return-void
.end method
