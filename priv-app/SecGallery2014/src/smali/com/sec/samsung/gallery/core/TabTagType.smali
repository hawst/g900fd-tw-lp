.class public final enum Lcom/sec/samsung/gallery/core/TabTagType;
.super Ljava/lang/Enum;
.source "TabTagType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/samsung/gallery/core/TabTagType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/samsung/gallery/core/TabTagType;

.field public static TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Lcom/sec/samsung/gallery/core/TabTagType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum TAB_SELECT_ALL:Lcom/sec/samsung/gallery/core/TabTagType;

.field public static final enum TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

.field public static final enum TAB_TAG_ALL:Lcom/sec/samsung/gallery/core/TabTagType;

.field public static final enum TAB_TAG_CATEGORY:Lcom/sec/samsung/gallery/core/TabTagType;

.field public static final enum TAB_TAG_DOCUMENTS:Lcom/sec/samsung/gallery/core/TabTagType;

.field public static final enum TAB_TAG_EVENT:Lcom/sec/samsung/gallery/core/TabTagType;

.field public static final enum TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

.field public static final enum TAB_TAG_FLOWERS:Lcom/sec/samsung/gallery/core/TabTagType;

.field public static final enum TAB_TAG_FOOD:Lcom/sec/samsung/gallery/core/TabTagType;

.field public static final enum TAB_TAG_LOCATION:Lcom/sec/samsung/gallery/core/TabTagType;

.field public static final enum TAB_TAG_OTHER_DEVICE:Lcom/sec/samsung/gallery/core/TabTagType;

.field public static final enum TAB_TAG_PETS:Lcom/sec/samsung/gallery/core/TabTagType;

.field public static final enum TAB_TAG_SCENERY:Lcom/sec/samsung/gallery/core/TabTagType;

.field public static final enum TAB_TAG_SEARCH:Lcom/sec/samsung/gallery/core/TabTagType;

.field public static final enum TAB_TAG_SECRET_BOX:Lcom/sec/samsung/gallery/core/TabTagType;

.field public static final enum TAB_TAG_SNS:Lcom/sec/samsung/gallery/core/TabTagType;

.field public static final enum TAB_TAG_TAG:Lcom/sec/samsung/gallery/core/TabTagType;

.field public static final enum TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

.field public static final enum TAB_TAG_VEHICLES:Lcom/sec/samsung/gallery/core/TabTagType;

.field private static mArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/core/TabTagType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 18
    new-instance v0, Lcom/sec/samsung/gallery/core/TabTagType;

    const-string v1, "TAB_TAG_TIMELINE"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/samsung/gallery/core/TabTagType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 19
    new-instance v0, Lcom/sec/samsung/gallery/core/TabTagType;

    const-string v1, "TAB_TAG_ALBUM"

    invoke-direct {v0, v1, v5, v5}, Lcom/sec/samsung/gallery/core/TabTagType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 20
    new-instance v0, Lcom/sec/samsung/gallery/core/TabTagType;

    const-string v1, "TAB_TAG_EVENT"

    invoke-direct {v0, v1, v6, v6}, Lcom/sec/samsung/gallery/core/TabTagType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_EVENT:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 21
    new-instance v0, Lcom/sec/samsung/gallery/core/TabTagType;

    const-string v1, "TAB_TAG_CATEGORY"

    invoke-direct {v0, v1, v7, v7}, Lcom/sec/samsung/gallery/core/TabTagType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_CATEGORY:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 22
    new-instance v0, Lcom/sec/samsung/gallery/core/TabTagType;

    const-string v1, "TAB_SELECT_ALL"

    invoke-direct {v0, v1, v8, v8}, Lcom/sec/samsung/gallery/core/TabTagType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_SELECT_ALL:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 23
    new-instance v0, Lcom/sec/samsung/gallery/core/TabTagType;

    const-string v1, "TAB_TAG_LOCATION"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/core/TabTagType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_LOCATION:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 24
    new-instance v0, Lcom/sec/samsung/gallery/core/TabTagType;

    const-string v1, "TAB_TAG_FACE"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/core/TabTagType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 25
    new-instance v0, Lcom/sec/samsung/gallery/core/TabTagType;

    const-string v1, "TAB_TAG_SECRET_BOX"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/core/TabTagType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SECRET_BOX:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 26
    new-instance v0, Lcom/sec/samsung/gallery/core/TabTagType;

    const-string v1, "TAB_TAG_SNS"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/core/TabTagType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SNS:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 27
    new-instance v0, Lcom/sec/samsung/gallery/core/TabTagType;

    const-string v1, "TAB_TAG_TAG"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/core/TabTagType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TAG:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 28
    new-instance v0, Lcom/sec/samsung/gallery/core/TabTagType;

    const-string v1, "TAB_TAG_ALL"

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/core/TabTagType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALL:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 29
    new-instance v0, Lcom/sec/samsung/gallery/core/TabTagType;

    const-string v1, "TAB_TAG_SCENERY"

    const/16 v2, 0xb

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/core/TabTagType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SCENERY:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 30
    new-instance v0, Lcom/sec/samsung/gallery/core/TabTagType;

    const-string v1, "TAB_TAG_DOCUMENTS"

    const/16 v2, 0xc

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/core/TabTagType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_DOCUMENTS:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 31
    new-instance v0, Lcom/sec/samsung/gallery/core/TabTagType;

    const-string v1, "TAB_TAG_FOOD"

    const/16 v2, 0xd

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/core/TabTagType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FOOD:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 32
    new-instance v0, Lcom/sec/samsung/gallery/core/TabTagType;

    const-string v1, "TAB_TAG_PETS"

    const/16 v2, 0xe

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/core/TabTagType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_PETS:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 33
    new-instance v0, Lcom/sec/samsung/gallery/core/TabTagType;

    const-string v1, "TAB_TAG_VEHICLES"

    const/16 v2, 0xf

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/core/TabTagType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_VEHICLES:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 34
    new-instance v0, Lcom/sec/samsung/gallery/core/TabTagType;

    const-string v1, "TAB_TAG_FLOWERS"

    const/16 v2, 0x10

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/core/TabTagType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FLOWERS:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 35
    new-instance v0, Lcom/sec/samsung/gallery/core/TabTagType;

    const-string v1, "TAB_TAG_OTHER_DEVICE"

    const/16 v2, 0x11

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/core/TabTagType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_OTHER_DEVICE:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 36
    new-instance v0, Lcom/sec/samsung/gallery/core/TabTagType;

    const-string v1, "TAB_TAG_SEARCH"

    const/16 v2, 0x12

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/core/TabTagType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SEARCH:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 14
    const/16 v0, 0x13

    new-array v0, v0, [Lcom/sec/samsung/gallery/core/TabTagType;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_EVENT:Lcom/sec/samsung/gallery/core/TabTagType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_CATEGORY:Lcom/sec/samsung/gallery/core/TabTagType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_SELECT_ALL:Lcom/sec/samsung/gallery/core/TabTagType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_LOCATION:Lcom/sec/samsung/gallery/core/TabTagType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SECRET_BOX:Lcom/sec/samsung/gallery/core/TabTagType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SNS:Lcom/sec/samsung/gallery/core/TabTagType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TAG:Lcom/sec/samsung/gallery/core/TabTagType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALL:Lcom/sec/samsung/gallery/core/TabTagType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SCENERY:Lcom/sec/samsung/gallery/core/TabTagType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_DOCUMENTS:Lcom/sec/samsung/gallery/core/TabTagType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FOOD:Lcom/sec/samsung/gallery/core/TabTagType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_PETS:Lcom/sec/samsung/gallery/core/TabTagType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_VEHICLES:Lcom/sec/samsung/gallery/core/TabTagType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FLOWERS:Lcom/sec/samsung/gallery/core/TabTagType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_OTHER_DEVICE:Lcom/sec/samsung/gallery/core/TabTagType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SEARCH:Lcom/sec/samsung/gallery/core/TabTagType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->$VALUES:[Lcom/sec/samsung/gallery/core/TabTagType;

    .line 43
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    .line 44
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    const v2, 0x7f0e0073

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    const v2, 0x7f0e00a4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_EVENT:Lcom/sec/samsung/gallery/core/TabTagType;

    const v2, 0x7f0e0441

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    const v2, 0x7f0e00a7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SCENERY:Lcom/sec/samsung/gallery/core/TabTagType;

    const v2, 0x7f0e0478

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_DOCUMENTS:Lcom/sec/samsung/gallery/core/TabTagType;

    const v2, 0x7f0e047c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FOOD:Lcom/sec/samsung/gallery/core/TabTagType;

    const v2, 0x7f0e0479

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_PETS:Lcom/sec/samsung/gallery/core/TabTagType;

    const v2, 0x7f0e047b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_VEHICLES:Lcom/sec/samsung/gallery/core/TabTagType;

    const v2, 0x7f0e047d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FLOWERS:Lcom/sec/samsung/gallery/core/TabTagType;

    const v2, 0x7f0e047f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 60
    iput p3, p0, Lcom/sec/samsung/gallery/core/TabTagType;->mIndex:I

    .line 61
    return-void
.end method

.method public static from(Lcom/sec/samsung/gallery/core/TabTagType;)I
    .locals 4
    .param p0, "tabTagType"    # Lcom/sec/samsung/gallery/core/TabTagType;

    .prologue
    .line 112
    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 113
    .local v0, "index":I
    if-gez v0, :cond_0

    .line 114
    const-string v1, "TabTagType"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "index:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", tabTagType:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mArrayList: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "TabTagType error"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 117
    :cond_0
    return v0
.end method

.method public static from(I)Lcom/sec/samsung/gallery/core/TabTagType;
    .locals 3
    .param p0, "position"    # I

    .prologue
    .line 103
    if-ltz p0, :cond_0

    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p0, v0, :cond_1

    .line 104
    :cond_0
    const-string v0, "TabTagType"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mArrayList.size():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", position:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mArrayList: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "unknown tab"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_1
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/core/TabTagType;

    return-object v0
.end method

.method public static initializeEnum(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 68
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 69
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_1

    .line 70
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEventView:Z

    if-eqz v0, :cond_2

    .line 75
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_EVENT:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    :cond_2
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_SELECT_ALL:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALL:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 80
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_LOCATION:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    :cond_3
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v0, :cond_4

    .line 83
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    :cond_4
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v0, :cond_5

    .line 86
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SECRET_BOX:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    :cond_5
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SNS:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v0, :cond_0

    .line 90
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_CATEGORY:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SCENERY:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_DOCUMENTS:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FOOD:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_PETS:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_VEHICLES:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FLOWERS:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->mArrayList:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SEARCH:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/samsung/gallery/core/TabTagType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v0, Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/core/TabTagType;

    return-object v0
.end method

.method public static values()[Lcom/sec/samsung/gallery/core/TabTagType;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->$VALUES:[Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v0}, [Lcom/sec/samsung/gallery/core/TabTagType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/samsung/gallery/core/TabTagType;

    return-object v0
.end method


# virtual methods
.method public getIndex()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/samsung/gallery/core/TabTagType;->mIndex:I

    return v0
.end method
