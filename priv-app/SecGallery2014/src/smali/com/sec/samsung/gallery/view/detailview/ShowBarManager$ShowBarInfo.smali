.class Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;
.super Ljava/lang/Object;
.source "ShowBarManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ShowBarInfo"
.end annotation


# instance fields
.field public disableAutoHide:Z

.field public showActionBar:I

.field public showFilm:I


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1, "showActionBar"    # I
    .param p2, "showFilm"    # I

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;->disableAutoHide:Z

    .line 32
    iput p1, p0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;->showActionBar:I

    .line 33
    iput p2, p0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;->showFilm:I

    .line 34
    return-void
.end method

.method public constructor <init>(IIZ)V
    .locals 0
    .param p1, "showActionBar"    # I
    .param p2, "showFilm"    # I
    .param p3, "disableAutoHide"    # Z

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;-><init>(II)V

    .line 39
    iput-boolean p3, p0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;->disableAutoHide:Z

    .line 40
    return-void
.end method
