.class public Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;
.super Ljava/lang/Object;
.source "GlGatherAnimation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$OnDragAnimListener;
    }
.end annotation


# static fields
.field public static DRAG_ANIM_FIRST:I = 0x0

.field public static DRAG_ANIM_NONE:I = 0x0

.field public static DRAG_ANIM_SEC:I = 0x0

.field public static DRAG_ANIM_THIRD:I = 0x0

.field private static final DST_RESIZE_FACTOR:F = 1.0f

.field private static final MAX_DIM_VALUE:F = 1.0f

.field private static final MIN_DIM_VALUE:F = 0.2f

.field private static final TAG:Ljava/lang/String; = "GlGatherAnimation"

.field public static posLatency:I

.field public static stackZ:F


# instance fields
.field private mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

.field private final mContext:Landroid/content/Context;

.field private mCount:I

.field private mCurrentAnim:I

.field private mDestObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field private mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field private mFollowAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

.field private mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

.field private mInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

.field private mIsInverseDragAnimationRunning:Z

.field private mIsMoveToAnimationRunning:Z

.field private mOnDragListener:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$OnDragAnimListener;

.field private mScrollDownArea:F

.field private mScrollSplit:F

.field private mScrollUpArea:F

.field private mSrcObj:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;",
            ">;"
        }
    .end annotation
.end field

.field private mThumbDragSet:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;",
            ">;"
        }
    .end annotation
.end field

.field protected mlistenerInverseDrag:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

.field protected mlistenerMoveTo:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 22
    const/4 v0, 0x0

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->DRAG_ANIM_NONE:I

    .line 23
    const/4 v0, 0x1

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->DRAG_ANIM_FIRST:I

    .line 24
    sput v1, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->DRAG_ANIM_SEC:I

    .line 25
    const/4 v0, 0x3

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->DRAG_ANIM_THIRD:I

    .line 31
    const/high16 v0, 0x42c80000    # 100.0f

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->stackZ:F

    .line 32
    sput v1, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->posLatency:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "composeView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr2;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr2;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mSrcObj:Ljava/util/ArrayList;

    .line 38
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mOnDragListener:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$OnDragAnimListener;

    .line 41
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 44
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCurrentAnim:I

    .line 46
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mIsInverseDragAnimationRunning:Z

    .line 47
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mIsMoveToAnimationRunning:Z

    .line 350
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$2;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mlistenerInverseDrag:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 373
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$3;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mlistenerMoveTo:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 393
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$4;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mFollowAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .line 50
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mContext:Landroid/content/Context;

    .line 51
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .line 52
    return-void
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;
    .param p1, "x1"    # I

    .prologue
    .line 19
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCurrentAnim:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->startFollowingAnim()V

    return-void
.end method

.method static synthetic access$202(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;
    .param p1, "x1"    # Z

    .prologue
    .line 19
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mIsInverseDragAnimationRunning:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;)Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;
    .param p1, "x1"    # Z

    .prologue
    .line 19
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mIsMoveToAnimationRunning:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;)Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;)Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$OnDragAnimListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mOnDragListener:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$OnDragAnimListener;

    return-object v0
.end method

.method private startFollowingAnim()V
    .locals 6

    .prologue
    .line 340
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 341
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .line 342
    .local v2, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->updateInitPosition()V

    .line 340
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 344
    .end local v2    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mFollowAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 345
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mFollowAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    const-wide/32 v4, 0x989680

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->setDuration(J)V

    .line 346
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mFollowAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->start()V

    .line 347
    sget v3, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->DRAG_ANIM_SEC:I

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCurrentAnim:I

    .line 348
    return-void
.end method


# virtual methods
.method public findFocused(Landroid/util/SparseArray;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<+",
            "Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "activeObjs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<+Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;>;"
    const/4 v6, 0x0

    .line 190
    const/4 v1, 0x0

    .line 195
    .local v1, "focusedObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-nez p1, :cond_3

    .line 196
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mDestObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsX()F

    move-result v4

    .line 197
    .local v4, "x":F
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mDestObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsY()F

    move-result v5

    .line 198
    .local v5, "y":F
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v7, v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->isInclude(FF)Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mDestObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 211
    .end local v4    # "x":F
    .end local v5    # "y":F
    :cond_0
    :goto_0
    if-eqz v1, :cond_5

    .line 212
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 213
    const/4 v6, 0x1

    invoke-virtual {v1, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setFocused(Z)V

    .line 218
    :cond_1
    :goto_1
    return-void

    .restart local v4    # "x":F
    .restart local v5    # "y":F
    :cond_2
    move-object v1, v6

    .line 198
    goto :goto_0

    .line 200
    .end local v4    # "x":F
    .end local v5    # "y":F
    :cond_3
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v0

    .line 201
    .local v0, "activeCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, v0, :cond_0

    .line 202
    invoke-virtual {p1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 203
    .local v3, "tmpObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsX()F

    move-result v4

    .line 204
    .restart local v4    # "x":F
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsY()F

    move-result v5

    .line 205
    .restart local v5    # "y":F
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v7, v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->isInclude(FF)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 206
    move-object v1, v3

    .line 207
    goto :goto_0

    .line 201
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 214
    .end local v0    # "activeCount":I
    .end local v2    # "i":I
    .end local v3    # "tmpObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .end local v4    # "x":F
    .end local v5    # "y":F
    :cond_5
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v7, :cond_1

    .line 215
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setFocused(Z)V

    .line 216
    iput-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    goto :goto_1
.end method

.method public getFocusedObject()Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    return-object v0
.end method

.method public getHeaderObject()Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    return-object v0
.end method

.method public getScrollIntensity()F
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 163
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->getAbsX()F

    move-result v0

    .line 164
    .local v0, "x":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->getAbsY()F

    move-result v1

    .line 166
    .local v1, "y":F
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mScrollSplit:F

    cmpl-float v3, v0, v3

    if-lez v3, :cond_1

    .line 172
    :cond_0
    :goto_0
    return v2

    .line 168
    :cond_1
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mScrollUpArea:F

    cmpl-float v3, v1, v3

    if-lez v3, :cond_2

    .line 169
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mScrollUpArea:F

    sub-float v2, v1, v2

    goto :goto_0

    .line 170
    :cond_2
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mScrollDownArea:F

    cmpg-float v3, v1, v3

    if-gez v3, :cond_0

    .line 171
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mScrollDownArea:F

    sub-float v2, v1, v2

    goto :goto_0
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFocused()Z
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLastAnimRunning()Z
    .locals 1

    .prologue
    .line 229
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mIsInverseDragAnimationRunning:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mIsMoveToAnimationRunning:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public releaseDrag()V
    .locals 5

    .prologue
    .line 304
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v3, :cond_0

    .line 305
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setFocused(Z)V

    .line 306
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 308
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCount:I

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 309
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .line 310
    .local v2, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->remove()V

    .line 308
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 312
    .end local v2    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 313
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mSrcObj:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 314
    return-void
.end method

.method public resetDrag()V
    .locals 4

    .prologue
    .line 317
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mSrcObj:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 327
    :goto_0
    return-void

    .line 319
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCount:I

    .local v1, "n":I
    :goto_1
    if-ge v0, v1, :cond_2

    .line 320
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mSrcObj:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 321
    .local v2, "srcObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz v2, :cond_1

    .line 322
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->stopDim()V

    .line 323
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setDim(F)V

    .line 319
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 326
    .end local v2    # "srcObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->releaseDrag()V

    goto :goto_0
.end method

.method public resetSourceDim(Z)V
    .locals 3
    .param p1, "doAnim"    # Z

    .prologue
    .line 330
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCount:I

    if-ge v0, v2, :cond_1

    .line 331
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mSrcObj:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 332
    .local v1, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz p1, :cond_0

    .line 333
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->undim()V

    .line 330
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 335
    :cond_0
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->resetDim()V

    goto :goto_1

    .line 337
    .end local v1    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_1
    return-void
.end method

.method public setOnDragAnimListener(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$OnDragAnimListener;)V
    .locals 0
    .param p1, "onDragListener"    # Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$OnDragAnimListener;

    .prologue
    .line 400
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mOnDragListener:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$OnDragAnimListener;

    .line 401
    return-void
.end method

.method public setScrollArea(FFF)V
    .locals 0
    .param p1, "split"    # F
    .param p2, "upArea"    # F
    .param p3, "downArea"    # F

    .prologue
    .line 55
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mScrollSplit:F

    .line 56
    iput p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mScrollUpArea:F

    .line 57
    iput p3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mScrollDownArea:F

    .line 58
    return-void
.end method

.method public setTargetPos(FF)V
    .locals 7
    .param p1, "dx"    # F
    .param p2, "dy"    # F

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .line 179
    .local v0, "dstObj":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    iget v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mHndDispmnt:I

    neg-float v5, p2

    const/4 v6, 0x0

    invoke-virtual {v0, v4, p1, v5, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setPos(IFFF)V

    .line 180
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCurrentAnim:I

    sget v5, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->DRAG_ANIM_SEC:I

    if-ne v4, v5, :cond_1

    .line 187
    :cond_0
    return-void

    .line 183
    :cond_1
    const/4 v1, 0x1

    .local v1, "i":I
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCount:I

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 184
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .line 185
    .local v3, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->getAbsX()F

    move-result v4

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->getAbsY()F

    move-result v5

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->getAbsZ()F

    move-result v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setTargetPos(FFF)V

    .line 183
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public startAnimation(Landroid/util/SparseArray;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;II)V
    .locals 13
    .param p2, "toObj"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .param p3, "destObj"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .param p4, "pressedX"    # I
    .param p5, "pressedY"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<+",
            "Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;",
            ">;",
            "Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;",
            "Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p1, "activeObjs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<+Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;>;"
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v6, v10, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .line 62
    .local v6, "posCtrl":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v1

    .line 67
    .local v1, "activeCount":I
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mDestObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 68
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 69
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mSrcObj:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 71
    invoke-virtual {p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsZ()F

    move-result v7

    .line 72
    .local v7, "sz":F
    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getWidth(Z)F

    move-result v10

    const/high16 v11, 0x3f800000    # 1.0f

    mul-float v4, v10, v11

    .line 73
    .local v4, "dstW":F
    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getHeight(Z)F

    move-result v10

    const/high16 v11, 0x3f800000    # 1.0f

    mul-float v3, v10, v11

    .line 75
    .local v3, "dstH":F
    const/4 v10, 0x0

    iput v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCount:I

    .line 76
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v1, :cond_2

    .line 77
    invoke-virtual {p1, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 78
    .local v8, "thmObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->isSelected()Z

    move-result v10

    if-eqz v10, :cond_0

    if-ne v8, p2, :cond_1

    .line 76
    :cond_0
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 81
    :cond_1
    new-instance v9, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-direct {v9, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    .line 82
    .local v9, "trailObj":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    iget v10, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    iput v10, v9, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mIndex:I

    .line 83
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    const/4 v11, 0x0

    invoke-virtual {v10, v11, v9}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 84
    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCount:I

    add-int/lit8 v10, v10, 0x1

    iput v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCount:I

    goto :goto_1

    .line 87
    .end local v8    # "thmObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .end local v9    # "trailObj":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    :cond_2
    new-instance v9, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-direct {v9, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    .line 88
    .restart local v9    # "trailObj":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    iget v10, p2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    iput v10, v9, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mIndex:I

    .line 89
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    const/4 v11, 0x0

    invoke-virtual {v10, v11, v9}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 90
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setPosIndex()I

    move-result v10

    iput v10, v9, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mHndDispmnt:I

    .line 91
    iput-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .line 92
    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCount:I

    add-int/lit8 v10, v10, 0x1

    iput v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCount:I

    .line 93
    const/4 v5, 0x0

    :goto_2
    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCount:I

    if-ge v5, v10, :cond_4

    .line 94
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "trailObj":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    check-cast v9, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .line 95
    .restart local v9    # "trailObj":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    iget v10, v9, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mIndex:I

    invoke-virtual {p1, v10}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 96
    .restart local v8    # "thmObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-nez v8, :cond_3

    .line 93
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 97
    :cond_3
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getWidth(Z)F

    move-result v10

    const/4 v11, 0x0

    invoke-virtual {v8, v11}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getHeight(Z)F

    move-result v11

    invoke-virtual {v9, v10, v11}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setSize(FF)V

    .line 98
    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getAbsX()F

    move-result v10

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getAbsY()F

    move-result v11

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getAbsZ()F

    move-result v12

    invoke-virtual {v9, v10, v11, v12}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setPos(FFF)V

    .line 99
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setCurrentPosToSource()V

    .line 100
    iget v10, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mWidth:F

    iget v11, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mHeight:F

    invoke-virtual {v9, v10, v11}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setSourceSize(FF)V

    .line 102
    invoke-virtual {v9, v4, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setTargetSize(FF)V

    .line 103
    move/from16 v0, p4

    int-to-float v10, v0

    move/from16 v0, p5

    int-to-float v11, v0

    invoke-virtual {v9, v10, v11, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setTargetPos(FFF)V

    .line 104
    sget v10, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->posLatency:I

    invoke-virtual {v9, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setTrailLatency(I)V

    .line 105
    invoke-virtual {v6, v9}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->update(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)Z

    .line 107
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mSrcObj:Ljava/util/ArrayList;

    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->dim()V

    .line 109
    const v10, 0x3f4ccccd    # 0.8f

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCount:I

    sub-int/2addr v11, v5

    int-to-float v11, v11

    mul-float/2addr v10, v11

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCount:I

    int-to-float v11, v11

    div-float/2addr v10, v11

    const v11, 0x3e4ccccd    # 0.2f

    add-float v2, v10, v11

    .line 110
    .local v2, "dim":F
    invoke-virtual {v9, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setDim(F)V

    goto :goto_3

    .line 112
    .end local v2    # "dim":F
    .end local v8    # "thmObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_4
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-static {v10}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->linkTrail(Ljava/util/ArrayList;)V

    .line 113
    return-void
.end method

.method public startDragAnimation()V
    .locals 15

    .prologue
    const-wide/16 v2, 0x1f4

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 116
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mIsInverseDragAnimationRunning:Z

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mIsMoveToAnimationRunning:Z

    if-eqz v6, :cond_1

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v14

    .line 123
    .local v14, "n":I
    if-ne v14, v7, :cond_2

    .line 124
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .line 125
    .local v0, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->startTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;JJLcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    move v1, v7

    move v2, v7

    move v3, v9

    move v4, v9

    move v5, v9

    .line 126
    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setEnableAnim(ZZZZZ)V

    .line 127
    sget v1, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->DRAG_ANIM_FIRST:I

    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCurrentAnim:I

    goto :goto_0

    .line 130
    .end local v0    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    :cond_2
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCount:I

    if-ge v13, v6, :cond_3

    .line 131
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v6, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .line 132
    .restart local v0    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->startTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;JJLcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    move-object v6, v0

    move v8, v7

    move v10, v9

    move v11, v9

    .line 133
    invoke-virtual/range {v6 .. v11}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setEnableAnim(ZZZZZ)V

    .line 130
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 135
    .end local v0    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    .line 136
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getHapticUtils()Lcom/sec/samsung/gallery/haptic/HapticUtils;

    move-result-object v1

    invoke-virtual {v1, v9}, Lcom/sec/samsung/gallery/haptic/HapticUtils;->playHaptic(I)V

    .line 137
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .line 138
    .restart local v0    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->getTransAnim()Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    move-result-object v12

    .line 139
    .local v12, "animation":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    new-instance v1, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$1;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;)V

    invoke-virtual {v12, v1}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    goto :goto_0
.end method

.method public startInverseDragAnimation()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 238
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 264
    :cond_0
    :goto_0
    return-void

    .line 240
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mFollowAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->stop()V

    .line 241
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    .line 242
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getHapticUtils()Lcom/sec/samsung/gallery/haptic/HapticUtils;

    move-result-object v1

    invoke-virtual {v1, v11}, Lcom/sec/samsung/gallery/haptic/HapticUtils;->playHaptic(I)V

    .line 243
    sget v1, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->DRAG_ANIM_THIRD:I

    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCurrentAnim:I

    .line 245
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mHndDispmnt:I

    invoke-virtual {v1, v2, v11}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->addPos(II)V

    .line 246
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mHndDispmnt:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->releasePosIndex(I)V

    .line 247
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 248
    .local v7, "activeCount":I
    if-eqz v7, :cond_0

    .line 250
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    if-ge v9, v7, :cond_2

    .line 251
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .line 252
    .local v0, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->getTransAnim()Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    move-result-object v8

    .line 253
    .local v8, "animation":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->stop()V

    .line 254
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mSrcObj:Ljava/util/ArrayList;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 255
    .local v10, "srcObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setCurrentPosToSource()V

    .line 256
    iget v1, v0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mWidth:F

    iget v2, v0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mHeight:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setSourceSize(FF)V

    .line 257
    iget v1, v10, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mWidth:F

    iget v2, v10, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHeight:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setTargetSize(FF)V

    .line 258
    invoke-virtual {v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsX()F

    move-result v1

    invoke-virtual {v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsY()F

    move-result v2

    invoke-virtual {v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsZ()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setTargetPos(FFF)V

    .line 259
    const/4 v1, 0x0

    const-wide/16 v2, 0x1f4

    const-wide/16 v4, 0x0

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->startTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;JJLcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    move v1, v12

    move v2, v12

    move v3, v11

    move v4, v11

    move v5, v11

    .line 260
    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setEnableAnim(ZZZZZ)V

    .line 250
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 262
    .end local v0    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    .end local v8    # "animation":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    .end local v10    # "srcObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->getTransAnim()Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    move-result-object v8

    .line 263
    .restart local v8    # "animation":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mlistenerInverseDrag:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    invoke-virtual {v8, v1}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    goto/16 :goto_0
.end method

.method public startMoveToAnimation()V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 268
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 272
    .local v9, "dstObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 299
    :cond_0
    :goto_0
    return-void

    .line 274
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mFollowAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->stop()V

    .line 275
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    .line 276
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getHapticUtils()Lcom/sec/samsung/gallery/haptic/HapticUtils;

    move-result-object v1

    invoke-virtual {v1, v13}, Lcom/sec/samsung/gallery/haptic/HapticUtils;->playHaptic(I)V

    .line 277
    sget v1, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->DRAG_ANIM_THIRD:I

    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCurrentAnim:I

    .line 279
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mHndDispmnt:I

    invoke-virtual {v1, v2, v12}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->addPos(II)V

    .line 280
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mHndDispmnt:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->releasePosIndex(I)V

    .line 281
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 282
    .local v7, "activeCount":I
    if-eqz v7, :cond_0

    .line 284
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    if-ge v10, v7, :cond_2

    .line 285
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .line 286
    .local v0, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->getTransAnim()Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    move-result-object v8

    .line 287
    .local v8, "animation":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->stop()V

    .line 288
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setCurrentPosToSource()V

    .line 289
    iget v1, v0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mWidth:F

    iget v2, v0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mHeight:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setSourceSize(FF)V

    .line 290
    iget v1, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mWidth:F

    iget v2, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHeight:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setTargetSize(FF)V

    .line 291
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsX()F

    move-result v1

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsY()F

    move-result v2

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsZ()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setTargetPos(FFF)V

    .line 293
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCount:I

    sub-int/2addr v1, v10

    add-int/lit8 v1, v1, -0x1

    mul-int/lit8 v11, v1, 0x28

    .line 294
    .local v11, "latency":I
    const/4 v1, 0x0

    const-wide/16 v2, 0xc8

    int-to-long v4, v11

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->startTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;JJLcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    move v1, v13

    move v2, v13

    move v3, v12

    move v4, v12

    move v5, v12

    .line 295
    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setEnableAnim(ZZZZZ)V

    .line 284
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 297
    .end local v0    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    .end local v8    # "animation":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    .end local v11    # "latency":I
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->getTransAnim()Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    move-result-object v8

    .line 298
    .restart local v8    # "animation":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mlistenerMoveTo:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    invoke-virtual {v8, v1}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    goto/16 :goto_0
.end method
