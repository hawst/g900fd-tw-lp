.class Lcom/sec/samsung/gallery/glview/GlGridView$11;
.super Ljava/lang/Object;
.source "GlGridView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGridView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V
    .locals 0

    .prologue
    .line 1076
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isReachedBoundary()I
    .locals 1

    .prologue
    .line 1134
    const/4 v0, 0x0

    return v0
.end method

.method public onPenEnter(Landroid/graphics/PointF;)Z
    .locals 1
    .param p1, "point"    # Landroid/graphics/PointF;

    .prologue
    .line 1081
    const/4 v0, 0x0

    return v0
.end method

.method public onPenMove(Landroid/graphics/Rect;Z)Z
    .locals 2
    .param p1, "r"    # Landroid/graphics/Rect;
    .param p2, "listScrollActive"    # Z

    .prologue
    const/4 v1, 0x1

    .line 1123
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mIsDisplayedPenSelectionBox:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1000(Lcom/sec/samsung/gallery/glview/GlGridView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1124
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mIsDisplayedPenSelectionBox:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1002(Lcom/sec/samsung/gallery/glview/GlGridView;Z)Z

    .line 1125
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->requestStart(Landroid/graphics/Rect;)V

    .line 1127
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->changeBackgroundSize(Landroid/graphics/Rect;)V

    .line 1128
    return v1
.end method

.method public onPenSelect(Landroid/graphics/PointF;Landroid/graphics/Rect;)Z
    .locals 13
    .param p1, "point"    # Landroid/graphics/PointF;
    .param p2, "r"    # Landroid/graphics/Rect;

    .prologue
    const/4 v12, 0x0

    .line 1087
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGridView;->mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    if-eqz v8, :cond_0

    .line 1088
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mIsDisplayedPenSelectionBox:Z
    invoke-static {v8, v12}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1002(Lcom/sec/samsung/gallery/glview/GlGridView;Z)Z

    .line 1089
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGridView;->mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->exitFromPenSelectionView()V

    .line 1093
    :cond_0
    :try_start_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/GlGridView;->getFirstVisiblePosition()I

    move-result v3

    .line 1094
    .local v3, "firstItem":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/GlGridView;->getLastVisiblePosition()I

    move-result v5

    .line 1096
    .local v5, "lastItem":I
    move v7, v3

    .line 1097
    .local v7, "start":I
    move v2, v5

    .line 1098
    .local v2, "end":I
    move v4, v3

    .local v4, "i":I
    :goto_0
    if-ge v4, v5, :cond_5

    .line 1099
    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->ALBUM_ITEM_MAX:I
    invoke-static {}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1100()I

    move-result v8

    rem-int v6, v4, v8

    .line 1100
    .local v6, "realIndex":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;
    invoke-static {v8}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1200(Lcom/sec/samsung/gallery/glview/GlGridView;)[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v8

    aget-object v8, v8, v6

    iget v0, v8, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    .line 1101
    .local v0, "albumIndex":I
    if-gez v0, :cond_2

    .line 1098
    :cond_1
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1103
    :cond_2
    if-nez v4, :cond_3

    move v7, v0

    .line 1104
    :cond_3
    if-ge v0, v7, :cond_4

    move v7, v0

    .line 1105
    :cond_4
    if-le v0, v2, :cond_1

    move v2, v0

    goto :goto_1

    .line 1107
    .end local v0    # "albumIndex":I
    .end local v6    # "realIndex":I
    :cond_5
    move v4, v7

    :goto_2
    if-gt v4, v2, :cond_9

    .line 1108
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    invoke-virtual {v8, v4}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v8

    if-nez v8, :cond_7

    .line 1107
    :cond_6
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1110
    :cond_7
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    invoke-virtual {v8, v4}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getObjectRect()Landroid/graphics/Rect;

    move-result-object v8

    invoke-static {p2, v8}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v8

    if-nez v8, :cond_8

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    invoke-virtual {v8, v4}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getObjectRect()Landroid/graphics/Rect;

    move-result-object v8

    invoke-virtual {p2, v8}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1111
    :cond_8
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnPenSelectionListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnPenSelectionListener;

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlGridView$11;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    invoke-virtual {v9, v4}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v9

    iget v9, v9, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    const/4 v10, -0x1

    const/4 v11, 0x0

    invoke-interface {v8, v9, v10, v11}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnPenSelectionListener;->onPenSelection(III)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 1114
    .end local v2    # "end":I
    .end local v3    # "firstItem":I
    .end local v4    # "i":I
    .end local v5    # "lastItem":I
    .end local v7    # "start":I
    :catch_0
    move-exception v1

    .line 1115
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 1117
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :cond_9
    return v12
.end method
