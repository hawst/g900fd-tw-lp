.class public Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;
.super Ljava/lang/Object;
.source "GallerySearchTimeUtils.java"


# instance fields
.field private day:I

.field private month:I

.field private year:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    .line 19
    iput v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    .line 21
    iput v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    .line 24
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 26
    .local v0, "calendar":Ljava/util/Calendar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    .line 27
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    .line 28
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    .line 29
    return-void
.end method


# virtual methods
.method public endOf7DaysNext()J
    .locals 7

    .prologue
    const/16 v5, 0x3b

    .line 257
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 258
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    const/16 v4, 0x17

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 259
    const/4 v1, 0x3

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 261
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public endOfBeforeYear()J
    .locals 7

    .prologue
    const/16 v5, 0x3b

    .line 293
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 294
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    add-int/lit8 v1, v1, -0x2

    const/16 v2, 0xb

    const/16 v3, 0x1f

    const/16 v4, 0x17

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 295
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public endOfNextWeek()J
    .locals 8

    .prologue
    const/16 v5, 0x3b

    const/4 v7, 0x1

    .line 221
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 222
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    const/16 v4, 0x17

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 223
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v7}, Ljava/util/Calendar;->set(II)V

    .line 224
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v7}, Ljava/util/Calendar;->add(II)V

    .line 226
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public endOfPastMonth()J
    .locals 8

    .prologue
    const/16 v7, 0x3b

    const/4 v4, 0x0

    .line 177
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 178
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    add-int/lit8 v2, v2, -0x1

    const/4 v3, 0x1

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 179
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v3

    .line 180
    .local v3, "days":I
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    add-int/lit8 v2, v2, -0x1

    const/16 v4, 0x17

    move v5, v7

    move v6, v7

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 182
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v4

    return-wide v4
.end method

.method public endOfPastWeek()J
    .locals 7

    .prologue
    const/16 v5, 0x3b

    .line 199
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 200
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    const/16 v4, 0x17

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 201
    const/4 v1, 0x7

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 202
    const/4 v1, 0x3

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 204
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public endOfPastYear()J
    .locals 7

    .prologue
    const/16 v5, 0x3b

    .line 241
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 242
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    add-int/lit8 v1, v1, -0x1

    const/16 v2, 0xb

    const/16 v3, 0x1f

    const/16 v4, 0x17

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 243
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public endOfToday()J
    .locals 7

    .prologue
    const/16 v5, 0x3b

    .line 136
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 137
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    const/16 v4, 0x17

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 139
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public endOfTomorrow()J
    .locals 7

    .prologue
    const/16 v5, 0x3b

    .line 150
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 151
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    add-int/lit8 v3, v3, 0x1

    const/16 v4, 0x17

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 153
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public endOfYesterday()J
    .locals 7

    .prologue
    const/16 v5, 0x3b

    .line 122
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 123
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    add-int/lit8 v3, v3, -0x1

    const/16 v4, 0x17

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 125
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public floor3digits(Ljava/lang/Long;)J
    .locals 4
    .param p1, "source"    # Ljava/lang/Long;

    .prologue
    const-wide v2, 0x408f400000000000L    # 1000.0

    .line 110
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-double v0, v0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    double-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    .line 111
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getConvertTimeToDate(Landroid/content/Context;J)Ljava/lang/String;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "milliseconds"    # J

    .prologue
    .line 281
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 282
    .local v1, "dateFormat":Ljava/text/DateFormat;
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p2, p3}, Ljava/util/Date;-><init>(J)V

    .line 283
    .local v0, "date":Ljava/util/Date;
    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public getLastYear()I
    .locals 1

    .prologue
    .line 299
    iget v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public last_24hours()J
    .locals 4

    .prologue
    .line 54
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 55
    .local v0, "c":Ljava/util/Calendar;
    const/4 v1, 0x6

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 57
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method public month_after_N(I)J
    .locals 6
    .param p1, "n"    # I

    .prologue
    const/4 v4, 0x0

    .line 89
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 90
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    add-int/2addr v2, p1

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 92
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method public month_ago()J
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 82
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 83
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 85
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method public now()J
    .locals 4

    .prologue
    .line 32
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 33
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    .line 35
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method public startOf30DaysAgo()J
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 266
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 267
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 269
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public startOf7DaysAgo()J
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 248
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 249
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 250
    const/4 v1, 0x3

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 252
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public startOfAYearAgo()J
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 274
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 275
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 277
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public startOfBeforeYear()J
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 287
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 288
    .local v0, "c":Ljava/util/Calendar;
    const/16 v1, 0x7b2

    const/4 v3, 0x1

    move v4, v2

    move v5, v2

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 289
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public startOfNextWeek()J
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 210
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 211
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 212
    const/4 v1, 0x7

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 213
    const/4 v1, 0x3

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 215
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public startOfPastMonth()J
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 159
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 160
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    add-int/lit8 v2, v2, -0x1

    const/4 v3, 0x1

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 162
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public startOfPastMonths(I)J
    .locals 7
    .param p1, "pastNum"    # I

    .prologue
    const/4 v4, 0x0

    .line 168
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 169
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    sub-int/2addr v2, p1

    const/4 v3, 0x1

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 171
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public startOfPastWeek()J
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 188
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 189
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 190
    const/4 v1, 0x7

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 191
    const/4 v1, 0x3

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 193
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public startOfPastYear()J
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 232
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 233
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    add-int/lit8 v1, v1, -0x1

    const/4 v3, 0x1

    move v4, v2

    move v5, v2

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 235
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public startOfToday()J
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 129
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 130
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 132
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public startOfTomorrow()J
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 143
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 144
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    add-int/lit8 v3, v3, 0x1

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 146
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public startOfYesterday()J
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 115
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 116
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    add-int/lit8 v3, v3, -0x1

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 118
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->floor3digits(Ljava/lang/Long;)J

    move-result-wide v2

    return-wide v2
.end method

.method public threeday()J
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 67
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 68
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    add-int/lit8 v3, v3, -0x3

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 70
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method public today()J
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 39
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 40
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 42
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method public week_ago()J
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 74
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 75
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 76
    const/4 v1, 0x3

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 78
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method public year_ago()J
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 96
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 97
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 99
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method public years_ago()J
    .locals 4

    .prologue
    .line 103
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 104
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    add-int/lit8 v1, v1, -0x5

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    .line 106
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method public yesterday()J
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 61
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 62
    .local v0, "c":Ljava/util/Calendar;
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->year:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->month:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->day:I

    add-int/lit8 v3, v3, -0x1

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 63
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method
