.class public Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
.super Ljava/lang/Object;
.source "RegionDecoder.java"


# instance fields
.field private mBFRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

.field private mSecMMCodecRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

.field private mXIVRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cancelAllNewInstances()V
    .locals 1

    .prologue
    .line 309
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v0, :cond_0

    .line 310
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->cancelAllNewInstances()V

    .line 312
    :cond_0
    return-void
.end method

.method public static getNativeInstance(Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)Ljava/lang/Object;
    .locals 1
    .param p0, "regionDecoder"    # Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .prologue
    .line 315
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mBFRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mBFRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    invoke-static {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->getNativeInstance(Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;)Ljava/lang/Object;

    move-result-object v0

    .line 318
    :goto_0
    return-object v0

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mXIVRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    if-eqz v0, :cond_1

    .line 318
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mXIVRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    invoke-static {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->getNativeInstance(Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 320
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public static isUseRDEx()Z
    .locals 1

    .prologue
    .line 52
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v0, :cond_0

    .line 53
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->isUseRDEx()Z

    move-result v0

    .line 55
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newInstance(Ljava/io/FileDescriptor;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .locals 3
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .param p1, "isShareable"    # Z

    .prologue
    .line 178
    const/4 v0, 0x0

    .line 179
    .local v0, "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecMMCodec:Z

    if-eqz v2, :cond_1

    .line 180
    invoke-static {p0, p1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->newInstance(Ljava/io/FileDescriptor;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    move-result-object v1

    .line 181
    .local v1, "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    if-eqz v1, :cond_0

    .line 182
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .end local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;-><init>()V

    .line 183
    .restart local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mSecMMCodecRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    .line 192
    .end local v1    # "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    :cond_0
    :goto_0
    return-object v0

    .line 186
    :cond_1
    invoke-static {p0, p1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->newInstance(Ljava/io/FileDescriptor;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    move-result-object v1

    .line 187
    .local v1, "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;
    if-eqz v1, :cond_0

    .line 188
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .end local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;-><init>()V

    .line 189
    .restart local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mBFRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    goto :goto_0
.end method

.method public static newInstance(Ljava/io/FileDescriptor;ZLcom/sec/samsung/gallery/decoder/DecoderInterface;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .locals 3
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .param p1, "isShareable"    # Z
    .param p2, "decoderInterface"    # Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    .prologue
    .line 263
    const/4 v0, 0x0

    .line 264
    .local v0, "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v2, :cond_1

    if-eqz p2, :cond_1

    .line 265
    invoke-virtual {p2}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->getXIVInterface()Lcom/sec/samsung/gallery/decoder/XIVInterface;

    move-result-object v2

    invoke-static {p0, p1, v2}, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->newInstance(Ljava/io/FileDescriptor;ZLcom/sec/samsung/gallery/decoder/XIVInterface;)Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    move-result-object v1

    .line 266
    .local v1, "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    if-eqz v1, :cond_0

    .line 267
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .end local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;-><init>()V

    .line 268
    .restart local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mXIVRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    :cond_0
    move-object v2, v0

    .line 273
    .end local v1    # "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    :goto_0
    return-object v2

    .line 271
    :cond_1
    invoke-static {p0, p1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->newInstance(Ljava/io/FileDescriptor;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v2

    goto :goto_0
.end method

.method public static newInstance(Ljava/io/InputStream;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .locals 3
    .param p0, "is"    # Ljava/io/InputStream;
    .param p1, "isShareable"    # Z

    .prologue
    .line 160
    const/4 v0, 0x0

    .line 161
    .local v0, "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecMMCodec:Z

    if-eqz v2, :cond_1

    .line 162
    invoke-static {p0, p1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->newInstance(Ljava/io/InputStream;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    move-result-object v1

    .line 163
    .local v1, "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    if-eqz v1, :cond_0

    .line 164
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .end local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;-><init>()V

    .line 165
    .restart local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mSecMMCodecRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    .line 174
    .end local v1    # "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    :cond_0
    :goto_0
    return-object v0

    .line 168
    :cond_1
    invoke-static {p0, p1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->newInstance(Ljava/io/InputStream;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    move-result-object v1

    .line 169
    .local v1, "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;
    if-eqz v1, :cond_0

    .line 170
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .end local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;-><init>()V

    .line 171
    .restart local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mBFRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    goto :goto_0
.end method

.method public static newInstance(Ljava/io/InputStream;ZLcom/sec/samsung/gallery/decoder/DecoderInterface;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .locals 3
    .param p0, "is"    # Ljava/io/InputStream;
    .param p1, "isShareable"    # Z
    .param p2, "decoderInterface"    # Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    .prologue
    .line 249
    const/4 v0, 0x0

    .line 250
    .local v0, "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v2, :cond_1

    if-eqz p2, :cond_1

    .line 251
    invoke-virtual {p2}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->getXIVInterface()Lcom/sec/samsung/gallery/decoder/XIVInterface;

    move-result-object v2

    invoke-static {p0, p1, v2}, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->newInstance(Ljava/io/InputStream;ZLcom/sec/samsung/gallery/decoder/XIVInterface;)Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    move-result-object v1

    .line 252
    .local v1, "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    if-eqz v1, :cond_0

    .line 253
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .end local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;-><init>()V

    .line 254
    .restart local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mXIVRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    :cond_0
    move-object v2, v0

    .line 259
    .end local v1    # "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    :goto_0
    return-object v2

    .line 257
    :cond_1
    invoke-static {p0, p1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->newInstance(Ljava/io/InputStream;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v2

    goto :goto_0
.end method

.method public static newInstance(Ljava/lang/Object;Lcom/sec/samsung/gallery/decoder/XIVInterface;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .locals 2
    .param p0, "qrd"    # Ljava/lang/Object;
    .param p1, "xivInterface"    # Lcom/sec/samsung/gallery/decoder/XIVInterface;

    .prologue
    .line 292
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;-><init>()V

    .line 293
    .local v0, "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-static {p0, p1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->newInstance(Ljava/lang/Object;Lcom/sec/samsung/gallery/decoder/XIVInterface;)Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mXIVRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    .line 294
    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .locals 3
    .param p0, "pathName"    # Ljava/lang/String;
    .param p1, "isShareable"    # Z

    .prologue
    .line 142
    const/4 v0, 0x0

    .line 143
    .local v0, "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecMMCodec:Z

    if-eqz v2, :cond_1

    .line 144
    invoke-static {p0, p1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->newInstance(Ljava/lang/String;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    move-result-object v1

    .line 145
    .local v1, "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    if-eqz v1, :cond_0

    .line 146
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .end local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;-><init>()V

    .line 147
    .restart local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mSecMMCodecRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    .line 156
    .end local v1    # "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    :cond_0
    :goto_0
    return-object v0

    .line 150
    :cond_1
    invoke-static {p0, p1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->newInstance(Ljava/lang/String;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    move-result-object v1

    .line 151
    .local v1, "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;
    if-eqz v1, :cond_0

    .line 152
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .end local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;-><init>()V

    .line 153
    .restart local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mBFRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    goto :goto_0
.end method

.method public static newInstance(Ljava/lang/String;ZLcom/sec/samsung/gallery/decoder/DecoderInterface;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .locals 3
    .param p0, "pathName"    # Ljava/lang/String;
    .param p1, "isShareable"    # Z
    .param p2, "decoderInterface"    # Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    .prologue
    .line 235
    const/4 v0, 0x0

    .line 236
    .local v0, "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v2, :cond_1

    if-eqz p2, :cond_1

    .line 237
    invoke-virtual {p2}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->getXIVInterface()Lcom/sec/samsung/gallery/decoder/XIVInterface;

    move-result-object v2

    invoke-static {p0, p1, v2}, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->newInstance(Ljava/lang/String;ZLcom/sec/samsung/gallery/decoder/XIVInterface;)Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    move-result-object v1

    .line 238
    .local v1, "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    if-eqz v1, :cond_0

    .line 239
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .end local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;-><init>()V

    .line 240
    .restart local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mXIVRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    :cond_0
    move-object v2, v0

    .line 245
    .end local v1    # "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    :goto_0
    return-object v2

    .line 243
    :cond_1
    invoke-static {p0, p1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->newInstance(Ljava/lang/String;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v2

    goto :goto_0
.end method

.method public static newInstance([BIIZ)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .locals 3
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "isShareable"    # Z

    .prologue
    .line 196
    const/4 v0, 0x0

    .line 197
    .local v0, "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecMMCodec:Z

    if-eqz v2, :cond_1

    .line 198
    invoke-static {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->newInstance([BIIZ)Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    move-result-object v1

    .line 199
    .local v1, "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    if-eqz v1, :cond_0

    .line 200
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .end local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;-><init>()V

    .line 201
    .restart local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mSecMMCodecRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    .line 210
    .end local v1    # "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    :cond_0
    :goto_0
    return-object v0

    .line 204
    :cond_1
    invoke-static {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->newInstance([BIIZ)Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    move-result-object v1

    .line 205
    .local v1, "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;
    if-eqz v1, :cond_0

    .line 206
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .end local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;-><init>()V

    .line 207
    .restart local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mBFRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    goto :goto_0
.end method

.method public static newInstance([BIIZLcom/sec/samsung/gallery/decoder/DecoderInterface;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .locals 3
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "isShareable"    # Z
    .param p4, "decoderInterface"    # Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    .prologue
    .line 277
    const/4 v0, 0x0

    .line 278
    .local v0, "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v2, :cond_1

    if-eqz p4, :cond_1

    .line 279
    invoke-virtual {p4}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->getXIVInterface()Lcom/sec/samsung/gallery/decoder/XIVInterface;

    move-result-object v2

    invoke-static {p0, p1, p2, p3, v2}, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->newInstance([BIIZLcom/sec/samsung/gallery/decoder/XIVInterface;)Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    move-result-object v1

    .line 280
    .local v1, "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    if-eqz v1, :cond_0

    .line 281
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .end local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;-><init>()V

    .line 282
    .restart local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mXIVRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    :cond_0
    move-object v2, v0

    .line 287
    .end local v1    # "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;
    :goto_0
    return-object v2

    .line 285
    :cond_1
    invoke-static {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->newInstance([BIIZ)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v2

    goto :goto_0
.end method

.method public static newInstance([BIIZZ)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .locals 3
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "isShareable"    # Z
    .param p4, "useBitmapFactory"    # Z

    .prologue
    .line 215
    const/4 v0, 0x0

    .line 216
    .local v0, "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecMMCodec:Z

    if-eqz v2, :cond_1

    if-nez p4, :cond_1

    .line 217
    invoke-static {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->newInstance([BIIZ)Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    move-result-object v1

    .line 218
    .local v1, "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    if-eqz v1, :cond_0

    .line 219
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .end local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;-><init>()V

    .line 220
    .restart local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mSecMMCodecRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    .line 229
    .end local v1    # "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;
    :cond_0
    :goto_0
    return-object v0

    .line 223
    :cond_1
    invoke-static {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->newInstance([BIIZ)Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    move-result-object v1

    .line 224
    .local v1, "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;
    if-eqz v1, :cond_0

    .line 225
    new-instance v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .end local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;-><init>()V

    .line 226
    .restart local v0    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mBFRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    goto :goto_0
.end method


# virtual methods
.method public decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "rect"    # Landroid/graphics/Rect;
    .param p2, "options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mXIVRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mXIVRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 44
    :goto_0
    return-object v0

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mSecMMCodecRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    if-eqz v0, :cond_1

    .line 42
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mSecMMCodecRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 43
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mBFRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    if-eqz v0, :cond_2

    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mBFRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 46
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public decodeRegionEx(Landroid/graphics/Rect;Landroid/graphics/Rect;IIII)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "wantRegion"    # Landroid/graphics/Rect;
    .param p2, "intersectRect"    # Landroid/graphics/Rect;
    .param p3, "imageWidth"    # I
    .param p4, "imageHeight"    # I
    .param p5, "tileSize"    # I
    .param p6, "level"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 61
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 62
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v5, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 63
    iput-boolean v1, v2, Landroid/graphics/BitmapFactory$Options;->inPreferQualityOverSpeed:Z

    .line 64
    shl-int v5, v1, p6

    iput v5, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 66
    iget-object v5, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mXIVRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    if-eqz v5, :cond_2

    invoke-static {}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->isUseTileReuseMode()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 67
    if-eqz p2, :cond_1

    .line 68
    invoke-virtual {p2, v3, v3, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 69
    invoke-virtual {p2, p1}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    .line 70
    iget-object v3, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mXIVRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    invoke-virtual {v3, p1, p2, v2}, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->decodeRegionEx(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 99
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v4

    .line 72
    goto :goto_0

    .line 74
    :cond_2
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v3, v3, p3, p4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v5, p1}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 76
    .local v1, "needClear":Z
    :goto_1
    invoke-static {}, Lcom/sec/android/photos/data/GalleryBitmapPool;->getInstance()Lcom/sec/android/photos/data/GalleryBitmapPool;

    move-result-object v5

    invoke-virtual {v5, p5, p5}, Lcom/sec/android/photos/data/GalleryBitmapPool;->get(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 77
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_6

    .line 78
    if-eqz v1, :cond_3

    invoke-virtual {v0, v3}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 83
    :cond_3
    :goto_2
    iput-object v0, v2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 86
    :try_start_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mXIVRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    if-eqz v3, :cond_7

    .line 87
    if-eqz p2, :cond_4

    .line 88
    iget-object v3, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mXIVRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    invoke-virtual {v3, p1, p2, v2}, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->decodeRegionEx(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 94
    :cond_4
    :goto_3
    iget-object v3, v2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    if-eq v3, v0, :cond_0

    iget-object v3, v2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_0

    .line 95
    invoke-static {}, Lcom/sec/android/photos/data/GalleryBitmapPool;->getInstance()Lcom/sec/android/photos/data/GalleryBitmapPool;

    move-result-object v3

    iget-object v5, v2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v5}, Lcom/sec/android/photos/data/GalleryBitmapPool;->put(Landroid/graphics/Bitmap;)Z

    .line 96
    iput-object v4, v2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "needClear":Z
    :cond_5
    move v1, v3

    .line 74
    goto :goto_1

    .line 80
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v1    # "needClear":Z
    :cond_6
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p5, p5, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_2

    .line 91
    :cond_7
    :try_start_1
    invoke-virtual {p0, p1, v2}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_3

    .line 94
    :catchall_0
    move-exception v3

    iget-object v5, v2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    if-eq v5, v0, :cond_8

    iget-object v5, v2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_8

    .line 95
    invoke-static {}, Lcom/sec/android/photos/data/GalleryBitmapPool;->getInstance()Lcom/sec/android/photos/data/GalleryBitmapPool;

    move-result-object v5

    iget-object v6, v2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v6}, Lcom/sec/android/photos/data/GalleryBitmapPool;->put(Landroid/graphics/Bitmap;)Z

    .line 96
    iput-object v4, v2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    :cond_8
    throw v3
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mXIVRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mXIVRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->getHeight()I

    move-result v0

    .line 111
    :goto_0
    return v0

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mSecMMCodecRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    if-eqz v0, :cond_1

    .line 109
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mSecMMCodecRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->getHeight()I

    move-result v0

    goto :goto_0

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mBFRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    if-eqz v0, :cond_2

    .line 111
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mBFRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->getHeight()I

    move-result v0

    goto :goto_0

    .line 113
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mXIVRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mXIVRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->getWidth()I

    move-result v0

    .line 123
    :goto_0
    return v0

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mSecMMCodecRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    if-eqz v0, :cond_1

    .line 121
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mSecMMCodecRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->getWidth()I

    move-result v0

    goto :goto_0

    .line 122
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mBFRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    if-eqz v0, :cond_2

    .line 123
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mBFRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->getWidth()I

    move-result v0

    goto :goto_0

    .line 125
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public final isRecycled()Z
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mXIVRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mXIVRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->isRecycled()Z

    move-result v0

    .line 137
    :goto_0
    return v0

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mSecMMCodecRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    if-eqz v0, :cond_1

    .line 133
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mSecMMCodecRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->isRecycled()Z

    move-result v0

    goto :goto_0

    .line 134
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mBFRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    if-eqz v0, :cond_2

    .line 135
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mBFRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->isRecycled()Z

    move-result v0

    goto :goto_0

    .line 137
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public recycle()V
    .locals 1

    .prologue
    .line 299
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mXIVRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    if-eqz v0, :cond_1

    .line 300
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mXIVRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/XIVRegionDecoder;->recycle()V

    .line 306
    :cond_0
    :goto_0
    return-void

    .line 301
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mSecMMCodecRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    if-eqz v0, :cond_2

    .line 302
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mSecMMCodecRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->recycle()V

    goto :goto_0

    .line 303
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mBFRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    if-eqz v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mBFRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->recycle()V

    goto :goto_0
.end method

.method public requestCanceDecode()V
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mSecMMCodecRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mSecMMCodecRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->requestCancelDecode()V

    .line 328
    :cond_0
    return-void
.end method

.method public useLTN()V
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mSecMMCodecRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->mSecMMCodecRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/SecMMCodecRegionDecoder;->useLTN()I

    .line 334
    :cond_0
    return-void
.end method
