.class Lcom/sec/samsung/gallery/view/AlbumTabBar$7;
.super Ljava/lang/Object;
.source "AlbumTabBar.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/AlbumTabBar;->hideMenues(Landroid/view/Menu;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

.field final synthetic val$menu:Landroid/view/Menu;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/AlbumTabBar;Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 481
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$7;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$7;->val$menu:Landroid/view/Menu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const v3, 0x7f0f0293

    const/4 v4, 0x0

    .line 484
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$7;->val$menu:Landroid/view/Menu;

    const v2, 0x7f0f026f

    invoke-static {v1, v2, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 485
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$7;->val$menu:Landroid/view/Menu;

    invoke-interface {v1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 486
    .local v0, "layoutModelItem":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 487
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$7;->val$menu:Landroid/view/Menu;

    invoke-static {v1, v3, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 489
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$7;->val$menu:Landroid/view/Menu;

    const v2, 0x7f0f028d

    invoke-static {v1, v2, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 490
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$7;->val$menu:Landroid/view/Menu;

    const v2, 0x7f0f0253

    invoke-static {v1, v2, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 491
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$7;->val$menu:Landroid/view/Menu;

    const v2, 0x7f0f028e

    invoke-static {v1, v2, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 492
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$7;->val$menu:Landroid/view/Menu;

    const v2, 0x7f0f028f

    invoke-static {v1, v2, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 493
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$7;->val$menu:Landroid/view/Menu;

    const v2, 0x7f0f0257

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSettings:Z

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 494
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$7;->val$menu:Landroid/view/Menu;

    const v2, 0x7f0f0290

    invoke-static {v1, v2, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 495
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$7;->val$menu:Landroid/view/Menu;

    const v2, 0x7f0f0291

    invoke-static {v1, v2, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 496
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$7;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eq v1, v2, :cond_1

    .line 497
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$7;->val$menu:Landroid/view/Menu;

    const v2, 0x7f0f0289

    invoke-static {v1, v2, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 499
    :cond_1
    return-void
.end method
