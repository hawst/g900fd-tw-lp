.class public Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;
.super Ljava/lang/Object;
.source "FindNewEvent.java"


# static fields
.field private static final BASE_IMAGE_URI:Landroid/net/Uri;

.field private static final BASE_VIDEO_URI:Landroid/net/Uri;

.field private static final IMAGE_PATH:Ljava/lang/String; = "/local/image/item"

.field public static final LOCK:Ljava/lang/Object;

.field private static final TAG:Ljava/lang/String; = "FindNewEvent"

.field private static final VIDEO_PATH:Ljava/lang/String; = "/local/video/item"

.field private static sInstance:Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;


# instance fields
.field private final mComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private mFindNewEventThread:Ljava/lang/Thread;

.field private mImageQuery:[Ljava/lang/String;

.field private mLastCheckTime:Ljava/lang/Long;

.field private mLatestCheckTime:Ljava/lang/Long;

.field private mMediaItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private mNeedStop:Z

.field private mVideoQuery:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->LOCK:Ljava/lang/Object;

    .line 54
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->BASE_IMAGE_URI:Landroid/net/Uri;

    .line 55
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->BASE_VIDEO_URI:Landroid/net/Uri;

    .line 71
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->sInstance:Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "bucket_id = ? AND datetaken > ?"

    aput-object v1, v0, v2

    const-string v1, "datetaken ASC, _id ASC"

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mImageQuery:[Ljava/lang/String;

    .line 61
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "bucket_id = ? AND datetaken > ?"

    aput-object v1, v0, v2

    const-string v1, "datetaken ASC, _id ASC"

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mVideoQuery:[Ljava/lang/String;

    .line 66
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mLastCheckTime:Ljava/lang/Long;

    .line 67
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mLatestCheckTime:Ljava/lang/Long;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mMediaItems:Ljava/util/ArrayList;

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mFindNewEventThread:Ljava/lang/Thread;

    .line 214
    new-instance v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent$2;-><init>(Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mComparator:Ljava/util/Comparator;

    .line 74
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mContext:Landroid/content/Context;

    .line 75
    return-void
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;)Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;

    .prologue
    .line 46
    sput-object p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->sInstance:Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;

    return-object p0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 78
    const-class v1, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;

    monitor-enter v1

    :try_start_0
    sget-object v2, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->LOCK:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 79
    :try_start_1
    sget-object v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->sInstance:Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;

    if-nez v0, :cond_0

    if-eqz p0, :cond_0

    .line 80
    new-instance v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->sInstance:Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;

    .line 82
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->sInstance:Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0

    .line 83
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 78
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private loadItemEntry(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/net/Uri;JZ)V
    .locals 11
    .param p1, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "bucketId"    # J
    .param p5, "isImage"    # Z

    .prologue
    .line 176
    if-eqz p5, :cond_0

    .line 177
    sget-object v2, Lcom/sec/android/gallery3d/data/LocalImage;->PROJECTION:[Ljava/lang/String;

    .line 178
    .local v2, "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mImageQuery:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v3, v0, v1

    .line 179
    .local v3, "whereClause":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mImageQuery:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v5, v0, v1

    .line 180
    .local v5, "orderClause":Ljava/lang/String;
    const-string v0, "/local/image/item"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v9

    .line 188
    .local v9, "path":Lcom/sec/android/gallery3d/data/Path;
    :goto_0
    const/4 v6, 0x0

    .line 190
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v4, v1

    const/4 v1, 0x1

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mLastCheckTime:Ljava/lang/Long;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v4, v1

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 193
    if-nez v6, :cond_1

    .line 194
    const-string v0, "FindNewEvent"

    const-string v1, "query fail: cursor is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 212
    :goto_1
    return-void

    .line 182
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "whereClause":Ljava/lang/String;
    .end local v5    # "orderClause":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v9    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_0
    sget-object v2, Lcom/sec/android/gallery3d/data/LocalVideo;->PROJECTION:[Ljava/lang/String;

    .line 183
    .restart local v2    # "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mVideoQuery:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v3, v0, v1

    .line 184
    .restart local v3    # "whereClause":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mVideoQuery:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v5, v0, v1

    .line 185
    .restart local v5    # "orderClause":Ljava/lang/String;
    const-string v0, "/local/video/item"

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v9

    .restart local v9    # "path":Lcom/sec/android/gallery3d/data/Path;
    goto :goto_0

    .line 197
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_1
    if-eqz v6, :cond_3

    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 199
    :cond_2
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 200
    .local v8, "id":I
    if-eqz p5, :cond_4

    .line 201
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mMediaItems:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v9, v8}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-direct {v1, v4, p1, v6}, Lcom/sec/android/gallery3d/data/LocalImage;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 204
    :goto_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 210
    .end local v8    # "id":I
    :cond_3
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .line 203
    .restart local v8    # "id":I
    :cond_4
    :try_start_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mMediaItems:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/gallery3d/data/LocalVideo;

    invoke-virtual {v9, v8}, Lcom/sec/android/gallery3d/data/Path;->getChild(I)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-direct {v1, v4, p1, v6}, Lcom/sec/android/gallery3d/data/LocalVideo;-><init>(Lcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/database/Cursor;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 206
    .end local v8    # "id":I
    :catch_0
    move-exception v7

    .line 207
    .local v7, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v0, "FindNewEvent"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadItemEntry "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 210
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method


# virtual methods
.method public needStop()Z
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mNeedStop:Z

    return v0
.end method

.method public setLatestCheckTime(J)V
    .locals 1
    .param p1, "latestCheckTime"    # J

    .prologue
    .line 116
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mLatestCheckTime:Ljava/lang/Long;

    .line 117
    return-void
.end method

.method public startFindNewEvent()V
    .locals 3

    .prologue
    .line 87
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEventView:Z

    if-nez v0, :cond_1

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mFindNewEventThread:Ljava/lang/Thread;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mFindNewEventThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mNeedStop:Z

    .line 92
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent$1;-><init>(Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;)V

    const-string v2, "FindNewEventThread"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mFindNewEventThread:Ljava/lang/Thread;

    .line 102
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mFindNewEventThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public stopFindNewEvent()V
    .locals 1

    .prologue
    .line 106
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEventView:Z

    if-nez v0, :cond_0

    .line 109
    :goto_0
    return-void

    .line 108
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mNeedStop:Z

    goto :goto_0
.end method

.method public update()V
    .locals 18

    .prologue
    .line 120
    const-string v3, "FindNewEvent"

    const-string v5, "FindNewEvent start"

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v4

    .line 122
    .local v4, "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v4}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/EventAlbumManager;

    move-result-object v14

    .line 123
    .local v14, "mgr":Lcom/sec/android/gallery3d/data/EventAlbumManager;
    const/16 v3, 0xa

    invoke-static {v3}, Landroid/os/Process;->setThreadPriority(I)V

    .line 125
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mContext:Landroid/content/Context;

    invoke-virtual {v14, v3}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->updateCurLocality(Landroid/content/Context;)V

    .line 126
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mContext:Landroid/content/Context;

    const-string v5, "EventInfo"

    const-wide/16 v6, 0x0

    invoke-static {v3, v5, v6, v7}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadLongKey(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mLastCheckTime:Ljava/lang/Long;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mLatestCheckTime:Ljava/lang/Long;

    .line 127
    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getExpiredTime()J

    move-result-wide v10

    .line 128
    .local v10, "expiredTime":J
    const/4 v12, 0x1

    .line 129
    .local v12, "isfinished":Z
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mLastCheckTime:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v3, v6, v10

    if-gez v3, :cond_0

    .line 130
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mLastCheckTime:Ljava/lang/Long;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mLatestCheckTime:Ljava/lang/Long;

    .line 132
    :cond_0
    const/4 v3, 0x1

    const-string v5, "images_event_album"

    invoke-virtual {v14, v3, v5}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->updateLocalDB(ZLjava/lang/String;)Ljava/util/List;

    .line 133
    const/4 v3, 0x0

    const-string/jumbo v5, "video_event_album"

    invoke-virtual {v14, v3, v5}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->updateLocalDB(ZLjava/lang/String;)Ljava/util/List;

    .line 135
    sget-object v5, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->BASE_IMAGE_URI:Landroid/net/Uri;

    sget v3, Lcom/sec/android/gallery3d/util/MediaSetUtils;->CAMERA_BUCKET_ID:I

    int-to-long v6, v3

    const/4 v8, 0x1

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->loadItemEntry(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/net/Uri;JZ)V

    .line 136
    sget-object v5, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->BASE_IMAGE_URI:Landroid/net/Uri;

    sget v3, Lcom/sec/android/gallery3d/util/MediaSetUtils;->SDCARD_CAMERA_BUCKET_ID:I

    int-to-long v6, v3

    const/4 v8, 0x1

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->loadItemEntry(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/net/Uri;JZ)V

    .line 137
    sget-object v5, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->BASE_VIDEO_URI:Landroid/net/Uri;

    sget v3, Lcom/sec/android/gallery3d/util/MediaSetUtils;->CAMERA_BUCKET_ID:I

    int-to-long v6, v3

    const/4 v8, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->loadItemEntry(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/net/Uri;JZ)V

    .line 138
    sget-object v5, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->BASE_VIDEO_URI:Landroid/net/Uri;

    sget v3, Lcom/sec/android/gallery3d/util/MediaSetUtils;->SDCARD_CAMERA_BUCKET_ID:I

    int-to-long v6, v3

    const/4 v8, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->loadItemEntry(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/net/Uri;JZ)V

    .line 139
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_5

    .line 140
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mMediaItems:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mComparator:Ljava/util/Comparator;

    invoke-static {v3, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 141
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 142
    .local v13, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_5

    .line 143
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/16 v5, 0x64

    if-le v3, v5, :cond_2

    const/16 v15, 0x64

    .line 144
    .local v15, "size":I
    :goto_1
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_2
    if-ge v9, v15, :cond_3

    .line 145
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mLatestCheckTime:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v16

    cmp-long v3, v6, v16

    if-gez v3, :cond_1

    .line 147
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mLatestCheckTime:Ljava/lang/Long;

    .line 144
    :cond_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 143
    .end local v9    # "i":I
    .end local v15    # "size":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v15

    goto :goto_1

    .line 149
    .restart local v9    # "i":I
    .restart local v15    # "size":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mContext:Landroid/content/Context;

    invoke-virtual {v14, v3, v13}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->addSuggestEventInfo(Landroid/content/Context;Ljava/util/ArrayList;)I

    move-result v3

    if-lez v3, :cond_4

    .line 150
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mContext:Landroid/content/Context;

    const-string v5, "EventInfo"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mLatestCheckTime:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v3, v5, v6, v7}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;J)V

    .line 155
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v13}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 156
    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 162
    .end local v9    # "i":I
    .end local v10    # "expiredTime":J
    .end local v12    # "isfinished":Z
    .end local v13    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v15    # "size":I
    :catch_0
    move-exception v2

    .line 163
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "FindNewEvent"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FindNewEvent "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 165
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 166
    const-string v3, "FindNewEvent"

    const-string v5, "FindNewEvent end"

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_3
    return-void

    .line 152
    .restart local v9    # "i":I
    .restart local v10    # "expiredTime":J
    .restart local v12    # "isfinished":Z
    .restart local v13    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .restart local v15    # "size":I
    :cond_4
    const/4 v12, 0x0

    .line 159
    .end local v9    # "i":I
    .end local v13    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v15    # "size":I
    :cond_5
    if-eqz v12, :cond_6

    .line 160
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mContext:Landroid/content/Context;

    invoke-virtual {v14, v3}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->updateSuggestAlbum(Landroid/content/Context;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 165
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 166
    const-string v3, "FindNewEvent"

    const-string v5, "FindNewEvent end"

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 165
    .end local v10    # "expiredTime":J
    .end local v12    # "isfinished":Z
    :catchall_0
    move-exception v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/eventview/FindNewEvent;->mMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 166
    const-string v5, "FindNewEvent"

    const-string v6, "FindNewEvent end"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    throw v3
.end method
