.class public final Lcom/sec/samsung/gallery/access/dcm/DCMInterface$SubCategoryType;
.super Ljava/lang/Object;
.source "DCMInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/access/dcm/DCMInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SubCategoryType"
.end annotation


# static fields
.field public static final AIRPLANES:Ljava/lang/String; = "Airplanes"

.field public static final BIKES:Ljava/lang/String; = "Bikes"

.field public static final BOATS:Ljava/lang/String; = "Boats"

.field public static final BUILDING:Ljava/lang/String; = "Building"

.field public static final CARS:Ljava/lang/String; = "Cars"

.field public static final CATS:Ljava/lang/String; = "Cats"

.field public static final CITY_OVERLOOK:Ljava/lang/String; = "City overlook"

.field public static final COAST:Ljava/lang/String; = "Coast"

.field public static final DOGS:Ljava/lang/String; = "Dogs"

.field public static final FOREST:Ljava/lang/String; = "Forest"

.field public static final MOUNTAIN:Ljava/lang/String; = "Mountain"

.field public static final NIGHT_SCENE:Ljava/lang/String; = "Night scene"

.field public static final OPEN_COUNTRY:Ljava/lang/String; = "Open country"

.field public static final SNOW:Ljava/lang/String; = "Snow"

.field public static final STREET:Ljava/lang/String; = "Street"

.field public static final TRAINS:Ljava/lang/String; = "Trains"

.field public static final WATERFALL:Ljava/lang/String; = "Waterfall"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
