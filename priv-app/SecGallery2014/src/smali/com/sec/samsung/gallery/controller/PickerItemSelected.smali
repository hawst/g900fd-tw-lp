.class public Lcom/sec/samsung/gallery/controller/PickerItemSelected;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "PickerItemSelected.java"


# static fields
.field private static final IMAGE_TYPE:Ljava/lang/String; = "image/*"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mBackupObject:Lcom/sec/android/gallery3d/data/MediaObject;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mBackupObject:Lcom/sec/android/gallery3d/data/MediaObject;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/PickerItemSelected;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/PickerItemSelected;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/PickerItemSelected;Landroid/content/Intent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/PickerItemSelected;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->needsCrop(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/controller/PickerItemSelected;Lcom/sec/android/gallery3d/data/MediaObject;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/PickerItemSelected;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaObject;
    .param p2, "x2"    # Landroid/content/Intent;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->startCropActivity(Lcom/sec/android/gallery3d/data/MediaObject;Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/controller/PickerItemSelected;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/PickerItemSelected;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->setResultAndFinish(Lcom/sec/android/gallery3d/data/MediaItem;)V

    return-void
.end method

.method private isPickSupported(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 12
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const-wide/16 v10, 0x20

    const/4 v3, 0x1

    const v8, 0x7f0e0107

    const/4 v2, 0x0

    .line 75
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v4

    if-nez v4, :cond_0

    .line 76
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    invoke-static {v3, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 139
    :goto_0
    return v2

    .line 81
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "set-as-wallpaper"

    invoke-virtual {v4, v5, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_1

    .line 82
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    invoke-static {v3, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 86
    :cond_1
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v4, :cond_3

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 87
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "set-as-wallpaper"

    invoke-virtual {v4, v5, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "set-as-contactphoto"

    invoke-virtual {v4, v5, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "set-as-image"

    invoke-virtual {v4, v5, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "fromMusicPlayer"

    invoke-virtual {v4, v5, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    const-string v5, "private_move_do_not_show"

    invoke-static {v4, v5, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_3

    .line 93
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 94
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030022

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 96
    .local v0, "checkView":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    new-instance v4, Lcom/sec/samsung/gallery/controller/PickerItemSelected$1;

    invoke-direct {v4, p0, v0, p1}, Lcom/sec/samsung/gallery/controller/PickerItemSelected$1;-><init>(Lcom/sec/samsung/gallery/controller/PickerItemSelected;Landroid/view/View;Lcom/sec/android/gallery3d/data/MediaItem;)V

    invoke-virtual {v3, v4}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 123
    .end local v0    # "checkView":Landroid/view/View;
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v4

    const-wide/16 v6, 0x4

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_6

    .line 124
    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->isCloudImage(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 125
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "set-as-wallpaper"

    invoke-virtual {v4, v5, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_6

    .line 126
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    invoke-static {v3, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 130
    :cond_4
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v4

    and-long/2addr v4, v10

    cmp-long v4, v4, v10

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "set-as-wallpaper"

    invoke-virtual {v4, v5, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_5

    move v2, v3

    .line 133
    goto/16 :goto_0

    .line 135
    :cond_5
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    invoke-static {v3, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto/16 :goto_0

    :cond_6
    move v2, v3

    .line 139
    goto/16 :goto_0
.end method

.method private needsCrop(Landroid/content/Intent;)Z
    .locals 1
    .param p1, "requestIntent"    # Landroid/content/Intent;

    .prologue
    .line 201
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "crop"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setResultAndFinish(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 7
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v5, -0x1

    .line 143
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    if-nez v3, :cond_0

    .line 144
    sget-object v3, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->TAG:Ljava/lang/String;

    const-string v4, "mActivity is null at setResultAndFinish"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    :goto_0
    return-void

    .line 148
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 149
    .local v0, "responseIntent":Landroid/content/Intent;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    .line 150
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 151
    .local v1, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    const-string v3, "mimeType"

    const-string v4, "image/*"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 153
    const-string v3, "selectedItems"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 156
    const-string v3, "selectedCount"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 160
    .end local v1    # "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_1
    :goto_1
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWallpaperCropNavigation:Z

    if-eqz v3, :cond_4

    .line 161
    const/4 v2, -0x1

    .line 162
    .local v2, "wallpaperType":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 163
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "wallpaper_type"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 165
    :cond_2
    if-eq v2, v5, :cond_4

    .line 166
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    const-string v5, "image/*"

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v2, v6}, Lcom/sec/samsung/gallery/controller/CropImageCmd;->startCropImageActivity(Landroid/app/Activity;Landroid/net/Uri;Ljava/lang/String;IZ)V

    goto :goto_0

    .line 157
    .end local v2    # "wallpaperType":I
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    .line 158
    const-string v3, "mimeType"

    const-string/jumbo v4, "video/*"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 170
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3, v5, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 171
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method private setResultAndFinish(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 3
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 175
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 176
    .local v0, "responseIntent":Landroid/content/Intent;
    const-string v1, "ALBUM_PATH"

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 177
    const-string v1, "mimeType"

    const-string v2, "image/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 178
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 179
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 180
    return-void
.end method

.method private startCropActivity(Lcom/sec/android/gallery3d/data/MediaObject;Landroid/content/Intent;)V
    .locals 4
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaObject;
    .param p2, "requestIntent"    # Landroid/content/Intent;

    .prologue
    .line 183
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.camera.action.CROP"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    const-string v3, "image/*"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    const-class v3, Lcom/sec/android/gallery3d/app/CropImage;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 187
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "output"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_0

    .line 188
    const-string v1, "return-data"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 191
    :cond_0
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWallpaperCropNavigation:Z

    if-eqz v1, :cond_1

    .line 192
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    const/16 v2, 0x403

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 198
    :goto_0
    return-void

    .line 194
    :cond_1
    const/high16 v1, 0x2000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 195
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 196
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 6
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 41
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/Object;

    move-object v3, v5

    check-cast v3, [Ljava/lang/Object;

    .line 42
    .local v3, "params":[Ljava/lang/Object;
    const/4 v5, 0x0

    aget-object v5, v3, v5

    check-cast v5, Landroid/app/Activity;

    iput-object v5, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    .line 43
    const/4 v5, 0x1

    aget-object v1, v3, v5

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 44
    .local v1, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v1, :cond_1

    .line 46
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mBackupObject:Lcom/sec/android/gallery3d/data/MediaObject;

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    instance-of v5, v1, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v5, :cond_2

    move-object v2, v1

    .line 51
    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 52
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->setResultAndFinish(Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_0

    .line 53
    .end local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_2
    instance-of v5, v1, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v5, :cond_0

    move-object v0, v1

    .line 54
    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 55
    .local v0, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->isPickSupported(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 58
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 59
    .local v4, "requestIntent":Landroid/content/Intent;
    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->needsCrop(Landroid/content/Intent;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 67
    invoke-direct {p0, v0, v4}, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->startCropActivity(Lcom/sec/android/gallery3d/data/MediaObject;Landroid/content/Intent;)V

    goto :goto_0

    .line 69
    :cond_3
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/controller/PickerItemSelected;->setResultAndFinish(Lcom/sec/android/gallery3d/data/MediaItem;)V

    goto :goto_0
.end method
