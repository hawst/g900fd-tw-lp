.class Lcom/sec/samsung/gallery/glview/GlGridView$7;
.super Ljava/lang/Object;
.source "GlGridView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGridView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V
    .locals 0

    .prologue
    .line 987
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrop(Lcom/sec/android/gallery3d/glcore/GlObject;Landroid/net/Uri;)Z
    .locals 4
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v3, 0x0

    .line 991
    move-object v1, p1

    check-cast v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget v0, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 993
    .local v0, "index":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 995
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnDropItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnDropItemClickListener;

    if-eqz v1, :cond_1

    if-ltz v0, :cond_1

    .line 996
    iget-boolean v1, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->isLacalDrag:Z

    if-nez v1, :cond_0

    .line 997
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mFocused:I
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$502(Lcom/sec/samsung/gallery/glview/GlGridView;I)I

    .line 999
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    const v2, 0x7f0e02e4

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 1001
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v0, v3, v3}, Lcom/sec/samsung/gallery/glview/GlGridView;->setCommand(IIII)V

    .line 1003
    :cond_1
    const/4 v1, 0x1

    return v1
.end method
