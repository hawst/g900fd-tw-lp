.class public Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;
.super Landroid/widget/LinearLayout;
.source "GallerySearchSelectedItemListView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;

.field protected mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

.field private mHorizontalSpacing:I

.field protected mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 26
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;

    .line 29
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mHorizontalSpacing:I

    .line 36
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->init()V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;

    .line 29
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mHorizontalSpacing:I

    .line 42
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->init()V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;

    .line 29
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mHorizontalSpacing:I

    .line 48
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->init()V

    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mHorizontalSpacing:I

    return v0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d034f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mHorizontalSpacing:I

    .line 55
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;

    .line 56
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 57
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->getAdapter()Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView$1;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 156
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 174
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 175
    .local v1, "parent":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 176
    .local v0, "data":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v2, v0}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->removeSelectedItem(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)V

    .line 177
    return-void
.end method

.method public removeView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 193
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 195
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 196
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->setVisibility(I)V

    .line 198
    :cond_0
    return-void
.end method

.method public resetData()V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->resetData()V

    .line 168
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->notifyDataSetChanged()V

    .line 170
    :cond_0
    return-void
.end method

.method public setData(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 159
    .local p1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->setData(Ljava/util/ArrayList;)V

    .line 161
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->notifyDataSetChanged()V

    .line 163
    :cond_0
    return-void
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 183
    .local v0, "parent":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 184
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 189
    :goto_0
    return-void

    .line 188
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method
