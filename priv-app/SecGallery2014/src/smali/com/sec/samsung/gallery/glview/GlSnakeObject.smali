.class public Lcom/sec/samsung/gallery/glview/GlSnakeObject;
.super Lcom/sec/samsung/gallery/glview/GlThumbObject;
.source "GlSnakeObject.java"


# instance fields
.field private mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

.field mSegNext:Lcom/sec/samsung/gallery/glview/GlSnakeObject;

.field mSegPre:Lcom/sec/samsung/gallery/glview/GlSnakeObject;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFI)V
    .locals 0
    .param p1, "baseLayer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "width"    # F
    .param p3, "height"    # F
    .param p4, "textureSize"    # I

    .prologue
    .line 14
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/glview/GlThumbObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFI)V

    .line 15
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFII)V
    .locals 0
    .param p1, "baseLayer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "width"    # F
    .param p3, "height"    # F
    .param p4, "textureWidth"    # I
    .param p5, "textureHeight"    # I

    .prologue
    .line 18
    invoke-direct/range {p0 .. p5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFII)V

    .line 19
    return-void
.end method

.method public static linkSnake(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/glview/GlSnakeObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "objs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/glview/GlSnakeObject;>;"
    if-nez p0, :cond_1

    .line 30
    :cond_0
    return-void

    .line 24
    :cond_1
    const/4 v1, 0x1

    .local v1, "i":I
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 25
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    .line 26
    .local v0, "cur":Lcom/sec/samsung/gallery/glview/GlSnakeObject;
    add-int/lit8 v4, v1, -0x1

    invoke-virtual {p0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    .line 27
    .local v3, "pre":Lcom/sec/samsung/gallery/glview/GlSnakeObject;
    if-eqz v3, :cond_2

    iput-object v0, v3, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mSegNext:Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    .line 28
    :cond_2
    if-eqz v0, :cond_3

    iput-object v3, v0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mSegPre:Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    .line 24
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static linkSnake([Lcom/sec/samsung/gallery/glview/GlSnakeObject;)V
    .locals 5
    .param p0, "objs"    # [Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    .prologue
    .line 33
    if-nez p0, :cond_1

    .line 41
    :cond_0
    return-void

    .line 35
    :cond_1
    const/4 v1, 0x1

    .local v1, "i":I
    array-length v2, p0

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 36
    aget-object v0, p0, v1

    .line 37
    .local v0, "cur":Lcom/sec/samsung/gallery/glview/GlSnakeObject;
    add-int/lit8 v4, v1, -0x1

    aget-object v3, p0, v4

    .line 38
    .local v3, "pre":Lcom/sec/samsung/gallery/glview/GlSnakeObject;
    if-eqz v3, :cond_2

    iput-object v0, v3, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mSegNext:Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    .line 39
    :cond_2
    if-eqz v0, :cond_3

    iput-object v3, v0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mSegPre:Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    .line 35
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getSnakeLatency()I
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    array-length v0, v0

    goto :goto_0
.end method

.method public setSnakeLatency(I)V
    .locals 5
    .param p1, "latency"    # I

    .prologue
    .line 44
    if-gez p1, :cond_1

    .line 50
    :cond_0
    return-void

    .line 46
    :cond_1
    new-array v1, p1, [Lcom/sec/android/gallery3d/glcore/GlPos2D;

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    .line 47
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 48
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    new-instance v2, Lcom/sec/android/gallery3d/glcore/GlPos2D;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->getX()F

    move-result v3

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->getY()F

    move-result v4

    invoke-direct {v2, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlPos2D;-><init>(FF)V

    aput-object v2, v1, v0

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public updateInitPosition()V
    .locals 3

    .prologue
    .line 57
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    if-nez v1, :cond_1

    .line 65
    :cond_0
    return-void

    .line 61
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 62
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    aget-object v1, v1, v0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->getX()F

    move-result v2

    iput v2, v1, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mX:F

    .line 63
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    aget-object v1, v1, v0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->getY()F

    move-result v2

    iput v2, v1, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mY:F

    .line 61
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public updateSnake(FF)V
    .locals 8
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 68
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    array-length v2, v2

    if-nez v2, :cond_3

    .line 69
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mSegNext:Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    if-eqz v2, :cond_1

    .line 70
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mSegNext:Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->getX()F

    move-result v3

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->getY()F

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->updateSnake(FF)V

    .line 72
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mSegPre:Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    if-eqz v2, :cond_2

    .line 73
    invoke-virtual {p0, p1, p2, v5, v7}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setPos(FFFI)V

    .line 90
    :cond_2
    :goto_0
    return-void

    .line 75
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mSegNext:Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    if-eqz v2, :cond_4

    .line 76
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    aget-object v1, v2, v3

    .line 77
    .local v1, "lastPos":Lcom/sec/android/gallery3d/glcore/GlPos2D;
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mSegNext:Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    iget v3, v1, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mX:F

    iget v4, v1, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mY:F

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->updateSnake(FF)V

    .line 80
    .end local v1    # "lastPos":Lcom/sec/android/gallery3d/glcore/GlPos2D;
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    array-length v2, v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_1
    if-lez v0, :cond_5

    .line 81
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    add-int/lit8 v4, v0, -0x1

    aget-object v3, v3, v4

    iget v3, v3, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mX:F

    iput v3, v2, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mX:F

    .line 82
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    add-int/lit8 v4, v0, -0x1

    aget-object v3, v3, v4

    iget v3, v3, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mY:F

    iput v3, v2, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mY:F

    .line 80
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 85
    :cond_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    aget-object v2, v2, v6

    iput p1, v2, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mX:F

    .line 86
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    aget-object v2, v2, v6

    iput p2, v2, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mY:F

    .line 87
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mSegPre:Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    if-eqz v2, :cond_2

    .line 88
    invoke-virtual {p0, p1, p2, v5, v7}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setPos(FFFI)V

    goto :goto_0
.end method
