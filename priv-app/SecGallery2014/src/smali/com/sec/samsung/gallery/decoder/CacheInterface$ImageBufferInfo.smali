.class public Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;
.super Ljava/lang/Object;
.source "CacheInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/decoder/CacheInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ImageBufferInfo"
.end annotation


# instance fields
.field public buffer:Ljava/nio/ByteBuffer;

.field public height:I

.field public width:I


# direct methods
.method public constructor <init>(IILjava/nio/ByteBuffer;)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "buffer"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    iput p1, p0, Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;->width:I

    .line 127
    iput p2, p0, Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;->height:I

    .line 128
    iput-object p3, p0, Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;->buffer:Ljava/nio/ByteBuffer;

    .line 129
    return-void
.end method

.method public constructor <init>(Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;)V
    .locals 1
    .param p1, "ibd"    # Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    iget v0, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->width:I

    iput v0, p0, Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;->width:I

    .line 133
    iget v0, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->height:I

    iput v0, p0, Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;->height:I

    .line 134
    iget-object v0, p1, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;->buffer:Ljava/nio/ByteBuffer;

    .line 135
    return-void
.end method
