.class Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$9;
.super Ljava/lang/Object;
.source "AlbumViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V
    .locals 0

    .prologue
    .line 603
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$9;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGenericMotionCancel(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 3
    .param p1, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    const/4 v2, -0x1

    .line 618
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$9;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getGenericMotionFocus()I

    move-result v0

    .line 619
    .local v0, "oldFocusedIndex":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$9;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-virtual {v1, v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->updateBorder(II)V

    .line 620
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$9;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->setGenericMotionFocus(I)V

    .line 621
    return-void
.end method

.method public onGenericMotionEnter(Lcom/sec/android/gallery3d/glcore/GlLayer;I)V
    .locals 4
    .param p1, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "position"    # I

    .prologue
    .line 606
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$9;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getGenericMotionFocus()I

    move-result v0

    .line 607
    .local v0, "oldFocusedIndex":I
    if-ne v0, p2, :cond_1

    .line 614
    :cond_0
    :goto_0
    return-void

    .line 609
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$9;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-virtual {v1, v0, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->updateBorder(II)V

    .line 610
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$9;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    if-eqz v1, :cond_0

    .line 611
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$9;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v1, p2}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->setGenericMotionFocus(I)V

    .line 612
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$9;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$9;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1, v2, p2, v3}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(Ljava/lang/Object;ILcom/sec/android/gallery3d/ui/SelectionManager;)V

    goto :goto_0
.end method
