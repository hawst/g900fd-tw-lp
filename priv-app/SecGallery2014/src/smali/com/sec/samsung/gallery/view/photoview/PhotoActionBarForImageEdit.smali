.class Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForImageEdit;
.super Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;
.source "PhotoActionBarForImageEdit.java"


# instance fields
.field private mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 24
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForImageEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForImageEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 25
    return-void
.end method


# virtual methods
.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    const/4 v3, 0x1

    .line 50
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 68
    :goto_0
    return-void

    .line 52
    :pswitch_0
    iget v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForImageEdit;->mCount:I

    if-le v1, v4, :cond_0

    .line 53
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForImageEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0114

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 55
    .local v0, "text":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForImageEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1, v0}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 59
    .end local v0    # "text":Ljava/lang/String;
    :cond_0
    iget v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForImageEdit;->mCount:I

    if-ge v1, v3, :cond_1

    .line 60
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForImageEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0116

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 62
    .restart local v0    # "text":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForImageEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1, v0}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 66
    .end local v0    # "text":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForImageEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->imageEdit()V

    goto :goto_0

    .line 50
    :pswitch_data_0
    .packed-switch 0x7f0f0024
        :pswitch_0
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v6, 0x7f0f0024

    const/4 v5, 0x0

    .line 29
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 30
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForImageEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v3

    .line 31
    .local v3, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v2, 0x0

    .line 32
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v0, 0x1

    .line 33
    .local v0, "bShowDone":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 34
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 35
    .restart local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v2, :cond_1

    .line 36
    invoke-static {p1, v6, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 46
    :cond_0
    :goto_1
    return-void

    .line 40
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForImageEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v4, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->isAvailableProceedEditAndCollage(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v0

    .line 41
    if-nez v0, :cond_2

    .line 42
    invoke-static {p1, v6, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_1

    .line 33
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
