.class Lcom/sec/samsung/gallery/glview/GlPhotoView$23;
.super Ljava/lang/Object;
.source "GlPhotoView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;->hideBgAndArrow(F)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 0

    .prologue
    .line 3786
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$23;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 2
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    const/4 v1, 0x0

    .line 3794
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$23;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3800(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->setVisibility(Z)V

    .line 3795
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFixScrollingAlbum:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$23;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$23;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3600(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3796
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$23;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setVisibility(Z)V

    .line 3797
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$23;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3600(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(Z)V

    .line 3799
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 0
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 3803
    return-void
.end method

.method public onAnimationStart(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 0
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 3790
    return-void
.end method
