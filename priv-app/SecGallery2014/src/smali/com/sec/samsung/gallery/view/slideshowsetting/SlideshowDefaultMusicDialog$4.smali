.class Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$4;
.super Ljava/lang/Object;
.source "SlideshowDefaultMusicDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$4;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 220
    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 221
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 222
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$4;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    # invokes: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->checkedItemPosition()I
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$500(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)I

    move-result v0

    if-nez v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$4;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$300(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v2, "SLIDESHOW_SETUP_MUSIC"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    move v0, v1

    .line 228
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
