.class public abstract Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelper;
.super Ljava/lang/Object;
.source "ViewHelperFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ViewHelper"
.end annotation


# instance fields
.field protected view:Landroid/view/View;


# direct methods
.method protected constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelper;->view:Landroid/view/View;

    .line 18
    const-string v0, "ViewHelper"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 19
    return-void
.end method


# virtual methods
.method public abstract isHardwareAccelerated()Z
.end method

.method public abstract postOnAnimation(Ljava/lang/Runnable;)V
.end method

.method public abstract setScrollX(I)V
.end method
