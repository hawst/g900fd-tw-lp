.class public Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;
.super Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
.source "PositionControllerPhoto.java"


# static fields
.field private static ALBUM_TITLE_HRATIO:F

.field private static GRID_COLCNT:[I

.field private static GRID_COLCNT_SPLIT_PANEL:[I

.field private static GRID_ITEM_GAP:[I

.field private static GRID_ITEM_GAP_EASYMODE:[I

.field private static GRID_THM_TYPES:[I

.field private static GRID_THM_TYPES_EXPAND:[I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected final REFER_HEIGHT:I

.field protected final REFER_WIDTH:I

.field private mFocusIndex:I

.field private mFocusedLeft:Z

.field private mItemSizeScale:F

.field protected mReferRatioForNewAlbum:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x5

    .line 18
    const-class v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->TAG:Ljava/lang/String;

    .line 22
    sput-object v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_COLCNT:[I

    .line 23
    sput-object v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_COLCNT_SPLIT_PANEL:[I

    .line 24
    sput-object v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_ITEM_GAP:[I

    .line 25
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_ITEM_GAP_EASYMODE:[I

    .line 26
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_THM_TYPES:[I

    .line 32
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_THM_TYPES_EXPAND:[I

    .line 39
    const v0, 0x3e75c28f    # 0.24f

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->ALBUM_TITLE_HRATIO:F

    return-void

    .line 25
    nop

    :array_0
    .array-data 4
        0x6
        0x9
        0xc
        0x10
        0xc
    .end array-data

    .line 26
    :array_1
    .array-data 4
        0x1
        0x1
        0x2
        0x2
        0x2
    .end array-data

    .line 32
    :array_2
    .array-data 4
        0x1
        0x2
        0x2
        0x2
        0x2
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;-><init>()V

    .line 41
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemSizeScale:F

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mFocusIndex:I

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mFocusedLeft:Z

    .line 45
    const/16 v0, 0x438

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->REFER_WIDTH:I

    .line 46
    const/16 v0, 0x780

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->REFER_HEIGHT:I

    return-void
.end method

.method private checkShowFocusedArrow(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;)V
    .locals 6
    .param p1, "groupInfo"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    .prologue
    const/4 v5, 0x0

    .line 71
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    check-cast v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget-object v0, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mFocusedArrow:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 74
    .local v0, "arrowObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-nez v0, :cond_0

    .line 78
    :goto_0
    return-void

    .line 76
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemSy:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mScrollable:F

    add-float/2addr v2, v3

    iget v3, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    sub-float/2addr v2, v3

    iget-object v3, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCy:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    sub-float v1, v2, v3

    .line 77
    .local v1, "ypos":F
    const/4 v2, 0x2

    invoke-virtual {v0, v5, v1, v5, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFFI)V

    goto :goto_0
.end method

.method private getPhotoLineCount(I)I
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mIsSplitViewExpanded:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_COLCNT:[I

    aget v0, v0, p1

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_COLCNT_SPLIT_PANEL:[I

    aget v0, v0, p1

    goto :goto_0
.end method

.method public static getThumbSizeType(I)I
    .locals 1
    .param p0, "level"    # I

    .prologue
    .line 50
    sget-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_THM_TYPES:[I

    aget v0, v0, p0

    return v0
.end method

.method private moveToGrid(IZ)Z
    .locals 12
    .param p1, "direction"    # I
    .param p2, "isExpanded"    # Z

    .prologue
    .line 670
    const/4 v3, 0x0

    .line 672
    .local v3, "itemH":F
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    .line 673
    .local v7, "posCtrlCom":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;
    iget v4, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    .line 674
    .local v4, "lastFocus":I
    iget-boolean v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsFocusVisible:Z

    if-nez v9, :cond_3

    .line 675
    if-nez p2, :cond_0

    const/4 v9, 0x2

    if-eq p1, v9, :cond_0

    .line 676
    const/4 v9, 0x0

    .line 790
    :goto_0
    return v9

    .line 678
    :cond_0
    const/4 v9, 0x1

    iput-boolean v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsFocusVisible:Z

    .line 679
    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGrpContentStart:I

    shr-int/lit8 v9, v9, 0x10

    iput v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    .line 680
    const/4 p1, 0x0

    .line 726
    :cond_1
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v9, v9

    if-lez v9, :cond_2

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    const/4 v10, 0x0

    aget-object v9, v9, v10

    if-nez v9, :cond_c

    .line 727
    :cond_2
    const/4 v9, 0x0

    goto :goto_0

    .line 681
    :cond_3
    const/4 v9, 0x1

    if-ne p1, v9, :cond_6

    .line 682
    iget v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-nez v9, :cond_5

    .line 683
    const/4 v1, 0x1

    .line 686
    .local v1, "focusDisappeared":Z
    :goto_1
    if-eqz v1, :cond_1

    .line 687
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    iget v10, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {v9, v10}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 688
    .local v6, "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-eqz v6, :cond_4

    .line 689
    const/4 v9, 0x0

    invoke-virtual {p0, v6, v9}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V

    .line 691
    :cond_4
    const/4 v9, 0x0

    iput-boolean v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsFocusVisible:Z

    .line 692
    const/4 v9, -0x1

    iput v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    .line 693
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mFocusedLeft:Z

    .line 694
    const/4 v9, 0x0

    goto :goto_0

    .line 685
    .end local v1    # "focusDisappeared":Z
    .end local v6    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_5
    const/4 v1, 0x0

    .restart local v1    # "focusDisappeared":Z
    goto :goto_1

    .line 696
    .end local v1    # "focusDisappeared":Z
    :cond_6
    const/4 v9, 0x3

    if-ne p1, v9, :cond_a

    .line 697
    const/4 v0, 0x0

    .line 698
    .local v0, "column":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    if-eqz v9, :cond_7

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v9, v9

    if-lez v9, :cond_7

    .line 699
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    const/4 v10, 0x0

    aget-object v9, v9, v10

    iget v0, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    .line 701
    :cond_7
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-ge v9, v0, :cond_9

    .line 702
    const/4 v1, 0x1

    .line 706
    .restart local v1    # "focusDisappeared":Z
    :goto_2
    if-eqz v1, :cond_1

    .line 707
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    iget v10, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {v9, v10}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 708
    .restart local v6    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-eqz v6, :cond_8

    .line 709
    const/4 v9, 0x0

    invoke-virtual {p0, v6, v9}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V

    .line 711
    :cond_8
    const/4 v9, 0x0

    iput-boolean v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsFocusVisible:Z

    .line 712
    const/4 v9, -0x1

    iput v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    .line 713
    const/4 v9, 0x0

    goto :goto_0

    .line 704
    .end local v1    # "focusDisappeared":Z
    .end local v6    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_9
    const/4 v1, 0x0

    .restart local v1    # "focusDisappeared":Z
    goto :goto_2

    .line 715
    .end local v0    # "column":I
    .end local v1    # "focusDisappeared":Z
    :cond_a
    const/4 v9, 0x5

    if-ne p1, v9, :cond_1

    .line 716
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    iget v10, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    shl-int/lit8 v10, v10, 0x10

    invoke-virtual {v9, v10}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 717
    .restart local v6    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-eqz v6, :cond_b

    .line 718
    const/4 v9, 0x0

    invoke-virtual {p0, v6, v9}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V

    .line 720
    :cond_b
    const/4 v9, 0x0

    iput-boolean v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsFocusVisible:Z

    .line 721
    const/4 v9, -0x1

    iput v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    .line 723
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 729
    .end local v6    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_c
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    const/4 v10, 0x0

    aget-object v2, v9, v10

    .line 730
    .local v2, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    if-nez v2, :cond_d

    .line 731
    sget-object v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->TAG:Ljava/lang/String;

    const-string v10, "gruupInfo is null"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 732
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 735
    :cond_d
    iget v9, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    if-lez v9, :cond_e

    .line 736
    iget-object v9, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemH:[F

    const/4 v10, 0x0

    aget v3, v9, v10

    .line 738
    :cond_e
    packed-switch p1, :pswitch_data_0

    .line 756
    sget-object v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Unknow direction : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 759
    :cond_f
    :goto_3
    iget v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-eq v4, v9, :cond_12

    const/4 v9, 0x1

    :goto_4
    iput-boolean v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mFocusChangedFlag:Z

    .line 760
    iget v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-eq v4, v9, :cond_16

    .line 761
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v9, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 762
    .restart local v6    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-eqz v6, :cond_10

    .line 763
    const/4 v9, 0x0

    invoke-virtual {p0, v6, v9}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V

    .line 765
    :cond_10
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    if-eqz v9, :cond_11

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    if-eqz v9, :cond_11

    iget v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    iget v10, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    if-lt v9, v10, :cond_13

    .line 766
    :cond_11
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 740
    .end local v6    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :pswitch_0
    iget v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    iget v10, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    add-int/2addr v9, v10

    iget v10, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    if-ge v9, v10, :cond_f

    .line 741
    iget v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    iget v10, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    add-int/2addr v9, v10

    iput v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    goto :goto_3

    .line 744
    :pswitch_1
    iget v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    add-int/lit8 v9, v9, 0x1

    iget v10, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    if-ge v9, v10, :cond_f

    .line 745
    iget v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    add-int/lit8 v9, v9, 0x1

    iput v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    goto :goto_3

    .line 748
    :pswitch_2
    iget v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    iget v10, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    sub-int/2addr v9, v10

    if-ltz v9, :cond_f

    .line 749
    iget v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    iget v10, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    sub-int/2addr v9, v10

    iput v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    goto :goto_3

    .line 752
    :pswitch_3
    iget v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    add-int/lit8 v9, v9, -0x1

    if-ltz v9, :cond_f

    .line 753
    iget v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    add-int/lit8 v9, v9, -0x1

    iput v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    goto :goto_3

    .line 759
    :cond_12
    const/4 v9, 0x0

    goto :goto_4

    .line 768
    .restart local v6    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_13
    iget v9, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget-object v10, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCy:[F

    iget v11, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    aget v10, v10, v11

    add-float/2addr v9, v10

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mScrollable:F

    add-float/2addr v10, v3

    cmpg-float v9, v9, v10

    if-gez v9, :cond_17

    .line 769
    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mScrollable:F

    iget-object v10, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCy:[F

    iget v11, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    aget v10, v10, v11

    sub-float v10, v3, v10

    add-float v8, v9, v10

    .line 770
    .local v8, "scrolAmount":F
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mScrollable:F

    sub-float/2addr v10, v8

    invoke-virtual {v9, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->doScroll(F)V

    .line 776
    .end local v8    # "scrolAmount":F
    :cond_14
    :goto_5
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    iget v10, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {v9, v10}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 777
    .local v5, "newThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-nez v5, :cond_15

    .line 778
    iget v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-ge v9, v4, :cond_18

    .line 779
    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mScrollable:F

    sub-float v8, v9, v3

    .line 784
    .restart local v8    # "scrolAmount":F
    :goto_6
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v9, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->doScroll(F)V

    .line 785
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    iget v10, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {v9, v10}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "newThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    check-cast v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 788
    .end local v8    # "scrolAmount":F
    .restart local v5    # "newThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_15
    const/4 v9, 0x1

    invoke-virtual {p0, v5, v9}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V

    .line 790
    .end local v5    # "newThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .end local v6    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_16
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 771
    .restart local v6    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_17
    iget v9, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget-object v10, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCy:[F

    iget v11, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    aget v10, v10, v11

    add-float/2addr v9, v10

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mValidH:F

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mScrollable:F

    add-float/2addr v10, v11

    cmpl-float v9, v9, v10

    if-lez v9, :cond_14

    .line 772
    iget-object v9, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCy:[F

    iget v10, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    aget v9, v9, v10

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mValidH:F

    sub-float v8, v9, v10

    .line 773
    .restart local v8    # "scrolAmount":F
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v9, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->doScroll(F)V

    goto :goto_5

    .line 781
    .end local v8    # "scrolAmount":F
    .restart local v5    # "newThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_18
    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mScrollable:F

    add-float v8, v9, v3

    .restart local v8    # "scrolAmount":F
    goto :goto_6

    .line 738
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private updateLeft(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;I)V
    .locals 17
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .param p2, "albumType"    # I

    .prologue
    .line 172
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-nez v1, :cond_0

    .line 173
    sget-object v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "update left is ignored because adapter is null"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    :goto_0
    return-void

    .line 183
    :cond_0
    move-object/from16 v0, p1

    iget v11, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    .line 184
    .local v11, "index":I
    shr-int/lit8 v2, v11, 0x10

    .line 185
    .local v2, "albumIdx":I
    const v1, 0xffff

    and-int v12, v11, v1

    .line 186
    .local v12, "photoIdx":I
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mFocusIndex:I

    if-ne v11, v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setFocusBorderVisible(Z)V

    .line 187
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->updateBorder(I)V

    .line 189
    if-ltz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v1, v1

    if-lt v2, v1, :cond_3

    .line 190
    :cond_1
    sget-object v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "update left is ignored albumIdx = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", length = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 186
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 193
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v9, v1, v2

    .line 194
    .local v9, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v1, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    if-lt v12, v1, :cond_4

    .line 195
    sget-object v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "update left is ignored photoIdx = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", length = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 198
    :cond_4
    iget-object v1, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemW:[F

    aget v16, v1, v12

    .line 199
    .local v16, "width":F
    iget-object v1, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemH:[F

    aget v10, v1, v12

    .line 200
    .local v10, "height":F
    const/high16 v13, 0x3f800000    # 1.0f

    .line 201
    .local v13, "ratio":F
    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mAlbumTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 202
    .local v15, "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-nez v15, :cond_8

    .line 203
    new-instance v15, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .end local v15    # "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-direct {v15, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    .line 204
    .restart local v15    # "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleCanvsW:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleCanvsH:I

    invoke-direct {v1, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {v15, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 205
    const/4 v1, 0x1

    invoke-virtual {v15, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setBlendType(I)V

    .line 206
    const/4 v1, 0x0

    invoke-virtual {v15, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setUseTouchEvent(Z)V

    .line 207
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 208
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    check-cast v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    const/4 v3, 0x2

    invoke-virtual {v1, v15, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setObjectListeners(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;I)V

    .line 209
    move-object/from16 v0, p1

    iput-object v15, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mAlbumTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 214
    :cond_5
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v15}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move/from16 v3, p2

    invoke-virtual/range {v1 .. v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getView(IILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v8

    .line 215
    .local v8, "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {v15, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 216
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v1

    if-nez v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTabletPickerMode(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPortraitMode:Z

    if-nez v1, :cond_7

    .line 218
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleW:F

    div-float v13, v16, v1

    .line 220
    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsRTL:Z

    if-eqz v1, :cond_9

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleTextMarginLeft:F

    neg-float v14, v1

    .line 221
    .local v14, "titleMarginLeft":F
    :goto_3
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleTextMarginTop:F

    neg-float v1, v1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleH:F

    add-float/2addr v3, v10

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    sub-float/2addr v1, v3

    const/4 v3, 0x0

    invoke-virtual {v15, v14, v1, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    .line 222
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleW:F

    mul-float/2addr v1, v13

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleH:F

    mul-float/2addr v3, v13

    invoke-virtual {v15, v1, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    goto/16 :goto_0

    .line 210
    .end local v8    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    .end local v14    # "titleMarginLeft":F
    :cond_8
    invoke-virtual {v15}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->getWidth()I

    move-result v1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleCanvsW:I

    if-eq v1, v3, :cond_5

    .line 211
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleCanvsW:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleCanvsH:I

    invoke-direct {v1, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {v15, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    goto/16 :goto_2

    .line 220
    .restart local v8    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_9
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleTextMarginLeft:F

    goto :goto_3
.end method


# virtual methods
.method public calcItemPosition(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;)F
    .locals 13
    .param p1, "groupInfo"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    .prologue
    .line 391
    iget-object v7, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCx:[F

    .line 392
    .local v7, "cx":[F
    iget-object v8, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCy:[F

    .line 393
    .local v8, "cy":[F
    iget-object v6, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemW:[F

    .line 394
    .local v6, "cw":[F
    iget-object v4, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemH:[F

    .line 395
    .local v4, "ch":[F
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPhotoLineCount:I

    .line 397
    .local v5, "colCount":I
    iput v5, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    .line 398
    iget v10, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPhotoLineCount:I

    add-int/2addr v10, v11

    add-int/lit8 v10, v10, -0x1

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPhotoLineCount:I

    div-int/2addr v10, v11

    iput v10, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mRow:I

    .line 400
    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mValidW:F

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemGapW:F

    add-int/lit8 v12, v5, -0x1

    int-to-float v12, v12

    mul-float/2addr v11, v12

    sub-float/2addr v10, v11

    int-to-float v11, v5

    div-float/2addr v10, v11

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemSizeScale:F

    mul-float v3, v10, v11

    .line 401
    .local v3, "SW":F
    move v2, v3

    .line 402
    .local v2, "SH":F
    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemGapW:F

    add-float v1, v3, v10

    .line 403
    .local v1, "NW":F
    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemGapH:F

    add-float v0, v2, v10

    .line 404
    .local v0, "NH":F
    iput v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    .line 405
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    iget v10, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    if-ge v9, v10, :cond_0

    .line 406
    rem-int v10, v9, v5

    int-to-float v10, v10

    mul-float/2addr v10, v1

    const/high16 v11, 0x40000000    # 2.0f

    div-float v11, v3, v11

    add-float/2addr v10, v11

    aput v10, v7, v9

    .line 407
    div-int v10, v9, v5

    int-to-float v10, v10

    mul-float/2addr v10, v0

    add-float/2addr v10, v2

    aput v10, v8, v9

    .line 408
    aput v3, v6, v9

    .line 409
    aput v2, v4, v9

    .line 405
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 411
    :cond_0
    iget v10, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mRow:I

    int-to-float v10, v10

    iget v11, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    mul-float/2addr v10, v11

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroupVGap:F

    add-float/2addr v10, v11

    return v10
.end method

.method public doQuickScroll(I)V
    .locals 0
    .param p1, "groupIndex"    # I

    .prologue
    .line 795
    return-void
.end method

.method public getFocused()I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mFocusIndex:I

    return v0
.end method

.method public getNewAlbumHeaderRect(Landroid/graphics/RectF;)V
    .locals 9
    .param p1, "bgRect"    # Landroid/graphics/RectF;

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    .line 449
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    check-cast v6, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v1

    .line 450
    .local v1, "mDimensionUtil":Lcom/sec/samsung/gallery/util/DimensionUtil;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v6, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPortraitMode:Z

    if-eqz v6, :cond_0

    sget v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->SPLIT_RATIO_PORT:F

    .line 451
    .local v2, "splitRatio":F
    :goto_0
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mSpaceWidth:F

    mul-float v5, v6, v2

    .line 452
    .local v5, "w":F
    const v6, 0x3f9ae148    # 1.21f

    mul-float v0, v5, v6

    .line 453
    .local v0, "h":F
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mSpaceWidth:F

    sub-float v6, v5, v6

    div-float/2addr v6, v8

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitExtraGapPixel()I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->convX(I)F

    move-result v7

    sub-float v3, v6, v7

    .line 454
    .local v3, "sx":F
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mSpaceHeight:F

    sub-float/2addr v6, v0

    div-float/2addr v6, v8

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getActionBarHeight()I

    move-result v7

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getSelectionModeLayoutHeight()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->convY(I)F

    move-result v7

    sub-float v4, v6, v7

    .line 456
    .local v4, "sy":F
    invoke-virtual {p1, v3, v4, v5, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 457
    return-void

    .line 450
    .end local v0    # "h":F
    .end local v2    # "splitRatio":F
    .end local v3    # "sx":F
    .end local v4    # "sy":F
    .end local v5    # "w":F
    :cond_0
    sget v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->SPLIT_RATIO_LAND:F

    goto :goto_0
.end method

.method public getScrollForIndex(I)F
    .locals 7
    .param p1, "index"    # I

    .prologue
    const/4 v4, 0x0

    .line 419
    shr-int/lit8 v0, p1, 0x10

    .line 420
    .local v0, "albumIdx":I
    const v5, 0xffff

    and-int v2, p1, v5

    .line 421
    .local v2, "photoIdx":I
    if-ltz p1, :cond_0

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroupCount:I

    if-lt v0, v5, :cond_1

    .line 428
    :cond_0
    :goto_0
    return v4

    .line 424
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v5, v0

    .line 425
    .local v1, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v5, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    if-lez v5, :cond_0

    .line 427
    iget v4, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    div-int v3, v2, v4

    .line 428
    .local v3, "row":I
    iget v4, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    int-to-float v5, v3

    iget v6, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    goto :goto_0
.end method

.method public getVisibleScrollDelta(F)F
    .locals 6
    .param p1, "scroll"    # F

    .prologue
    const/4 v2, 0x0

    .line 433
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mScrollable:F

    sub-float v0, p1, v3

    .line 434
    .local v0, "resScroll":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mAlbumList:Z

    if-nez v3, :cond_0

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mValidH:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemH:F

    sub-float v1, v3, v4

    .line 437
    .local v1, "visibleHeight":F
    :goto_0
    cmpg-float v3, v0, v2

    if-gez v3, :cond_1

    .line 442
    .end local v0    # "resScroll":F
    :goto_1
    return v0

    .line 434
    .end local v1    # "visibleHeight":F
    .restart local v0    # "resScroll":F
    :cond_0
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mValidH:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemH:F

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemH:F

    sget v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->ALBUM_TITLE_HRATIO:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mScrollTopMargine:F

    sub-float v1, v3, v4

    goto :goto_0

    .line 439
    .restart local v1    # "visibleHeight":F
    :cond_1
    cmpl-float v3, v0, v1

    if-lez v3, :cond_2

    .line 440
    sub-float/2addr v0, v1

    goto :goto_1

    :cond_2
    move v0, v2

    .line 442
    goto :goto_1
.end method

.method public initEnv(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;)V
    .locals 2
    .param p1, "comCtrl"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    .prologue
    .line 54
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->initEnv(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;)V

    .line 55
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0d00ec

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleWidthPixel:I

    .line 56
    return-void
.end method

.method public moveTo(I)Z
    .locals 6
    .param p1, "direction"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 552
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mAlbumList:Z

    if-eqz v3, :cond_0

    move v3, v4

    .line 576
    :goto_0
    return v3

    .line 556
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getParent()Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .line 557
    .local v2, "splitView":Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
    const/4 v0, 0x1

    .line 558
    .local v0, "isExpanded":Z
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    instance-of v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    if-eqz v3, :cond_1

    .line 559
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    check-cast v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->isExpanded()Z

    move-result v0

    .line 562
    :cond_1
    iget-object v1, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    check-cast v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    .line 564
    .local v1, "positionControllerPhoto":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mFocusedLeft:Z

    if-eqz v3, :cond_3

    .line 565
    invoke-virtual {v1, p1, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->moveToSplit(IZ)Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v5

    .line 566
    goto :goto_0

    .line 568
    :cond_2
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mFocusedLeft:Z

    .line 569
    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->moveToGrid(IZ)Z

    move-result v3

    goto :goto_0

    .line 572
    :cond_3
    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->moveToGrid(IZ)Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v5

    .line 573
    goto :goto_0

    .line 575
    :cond_4
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mFocusedLeft:Z

    .line 576
    invoke-virtual {v1, p1, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->moveToSplit(IZ)Z

    move-result v3

    goto :goto_0
.end method

.method public moveToSplit(IZ)Z
    .locals 11
    .param p1, "direction"    # I
    .param p2, "isExpanded"    # Z

    .prologue
    const/4 v10, -0x1

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 588
    if-eqz p2, :cond_1

    .line 661
    :cond_0
    :goto_0
    return v9

    .line 591
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    .line 592
    .local v5, "posCtrlCom":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;
    iget v2, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    .line 593
    .local v2, "lastFocus":I
    iget-boolean v7, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsFocusVisible:Z

    if-nez v7, :cond_a

    .line 594
    const/4 v7, 0x4

    if-eq p1, v7, :cond_2

    if-ne p1, v8, :cond_0

    .line 597
    :cond_2
    iput-boolean v8, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsFocusVisible:Z

    .line 598
    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGrpActiveStart:I

    shr-int/lit8 v7, v7, 0x10

    iput v7, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    .line 599
    const/4 p1, 0x0

    .line 626
    :cond_3
    packed-switch p1, :pswitch_data_0

    .line 640
    :cond_4
    :goto_1
    :pswitch_0
    iget v7, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-eq v2, v7, :cond_10

    move v7, v8

    :goto_2
    iput-boolean v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mFocusChangedFlag:Z

    .line 641
    iget v7, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-eq v2, v7, :cond_9

    .line 642
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    shl-int/lit8 v10, v2, 0x10

    invoke-virtual {v7, v10}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 643
    .local v4, "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-eqz v4, :cond_5

    .line 644
    invoke-virtual {p0, v4, v9}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V

    .line 646
    :cond_5
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    if-eqz v7, :cond_0

    iget v7, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-ltz v7, :cond_0

    .line 648
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    iget v9, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    aget-object v1, v7, v9

    .line 649
    .local v1, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v7, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mScrollable:F

    cmpg-float v7, v7, v9

    if-gez v7, :cond_6

    .line 650
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v9, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    invoke-virtual {v7, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->doScroll(F)V

    .line 652
    :cond_6
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    iget v9, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    shl-int/lit8 v9, v9, 0x10

    invoke-virtual {v7, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 653
    .local v3, "newThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    iget v7, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v9, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float/2addr v7, v9

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroupVGap:F

    sub-float v6, v7, v9

    .line 654
    .local v6, "scrolAmount":F
    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mValidH:F

    sub-float/2addr v6, v7

    .line 655
    if-eqz v3, :cond_7

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mScrollable:F

    cmpg-float v7, v7, v6

    if-gez v7, :cond_8

    .line 656
    :cond_7
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v7, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->doScroll(F)V

    .line 657
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    iget v9, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    shl-int/lit8 v9, v9, 0x10

    invoke-virtual {v7, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "newThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    check-cast v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 659
    .restart local v3    # "newThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_8
    invoke-virtual {p0, v3, v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V

    .end local v1    # "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    .end local v3    # "newThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .end local v4    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .end local v6    # "scrolAmount":F
    :cond_9
    move v9, v8

    .line 661
    goto/16 :goto_0

    .line 600
    :cond_a
    const/4 v7, 0x3

    if-ne p1, v7, :cond_d

    .line 601
    iget v7, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-nez v7, :cond_c

    .line 602
    const/4 v0, 0x1

    .line 606
    .local v0, "focusDisappeared":Z
    :goto_3
    if-eqz v0, :cond_3

    .line 607
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    iget v8, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    shl-int/lit8 v8, v8, 0x10

    invoke-virtual {v7, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 608
    .restart local v4    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-eqz v4, :cond_b

    .line 609
    invoke-virtual {p0, v4, v9}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V

    .line 611
    :cond_b
    iput-boolean v9, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsFocusVisible:Z

    .line 612
    iput v10, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    goto/16 :goto_0

    .line 604
    .end local v0    # "focusDisappeared":Z
    .end local v4    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_c
    const/4 v0, 0x0

    .restart local v0    # "focusDisappeared":Z
    goto :goto_3

    .line 615
    .end local v0    # "focusDisappeared":Z
    :cond_d
    const/4 v7, 0x2

    if-eq p1, v7, :cond_e

    const/4 v7, 0x5

    if-ne p1, v7, :cond_3

    .line 616
    :cond_e
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    iget v8, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    shl-int/lit8 v8, v8, 0x10

    invoke-virtual {v7, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 617
    .restart local v4    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-eqz v4, :cond_f

    .line 618
    invoke-virtual {p0, v4, v9}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V

    .line 620
    :cond_f
    iput-boolean v9, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsFocusVisible:Z

    .line 621
    iput v10, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    goto/16 :goto_0

    .line 628
    .end local v4    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :pswitch_1
    iget v7, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    add-int/lit8 v7, v7, 0x1

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroupCount:I

    if-ge v7, v10, :cond_4

    .line 629
    iget v7, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    add-int/lit8 v7, v7, 0x1

    iput v7, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    goto/16 :goto_1

    .line 634
    :pswitch_2
    iget v7, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    add-int/lit8 v7, v7, -0x1

    if-ltz v7, :cond_4

    .line 635
    iget v7, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    add-int/lit8 v7, v7, -0x1

    iput v7, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    goto/16 :goto_1

    :cond_10
    move v7, v9

    .line 640
    goto/16 :goto_2

    .line 626
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateThumbObj(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)V
    .locals 0
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .prologue
    .line 801
    return-void
.end method

.method public resetAttributes(IZ)V
    .locals 13
    .param p1, "option"    # I
    .param p2, "portraitMode"    # Z

    .prologue
    const v8, 0x7f0d00e6

    const/high16 v12, 0x40000000    # 2.0f

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 261
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getLcdRect(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mLcdRect:Landroid/graphics/Rect;

    .line 264
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mLcdRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    int-to-float v7, v6

    if-eqz p2, :cond_3

    const/16 v6, 0x438

    :goto_0
    int-to-float v6, v6

    div-float v6, v7, v6

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mReferRatioForNewAlbum:F

    .line 265
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mResource:Landroid/content/res/Resources;

    .line 266
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    check-cast v6, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v1

    .line 268
    .local v1, "mDimensionUtil":Lcom/sec/samsung/gallery/util/DimensionUtil;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f0d00ec

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleWidthPixel:I

    .line 269
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f0d00eb

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleHeightPixel:I

    .line 270
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f0d00ea

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleTextMarginTopPixel:I

    .line 271
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f0d00ed

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleTextMarginBottomPixel:I

    .line 272
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumTitlePaddingLeft()I

    move-result v6

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleTextMarginLeftPixel:I

    .line 274
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mAlbumList:Z

    if-eqz v6, :cond_4

    .line 275
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getActionBarHeight()I

    move-result v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mResource:Landroid/content/res/Resources;

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    add-int/2addr v6, v7

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mMarginTopPixel:I

    .line 276
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f0d00e7

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mMarginLeftPixel:I

    .line 281
    :goto_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPickMode(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->IsCheckMode()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 282
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mMarginTopPixel:I

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getSelectionModeLayoutHeight()I

    move-result v7

    add-int/2addr v6, v7

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mMarginTopPixel:I

    .line 284
    :cond_0
    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->resetValues(Z)V

    .line 286
    sget-object v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "resetAttributes :: mTitleCanvsH : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleCanvsH:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mTitleCanvsW:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleCanvsW:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mLevel:I

    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->getPhotoLineCount(I)I

    move-result v6

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPhotoLineCount:I

    .line 289
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v6, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPortraitMode:Z

    if-eqz v6, :cond_5

    .line 290
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget v5, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mPortRatio:F

    .line 330
    .local v5, "splitRatio":F
    :cond_1
    :goto_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mAlbumList:Z

    if-eqz v6, :cond_d

    .line 331
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mValidW:F

    mul-float/2addr v6, v5

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mValidW:F

    .line 332
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemGapHPixel:I

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->convX(I)F

    move-result v6

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemGapW:F

    .line 333
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemGapVPixel:I

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->convY(I)F

    move-result v6

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemGapH:F

    .line 336
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleH:F

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleTextMarginTop:F

    add-float/2addr v6, v7

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleTextMarginBottom:F

    add-float/2addr v6, v7

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroupVGap:F

    .line 337
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mSpaceWidth:F

    neg-float v6, v6

    div-float/2addr v6, v12

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mMargLeft:F

    add-float/2addr v6, v7

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemSx:F

    .line 338
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->IsCheckMode()Z

    move-result v6

    iput-boolean v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mDisplaySelectedCount:Z

    .line 362
    :cond_2
    :goto_3
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mValidW:F

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemGapW:F

    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPhotoLineCount:I

    add-int/lit8 v8, v8, -0x1

    int-to-float v8, v8

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPhotoLineCount:I

    int-to-float v7, v7

    div-float/2addr v6, v7

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemW:F

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemH:F

    .line 363
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mItemSizeScale:F

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemSizeScale:F

    .line 364
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->resetItemMaxSize()V

    .line 365
    return-void

    .line 264
    .end local v1    # "mDimensionUtil":Lcom/sec/samsung/gallery/util/DimensionUtil;
    .end local v5    # "splitRatio":F
    :cond_3
    const/16 v6, 0x780

    goto/16 :goto_0

    .line 278
    .restart local v1    # "mDimensionUtil":Lcom/sec/samsung/gallery/util/DimensionUtil;
    :cond_4
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getActionBarHeight()I

    move-result v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mResource:Landroid/content/res/Resources;

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    add-int/2addr v6, v7

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mMarginTopPixel:I

    goto/16 :goto_1

    .line 292
    :cond_5
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget v5, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mLandRatio:F

    .line 294
    .restart local v5    # "splitRatio":F
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mLcdRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->convX(I)F

    move-result v7

    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mViewWidth:I

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mLcdRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    div-int/lit8 v8, v8, 0x4

    if-ge v6, v8, :cond_8

    const/16 v6, 0xa

    :goto_4
    int-to-float v6, v6

    div-float v2, v7, v6

    .line 296
    .local v2, "minW":F
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v6

    if-nez v6, :cond_6

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    check-cast v6, Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTabletPickerMode(Landroid/content/Intent;)Z

    move-result v6

    if-eqz v6, :cond_c

    :cond_6
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mValidW:F

    mul-float/2addr v6, v5

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPhotoLineCount:I

    int-to-float v7, v7

    mul-float/2addr v7, v2

    cmpg-float v6, v6, v7

    if-gez v6, :cond_c

    .line 298
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mAlbumList:Z

    if-nez v6, :cond_b

    .line 299
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPhotoLineCount:I

    .line 302
    .local v4, "originalPhotoLineCount":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mIsSplitViewExpanded:Z

    if-eqz v6, :cond_9

    .line 303
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mLcdRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->convX(I)F

    move-result v6

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPhotoLineCount:I

    int-to-float v7, v7

    div-float v2, v6, v7

    .line 304
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mValidW:F

    div-float/2addr v6, v2

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 305
    .local v3, "newPhotoLineCount":I
    invoke-static {v3, v9, v4}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v6

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPhotoLineCount:I

    .line 319
    :cond_7
    :goto_5
    invoke-static {v3, v9, v4}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v6

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPhotoLineCount:I

    goto/16 :goto_2

    .line 294
    .end local v2    # "minW":F
    .end local v3    # "newPhotoLineCount":I
    .end local v4    # "originalPhotoLineCount":I
    :cond_8
    const/4 v6, 0x6

    goto :goto_4

    .line 307
    .restart local v2    # "minW":F
    .restart local v4    # "originalPhotoLineCount":I
    :cond_9
    const/high16 v6, 0x3f800000    # 1.0f

    sget v7, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->SPLIT_RATIO_LAND:F

    sub-float v5, v6, v7

    .line 308
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mValidW:F

    mul-float/2addr v6, v5

    div-float/2addr v6, v2

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 310
    .restart local v3    # "newPhotoLineCount":I
    sget-object v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_COLCNT:[I

    aget v6, v6, v10

    if-ne v4, v6, :cond_a

    .line 312
    sget-object v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_COLCNT:[I

    aget v6, v6, v10

    sget-object v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_COLCNT:[I

    aget v7, v7, v11

    sub-int v0, v6, v7

    .line 313
    .local v0, "addCnt":I
    add-int/2addr v3, v0

    .line 314
    goto :goto_5

    .end local v0    # "addCnt":I
    :cond_a
    sget-object v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_COLCNT:[I

    aget v6, v6, v9

    if-lt v4, v6, :cond_7

    .line 315
    sget-object v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_COLCNT:[I

    aget v6, v6, v9

    sget-object v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_COLCNT:[I

    aget v7, v7, v11

    sub-int v0, v6, v7

    .line 316
    .restart local v0    # "addCnt":I
    add-int/2addr v3, v0

    goto :goto_5

    .line 321
    .end local v0    # "addCnt":I
    .end local v3    # "newPhotoLineCount":I
    .end local v4    # "originalPhotoLineCount":I
    :cond_b
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mValidW:F

    div-float v5, v2, v6

    .line 322
    sput v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->SPLIT_RATIO_LAND:F

    goto/16 :goto_2

    .line 325
    :cond_c
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mAlbumList:Z

    if-eqz v6, :cond_1

    .line 326
    sput v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->SPLIT_RATIO_LAND:F

    goto/16 :goto_2

    .line 340
    .end local v2    # "minW":F
    :cond_d
    sget-object v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_ITEM_GAP:[I

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mLevel:I

    aget v6, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->convX(I)F

    move-result v6

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemGapW:F

    .line 341
    sget-object v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_ITEM_GAP:[I

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mLevel:I

    aget v6, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->convY(I)F

    move-result v6

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemGapH:F

    .line 342
    const/4 v6, 0x0

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroupVGap:F

    .line 344
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-boolean v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mIsEasyMode:Z

    if-eqz v6, :cond_10

    .line 345
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isWQHD(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 346
    sget-object v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_ITEM_GAP_EASYMODE:[I

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mLevel:I

    aget v6, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->convX(I)F

    move-result v6

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemGapW:F

    .line 347
    sget-object v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_ITEM_GAP_EASYMODE:[I

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mLevel:I

    aget v6, v6, v7

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->convY(I)F

    move-result v6

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemGapH:F

    .line 349
    :cond_e
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPhotoLineCount:I

    if-ne v6, v9, :cond_f

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v6, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPortraitMode:Z

    if-nez v6, :cond_f

    .line 350
    iput v10, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPhotoLineCount:I

    .line 351
    :cond_f
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mIsSplitViewExpanded:Z

    if-eqz v6, :cond_10

    .line 352
    iput v10, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPhotoLineCount:I

    .line 355
    :cond_10
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-boolean v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mIsEasyMode:Z

    if-eqz v6, :cond_11

    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mLevel:I

    if-eq v6, v11, :cond_2

    .line 357
    :cond_11
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mIsSplitViewExpanded:Z

    if-nez v6, :cond_2

    .line 358
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mValidW:F

    mul-float/2addr v6, v5

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mValidW:F

    .line 359
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mSpaceWidth:F

    div-float/2addr v6, v12

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mValidW:F

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mMargRight:F

    sub-float/2addr v6, v7

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemSx:F

    goto/16 :goto_3
.end method

.method protected resetItemMaxSize()V
    .locals 6

    .prologue
    .line 227
    const/4 v3, 0x0

    .line 228
    .local v3, "tempMaxSize":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-boolean v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mIsEasyMode:Z

    if-eqz v4, :cond_0

    .line 229
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemW:F

    .line 251
    :goto_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mItemSizeScale:F

    mul-float/2addr v3, v4

    .line 252
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->rConvX(F)I

    move-result v4

    int-to-float v4, v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemMaxW:F

    .line 253
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->rConvY(F)I

    move-result v4

    int-to-float v4, v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemMaxH:F

    .line 254
    return-void

    .line 231
    :cond_0
    const/4 v1, 0x0

    .line 232
    .local v1, "maxLevel":I
    const/4 v2, 0x0

    .line 233
    .local v2, "photoCount":I
    const/4 v0, 0x0

    .line 234
    .local v0, "itemGapW":F
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mLevel:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_3

    .line 235
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mLevel:I

    .line 243
    :goto_1
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->getPhotoLineCount(I)I

    move-result v2

    .line 244
    sget-object v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_ITEM_GAP:[I

    aget v4, v4, v1

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->convX(I)F

    move-result v0

    .line 245
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTabletPickerMode(Landroid/content/Intent;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 246
    :cond_1
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPhotoLineCount:I

    .line 248
    :cond_2
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mValidW:F

    add-int/lit8 v5, v2, -0x1

    int-to-float v5, v5

    mul-float/2addr v5, v0

    sub-float/2addr v4, v5

    int-to-float v5, v2

    div-float v3, v4, v5

    goto :goto_0

    .line 236
    :cond_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mIsSplitViewExpanded:Z

    if-eqz v4, :cond_4

    .line 237
    const/4 v1, 0x2

    goto :goto_1

    .line 239
    :cond_4
    const/4 v1, 0x3

    goto :goto_1
.end method

.method public resetPosition()V
    .locals 5

    .prologue
    .line 370
    const/4 v3, 0x0

    .line 372
    .local v3, "scrollable":F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroupCount:I

    if-ge v1, v4, :cond_1

    .line 373
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v0, v4, v1

    .line 374
    .local v0, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    if-nez v0, :cond_0

    .line 372
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 377
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->calcItemPosition(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;)F

    move-result v2

    .line 378
    .local v2, "scrollUnit":F
    iput v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    .line 379
    iput v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    .line 380
    add-float/2addr v3, v2

    goto :goto_1

    .line 382
    .end local v0    # "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    .end local v2    # "scrollUnit":F
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->setMaxScrollable()V

    .line 383
    return-void
.end method

.method public resetPosition(II)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 386
    return-void
.end method

.method protected resetValues(Z)V
    .locals 2
    .param p1, "portraitMode"    # Z

    .prologue
    .line 82
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->resetValues(Z)V

    .line 83
    if-eqz p1, :cond_0

    .line 84
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f090089

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_COLCNT:[I

    .line 85
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f09008a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_COLCNT_SPLIT_PANEL:[I

    .line 90
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f09008b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_ITEM_GAP:[I

    .line 91
    return-void

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f090090

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_COLCNT:[I

    .line 88
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f090091

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_COLCNT_SPLIT_PANEL:[I

    goto :goto_0
.end method

.method public setCheckBoxVisibility()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mAlbumList:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->IsCheckMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mDisplaySelectedCount:Z

    .line 127
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setCheckBoxVisibility()V

    .line 128
    return-void

    .line 126
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFocused(IZ)V
    .locals 5
    .param p1, "index"    # I
    .param p2, "applyNow"    # Z

    .prologue
    const/4 v4, 0x0

    .line 100
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mFocusIndex:I

    .line 102
    .local v2, "prevIndex":I
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mFocusIndex:I

    .line 103
    if-nez p2, :cond_1

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 106
    invoke-virtual {p0, v2, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->update(II)Z

    .line 108
    :cond_2
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mFocusIndex:I

    if-ltz v3, :cond_3

    .line 109
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mFocusIndex:I

    invoke-virtual {p0, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->update(II)Z

    .line 111
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mAlbumList:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mFocusIndex:I

    if-ltz v3, :cond_0

    .line 113
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mFocusIndex:I

    shr-int/lit8 v0, v3, 0x10

    .line 114
    .local v0, "groupIndex":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v3, v3

    if-le v3, v0, :cond_0

    .line 116
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v3, v0

    .line 117
    .local v1, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v3, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    if-eqz v3, :cond_0

    .line 120
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->checkShowFocusedArrow(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;)V

    goto :goto_0
.end method

.method public setScroll(FZ)V
    .locals 3
    .param p1, "scroll"    # F
    .param p2, "layoutChanged"    # Z

    .prologue
    .line 132
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setScroll(FZ)V

    .line 134
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mAlbumList:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mFocusIndex:I

    if-gez v2, :cond_1

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 136
    :cond_1
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mFocusIndex:I

    shr-int/lit8 v0, v2, 0x10

    .line 137
    .local v0, "groupIndex":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v2, v2

    if-le v2, v0, :cond_0

    .line 139
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v2, v0

    .line 140
    .local v1, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v2, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    if-eqz v2, :cond_0

    .line 143
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->checkShowFocusedArrow(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;)V

    goto :goto_0
.end method

.method public setToCurrentCtrl()V
    .locals 3

    .prologue
    .line 61
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mSplitViewExpanded:Z

    if-eqz v1, :cond_1

    .line 62
    sget-object v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_THM_TYPES_EXPAND:[I

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mLevel:I

    aget v0, v1, v2

    .line 65
    .local v0, "thmLevel":I
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v1, :cond_0

    .line 66
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->setThumbReslevel(I)V

    .line 67
    :cond_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setToCurrentCtrl()V

    .line 68
    return-void

    .line 64
    .end local v0    # "thmLevel":I
    :cond_1
    sget-object v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->GRID_THM_TYPES:[I

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mLevel:I

    aget v0, v1, v2

    .restart local v0    # "thmLevel":I
    goto :goto_0
.end method

.method public setVisibleRange()V
    .locals 14

    .prologue
    .line 461
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mScrollable:F

    neg-float v9, v11

    .line 464
    .local v9, "scrollAmount":F
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroupCount:I

    add-int/lit8 v8, v11, -0x1

    .line 465
    .local v8, "lastGrp":I
    move v7, v8

    .line 466
    .local v7, "inGrpStart":I
    move v5, v8

    .line 467
    .local v5, "inGrpEnd":I
    move v4, v8

    .line 468
    .local v4, "inCntGrpStart":I
    move v3, v8

    .line 471
    .local v3, "inCntGrpEnd":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroupCount:I

    if-ge v2, v11, :cond_3

    .line 472
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v11, v2

    .line 474
    .local v1, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v11, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float/2addr v9, v11

    .line 475
    if-ge v2, v4, :cond_0

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mExtraTop:F

    cmpg-float v11, v11, v9

    if-gtz v11, :cond_0

    .line 476
    move v4, v2

    .line 478
    :cond_0
    if-ge v2, v7, :cond_1

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mMargTop:F

    neg-float v11, v11

    cmpg-float v11, v11, v9

    if-gtz v11, :cond_1

    .line 479
    move v7, v2

    .line 481
    :cond_1
    if-ne v5, v8, :cond_2

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mValidH:F

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mMargBtm:F

    add-float/2addr v11, v12

    cmpl-float v11, v9, v11

    if-ltz v11, :cond_2

    .line 482
    move v5, v2

    .line 484
    :cond_2
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mExtraBtm:F

    cmpl-float v11, v9, v11

    if-ltz v11, :cond_4

    .line 485
    move v3, v2

    .line 490
    .end local v1    # "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    :cond_3
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v11, v7

    .line 491
    .restart local v1    # "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v11, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mScrollable:F

    sub-float v0, v11, v12

    .line 492
    .local v0, "exceedAmount":F
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mMargTop:F

    neg-float v11, v11

    cmpl-float v11, v0, v11

    if-lez v11, :cond_5

    .line 493
    const/4 v6, 0x0

    .line 503
    .local v6, "inGrpIndex":I
    :goto_1
    shl-int/lit8 v11, v7, 0x10

    or-int/2addr v11, v6

    iput v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGrpActiveStart:I

    .line 505
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v11, v5

    .line 506
    iget v11, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v12, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float/2addr v11, v12

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mScrollable:F

    sub-float v0, v11, v12

    .line 507
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mValidH:F

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mMargBtm:F

    add-float/2addr v11, v12

    cmpg-float v11, v0, v11

    if-gtz v11, :cond_7

    .line 508
    iget v6, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    .line 518
    :goto_2
    shl-int/lit8 v11, v5, 0x10

    or-int/2addr v11, v6

    iput v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGrpActiveEnd:I

    .line 520
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v11, v4

    .line 521
    iget v11, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mScrollable:F

    sub-float v0, v11, v12

    .line 522
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mExtraTop:F

    cmpl-float v11, v0, v11

    if-lez v11, :cond_9

    .line 523
    const/4 v6, 0x0

    .line 533
    :goto_3
    shl-int/lit8 v11, v4, 0x10

    or-int/2addr v11, v6

    iput v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGrpContentStart:I

    .line 535
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v11, v3

    .line 536
    iget v11, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v12, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float/2addr v11, v12

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mScrollable:F

    sub-float v0, v11, v12

    .line 537
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mExtraBtm:F

    cmpg-float v11, v0, v11

    if-gtz v11, :cond_b

    .line 538
    iget v6, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    .line 548
    :goto_4
    shl-int/lit8 v11, v3, 0x10

    or-int/2addr v11, v6

    iput v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mGrpContentEnd:I

    .line 549
    return-void

    .line 471
    .end local v0    # "exceedAmount":F
    .end local v6    # "inGrpIndex":I
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 495
    .restart local v0    # "exceedAmount":F
    :cond_5
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mMargTop:F

    add-float/2addr v11, v0

    neg-float v10, v11

    .line 496
    .local v10, "scrollExc":F
    const/4 v11, 0x0

    cmpg-float v11, v10, v11

    if-gtz v11, :cond_6

    .line 497
    const/4 v6, 0x0

    .restart local v6    # "inGrpIndex":I
    goto :goto_1

    .line 499
    .end local v6    # "inGrpIndex":I
    :cond_6
    iget v11, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    div-float v11, v10, v11

    float-to-int v6, v11

    .line 500
    .restart local v6    # "inGrpIndex":I
    iget v11, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    mul-int/2addr v6, v11

    goto :goto_1

    .line 510
    .end local v10    # "scrollExc":F
    :cond_7
    iget v11, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mValidH:F

    sub-float v12, v0, v12

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mMargBtm:F

    sub-float/2addr v12, v13

    sub-float v10, v11, v12

    .line 511
    .restart local v10    # "scrollExc":F
    const/4 v11, 0x0

    cmpg-float v11, v10, v11

    if-gtz v11, :cond_8

    .line 512
    const/4 v6, 0x0

    goto :goto_2

    .line 514
    :cond_8
    iget v11, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    div-float v11, v10, v11

    const/high16 v12, 0x3f800000    # 1.0f

    add-float/2addr v11, v12

    float-to-int v6, v11

    .line 515
    iget v11, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    mul-int/2addr v11, v6

    iget v12, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    invoke-static {v11, v12}, Ljava/lang/Math;->min(II)I

    move-result v6

    goto :goto_2

    .line 525
    .end local v10    # "scrollExc":F
    :cond_9
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mExtraTop:F

    sub-float v10, v11, v0

    .line 526
    .restart local v10    # "scrollExc":F
    const/4 v11, 0x0

    cmpg-float v11, v10, v11

    if-gtz v11, :cond_a

    .line 527
    const/4 v6, 0x0

    goto :goto_3

    .line 529
    :cond_a
    iget v11, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    div-float v11, v10, v11

    float-to-int v6, v11

    .line 530
    iget v11, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    mul-int/2addr v6, v11

    goto :goto_3

    .line 540
    .end local v10    # "scrollExc":F
    :cond_b
    iget v11, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mExtraBtm:F

    sub-float v12, v0, v12

    sub-float v10, v11, v12

    .line 541
    .restart local v10    # "scrollExc":F
    const/4 v11, 0x0

    cmpg-float v11, v10, v11

    if-gtz v11, :cond_c

    .line 542
    const/4 v6, 0x0

    goto :goto_4

    .line 544
    :cond_c
    iget v11, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    div-float v11, v10, v11

    const/high16 v12, 0x3f800000    # 1.0f

    add-float/2addr v11, v12

    float-to-int v6, v11

    .line 545
    iget v11, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    mul-int/2addr v11, v6

    iget v12, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    invoke-static {v11, v12}, Ljava/lang/Math;->min(II)I

    move-result v6

    goto :goto_4
.end method

.method public update(II)Z
    .locals 2
    .param p1, "index"    # I
    .param p2, "type"    # I

    .prologue
    .line 148
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mAlbumList:Z

    if-nez v1, :cond_0

    .line 149
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->update(II)Z

    move-result v1

    .line 156
    :goto_0
    return v1

    .line 151
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 152
    .local v0, "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-eqz v0, :cond_1

    .line 153
    const/4 v1, -0x2

    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->updateLeft(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;I)V

    .line 154
    const/4 v1, 0x1

    goto :goto_0

    .line 156
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public update(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)Z
    .locals 1
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .prologue
    .line 161
    if-nez p1, :cond_0

    .line 162
    const/4 v0, 0x0

    .line 168
    :goto_0
    return v0

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mAlbumList:Z

    if-nez v0, :cond_1

    .line 165
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->update(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)Z

    move-result v0

    goto :goto_0

    .line 167
    :cond_1
    const/4 v0, -0x2

    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->updateLeft(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;I)V

    .line 168
    const/4 v0, 0x1

    goto :goto_0
.end method
