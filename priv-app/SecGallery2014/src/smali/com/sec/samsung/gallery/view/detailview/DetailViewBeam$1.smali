.class Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$1;
.super Ljava/lang/Object;
.source "DetailViewBeam.java"

# interfaces
.implements Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->setBeamListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetFilePath()[Landroid/net/Uri;
    .locals 6

    .prologue
    .line 35
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    .line 36
    .local v1, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v0

    .line 37
    .local v0, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->access$100()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onGetFilePath(). Selected item count= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$AndroidBeamUtils;->toUriPathArray(Landroid/content/Context;Ljava/util/List;)[Landroid/net/Uri;

    move-result-object v2

    .line 41
    .local v2, "uriPathArray":[Landroid/net/Uri;
    return-object v2
.end method
