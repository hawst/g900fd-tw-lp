.class public Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;
.source "TimeViewPeopleActionBarForEdit.java"


# instance fields
.field private isNeedScanLastShareAPPSupport:Z

.field private mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

.field private mIsSelectAll:Z

.field private mMenu:Landroid/view/Menu;

.field protected mQuantitySelectedAlbum:I

.field protected mQuantitySelectedItem:I

.field private mSelectionCount:Landroid/widget/TextView;

.field private needScanLastShareAPPSupportOneMoreTime:Z

.field private outOfShareMaxItemLimit:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 43
    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;I)V

    .line 31
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mIsSelectAll:Z

    .line 34
    iput v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mQuantitySelectedAlbum:I

    .line 35
    iput v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mQuantitySelectedItem:I

    .line 38
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->isNeedScanLastShareAPPSupport:Z

    .line 39
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->needScanLastShareAPPSupportOneMoreTime:Z

    .line 40
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->outOfShareMaxItemLimit:Z

    .line 44
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->setSelectAllButtonTitle(ILjava/lang/String;)V

    return-void
.end method

.method private checkNeedScanLastShareAppSupport()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 223
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberItemOfMarkedAsSelected()I

    move-result v0

    .line 224
    .local v0, "numberMarkItem":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v3

    if-eqz v3, :cond_2

    if-le v0, v1, :cond_2

    sget v3, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    if-ge v0, v3, :cond_2

    .line 225
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->outOfShareMaxItemLimit:Z

    if-eqz v3, :cond_1

    .line 226
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->isNeedScanLastShareAPPSupport:Z

    .line 227
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->outOfShareMaxItemLimit:Z

    .line 238
    :goto_0
    sget v3, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    if-le v0, v3, :cond_0

    .line 239
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->outOfShareMaxItemLimit:Z

    .line 242
    :cond_0
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->isNeedScanLastShareAPPSupport:Z

    if-eqz v3, :cond_5

    .line 248
    :goto_1
    return v1

    .line 229
    :cond_1
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->isNeedScanLastShareAPPSupport:Z

    goto :goto_0

    .line 231
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v3

    if-eqz v3, :cond_4

    if-eqz v0, :cond_3

    if-ne v0, v1, :cond_4

    .line 232
    :cond_3
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->needScanLastShareAPPSupportOneMoreTime:Z

    .line 233
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->isNeedScanLastShareAPPSupport:Z

    goto :goto_0

    .line 235
    :cond_4
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->isNeedScanLastShareAPPSupport:Z

    goto :goto_0

    .line 244
    :cond_5
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->needScanLastShareAPPSupportOneMoreTime:Z

    if-eqz v3, :cond_6

    .line 245
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->needScanLastShareAPPSupportOneMoreTime:Z

    goto :goto_1

    :cond_6
    move v1, v2

    .line 248
    goto :goto_1
.end method

.method private getOptionMenuId()I
    .locals 1

    .prologue
    .line 204
    const v0, 0x7f12002c

    return v0
.end method

.method private isValidSelection()Z
    .locals 7

    .prologue
    .line 208
    const/4 v0, 0x1

    .line 210
    .local v0, "isValid":Z
    iget v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mQuantitySelectedAlbum:I

    if-nez v2, :cond_1

    .line 211
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0e0113

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 212
    const/4 v0, 0x0

    .line 219
    :cond_0
    :goto_0
    return v0

    .line 213
    :cond_1
    iget v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mQuantitySelectedItem:I

    sget v3, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    if-le v2, v3, :cond_0

    .line 214
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0114

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget v6, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 215
    .local v1, "text":Ljava/lang/CharSequence;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 216
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 49
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 50
    .local v0, "inflater":Landroid/view/MenuInflater;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->getOptionMenuId()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 51
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v3, 0x1

    .line 109
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 111
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->isValidSelection()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showShareDialog(Z)V

    goto :goto_0

    .line 116
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->isValidSelection()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 117
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_LAST_SHARE_APP:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 121
    :sswitch_2
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_DELETE_DIALOG:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 125
    :sswitch_3
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_CATEGORY:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 128
    :sswitch_4
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_SLIDESHOW_SETTING:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 131
    :sswitch_5
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_EDIT_CATEGORY:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 134
    :sswitch_6
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    sget-object v2, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;->CONFIRM_TAG:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    invoke-virtual {v1, v3, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showRemoveConfirmTagDialog(ZLcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;)V

    goto :goto_0

    .line 137
    :sswitch_7
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/16 v2, 0x904

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->startContactActivity(I)V

    goto :goto_0

    .line 140
    :sswitch_8
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    sget-object v2, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;->REMOVE_TAG:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    invoke-virtual {v1, v3, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showRemoveConfirmTagDialog(ZLcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;)V

    goto :goto_0

    .line 143
    :sswitch_9
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x0

    aput-object v1, v0, v3

    .line 144
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "S_STUDIO"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 147
    .end local v0    # "params":[Ljava/lang/Object;
    :sswitch_a
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->printImage()V

    goto/16 :goto_0

    .line 150
    :sswitch_b
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showDetailsDialog(Z)V

    goto/16 :goto_0

    .line 153
    :sswitch_c
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/16 v2, 0x905

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAddUserTagDialog(IZ)V

    goto/16 :goto_0

    .line 109
    :sswitch_data_0
    .sparse-switch
        0x7f0f026c -> :sswitch_0
        0x7f0f026d -> :sswitch_1
        0x7f0f026e -> :sswitch_2
        0x7f0f026f -> :sswitch_4
        0x7f0f0272 -> :sswitch_6
        0x7f0f0273 -> :sswitch_7
        0x7f0f0274 -> :sswitch_8
        0x7f0f027b -> :sswitch_3
        0x7f0f0282 -> :sswitch_c
        0x7f0f0283 -> :sswitch_9
        0x7f0f0285 -> :sswitch_a
        0x7f0f0297 -> :sswitch_3
        0x7f0f029b -> :sswitch_b
        0x7f0f02cb -> :sswitch_5
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 14
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v13, 0x7f0f026e

    const v12, 0x7f0f026c

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 55
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mMenu:Landroid/view/Menu;

    .line 56
    const v6, 0x7f0f02ca

    invoke-interface {p1, v6, v10}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 57
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->getSupportedOperationForSelectedItem()J

    move-result-wide v4

    .line 58
    .local v4, "supportOperation":J
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->checkNeedScanLastShareAppSupport()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 59
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mMenu:Landroid/view/Menu;

    invoke-static {v6, v7, v4, v5, v10}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkLastShareAppSupport(Landroid/content/Context;Landroid/view/Menu;JZ)J

    move-result-wide v4

    .line 63
    :goto_0
    invoke-static {p1, v4, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->updateMenuOperation(Landroid/view/Menu;J)V

    .line 65
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    .line 66
    .local v2, "isNothingSelected":Z
    if-eqz v2, :cond_1

    .line 67
    const v6, 0x7f0f02ca

    invoke-interface {p1, v6, v9}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 105
    :goto_1
    return-void

    .line 61
    .end local v2    # "isNothingSelected":Z
    :cond_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mMenu:Landroid/view/Menu;

    invoke-static {v6, v7, v4, v5, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkLastShareAppSupport(Landroid/content/Context;Landroid/view/Menu;JZ)J

    move-result-wide v4

    goto :goto_0

    .line 70
    .restart local v2    # "isNothingSelected":Z
    :cond_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v3

    .line 71
    .local v3, "type":Lcom/sec/samsung/gallery/core/TabTagType;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    sget-object v6, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v6, v3}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v7, v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 72
    .local v1, "category_name":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v7, 0x7f0e0463

    new-array v8, v10, [Ljava/lang/Object;

    aput-object v1, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "category_menu":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v7, 0x7f0f0297

    invoke-static {v6, v7, v0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemTitle(Landroid/view/Menu;ILjava/lang/String;)V

    .line 76
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-nez v6, :cond_2

    .line 77
    const v6, 0x7f0f0297

    invoke-static {p1, v6, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 78
    const v6, 0x7f0f02cb

    invoke-static {p1, v6, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 80
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 81
    const v6, 0x7f0f0285

    invoke-static {p1, v6, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 83
    :cond_3
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    if-ne v6, v10, :cond_9

    .line 84
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v6

    if-nez v6, :cond_4

    .line 85
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mMenu:Landroid/view/Menu;

    invoke-static {v6, v13, v11}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 87
    :cond_4
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 88
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mMenu:Landroid/view/Menu;

    invoke-static {v6, v12, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 89
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v7, 0x7f020030

    invoke-static {v6, v12, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIcon(Landroid/view/Menu;II)V

    .line 96
    :cond_5
    :goto_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/samsung/gallery/controller/SStudioCmd;->isSStudioAvailable(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 97
    :cond_6
    const v6, 0x7f0f0283

    invoke-static {p1, v6, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 99
    :cond_7
    const v6, 0x7f0f027b

    invoke-static {p1, v6, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 101
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-nez v6, :cond_8

    .line 102
    const v6, 0x7f0f0282

    invoke-static {p1, v6, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 104
    :cond_8
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v6

    const-string v7, "UPDATE_CONFIRM_MENU"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 92
    :cond_9
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v6

    if-nez v6, :cond_5

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v6

    if-nez v6, :cond_5

    .line 93
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mMenu:Landroid/view/Menu;

    invoke-static {v6, v13, v11}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    goto :goto_2
.end method

.method protected setSelectedItemCount(I)V
    .locals 0
    .param p1, "numberOfItemsSelected"    # I

    .prologue
    .line 190
    iput p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mQuantitySelectedItem:I

    .line 191
    return-void
.end method

.method protected setTitle(I)V
    .locals 2
    .param p1, "numberOfItemsSelected"    # I

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit$1;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 186
    return-void
.end method

.method public updateConfirm(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "format"    # Ljava/lang/String;

    .prologue
    .line 195
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mMenu:Landroid/view/Menu;

    if-eqz v1, :cond_0

    .line 196
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v2, 0x7f0f0272

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 197
    .local v0, "confirmAsMenu":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 198
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 201
    .end local v0    # "confirmAsMenu":Landroid/view/MenuItem;
    :cond_0
    return-void
.end method
