.class public Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
.super Landroid/app/AlertDialog;
.source "CustomProgressDialog.java"


# static fields
.field private static final MAX_PROGRESS_VALUE:I = 0x64


# instance fields
.field mClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mCountText:Landroid/widget/TextView;

.field private mCurrentCount:I

.field private mCurrentSize:J

.field private mHandler:Landroid/os/Handler;

.field private mListener:Landroid/content/DialogInterface$OnCancelListener;

.field private mMessage:Landroid/widget/TextView;

.field private mPercentRate:I

.field private mPercentText:Landroid/widget/TextView;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mTotalCount:I

.field private mTotalSize:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 37
    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 29
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mHandler:Landroid/os/Handler;

    .line 124
    new-instance v2, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog$1;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog$1;-><init>(Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 38
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mContext:Landroid/content/Context;

    .line 39
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 40
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030045

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 41
    .local v1, "mProgressView":Landroid/view/View;
    const v2, 0x7f0f00c1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    .line 42
    const v2, 0x7f0f00c2

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mCountText:Landroid/widget/TextView;

    .line 43
    const v2, 0x7f0f00c3

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mPercentText:Landroid/widget/TextView;

    .line 44
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setIcon(I)V

    .line 45
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setView(Landroid/view/View;)V

    .line 46
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 47
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 51
    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 29
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mHandler:Landroid/os/Handler;

    .line 124
    new-instance v2, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog$1;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog$1;-><init>(Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 52
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mContext:Landroid/content/Context;

    .line 53
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 54
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030046

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 55
    .local v1, "mProgressView":Landroid/view/View;
    const v2, 0x7f0f00c1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    .line 56
    const v2, 0x7f0f00c2

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mCountText:Landroid/widget/TextView;

    .line 57
    const v2, 0x7f0f00c3

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mPercentText:Landroid/widget/TextView;

    .line 58
    const v2, 0x7f0f00c4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mMessage:Landroid/widget/TextView;

    .line 59
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setIcon(I)V

    .line 60
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setView(Landroid/view/View;)V

    .line 61
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 62
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 63
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;)Landroid/content/DialogInterface$OnCancelListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mListener:Landroid/content/DialogInterface$OnCancelListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mPercentRate:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mCurrentCount:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mTotalCount:I

    return v0
.end method

.method private initCurrentStatus()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 133
    iput-wide v2, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mCurrentSize:J

    .line 134
    iput v1, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mCurrentCount:I

    .line 135
    iput v1, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mPercentRate:I

    .line 136
    const/4 v0, 0x1

    invoke-direct {p0, v2, v3, v0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->needUpdate(JZ)Z

    .line 137
    invoke-direct {p0, v2, v3, v1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->needUpdate(JZ)Z

    .line 138
    return-void
.end method

.method private needUpdate(JZ)Z
    .locals 9
    .param p1, "step"    # J
    .param p3, "isPercent"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 199
    if-eqz p3, :cond_3

    .line 200
    iget-wide v4, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mTotalSize:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-gtz v4, :cond_1

    .line 201
    iput v2, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mPercentRate:I

    .line 220
    :cond_0
    :goto_0
    return v2

    .line 204
    :cond_1
    iget-wide v4, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mCurrentSize:J

    add-long/2addr v4, p1

    iput-wide v4, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mCurrentSize:J

    .line 205
    iget-wide v4, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mCurrentSize:J

    const-wide/16 v6, 0x64

    mul-long/2addr v4, v6

    iget-wide v6, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mTotalSize:J

    div-long/2addr v4, v6

    long-to-int v1, v4

    .line 206
    .local v1, "rate":I
    iget v4, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mPercentRate:I

    if-le v1, v4, :cond_0

    .line 207
    const/16 v2, 0x64

    if-le v1, v2, :cond_2

    .line 208
    const/16 v1, 0x64

    .line 210
    :cond_2
    iput v1, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mPercentRate:I

    move v2, v3

    .line 211
    goto :goto_0

    .line 213
    .end local v1    # "rate":I
    :cond_3
    iget v4, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mTotalCount:I

    if-gtz v4, :cond_4

    .line 214
    iput v2, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mCurrentCount:I

    goto :goto_0

    .line 217
    :cond_4
    iget v2, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mCurrentCount:I

    int-to-long v4, v2

    add-long/2addr v4, p1

    long-to-int v0, v4

    .line 218
    .local v0, "count":I
    iget v2, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mTotalCount:I

    if-le v0, v2, :cond_5

    iget v0, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mTotalCount:I

    .line 219
    :cond_5
    iput v0, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mCurrentCount:I

    move v2, v3

    .line 220
    goto :goto_0
.end method


# virtual methods
.method public closeCustomProgressDialog()V
    .locals 1

    .prologue
    .line 141
    const/16 v0, 0x3e8

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->closeCustomProgressDialog(I)V

    .line 142
    return-void
.end method

.method public closeCustomProgressDialog(I)V
    .locals 4
    .param p1, "delay"    # I

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog$2;-><init>(Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;)V

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 157
    :cond_0
    return-void
.end method

.method public increaseProgress(JZ)V
    .locals 1
    .param p1, "step"    # J
    .param p3, "isFileSize"    # Z

    .prologue
    .line 170
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->increaseProgress(JZZ)V

    .line 171
    return-void
.end method

.method public increaseProgress(JZZ)V
    .locals 3
    .param p1, "step"    # J
    .param p3, "isFileSize"    # Z
    .param p4, "forceUpdate"    # Z

    .prologue
    .line 174
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->needUpdate(JZ)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p4, :cond_0

    .line 196
    :goto_0
    return-void

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog$3;

    invoke-direct {v1, p0, p3}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog$3;-><init>(Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method setCountText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mCountText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mCountText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    :cond_0
    return-void
.end method

.method setPercentText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mPercentText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mPercentText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    :cond_0
    return-void
.end method

.method setProcessing(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 69
    :cond_0
    return-void
.end method

.method public setTotalCount(I)V
    .locals 4
    .param p1, "count"    # I

    .prologue
    .line 160
    iput p1, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mTotalCount:I

    .line 161
    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->increaseProgress(JZZ)V

    .line 162
    return-void
.end method

.method public setTotalSize(J)V
    .locals 3
    .param p1, "size"    # J

    .prologue
    const/4 v2, 0x1

    .line 165
    iput-wide p1, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mTotalSize:J

    .line 166
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1, v2, v2}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->increaseProgress(JZZ)V

    .line 167
    return-void
.end method

.method public showProgressDialog(Ljava/lang/String;Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V
    .locals 4
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "cancellable"    # Z
    .param p4, "cancelListener"    # Landroid/content/DialogInterface$OnCancelListener;

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 87
    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 88
    invoke-virtual {p0, p3}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setCancelable(Z)V

    .line 89
    const/4 v1, -0x2

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mContext:Landroid/content/Context;

    const v3, 0x7f0e0046

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mClickListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 91
    iput-object p4, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 92
    invoke-virtual {p0, p4}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 93
    invoke-static {p0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->hideStatusIcon(Ljava/lang/Object;)V

    .line 94
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->initCurrentStatus()V

    .line 96
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->show()V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Landroid/view/WindowManager$BadTokenException;
    invoke-virtual {v0}, Landroid/view/WindowManager$BadTokenException;->printStackTrace()V

    .line 99
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mListener:Landroid/content/DialogInterface$OnCancelListener;

    if-eqz v1, :cond_0

    .line 100
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mListener:Landroid/content/DialogInterface$OnCancelListener;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/content/DialogInterface$OnCancelListener;->onCancel(Landroid/content/DialogInterface;)V

    goto :goto_0
.end method

.method public showProgressDialog(Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V
    .locals 4
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "cancellable"    # Z
    .param p3, "cancelListener"    # Landroid/content/DialogInterface$OnCancelListener;

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 108
    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setCancelable(Z)V

    .line 109
    const/4 v1, -0x2

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mContext:Landroid/content/Context;

    const v3, 0x7f0e0046

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mClickListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 111
    iput-object p3, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 112
    invoke-virtual {p0, p3}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 113
    invoke-static {p0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->hideStatusIcon(Ljava/lang/Object;)V

    .line 114
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->initCurrentStatus()V

    .line 116
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->show()V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 117
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Landroid/view/WindowManager$BadTokenException;
    invoke-virtual {v0}, Landroid/view/WindowManager$BadTokenException;->printStackTrace()V

    .line 119
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mListener:Landroid/content/DialogInterface$OnCancelListener;

    if-eqz v1, :cond_0

    .line 120
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->mListener:Landroid/content/DialogInterface$OnCancelListener;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/content/DialogInterface$OnCancelListener;->onCancel(Landroid/content/DialogInterface;)V

    goto :goto_0
.end method
