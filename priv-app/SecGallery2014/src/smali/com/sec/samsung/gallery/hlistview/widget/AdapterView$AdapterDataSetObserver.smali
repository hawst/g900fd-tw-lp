.class Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;
.super Landroid/database/DataSetObserver;
.source "AdapterView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AdapterDataSetObserver"
.end annotation


# instance fields
.field private mInstanceState:Landroid/os/Parcelable;

.field final synthetic this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;)V
    .locals 1

    .prologue
    .line 800
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>.AdapterDataSetObserver;"
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    .line 802
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->mInstanceState:Landroid/os/Parcelable;

    return-void
.end method


# virtual methods
.method public clearSavedState()V
    .locals 1

    .prologue
    .line 851
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>.AdapterDataSetObserver;"
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->mInstanceState:Landroid/os/Parcelable;

    .line 852
    return-void
.end method

.method public onChanged()V
    .locals 2

    .prologue
    .line 806
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>.AdapterDataSetObserver;"
    const-string v0, "View"

    const-string v1, "onChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 807
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mDataChanged:Z

    .line 808
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    iget v1, v1, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mItemCount:I

    iput v1, v0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOldItemCount:I

    .line 809
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/Adapter;->getCount()I

    move-result v1

    iput v1, v0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mItemCount:I

    .line 813
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->mInstanceState:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOldItemCount:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mItemCount:I

    if-lez v0, :cond_0

    .line 815
    const-string v0, "View"

    const-string v1, "calling onRestoreInstanceState"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 816
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->mInstanceState:Landroid/os/Parcelable;

    # invokes: Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->onRestoreInstanceState(Landroid/os/Parcelable;)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->access$000(Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;Landroid/os/Parcelable;)V

    .line 817
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->mInstanceState:Landroid/os/Parcelable;

    .line 822
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->checkFocus()V

    .line 823
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->requestLayout()V

    .line 824
    return-void

    .line 819
    :cond_0
    const-string v0, "View"

    const-string v1, "else calling rememberSyncState"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 820
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->rememberSyncState()V

    goto :goto_0
.end method

.method public onInvalidated()V
    .locals 6

    .prologue
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>.AdapterDataSetObserver;"
    const-wide/high16 v4, -0x8000000000000000L

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 828
    const-string v0, "View"

    const-string v1, "onInvalidated"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 829
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mDataChanged:Z

    .line 831
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 834
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    # invokes: Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->onSaveInstanceState()Landroid/os/Parcelable;
    invoke-static {v0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->access$100(Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->mInstanceState:Landroid/os/Parcelable;

    .line 838
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    iget v1, v1, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mItemCount:I

    iput v1, v0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOldItemCount:I

    .line 839
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    iput v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mItemCount:I

    .line 840
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    iput v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSelectedPosition:I

    .line 841
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    iput-wide v4, v0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSelectedColId:J

    .line 842
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    iput v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNextSelectedPosition:I

    .line 843
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    iput-wide v4, v0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNextSelectedColId:J

    .line 844
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNeedSync:Z

    .line 846
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->checkFocus()V

    .line 847
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->requestLayout()V

    .line 848
    return-void
.end method
