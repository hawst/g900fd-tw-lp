.class public Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;
.super Landroid/view/accessibility/AccessibilityNodeProvider;
.source "GlAccessibilityNodeProvider.java"


# instance fields
.field currentSelectedItem:Lcom/sec/android/gallery3d/glcore/GlObject;

.field private mContext:Landroid/content/Context;

.field private mFocused:I

.field private mView:Lcom/sec/android/gallery3d/glcore/GlRootView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlRootView;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/view/accessibility/AccessibilityNodeProvider;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mFocused:I

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->currentSelectedItem:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 33
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 34
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mContext:Landroid/content/Context;

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;)Lcom/sec/android/gallery3d/glcore/GlRootView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    return-object v0
.end method


# virtual methods
.method public clearAccessibilityFocus()V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->currentSelectedItem:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->currentSelectedItem:Lcom/sec/android/gallery3d/glcore/GlObject;

    const/high16 v1, 0x10000

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->sendAccessibilityEventForVirtualView(Lcom/sec/android/gallery3d/glcore/GlObject;I)Z

    .line 175
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->currentSelectedItem:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 176
    return-void
.end method

.method public createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;
    .locals 5
    .param p1, "virtualDescendantId"    # I

    .prologue
    .line 87
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 89
    const/4 v1, 0x0

    .line 91
    .local v1, "info":Landroid/view/accessibility/AccessibilityNodeInfo;
    :try_start_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    .line 92
    .local v2, "viewStateProxy":Lcom/sec/android/gallery3d/app/StateManager;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getRootLayer()Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v3

    instance-of v3, v3, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-nez v3, :cond_1

    .line 93
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getRootLayer()Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v1

    .line 94
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->invalidate()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    .line 104
    .end local v1    # "info":Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v2    # "viewStateProxy":Lcom/sec/android/gallery3d/app/StateManager;
    :goto_1
    return-object v1

    .line 95
    .restart local v1    # "info":Landroid/view/accessibility/AccessibilityNodeInfo;
    .restart local v2    # "viewStateProxy":Lcom/sec/android/gallery3d/app/StateManager;
    :cond_1
    :try_start_1
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v3

    instance-of v3, v3, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-eqz v3, :cond_0

    .line 96
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContentPane()Lcom/sec/android/gallery3d/ui/GLView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v3, p1, v4}, Lcom/sec/android/gallery3d/ui/GLView;->createAccessibilityNodeInfo(ILcom/sec/android/gallery3d/glcore/GlRootView;)Landroid/view/accessibility/AccessibilityNodeInfo;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_0

    .line 99
    .end local v2    # "viewStateProxy":Lcom/sec/android/gallery3d/app/StateManager;
    :catch_0
    move-exception v0

    .line 100
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 102
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    .line 104
    const/4 v1, 0x0

    goto :goto_1

    .line 102
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    throw v3
.end method

.method public findAccessibilityNodeInfosByText(Ljava/lang/String;I)Ljava/util/List;
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "virtualDescendantId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFocusedIndex()I
    .locals 1

    .prologue
    .line 183
    iget v0, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mFocused:I

    return v0
.end method

.method public performAction(IILandroid/os/Bundle;)Z
    .locals 6
    .param p1, "virtualViewId"    # I
    .param p2, "action"    # I
    .param p3, "arguments"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 39
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 41
    if-ne p1, v5, :cond_1

    .line 42
    const/16 v4, 0x40

    if-ne p2, v4, :cond_0

    .line 75
    :goto_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    .line 77
    :goto_1
    return v2

    :cond_0
    move v2, v3

    .line 42
    goto :goto_0

    .line 43
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getRootLayer()Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 44
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getRootLayer()Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-result-object v2

    invoke-virtual {v2, p1, p2, p3}, Lcom/sec/android/gallery3d/glcore/GlLayer;->performAction(IILandroid/os/Bundle;)Z

    move-result v1

    .line 45
    .local v1, "result":Z
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->invalidate()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    move v2, v1

    goto :goto_1

    .line 48
    .end local v1    # "result":Z
    :cond_2
    sparse-switch p2, :sswitch_data_0

    .line 75
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    move v2, v3

    goto :goto_1

    .line 50
    :sswitch_0
    :try_start_1
    iget v4, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mFocused:I

    if-eq v4, p1, :cond_4

    .line 51
    iput p1, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mFocused:I

    .line 52
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContentPane()Lcom/sec/android/gallery3d/ui/GLView;

    move-result-object v4

    invoke-virtual {v4, p1, p2, p3}, Lcom/sec/android/gallery3d/ui/GLView;->performAction(IILandroid/os/Bundle;)Z

    .line 53
    const v4, 0x8000

    invoke-virtual {p0, p1, v4}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->sendAccessibilityEventForVirtualView(II)Z

    .line 57
    :cond_3
    :goto_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlRootView;->invalidate()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    goto :goto_1

    .line 54
    :cond_4
    :try_start_2
    iget v4, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mFocused:I

    if-nez v4, :cond_3

    if-nez p1, :cond_3

    .line 55
    const v4, 0x8000

    invoke-virtual {p0, p1, v4}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->sendAccessibilityEventForVirtualView(II)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 72
    :catch_0
    move-exception v0

    .line 73
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 75
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    move v2, v3

    .line 77
    goto :goto_1

    .line 61
    .end local v0    # "e":Ljava/lang/Exception;
    :sswitch_1
    :try_start_4
    iget v4, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mFocused:I

    if-ne v4, p1, :cond_5

    .line 62
    const/4 v4, -0x1

    iput v4, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mFocused:I

    .line 63
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContentPane()Lcom/sec/android/gallery3d/ui/GLView;

    move-result-object v4

    invoke-virtual {v4, p1, p2, p3}, Lcom/sec/android/gallery3d/ui/GLView;->performAction(IILandroid/os/Bundle;)Z

    .line 64
    iget v4, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mFocused:I

    const/high16 v5, 0x10000

    invoke-virtual {p0, v4, v5}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->sendAccessibilityEventForVirtualView(II)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 75
    :cond_5
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    goto :goto_1

    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    throw v2

    .line 48
    :sswitch_data_0
    .sparse-switch
        0x40 -> :sswitch_0
        0x80 -> :sswitch_1
    .end sparse-switch
.end method

.method public refreshAccessibilityFocus()V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mContext:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 159
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider$2;-><init>(Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public refreshAccessibilityFocus(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider$3;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider$3;-><init>(Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 170
    return-void
.end method

.method public sendAccessibilityEventForVirtualView(I)Z
    .locals 1
    .param p1, "eventType"    # I

    .prologue
    .line 131
    iget v0, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mFocused:I

    invoke-virtual {p0, v0, p1}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->sendAccessibilityEventForVirtualView(II)Z

    move-result v0

    return v0
.end method

.method public sendAccessibilityEventForVirtualView(II)Z
    .locals 3
    .param p1, "index"    # I
    .param p2, "eventType"    # I

    .prologue
    .line 135
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isExploreByTouchEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 136
    invoke-static {p2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 137
    .local v0, "event":Landroid/view/accessibility/AccessibilityEvent;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    .line 138
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    .line 139
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContentPane()Lcom/sec/android/gallery3d/ui/GLView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 140
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContentPane()Lcom/sec/android/gallery3d/ui/GLView;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 141
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, v1, p1}, Landroid/view/accessibility/AccessibilityEvent;->setSource(Landroid/view/View;I)V

    .line 142
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-interface {v1, v2, v0}, Landroid/view/ViewParent;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v1

    .line 144
    .end local v0    # "event":Landroid/view/accessibility/AccessibilityEvent;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public sendAccessibilityEventForVirtualView(Lcom/sec/android/gallery3d/glcore/GlObject;I)Z
    .locals 3
    .param p1, "view"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "eventType"    # I

    .prologue
    .line 108
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isExploreByTouchEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 109
    invoke-static {p2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 110
    .local v0, "event":Landroid/view/accessibility/AccessibilityEvent;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    .line 111
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 112
    instance-of v1, p1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-nez v1, :cond_0

    instance-of v1, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    if-eqz v1, :cond_2

    .line 113
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    check-cast p1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .end local p1    # "view":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget v2, p1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    invoke-virtual {v0, v1, v2}, Landroid/view/accessibility/AccessibilityEvent;->setSource(Landroid/view/View;I)V

    .line 117
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider$1;

    invoke-direct {v2, p0, v0}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider$1;-><init>(Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;Landroid/view/accessibility/AccessibilityEvent;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 125
    const/4 v1, 0x1

    .line 127
    .end local v0    # "event":Landroid/view/accessibility/AccessibilityEvent;
    :goto_1
    return v1

    .line 114
    .restart local v0    # "event":Landroid/view/accessibility/AccessibilityEvent;
    .restart local p1    # "view":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_2
    instance-of v1, p1, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-eqz v1, :cond_1

    .line 115
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    check-cast p1, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .end local p1    # "view":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget v2, p1, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    invoke-virtual {v0, v1, v2}, Landroid/view/accessibility/AccessibilityEvent;->setSource(Landroid/view/View;I)V

    goto :goto_0

    .line 127
    .end local v0    # "event":Landroid/view/accessibility/AccessibilityEvent;
    .restart local p1    # "view":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public setCurrentSelectedItem(Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 0
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->currentSelectedItem:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 180
    return-void
.end method

.method public setFocusedIndex(I)V
    .locals 0
    .param p1, "focused"    # I

    .prologue
    .line 187
    iput p1, p0, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->mFocused:I

    .line 188
    return-void
.end method
