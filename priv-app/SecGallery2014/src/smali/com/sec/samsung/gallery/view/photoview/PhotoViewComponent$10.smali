.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$10;
.super Ljava/lang/Object;
.source "PhotoViewComponent.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/OnProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->addToSlideshowProxy(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

.field final synthetic val$burstPlay:Z


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Z)V
    .locals 0

    .prologue
    .line 1667
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$10;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iput-boolean p2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$10;->val$burstPlay:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 1
    .param p1, "object"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 1680
    const/4 v0, 0x0

    return v0
.end method

.method public handleOperation(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1685
    .local p1, "objectList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public onCompleted(Z)V
    .locals 2
    .param p1, "result"    # Z

    .prologue
    .line 1674
    if-eqz p1, :cond_0

    .line 1675
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$10;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$10;->val$burstPlay:Z

    # invokes: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->startSlideShow(Z)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2400(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Z)V

    .line 1676
    :cond_0
    return-void
.end method

.method public onProgress(II)V
    .locals 0
    .param p1, "progress"    # I
    .param p2, "total"    # I

    .prologue
    .line 1670
    return-void
.end method
