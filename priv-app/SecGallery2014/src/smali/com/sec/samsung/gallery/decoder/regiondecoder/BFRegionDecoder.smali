.class public Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;
.super Ljava/lang/Object;
.source "BFRegionDecoder.java"


# instance fields
.field private mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getNativeInstance(Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;)Ljava/lang/Object;
    .locals 1
    .param p0, "regionDecoder"    # Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    return-object v0
.end method

.method public static newInstance(Ljava/io/FileDescriptor;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;
    .locals 3
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .param p1, "isShareable"    # Z

    .prologue
    .line 88
    new-instance v1, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    invoke-direct {v1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;-><init>()V

    .line 90
    .local v1, "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;
    :try_start_0
    invoke-static {p0, p1}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/io/FileDescriptor;Z)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :goto_0
    iget-object v2, v1, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v2, :cond_0

    .line 99
    .end local v1    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;
    :goto_1
    return-object v1

    .line 91
    .restart local v1    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;
    :catch_0
    move-exception v0

    .line 93
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 99
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static newInstance(Ljava/io/InputStream;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;
    .locals 3
    .param p0, "is"    # Ljava/io/InputStream;
    .param p1, "isShareable"    # Z

    .prologue
    .line 72
    new-instance v1, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    invoke-direct {v1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;-><init>()V

    .line 74
    .local v1, "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;
    :try_start_0
    invoke-static {p0, p1}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/io/InputStream;Z)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :goto_0
    iget-object v2, v1, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v2, :cond_0

    .line 83
    .end local v1    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;
    :goto_1
    return-object v1

    .line 75
    .restart local v1    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;
    :catch_0
    move-exception v0

    .line 77
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 83
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static newInstance(Ljava/lang/String;Z)Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;
    .locals 3
    .param p0, "pathName"    # Ljava/lang/String;
    .param p1, "isShareable"    # Z

    .prologue
    .line 56
    new-instance v1, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    invoke-direct {v1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;-><init>()V

    .line 58
    .local v1, "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;
    :try_start_0
    invoke-static {p0, p1}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/lang/String;Z)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :goto_0
    iget-object v2, v1, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v2, :cond_0

    .line 67
    .end local v1    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;
    :goto_1
    return-object v1

    .line 59
    .restart local v1    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 67
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static newInstance([BIIZ)Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;
    .locals 3
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "isShareable"    # Z

    .prologue
    .line 104
    new-instance v1, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;

    invoke-direct {v1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;-><init>()V

    .line 106
    .local v1, "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;
    :try_start_0
    invoke-static {p0, p1, p2, p3}, Landroid/graphics/BitmapRegionDecoder;->newInstance([BIIZ)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    :goto_0
    iget-object v2, v1, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v2, :cond_0

    .line 115
    .end local v1    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;
    :goto_1
    return-object v1

    .line 107
    .restart local v1    # "rdInstance":Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;
    :catch_0
    move-exception v0

    .line 109
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 115
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "rect"    # Landroid/graphics/Rect;
    .param p2, "options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/BitmapRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getHeight()I
    .locals 2

    .prologue
    .line 36
    const/4 v0, 0x0

    .line 37
    .local v0, "result":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v1}, Landroid/graphics/BitmapRegionDecoder;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 38
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v1}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v0

    .line 40
    :cond_0
    return v0
.end method

.method public getWidth()I
    .locals 2

    .prologue
    .line 44
    const/4 v0, 0x0

    .line 45
    .local v0, "result":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v1}, Landroid/graphics/BitmapRegionDecoder;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 46
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v1}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v0

    .line 48
    :cond_0
    return v0
.end method

.method public final isRecycled()Z
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->isRecycled()Z

    move-result v0

    return v0
.end method

.method public recycle()V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/regiondecoder/BFRegionDecoder;->mBitmapRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->recycle()V

    .line 125
    return-void
.end method
