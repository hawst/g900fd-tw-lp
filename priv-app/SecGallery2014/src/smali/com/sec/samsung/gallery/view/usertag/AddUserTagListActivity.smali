.class public Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;
.super Landroid/app/Activity;
.source "AddUserTagListActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$AddTagTask;,
        Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;
    }
.end annotation


# static fields
.field public static final FILES_URI:Landroid/net/Uri;

.field private static final MSG_UPDATE_ACTIONBAR:I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private allTags:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAddTagEditActionbar:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

.field private mCheckedData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mIsDetailView:Z

.field private mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

.field private mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mTagListAdater:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;

.field private mTagListView:Landroid/widget/ListView;

.field private mUriList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mUserTagEditView:Landroid/view/View;

.field private mUserTagList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private noItemsView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->TAG:Ljava/lang/String;

    .line 68
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->FILES_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUriList:Ljava/util/ArrayList;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUserTagList:Ljava/util/ArrayList;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mCheckedData:Ljava/util/ArrayList;

    .line 70
    new-instance v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$1;-><init>(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

    .line 90
    new-instance v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$2;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$2;-><init>(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mHandler:Landroid/os/Handler;

    .line 559
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mAddTagEditActionbar:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mTagListAdater:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mIsDetailView:Z

    return v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUserTagList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mCheckedData:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUriList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private getMediaEjectFilter()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 146
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 147
    .local v0, "iFilter":Landroid/content/IntentFilter;
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 148
    return-object v0
.end method

.method private initView()V
    .locals 13

    .prologue
    const/4 v10, 0x0

    const/4 v12, 0x1

    const/4 v11, -0x1

    .line 214
    const v9, 0x7f03001b

    invoke-virtual {p0, v9}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->setContentView(I)V

    .line 216
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    check-cast v9, Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 217
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v9, 0x7f0f0056

    invoke-virtual {p0, v9}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    .line 219
    .local v7, "subContainer":Landroid/widget/RelativeLayout;
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getAllUserTag(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 220
    const v9, 0x7f0f01b5

    invoke-virtual {v7, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->noItemsView:Landroid/view/View;

    .line 221
    const v9, 0x7f03009f

    invoke-virtual {v1, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->noItemsView:Landroid/view/View;

    .line 222
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->noItemsView:Landroid/view/View;

    new-instance v10, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v10, v11, v11}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v9, v10}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 224
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->noItemsView:Landroid/view/View;

    invoke-virtual {v7, v9}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 225
    invoke-direct {p0, v12}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->setVisibility(Z)V

    .line 271
    :goto_0
    return-void

    .line 228
    :cond_0
    const v9, 0x7f03009c

    invoke-virtual {v1, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUserTagEditView:Landroid/view/View;

    .line 229
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUserTagEditView:Landroid/view/View;

    new-instance v10, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v10, v11, v11}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v9, v10}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 231
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUserTagEditView:Landroid/view/View;

    invoke-virtual {v7, v9}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 233
    const v9, 0x7f0f0057

    invoke-virtual {p0, v9}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 234
    .local v6, "scrollView":Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 235
    .local v0, "background":Landroid/graphics/drawable/Drawable;
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUserTagEditView:Landroid/view/View;

    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    .end local v0    # "background":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/view/View;->setBackgroundColor(I)V

    .line 236
    new-instance v9, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUserTagList:Ljava/util/ArrayList;

    invoke-direct {v9, p0, v10, v11}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;-><init>(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v9, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mTagListAdater:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;

    .line 237
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUserTagEditView:Landroid/view/View;

    const v10, 0x7f0f01a9

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ListView;

    iput-object v9, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mTagListView:Landroid/widget/ListView;

    .line 239
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mTagListView:Landroid/widget/ListView;

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mTagListAdater:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 240
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mTagListView:Landroid/widget/ListView;

    new-instance v10, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$3;

    invoke-direct {v10, p0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$3;-><init>(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;)V

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 258
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 259
    .local v5, "resources":Landroid/content/res/Resources;
    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v2, v9, Landroid/content/res/Configuration;->orientation:I

    .line 260
    .local v2, "orientation":I
    if-ne v2, v12, :cond_1

    .line 261
    const v9, 0x7f0d009f

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 263
    .local v8, "tagPaddingTop":I
    const v9, 0x7f0d0081

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 265
    .local v3, "paddingLeft":I
    const v9, 0x7f0d0080

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 267
    .local v4, "paddingRight":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUserTagEditView:Landroid/view/View;

    const/4 v10, 0x0

    invoke-virtual {v9, v3, v8, v4, v10}, Landroid/view/View;->setPadding(IIII)V

    .line 269
    .end local v3    # "paddingLeft":I
    .end local v4    # "paddingRight":I
    .end local v8    # "tagPaddingTop":I
    :cond_1
    invoke-direct {p0, v12}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->setVisibility(Z)V

    goto/16 :goto_0
.end method

.method private setVisibility(Z)V
    .locals 7
    .param p1, "userTagEdit"    # Z

    .prologue
    const/16 v5, 0x8

    const/4 v6, 0x0

    .line 364
    const v4, 0x7f0f0056

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 365
    .local v2, "subContainer":Landroid/widget/RelativeLayout;
    const v4, 0x7f0f0057

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 366
    .local v1, "scrollView":Landroid/view/View;
    if-eqz p1, :cond_0

    move v4, v5

    :goto_0
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 367
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getAllUserTag(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 368
    const v4, 0x7f0f01b5

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 369
    .local v0, "noItemsView":Landroid/view/View;
    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 374
    .end local v0    # "noItemsView":Landroid/view/View;
    :goto_2
    return-void

    :cond_0
    move v4, v6

    .line 366
    goto :goto_0

    .restart local v0    # "noItemsView":Landroid/view/View;
    :cond_1
    move v6, v5

    .line 369
    goto :goto_1

    .line 371
    .end local v0    # "noItemsView":Landroid/view/View;
    :cond_2
    const v4, 0x7f0f01a8

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 372
    .local v3, "userTagEditView":Landroid/view/View;
    if-eqz p1, :cond_3

    :goto_3
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_3
    move v6, v5

    goto :goto_3
.end method

.method private startHandleAddTag()V
    .locals 10

    .prologue
    const v9, 0x7f0e04a8

    const v8, 0x7f0e0217

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 513
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUriList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 550
    :cond_0
    :goto_0
    return-void

    .line 517
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mTagListAdater:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->getData()Ljava/util/ArrayList;

    move-result-object v2

    .line 519
    .local v2, "selectedUserTagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v3, v7, :cond_2

    .line 520
    new-instance v3, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$AddTagTask;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUriList:Ljava/util/ArrayList;

    invoke-direct {v3, p0, v4, v5, v2}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$AddTagTask;-><init>(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    new-array v4, v6, [Ljava/lang/Void;

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$AddTagTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 522
    :cond_2
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAddTagAsMoreinfo:Z

    if-eqz v3, :cond_6

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mIsDetailView:Z

    if-eqz v3, :cond_6

    .line 523
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->isDataSetChanged()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 526
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUserTagList:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mCheckedData:Ljava/util/ArrayList;

    invoke-static {v4, v3, v5, v6}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->updateUserTag(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 527
    sget-object v3, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->TAG:Ljava/lang/String;

    const-string v4, "Error during update UserTag"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v7, :cond_5

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mCheckedData:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUserTagList:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 530
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    invoke-static {v3, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 535
    :cond_4
    :goto_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUserTagList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 536
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUserTagList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mCheckedData:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 547
    :goto_2
    const/4 v3, -0x1

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->setResult(I)V

    .line 548
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->finish()V

    goto :goto_0

    .line 532
    :cond_5
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_4

    .line 533
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    invoke-static {v3, v9}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_1

    .line 538
    :cond_6
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getUserTag(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 539
    .local v1, "oldTags":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUriList:Ljava/util/ArrayList;

    invoke-static {v3, v4, v2}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->addUserTagInSelectionMode(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    .line 540
    invoke-static {v2}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->makeStringForType(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    .line 541
    .local v0, "newTags":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v7, :cond_7

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 542
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    invoke-static {v3, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_2

    .line 544
    :cond_7
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    invoke-static {v3, v9}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_2
.end method


# virtual methods
.method public editActionCancel()V
    .locals 1

    .prologue
    .line 360
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->setVisibility(Z)V

    .line 361
    return-void
.end method

.method public editActionDone()V
    .locals 1

    .prologue
    .line 356
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->setVisibility(Z)V

    .line 357
    return-void
.end method

.method protected isDataSetChanged()Z
    .locals 2

    .prologue
    .line 553
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mCheckedData:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUserTagList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUserTagList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mCheckedData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 554
    const/4 v0, 0x0

    .line 556
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->finish()V

    .line 184
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 12
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    const v11, 0x7f0d009f

    const/4 v10, 0x0

    .line 300
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 301
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 302
    .local v6, "resources":Landroid/content/res/Resources;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getAllUserTag(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 303
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->noItemsView:Landroid/view/View;

    const v9, 0x7f0f01b7

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 305
    .local v0, "mNoItemBg":Landroid/widget/ImageView;
    iget v8, p1, Landroid/content/res/Configuration;->orientation:I

    packed-switch v8, :pswitch_data_0

    .line 348
    .end local v0    # "mNoItemBg":Landroid/widget/ImageView;
    :goto_0
    return-void

    .line 307
    .restart local v0    # "mNoItemBg":Landroid/widget/ImageView;
    :pswitch_0
    const v8, 0x7f020520

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 308
    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 310
    .local v4, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const v8, 0x7f0d037e

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, v4, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 312
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 316
    .end local v4    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :pswitch_1
    const v8, 0x7f020521

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 317
    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 319
    .local v5, "params2":Landroid/widget/RelativeLayout$LayoutParams;
    const v8, 0x7f0d037f

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, v5, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 321
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 328
    .end local v0    # "mNoItemBg":Landroid/widget/ImageView;
    .end local v5    # "params2":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUserTagEditView:Landroid/view/View;

    const v9, 0x7f0f01a8

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 330
    .local v7, "userTagEditView":Landroid/view/View;
    if-eqz p1, :cond_1

    iget v8, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_1

    .line 331
    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 333
    .local v3, "paddingTop":I
    const v8, 0x7f0d0081

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 335
    .local v1, "paddingLeft":I
    const v8, 0x7f0d0080

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 337
    .local v2, "paddingRight":I
    invoke-virtual {v7, v1, v3, v2, v10}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    .line 339
    .end local v1    # "paddingLeft":I
    .end local v2    # "paddingRight":I
    .end local v3    # "paddingTop":I
    :cond_1
    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 341
    .restart local v3    # "paddingTop":I
    const v8, 0x7f0d007f

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 343
    .restart local v1    # "paddingLeft":I
    const v8, 0x7f0d007e

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 345
    .restart local v2    # "paddingRight":I
    invoke-virtual {v7, v1, v3, v2, v10}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    .line 305
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/16 v3, 0x400

    .line 120
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 122
    iput-object p0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    .line 123
    const v2, 0x7f110039

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->setTheme(I)V

    .line 125
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFullScreen(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 128
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->hideStatusIcon(Ljava/lang/Object;)V

    .line 130
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mMediaEjectReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->getMediaEjectFilter()Landroid/content/IntentFilter;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 132
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 133
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 134
    const-string/jumbo v2, "uriList"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 135
    .local v1, "uriArray":[Ljava/lang/String;
    const-string v2, "isDetailView"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mIsDetailView:Z

    .line 136
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mUriList:Ljava/util/ArrayList;

    .line 138
    .end local v1    # "uriArray":[Ljava/lang/String;
    :cond_1
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->setSavedUserTag(Z)V

    .line 140
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->initView()V

    .line 141
    new-instance v2, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;-><init>(Landroid/app/Activity;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mAddTagEditActionbar:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    .line 143
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 153
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 154
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mAddTagEditActionbar:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 155
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 210
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 211
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 167
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 168
    .local v0, "id":I
    sparse-switch v0, :sswitch_data_0

    .line 178
    :goto_0
    return v1

    .line 170
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->finish()V

    goto :goto_0

    .line 174
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->startHandleAddTag()V

    .line 175
    invoke-static {v1}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->setSavedUserTag(Z)V

    goto :goto_0

    .line 168
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0f0025 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 203
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 204
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mAddTagEditActionbar:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->onPause()V

    .line 205
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 160
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 161
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mAddTagEditActionbar:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getAllUserTag(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->onPrepareOptionsMenu(Landroid/view/Menu;Z)V

    .line 162
    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 195
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 196
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mAddTagEditActionbar:Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->onResume()V

    .line 197
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->updateActionBar()V

    .line 198
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 189
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 190
    return-void
.end method

.method public selectAll()V
    .locals 4

    .prologue
    .line 275
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getAllUserTag(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->allTags:Ljava/util/ArrayList;

    .line 276
    const/4 v0, 0x0

    .local v0, "pos":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->allTags:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 277
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mTagListAdater:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;

    invoke-virtual {v2, v0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 278
    .local v1, "tag":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mCheckedData:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 279
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mCheckedData:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 276
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 282
    .end local v1    # "tag":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mTagListAdater:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->notifyDataSetChanged()V

    .line 283
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 284
    return-void
.end method

.method public unSelectAll()V
    .locals 4

    .prologue
    .line 287
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getAllUserTag(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->allTags:Ljava/util/ArrayList;

    .line 288
    const/4 v0, 0x0

    .local v0, "pos":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->allTags:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 289
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mTagListAdater:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;

    invoke-virtual {v2, v0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 290
    .local v1, "tag":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mCheckedData:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 291
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mCheckedData:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 288
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 294
    .end local v1    # "tag":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mTagListAdater:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$UserTagListAdapter;->notifyDataSetChanged()V

    .line 295
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 296
    return-void
.end method

.method public updateActionBar()V
    .locals 2

    .prologue
    .line 351
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getAllUserTag(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 353
    :cond_0
    return-void
.end method
