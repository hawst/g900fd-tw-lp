.class Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$2;
.super Ljava/lang/Object;
.source "EditCategoryDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->initDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;

.field final synthetic val$categoryList:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;Landroid/widget/ListView;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$2;->this$0:Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$2;->val$categoryList:Landroid/widget/ListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 69
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$2;->val$categoryList:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, p3, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 70
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$2;->this$0:Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    # setter for: Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mItem:Ljava/lang/String;
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->access$102(Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;Ljava/lang/String;)Ljava/lang/String;

    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$2;->this$0:Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mAdapter:Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->access$300(Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;)Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;->notifyDataSetChanged()V

    .line 72
    return-void
.end method
