.class public Lcom/sec/samsung/gallery/drawer/DrawerBackgroundImageView;
.super Landroid/widget/ImageView;
.source "DrawerBackgroundImageView.java"


# instance fields
.field mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    return-void
.end method


# virtual methods
.method public extractSmartClipData(Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;)I
    .locals 1
    .param p1, "resultElement"    # Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;
    .param p2, "croppedArea"    # Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerBackgroundImageView;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->extractSmartClipData(Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipDataElement;Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipCroppedArea;)I

    move-result v0

    return v0
.end method

.method public setGLRootView(Lcom/sec/android/gallery3d/glcore/GlRootView;)V
    .locals 0
    .param p1, "glRootView"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/samsung/gallery/drawer/DrawerBackgroundImageView;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 34
    return-void
.end method
