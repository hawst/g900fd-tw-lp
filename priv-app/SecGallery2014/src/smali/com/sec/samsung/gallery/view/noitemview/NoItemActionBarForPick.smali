.class public Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarView;
.source "NoItemActionBarForPick.java"


# instance fields
.field private mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

.field private mIsFocusInited:Z

.field private mIsHelpMode:Z


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 31
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mIsFocusInited:Z

    .line 32
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mIsHelpMode:Z

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick$1;-><init>(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mIsHelpMode:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method private isSingleAlbumMode()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 149
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "single-album"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 151
    :cond_0
    :goto_0
    return v1

    .line 150
    :catch_0
    move-exception v0

    .line 151
    .local v0, "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method private setTextOnlyButton(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 143
    const v1, 0x7f0f0288

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 144
    .local v0, "cancel":Landroid/view/MenuItem;
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->setTextOnlyButton(Landroid/view/MenuItem;)V

    .line 145
    return-void
.end method


# virtual methods
.method public hideDrawer()V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick$2;-><init>(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 165
    return-void
.end method

.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x1

    .line 71
    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2, v3}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    .line 72
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mIsHelpMode:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    if-eqz v1, :cond_0

    .line 73
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->show(Z)V

    .line 75
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 76
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f12000c

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 78
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->setTextOnlyButton(Landroid/view/Menu;)V

    .line 79
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 116
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 140
    :goto_0
    return-void

    .line 118
    :sswitch_0
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_HIDDEN_ALBUMS:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 121
    :sswitch_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 122
    .local v0, "activity":Landroid/app/Activity;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setResult(I)V

    .line 123
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 126
    .end local v0    # "activity":Landroid/app/Activity;
    :sswitch_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "START_CAMERA"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 129
    :sswitch_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "SHOW_CONTENT_TO_DISPLAY_DIALOG"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 133
    :sswitch_4
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-class v3, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 134
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 137
    .end local v1    # "intent":Landroid/content/Intent;
    :sswitch_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->goToHelpGallery(Landroid/content/Context;)V

    goto :goto_0

    .line 116
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f0251 -> :sswitch_2
        0x7f0f0257 -> :sswitch_4
        0x7f0f0287 -> :sswitch_0
        0x7f0f0288 -> :sswitch_1
        0x7f0f0289 -> :sswitch_3
        0x7f0f028a -> :sswitch_5
    .end sparse-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mIsHelpMode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->hide()V

    .line 87
    :cond_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->onPause()V

    .line 88
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 6
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 93
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v4, v3}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 94
    const v4, 0x7f0f0286

    invoke-interface {p1, v4, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 96
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isMEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->isSingleAlbumMode()Z

    move-result v4

    if-nez v4, :cond_3

    .line 97
    .local v2, "visibility":Z
    :cond_1
    :goto_0
    const v4, 0x7f0f0251

    invoke-static {p1, v4, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 98
    const v4, 0x7f0f0287

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkShowHiddenMenu(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v5

    invoke-static {p1, v4, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 99
    const v4, 0x7f0f0288

    invoke-static {p1, v4, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 101
    const v4, 0x7f0f0257

    invoke-static {p1, v4, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 102
    const v4, 0x7f0f028a

    invoke-static {p1, v4, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 103
    const v4, 0x7f0f0289

    invoke-static {p1, v4, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 105
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mIsFocusInited:Z

    if-nez v3, :cond_2

    .line 106
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 107
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    .line 108
    .local v1, "actionView":Landroid/view/View;
    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 109
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mIsFocusInited:Z

    .line 112
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    .end local v1    # "actionView":Landroid/view/View;
    :cond_2
    return-void

    .end local v2    # "visibility":Z
    :cond_3
    move v2, v3

    .line 96
    goto :goto_0

    .line 107
    .restart local v0    # "actionBar":Landroid/app/ActionBar;
    .restart local v2    # "visibility":Z
    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method protected setupButtons()V
    .locals 1

    .prologue
    .line 64
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->showHomeButtonOnTopLeft()V

    .line 67
    :cond_0
    return-void
.end method
