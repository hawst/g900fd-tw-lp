.class public Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;
.super Lcom/sec/samsung/gallery/view/help/HelpViewState;
.source "HelpIconLabelState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;


# static fields
.field private static final TAG:Ljava/lang/String; = "HelpIconLabelState"


# instance fields
.field private INVALID_POSITION:F

.field private IsShowCompletePopup:Z

.field private mButtonClickListener:Landroid/view/View$OnClickListener;

.field private mHelpHoverIconListener:Landroid/view/View$OnHoverListener;

.field private mX:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;-><init>()V

    .line 122
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->INVALID_POSITION:F

    .line 124
    iget v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->INVALID_POSITION:F

    iput v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mX:F

    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->IsShowCompletePopup:Z

    .line 128
    new-instance v0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;-><init>(Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mHelpHoverIconListener:Landroid/view/View$OnHoverListener;

    .line 174
    new-instance v0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$3;-><init>(Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mButtonClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->IsShowCompletePopup:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->IsShowCompletePopup:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method private setViewIconLabels()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const v9, 0x7f0f0141

    const/4 v3, 0x2

    const/4 v1, -0x1

    const/4 v5, 0x4

    .line 51
    new-instance v7, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v7, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 53
    .local v7, "mLayoutParamsToolbar":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f030072

    invoke-virtual {v2, v4, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mRelativeLayoutToolBar:Landroid/widget/FrameLayout;

    .line 55
    const/16 v2, 0x30

    iput v2, v7, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 56
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mParentView:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mRelativeLayoutToolBar:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v4, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 58
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 59
    .local v8, "res":Landroid/content/res/Resources;
    const v2, 0x7f0d01bd

    invoke-virtual {v8, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    sput v2, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mPointX:F

    .line 60
    const v2, 0x7f0d01be

    invoke-virtual {v8, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    sput v2, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mPointY:F

    .line 62
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mText:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 63
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 64
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mHead:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 66
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v3, :cond_2

    .line 67
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mBG:Landroid/widget/ImageView;

    const v4, 0x7f0203fc

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 71
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mRelativeLayout:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_0

    .line 72
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mWindowManager:Landroid/view/WindowManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-interface {v2, v4}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 75
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v4, 0x7f040002

    invoke-static {v2, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 76
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f030071

    invoke-virtual {v2, v4, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mRelativeLayout:Landroid/widget/RelativeLayout;

    .line 77
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mRelativeLayout:Landroid/widget/RelativeLayout;

    const v4, 0x7f0f013b

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mTap:Landroid/widget/ImageView;

    .line 78
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mRelativeLayout:Landroid/widget/RelativeLayout;

    const v4, 0x7f0f0128

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mText:Landroid/widget/TextView;

    .line 79
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mRelativeLayout:Landroid/widget/RelativeLayout;

    const v4, 0x7f0f013a

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mHead:Landroid/widget/ImageView;

    .line 80
    iget v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mViewMode:I

    const/16 v4, 0xd

    if-ne v2, v4, :cond_3

    .line 81
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mText:Landroid/widget/TextView;

    const v4, 0x7f0e02cf

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 82
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mTap:Landroid/widget/ImageView;

    sget v4, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mPointX:F

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setX(F)V

    .line 83
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mTap:Landroid/widget/ImageView;

    sget v4, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mPointY:F

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setY(F)V

    .line 84
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mHead:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getX()F

    move-result v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getWidth()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setX(F)V

    .line 98
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->invalidate()V

    .line 99
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v4, 0x418

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    .line 105
    .local v0, "mLayoutParams":Landroid/view/WindowManager$LayoutParams;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mWindowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 106
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mRelativeLayoutToolBar:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v9}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mHelpHoverIconListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 108
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mRelativeLayoutToolBar:Landroid/widget/FrameLayout;

    const v2, 0x7f0f013e

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mRelativeLayoutToolBar:Landroid/widget/FrameLayout;

    const v2, 0x7f0f013f

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mRelativeLayoutToolBar:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v9}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mRelativeLayoutToolBar:Landroid/widget/FrameLayout;

    const v2, 0x7f0f0142

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mRelativeLayoutToolBar:Landroid/widget/FrameLayout;

    const v2, 0x7f0f0143

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mRelativeLayoutToolBar:Landroid/widget/FrameLayout;

    const v2, 0x7f0f0144

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mRelativeLayoutToolBar:Landroid/widget/FrameLayout;

    const v2, 0x7f0f0146

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mRelativeLayoutToolBar:Landroid/widget/FrameLayout;

    const v2, 0x7f0f0148

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    return-void

    .line 69
    .end local v0    # "mLayoutParams":Landroid/view/WindowManager$LayoutParams;
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mBG:Landroid/widget/ImageView;

    const v4, 0x7f02036b

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 85
    :cond_3
    iget v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mViewMode:I

    const/16 v4, 0xa

    if-ne v2, v4, :cond_1

    .line 86
    new-instance v6, Landroid/os/Handler;

    invoke-direct {v6}, Landroid/os/Handler;-><init>()V

    .line 87
    .local v6, "mHandler":Landroid/os/Handler;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mText:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 88
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 89
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mHead:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 90
    new-instance v2, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$1;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$1;-><init>(Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;)V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v6, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    .line 34
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 36
    const-string v0, "Help"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mHelpMode:I

    .line 37
    const/16 v0, 0xd

    iput v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mViewMode:I

    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 40
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->setHelpView()V

    .line 41
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->animationInit(Landroid/content/Context;)V

    .line 42
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->setOnClickListener()V

    .line 43
    return-void
.end method

.method protected setHelpView()V
    .locals 0

    .prologue
    .line 46
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->setHelpView()V

    .line 47
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->setViewIconLabels()V

    .line 48
    return-void
.end method
