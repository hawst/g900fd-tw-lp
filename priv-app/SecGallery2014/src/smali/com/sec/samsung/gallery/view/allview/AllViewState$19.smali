.class Lcom/sec/samsung/gallery/view/allview/AllViewState$19;
.super Ljava/lang/Thread;
.source "AllViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/allview/AllViewState;->handleBackgroundDeletion()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V
    .locals 0

    .prologue
    .line 1216
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$19;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 1219
    const/4 v0, 0x0

    .line 1221
    .local v0, "exitSelection":Z
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$19;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$2000(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v6

    if-nez v6, :cond_1

    .line 1245
    :cond_0
    :goto_0
    return-void

    .line 1223
    :cond_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$19;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$2000(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCloneMediaList()Ljava/util/LinkedList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 1224
    .local v5, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v6, v5, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v6, :cond_2

    move-object v4, v5

    .line 1225
    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1226
    .local v4, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v6, v4, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-nez v6, :cond_3

    instance-of v6, v4, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-eqz v6, :cond_5

    :cond_3
    const/4 v3, 0x1

    .line 1227
    .local v3, "isCloudItem":Z
    :goto_2
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 1228
    new-instance v1, Ljava/io/File;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1229
    .local v1, "file":Ljava/io/File;
    if-eqz v3, :cond_4

    .line 1230
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$19;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # invokes: Lcom/sec/samsung/gallery/view/allview/AllViewState;->isCloudItemDeleted(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    invoke-static {v6, v4}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$4400(Lcom/sec/samsung/gallery/view/allview/AllViewState;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1231
    const/4 v0, 0x1

    .line 1235
    :cond_4
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_2

    .line 1236
    const/4 v0, 0x1

    goto :goto_1

    .line 1226
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "isCloudItem":Z
    :cond_5
    const/4 v3, 0x0

    goto :goto_2

    .line 1241
    .end local v4    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v5    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_6
    if-eqz v0, :cond_0

    .line 1242
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$19;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$3300(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->dismissDialogs()V

    .line 1243
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$19;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->exitSelectionMode()V

    goto :goto_0
.end method
