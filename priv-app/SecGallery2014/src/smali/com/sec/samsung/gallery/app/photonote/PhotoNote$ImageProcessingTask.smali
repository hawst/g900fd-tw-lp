.class Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;
.super Landroid/os/AsyncTask;
.source "PhotoNote.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/app/photonote/PhotoNote;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImageProcessingTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final CloseDelayedTime:I = 0xa


# instance fields
.field private mItemId:I

.field private final mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

.field private mSaveFolder:Ljava/io/File;

.field private mTitle:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;Ljava/io/File;ILjava/lang/String;)V
    .locals 3
    .param p2, "folder"    # Ljava/io/File;
    .param p3, "itemId"    # I
    .param p4, "title"    # Ljava/lang/String;

    .prologue
    .line 669
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 663
    new-instance v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;-><init>(II)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    .line 670
    iput p3, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->mItemId:I

    .line 671
    iput-object p4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->mTitle:Ljava/lang/String;

    .line 672
    iput-object p2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->mSaveFolder:Ljava/io/File;

    .line 673
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 661
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 7
    .param p1, "arg"    # [Ljava/lang/Void;

    .prologue
    const/4 v2, 0x0

    .line 684
    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->mSaveFolder:Ljava/io/File;

    if-nez v3, :cond_0

    move-object v1, v2

    .line 696
    :goto_0
    return-object v1

    .line 688
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->mSaveFolder:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x2f

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->mSaveFolder:Ljava/io/File;

    const-string v5, "image"

    const-string v6, "png"

    invoke-static {v4, v5, v6}, Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;->getUniqueFilename(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 691
    .local v1, "savePath":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    invoke-static {v3}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->access$500(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->getCapturePageBitmp()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 692
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    invoke-virtual {v3, v1, v0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->saveBitmapPNG(Ljava/lang/String;Landroid/graphics/Bitmap;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 693
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    :cond_1
    move-object v1, v2

    .line 696
    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 661
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 11
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v10, 0x0

    .line 701
    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->closeProgressDialog(I)V

    .line 702
    if-eqz p1, :cond_1

    .line 703
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 704
    .local v3, "newFile":Ljava/io/File;
    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 705
    .local v2, "fileUri":Landroid/net/Uri;
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFindoSearch:Z

    if-eqz v4, :cond_0

    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    if-eqz v4, :cond_0

    .line 707
    :try_start_0
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;

    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFilePath:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->access$000(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v4, v5, v6, v2}, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 708
    .local v0, "StrokeMgr":Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;
    const/4 v4, 0x1

    new-array v4, v4, [Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mNoteSurfaceView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    invoke-static {v6}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->access$500(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->getSpenPageDoc()Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 713
    .end local v0    # "StrokeMgr":Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;
    :cond_0
    :goto_0
    iget v4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->mItemId:I

    sparse-switch v4, :sswitch_data_0

    .line 737
    .end local v2    # "fileUri":Landroid/net/Uri;
    .end local v3    # "newFile":Ljava/io/File;
    :cond_1
    :goto_1
    return-void

    .line 709
    .restart local v2    # "fileUri":Landroid/net/Uri;
    .restart local v3    # "newFile":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 710
    .local v1, "e":Ljava/lang/Exception;
    const-string v4, "PhotoNote"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 715
    .end local v1    # "e":Ljava/lang/Exception;
    :sswitch_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    # invokes: Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->actionShare(Landroid/net/Uri;)V
    invoke-static {v4, v2}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->access$600(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;Landroid/net/Uri;)V

    goto :goto_1

    .line 718
    :sswitch_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    # invokes: Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->actionExport()V
    invoke-static {v4}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->access$700(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;)V

    goto :goto_1

    .line 721
    :sswitch_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    # invokes: Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->actionPrint(Landroid/net/Uri;)V
    invoke-static {v4, v2}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->access$800(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;Landroid/net/Uri;)V

    goto :goto_1

    .line 724
    :sswitch_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->access$400(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;)Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    const v7, 0x7f0e020c

    new-array v8, v8, [Ljava/lang/Object;

    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    const v9, 0x7f0e01ff

    invoke-virtual {v4, v9}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_2
    aput-object v4, v8, v10

    invoke-virtual {v6, v7, v8}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 729
    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->access$400(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;)Landroid/content/Context;

    move-result-object v4

    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.MEDIA_SCAN"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "file://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v4, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 731
    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    # invokes: Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->finish_note(Z)V
    invoke-static {v4, v10}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->access$100(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;Z)V

    goto/16 :goto_1

    .line 724
    :cond_2
    const-string v4, "Photo frame"

    goto :goto_2

    .line 713
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f0025 -> :sswitch_3
        0x7f0f0265 -> :sswitch_0
        0x7f0f0266 -> :sswitch_1
        0x7f0f0268 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPreExecute()V
    .locals 6

    .prologue
    .line 677
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 678
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->access$400(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->mTitle:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$ImageProcessingTask;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->access$400(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e0037

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->showProgressDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 680
    return-void
.end method
