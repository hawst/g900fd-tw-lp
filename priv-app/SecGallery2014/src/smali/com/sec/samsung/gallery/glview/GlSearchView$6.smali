.class Lcom/sec/samsung/gallery/glview/GlSearchView$6;
.super Ljava/lang/Object;
.source "GlSearchView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlSearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V
    .locals 0

    .prologue
    .line 456
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Lcom/sec/android/gallery3d/glcore/GlObject;)Z
    .locals 4
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/4 v3, 0x1

    .line 458
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->isScreenLocked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 475
    .end local p1    # "object":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_0
    :goto_0
    return v3

    .line 461
    .restart local p1    # "object":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_1
    check-cast p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .end local p1    # "object":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget v0, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 463
    .local v0, "index":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$800(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 464
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->mMode:I

    sget v2, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_NORMAL:I

    if-ne v1, v2, :cond_3

    .line 465
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 470
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->mOnItemClickListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListenerEx;

    if-eqz v1, :cond_0

    .line 471
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iput v0, v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFocused:I

    .line 472
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mDeletingIndex:I
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$902(Lcom/sec/samsung/gallery/glview/GlSearchView;I)I

    .line 473
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # invokes: Lcom/sec/samsung/gallery/glview/GlSearchView;->startKeywordDeleteAnimation(I)V
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$1000(Lcom/sec/samsung/gallery/glview/GlSearchView;I)V

    goto :goto_0

    .line 467
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$800(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    goto :goto_1
.end method
