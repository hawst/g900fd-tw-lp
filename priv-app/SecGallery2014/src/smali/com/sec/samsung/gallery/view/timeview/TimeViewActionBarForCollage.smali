.class Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForCollage;
.super Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;
.source "TimeViewActionBarForCollage.java"


# instance fields
.field private mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 22
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForCollage;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForCollage;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 23
    return-void
.end method


# virtual methods
.method public checkShowToastOverAvailableSelect(Lcom/sec/android/gallery3d/data/MediaItem;I)Z
    .locals 8
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "numberOfSelect"    # I

    .prologue
    const/4 v7, 0x6

    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 41
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForCollage;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v3, p1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->isAvailableProceedEditAndCollage(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 42
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForCollage;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0e0107

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 52
    :goto_0
    return v1

    .line 45
    :cond_0
    if-le p2, v7, :cond_1

    .line 46
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForCollage;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0115

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForCollage;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2, v0}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .end local v0    # "text":Ljava/lang/String;
    :cond_1
    move v1, v2

    .line 52
    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 8
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const v7, 0x7f0e0115

    const/4 v6, 0x1

    const/4 v5, 0x6

    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 57
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 86
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 59
    :pswitch_1
    iget v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForCollage;->mCount:I

    if-le v1, v5, :cond_1

    .line 60
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForCollage;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v1, v7, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 63
    .local v0, "text":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForCollage;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1, v0}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 67
    .end local v0    # "text":Ljava/lang/String;
    :cond_1
    iget v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForCollage;->mCount:I

    if-ge v1, v3, :cond_2

    .line 68
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForCollage;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v1, v7, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 71
    .restart local v0    # "text":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForCollage;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1, v0}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 75
    .end local v0    # "text":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForCollage;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->startCollageMaker()V

    goto :goto_0

    .line 78
    :pswitch_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0f0022

    if-ne v1, v2, :cond_0

    .line 79
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForCollage;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "EXIT_SELECTION_MODE"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 57
    :pswitch_data_0
    .packed-switch 0x7f0f0022
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 27
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 28
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForCollage;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    .line 31
    .local v1, "count":I
    const/4 v2, 0x2

    if-lt v1, v2, :cond_0

    const/4 v2, 0x6

    if-gt v1, v2, :cond_0

    .line 33
    const/4 v0, 0x1

    .line 36
    .local v0, "bShowDone":Z
    :goto_0
    const v2, 0x7f0f0024

    invoke-static {p1, v2, v0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 37
    return-void

    .line 35
    .end local v0    # "bShowDone":Z
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "bShowDone":Z
    goto :goto_0
.end method
