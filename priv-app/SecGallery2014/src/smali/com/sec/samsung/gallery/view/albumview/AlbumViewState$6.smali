.class Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$6;
.super Ljava/lang/Object;
.source "AlbumViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V
    .locals 0

    .prologue
    .line 500
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$6;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/android/gallery3d/glcore/GlView;II)Z
    .locals 9
    .param p1, "parentView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;
    .param p2, "view"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "albumIndex"    # I
    .param p4, "itemIndex"    # I

    .prologue
    const/16 v8, 0x3e8

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 504
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$6;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$2700(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v5

    sget-object v6, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v5, v6, :cond_3

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$6;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsAlbumPick:Z
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$2800(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 505
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$6;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v5, p3}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 506
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v1, :cond_0

    .line 526
    .end local v1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_0
    return v3

    .line 507
    .restart local v1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    .line 508
    .local v0, "count":I
    const/4 v2, 0x0

    .line 509
    .local v2, "text":Ljava/lang/String;
    if-le v0, v8, :cond_1

    .line 510
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$6;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$2900(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0117

    new-array v7, v4, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 511
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$6;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$3000(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    move v3, v4

    .line 512
    goto :goto_0

    .line 514
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$6;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mAlbumEventHandle:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$3100(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    move-result-object v3

    invoke-virtual {v3, p1, p2, p3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->onItemClick(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .end local v0    # "count":I
    .end local v1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v2    # "text":Ljava/lang/String;
    :cond_2
    :goto_1
    move v3, v4

    .line 526
    goto :goto_0

    .line 516
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$6;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$2700(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v3

    sget-object v5, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v3, v5, :cond_4

    .line 517
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$6;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mAlbumEventHandle:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$3100(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    move-result-object v3

    invoke-virtual {v3, p1, p2, p3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->onItemClick(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlView;I)V

    goto :goto_1

    .line 518
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$6;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$2700(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v3

    sget-object v5, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v3, v5, :cond_5

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$6;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsAlbumPick:Z
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$2800(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 519
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$6;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->startPhotoView(I)V
    invoke-static {v3, p3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$3200(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;I)V

    goto :goto_1

    .line 520
    :cond_5
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$6;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 521
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$6;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-virtual {v3, p3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->selectAlbum(I)V

    goto :goto_1

    .line 522
    :cond_6
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$6;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->getMode()I

    move-result v3

    const/4 v5, 0x3

    if-eq v3, v5, :cond_2

    .line 523
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$6;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->latestSelectedAlbuminfo(I)V
    invoke-static {v3, p3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$3300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;I)V

    .line 524
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$6;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->startPhotoView(I)V
    invoke-static {v3, p3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$3200(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;I)V

    goto :goto_1
.end method
