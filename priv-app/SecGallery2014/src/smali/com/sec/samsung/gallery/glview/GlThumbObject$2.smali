.class Lcom/sec/samsung/gallery/glview/GlThumbObject$2;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlThumbObject.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlThumbObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V
    .locals 0

    .prologue
    .line 251
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 17
    .param p1, "ratio"    # F

    .prologue
    .line 253
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getSourceX()F

    move-result v4

    .line 254
    .local v4, "sx":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getSourceY()F

    move-result v5

    .line 255
    .local v5, "sy":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getSourceZ()F

    move-result v6

    .line 256
    .local v6, "sz":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getTargetX()F

    move-result v10

    .line 257
    .local v10, "tx":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getTargetY()F

    move-result v11

    .line 258
    .local v11, "ty":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getTargetZ()F

    move-result v12

    .line 259
    .local v12, "tz":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    sub-float v14, v10, v4

    mul-float v14, v14, p1

    add-float/2addr v14, v4

    sub-float v15, v11, v5

    mul-float v15, v15, p1

    add-float/2addr v15, v5

    sub-float v16, v12, v6

    mul-float v16, v16, p1

    add-float v16, v16, v6

    invoke-virtual/range {v13 .. v16}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setPos(FFF)V

    .line 261
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget v3, v13, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mSourceWidth:F

    .line 262
    .local v3, "sw":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget v1, v13, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mSourceHeight:F

    .line 263
    .local v1, "sh":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget v9, v13, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mTargetWidth:F

    .line 264
    .local v9, "tw":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget v7, v13, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mTargetHeight:F

    .line 265
    .local v7, "th":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    sub-float v14, v9, v3

    mul-float v14, v14, p1

    add-float/2addr v14, v3

    sub-float v15, v7, v1

    mul-float v15, v15, p1

    add-float/2addr v15, v1

    invoke-virtual {v13, v14, v15}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setSize(FF)V

    .line 267
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget v2, v13, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mSourceRoll:F

    .line 268
    .local v2, "sr":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget v8, v13, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mTargetRoll:F

    .line 269
    .local v8, "tr":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    sub-float v14, v8, v2

    mul-float v14, v14, p1

    add-float/2addr v14, v2

    iput v14, v13, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mRoll:F

    .line 270
    return-void
.end method

.method protected onStop()V
    .locals 4

    .prologue
    .line 273
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getTargetX()F

    move-result v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getTargetY()F

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getTargetZ()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setPos(FFF)V

    .line 274
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mTargetWidth:F

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mTargetHeight:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setSize(FF)V

    .line 275
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mTargetRoll:F

    iput v1, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mRoll:F

    .line 276
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$2;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 277
    return-void
.end method
