.class public Lcom/sec/samsung/gallery/glview/GlTextObject;
.super Lcom/sec/samsung/gallery/glview/GlBaseObject;
.source "GlTextObject.java"


# instance fields
.field public final mGlThumbObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/samsung/gallery/glview/GlBaseObject;II)V
    .locals 2
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "glThumbObject"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 11
    invoke-direct {p0, p1, v0, v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 12
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/GlTextObject;->mGlThumbObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 13
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v1, p1, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v0, v1, p3, p4}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlTextObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 14
    return-void
.end method


# virtual methods
.method public getMainObject()Lcom/sec/android/gallery3d/glcore/GlObject;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlTextObject;->mGlThumbObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    return-object v0
.end method
