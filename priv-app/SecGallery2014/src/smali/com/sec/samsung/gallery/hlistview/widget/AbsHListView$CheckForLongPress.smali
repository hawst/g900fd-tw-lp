.class Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;
.super Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$WindowRunnnable;
.source "AbsHListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckForLongPress"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V
    .locals 1

    .prologue
    .line 2417
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$WindowRunnnable;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$1;

    .prologue
    .line 2417
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2421
    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget v5, v6, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionPosition:I

    .line 2422
    .local v5, "motionPosition":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget v7, v7, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    sub-int v7, v5, v7

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2423
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 2424
    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget v4, v6, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionPosition:I

    .line 2425
    .local v4, "longPressPosition":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget v7, v7, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionPosition:I

    invoke-interface {v6, v7}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    .line 2427
    .local v2, "longPressId":J
    const/4 v1, 0x0

    .line 2428
    .local v1, "handled":Z
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;->sameWindow()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget-boolean v6, v6, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataChanged:Z

    if-nez v6, :cond_0

    .line 2429
    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-virtual {v6, v0, v4, v2, v3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->performLongPress(Landroid/view/View;IJ)Z

    move-result v1

    .line 2431
    :cond_0
    if-eqz v1, :cond_2

    .line 2432
    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    const/4 v7, -0x1

    iput v7, v6, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 2433
    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-virtual {v6, v8}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setPressed(Z)V

    .line 2434
    invoke-virtual {v0, v8}, Landroid/view/View;->setPressed(Z)V

    .line 2439
    .end local v1    # "handled":Z
    .end local v2    # "longPressId":J
    .end local v4    # "longPressPosition":I
    :cond_1
    :goto_0
    return-void

    .line 2436
    .restart local v1    # "handled":Z
    .restart local v2    # "longPressId":J
    .restart local v4    # "longPressPosition":I
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    const/4 v7, 0x2

    iput v7, v6, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    goto :goto_0
.end method
