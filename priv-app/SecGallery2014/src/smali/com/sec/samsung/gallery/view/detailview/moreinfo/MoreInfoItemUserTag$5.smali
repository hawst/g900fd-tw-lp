.class Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$5;
.super Ljava/lang/Object;
.source "MoreInfoItemUserTag.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->getView()Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$5;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 193
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$5;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 194
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$5;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mRunEditButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$600(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 195
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$5;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mRunEditButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$600(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 196
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$5;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mAddTagButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$700(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 204
    :cond_0
    :goto_0
    return-void

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$5;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mAddTagButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$700(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 200
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$5;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mAddTagButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$700(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 201
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$5;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mRunEditButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$600(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 205
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 206
    return-void
.end method
