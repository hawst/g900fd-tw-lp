.class public Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "StrokeDbHelper.java"


# static fields
.field public static final COLUMN_ID:Ljava/lang/String; = "_id"

.field public static final COLUMN_MEDIA_ID:Ljava/lang/String; = "media_id"

.field public static final COLUMN_PHOTO:Ljava/lang/String; = "photo"

.field public static final COLUMN_REGION:Ljava/lang/String; = "region"

.field public static final COLUMN_TYPE:Ljava/lang/String; = "type"

.field public static final COLUMN_WORD:Ljava/lang/String; = "word"

.field public static final DATABASE_NAME:Ljava/lang/String; = "StrokeDb.db"

.field public static final DATABASE_VERSION:I = 0x1

.field public static final TABLE_NAME:Ljava/lang/String; = "stroke"

.field private static sInstance:Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;


# instance fields
.field private TAG:Ljava/lang/String;

.field protected final _INTEGER_TYPE:Ljava/lang/String;

.field protected final _PRIMARY_KEY:Ljava/lang/String;

.field protected final _VARCHAR_TYPE:Ljava/lang/String;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "factory"    # Landroid/database/sqlite/SQLiteDatabase$CursorFactory;
    .param p4, "version"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 18
    const-class v0, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->TAG:Ljava/lang/String;

    .line 20
    const-string v0, "INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->_PRIMARY_KEY:Ljava/lang/String;

    .line 21
    const-string v0, "INTEGER"

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->_INTEGER_TYPE:Ljava/lang/String;

    .line 22
    const-string v0, "VARCHAR(50)"

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->_VARCHAR_TYPE:Ljava/lang/String;

    .line 39
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->mContext:Landroid/content/Context;

    .line 40
    return-void
.end method

.method private createTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 53
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 56
    :try_start_0
    const-string v1, "CREATE TABLE IF NOT EXISTS stroke (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,media_id INTEGER UNIQUE, word VARCHAR(50),region VARCHAR(50),photo VARCHAR(50),type INTEGER);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 67
    :goto_0
    return-void

    .line 61
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->TAG:Ljava/lang/String;

    const-string v2, "Error executing SQL(StrokeDb.db)"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 65
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v1

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    const-class v1, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->sInstance:Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    if-nez v0, :cond_0

    .line 77
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    const-string v2, "StrokeDb.db"

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {v0, p0, v2, v3, v4}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    sput-object v0, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->sInstance:Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    .line 81
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->sInstance:Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getKeyForStroke(Lcom/sec/android/gallery3d/provider/SearchParser;)Ljava/lang/String;
    .locals 7
    .param p1, "sp"    # Lcom/sec/android/gallery3d/provider/SearchParser;

    .prologue
    .line 221
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->getKeyString()Ljava/lang/String;

    move-result-object v0

    .line 222
    .local v0, "full_name":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 223
    const/4 v4, 0x0

    .line 237
    :cond_0
    return-object v4

    .line 226
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 228
    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 229
    .local v3, "names":[Ljava/lang/String;
    const-string v4, ""

    .line 230
    .local v4, "selection_part":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, v3

    if-ge v1, v5, :cond_0

    .line 231
    aget-object v2, v3, v1

    .line 232
    .local v2, "name":Ljava/lang/String;
    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 233
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 234
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "word LIKE \'%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 230
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private upgradeTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 71
    const-string v0, "DROP TABLE IF EXISTS stroke"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 72
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 73
    return-void
.end method


# virtual methods
.method public delete(I)Z
    .locals 10
    .param p1, "mediaId"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 134
    const-wide/16 v2, -0x1

    .line 136
    .local v2, "rowId":J
    monitor-enter p0

    .line 137
    const/4 v0, 0x0

    .line 139
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 140
    const/4 v7, 0x1

    new-array v4, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v7

    .line 141
    .local v4, "whereArgs":[Ljava/lang/String;
    const-string/jumbo v7, "stroke"

    const-string v8, "media_id=?"

    invoke-virtual {v0, v7, v8, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    int-to-long v2, v7

    .line 145
    if-eqz v0, :cond_0

    .line 147
    :try_start_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 152
    .end local v4    # "whereArgs":[Ljava/lang/String;
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 154
    const-wide/16 v8, -0x1

    cmp-long v7, v2, v8

    if-nez v7, :cond_2

    :goto_1
    return v5

    .line 142
    :catch_0
    move-exception v1

    .line 143
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 145
    if-eqz v0, :cond_0

    .line 147
    :try_start_4
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 148
    :catch_1
    move-exception v7

    goto :goto_0

    .line 145
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v5

    if-eqz v0, :cond_1

    .line 147
    :try_start_5
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 149
    :cond_1
    :goto_2
    :try_start_6
    throw v5

    .line 152
    :catchall_1
    move-exception v5

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v5

    :cond_2
    move v5, v6

    .line 154
    goto :goto_1

    .line 148
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    :catch_2
    move-exception v7

    goto :goto_0

    .end local v4    # "whereArgs":[Ljava/lang/String;
    :catch_3
    move-exception v6

    goto :goto_2
.end method

.method public insert(Landroid/content/ContentValues;)J
    .locals 7
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 85
    const-wide/16 v2, -0x1

    .line 86
    .local v2, "rowId":J
    monitor-enter p0

    .line 87
    const/4 v0, 0x0

    .line 90
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 91
    const-string/jumbo v4, "stroke"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 92
    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    if-eqz v0, :cond_0

    .line 98
    :try_start_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 103
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 105
    return-wide v2

    .line 93
    :catch_0
    move-exception v1

    .line 94
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 96
    if-eqz v0, :cond_0

    .line 98
    :try_start_4
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 99
    :catch_1
    move-exception v4

    goto :goto_0

    .line 96
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v4

    if-eqz v0, :cond_1

    .line 98
    :try_start_5
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 100
    :cond_1
    :goto_1
    :try_start_6
    throw v4

    .line 103
    :catchall_1
    move-exception v4

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v4

    .line 99
    :catch_2
    move-exception v4

    goto :goto_0

    :catch_3
    move-exception v5

    goto :goto_1
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 45
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->upgradeTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 50
    return-void
.end method

.method public select(Lcom/sec/android/gallery3d/provider/SearchParser;)Landroid/database/Cursor;
    .locals 10
    .param p1, "sp"    # Lcom/sec/android/gallery3d/provider/SearchParser;

    .prologue
    const/4 v1, 0x0

    .line 190
    const/4 v8, 0x0

    .line 191
    .local v8, "cursor":Landroid/database/Cursor;
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->getKeyForStroke(Lcom/sec/android/gallery3d/provider/SearchParser;)Ljava/lang/String;

    move-result-object v3

    .line 194
    .local v3, "key":Ljava/lang/String;
    monitor-enter p0

    .line 195
    const/4 v0, 0x0

    .line 198
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 200
    const/4 v4, 0x5

    new-array v2, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "media_id"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "word"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string v5, "region"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const-string v5, "photo"

    aput-object v5, v2, v4

    const/4 v4, 0x4

    const-string/jumbo v5, "type"

    aput-object v5, v2, v4

    .line 201
    .local v2, "projection":[Ljava/lang/String;
    if-nez v3, :cond_1

    .line 202
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/provider/SearchParser;->getSearchFilter()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 203
    const-string/jumbo v1, "stroke"

    const-string v3, "media_id LIKE \'%%\'"

    .end local v3    # "key":Ljava/lang/String;
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 215
    .end local v2    # "projection":[Ljava/lang/String;
    :goto_0
    :try_start_1
    monitor-exit p0

    move-object v1, v8

    .line 217
    :goto_1
    return-object v1

    .line 205
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v3    # "key":Ljava/lang/String;
    :cond_0
    monitor-exit p0

    goto :goto_1

    .line 215
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "key":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 207
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v3    # "key":Ljava/lang/String;
    :cond_1
    :try_start_2
    const-string/jumbo v1, "stroke"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v8

    goto :goto_0

    .line 212
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "key":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 213
    .local v9, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_3
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public selectMediaId(I)I
    .locals 11
    .param p1, "mediaId"    # I

    .prologue
    .line 158
    const/4 v8, 0x0

    .line 159
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v10, -0x1

    .line 161
    .local v10, "mId":I
    monitor-enter p0

    .line 162
    const/4 v0, 0x0

    .line 165
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 167
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "media_id"

    aput-object v3, v2, v1

    .line 168
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v1, "stroke"

    const-string v3, "media_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    int-to-long v6, p1

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 170
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 171
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    goto :goto_0

    .line 176
    :cond_0
    :try_start_1
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 177
    if-eqz v0, :cond_1

    .line 179
    :try_start_2
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 184
    .end local v2    # "projection":[Ljava/lang/String;
    :cond_1
    :goto_1
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 186
    return v10

    .line 173
    :catch_0
    move-exception v9

    .line 174
    .local v9, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_4
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 176
    :try_start_5
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 177
    if-eqz v0, :cond_1

    .line 179
    :try_start_6
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    .line 180
    :catch_1
    move-exception v1

    goto :goto_1

    .line 176
    .end local v9    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v1

    :try_start_7
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 177
    if-eqz v0, :cond_2

    .line 179
    :try_start_8
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 181
    :cond_2
    :goto_2
    :try_start_9
    throw v1

    .line 184
    :catchall_1
    move-exception v1

    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    throw v1

    .line 180
    .restart local v2    # "projection":[Ljava/lang/String;
    :catch_2
    move-exception v1

    goto :goto_1

    .end local v2    # "projection":[Ljava/lang/String;
    :catch_3
    move-exception v3

    goto :goto_2
.end method

.method public update(Landroid/content/ContentValues;I)Z
    .locals 10
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "mediaId"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 109
    const-wide/16 v2, -0x1

    .line 111
    .local v2, "rowId":J
    monitor-enter p0

    .line 112
    const/4 v0, 0x0

    .line 114
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->getInstance(Landroid/content/Context;)Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 115
    const/4 v7, 0x1

    new-array v4, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v7

    .line 116
    .local v4, "whereArgs":[Ljava/lang/String;
    const-string/jumbo v7, "stroke"

    const-string v8, "media_id=?"

    invoke-virtual {v0, v7, p1, v8, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    int-to-long v2, v7

    .line 117
    iget-object v7, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeDbHelper;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    if-eqz v0, :cond_0

    .line 123
    :try_start_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 128
    .end local v4    # "whereArgs":[Ljava/lang/String;
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 130
    const-wide/16 v8, -0x1

    cmp-long v7, v2, v8

    if-nez v7, :cond_2

    :goto_1
    return v5

    .line 118
    :catch_0
    move-exception v1

    .line 119
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 121
    if-eqz v0, :cond_0

    .line 123
    :try_start_4
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 124
    :catch_1
    move-exception v7

    goto :goto_0

    .line 121
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v5

    if-eqz v0, :cond_1

    .line 123
    :try_start_5
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 125
    :cond_1
    :goto_2
    :try_start_6
    throw v5

    .line 128
    :catchall_1
    move-exception v5

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v5

    :cond_2
    move v5, v6

    .line 130
    goto :goto_1

    .line 124
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    :catch_2
    move-exception v7

    goto :goto_0

    .end local v4    # "whereArgs":[Ljava/lang/String;
    :catch_3
    move-exception v6

    goto :goto_2
.end method
