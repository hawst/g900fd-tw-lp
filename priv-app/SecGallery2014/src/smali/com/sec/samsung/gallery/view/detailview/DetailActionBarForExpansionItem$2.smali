.class Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$2;
.super Ljava/lang/Object;
.source "DetailActionBarForExpansionItem.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->setupButtons()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)V
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 226
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 227
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e004f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->setupPopupMenu(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$3300(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;Ljava/lang/String;)V

    .line 229
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$3400(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSourceMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 230
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->updateSelectionButton(Lcom/sec/android/gallery3d/data/MediaSet;Z)V
    invoke-static {v1, v0, v6}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;Lcom/sec/android/gallery3d/data/MediaSet;Z)V

    .line 231
    return-void
.end method
