.class public Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;
.super Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;
.source "GlSlideshowZoomOnFace.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$2;,
        Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;,
        Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$animState;
    }
.end annotation


# static fields
.field private static final FACE_TRANSITION_DURATION:J = 0x7d0L

.field private static final STATE_SHOWING_IMAGE:I = 0x0

.field private static final STATE_TRANSITIONING_IMAGE:I = 0x1

.field private static final TRANSITION_DURATION:J = 0x3e8L

.field private static final ZOOMING_DURATION:J = 0x3e8L

.field private static mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;


# instance fields
.field private mAnimDuration:J

.field private mContext:Landroid/content/Context;

.field private mCurrentPositionX:F

.field private mCurrentPositionY:F

.field private mCurrentScaleValue:F

.field private mCurrentState:I

.field private mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

.field private mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

.field private misFromVideoPlay:Z


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/GlSlideShowView;Landroid/content/Context;)V
    .locals 4
    .param p1, "view"    # Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 68
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;-><init>(Lcom/sec/samsung/gallery/glview/GlSlideShowView;Landroid/content/Context;)V

    .line 49
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAnimDuration:J

    .line 51
    iput v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentPositionX:F

    .line 53
    iput v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentPositionY:F

    .line 55
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentScaleValue:F

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->misFromVideoPlay:Z

    .line 61
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    .line 63
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    .line 69
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mContext:Landroid/content/Context;

    .line 70
    return-void
.end method

.method private calculateFaceCoordinate(FF)V
    .locals 8
    .param p1, "objW"    # F
    .param p2, "objH"    # F

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v6, 0x3fa00000    # 1.25f

    .line 74
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    array-length v3, v3

    new-array v3, v3, [Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    .line 75
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 76
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    aget-object v3, v3, v2

    iget v3, v3, Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;->ratioX:F

    mul-float v0, p1, v3

    .line 77
    .local v0, "faceX":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    aget-object v3, v3, v2

    iget v3, v3, Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;->ratioY:F

    mul-float v1, p2, v3

    .line 78
    .local v1, "faceY":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    new-instance v4, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    invoke-direct {v4, p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;-><init>(Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;)V

    aput-object v4, v3, v2

    .line 79
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v3, v3, v2

    div-float v4, p1, v7

    sub-float v4, v0, v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    aget-object v5, v5, v2

    iget v5, v5, Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;->scaleValue:F

    div-float/2addr v5, v6

    mul-float/2addr v4, v5

    iput v4, v3, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->faceX:F

    .line 80
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v3, v3, v2

    div-float v4, p2, v7

    sub-float/2addr v4, v1

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    aget-object v5, v5, v2

    iget v5, v5, Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;->scaleValue:F

    div-float/2addr v5, v6

    mul-float/2addr v4, v5

    iput v4, v3, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->faceY:F

    .line 81
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v3, v3, v2

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    aget-object v4, v4, v2

    iget v4, v4, Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;->scaleValue:F

    iput v4, v3, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->scaleValue:F

    .line 82
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v3, v3, v2

    sget-object v4, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$animState;->MOVING_TO_FACE:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$animState;

    iput-object v4, v3, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->state:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$animState;

    .line 75
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 84
    .end local v0    # "faceX":F
    .end local v1    # "faceY":F
    :cond_0
    return-void
.end method


# virtual methods
.method public checkForVideoItem(I)V
    .locals 3
    .param p1, "itemIndex"    # I

    .prologue
    .line 296
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 297
    .local v0, "mediaitem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v1

    if-nez v1, :cond_0

    .line 298
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->isPlayingVideoItem:Z

    .line 299
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$1;

    invoke-direct {v2, p0, v0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$1;-><init>(Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;Lcom/sec/android/gallery3d/data/MediaItem;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 308
    :cond_0
    return-void
.end method

.method public getMaxDisplayObjCount()I
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x2

    return v0
.end method

.method public isBitmapdecoded()Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x7d0

    const/4 v2, 0x0

    .line 113
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    if-eqz v3, :cond_1

    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    .line 115
    .local v0, "adapter":Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;
    iget v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageIndex:I

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getCount()I

    move-result v4

    if-lt v3, v4, :cond_0

    .line 116
    iput v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageIndex:I

    .line 118
    :cond_0
    iget v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageIndex:I

    invoke-virtual {v0, v3, v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getItemImage(II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 119
    .local v1, "bmp":Landroid/graphics/Bitmap;
    if-nez v1, :cond_2

    iget v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageIndex:I

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->isRequestInProgress(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 143
    .end local v0    # "adapter":Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;
    .end local v1    # "bmp":Landroid/graphics/Bitmap;
    :cond_1
    :goto_0
    return v2

    .line 122
    .restart local v0    # "adapter":Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;
    .restart local v1    # "bmp":Landroid/graphics/Bitmap;
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageCache:Ljava/util/LinkedList;

    invoke-virtual {v3, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 123
    iget v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageIndex:I

    .line 124
    iget v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageIndex:I

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getCount()I

    move-result v4

    rem-int/2addr v3, v4

    iput v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageIndex:I

    .line 126
    iget v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mSetImageCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mSetImageCount:I

    .line 127
    iget v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mSetImageCount:I

    const/4 v4, 0x2

    if-lt v3, v4, :cond_3

    .line 128
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getFacesInfo()[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    .line 129
    const/4 v2, 0x1

    goto :goto_0

    .line 131
    :cond_3
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getFacesInfo()[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    .line 132
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    if-eqz v3, :cond_4

    .line 133
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v3

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mWidth:F

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v4

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mHeight:F

    invoke-direct {p0, v3, v4}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->calculateFaceCoordinate(FF)V

    .line 134
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    array-length v3, v3

    int-to-long v4, v3

    mul-long/2addr v4, v6

    const-wide/16 v6, 0x3e8

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAnimDuration:J

    .line 136
    iget-wide v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAnimDuration:J

    invoke-virtual {p0, v4, v5}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->setDuration(J)V

    goto :goto_0

    .line 138
    :cond_4
    iput-wide v6, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAnimDuration:J

    .line 139
    iget-wide v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAnimDuration:J

    invoke-virtual {p0, v4, v5}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->setDuration(J)V

    goto :goto_0
.end method

.method public isInitialLoaded()Z
    .locals 11

    .prologue
    const-wide/16 v8, 0x7d0

    const/4 v6, 0x2

    const/4 v10, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 147
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    if-eqz v2, :cond_6

    .line 148
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->isPlayingVideoItem:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mSetImageCount:I

    if-ge v2, v6, :cond_8

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    .line 151
    .local v0, "adapter":Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;
    iget v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageIndex:I

    invoke-virtual {v0, v2, v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getItemImage(II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 152
    .local v1, "bmp":Landroid/graphics/Bitmap;
    if-nez v1, :cond_1

    iget v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageIndex:I

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->isRequestInProgress(I)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v4

    .line 209
    .end local v0    # "adapter":Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;
    .end local v1    # "bmp":Landroid/graphics/Bitmap;
    :goto_0
    return v2

    .line 155
    .restart local v0    # "adapter":Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;
    .restart local v1    # "bmp":Landroid/graphics/Bitmap;
    :cond_1
    if-nez v1, :cond_2

    move v2, v4

    .line 156
    goto :goto_0

    .line 157
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageCache:Ljava/util/LinkedList;

    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 158
    iget v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageIndex:I

    .line 159
    iget v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageIndex:I

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getCount()I

    move-result v5

    if-lt v2, v5, :cond_3

    .line 160
    iput v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageIndex:I

    .line 163
    :cond_3
    iget v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mSetImageCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mSetImageCount:I

    .line 165
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->isPlayingVideoItem:Z

    if-eqz v2, :cond_4

    .line 166
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageCache:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getMaxDisplayObjCount()I

    move-result v5

    if-lt v2, v5, :cond_4

    .line 167
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v5

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageCache:Ljava/util/LinkedList;

    invoke-virtual {v2, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, v5, v2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->updateObject(Lcom/sec/samsung/gallery/glview/GlBaseObject;Landroid/graphics/Bitmap;)V

    .line 168
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v5

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageCache:Ljava/util/LinkedList;

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, v5, v2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->updateObject(Lcom/sec/samsung/gallery/glview/GlBaseObject;Landroid/graphics/Bitmap;)V

    .line 169
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    .line 170
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->misFromVideoPlay:Z

    .line 171
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->isPlayingVideoItem:Z

    move v2, v3

    .line 172
    goto :goto_0

    .line 176
    :cond_4
    iget v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mSetImageCount:I

    if-lt v2, v6, :cond_5

    .line 177
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getFacesInfo()[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    .line 178
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v4

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageCache:Ljava/util/LinkedList;

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, v4, v2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->updateObject(Lcom/sec/samsung/gallery/glview/GlBaseObject;Landroid/graphics/Bitmap;)V

    move v2, v3

    .line 179
    goto :goto_0

    .line 181
    :cond_5
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getFacesInfo()[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    .line 182
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v5

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageCache:Ljava/util/LinkedList;

    invoke-virtual {v2, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, v5, v2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->updateObject(Lcom/sec/samsung/gallery/glview/GlBaseObject;Landroid/graphics/Bitmap;)V

    .line 183
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    if-eqz v2, :cond_7

    .line 184
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v2

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mWidth:F

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v5

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mHeight:F

    invoke-direct {p0, v2, v5}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->calculateFaceCoordinate(FF)V

    .line 186
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    array-length v2, v2

    int-to-long v6, v2

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x3e8

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAnimDuration:J

    .line 189
    iget-wide v6, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAnimDuration:J

    invoke-virtual {p0, v6, v7}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->setDuration(J)V

    .line 194
    :goto_1
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v2

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v2, v5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    .line 195
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    .end local v0    # "adapter":Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;
    .end local v1    # "bmp":Landroid/graphics/Bitmap;
    :cond_6
    move v2, v4

    .line 209
    goto/16 :goto_0

    .line 191
    .restart local v0    # "adapter":Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;
    .restart local v1    # "bmp":Landroid/graphics/Bitmap;
    :cond_7
    iput-wide v8, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAnimDuration:J

    .line 192
    iget-wide v6, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAnimDuration:J

    invoke-virtual {p0, v6, v7}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->setDuration(J)V

    goto :goto_1

    .line 198
    .end local v0    # "adapter":Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;
    .end local v1    # "bmp":Landroid/graphics/Bitmap;
    :cond_8
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageCache:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getMaxDisplayObjCount()I

    move-result v5

    if-lt v2, v5, :cond_6

    .line 199
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v5

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageCache:Ljava/util/LinkedList;

    invoke-virtual {v2, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, v5, v2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->updateObject(Lcom/sec/samsung/gallery/glview/GlBaseObject;Landroid/graphics/Bitmap;)V

    .line 200
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v5

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageCache:Ljava/util/LinkedList;

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, v5, v2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->updateObject(Lcom/sec/samsung/gallery/glview/GlBaseObject;Landroid/graphics/Bitmap;)V

    .line 201
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    .line 202
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->misFromVideoPlay:Z

    .line 203
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->isPlayingVideoItem:Z

    move v2, v3

    .line 204
    goto/16 :goto_0
.end method

.method public isNextLoaded()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 214
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    if-eqz v5, :cond_0

    .line 215
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    iget v6, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentIndex:I

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .line 216
    .local v2, "mediaitem":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->load(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v5

    sput-object v5, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    .line 217
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideshowVideoPlay:Z

    if-eqz v5, :cond_1

    sget-object v5, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getVideoPlayEnable()Z

    move-result v5

    if-eqz v5, :cond_1

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v5

    const/4 v6, 0x4

    if-ne v5, v6, :cond_1

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v5

    if-nez v5, :cond_1

    .line 219
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->isPlayingVideoItem:Z

    if-eqz v5, :cond_1

    .line 238
    .end local v2    # "mediaitem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    :goto_0
    return v3

    .line 222
    .restart local v2    # "mediaitem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getSlideshowVideoView()Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    move-result-object v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getSlideshowVideoView()Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    move-result-object v5

    iget-boolean v5, v5, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->misVideoPlaying:Z

    if-nez v5, :cond_0

    .line 224
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    .line 225
    .local v0, "adapter":Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;
    iget v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageIndex:I

    invoke-virtual {v0, v5, v3}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getItemImage(II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 226
    .local v1, "bmp":Landroid/graphics/Bitmap;
    if-nez v1, :cond_3

    iget v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageIndex:I

    invoke-virtual {v0, v5}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->isRequestInProgress(I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 231
    :cond_3
    iget v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageIndex:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageIndex:I

    .line 232
    iget v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageIndex:I

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getCount()I

    move-result v6

    if-lt v5, v6, :cond_4

    .line 233
    iput v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageIndex:I

    .line 235
    :cond_4
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v3

    invoke-virtual {p0, v3, v1}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->updateObject(Lcom/sec/samsung/gallery/glview/GlBaseObject;Landroid/graphics/Bitmap;)V

    move v3, v4

    .line 236
    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 100
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->load(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v1

    sput-object v1, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    .line 101
    sget-object v1, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getImageIndex()I

    move-result v0

    .line 103
    .local v0, "lastIndex":I
    sget-boolean v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isSlideShowPaused:Z

    if-eqz v1, :cond_0

    .line 104
    iput v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentIndex:I

    .line 105
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isSlideShowPaused:Z

    .line 107
    :cond_0
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->isPlayingVideoItem:Z

    if-nez v1, :cond_1

    .line 108
    iget v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentIndex:I

    iput v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mImageIndex:I

    .line 109
    :cond_1
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->load(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    .line 94
    sget-object v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentIndex:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->setImageIndex(I)V

    .line 95
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->save(Landroid/content/Context;Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;)V

    .line 96
    return-void
.end method

.method public onLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 243
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 244
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->stop()V

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 246
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    if-eqz v0, :cond_0

    .line 247
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    .line 248
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->misFromVideoPlay:Z

    if-nez v0, :cond_0

    .line 249
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mWidth:F

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v1

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mHeight:F

    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->calculateFaceCoordinate(FF)V

    .line 250
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->misFromVideoPlay:Z

    goto :goto_0
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->load(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    .line 259
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideshowVideoPlay:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getVideoPlayEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    iget v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentIndex:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->checkForVideoItem(I)V

    .line 261
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 264
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->onStop()V

    .line 265
    iget v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentIndex:I

    .line 266
    iget v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentIndex:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getCount()I

    move-result v3

    rem-int/2addr v2, v3

    iput v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentIndex:I

    .line 268
    iput-object v8, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    .line 269
    iput-object v8, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    .line 270
    iput v7, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentScaleValue:F

    .line 271
    iput v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentPositionX:F

    .line 272
    iput v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentPositionY:F

    .line 274
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v1, v2, v4

    .line 275
    .local v1, "tempObject":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v3, v3, v6

    aput-object v3, v2, v4

    .line 276
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aput-object v1, v2, v6

    .line 278
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    .line 279
    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    .line 281
    iput v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentState:I

    .line 282
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    .line 283
    .local v0, "a":Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getFacesInfo()[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    .line 284
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    if-eqz v2, :cond_1

    .line 285
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->isPlayingVideoItem:Z

    if-nez v2, :cond_0

    .line 286
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v2

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mWidth:F

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v3

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mHeight:F

    invoke-direct {p0, v2, v3}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->calculateFaceCoordinate(FF)V

    .line 287
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    array-length v2, v2

    int-to-long v2, v2

    const-wide/16 v4, 0x7d0

    mul-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAnimDuration:J

    .line 288
    iget-wide v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAnimDuration:J

    invoke-virtual {p0, v2, v3}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->setDuration(J)V

    .line 293
    :goto_0
    return-void

    .line 290
    :cond_1
    const-wide/16 v2, 0x7d0

    iput-wide v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAnimDuration:J

    .line 291
    iget-wide v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAnimDuration:J

    invoke-virtual {p0, v2, v3}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->setDuration(J)V

    goto :goto_0
.end method

.method protected onUpdate(F)V
    .locals 14
    .param p1, "ratio"    # F

    .prologue
    .line 312
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    if-nez v10, :cond_1

    .line 421
    :cond_0
    :goto_0
    return-void

    .line 315
    :cond_1
    iget-boolean v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->isPlayingVideoItem:Z

    if-nez v10, :cond_0

    .line 317
    iget v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentState:I

    if-nez v10, :cond_8

    .line 318
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    if-eqz v10, :cond_6

    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    if-eqz v10, :cond_6

    .line 319
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v9

    .line 320
    .local v9, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-eqz v9, :cond_0

    .line 323
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_1
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    array-length v10, v10

    if-ge v8, v10, :cond_0

    .line 324
    iget-wide v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAnimDuration:J

    const-wide/16 v12, 0x3e8

    sub-long/2addr v10, v12

    long-to-float v10, v10

    iget-wide v12, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAnimDuration:J

    long-to-float v11, v12

    div-float/2addr v10, v11

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    array-length v11, v11

    int-to-float v11, v11

    div-float v0, v10, v11

    .line 326
    .local v0, "aniTime":F
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v10, v10, v8

    iget-boolean v10, v10, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->anim:Z

    if-eqz v10, :cond_2

    .line 323
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 329
    :cond_2
    sget-object v10, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$2;->$SwitchMap$com$sec$samsung$gallery$glview$slideshoweffect$GlSlideshowZoomOnFace$animState:[I

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v11, v11, v8

    iget-object v11, v11, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->state:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$animState;

    invoke-virtual {v11}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$animState;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    goto :goto_0

    .line 334
    :pswitch_0
    const/high16 v10, 0x40400000    # 3.0f

    div-float v10, v0, v10

    const/high16 v11, 0x40000000    # 2.0f

    mul-float/2addr v10, v11

    int-to-float v11, v8

    mul-float/2addr v11, v0

    add-float/2addr v10, v11

    cmpl-float v10, p1, v10

    if-lez v10, :cond_3

    .line 335
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v10, v10, v8

    iget v10, v10, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->scaleValue:F

    const v11, 0x3f733333    # 0.95f

    mul-float/2addr v10, v11

    iput v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentScaleValue:F

    .line 336
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v10, v10, v8

    iget v10, v10, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->faceX:F

    const v11, 0x3f733333    # 0.95f

    mul-float/2addr v10, v11

    iput v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentPositionX:F

    .line 337
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v10, v10, v8

    iget v10, v10, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->faceY:F

    const v11, 0x3f733333    # 0.95f

    mul-float/2addr v10, v11

    iput v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentPositionY:F

    .line 338
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v10, v10, v8

    sget-object v11, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$animState;->ZOOMING_ON_FACE:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$animState;

    iput-object v11, v10, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->state:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$animState;

    goto/16 :goto_0

    .line 341
    :cond_3
    int-to-float v10, v8

    mul-float/2addr v10, v0

    sub-float v10, p1, v10

    const/high16 v11, 0x40400000    # 3.0f

    div-float v11, v0, v11

    const/high16 v12, 0x40000000    # 2.0f

    mul-float/2addr v11, v12

    int-to-float v12, v8

    mul-float/2addr v12, v0

    add-float/2addr v11, v12

    int-to-float v12, v8

    mul-float/2addr v12, v0

    sub-float/2addr v11, v12

    div-float p1, v10, v11

    .line 344
    if-nez v8, :cond_4

    .line 345
    iget v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentPositionX:F

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v11, v11, v8

    iget v11, v11, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->faceX:F

    const v12, 0x3f733333    # 0.95f

    mul-float/2addr v11, v12

    invoke-virtual {p0, p1, v10, v11}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getDelta_sinusoidalOut(FFF)F

    move-result v3

    .line 347
    .local v3, "deltaX":F
    iget v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentPositionY:F

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v11, v11, v8

    iget v11, v11, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->faceY:F

    const v12, 0x3f733333    # 0.95f

    mul-float/2addr v11, v12

    invoke-virtual {p0, p1, v10, v11}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getDelta_sinusoidalIN(FFF)F

    move-result v5

    .line 349
    .local v5, "deltaY":F
    iget v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentScaleValue:F

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v11, v11, v8

    iget v11, v11, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->scaleValue:F

    const v12, 0x3f733333    # 0.95f

    mul-float/2addr v11, v12

    invoke-virtual {p0, p1, v10, v11}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getDelta_sinusoidalOut(FFF)F

    move-result v1

    .line 359
    .local v1, "deltaS":F
    :goto_2
    invoke-virtual {v9, v1, v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setScale(FF)V

    .line 360
    neg-float v10, v3

    neg-float v11, v5

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getDistance()F

    move-result v12

    neg-float v12, v12

    invoke-virtual {v9, v10, v11, v12}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    goto/16 :goto_0

    .line 352
    .end local v1    # "deltaS":F
    .end local v3    # "deltaX":F
    .end local v5    # "deltaY":F
    :cond_4
    iget v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentPositionX:F

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v11, v11, v8

    iget v11, v11, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->faceX:F

    const v12, 0x3f733333    # 0.95f

    mul-float/2addr v11, v12

    invoke-virtual {p0, p1, v10, v11}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getDelta_linear(FFF)F

    move-result v3

    .line 354
    .restart local v3    # "deltaX":F
    iget v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentPositionY:F

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v11, v11, v8

    iget v11, v11, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->faceY:F

    const v12, 0x3f733333    # 0.95f

    mul-float/2addr v11, v12

    invoke-virtual {p0, p1, v10, v11}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getDelta_linear(FFF)F

    move-result v5

    .line 356
    .restart local v5    # "deltaY":F
    iget v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentScaleValue:F

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v11, v11, v8

    iget v11, v11, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->scaleValue:F

    const v12, 0x3f733333    # 0.95f

    mul-float/2addr v11, v12

    invoke-virtual {p0, p1, v10, v11}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getDelta_linear(FFF)F

    move-result v1

    .restart local v1    # "deltaS":F
    goto :goto_2

    .line 364
    .end local v1    # "deltaS":F
    .end local v3    # "deltaX":F
    .end local v5    # "deltaY":F
    :pswitch_1
    int-to-float v10, v8

    mul-float/2addr v10, v0

    add-float/2addr v10, v0

    cmpl-float v10, p1, v10

    if-lez v10, :cond_5

    .line 365
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v10, v10, v8

    const/4 v11, 0x1

    iput-boolean v11, v10, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->anim:Z

    .line 366
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v10, v10, v8

    iget v10, v10, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->scaleValue:F

    iput v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentScaleValue:F

    .line 367
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v10, v10, v8

    iget v10, v10, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->faceX:F

    iput v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentPositionX:F

    .line 368
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v10, v10, v8

    iget v10, v10, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->faceY:F

    iput v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentPositionY:F

    .line 369
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    array-length v10, v10

    add-int/lit8 v10, v10, -0x1

    if-ne v8, v10, :cond_0

    .line 370
    const/4 v10, 0x1

    iput v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentState:I

    goto/16 :goto_0

    .line 375
    :cond_5
    const/high16 v10, 0x40400000    # 3.0f

    div-float v10, v0, v10

    const/high16 v11, 0x40000000    # 2.0f

    mul-float/2addr v10, v11

    int-to-float v11, v8

    mul-float/2addr v11, v0

    add-float/2addr v10, v11

    sub-float v10, p1, v10

    int-to-float v11, v8

    mul-float/2addr v11, v0

    add-float/2addr v11, v0

    const/high16 v12, 0x40400000    # 3.0f

    div-float v12, v0, v12

    const/high16 v13, 0x40000000    # 2.0f

    mul-float/2addr v12, v13

    int-to-float v13, v8

    mul-float/2addr v13, v0

    add-float/2addr v12, v13

    sub-float/2addr v11, v12

    div-float p1, v10, v11

    .line 378
    iget v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentScaleValue:F

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v11, v11, v8

    iget v11, v11, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->scaleValue:F

    invoke-virtual {p0, p1, v10, v11}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getDelta_linear(FFF)F

    move-result v7

    .line 380
    .local v7, "deltaZ":F
    iget v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentPositionX:F

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v11, v11, v8

    iget v11, v11, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->faceX:F

    invoke-virtual {p0, p1, v10, v11}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getDelta_linear(FFF)F

    move-result v4

    .line 382
    .local v4, "deltaX1":F
    iget v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mCurrentPositionY:F

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mLocalFaceInfo:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;

    aget-object v11, v11, v8

    iget v11, v11, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace$LocalFaceInfo;->faceY:F

    invoke-virtual {p0, p1, v10, v11}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getDelta_linear(FFF)F

    move-result v6

    .line 385
    .local v6, "deltaY1":F
    invoke-virtual {v9, v7, v7}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setScale(FF)V

    .line 386
    neg-float v10, v4

    neg-float v11, v6

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getDistance()F

    move-result v12

    neg-float v12, v12

    invoke-virtual {v9, v10, v11, v12}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    goto/16 :goto_0

    .line 395
    .end local v0    # "aniTime":F
    .end local v4    # "deltaX1":F
    .end local v6    # "deltaY1":F
    .end local v7    # "deltaZ":F
    .end local v8    # "j":I
    .end local v9    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_6
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v9

    .line 396
    .restart local v9    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-eqz v9, :cond_0

    .line 399
    const/high16 v10, 0x3f400000    # 0.75f

    cmpg-float v10, p1, v10

    if-gez v10, :cond_7

    .line 401
    const/high16 v10, 0x3f800000    # 1.0f

    const v11, 0x3ff33333    # 1.9f

    invoke-virtual {p0, p1, v10, v11}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getDelta_linear(FFF)F

    move-result v2

    .line 402
    .local v2, "deltaScale":F
    invoke-virtual {v9, v2, v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setScale(FF)V

    goto/16 :goto_0

    .line 405
    .end local v2    # "deltaScale":F
    :cond_7
    const/high16 v10, 0x3f400000    # 0.75f

    sub-float v10, p1, v10

    const/high16 v11, 0x3e800000    # 0.25f

    div-float p1, v10, v11

    .line 406
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v10

    const/high16 v11, 0x3f000000    # 0.5f

    const/high16 v12, 0x40000000    # 2.0f

    div-float v12, p1, v12

    sub-float/2addr v11, v12

    invoke-virtual {v10, v11}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    .line 407
    const/4 v10, 0x1

    invoke-virtual {p0, v10}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v10

    const/high16 v11, 0x3f000000    # 0.5f

    const/high16 v12, 0x40000000    # 2.0f

    div-float v12, p1, v12

    add-float/2addr v11, v12

    invoke-virtual {v10, v11}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    goto/16 :goto_0

    .line 411
    .end local v9    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_8
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    if-eqz v10, :cond_9

    .line 412
    iget-wide v10, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAnimDuration:J

    const-wide/16 v12, 0x3e8

    sub-long/2addr v10, v12

    long-to-float v10, v10

    iget-wide v12, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAnimDuration:J

    long-to-float v11, v12

    div-float/2addr v10, v11

    sub-float v10, p1, v10

    const/high16 v11, 0x447a0000    # 1000.0f

    iget-wide v12, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mAnimDuration:J

    long-to-float v12, v12

    div-float/2addr v11, v12

    div-float p1, v10, v11

    .line 417
    :goto_3
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v10

    const/high16 v11, 0x3f000000    # 0.5f

    const/high16 v12, 0x40000000    # 2.0f

    div-float v12, p1, v12

    sub-float/2addr v11, v12

    invoke-virtual {v10, v11}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    .line 418
    const/4 v10, 0x1

    invoke-virtual {p0, v10}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v10

    const/high16 v11, 0x3f000000    # 0.5f

    const/high16 v12, 0x40000000    # 2.0f

    div-float v12, p1, v12

    add-float/2addr v11, v12

    invoke-virtual {v10, v11}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    goto/16 :goto_0

    .line 415
    :cond_9
    const/high16 v10, 0x3f000000    # 0.5f

    sub-float v10, p1, v10

    const/high16 v11, 0x3f000000    # 0.5f

    div-float p1, v10, v11

    goto :goto_3

    .line 329
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected setObjectAttrib(Lcom/sec/samsung/gallery/glview/GlBaseObject;Landroid/graphics/Bitmap;)V
    .locals 22
    .param p1, "objs"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 426
    if-nez p2, :cond_0

    .line 462
    :goto_0
    return-void

    .line 429
    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v0, v3

    move/from16 v18, v0

    .line 430
    .local v18, "w":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v9, v3

    .line 431
    .local v9, "h":F
    div-float v14, v18, v9

    .line 432
    .local v14, "srcRatio":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mWidth:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mHeight:I

    int-to-float v4, v4

    div-float v13, v3, v4

    .line 433
    .local v13, "screenRatio":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v12, v3, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mWidthSpace:F

    .line 434
    .local v12, "objW":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v11, v3, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mHeightSpace:F

    .line 436
    .local v11, "objH":F
    cmpl-float v3, v14, v13

    if-lez v3, :cond_2

    .line 437
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    iget v3, v3, Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;->scaleValue:F

    const v4, 0x3f99999a    # 1.2f

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_1

    float-to-double v4, v14

    const-wide/high16 v20, 0x3fe0000000000000L    # 0.5

    cmpl-double v3, v4, v20

    if-lez v3, :cond_1

    const/high16 v3, 0x40000000    # 2.0f

    cmpg-float v3, v14, v3

    if-gez v3, :cond_1

    .line 438
    const/high16 v3, 0x3fc00000    # 1.5f

    div-float v3, v12, v3

    const/high16 v4, 0x40400000    # 3.0f

    mul-float v4, v4, v18

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v12

    .line 440
    :cond_1
    div-float v11, v12, v14

    .line 448
    :goto_1
    move v7, v12

    .line 449
    .local v7, "itemW":F
    move v8, v11

    .line 450
    .local v8, "itemH":F
    neg-float v3, v12

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v7, v4

    add-float v15, v3, v4

    .line 451
    .local v15, "startX":F
    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, v8, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v8, v4

    sub-float v16, v3, v4

    .line 452
    .local v16, "startY":F
    move-object/from16 v2, p1

    .line 454
    .local v2, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->getDistance()F

    move-result v3

    neg-float v3, v3

    move/from16 v0, v16

    invoke-virtual {v2, v15, v0, v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 455
    invoke-virtual {v2, v7, v8}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setSize(FF)V

    .line 456
    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setScale(FF)V

    .line 458
    const/4 v10, 0x0

    .line 459
    .local v10, "left":F
    const/16 v17, 0x0

    .line 461
    .local v17, "top":F
    const/4 v3, 0x0

    const/4 v4, 0x0

    add-float/2addr v4, v7

    const/4 v5, 0x0

    const/4 v6, 0x0

    add-float/2addr v6, v8

    invoke-virtual/range {v2 .. v8}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setCustomTexCoord(FFFFFF)V

    goto/16 :goto_0

    .line 442
    .end local v2    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .end local v7    # "itemW":F
    .end local v8    # "itemH":F
    .end local v10    # "left":F
    .end local v15    # "startX":F
    .end local v16    # "startY":F
    .end local v17    # "top":F
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowZoomOnFace;->mFacesInfo:[Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    iget v3, v3, Lcom/sec/samsung/gallery/view/slideshowview/SlidehShowFaceInfo;->scaleValue:F

    const v4, 0x3f99999a    # 1.2f

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_3

    float-to-double v4, v14

    const-wide/high16 v20, 0x3fe0000000000000L    # 0.5

    cmpl-double v3, v4, v20

    if-lez v3, :cond_3

    const/high16 v3, 0x40000000    # 2.0f

    cmpg-float v3, v14, v3

    if-gez v3, :cond_3

    .line 443
    const/high16 v3, 0x3fc00000    # 1.5f

    div-float v3, v11, v3

    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v4, v9

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v11

    .line 445
    :cond_3
    mul-float v12, v11, v14

    goto :goto_1
.end method
