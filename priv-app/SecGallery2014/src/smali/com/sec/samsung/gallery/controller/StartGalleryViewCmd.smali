.class public Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "StartGalleryViewCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field private static final EXTRA_SLIDESHOW:Ljava/lang/String; = "slideshow"

.field private static final TAG:Ljava/lang/String;

.field private static mBundle:Landroid/os/Bundle;


# instance fields
.field private ATCH:Ljava/lang/String;

.field private PICK:Ljava/lang/String;

.field private mActivity:Landroid/app/Activity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-class v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->TAG:Ljava/lang/String;

    .line 66
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mBundle:Landroid/os/Bundle;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 62
    const-string v0, "PICK"

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->PICK:Ljava/lang/String;

    .line 63
    const-string v0, "ATCH"

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->ATCH:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$100()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method private addToSlideshowProxy()V
    .locals 4

    .prologue
    .line 312
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSlideShowSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    .line 313
    .local v1, "slideShowProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    .line 314
    .local v0, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 315
    .local v2, "topSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_0

    .line 316
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    .line 317
    new-instance v3, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd$7;

    invoke-direct {v3, p0, v1}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd$7;-><init>(Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/data/MediaSet;->enumerateTotalMediaItems(Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;)V

    .line 325
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setCurrentIndex(I)V

    .line 326
    return-void
.end method

.method private getCoverItemPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "cameraAlbumPath"    # Ljava/lang/String;

    .prologue
    .line 224
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getCoverMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 227
    :goto_0
    return-object v1

    .line 226
    :catch_0
    move-exception v0

    .line 227
    .local v0, "e":Ljava/lang/NullPointerException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getLastestActivityState(Landroid/content/Context;)Ljava/lang/Class;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 342
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDefaultAlbumView:Z

    if-eqz v7, :cond_1

    .line 343
    const-class v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .line 344
    .local v0, "activityState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 350
    .local v3, "defaultTabTagType":Lcom/sec/samsung/gallery/core/TabTagType;
    :goto_0
    const/4 v7, 0x0

    sput-object v7, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mBundle:Landroid/os/Bundle;

    move-object v7, p0

    .line 352
    check-cast v7, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v4

    .line 353
    .local v4, "lastTabTagType":Lcom/sec/samsung/gallery/core/TabTagType;
    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v4, v7, :cond_2

    .line 354
    const-class v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .line 411
    .end local p0    # "context":Landroid/content/Context;
    :cond_0
    :goto_1
    return-object v0

    .line 346
    .end local v0    # "activityState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    .end local v3    # "defaultTabTagType":Lcom/sec/samsung/gallery/core/TabTagType;
    .end local v4    # "lastTabTagType":Lcom/sec/samsung/gallery/core/TabTagType;
    .restart local p0    # "context":Landroid/content/Context;
    :cond_1
    const-class v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .line 347
    .restart local v0    # "activityState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    .restart local v3    # "defaultTabTagType":Lcom/sec/samsung/gallery/core/TabTagType;
    goto :goto_0

    .line 355
    .restart local v4    # "lastTabTagType":Lcom/sec/samsung/gallery/core/TabTagType;
    :cond_2
    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eq v4, v7, :cond_3

    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v4, v7, :cond_4

    .line 356
    :cond_3
    const-class v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .line 357
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCategoryViewDefault:Z

    if-nez v7, :cond_0

    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDefaultAlbumView:Z

    if-nez v7, :cond_0

    .line 358
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v7

    invoke-virtual {v7, v3}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    goto :goto_1

    .line 360
    .restart local p0    # "context":Landroid/content/Context;
    :cond_4
    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_OTHER_DEVICE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eq v4, v7, :cond_5

    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALL:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v4, v7, :cond_7

    .line 361
    :cond_5
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDefaultAlbumView:Z

    if-nez v7, :cond_6

    .line 362
    const-class v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .line 364
    :cond_6
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v7

    invoke-virtual {v7, v3}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    goto :goto_1

    .restart local p0    # "context":Landroid/content/Context;
    :cond_7
    move-object v7, p0

    .line 366
    check-cast v7, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v7

    invoke-virtual {v7, v3}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 368
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 369
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v2, ""

    .line 370
    .local v2, "categoryType":Ljava/lang/String;
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCategoryViewDefault:Z

    if-eqz v7, :cond_0

    .line 371
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v7

    invoke-virtual {v7, v4}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 373
    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SCENERY:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v4, v7, :cond_9

    .line 374
    const-string v2, "Scenery"

    .line 389
    :cond_8
    :goto_2
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_e

    .line 390
    sget-object v7, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getLastestActivityState(): lastTabTagType="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "; activityState="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    :goto_3
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEventView:Z

    if-eqz v7, :cond_0

    .line 404
    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_EVENT:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v4, v7, :cond_0

    .line 405
    const-class v0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    goto/16 :goto_1

    .line 375
    :cond_9
    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_DOCUMENTS:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v4, v7, :cond_a

    .line 376
    const-string v2, "Documents"

    goto :goto_2

    .line 377
    :cond_a
    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FOOD:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v4, v7, :cond_b

    .line 378
    const-string v2, "Food"

    goto :goto_2

    .line 379
    :cond_b
    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_PETS:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v4, v7, :cond_c

    .line 380
    const-string v2, "Pets"

    goto :goto_2

    .line 381
    :cond_c
    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_VEHICLES:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v4, v7, :cond_d

    .line 382
    const-string v2, "Vehicles"

    goto :goto_2

    .line 383
    :cond_d
    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FLOWERS:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v4, v7, :cond_8

    .line 384
    const-string v2, "Flower"

    goto :goto_2

    .line 393
    :cond_e
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/local/categoryalbumset/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 394
    .local v6, "topSetPath":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/local/categoryalbum/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 395
    .local v5, "path":Ljava/lang/String;
    const-string v7, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v1, v7, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    const-string v7, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v1, v7, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    const-string v7, "KEY_MEDIA_SET_POSITION"

    const/4 v8, 0x0

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 398
    const-string v7, "KEY_NO_SPLIT_MODE"

    const/4 v8, 0x1

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 399
    sput-object v1, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mBundle:Landroid/os/Bundle;

    .line 400
    const-class v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    goto :goto_3
.end method

.method private processIntent()V
    .locals 32

    .prologue
    .line 75
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v27

    .line 76
    .local v27, "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v21

    .line 77
    .local v21, "intent":Landroid/content/Intent;
    invoke-virtual/range {v21 .. v21}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 78
    .local v4, "action":Ljava/lang/String;
    invoke-virtual/range {v21 .. v21}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v16

    .line 79
    .local v16, "extras":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-static {v5}, Lsstream/lib/SStreamContentManager;->getStoryItemIdFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v28

    .line 81
    .local v28, "storyItemId":Ljava/lang/String;
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLowMemoryCheckDialog:Z

    if-eqz v5, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    check-cast v5, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    check-cast v5, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v6, v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->checkLowStorage(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 220
    :cond_0
    :goto_0
    return-void

    .line 87
    :cond_1
    if-nez v4, :cond_3

    .line 88
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    check-cast v5, Lcom/sec/android/gallery3d/app/GalleryActivity;

    const/4 v6, 0x1

    iput-boolean v6, v5, Lcom/sec/android/gallery3d/app/GalleryActivity;->mNeedFirstUpOfDetailView:Z

    .line 89
    const/4 v5, 0x1

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/app/StateManager;->setUpButtonVisible(Z)V

    .line 90
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "/local/all/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v6, Lcom/sec/android/gallery3d/util/MediaSetUtils;->CAMERA_BUCKET_ID:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 91
    .local v14, "cameraAlbumPath":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->getCoverItemPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 92
    .local v15, "coverItemPath":Ljava/lang/String;
    if-nez v15, :cond_2

    .line 93
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->getResIdFromFilterType(Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)I

    move-result v6

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 95
    :cond_2
    new-instance v13, Landroid/os/Bundle;

    invoke-direct {v13}, Landroid/os/Bundle;-><init>()V

    .line 96
    .local v13, "bundle":Landroid/os/Bundle;
    const-string v5, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v13, v5, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string v5, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v13, v5, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string v5, "KEY_MEDIA_SET_POSITION"

    const/4 v6, 0x0

    invoke-virtual {v13, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 99
    const-string v5, "KEY_ITEM_POSITION"

    const/4 v6, 0x0

    invoke-virtual {v13, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 100
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v5

    const-class v6, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v5, v6, v13}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0

    .line 101
    .end local v13    # "bundle":Landroid/os/Bundle;
    .end local v14    # "cameraAlbumPath":Ljava/lang/String;
    .end local v15    # "coverItemPath":Ljava/lang/String;
    :cond_3
    const-string v5, "android.intent.action.GET_CONTENT"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string v5, "android.intent.action.PICK"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 103
    :cond_4
    if-eqz v27, :cond_7

    .line 104
    if-eqz v16, :cond_5

    const-string v5, "from-photowall"

    const/4 v6, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-nez v5, :cond_6

    :cond_5
    if-eqz v16, :cond_8

    const-string v5, "multi-pick"

    const/4 v6, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 106
    :cond_6
    sget-object v5, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentLaunchMode(Lcom/sec/samsung/gallery/core/LaunchModeType;)V

    .line 111
    :cond_7
    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->startPickMode()V

    goto/16 :goto_0

    .line 108
    :cond_8
    sget-object v5, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentLaunchMode(Lcom/sec/samsung/gallery/core/LaunchModeType;)V

    goto :goto_1

    .line 112
    :cond_9
    const-string v5, "android.intent.action.MULTIPLE_PICK"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 113
    if-eqz v27, :cond_a

    .line 114
    sget-object v5, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentLaunchMode(Lcom/sec/samsung/gallery/core/LaunchModeType;)V

    .line 116
    :cond_a
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->startPickMode()V

    goto/16 :goto_0

    .line 117
    :cond_b
    const-string v5, "android.intent.action.PERSON_PICK"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 118
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "person-pick"

    const/16 v30, 0x1

    move/from16 v0, v30

    invoke-virtual {v5, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 119
    if-eqz v27, :cond_c

    .line 120
    sget-object v5, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentLaunchMode(Lcom/sec/samsung/gallery/core/LaunchModeType;)V

    .line 122
    :cond_c
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->startPickMode()V

    goto/16 :goto_0

    .line 123
    :cond_d
    const-string v5, "android.intent.action.VIEW"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_e

    const-string v5, "com.android.camera.action.REVIEW"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_e

    const-string v5, "android.intent.action.VIEW_GALLERY_TO_MEMO"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_e

    invoke-static/range {v21 .. v21}, Lcom/sec/android/gallery3d/sconnect/SConnectUtil;->isSConnectIntent(Landroid/content/Intent;)Z

    move-result v5

    if-nez v5, :cond_e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    move-object/from16 v0, v21

    invoke-static {v5, v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->isSLinkIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 128
    :cond_e
    if-eqz v16, :cond_14

    .line 129
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    check-cast v5, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getGalleryMotion()Lcom/sec/android/gallery3d/app/GalleryMotion;

    move-result-object v17

    .line 130
    .local v17, "galleryMotion":Lcom/sec/android/gallery3d/app/GalleryMotion;
    if-eqz v17, :cond_11

    .line 131
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v24

    .line 132
    .local v24, "motionTutorialType":Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    sget-object v5, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_NONE:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-object/from16 v0, v24

    if-eq v0, v5, :cond_11

    .line 133
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v5, :cond_10

    sget-object v5, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->AIR_MOTION_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-object/from16 v0, v24

    if-eq v0, v5, :cond_f

    sget-object v5, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->AIR_MOTION_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-object/from16 v0, v24

    if-ne v0, v5, :cond_10

    .line 135
    :cond_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/samsung/gallery/view/help/GalleryHelp;->isAvailableDownloadableHelp(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_10

    .line 136
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0e0057

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 137
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 141
    :cond_10
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->startTryTiltMode(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 145
    .end local v24    # "motionTutorialType":Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    :cond_11
    const-string v5, "IsHelpMode"

    const/4 v6, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v18

    .line 146
    .local v18, "helpMode":Z
    if-eqz v18, :cond_12

    .line 147
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->startHelpMode(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 150
    :cond_12
    const-string/jumbo v5, "slideshow"

    const/4 v6, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v26

    .line 151
    .local v26, "slideshow":Z
    if-eqz v26, :cond_13

    .line 152
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->startSlideShowMode()V

    goto/16 :goto_0

    .line 155
    :cond_13
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFestival:Z

    if-eqz v5, :cond_14

    .line 156
    const-string v5, "festivalMemoryWidget"

    const/4 v6, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v22

    .line 157
    .local v22, "isFestivalmode":Z
    const/4 v5, 0x1

    move/from16 v0, v22

    if-ne v0, v5, :cond_14

    .line 158
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    .line 159
    .local v12, "appImpl":Lcom/sec/android/gallery3d/app/GalleryAppImpl;
    const-string v5, "mediaCount"

    const/4 v6, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 160
    .local v7, "count":I
    const-string/jumbo v5, "startDate"

    const-wide/16 v30, 0x0

    move-object/from16 v0, v21

    move-wide/from16 v1, v30

    invoke-virtual {v0, v5, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    .line 161
    .local v8, "startDate":J
    const-string v5, "endDate"

    const-wide/16 v30, 0x0

    move-object/from16 v0, v21

    move-wide/from16 v1, v30

    invoke-virtual {v0, v5, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v10

    .line 162
    .local v10, "endDate":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    check-cast v5, Lcom/sec/android/gallery3d/app/GalleryActivity;

    const/4 v6, 0x1

    invoke-virtual/range {v5 .. v11}, Lcom/sec/android/gallery3d/app/GalleryActivity;->setFestivalData(ZIJJ)V

    .line 163
    const/4 v6, 0x1

    move-object v5, v12

    invoke-virtual/range {v5 .. v11}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->setImplFestivalData(ZIJJ)V

    .line 164
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    new-instance v6, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd$1;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;)V

    invoke-virtual {v5, v6}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 176
    .end local v7    # "count":I
    .end local v8    # "startDate":J
    .end local v10    # "endDate":J
    .end local v12    # "appImpl":Lcom/sec/android/gallery3d/app/GalleryAppImpl;
    .end local v17    # "galleryMotion":Lcom/sec/android/gallery3d/app/GalleryMotion;
    .end local v18    # "helpMode":Z
    .end local v22    # "isFestivalmode":Z
    .end local v26    # "slideshow":Z
    :cond_14
    invoke-static/range {v21 .. v21}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->setPendedPlayer(Landroid/content/Intent;)V

    .line 178
    const-string v5, "android.intent.action.VIEW"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_15

    invoke-virtual/range {v21 .. v21}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    if-nez v5, :cond_15

    .line 179
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->startDefaultMode()V

    goto/16 :goto_0

    .line 180
    :cond_15
    const-string v5, "android.intent.action.VIEW"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_16

    if-eqz v28, :cond_16

    .line 181
    const-string v5, "_"

    move-object/from16 v0, v28

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v6

    move-object/from16 v0, v28

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    .line 183
    .local v23, "keyword":Ljava/lang/String;
    if-eqz v23, :cond_0

    .line 184
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v29

    .line 185
    .local v29, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 186
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->startImageViewMode()V

    goto/16 :goto_0

    .line 189
    .end local v23    # "keyword":Ljava/lang/String;
    .end local v29    # "uri":Landroid/net/Uri;
    :cond_16
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->startImageViewMode()V

    goto/16 :goto_0

    .line 192
    :cond_17
    const-string v5, "com.android.gallery.action.SEARCH_VIEW"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 193
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v29

    .line 194
    .restart local v29    # "uri":Landroid/net/Uri;
    if-eqz v29, :cond_0

    .line 195
    invoke-virtual/range {v29 .. v29}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v20

    .line 196
    .local v20, "id":Ljava/lang/String;
    const-string v5, "query"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 197
    .local v25, "query":Ljava/lang/String;
    if-eqz v20, :cond_0

    .line 198
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v29

    .line 199
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 200
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->startImageViewMode()V

    goto/16 :goto_0

    .line 203
    .end local v20    # "id":Ljava/lang/String;
    .end local v25    # "query":Ljava/lang/String;
    .end local v29    # "uri":Landroid/net/Uri;
    :cond_18
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v5, :cond_19

    const-string v5, "com.android.gallery.NATIVE_VIEWER"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_19

    .line 204
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    check-cast v5, Lcom/sec/android/gallery3d/app/GalleryActivity;

    move-object/from16 v0, v21

    invoke-static {v0, v5}, Lcom/sec/android/gallery3d/homesync/HomeSyncIntent;->startHomeSyncNativeViewerMode(Landroid/content/Intent;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    goto/16 :goto_0

    .line 206
    :cond_19
    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    .line 207
    .local v19, "hidden":Ljava/lang/Boolean;
    if-eqz v16, :cond_1a

    .line 208
    const-string v5, "hiddenitem"

    const/4 v6, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    .line 210
    :cond_1a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->setCurrentClusterType(I)V

    .line 214
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 215
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v5

    const-class v6, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    const/16 v30, 0x0

    move-object/from16 v0, v30

    invoke-virtual {v5, v6, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 218
    :cond_1b
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->startDefaultMode()V

    goto/16 :goto_0
.end method

.method private showBargeInNotification()V
    .locals 3

    .prologue
    .line 415
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;->SHOW:Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;

    aput-object v2, v0, v1

    .line 418
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "SHOW_BARGEIN_NOTIFICATION"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 420
    return-void
.end method

.method private startDefaultMode()V
    .locals 2

    .prologue
    .line 329
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd$8;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd$8;-><init>(Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 336
    return-void
.end method

.method private startHelpMode(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    .line 245
    const-string v3, "HelpMode"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 246
    .local v0, "mode":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/GalleryHelp;->getHelpType(Ljava/lang/String;)I

    move-result v2

    .line 247
    .local v2, "type":I
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v3, :cond_0

    const/4 v3, 0x6

    if-eq v2, v3, :cond_0

    const/4 v3, 0x4

    if-eq v2, v3, :cond_0

    .line 249
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/samsung/gallery/view/help/GalleryHelp;->isAvailableDownloadableHelp(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 250
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e0057

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 251
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    .line 266
    :goto_0
    return-void

    .line 256
    :cond_0
    const/4 v3, 0x3

    new-array v1, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v1, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    aput-object v4, v1, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    aput-object v4, v1, v3

    .line 259
    .local v1, "params":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    new-instance v4, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd$3;

    invoke-direct {v4, p0, v1}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd$3;-><init>(Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;[Ljava/lang/Object;)V

    invoke-virtual {v3, v4}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private startImageViewMode()V
    .locals 3

    .prologue
    .line 269
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mNeedFirstUpOfDetailView:Z

    .line 270
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd$4;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd$4;-><init>(Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 280
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->ATCH:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 281
    return-void
.end method

.method private startPickMode()V
    .locals 3

    .prologue
    .line 284
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd$5;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd$5;-><init>(Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 294
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->PICK:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 295
    return-void
.end method

.method private startSlideShowMode()V
    .locals 2

    .prologue
    .line 298
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->addToSlideshowProxy()V

    .line 299
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd$6;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd$6;-><init>(Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 309
    return-void
.end method

.method private startTryTiltMode(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    .line 232
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    aput-object v2, v0, v1

    .line 235
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    new-instance v2, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd$2;

    invoke-direct {v2, p0, v0}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd$2;-><init>(Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;[Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 242
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 1
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 70
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->mActivity:Landroid/app/Activity;

    .line 71
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->processIntent()V

    .line 72
    return-void
.end method
