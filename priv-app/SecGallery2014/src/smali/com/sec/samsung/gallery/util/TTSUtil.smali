.class public Lcom/sec/samsung/gallery/util/TTSUtil;
.super Ljava/lang/Object;
.source "TTSUtil.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mInstance:Lcom/sec/samsung/gallery/util/TTSUtil;


# instance fields
.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mActivityCount:I

.field private mContext:Landroid/content/Context;

.field private mTTS:Landroid/speech/tts/TextToSpeech;

.field private mVibrator:Landroid/os/Vibrator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/sec/samsung/gallery/util/TTSUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/util/TTSUtil;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mActivityCount:I

    .line 49
    return-void
.end method

.method public static getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mInstance:Lcom/sec/samsung/gallery/util/TTSUtil;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Lcom/sec/samsung/gallery/util/TTSUtil;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/util/TTSUtil;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mInstance:Lcom/sec/samsung/gallery/util/TTSUtil;

    .line 54
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mInstance:Lcom/sec/samsung/gallery/util/TTSUtil;

    return-object v0
.end method

.method private getPeopleClusterString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "speech"    # Ljava/lang/String;

    .prologue
    .line 377
    iget-object v2, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    move-object v0, p1

    .line 384
    .end local p1    # "speech":Ljava/lang/String;
    .local v0, "speech":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 379
    .end local v0    # "speech":Ljava/lang/String;
    .restart local p1    # "speech":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v2

    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v2, v3, :cond_2

    const-string v2, "/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 381
    const-string v2, "/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 382
    .local v1, "strList":[Ljava/lang/String;
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aget-object p1, v1, v2

    .end local v1    # "strList":[Ljava/lang/String;
    :cond_2
    move-object v0, p1

    .line 384
    .end local p1    # "speech":Ljava/lang/String;
    .restart local v0    # "speech":Ljava/lang/String;
    goto :goto_0
.end method

.method private getPeopleString(Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;I)Ljava/lang/String;
    .locals 9
    .param p1, "adapter"    # Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;
    .param p2, "index"    # I

    .prologue
    const v8, 0x7f0e02b8

    .line 388
    const-string v3, ""

    .line 389
    .local v3, "sentence":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 390
    sget-object v6, Lcom/sec/samsung/gallery/util/TTSUtil;->TAG:Ljava/lang/String;

    const-string v7, "ComposeMediaItemAdpater: Null"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v3

    .line 421
    .end local v3    # "sentence":Ljava/lang/String;
    .local v4, "sentence":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 394
    .end local v4    # "sentence":Ljava/lang/String;
    .restart local v3    # "sentence":Ljava/lang/String;
    :cond_0
    shr-int/lit8 v6, p2, 0x10

    invoke-virtual {p1, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 395
    shr-int/lit8 v6, p2, 0x10

    invoke-virtual {p1, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v2

    .line 396
    .local v2, "peopleName":Ljava/lang/String;
    shr-int/lit8 v6, p2, 0x10

    const v7, 0xffff

    and-int/2addr v7, p2

    invoke-virtual {p1, v6, v7}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    .line 397
    .local v1, "media":Lcom/sec/android/gallery3d/data/MediaItem;
    const-string v6, "/"

    invoke-virtual {v2, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 399
    const-string v6, "/"

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 400
    .local v5, "strList":[Ljava/lang/String;
    array-length v6, v5

    add-int/lit8 v6, v6, -0x1

    aget-object v2, v5, v6

    .line 403
    .end local v5    # "strList":[Ljava/lang/String;
    :cond_1
    if-eqz v1, :cond_5

    .line 404
    const-string v0, ""

    .line 405
    .local v0, "itemName":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 406
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v0

    .line 415
    :cond_2
    :goto_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .end local v0    # "itemName":Ljava/lang/String;
    .end local v1    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v2    # "peopleName":Ljava/lang/String;
    :cond_3
    :goto_2
    move-object v4, v3

    .line 421
    .end local v3    # "sentence":Ljava/lang/String;
    .restart local v4    # "sentence":Ljava/lang/String;
    goto :goto_0

    .line 408
    .end local v4    # "sentence":Ljava/lang/String;
    .restart local v0    # "itemName":Ljava/lang/String;
    .restart local v1    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v2    # "peopleName":Ljava/lang/String;
    .restart local v3    # "sentence":Ljava/lang/String;
    :cond_4
    const-string v6, ""

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 409
    new-instance v6, Ljava/io/File;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 417
    .end local v0    # "itemName":Ljava/lang/String;
    :cond_5
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_2
.end method


# virtual methods
.method public getFormatedString(Lcom/sec/android/gallery3d/data/MediaObject;Lcom/sec/android/gallery3d/ui/SelectionManager;)Ljava/lang/String;
    .locals 24
    .param p1, "media"    # Lcom/sec/android/gallery3d/data/MediaObject;
    .param p2, "selectionProxy"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    .line 166
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v20, v0

    if-nez v20, :cond_1

    .line 167
    const/4 v4, 0x0

    .line 331
    .end local p1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_0
    :goto_0
    return-object v4

    .line 170
    .restart local p1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    const/4 v5, -0x1

    .line 171
    .local v5, "count":I
    const/4 v12, 0x0

    .line 172
    .local v12, "name":Ljava/lang/String;
    const/4 v10, 0x0

    .line 173
    .local v10, "mimeType":Ljava/lang/String;
    const/4 v6, 0x0

    .line 174
    .local v6, "date":Ljava/lang/String;
    const/4 v11, 0x0

    .line 175
    .local v11, "mimetypedate":Ljava/lang/String;
    const/4 v4, 0x0

    .line 176
    .local v4, "baseString":Ljava/lang/String;
    const/4 v7, 0x0

    .line 177
    .local v7, "isMediaSet":Z
    const/16 v16, 0x1

    .line 178
    .local v16, "speakName":Z
    const/4 v15, 0x0

    .line 179
    .local v15, "speakCount":Z
    const/16 v17, 0x0

    .line 180
    .local v17, "specialBaseString":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v18

    .line 181
    .local v18, "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v20

    sget-object v21, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_6

    const/4 v8, 0x1

    .line 182
    .local v8, "isPickerMode":Z
    :goto_1
    const/4 v9, 0x0

    .line 184
    .local v9, "isSoundShot":Z
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/MediaSet;

    move/from16 v20, v0

    if-eqz v20, :cond_7

    move-object/from16 v13, p1

    .line 185
    check-cast v13, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 186
    .local v13, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v5

    .line 187
    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v12

    .line 188
    const/4 v7, 0x1

    .line 197
    .end local v13    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_2
    if-eqz p2, :cond_9

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v20

    if-eqz v20, :cond_9

    .line 200
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 201
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "%s. "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e029c

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e029d

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 279
    :goto_3
    if-eqz v7, :cond_2

    sget-boolean v20, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v20, :cond_2

    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    move/from16 v20, v0

    if-nez v20, :cond_2

    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    move/from16 v20, v0

    if-nez v20, :cond_2

    if-eqz v12, :cond_2

    const-string v20, "/"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 281
    invoke-static {v12}, Lcom/sec/android/gallery3d/util/GalleryUtils;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v19

    .line 282
    .local v19, "values":[Ljava/lang/String;
    const/16 v20, 0x1

    aget-object v12, v19, v20

    .line 286
    .end local v19    # "values":[Ljava/lang/String;
    :cond_2
    if-eqz v17, :cond_3

    .line 287
    if-eqz v9, :cond_17

    .line 288
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "%s, "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e02b8

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 294
    :cond_3
    :goto_4
    sget-boolean v20, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v20, :cond_4

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecretModeOn()Z

    move-result v20

    if-eqz v20, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/samsung/android/privatemode/PrivateModeManager;->isPrivateStorageMounted(Landroid/content/Context;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 295
    if-eqz v7, :cond_18

    .line 296
    check-cast p1, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local p1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 297
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e02ad

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 314
    :cond_4
    :goto_5
    :pswitch_0
    sget-boolean v20, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoReader:Z

    if-eqz v20, :cond_1b

    .line 315
    if-eqz v16, :cond_0

    if-eqz v12, :cond_0

    .line 317
    if-eqz v15, :cond_5

    const/16 v20, -0x1

    move/from16 v0, v20

    if-ne v5, v0, :cond_1a

    .line 318
    :cond_5
    if-eqz v7, :cond_19

    .line 319
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v12, v20, v21

    move-object/from16 v0, v20

    invoke-static {v4, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 181
    .end local v8    # "isPickerMode":Z
    .end local v9    # "isSoundShot":Z
    .restart local p1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_6
    const/4 v8, 0x0

    goto/16 :goto_1

    .restart local v8    # "isPickerMode":Z
    .restart local v9    # "isSoundShot":Z
    :cond_7
    move-object/from16 v20, p1

    .line 190
    check-cast v20, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v20, p1

    .line 191
    check-cast v20, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x2f

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->indexOf(I)I

    move-result v14

    .local v14, "slashPos":I
    move-object/from16 v20, p1

    .line 192
    check-cast v20, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v1, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v20, p1

    check-cast v20, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v22

    invoke-static/range {v21 .. v23}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    .line 194
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_2

    .line 203
    .end local v14    # "slashPos":I
    :cond_8
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "%s. "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e029c

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e029e

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    .line 204
    :cond_9
    if-eqz v7, :cond_c

    .line 205
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    move/from16 v20, v0

    if-eqz v20, :cond_a

    move-object/from16 v20, p1

    check-cast v20, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getMediaSetType()I

    move-result v20

    if-nez v20, :cond_a

    .line 208
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "%s. "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e0298

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e02b8

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    .line 209
    :cond_a
    const/16 v20, 0x1

    move/from16 v0, v20

    if-ne v5, v0, :cond_b

    .line 211
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "%s. "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e0292

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e02b8

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    .line 213
    :cond_b
    const/4 v15, 0x1

    .line 215
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "%s. "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e0293

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e02b8

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    .line 218
    :cond_c
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/data/MediaObject;->getMediaType()I

    move-result v20

    packed-switch v20, :pswitch_data_0

    .line 268
    :pswitch_1
    if-eqz v8, :cond_16

    .line 270
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "%s. "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e029b

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    .line 220
    :pswitch_2
    if-eqz v8, :cond_d

    .line 222
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "%s. "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e029b

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    .line 225
    :cond_d
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "%s. "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e029a

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 226
    goto/16 :goto_3

    .line 228
    :pswitch_3
    const/16 v16, 0x0

    .line 230
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e0298

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e02b8

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 231
    goto/16 :goto_3

    .line 233
    :pswitch_4
    if-eqz v8, :cond_e

    .line 235
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "%s. "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e029b

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    :cond_e
    move-object/from16 v20, p1

    .line 237
    check-cast v20, Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v22, 0x4

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v20

    if-eqz v20, :cond_f

    .line 238
    const/16 v17, 0x1

    .line 240
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f0e02f3

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    :cond_f
    move-object/from16 v20, p1

    .line 241
    check-cast v20, Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v22, 0x10

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v20

    if-eqz v20, :cond_10

    .line 242
    const/16 v17, 0x1

    .line 243
    const/4 v9, 0x1

    .line 245
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f0e027e

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    :cond_10
    move-object/from16 v20, p1

    .line 246
    check-cast v20, Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v22, 0x20

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v20

    if-eqz v20, :cond_11

    .line 247
    const/16 v17, 0x1

    .line 249
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f0e02f1

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    :cond_11
    move-object/from16 v20, p1

    .line 250
    check-cast v20, Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/32 v22, 0x80000

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v20

    if-eqz v20, :cond_12

    .line 251
    const/16 v17, 0x1

    .line 253
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f0e01fe

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    :cond_12
    move-object/from16 v20, p1

    .line 254
    check-cast v20, Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v22, 0x400

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v20

    if-eqz v20, :cond_13

    .line 255
    const/16 v17, 0x1

    .line 257
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f0e04ab

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    :cond_13
    move-object/from16 v20, p1

    .line 258
    check-cast v20, Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v22, 0x8

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v20

    if-nez v20, :cond_14

    move-object/from16 v20, p1

    check-cast v20, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/data/MediaItem;->is3DPanorama()Z

    move-result v20

    if-eqz v20, :cond_15

    .line 259
    :cond_14
    const/16 v17, 0x1

    .line 261
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f0e04ac

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    .line 264
    :cond_15
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "%s. "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e02b8

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 266
    goto/16 :goto_3

    .line 273
    :cond_16
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "%s. "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e02b8

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    .line 290
    :cond_17
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "%s, "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e02b8

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_4

    .line 299
    :cond_18
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/MediaItem;

    move/from16 v20, v0

    if-eqz v20, :cond_4

    move-object/from16 v20, p1

    .line 300
    check-cast v20, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 301
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/data/MediaObject;->getMediaType()I

    move-result v20

    packed-switch v20, :pswitch_data_1

    :pswitch_5
    goto/16 :goto_5

    .line 306
    :pswitch_6
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f0e02ad

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_5

    .line 321
    .end local p1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_19
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v11, v20, v21

    move-object/from16 v0, v20

    invoke-static {v4, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 324
    :cond_1a
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v12, v20, v21

    const/16 v21, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    move-object/from16 v0, v20

    invoke-static {v4, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 326
    :cond_1b
    if-eqz v16, :cond_0

    if-eqz v12, :cond_0

    .line 328
    if-eqz v15, :cond_1c

    const/16 v20, -0x1

    move/from16 v0, v20

    if-ne v5, v0, :cond_1d

    .line 329
    :cond_1c
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v12, v20, v21

    move-object/from16 v0, v20

    invoke-static {v4, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 331
    :cond_1d
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v12, v20, v21

    const/16 v21, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    move-object/from16 v0, v20

    invoke-static {v4, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 218
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 301
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public init(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mActivityCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mActivityCount:I

    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    if-nez v0, :cond_0

    .line 60
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    .line 61
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    new-instance v0, Landroid/speech/tts/TextToSpeech;

    iget-object v1, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    .line 63
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mVibrator:Landroid/os/Vibrator;

    .line 66
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 361
    iput-object v1, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 362
    iget v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mActivityCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mActivityCount:I

    if-lez v0, :cond_0

    .line 374
    :goto_0
    return-void

    .line 365
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_1

    .line 366
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 367
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 368
    iput-object v1, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    .line 370
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mVibrator:Landroid/os/Vibrator;

    if-eqz v0, :cond_2

    .line 371
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 372
    :cond_2
    iput-object v1, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mVibrator:Landroid/os/Vibrator;

    .line 373
    sput-object v1, Lcom/sec/samsung/gallery/util/TTSUtil;->mInstance:Lcom/sec/samsung/gallery/util/TTSUtil;

    goto :goto_0
.end method

.method public onInit(I)V
    .locals 2
    .param p1, "initStatus"    # I

    .prologue
    .line 337
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 338
    sget-object v0, Lcom/sec/samsung/gallery/util/TTSUtil;->TAG:Ljava/lang/String;

    const-string v1, "Warning, TTS not initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    :cond_0
    return-void
.end method

.method public onResume(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 3
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 342
    iput-object p1, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 344
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    if-nez v1, :cond_0

    .line 345
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 346
    new-instance v1, Landroid/speech/tts/TextToSpeech;

    iget-object v2, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, p0}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    .line 347
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "vibrator"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    iput-object v1, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mVibrator:Landroid/os/Vibrator;

    .line 352
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v1}, Landroid/speech/tts/TextToSpeech;->getCurrentEngine()Ljava/lang/String;

    move-result-object v0

    .line 353
    .local v0, "engine":Ljava/lang/String;
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v1}, Landroid/speech/tts/TextToSpeech;->getDefaultEngine()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 354
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v1}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 355
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v1}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 356
    new-instance v1, Landroid/speech/tts/TextToSpeech;

    iget-object v2, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, p0}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    .line 358
    .end local v0    # "engine":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public varargs speak(I[Ljava/lang/Object;)V
    .locals 5
    .param p1, "formatResId"    # I
    .param p2, "param"    # [Ljava/lang/Object;

    .prologue
    .line 69
    iget-object v2, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 78
    :goto_0
    return-void

    .line 71
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "formatStr":Ljava/lang/String;
    move-object v1, v0

    .line 73
    .local v1, "sentence":Ljava/lang/String;
    const v2, 0x7f0e029d

    if-eq p1, v2, :cond_1

    const v2, 0x7f0e029e

    if-ne p1, v2, :cond_2

    .line 74
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%s. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    const v4, 0x7f0e029c

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v3, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 75
    :cond_2
    if-eqz p2, :cond_3

    .line 76
    invoke-static {v0, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 77
    :cond_3
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public speak(Lcom/sec/android/gallery3d/data/MediaObject;)V
    .locals 2
    .param p1, "media"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 100
    const/4 v0, 0x0

    .line 102
    .local v0, "sentence":Ljava/lang/String;
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-eqz v1, :cond_0

    .line 103
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Lcom/sec/samsung/gallery/util/TTSUtil;->getFormatedString(Lcom/sec/android/gallery3d/data/MediaObject;Lcom/sec/android/gallery3d/ui/SelectionManager;)Ljava/lang/String;

    move-result-object v0

    .line 105
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(Ljava/lang/String;)V

    .line 106
    return-void
.end method

.method public speak(Ljava/lang/Object;I)V
    .locals 1
    .param p1, "adapter"    # Ljava/lang/Object;
    .param p2, "index"    # I

    .prologue
    .line 96
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(Ljava/lang/Object;ILcom/sec/android/gallery3d/ui/SelectionManager;)V

    .line 97
    return-void
.end method

.method public speak(Ljava/lang/Object;ILcom/sec/android/gallery3d/ui/SelectionManager;)V
    .locals 12
    .param p1, "adapter"    # Ljava/lang/Object;
    .param p2, "index"    # I
    .param p3, "selectionProxy"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    const v11, 0x7f0e02b8

    const v10, 0x7f0e029f

    .line 109
    iget-object v7, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-nez v7, :cond_1

    .line 163
    .end local p1    # "adapter":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-void

    .line 112
    .restart local p1    # "adapter":Ljava/lang/Object;
    :cond_1
    const/4 v5, 0x0

    .line 113
    .local v5, "sentence":Ljava/lang/String;
    const/4 v4, 0x0

    .line 114
    .local v4, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    iget-object v7, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v6

    .line 115
    .local v6, "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v7

    sget-object v8, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v7, v8, :cond_5

    const/4 v1, 0x1

    .line 117
    .local v1, "isPeopleView":Z
    :goto_1
    instance-of v7, p1, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    if-eqz v7, :cond_6

    move-object v7, p1

    .line 118
    check-cast v7, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    invoke-virtual {v7, p2}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    .line 154
    :cond_2
    :goto_2
    if-eqz v4, :cond_3

    .line 155
    invoke-virtual {p0, v4, p3}, Lcom/sec/samsung/gallery/util/TTSUtil;->getFormatedString(Lcom/sec/android/gallery3d/data/MediaObject;Lcom/sec/android/gallery3d/ui/SelectionManager;)Ljava/lang/String;

    move-result-object v5

    .line 158
    :cond_3
    if-eqz v1, :cond_4

    instance-of v7, p1, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    if-eqz v7, :cond_4

    .line 159
    check-cast p1, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    .end local p1    # "adapter":Ljava/lang/Object;
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/util/TTSUtil;->getPeopleString(Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;I)Ljava/lang/String;

    move-result-object v5

    .line 162
    :cond_4
    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(Ljava/lang/String;)V

    goto :goto_0

    .line 115
    .end local v1    # "isPeopleView":Z
    .restart local p1    # "adapter":Ljava/lang/Object;
    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    .line 119
    .restart local v1    # "isPeopleView":Z
    :cond_6
    instance-of v7, p1, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;

    if-eqz v7, :cond_7

    move-object v2, p1

    .line 120
    check-cast v2, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;

    .line 121
    .local v2, "itemAdapter":Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;
    invoke-virtual {v2, p2}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    move-object v7, v4

    .line 122
    check-cast v7, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2, v7}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->isBurstShot(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 123
    const/4 v4, 0x0

    .line 125
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v8, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v8, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    .line 127
    .end local v2    # "itemAdapter":Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;
    :cond_7
    instance-of v7, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-eqz v7, :cond_8

    move-object v7, p1

    .line 128
    check-cast v7, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v7, p2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    goto :goto_2

    .line 129
    :cond_8
    instance-of v7, p1, Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    if-eqz v7, :cond_9

    move-object v7, p1

    .line 130
    check-cast v7, Lcom/sec/android/gallery3d/app/AlbumDataLoader;

    invoke-virtual {v7, p2}, Lcom/sec/android/gallery3d/app/AlbumDataLoader;->get(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    goto :goto_2

    .line 131
    :cond_9
    instance-of v7, p1, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;

    if-eqz v7, :cond_a

    move-object v7, p1

    .line 132
    check-cast v7, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;

    invoke-virtual {v7, p2}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->getMedia(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v4

    goto :goto_2

    .line 133
    :cond_a
    instance-of v7, p1, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    if-eqz v7, :cond_b

    .line 134
    shr-int/lit8 v0, p2, 0x10

    .local v0, "albumIndex":I
    move-object v7, p1

    .line 135
    check-cast v7, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v7, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    .line 136
    goto/16 :goto_2

    .end local v0    # "albumIndex":I
    :cond_b
    instance-of v7, p1, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    if-eqz v7, :cond_c

    .line 137
    shr-int/lit8 v0, p2, 0x10

    .restart local v0    # "albumIndex":I
    move-object v7, p1

    .line 138
    check-cast v7, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v7, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    .line 139
    goto/16 :goto_2

    .end local v0    # "albumIndex":I
    :cond_c
    instance-of v7, p1, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    if-eqz v7, :cond_d

    .line 140
    shr-int/lit8 v0, p2, 0x10

    .line 141
    .restart local v0    # "albumIndex":I
    const v7, 0xffff

    and-int v3, p2, v7

    .local v3, "itemIndex":I
    move-object v7, p1

    .line 142
    check-cast v7, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v7, v0, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    .line 143
    if-eqz v4, :cond_2

    move-object v7, p1

    .line 144
    check-cast v7, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    move-object v8, v4

    check-cast v8, Lcom/sec/android/gallery3d/data/MediaItem;

    move-object v9, p1

    check-cast v9, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v9, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->isBurstShot(Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaSet;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 145
    const/4 v4, 0x0

    .line 147
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v8, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v8, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2

    .line 150
    .end local v0    # "albumIndex":I
    .end local v3    # "itemIndex":I
    :cond_d
    instance-of v7, p1, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    if-eqz v7, :cond_2

    move-object v7, p1

    .line 151
    check-cast v7, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v7, p2}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    goto/16 :goto_2
.end method

.method public speak(Ljava/lang/String;)V
    .locals 4
    .param p1, "speech"    # Ljava/lang/String;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 83
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    if-nez v0, :cond_3

    .line 84
    sget-object v0, Lcom/sec/samsung/gallery/util/TTSUtil;->TAG:Ljava/lang/String;

    const-string v1, "Error, TTS not initialized"

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 89
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mVibrator:Landroid/os/Vibrator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 91
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mVibrator:Landroid/os/Vibrator;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    goto :goto_0

    .line 85
    :cond_3
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 86
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/TTSUtil;->getPeopleClusterString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 87
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/TTSUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    goto :goto_1
.end method
