.class public Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;
.super Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;
.source "GlSlideshowTransitionShow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$3;,
        Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mCurrentTransitionType:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;

.field private mIsRandom:Z

.field private mRandom:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/samsung/gallery/glview/GlSlideShowView;Landroid/content/Context;)V
    .locals 1
    .param p1, "view"    # Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;-><init>(Lcom/sec/samsung/gallery/glview/GlSlideShowView;Landroid/content/Context;)V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mIsRandom:Z

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mRandom:Ljava/util/Random;

    .line 43
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mContext:Landroid/content/Context;

    .line 44
    return-void
.end method

.method private startSoundScene(Ljava/lang/String;)V
    .locals 4
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 278
    new-instance v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$2;-><init>(Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;)V

    .line 285
    .local v0, "onCompletionListener":Landroid/media/MediaPlayer$OnCompletionListener;
    const/4 v2, 0x4

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mContext:Landroid/content/Context;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/sec/samsung/gallery/controller/SoundSceneCmd$SoundSceneCmdType;->START_PLAY_SOUND:Lcom/sec/samsung/gallery/controller/SoundSceneCmd$SoundSceneCmdType;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object p1, v1, v2

    const/4 v2, 0x3

    aput-object v0, v1, v2

    .line 288
    .local v1, "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getGalleryId()I

    move-result v2

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(I)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "SOUND_SCENE"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 290
    return-void
.end method

.method private stopSoundScene()V
    .locals 3

    .prologue
    .line 293
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mContext:Landroid/content/Context;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/controller/SoundSceneCmd$SoundSceneCmdType;->STOP_PLAY_SOUND:Lcom/sec/samsung/gallery/controller/SoundSceneCmd$SoundSceneCmdType;

    aput-object v2, v0, v1

    .line 296
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getGalleryId()I

    move-result v1

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(I)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "SOUND_SCENE"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 298
    return-void
.end method


# virtual methods
.method public checkForVideoItem(I)V
    .locals 3
    .param p1, "itemIndex"    # I

    .prologue
    .line 198
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 199
    .local v0, "mediaitem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v1

    if-nez v1, :cond_0

    .line 200
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->isPlayingVideoItem:Z

    .line 201
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$1;

    invoke-direct {v2, p0, v0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$1;-><init>(Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;Lcom/sec/android/gallery3d/data/MediaItem;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 209
    :cond_0
    return-void
.end method

.method public getMaxDisplayObjCount()I
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x2

    return v0
.end method

.method public getWaitDuration()J
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mWaitDuration:J

    return-wide v0
.end method

.method public isBitmapdecoded()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 98
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    if-nez v3, :cond_1

    .line 124
    :cond_0
    :goto_0
    return v2

    .line 100
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getCount()I

    move-result v1

    .line 101
    .local v1, "count":I
    if-nez v1, :cond_2

    .line 102
    sget-object v3, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->TAG:Ljava/lang/String;

    const-string v4, "count is 0"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 105
    :cond_2
    iget v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageIndex:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getCount()I

    move-result v4

    if-lt v3, v4, :cond_3

    .line 106
    iput v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageIndex:I

    .line 108
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageIndex:I

    invoke-virtual {v3, v4, v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getItemImage(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 109
    .local v0, "bmp":Landroid/graphics/Bitmap;
    if-nez v0, :cond_4

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageIndex:I

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->isRequestInProgress(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 112
    :cond_4
    if-eqz v0, :cond_0

    .line 114
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageCache:Ljava/util/LinkedList;

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 115
    iget v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageIndex:I

    .line 116
    iget v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageIndex:I

    rem-int/2addr v3, v1

    iput v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageIndex:I

    .line 119
    iget v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mSetImageCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mSetImageCount:I

    .line 120
    iget v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mSetImageCount:I

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->getMaxDisplayObjCount()I

    move-result v4

    if-lt v3, v4, :cond_0

    .line 121
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public isInitialLoaded()Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 64
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    if-nez v2, :cond_0

    move v2, v3

    .line 94
    :goto_0
    return v2

    .line 67
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getCount()I

    move-result v1

    .line 68
    .local v1, "count":I
    if-nez v1, :cond_1

    .line 69
    sget-object v2, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->TAG:Ljava/lang/String;

    const-string v4, "count is 0"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    .line 70
    goto :goto_0

    .line 72
    :cond_1
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->isPlayingVideoItem:Z

    if-nez v2, :cond_3

    .line 75
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageIndex:I

    invoke-virtual {v2, v5, v3}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getItemImage(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 76
    .local v0, "bmp":Landroid/graphics/Bitmap;
    if-nez v0, :cond_2

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageIndex:I

    invoke-virtual {v2, v5}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->isRequestInProgress(I)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v3

    .line 77
    goto :goto_0

    .line 79
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageCache:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 81
    iget v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageIndex:I

    .line 82
    iget v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageIndex:I

    rem-int/2addr v2, v1

    iput v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageIndex:I

    .line 84
    iget v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mSetImageCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mSetImageCount:I

    .line 86
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    :cond_3
    iget v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mSetImageCount:I

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->getMaxDisplayObjCount()I

    move-result v5

    if-lt v2, v5, :cond_4

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageCache:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-le v2, v4, :cond_4

    .line 87
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v5

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageCache:Ljava/util/LinkedList;

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, v5, v2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->updateObject(Lcom/sec/samsung/gallery/glview/GlBaseObject;Landroid/graphics/Bitmap;)V

    .line 88
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v5

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageCache:Ljava/util/LinkedList;

    invoke-virtual {v2, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, v5, v2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->updateObject(Lcom/sec/samsung/gallery/glview/GlBaseObject;Landroid/graphics/Bitmap;)V

    .line 89
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v2

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    .line 90
    iget-wide v6, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mEffectDuration:J

    invoke-virtual {p0, v6, v7}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->setDuration(J)V

    .line 91
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->isPlayingVideoItem:Z

    move v2, v4

    .line 92
    goto :goto_0

    :cond_4
    move v2, v3

    .line 94
    goto/16 :goto_0
.end method

.method public isNextLoaded()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 129
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    if-eqz v3, :cond_0

    .line 130
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mCurrentIndex:I

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    .line 131
    .local v1, "mediaitem":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->load(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v3

    sput-object v3, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    .line 132
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideshowVideoPlay:Z

    if-eqz v3, :cond_1

    sget-object v3, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getVideoPlayEnable()Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v3

    if-nez v3, :cond_1

    .line 134
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->isPlayingVideoItem:Z

    if-eqz v3, :cond_1

    .line 150
    .end local v1    # "mediaitem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    :goto_0
    return v2

    .line 137
    .restart local v1    # "mediaitem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getSlideshowVideoView()Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getSlideshowVideoView()Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    move-result-object v3

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->misVideoPlaying:Z

    if-nez v3, :cond_0

    .line 139
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageIndex:I

    invoke-virtual {v3, v4, v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getItemImage(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 140
    .local v0, "bmp":Landroid/graphics/Bitmap;
    if-nez v0, :cond_3

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageIndex:I

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->isRequestInProgress(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 143
    :cond_3
    iget v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageIndex:I

    .line 144
    iget v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageIndex:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getCount()I

    move-result v3

    rem-int/2addr v2, v3

    iput v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageIndex:I

    .line 146
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->getMaxDisplayObjCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v2

    invoke-virtual {p0, v2, v0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->updateObject(Lcom/sec/samsung/gallery/glview/GlBaseObject;Landroid/graphics/Bitmap;)V

    .line 147
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->getMaxDisplayObjCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    .line 148
    const/4 v2, 0x1

    goto :goto_0
.end method

.method protected onCreate()V
    .locals 2

    .prologue
    .line 155
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->load(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v1

    sput-object v1, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    .line 156
    sget-object v1, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getImageIndex()I

    move-result v0

    .line 158
    .local v0, "lastIndex":I
    sget-boolean v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isSlideShowPaused:Z

    if-eqz v1, :cond_0

    .line 159
    iput v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mCurrentIndex:I

    .line 160
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isSlideShowPaused:Z

    .line 162
    :cond_0
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->isPlayingVideoItem:Z

    if-nez v1, :cond_1

    .line 163
    iget v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mCurrentIndex:I

    iput v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mImageIndex:I

    .line 164
    :cond_1
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->load(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    .line 169
    sget-object v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mCurrentIndex:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->setImageIndex(I)V

    .line 170
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->save(Landroid/content/Context;Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;)V

    .line 171
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->stopSoundScene()V

    .line 172
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->load(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    .line 192
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideshowVideoPlay:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getVideoPlayEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    iget v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mCurrentIndex:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->checkForVideoItem(I)V

    .line 194
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 176
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->onStop()V

    .line 178
    iget v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mCurrentIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mCurrentIndex:I

    .line 179
    iget v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mCurrentIndex:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getCount()I

    move-result v2

    rem-int/2addr v1, v2

    iput v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mCurrentIndex:I

    .line 181
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v0, v1, v3

    .line 182
    .local v0, "tempObject":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v2, v2, v4

    aput-object v2, v1, v3

    .line 183
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aput-object v0, v1, v4

    .line 184
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mIsRandom:Z

    if-eqz v1, :cond_0

    .line 185
    invoke-static {}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;->values()[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mRandom:Ljava/util/Random;

    invoke-static {}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;->values()[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;

    move-result-object v3

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mCurrentTransitionType:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;

    .line 188
    :cond_0
    return-void
.end method

.method protected onUpdate(F)V
    .locals 9
    .param p1, "ratio"    # F

    .prologue
    const/4 v7, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    .line 212
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v3

    .line 213
    .local v3, "mFromGlObject":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v4

    .line 215
    .local v4, "mToGlObject":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    const/4 v2, 0x0

    .line 216
    .local v2, "distance":F
    sget-object v5, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$3;->$SwitchMap$com$sec$samsung$gallery$glview$slideshoweffect$GlSlideshowTransitionShow$TransitionType:[I

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mCurrentTransitionType:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 218
    :pswitch_0
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->isPlayingVideoItem:Z

    if-nez v5, :cond_0

    .line 220
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mWidthSpace:F

    mul-float v2, p1, v5

    .line 221
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mIsRandom:Z

    if-eqz v5, :cond_1

    neg-float v0, v2

    .line 223
    .local v0, "deltaFromX":F
    :goto_1
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getY()F

    move-result v5

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getZ()F

    move-result v6

    invoke-virtual {v3, v0, v5, v6}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 225
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mIsRandom:Z

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mWidthSpace:F

    sub-float v1, v5, v2

    .line 227
    .local v1, "deltaToX":F
    :goto_2
    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getY()F

    move-result v5

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getZ()F

    move-result v6

    invoke-virtual {v4, v1, v5, v6}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 228
    invoke-virtual {v3, v8}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    .line 229
    invoke-virtual {v4, v8}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    goto :goto_0

    .line 221
    .end local v0    # "deltaFromX":F
    .end local v1    # "deltaToX":F
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mWidthSpace:F

    neg-float v5, v5

    invoke-virtual {p0, p1, v7, v5}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->getDelta_sinusoidalOut(FFF)F

    move-result v0

    goto :goto_1

    .line 225
    .restart local v0    # "deltaFromX":F
    :cond_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mWidthSpace:F

    invoke-virtual {p0, p1, v5, v7}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->getDelta_sinusoidalOut(FFF)F

    move-result v1

    goto :goto_2

    .line 232
    .end local v0    # "deltaFromX":F
    :pswitch_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mWidthSpace:F

    mul-float v2, p1, v5

    .line 233
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getY()F

    move-result v5

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getZ()F

    move-result v6

    invoke-virtual {v3, v2, v5, v6}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 234
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mWidthSpace:F

    neg-float v5, v5

    add-float/2addr v5, v2

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getY()F

    move-result v6

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getZ()F

    move-result v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 236
    invoke-virtual {v3, v8}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    .line 237
    invoke-virtual {v4, v8}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    goto :goto_0

    .line 240
    :pswitch_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mHeightSpace:F

    mul-float v2, p1, v5

    .line 241
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getX()F

    move-result v5

    neg-float v6, v2

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getZ()F

    move-result v7

    invoke-virtual {v3, v5, v6, v7}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 242
    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getX()F

    move-result v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mHeightSpace:F

    sub-float/2addr v6, v2

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getZ()F

    move-result v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 244
    invoke-virtual {v3, v8}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    .line 245
    invoke-virtual {v4, v8}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    goto/16 :goto_0

    .line 248
    :pswitch_3
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mHeightSpace:F

    mul-float v2, p1, v5

    .line 249
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getX()F

    move-result v5

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getZ()F

    move-result v6

    invoke-virtual {v3, v5, v2, v6}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 250
    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getX()F

    move-result v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mHeightSpace:F

    neg-float v6, v6

    add-float/2addr v6, v2

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getZ()F

    move-result v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 252
    invoke-virtual {v3, v8}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    .line 253
    invoke-virtual {v4, v8}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    goto/16 :goto_0

    .line 256
    :pswitch_4
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->isPlayingVideoItem:Z

    if-nez v5, :cond_0

    .line 258
    sub-float v5, v8, p1

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    .line 259
    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    goto/16 :goto_0

    .line 216
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setSlideShowInfo(ILcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;)V
    .locals 4
    .param p1, "effectDuration"    # I
    .param p2, "transitionType"    # Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;

    .prologue
    .line 266
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->setSlideShowInfo(ILcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;)V

    .line 267
    if-nez p2, :cond_0

    .line 268
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mRandom:Ljava/util/Random;

    .line 269
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mIsRandom:Z

    .line 270
    invoke-static {}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;->values()[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mRandom:Ljava/util/Random;

    invoke-static {}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;->values()[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;

    move-result-object v2

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mCurrentTransitionType:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;

    .line 275
    :goto_0
    return-void

    .line 273
    :cond_0
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow;->mCurrentTransitionType:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;

    goto :goto_0
.end method
