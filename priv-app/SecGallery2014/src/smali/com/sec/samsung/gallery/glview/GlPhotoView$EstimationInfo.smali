.class Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;
.super Ljava/lang/Object;
.source "GlPhotoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EstimationInfo"
.end annotation


# instance fields
.field firstX:F

.field firstY:F

.field firstZ:F

.field gapH:F

.field gapW:F

.field hCount:I

.field objH:F

.field objW:F

.field scroll:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 0

    .prologue
    .line 835
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/samsung/gallery/glview/GlPhotoView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/glview/GlPhotoView$1;

    .prologue
    .line 835
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    return-void
.end method


# virtual methods
.method public getObject(I)Lcom/sec/android/gallery3d/glcore/GlObject;
    .locals 7
    .param p1, "index"    # I

    .prologue
    .line 846
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->objW:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->objH:F

    invoke-direct {v1, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 848
    .local v1, "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->hCount:I

    rem-int v0, p1, v3

    .line 849
    .local v0, "col":I
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->hCount:I

    div-int v2, p1, v3

    .line 850
    .local v2, "row":I
    int-to-float v3, v0

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->gapW:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->firstX:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->firstY:F

    int-to-float v5, v2

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->gapH:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->scroll:F

    add-float/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->firstZ:F

    invoke-virtual {v1, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPos(FFF)V

    .line 851
    return-object v1
.end method

.method public saveState()V
    .locals 1

    .prologue
    .line 855
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    move-result-object v0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemW:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->objW:F

    .line 856
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    move-result-object v0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemH:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->objH:F

    .line 857
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    move-result-object v0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemGapW:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->gapW:F

    .line 858
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    move-result-object v0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemGapH:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->gapH:F

    .line 859
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    move-result-object v0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemStartX:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->firstX:F

    .line 860
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    move-result-object v0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemStartY:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->firstY:F

    .line 861
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    move-result-object v0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemStartZ:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->firstZ:F

    .line 862
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    move-result-object v0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemHVisibleCnt:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->hCount:I

    .line 863
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    move-result-object v0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScroll:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfo;->scroll:F

    .line 864
    return-void
.end method
