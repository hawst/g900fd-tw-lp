.class Lcom/sec/samsung/gallery/model/ThumbnailProxy$2;
.super Landroid/os/Handler;
.source "ThumbnailProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/model/ThumbnailProxy;-><init>(Ljava/lang/String;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/model/ThumbnailProxy;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 199
    iput-object p1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$2;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 202
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 216
    :cond_0
    :goto_0
    return-void

    .line 204
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$2;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mWindowContentChangedListener:Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$1000(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$2;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mIsActive:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$100(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$2;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mWindowContentChangedListener:Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$1000(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$2;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mSize:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$700(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;->onSizeChanged(I)V

    goto :goto_0

    .line 209
    :pswitch_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$2;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    iget v1, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->procBitmapReady(I)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$1500(Lcom/sec/samsung/gallery/model/ThumbnailProxy;I)V

    goto :goto_0

    .line 212
    :pswitch_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$2;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveRequestCount:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$1400(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)I

    move-result v0

    if-nez v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$2;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # invokes: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->requestNonactiveImages()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$1600(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)V

    goto :goto_0

    .line 202
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
