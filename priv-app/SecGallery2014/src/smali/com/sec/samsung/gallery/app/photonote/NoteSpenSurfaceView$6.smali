.class Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$6;
.super Ljava/lang/Object;
.source "NoteSpenSurfaceView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)V
    .locals 0

    .prologue
    .line 578
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$6;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCommit(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 2
    .param p1, "pageDoc"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .prologue
    .line 581
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$6;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mActionBar:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$200(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isUndoable()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setUndoable(Z)V

    .line 582
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$6;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mActionBar:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$200(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isRedoable()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setRedoable(Z)V

    .line 583
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$6;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mActionBar:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$200(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isUndoable()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->updateButtonState(Z)V

    .line 584
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$6;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mViewActionListener:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;

    if-eqz v0, :cond_0

    .line 585
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$6;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mViewActionListener:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isChanged()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;->setDataChanged(Z)V

    .line 587
    :cond_0
    return-void
.end method

.method public onRedoable(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)V
    .locals 0
    .param p1, "arg0"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "arg1"    # Z

    .prologue
    .line 591
    return-void
.end method

.method public onUndoable(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)V
    .locals 0
    .param p1, "arg0"    # Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .param p2, "arg1"    # Z

    .prologue
    .line 595
    return-void
.end method
