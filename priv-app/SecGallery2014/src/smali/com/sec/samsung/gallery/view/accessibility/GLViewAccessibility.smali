.class public Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;
.super Ljava/lang/Object;
.source "GLViewAccessibility.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;
    }
.end annotation


# instance fields
.field private mAccessibilityNodeInfoListener:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    return-void
.end method


# virtual methods
.method public createAccessibilityNodeInfo(ILandroid/view/accessibility/AccessibilityNodeInfo;)Landroid/view/accessibility/AccessibilityNodeInfo;
    .locals 4
    .param p1, "virtualDescendantId"    # I
    .param p2, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    const/16 v3, 0x2000

    const/16 v2, 0x1000

    const/4 v1, 0x1

    .line 25
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;->mAccessibilityNodeInfoListener:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;->mAccessibilityNodeInfoListener:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;->hasAccessibilityChildren()Z

    move-result v0

    if-nez v0, :cond_2

    .line 26
    :cond_0
    const/4 p2, 0x0

    .line 46
    .end local p2    # "info":Landroid/view/accessibility/AccessibilityNodeInfo;
    :cond_1
    :goto_0
    return-object p2

    .line 28
    .restart local p2    # "info":Landroid/view/accessibility/AccessibilityNodeInfo;
    :cond_2
    const/4 v0, -0x1

    if-ne p1, v0, :cond_3

    .line 29
    invoke-virtual {p2, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setScrollable(Z)V

    .line 30
    invoke-virtual {p2, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setEnabled(Z)V

    .line 31
    invoke-virtual {p2, v3}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 32
    invoke-virtual {p2, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 33
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;->mAccessibilityNodeInfoListener:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;

    invoke-interface {v0, p2}, Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;->update(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    goto :goto_0

    .line 34
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;->mAccessibilityNodeInfoListener:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;

    invoke-interface {v0, p1}, Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;->isReqeustHandlable(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 35
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;->mAccessibilityNodeInfoListener:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;

    invoke-interface {v0, p2, p1}, Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;->setupAccInfo(Landroid/view/accessibility/AccessibilityNodeInfo;I)V

    .line 36
    invoke-virtual {p2, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setVisibleToUser(Z)V

    .line 37
    const/16 v0, 0x40

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 38
    const/16 v0, 0x80

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 39
    const/16 v0, 0x10

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 40
    invoke-virtual {p2, v3}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 41
    invoke-virtual {p2, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 42
    const-string v0, ""

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setText(Ljava/lang/CharSequence;)V

    .line 43
    invoke-virtual {p2, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setFocused(Z)V

    .line 44
    invoke-virtual {p2, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setAccessibilityFocused(Z)V

    goto :goto_0
.end method

.method public performAction(IILandroid/os/Bundle;)Z
    .locals 1
    .param p1, "virtualViewId"    # I
    .param p2, "action"    # I
    .param p3, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;->mAccessibilityNodeInfoListener:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;

    if-nez v0, :cond_0

    .line 51
    const/4 v0, 0x0

    .line 52
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;->mAccessibilityNodeInfoListener:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;->performAction(IILandroid/os/Bundle;)Z

    move-result v0

    goto :goto_0
.end method

.method public setAccessibilityNodeInfoListener(Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility;->mAccessibilityNodeInfoListener:Lcom/sec/samsung/gallery/view/accessibility/GLViewAccessibility$AccessibilityNodeInfoListener;

    .line 22
    return-void
.end method
