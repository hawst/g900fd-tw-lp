.class Lcom/sec/samsung/gallery/view/timeview/TimeViewState$SelectionTask;
.super Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;
.source "TimeViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/timeview/TimeViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SelectionTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "onProgressListener"    # Lcom/sec/android/gallery3d/data/OnProgressListener;
    .param p4, "maxCount"    # I

    .prologue
    .line 3027
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$SelectionTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .line 3028
    invoke-direct {p0, p2, p3, p4}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;I)V

    .line 3029
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 3026
    check-cast p1, [Lcom/sec/android/gallery3d/data/MediaSet;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$SelectionTask;->doInBackground([Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/Void;
    .locals 4
    .param p1, "arg0"    # [Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 3033
    const/4 v1, 0x0

    aget-object v0, p1, v1

    .line 3034
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$SelectionTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$SelectionTask;->mContext:Landroid/content/Context;

    const/4 v3, -0x1

    # invokes: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAllProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;ILcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;)V
    invoke-static {v1, v2, v0, v3, p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$7600(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;ILcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;)V

    .line 3035
    const/4 v1, 0x0

    return-object v1
.end method
