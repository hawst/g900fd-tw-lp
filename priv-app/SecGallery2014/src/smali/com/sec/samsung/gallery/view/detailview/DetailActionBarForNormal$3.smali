.class Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal$3;
.super Ljava/lang/Object;
.source "DetailActionBarForNormal.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->setTitle(Ljava/lang/String;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;

.field final synthetic val$currentPosition:I

.field final synthetic val$itemName:Ljava/lang/String;

.field final synthetic val$numberOfItemsInAlbum:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;ILjava/lang/String;I)V
    .locals 0

    .prologue
    .line 998
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;

    iput p2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal$3;->val$numberOfItemsInAlbum:I

    iput-object p3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal$3;->val$itemName:Ljava/lang/String;

    iput p4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal$3;->val$currentPosition:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 1001
    const-string v0, ""

    .line 1002
    .local v0, "photoLabelText":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->access$200(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    const v4, 0x7f0f0014

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1004
    .local v2, "t":Landroid/widget/TextView;
    iget v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal$3;->val$numberOfItemsInAlbum:I

    if-lez v3, :cond_0

    .line 1005
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal$3;->val$itemName:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_2

    .line 1006
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal$3;->val$currentPosition:I

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal$3;->val$numberOfItemsInAlbum:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1015
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 1016
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1017
    :cond_1
    return-void

    .line 1008
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->access$300(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1009
    .local v1, "res":Landroid/content/res/Resources;
    const v3, 0x7f0e01c1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal$3;->val$itemName:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal$3;->val$currentPosition:I

    add-int/lit8 v6, v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal$3;->val$numberOfItemsInAlbum:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
