.class Lcom/sec/samsung/gallery/glview/GlThumbObject$4;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlThumbObject.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlThumbObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mDimStart:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V
    .locals 0

    .prologue
    .line 291
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$4;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 4
    .param p1, "ratio"    # F

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 295
    sub-float v0, v3, p1

    sub-float v1, v3, p1

    mul-float/2addr v0, v1

    sub-float p1, v3, v0

    .line 296
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$4;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$4;->mDimStart:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$4;->mDimStart:F

    sub-float v2, v3, v2

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setDimEx(F)V

    .line 297
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$4;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHndDim:I

    if-nez v0, :cond_0

    .line 301
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$4;->mDimStart:F

    .line 304
    :goto_0
    return-void

    .line 303
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$4;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$4;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHndDim:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getDim(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$4;->mDimStart:F

    goto :goto_0
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$4;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 308
    return-void
.end method
