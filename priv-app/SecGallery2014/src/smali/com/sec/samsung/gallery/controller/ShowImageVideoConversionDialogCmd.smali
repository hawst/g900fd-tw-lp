.class public Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "ShowImageVideoConversionDialogCmd.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAlertDialog:Landroid/app/AlertDialog;

.field private mContext:Landroid/content/Context;

.field type:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->showShareDialog()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->dismissDialog()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->convertMultiFormatToMP4()V

    return-void
.end method

.method private convertMultiFormatToMP4()V
    .locals 24

    .prologue
    .line 108
    new-instance v6, Ljava/io/File;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/RW_LIB"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 109
    .local v6, "folder":Ljava/io/File;
    const/16 v19, 0x1

    .line 110
    .local v19, "success":Z
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v20

    if-nez v20, :cond_0

    .line 111
    invoke-virtual {v6}, Ljava/io/File;->mkdir()Z

    move-result v19

    .line 113
    :cond_0
    if-nez v19, :cond_1

    .line 114
    sget-object v20, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->TAG:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Can not create RW_LIB folder in "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    check-cast v20, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v18

    .line 117
    .local v18, "selectionProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    check-cast v20, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v5

    .line 118
    .local v5, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 120
    .local v12, "mediaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    instance-of v0, v5, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    move/from16 v20, v0

    if-nez v20, :cond_2

    instance-of v0, v5, Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    move/from16 v20, v0

    if-eqz v20, :cond_7

    .line 121
    :cond_2
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v16

    .line 122
    .local v16, "selectedItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_b

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 124
    .local v15, "selectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v15, :cond_3

    .line 128
    instance-of v0, v15, Lcom/sec/android/gallery3d/data/MediaSet;

    move/from16 v20, v0

    if-eqz v20, :cond_3

    move-object/from16 v17, v15

    .line 129
    check-cast v17, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 130
    .local v17, "selectedSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v4

    .line 131
    .local v4, "count":I
    const/16 v20, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v10

    .line 133
    .local v10, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 134
    .local v11, "media":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v0, v11, Lcom/sec/android/gallery3d/data/MediaItem;

    move/from16 v20, v0

    if-eqz v20, :cond_5

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v20

    if-nez v20, :cond_6

    :cond_5
    instance-of v0, v11, Lcom/sec/android/gallery3d/data/LocalImage;

    move/from16 v20, v0

    if-eqz v20, :cond_4

    move-object/from16 v20, v11

    check-cast v20, Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/16 v22, 0x10

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/LocalImage;->hasAttribute(J)Z

    move-result v20

    if-nez v20, :cond_6

    move-object/from16 v20, v11

    check-cast v20, Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/16 v22, 0x400

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/LocalImage;->hasAttribute(J)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 137
    :cond_6
    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 142
    .end local v4    # "count":I
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v11    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v15    # "selectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v16    # "selectedItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    .end local v17    # "selectedSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_7
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v14

    .line 143
    .local v14, "selectedCount":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    if-ge v7, v14, :cond_b

    .line 144
    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v11

    .line 145
    .local v11, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v0, v11, Lcom/sec/android/gallery3d/data/MediaItem;

    move/from16 v20, v0

    if-eqz v20, :cond_8

    move-object/from16 v20, v11

    check-cast v20, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v20

    if-nez v20, :cond_9

    :cond_8
    instance-of v0, v11, Lcom/sec/android/gallery3d/data/LocalImage;

    move/from16 v20, v0

    if-eqz v20, :cond_a

    move-object/from16 v20, v11

    check-cast v20, Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/16 v22, 0x10

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/LocalImage;->hasAttribute(J)Z

    move-result v20

    if-nez v20, :cond_9

    move-object/from16 v20, v11

    check-cast v20, Lcom/sec/android/gallery3d/data/LocalImage;

    const-wide/16 v22, 0x400

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/LocalImage;->hasAttribute(J)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 148
    :cond_9
    check-cast v11, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local v11    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    :cond_a
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 153
    .end local v7    # "i":I
    .end local v14    # "selectedCount":I
    :cond_b
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v20

    if-lez v20, :cond_c

    .line 154
    const/16 v20, 0x3

    move/from16 v0, v20

    new-array v13, v0, [Ljava/lang/Object;

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    aput-object v21, v13, v20

    const/16 v20, 0x1

    aput-object v12, v13, v20

    const/16 v20, 0x2

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->type:Z

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v21

    aput-object v21, v13, v20

    .line 157
    .local v13, "params":[Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    check-cast v20, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static/range {v20 .. v20}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v20

    const-string v21, "CONVERT_MULTI_FORMAT_TO_MP4"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v13}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 162
    .end local v13    # "params":[Ljava/lang/Object;
    :goto_2
    return-void

    .line 160
    :cond_c
    sget-object v20, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->TAG:Ljava/lang/String;

    const-string v21, "mediaList size is 0"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private dismissDialog()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 91
    :cond_0
    return-void
.end method

.method private showDialog()V
    .locals 7

    .prologue
    .line 52
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e02e8

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 53
    .local v1, "as_video_file":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e02e9

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 54
    .local v0, "as_image_file":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0047

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 55
    .local v4, "title":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0046

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 56
    .local v2, "cancel":Ljava/lang/String;
    const/4 v5, 0x2

    new-array v3, v5, [Ljava/lang/CharSequence;

    const/4 v5, 0x0

    aput-object v0, v3, v5

    const/4 v5, 0x1

    aput-object v1, v3, v5

    .line 60
    .local v3, "items":[Ljava/lang/CharSequence;
    new-instance v5, Landroid/app/AlertDialog$Builder;

    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    new-instance v6, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd$2;

    invoke-direct {v6, p0}, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd$2;-><init>(Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;)V

    invoke-virtual {v5, v2, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    new-instance v6, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd$1;

    invoke-direct {v6, p0}, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;)V

    invoke-virtual {v5, v3, v6}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->mAlertDialog:Landroid/app/AlertDialog;

    .line 86
    return-void
.end method

.method private showShareDialog()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 94
    const/4 v1, 0x1

    .line 95
    .local v1, "show":Z
    const/4 v2, 0x3

    new-array v0, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->mContext:Landroid/content/Context;

    aput-object v2, v0, v4

    const/4 v2, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v0, v2

    .line 98
    .local v0, "params":[Ljava/lang/Object;
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->type:Z

    if-eqz v2, :cond_0

    .line 99
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "LAST_SHARE_APP"

    invoke-virtual {v2, v3, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 105
    :goto_0
    return-void

    .line 102
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "SHOW_SHARE_DIALOG"

    invoke-virtual {v2, v3, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 3
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 41
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Object;

    move-object v0, v2

    check-cast v0, [Ljava/lang/Object;

    .line 42
    .local v0, "params":[Ljava/lang/Object;
    const/4 v2, 0x0

    aget-object v2, v0, v2

    check-cast v2, Landroid/content/Context;

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->mContext:Landroid/content/Context;

    .line 43
    const/4 v2, 0x1

    aget-object v2, v0, v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 44
    .local v1, "show":Z
    const/4 v2, 0x2

    aget-object v2, v0, v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->type:Z

    .line 46
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->dismissDialog()V

    .line 47
    if-eqz v1, :cond_0

    .line 48
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->showDialog()V

    .line 49
    :cond_0
    return-void
.end method
