.class Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd$1;
.super Landroid/content/BroadcastReceiver;
.source "SecretModeReceiverCmd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 34
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecretModeOn()Z

    move-result v1

    .line 35
    .local v1, "secretMode":Z
    const-string v2, "com.samsung.android.intent.action.PRIVATE_MODE_ON"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 36
    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setSecretModeOn(Z)V

    .line 40
    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecretModeOn()Z

    move-result v2

    if-eq v1, v2, :cond_2

    .line 41
    new-array v0, v5, [Ljava/lang/Object;

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecretModeOn()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v4

    .local v0, "param":[Ljava/lang/Object;
    move-object v2, p1

    .line 44
    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "SECRET_MODE_CHANGED"

    invoke-virtual {v2, v3, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 47
    const-string v2, "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 48
    sget-object v2, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateTasks:Ljava/util/HashMap;

    if-eqz v2, :cond_1

    sget-object v2, Lcom/sec/samsung/gallery/util/SecretboxOperations;->mOnSecretboxListener:Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;

    if-eqz v2, :cond_1

    .line 49
    # getter for: Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "mOnSecretboxListener cancelOperation"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    sget-object v2, Lcom/sec/samsung/gallery/util/SecretboxOperations;->mOnSecretboxListener:Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;

    invoke-interface {v2}, Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;->cancelOperation()V

    .line 53
    :cond_1
    sget-object v2, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mPrivateModeChangeListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$PrivateModeChangeListener;

    if-eqz v2, :cond_2

    .line 54
    sget-object v2, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mPrivateModeChangeListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$PrivateModeChangeListener;

    invoke-interface {v2}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$PrivateModeChangeListener;->onPrivateModeChanged()V

    .line 58
    .end local v0    # "param":[Ljava/lang/Object;
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->isPhotoPage()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 59
    check-cast p1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->requestUpdateScreenByCategoryChange(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 61
    :cond_3
    return-void

    .line 37
    .restart local p1    # "context":Landroid/content/Context;
    :cond_4
    const-string v2, "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 38
    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setSecretModeOn(Z)V

    goto :goto_0
.end method
