.class public Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "ShowRenameDialogCmd.java"

# interfaces
.implements Ljava/util/Observer;
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mRenameDialog:Lcom/sec/samsung/gallery/view/common/RenameDialog;

.field private mSelectedItem:Lcom/sec/android/gallery3d/data/MediaObject;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method private dismissDialog()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;->mRenameDialog:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;->mRenameDialog:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->dismissMediaRenameDialog()V

    .line 82
    :cond_0
    return-void
.end method

.method private showDialog()V
    .locals 3

    .prologue
    .line 59
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;->mSelectedItem:Lcom/sec/android/gallery3d/data/MediaObject;

    if-nez v1, :cond_0

    .line 60
    sget-object v1, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;->TAG:Ljava/lang/String;

    const-string v2, "no selected item exists! hide dialog"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    :goto_0
    return-void

    .line 64
    :cond_0
    new-instance v1, Lcom/sec/samsung/gallery/view/common/RenameDialog;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/common/RenameDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;->mRenameDialog:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    .line 65
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;->mRenameDialog:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    invoke-virtual {v1, p0}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->addObserver(Ljava/util/Observer;)V

    .line 66
    const/4 v0, 0x0

    .line 68
    .local v0, "nameOfSelectedItem":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;->mSelectedItem:Lcom/sec/android/gallery3d/data/MediaObject;

    instance-of v1, v1, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v1, :cond_1

    .line 69
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;->mSelectedItem:Lcom/sec/android/gallery3d/data/MediaObject;

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v0

    .line 73
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;->mRenameDialog:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->setCurrentName(Ljava/lang/String;)V

    .line 74
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;->mRenameDialog:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;->mSelectedItem:Lcom/sec/android/gallery3d/data/MediaObject;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->setCurrentItem(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 75
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;->mRenameDialog:Lcom/sec/samsung/gallery/view/common/RenameDialog;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->showMediaRenameDialog()V

    goto :goto_0

    .line 71
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;->mSelectedItem:Lcom/sec/android/gallery3d/data/MediaObject;

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 3
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 32
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Object;

    move-object v0, v2

    check-cast v0, [Ljava/lang/Object;

    .line 33
    .local v0, "params":[Ljava/lang/Object;
    const/4 v2, 0x0

    aget-object v2, v0, v2

    check-cast v2, Landroid/content/Context;

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;->mContext:Landroid/content/Context;

    .line 34
    const/4 v2, 0x1

    aget-object v2, v0, v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaObject;

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;->mSelectedItem:Lcom/sec/android/gallery3d/data/MediaObject;

    .line 35
    const/4 v2, 0x2

    aget-object v2, v0, v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 37
    .local v1, "show":Z
    if-eqz v1, :cond_0

    .line 38
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;->showDialog()V

    .line 42
    :goto_0
    return-void

    .line 40
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;->dismissDialog()V

    goto :goto_0
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 5
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 46
    move-object v0, p2

    check-cast v0, Lcom/sec/samsung/gallery/core/Event;

    .line 48
    .local v0, "ev":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v3

    sget v4, Lcom/sec/samsung/gallery/core/Event;->EVENT_RENAME_MEDIA:I

    if-ne v3, v4, :cond_0

    .line 49
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 50
    .local v1, "newName":Ljava/lang/String;
    const/4 v3, 0x2

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;->mContext:Landroid/content/Context;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v1, v2, v3

    .line 53
    .local v2, "params":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v3

    const-string v4, "RENAME_ALBUM_OR_FILE"

    invoke-virtual {v3, v4, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 55
    .end local v1    # "newName":Ljava/lang/String;
    .end local v2    # "params":[Ljava/lang/Object;
    :cond_0
    return-void
.end method
