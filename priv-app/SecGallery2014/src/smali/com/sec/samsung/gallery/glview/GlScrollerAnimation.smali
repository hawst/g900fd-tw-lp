.class public Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlScrollerAnimation.java"


# static fields
.field public static final DEF_DURATION:J = 0x1f4L

.field public static final MODE_TRANSLATE_SCROLL:I = 0x1

.field public static final MODE_TRANSLATE_SCROLL_WITH_FADEIN:I = 0x2

.field public static final MODE_TRANSLATE_START_POS:I


# instance fields
.field private mFromScroll:F

.field private mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mItemSourceStartY:F

.field private mItemTargetStartY:F

.field private mMode:I

.field private mScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

.field private mToScroll:F


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/samsung/gallery/glview/GlScroller;)V
    .locals 2
    .param p1, "root"    # Lcom/sec/android/gallery3d/glcore/GlRootView;
    .param p2, "scroller"    # Lcom/sec/samsung/gallery/glview/GlScroller;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 26
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    .line 27
    const-wide/16 v0, 0x1f4

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->setDuration(J)V

    .line 28
    new-instance v0, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;-><init>()V

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->setInterpolator(Lcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    .line 29
    return-void
.end method

.method private startAnimationInter(J)V
    .locals 1
    .param p1, "delay"    # J

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 109
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->startAfter(J)V

    .line 110
    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 8
    .param p1, "ratio"    # F

    .prologue
    .line 33
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-nez v5, :cond_1

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 38
    :cond_1
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mMode:I

    if-nez v5, :cond_2

    .line 39
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mItemSourceStartY:F

    .line 40
    .local v2, "sy":F
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mItemTargetStartY:F

    .line 41
    .local v4, "ty":F
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    sub-float v6, v4, v2

    mul-float/2addr v6, p1

    add-float/2addr v6, v2

    iput v6, v5, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemStartY:F

    .line 43
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v5, v5

    if-ge v0, v5, :cond_3

    .line 44
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v6, v6, v0

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/glview/GlScroller;->setThumbPosition(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 43
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 46
    .end local v0    # "i":I
    .end local v2    # "sy":F
    .end local v4    # "ty":F
    :cond_2
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mMode:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_4

    .line 47
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mFromScroll:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mToScroll:F

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mFromScroll:F

    sub-float/2addr v6, v7

    mul-float/2addr v6, p1

    add-float v3, v5, v6

    .line 49
    .local v3, "targetScroll":F
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v5, v3}, Lcom/sec/samsung/gallery/glview/GlScroller;->setScroll(F)V

    .line 67
    .end local v3    # "targetScroll":F
    :cond_3
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v5, p1}, Lcom/sec/samsung/gallery/glview/GlScroller;->onAnimation(F)V

    goto :goto_0

    .line 50
    :cond_4
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mMode:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_3

    .line 51
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mFromScroll:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mToScroll:F

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mFromScroll:F

    sub-float/2addr v6, v7

    mul-float/2addr v6, p1

    add-float v3, v5, v6

    .line 53
    .restart local v3    # "targetScroll":F
    const/4 v5, 0x0

    cmpl-float v5, v3, v5

    if-eqz v5, :cond_5

    .line 54
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v5, v3}, Lcom/sec/samsung/gallery/glview/GlScroller;->setScroll(F)V

    .line 56
    :cond_5
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget-object v1, v5, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 57
    .local v1, "mObjects":[Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-eqz v1, :cond_0

    .line 60
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    array-length v5, v1

    if-ge v0, v5, :cond_3

    .line 61
    aget-object v5, v1, v0

    if-nez v5, :cond_6

    .line 60
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 64
    :cond_6
    aget-object v5, v1, v0

    invoke-virtual {v5, p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlpha(F)V

    goto :goto_3
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 73
    return-void
.end method

.method public startScrollerAnimation(F)V
    .locals 1
    .param p1, "y"    # F

    .prologue
    .line 76
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->startScrollerAnimation(FZ)V

    .line 77
    return-void
.end method

.method public startScrollerAnimation(FZ)V
    .locals 2
    .param p1, "y"    # F
    .param p2, "isDelta"    # Z

    .prologue
    .line 80
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mMode:I

    .line 81
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemStartY:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mItemSourceStartY:F

    .line 82
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemStartY:F

    add-float/2addr p1, v0

    .end local p1    # "y":F
    :cond_0
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mItemTargetStartY:F

    .line 83
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->startAnimationInter(J)V

    .line 84
    return-void
.end method

.method public startTranlateScrollAnimation(FF)V
    .locals 2
    .param p1, "from"    # F
    .param p2, "to"    # F

    .prologue
    .line 87
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->startTranlateScrollAnimation(FFJ)V

    .line 88
    return-void
.end method

.method public startTranlateScrollAnimation(FFJ)V
    .locals 1
    .param p1, "from"    # F
    .param p2, "to"    # F
    .param p3, "delay"    # J

    .prologue
    .line 91
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mMode:I

    .line 92
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mFromScroll:F

    .line 93
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mToScroll:F

    .line 94
    invoke-direct {p0, p3, p4}, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->startAnimationInter(J)V

    .line 95
    return-void
.end method

.method public startTranlateScrollAnimation(FFJZ)V
    .locals 1
    .param p1, "from"    # F
    .param p2, "to"    # F
    .param p3, "delay"    # J
    .param p5, "isAlpha"    # Z

    .prologue
    .line 98
    if-eqz p5, :cond_0

    .line 99
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mMode:I

    .line 102
    :goto_0
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mFromScroll:F

    .line 103
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mToScroll:F

    .line 104
    invoke-direct {p0, p3, p4}, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->startAnimationInter(J)V

    .line 105
    return-void

    .line 101
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->mMode:I

    goto :goto_0
.end method
