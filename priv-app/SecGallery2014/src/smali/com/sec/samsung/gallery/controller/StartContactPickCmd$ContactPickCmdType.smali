.class public final enum Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;
.super Ljava/lang/Enum;
.source "StartContactPickCmd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/StartContactPickCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ContactPickCmdType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

.field public static final enum INCLUDE_PROFILE:Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

.field public static final enum NOT_INCLUDE_PROFILE:Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    const-string v1, "INCLUDE_PROFILE"

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;->INCLUDE_PROFILE:Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    new-instance v0, Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    const-string v1, "NOT_INCLUDE_PROFILE"

    invoke-direct {v0, v1, v3}, Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;->NOT_INCLUDE_PROFILE:Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    .line 23
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    sget-object v1, Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;->INCLUDE_PROFILE:Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;->NOT_INCLUDE_PROFILE:Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;->$VALUES:[Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    return-object v0
.end method

.method public static values()[Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;->$VALUES:[Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    invoke-virtual {v0}, [Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    return-object v0
.end method
