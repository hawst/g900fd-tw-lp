.class public Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;
.super Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;
.source "MediaItemAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$1;,
        Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;
    }
.end annotation


# static fields
.field public static final ATTR_ENABLE_FRAME:J = 0x2L

.field public static final ATTR_MULTISHOT_GROUP:J = 0x1L

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAttr:J

.field private mConfig:Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

.field protected final mContext:Landroid/content/Context;

.field private mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

.field private mDisplayBrandicon:Z

.field private mDisplayMediaIcon:Z

.field private mFilterMimeType:Ljava/lang/String;

.field private mForceDisplaySourceIcon:Z

.field private mGroupCounts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mGroupIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mIconLeft:I

.field private mIsChangedSource:Z

.field private mIsConnectedDevice:Z

.field private mIsGroupCameraSet:Z

.field private mIsPhotoFrameSet:Z

.field private mItemIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected mMediaItemList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field protected mNewAlbumHeader:Z

.field private mRemovedCount:I

.field private mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mSetPath:Ljava/lang/String;

.field private mTotalRemovedCount:I

.field private mUngroupAllBurstImages:Z

.field private mUngroupIdSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;IJ)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "setPath"    # Ljava/lang/String;
    .param p3, "index"    # I
    .param p4, "attr"    # J

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 86
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;-><init>(Landroid/content/Context;)V

    .line 59
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    .line 61
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mForceDisplaySourceIcon:Z

    .line 63
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mAttr:J

    .line 64
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaItemList:Ljava/util/List;

    .line 67
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mNewAlbumHeader:Z

    .line 69
    iput v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    .line 70
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIsChangedSource:Z

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mDisplayBrandicon:Z

    .line 72
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mDisplayMediaIcon:Z

    .line 73
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mConfig:Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    .line 74
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mFilterMimeType:Ljava/lang/String;

    .line 75
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mUngroupIdSet:Ljava/util/Set;

    .line 76
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mUngroupAllBurstImages:Z

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mItemIds:Ljava/util/ArrayList;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupIds:Ljava/util/ArrayList;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupCounts:Ljava/util/ArrayList;

    .line 81
    iput v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mRemovedCount:I

    .line 82
    iput v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mTotalRemovedCount:I

    .line 83
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIsConnectedDevice:Z

    .line 87
    if-nez p2, :cond_0

    move-object v0, p1

    .line 88
    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/view/utils/DataPath;->getOneAlbumPath(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Ljava/lang/String;

    move-result-object p2

    .line 90
    :cond_0
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSetPath:Ljava/lang/String;

    .line 91
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    .line 92
    iput-wide p4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mAttr:J

    .line 93
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    .line 94
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSetPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 95
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 96
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mConfig:Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    .line 97
    return-void
.end method

.method private GetUpdateInfo(J)Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;
    .locals 9
    .param p1, "version"    # J

    .prologue
    const/4 v5, 0x0

    const/16 v8, 0x40

    .line 1065
    new-instance v2, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;

    invoke-direct {v2, v5}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;-><init>(Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$1;)V

    .line 1067
    .local v2, "info":Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;
    iget-wide v6, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSourceVersion:J

    iput-wide v6, v2, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->version:J

    .line 1068
    iget v6, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSize:I

    iput v6, v2, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->size:I

    .line 1069
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSetVersion:[J

    .line 1070
    .local v4, "setVersion":[J
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mVisibleStart:I

    .local v0, "i":I
    iget v6, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mVisibleEnd:I

    iget v7, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSize:I

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v3

    .local v3, "n":I
    :goto_0
    if-ge v0, v3, :cond_2

    .line 1071
    rem-int/lit16 v1, v0, 0x3e8

    .line 1072
    .local v1, "index":I
    aget-wide v6, v4, v1

    cmp-long v6, v6, p1

    if-eqz v6, :cond_1

    .line 1073
    iput v0, v2, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadStart:I

    .line 1074
    sub-int v5, v3, v0

    invoke-static {v8, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    iput v5, v2, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadCount:I

    .line 1118
    .end local v1    # "index":I
    .end local v2    # "info":Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;
    :cond_0
    :goto_1
    return-object v2

    .line 1070
    .restart local v1    # "index":I
    .restart local v2    # "info":Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1079
    .end local v1    # "index":I
    :cond_2
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mLoadContentData:Z

    if-nez v6, :cond_3

    iget v6, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mVisibleEnd:I

    if-eqz v6, :cond_3

    move-object v2, v5

    .line 1080
    goto :goto_1

    .line 1083
    :cond_3
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mThumbnailActiveStart:I

    iget v6, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mVisibleStart:I

    iget v7, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSize:I

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v3

    :goto_2
    if-ge v0, v3, :cond_5

    .line 1084
    rem-int/lit16 v1, v0, 0x3e8

    .line 1085
    .restart local v1    # "index":I
    aget-wide v6, v4, v1

    cmp-long v6, v6, p1

    if-eqz v6, :cond_4

    .line 1086
    iput v0, v2, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadStart:I

    .line 1087
    sub-int v5, v3, v0

    invoke-static {v8, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    iput v5, v2, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadCount:I

    goto :goto_1

    .line 1083
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1092
    .end local v1    # "index":I
    :cond_5
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mVisibleEnd:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mThumbnailActiveEnd:I

    :goto_3
    if-ge v0, v3, :cond_7

    .line 1093
    rem-int/lit16 v1, v0, 0x3e8

    .line 1094
    .restart local v1    # "index":I
    aget-wide v6, v4, v1

    cmp-long v6, v6, p1

    if-eqz v6, :cond_6

    .line 1095
    iput v0, v2, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadStart:I

    .line 1096
    sub-int v5, v3, v0

    invoke-static {v8, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    iput v5, v2, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadCount:I

    goto :goto_1

    .line 1092
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1101
    .end local v1    # "index":I
    :cond_7
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContentStart:I

    iget v6, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mThumbnailActiveStart:I

    iget v7, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSize:I

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v3

    :goto_4
    if-ge v0, v3, :cond_9

    .line 1102
    rem-int/lit16 v1, v0, 0x3e8

    .line 1103
    .restart local v1    # "index":I
    aget-wide v6, v4, v1

    cmp-long v6, v6, p1

    if-eqz v6, :cond_8

    .line 1104
    iput v0, v2, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadStart:I

    .line 1105
    sub-int v5, v3, v0

    invoke-static {v8, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    iput v5, v2, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadCount:I

    goto :goto_1

    .line 1101
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1110
    .end local v1    # "index":I
    :cond_9
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mThumbnailActiveEnd:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContentEnd:I

    :goto_5
    if-ge v0, v3, :cond_b

    .line 1111
    rem-int/lit16 v1, v0, 0x3e8

    .line 1112
    .restart local v1    # "index":I
    aget-wide v6, v4, v1

    cmp-long v6, v6, p1

    if-eqz v6, :cond_a

    .line 1113
    iput v0, v2, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadStart:I

    .line 1114
    sub-int v5, v3, v0

    invoke-static {v8, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    iput v5, v2, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadCount:I

    goto/16 :goto_1

    .line 1110
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 1118
    .end local v1    # "index":I
    :cond_b
    iget-wide v6, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSourceVersion:J

    cmp-long v6, v6, p1

    if-nez v6, :cond_0

    move-object v2, v5

    goto/16 :goto_1
.end method

.method private draw3DImageIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 8
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/16 v7, 0x1b

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 572
    invoke-virtual {p1, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 574
    .local v0, "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_1

    const-wide/16 v4, 0x2

    invoke-virtual {p2, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 575
    if-nez v0, :cond_0

    .line 576
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0200fa

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 577
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 578
    .restart local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 579
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 580
    const/4 v4, 0x3

    invoke-virtual {v0, v2, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 581
    invoke-virtual {p1, v0, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 583
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0, v4, v3, v3, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 584
    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    iput v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    .line 585
    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 590
    :goto_0
    return v2

    .line 588
    :cond_1
    if-eqz v0, :cond_2

    .line 589
    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    :cond_2
    move v2, v3

    .line 590
    goto :goto_0
.end method

.method private draw3DPanoramaIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 8
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/16 v7, 0x17

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 624
    if-nez p2, :cond_1

    .line 644
    :cond_0
    :goto_0
    return v2

    .line 626
    :cond_1
    invoke-virtual {p1, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 627
    .local v0, "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-static {p2}, Lcom/sec/samsung/gallery/util/Panorama3DUtil;->is3DPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->is3DPanorama()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 628
    :cond_2
    if-nez v0, :cond_3

    .line 629
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0200fb

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 630
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 631
    .restart local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 632
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 633
    const/4 v4, 0x3

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 634
    invoke-virtual {p1, v0, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 636
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_3
    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    add-int/lit8 v4, v4, 0x5

    iput v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    .line 637
    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0, v4, v2, v2, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 638
    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    iput v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    .line 639
    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    move v2, v3

    .line 640
    goto :goto_0

    .line 642
    :cond_4
    if-eqz v0, :cond_0

    .line 643
    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private draw3DTourIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 8
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/16 v7, 0x2a

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 838
    invoke-virtual {p1, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 840
    .local v0, "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_2

    const-wide/16 v2, 0x400

    invoke-virtual {p2, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 841
    if-nez v0, :cond_0

    .line 842
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200fc

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 843
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 844
    .restart local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 845
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 846
    const/4 v2, 0x3

    invoke-virtual {v0, v6, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 847
    invoke-virtual {p1, v0, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 849
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0, v2, v5, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 850
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    .line 851
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 856
    :cond_1
    :goto_0
    return-void

    .line 853
    :cond_2
    if-eqz v0, :cond_1

    .line 854
    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private drawBestFaceIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 8
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/16 v7, 0x26

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 754
    invoke-virtual {p1, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 756
    .local v0, "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_2

    const-wide/16 v2, 0x2000

    invoke-virtual {p2, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 757
    if-nez v0, :cond_0

    .line 758
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200fe

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 759
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 760
    .restart local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 761
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 762
    const/4 v2, 0x3

    invoke-virtual {v0, v6, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 763
    invoke-virtual {p1, v0, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 765
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0, v2, v5, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 766
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    .line 767
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 772
    :cond_1
    :goto_0
    return-void

    .line 769
    :cond_2
    if-eqz v0, :cond_1

    .line 770
    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private drawBestPhotoIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 8
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/16 v7, 0x25

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 733
    invoke-virtual {p1, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 735
    .local v0, "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_2

    const-wide/16 v2, 0x1000

    invoke-virtual {p2, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 736
    if-nez v0, :cond_0

    .line 737
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200ff

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 738
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 739
    .restart local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 740
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 741
    const/4 v2, 0x3

    invoke-virtual {v0, v6, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 742
    invoke-virtual {p1, v0, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 744
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0, v2, v5, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 745
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    .line 746
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 751
    :cond_1
    :goto_0
    return-void

    .line 748
    :cond_2
    if-eqz v0, :cond_1

    .line 749
    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private drawBurstShotIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 8
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/16 v7, 0x18

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 649
    invoke-virtual {p1, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 651
    .local v0, "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_2

    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->isBurstShot(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 652
    if-nez v0, :cond_0

    .line 653
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020100

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 654
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 655
    .restart local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 656
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 657
    const/4 v2, 0x3

    invoke-virtual {v0, v6, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 658
    invoke-virtual {p1, v0, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 660
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0, v2, v5, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 661
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    .line 662
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 667
    :cond_1
    :goto_0
    return-void

    .line 664
    :cond_2
    if-eqz v0, :cond_1

    .line 665
    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private drawCinePicIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 8
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/16 v7, 0x1a

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 691
    invoke-virtual {p1, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 693
    .local v0, "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_2

    const-wide/16 v2, 0x20

    invoke-virtual {p2, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 694
    if-nez v0, :cond_0

    .line 695
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02010e

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 696
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 697
    .restart local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 698
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 699
    const/4 v2, 0x3

    invoke-virtual {v0, v6, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 700
    invoke-virtual {p1, v0, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 702
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0, v2, v5, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 703
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    .line 704
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 709
    :cond_1
    :goto_0
    return-void

    .line 706
    :cond_2
    if-eqz v0, :cond_1

    .line 707
    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private drawDramaShotIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 8
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/16 v7, 0x28

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 796
    invoke-virtual {p1, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 798
    .local v0, "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_2

    const-wide/32 v2, 0x8000

    invoke-virtual {p2, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 799
    if-nez v0, :cond_0

    .line 800
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020107

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 801
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 802
    .restart local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 803
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 804
    const/4 v2, 0x3

    invoke-virtual {v0, v6, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 805
    invoke-virtual {p1, v0, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 807
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0, v2, v5, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 808
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    .line 809
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 814
    :cond_1
    :goto_0
    return-void

    .line 811
    :cond_2
    if-eqz v0, :cond_1

    .line 812
    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private drawEraserIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 8
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/16 v7, 0x27

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 775
    invoke-virtual {p1, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 777
    .local v0, "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_2

    const-wide/16 v2, 0x4000

    invoke-virtual {p2, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 778
    if-nez v0, :cond_0

    .line 779
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020109

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 780
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 781
    .restart local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 782
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 783
    const/4 v2, 0x3

    invoke-virtual {v0, v6, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 784
    invoke-virtual {p1, v0, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 786
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0, v2, v5, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 787
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    .line 788
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 793
    :cond_1
    :goto_0
    return-void

    .line 790
    :cond_2
    if-eqz v0, :cond_1

    .line 791
    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private drawFocusBox(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V
    .locals 4
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x0

    .line 915
    invoke-virtual {p1, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 916
    .local v0, "focusBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGenericMotionFocus:I

    if-ne v1, p2, :cond_2

    .line 917
    if-nez v0, :cond_0

    .line 918
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "focusBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 919
    .restart local v0    # "focusBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const v1, 0x7f020291

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 920
    invoke-virtual {p1, v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 922
    :cond_0
    invoke-virtual {v0, v2, v2, v2, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setPaddings(IIII)V

    .line 923
    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 927
    :cond_1
    :goto_0
    return-void

    .line 924
    :cond_2
    if-eqz v0, :cond_1

    .line 925
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private drawFrame(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 6
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/high16 v5, 0x55000000

    const/4 v4, 0x1

    .line 937
    invoke-virtual {p1, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlColorFrameView;

    .line 939
    .local v0, "frame":Lcom/sec/android/gallery3d/glcore/GlColorFrameView;
    if-nez v0, :cond_0

    .line 940
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlColorFrameView;

    .end local v0    # "frame":Lcom/sec/android/gallery3d/glcore/GlColorFrameView;
    invoke-direct {v0}, Lcom/sec/android/gallery3d/glcore/GlColorFrameView;-><init>()V

    .line 941
    .restart local v0    # "frame":Lcom/sec/android/gallery3d/glcore/GlColorFrameView;
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlColorFrameView$Frame;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v1, v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlColorFrameView$Frame;-><init>(Lcom/sec/android/gallery3d/glcore/GlColorFrameView;II)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlColorFrameView;->addFrame(Lcom/sec/android/gallery3d/glcore/GlColorFrameView$Frame;)V

    .line 942
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlColorFrameView$Frame;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x3

    const v3, -0x232324

    invoke-direct {v1, v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlColorFrameView$Frame;-><init>(Lcom/sec/android/gallery3d/glcore/GlColorFrameView;II)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlColorFrameView;->addFrame(Lcom/sec/android/gallery3d/glcore/GlColorFrameView$Frame;)V

    .line 943
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlColorFrameView$Frame;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v1, v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlColorFrameView$Frame;-><init>(Lcom/sec/android/gallery3d/glcore/GlColorFrameView;II)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlColorFrameView;->addFrame(Lcom/sec/android/gallery3d/glcore/GlColorFrameView$Frame;)V

    .line 944
    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlColorFrameView;->setStartOffset(I)V

    .line 945
    invoke-virtual {p1, v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 947
    :cond_0
    return-void
.end method

.method private drawImageNoteIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;I)V
    .locals 9
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p3, "position"    # I

    .prologue
    const/16 v8, 0xf

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 503
    const/4 v0, 0x0

    .line 505
    .local v0, "drawIcon":Z
    if-eqz p2, :cond_0

    .line 506
    const-wide/32 v4, 0x80000

    invoke-virtual {p2, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v0

    .line 509
    :cond_0
    invoke-virtual {p1, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 510
    .local v2, "view":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v0, :cond_3

    .line 511
    if-nez v2, :cond_1

    .line 512
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020108

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 513
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v2, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v2    # "view":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 514
    .restart local v2    # "view":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 515
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 516
    const/4 v3, 0x3

    invoke-virtual {v2, v7, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 517
    invoke-virtual {v2, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 518
    invoke-virtual {p1, v2, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 520
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    iget v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v2, v3, v6, v6, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 521
    iget v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    .line 522
    invoke-virtual {v2, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 526
    :cond_2
    :goto_0
    return-void

    .line 523
    :cond_3
    if-eqz v2, :cond_2

    .line 524
    invoke-virtual {v2, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private drawMagicShotIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 8
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/16 v7, 0x24

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 712
    invoke-virtual {p1, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 714
    .local v0, "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_2

    const-wide/16 v2, 0x800

    invoke-virtual {p2, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 715
    if-nez v0, :cond_0

    .line 716
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020118

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 717
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 718
    .restart local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 719
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 720
    const/4 v2, 0x3

    invoke-virtual {v0, v6, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 721
    invoke-virtual {p1, v0, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 723
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0, v2, v5, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 724
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    .line 725
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 730
    :cond_1
    :goto_0
    return-void

    .line 727
    :cond_2
    if-eqz v0, :cond_1

    .line 728
    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private drawMediaTypeIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;Z)V
    .locals 8
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p3, "show"    # Z

    .prologue
    const/16 v7, 0xe

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 529
    invoke-virtual {p1, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 530
    .local v2, "view":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mForceDisplaySourceIcon:Z

    invoke-direct {p0, p2, v3}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getImageResourceId(Lcom/sec/android/gallery3d/data/MediaItem;Z)I

    move-result v1

    .line 532
    .local v1, "resId":I
    if-eqz p3, :cond_2

    const/4 v3, -0x1

    if-eq v1, v3, :cond_2

    .line 533
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 534
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    if-nez v2, :cond_0

    .line 535
    new-instance v2, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v2    # "view":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 536
    .restart local v2    # "view":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const/4 v3, 0x3

    invoke-virtual {v2, v6, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 537
    invoke-virtual {p1, v2, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 539
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 540
    iget v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v2, v3, v5, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 541
    invoke-virtual {v2, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 542
    iget v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    .line 543
    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 544
    invoke-virtual {v2, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 548
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    :goto_0
    return-void

    .line 545
    :cond_2
    if-eqz v2, :cond_1

    .line 546
    invoke-virtual {v2, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private drawOutOfFocus(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 8
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 859
    const/16 v3, 0x2b

    invoke-virtual {p1, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 861
    .local v0, "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_2

    const-wide/32 v4, 0x20000

    invoke-virtual {p2, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 862
    if-nez v0, :cond_0

    .line 863
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02010b

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 864
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 865
    .restart local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 866
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 867
    const/4 v3, 0x3

    invoke-virtual {v0, v7, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 869
    const/16 v3, 0x2a

    :try_start_0
    invoke-virtual {p1, v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 875
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    iget v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0, v3, v6, v6, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 876
    iget v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    .line 877
    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 882
    :cond_1
    :goto_0
    return-void

    .line 870
    .restart local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :catch_0
    move-exception v2

    .line 871
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 879
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    if-eqz v0, :cond_1

    .line 880
    invoke-virtual {v0, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private drawPanoramaIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;Z)V
    .locals 8
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p3, "show"    # Z

    .prologue
    const/16 v7, 0x14

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 483
    invoke-virtual {p1, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 484
    .local v1, "view":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p3, :cond_2

    if-eqz p2, :cond_2

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIsPhotoFrameSet:Z

    if-nez v2, :cond_2

    invoke-static {p2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 485
    if-nez v1, :cond_0

    .line 486
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020111

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 487
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v1    # "view":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 488
    .restart local v1    # "view":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 489
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 490
    const/4 v2, 0x3

    invoke-virtual {v1, v6, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 491
    invoke-virtual {p1, v1, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 493
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    add-int/lit8 v2, v2, 0x3

    invoke-virtual {v1, v2, v5, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 494
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    .line 495
    invoke-virtual {v1, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 500
    :cond_1
    :goto_0
    return-void

    .line 497
    :cond_2
    if-eqz v1, :cond_1

    .line 498
    invoke-virtual {v1, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private drawPicMotionIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 8
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/16 v7, 0x29

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 817
    invoke-virtual {p1, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 819
    .local v0, "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_2

    const-wide/32 v2, 0x10000

    invoke-virtual {p2, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 820
    if-nez v0, :cond_0

    .line 821
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020105

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 822
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 823
    .restart local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 824
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 825
    const/4 v2, 0x3

    invoke-virtual {v0, v6, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 826
    invoke-virtual {p1, v0, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 828
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0, v2, v5, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 829
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    .line 830
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 835
    :cond_1
    :goto_0
    return-void

    .line 832
    :cond_2
    if-eqz v0, :cond_1

    .line 833
    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private drawSecretBoxIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 8
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/16 v7, 0x1e

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 670
    invoke-virtual {p1, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 672
    .local v0, "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_2

    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->isSecretbox(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 673
    if-nez v0, :cond_0

    .line 674
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200d7

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 675
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 676
    .restart local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 677
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 678
    const/4 v2, 0x3

    invoke-virtual {v0, v6, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 679
    invoke-virtual {p1, v0, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 681
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0, v2, v5, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 682
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    .line 683
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 688
    :cond_1
    :goto_0
    return-void

    .line 685
    :cond_2
    if-eqz v0, :cond_1

    .line 686
    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private drawSequenceIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 9
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/16 v8, 0x2c

    const/4 v7, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 595
    invoke-virtual {p1, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 597
    .local v0, "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_2

    const-wide/32 v4, 0x40000

    invoke-virtual {p2, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v4

    if-nez v4, :cond_0

    const-wide/16 v4, 0x80

    invoke-virtual {p2, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 599
    :cond_0
    if-nez v0, :cond_1

    .line 600
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020117

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 601
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 602
    .restart local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 603
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 604
    invoke-virtual {v0, v7, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 605
    invoke-virtual {p1, v0, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 607
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0, v4, v3, v3, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 608
    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    iput v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    .line 609
    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 614
    :goto_0
    return v2

    .line 612
    :cond_2
    if-eqz v0, :cond_3

    .line 613
    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    :cond_3
    move v2, v3

    .line 614
    goto :goto_0
.end method

.method private drawSoundSceneIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 8
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/16 v7, 0x16

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 551
    invoke-virtual {p1, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 553
    .local v0, "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_2

    const-wide/16 v2, 0x10

    invoke-virtual {p2, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 554
    if-nez v0, :cond_0

    .line 555
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02011c

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 556
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 557
    .restart local v0    # "ImageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 558
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 559
    const/4 v2, 0x3

    invoke-virtual {v0, v6, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 560
    invoke-virtual {p1, v0, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 562
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0, v2, v5, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 563
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    .line 564
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 569
    :cond_1
    :goto_0
    return-void

    .line 566
    :cond_2
    if-eqz v0, :cond_1

    .line 567
    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private drawThumbnailImage(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 15
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I
    .param p3, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 950
    if-nez p1, :cond_0

    .line 987
    .end local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :goto_0
    return-void

    .line 953
    .restart local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    if-nez p3, :cond_1

    .line 954
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->readyDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 959
    :cond_1
    const/16 v1, 0xa

    :try_start_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    if-eqz v1, :cond_2

    .line 962
    const/4 v14, 0x6

    .line 963
    .local v14, "padding":I
    const/4 v1, 0x6

    const/4 v3, 0x6

    const/4 v4, 0x6

    const/4 v7, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3, v4, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setPaddings(IIII)V

    .line 966
    .end local v14    # "padding":I
    :cond_2
    move/from16 v0, p2

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getThumbnailBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 968
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    if-nez v2, :cond_3

    .line 969
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->readyDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 984
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    .end local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :catch_0
    move-exception v13

    .line 985
    .local v13, "e":Ljava/lang/Exception;
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 971
    .end local v13    # "e":Ljava/lang/Exception;
    .restart local v2    # "bitmap":Landroid/graphics/Bitmap;
    .restart local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_3
    :try_start_1
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-eqz v1, :cond_4

    if-eqz p3, :cond_4

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 972
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->getBrokenPictureThumbnail(Landroid/content/res/Resources;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 974
    :cond_4
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v5

    .line 975
    .local v5, "rotation":I
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceThumbnail:Z

    if-eqz v1, :cond_5

    move-object/from16 v0, p3

    instance-of v1, v0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v1, :cond_5

    invoke-static/range {p3 .. p3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 977
    check-cast p3, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/data/LocalImage;->getRectOfAllFaces()Landroid/graphics/RectF;

    move-result-object v6

    .line 978
    .local v6, "faceRect":Landroid/graphics/RectF;
    const/16 v3, 0x140

    const/16 v4, 0x140

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setCroppedBitmap(Landroid/graphics/Bitmap;IIILandroid/graphics/RectF;)V

    goto :goto_0

    .line 981
    .end local v6    # "faceRect":Landroid/graphics/RectF;
    .restart local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_5
    const/16 v9, 0x140

    const/16 v10, 0x140

    const/4 v12, 0x0

    move-object/from16 v7, p1

    move-object v8, v2

    move v11, v5

    invoke-virtual/range {v7 .. v12}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setCroppedBitmap(Landroid/graphics/Bitmap;IIILandroid/graphics/RectF;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private drawUnconfirmIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 11
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v10, 0x7

    const/4 v9, 0x1

    const/4 v8, 0x3

    const/4 v7, 0x0

    .line 449
    invoke-virtual {p1, v10}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 450
    .local v3, "unknowIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemType()I

    move-result v4

    if-nez v4, :cond_4

    .line 451
    if-nez v3, :cond_0

    .line 452
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0200de

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 453
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v3, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v3    # "unknowIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 454
    .restart local v3    # "unknowIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v3, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 455
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 456
    invoke-virtual {v3, v8, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 460
    :goto_0
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 461
    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 462
    invoke-virtual {p1, v3, v10}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 464
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    const/4 v1, 0x0

    .local v1, "left_margine":I
    move-object v2, p2

    .line 465
    check-cast v2, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .line 467
    .local v2, "localMediaItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    iget-boolean v4, v2, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mIsGroupItem:Z

    if-nez v4, :cond_3

    .line 468
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mConfig:Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    iget v4, v4, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_left_margin:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mConfig:Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    iget v5, v5, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_icon_left_margin:I

    add-int v1, v4, v5

    .line 469
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mConfig:Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    iget v4, v4, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_bottom_margin:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mConfig:Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    iget v5, v5, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_icon_bottom_margin:I

    add-int/2addr v4, v5

    invoke-virtual {v3, v1, v7, v7, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 475
    :goto_1
    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    iput v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    .line 476
    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 480
    .end local v1    # "left_margine":I
    .end local v2    # "localMediaItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :cond_1
    :goto_2
    return-void

    .line 458
    .restart local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_2
    invoke-virtual {v3, v9, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    goto :goto_0

    .line 471
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    .restart local v1    # "left_margine":I
    .restart local v2    # "localMediaItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :cond_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mConfig:Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    iget v4, v4, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_multishot_left_margin:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mConfig:Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    iget v5, v5, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_icon_left_margin:I

    add-int v1, v4, v5

    .line 472
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mConfig:Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    iget v4, v4, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_multishot_bottom_margin:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mConfig:Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    iget v5, v5, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_icon_bottom_margin:I

    add-int/2addr v4, v5

    invoke-virtual {v3, v1, v7, v7, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    goto :goto_1

    .line 477
    .end local v1    # "left_margine":I
    .end local v2    # "localMediaItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :cond_4
    if-eqz v3, :cond_1

    .line 478
    invoke-virtual {v3, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_2
.end method

.method private drawVideoPlayIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;Z)V
    .locals 10
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p3, "show"    # Z

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 421
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v3

    const/4 v6, 0x4

    if-ne v3, v6, :cond_3

    instance-of v3, p2, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-nez v3, :cond_3

    move v1, v4

    .line 423
    .local v1, "isVideo":Z
    :goto_0
    invoke-virtual {p1, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 425
    .local v2, "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p3, :cond_4

    if-eqz v1, :cond_4

    .line 426
    if-nez v2, :cond_1

    .line 427
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v3

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020308

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 429
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v2, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v2    # "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 430
    .restart local v2    # "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 431
    invoke-virtual {v2, v8, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 432
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    invoke-virtual {v2, v3, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 433
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v3

    if-lez v3, :cond_0

    instance-of v3, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    if-eqz v3, :cond_0

    move-object v3, p0

    check-cast v3, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getIsDragging()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 437
    check-cast p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    .end local p0    # "this":Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;
    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->setIsDragging(Z)V

    .line 439
    :cond_0
    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 440
    invoke-virtual {p1, v2, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 442
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    invoke-virtual {v2, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 446
    :cond_2
    :goto_1
    return-void

    .end local v1    # "isVideo":Z
    .end local v2    # "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .restart local p0    # "this":Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;
    :cond_3
    move v1, v5

    .line 421
    goto :goto_0

    .line 443
    .restart local v1    # "isVideo":Z
    .restart local v2    # "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_4
    if-eqz v2, :cond_2

    .line 444
    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method private getGroupIds()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v3, 0x0

    .line 275
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 276
    .local v1, "baseUri":Landroid/net/Uri;
    const-string v9, "bucket_id = ?"

    .line 277
    .local v9, "whereClauseGrouping":Ljava/lang/String;
    const-string v10, "bucket_id = ? and group_id = ?"

    .line 278
    .local v10, "whereClauseID":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "count(*)"

    aput-object v0, v2, v3

    const-string v0, "group_id"

    aput-object v0, v2, v12

    .line 279
    .local v2, "projectionGrouping":[Ljava/lang/String;
    new-array v8, v12, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v8, v3

    .line 281
    .local v8, "projectionID":[Ljava/lang/String;
    const/4 v6, -0x1

    .line 282
    .local v6, "bucketId":I
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAllviewBurstshot:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->getCameraBucketId()I

    move-result v6

    .line 287
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mItemIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 288
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 289
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupCounts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 291
    const/4 v7, 0x0

    .line 293
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and group_id>0) GROUP BY (group_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 295
    if-nez v7, :cond_1

    .line 296
    sget-object v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->TAG:Ljava/lang/String;

    const-string v3, "query fail"

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 307
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 310
    :goto_1
    return-void

    .line 285
    .end local v7    # "cursor":Landroid/database/Cursor;
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v6

    goto :goto_0

    .line 300
    .restart local v7    # "cursor":Landroid/database/Cursor;
    :cond_1
    :goto_2
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 301
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-le v0, v12, :cond_1

    .line 302
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupCounts:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 303
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupIds:Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 307
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    :cond_2
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 309
    invoke-direct {p0, v1, v8, v10, v6}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getItemIds(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1
.end method

.method private getImageResourceId(Lcom/sec/android/gallery3d/data/MediaItem;Z)I
    .locals 2
    .param p1, "media"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "forceDisplay"    # Z

    .prologue
    const/4 v0, -0x1

    .line 885
    if-nez p1, :cond_1

    .line 909
    :cond_0
    :goto_0
    return v0

    .line 887
    :cond_1
    instance-of v1, p1, Lcom/sec/android/gallery3d/remote/cloud/CloudMediaItem;

    if-eqz v1, :cond_3

    .line 888
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaidu:Z

    if-nez v0, :cond_2

    .line 889
    const v0, 0x7f0200d9

    goto :goto_0

    .line 891
    :cond_2
    const v0, 0x7f0200d8

    goto :goto_0

    .line 892
    :cond_3
    instance-of v1, p1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMediaItem;

    if-eqz v1, :cond_4

    .line 893
    const v0, 0x7f0200dd

    goto :goto_0

    .line 894
    :cond_4
    instance-of v1, p1, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    if-eqz v1, :cond_5

    .line 895
    const v0, 0x7f0200da

    goto :goto_0

    .line 896
    :cond_5
    instance-of v1, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-eqz v1, :cond_6

    .line 897
    const v0, 0x7f0201e2

    goto :goto_0

    .line 898
    :cond_6
    if-eqz p2, :cond_0

    .line 899
    instance-of v1, p1, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 900
    check-cast v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->isCameraItem()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 901
    const v0, 0x7f020102

    goto :goto_0

    .line 902
    :cond_7
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardPath(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 903
    const v0, 0x7f02010d

    goto :goto_0

    .line 905
    :cond_8
    const v0, 0x7f02010c

    goto :goto_0
.end method

.method private getItemIds(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;I)V
    .locals 10
    .param p1, "baseUri"    # Landroid/net/Uri;
    .param p2, "projectionID"    # [Ljava/lang/String;
    .param p3, "whereClauseID"    # Ljava/lang/String;
    .param p4, "bucketId"    # I

    .prologue
    .line 1268
    const/4 v8, 0x0

    .local v8, "i":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    .local v9, "n":I
    :goto_0
    if-ge v8, v9, :cond_2

    .line 1269
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupIds:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    .line 1270
    .local v7, "gid":Ljava/lang/Long;
    if-nez v7, :cond_0

    .line 1268
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1274
    :cond_0
    const/4 v6, 0x0

    .line 1276
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    const/4 v1, 0x1

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    const-string v5, "datetaken DESC,_id DESC limit 1"

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1281
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1282
    :cond_1
    sget-object v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->TAG:Ljava/lang/String;

    const-string v1, "query fail"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1283
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mItemIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1284
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1285
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupCounts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1291
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 1294
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "gid":Ljava/lang/Long;
    :cond_2
    return-void

    .line 1289
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v7    # "gid":Ljava/lang/Long;
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mItemIds:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1291
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private isPhotoFrameSet()Z
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->isPhotoFrameAlbum()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private procGrouping(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v10, 0x0

    .line 108
    iput v10, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mRemovedCount:I

    .line 110
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupIds:Ljava/util/ArrayList;

    if-nez v10, :cond_0

    .line 144
    .end local p1    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :goto_0
    return-object p1

    .line 114
    .restart local p1    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_0
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupIds:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 115
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getGroupIds()V

    .line 118
    :cond_1
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 120
    .local v8, "mediaItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :try_start_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    .local v9, "size":I
    :goto_1
    if-ge v3, v9, :cond_4

    .line 121
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 123
    .local v6, "media":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v6, :cond_2

    instance-of v10, v6, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v10, :cond_2

    .line 124
    move-object v0, v6

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    move-object v7, v0

    .line 125
    .local v7, "mediaItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    const/4 v10, 0x0

    iput-boolean v10, v7, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mIsGroupItem:Z

    .line 127
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getGroupId()J

    move-result-wide v4

    .line 128
    .local v4, "gId":J
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupIds:Ljava/util/ArrayList;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mUngroupIdSet:Ljava/util/Set;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_5

    .line 129
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mItemIds:Ljava/util/ArrayList;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getItemId()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 130
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    :goto_2
    const/4 v10, 0x1

    iput-boolean v10, v7, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mIsGroupItem:Z

    .line 120
    .end local v4    # "gId":J
    .end local v7    # "mediaItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :cond_2
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 132
    .restart local v4    # "gId":J
    .restart local v7    # "mediaItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :cond_3
    iget v10, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mRemovedCount:I

    add-int/lit8 v10, v10, 0x1

    iput v10, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mRemovedCount:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 140
    .end local v4    # "gId":J
    .end local v6    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v7    # "mediaItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    .end local v9    # "size":I
    :catch_0
    move-exception v2

    .line 141
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 143
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_4
    iget v10, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mTotalRemovedCount:I

    iget v11, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mRemovedCount:I

    add-int/2addr v10, v11

    iput v10, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mTotalRemovedCount:I

    move-object p1, v8

    .line 144
    goto :goto_0

    .line 136
    .restart local v4    # "gId":J
    .restart local v6    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v7    # "mediaItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    .restart local v9    # "size":I
    :cond_5
    :try_start_1
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3
.end method

.method private updateContent(Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;)V
    .locals 18
    .param p1, "info"    # Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;

    .prologue
    .line 1124
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSourceVersion:J

    const-wide/16 v16, -0x1

    cmp-long v13, v14, v16

    if-nez v13, :cond_3

    const/4 v3, 0x1

    .line 1125
    .local v3, "firstLoading":Z
    :goto_0
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->version:J

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSourceVersion:J

    .line 1126
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    .line 1127
    .local v9, "modelListener":Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIsChangedSource:Z

    if-nez v13, :cond_0

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSize:I

    move-object/from16 v0, p1

    iget v14, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->size:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mTotalRemovedCount:I

    sub-int/2addr v14, v15

    if-ne v13, v14, :cond_0

    move-object/from16 v0, p1

    iget v13, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->size:I

    if-nez v13, :cond_1

    .line 1128
    :cond_0
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIsChangedSource:Z

    .line 1129
    move-object/from16 v0, p1

    iget v13, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->size:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v3}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->setContentRange(IZ)V

    .line 1130
    if-eqz v9, :cond_1

    .line 1131
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSize:I

    invoke-interface {v9, v13}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->onSizeChanged(I)V

    .line 1135
    :cond_1
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->items:Ljava/util/ArrayList;

    .line 1136
    .local v8, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-nez v8, :cond_4

    .line 1167
    :cond_2
    :goto_1
    return-void

    .line 1124
    .end local v3    # "firstLoading":Z
    .end local v8    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v9    # "modelListener":Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;
    :cond_3
    const/4 v3, 0x0

    goto :goto_0

    .line 1137
    .restart local v3    # "firstLoading":Z
    .restart local v8    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .restart local v9    # "modelListener":Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;
    :cond_4
    move-object/from16 v0, p1

    iget v13, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadStart:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContentStart:I

    invoke-static {v13, v14}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 1138
    .local v11, "start":I
    move-object/from16 v0, p1

    iget v13, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadStart:I

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v14

    add-int/2addr v13, v14

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContentEnd:I

    invoke-static {v13, v14}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1140
    .local v2, "end":I
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    .line 1141
    .local v10, "reloadTask":Lcom/sec/samsung/gallery/view/adapter/ReloadTask;
    move v4, v11

    .local v4, "i":I
    :goto_2
    if-ge v4, v2, :cond_5

    .line 1142
    if-eqz v10, :cond_5

    invoke-virtual {v10}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->isActive()Z

    move-result v13

    if-nez v13, :cond_8

    .line 1157
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIsChangedSource:Z

    if-eqz v13, :cond_6

    if-gtz v2, :cond_7

    :cond_6
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSize:I

    move-object/from16 v0, p1

    iget v14, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->size:I

    if-ne v13, v14, :cond_7

    move-object/from16 v0, p1

    iget v13, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->size:I

    if-nez v13, :cond_2

    .line 1158
    :cond_7
    if-lez v2, :cond_b

    .line 1159
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIsChangedSource:Z

    .line 1162
    :goto_3
    move-object/from16 v0, p1

    iget v13, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->size:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v3}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->setContentRange(IZ)V

    .line 1163
    if-eqz v9, :cond_2

    .line 1164
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSize:I

    invoke-interface {v9, v13}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->onSizeChanged(I)V

    goto :goto_1

    .line 1144
    :cond_8
    rem-int/lit16 v5, v4, 0x3e8

    .line 1145
    .local v5, "index":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSetVersion:[J

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->version:J

    aput-wide v14, v13, v5

    .line 1146
    move-object/from16 v0, p1

    iget v13, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadStart:I

    sub-int v13, v4, v13

    invoke-virtual {v8, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1147
    .local v12, "updateItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v12, :cond_a

    const-wide/16 v6, -0x1

    .line 1148
    .local v6, "itemVersion":J
    :goto_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mItemVersion:[J

    aget-wide v14, v13, v5

    cmp-long v13, v14, v6

    if-eqz v13, :cond_9

    .line 1149
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mItemVersion:[J

    aput-wide v6, v13, v5

    .line 1150
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    aput-object v12, v13, v5

    .line 1151
    if-eqz v9, :cond_9

    .line 1152
    invoke-interface {v9, v4}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->onWindowContentChanged(I)V

    .line 1141
    :cond_9
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1147
    .end local v6    # "itemVersion":J
    :cond_a
    invoke-virtual {v12}, Lcom/sec/android/gallery3d/data/MediaItem;->getDataVersion()J

    move-result-wide v6

    goto :goto_4

    .line 1161
    .end local v5    # "index":I
    .end local v12    # "updateItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_b
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIsChangedSource:Z

    goto :goto_3
.end method


# virtual methods
.method protected addContentListener()V
    .locals 2

    .prologue
    .line 1245
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_0

    .line 1246
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSourceListener:Lcom/sec/android/gallery3d/data/ContentListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 1247
    :cond_0
    return-void
.end method

.method public addToUngroupSet(J)V
    .locals 3
    .param p1, "groupId"    # J

    .prologue
    .line 1019
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mUngroupIdSet:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1020
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->resetAdapter()V

    .line 1021
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->reloadData()V

    .line 1022
    return-void
.end method

.method public changeToNewAlbumHeader(Z)V
    .locals 0
    .param p1, "isNewAlbumHeader"    # Z

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mNewAlbumHeader:Z

    .line 105
    return-void
.end method

.method public getCount()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 153
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIsChangedSource:Z

    if-eqz v2, :cond_0

    .line 156
    :goto_0
    return v1

    .line 155
    :cond_0
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mNewAlbumHeader:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 156
    .local v0, "count":I
    :goto_1
    if-ltz v0, :cond_2

    .end local v0    # "count":I
    :goto_2
    move v1, v0

    goto :goto_0

    .line 155
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSize:I

    goto :goto_1

    .restart local v0    # "count":I
    :cond_2
    move v0, v1

    .line 156
    goto :goto_2
.end method

.method public getIntrItems(I)Ljava/util/List;
    .locals 20
    .param p1, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 198
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIsGroupCameraSet:Z

    if-nez v15, :cond_0

    .line 199
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 200
    .local v11, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual/range {p0 .. p1}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v15

    invoke-interface {v11, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v15, v11

    .line 242
    :goto_0
    return-object v15

    .line 204
    .end local v11    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_0
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 205
    .restart local v11    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual/range {p0 .. p1}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v9

    .line 206
    .local v9, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v9, :cond_1

    .line 207
    sget-object v15, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->TAG:Ljava/lang/String;

    const-string v16, "item is null!"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    const/4 v15, 0x0

    goto :goto_0

    :cond_1
    move-object v15, v9

    .line 211
    check-cast v15, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    invoke-virtual {v15}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getGroupId()J

    move-result-wide v6

    .line 212
    .local v6, "groupId":J
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupIds:Ljava/util/ArrayList;

    if-eqz v15, :cond_2

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupIds:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v15

    if-eqz v15, :cond_2

    .line 213
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getGroupIds()V

    .line 215
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupIds:Ljava/util/ArrayList;

    if-eqz v15, :cond_3

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupIds:Ljava/util/ArrayList;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_4

    .line 216
    :cond_3
    invoke-interface {v11, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v15, v11

    .line 217
    goto :goto_0

    .line 220
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupCounts:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupIds:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 221
    .local v2, "count":I
    const/4 v15, 0x1

    if-gt v2, v15, :cond_5

    .line 222
    invoke-interface {v11, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v15, v11

    .line 223
    goto :goto_0

    .line 226
    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v16

    move-object/from16 v0, v16

    move/from16 v1, p1

    invoke-virtual {v15, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getIndexOfItem(Lcom/sec/android/gallery3d/data/Path;I)I

    move-result v8

    .line 227
    .local v8, "indexOfItem":I
    sub-int v15, v8, v2

    if-ltz v15, :cond_7

    sub-int v13, v8, v2

    .line 228
    .local v13, "startIndex":I
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getIntrinsicCount()I

    move-result v15

    add-int/lit8 v4, v15, -0x1

    .line 229
    .local v4, "endIndex":I
    sub-int v15, v8, v2

    if-ltz v15, :cond_8

    move v15, v2

    :goto_2
    add-int v16, v8, v2

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    if-gt v0, v4, :cond_9

    .end local v2    # "count":I
    :goto_3
    add-int v12, v15, v2

    .line 232
    .local v12, "resizedcount":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v15, v13, v12}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v10

    .line 233
    .local v10, "itemlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_6
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 234
    .local v14, "subItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v14, :cond_6

    move-object v0, v14

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    move-object v15, v0

    invoke-virtual {v15}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getGroupId()J

    move-result-wide v16

    move-object v0, v9

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    move-object v15, v0

    invoke-virtual {v15}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getGroupId()J

    move-result-wide v18

    cmp-long v15, v16, v18

    if-nez v15, :cond_6

    .line 237
    invoke-interface {v11, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    .line 240
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v10    # "itemlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v14    # "subItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :catch_0
    move-exception v3

    .line 241
    .local v3, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v3}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    .line 242
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 227
    .end local v3    # "e":Ljava/lang/IndexOutOfBoundsException;
    .end local v4    # "endIndex":I
    .end local v12    # "resizedcount":I
    .end local v13    # "startIndex":I
    .restart local v2    # "count":I
    :cond_7
    const/4 v13, 0x0

    goto :goto_1

    .restart local v4    # "endIndex":I
    .restart local v13    # "startIndex":I
    :cond_8
    move v15, v8

    .line 229
    goto :goto_2

    :cond_9
    sub-int v16, v4, v8

    add-int/lit8 v2, v16, 0x1

    goto :goto_3

    .end local v2    # "count":I
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v10    # "itemlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .restart local v12    # "resizedcount":I
    :cond_a
    move-object v15, v11

    .line 239
    goto/16 :goto_0
.end method

.method public getIntrItemsList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getIntrinsicCount()I
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    return v0
.end method

.method public getIntrinsicIndex(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v1, -0x1

    .line 189
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getIntrinsicCount()I

    move-result v2

    if-lt p1, v2, :cond_1

    .line 194
    :cond_0
    :goto_0
    return v1

    .line 191
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 192
    .local v0, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_0

    .line 194
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getIndexOfItem(Lcom/sec/android/gallery3d/data/Path;I)I

    move-result v1

    goto :goto_0
.end method

.method public getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 4
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x0

    .line 161
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mNewAlbumHeader:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-nez v1, :cond_0

    move-object v1, v2

    .line 181
    :goto_0
    return-object v1

    .line 166
    :cond_0
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mNewAlbumHeader:Z

    if-eqz v1, :cond_2

    .line 167
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    if-lt p1, v1, :cond_1

    move-object v1, v2

    .line 168
    goto :goto_0

    .line 170
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    goto :goto_0

    .line 172
    :cond_2
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSize:I

    if-lt p1, v1, :cond_3

    move-object v1, v2

    .line 173
    goto :goto_0

    .line 174
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    if-eqz v1, :cond_4

    .line 175
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    array-length v3, v3

    rem-int v3, p1, v3

    aget-object v1, v1, v3

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 177
    :catch_0
    move-exception v0

    .line 178
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    move-object v1, v2

    .line 181
    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemImage(II)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "position"    # I
    .param p2, "type"    # I

    .prologue
    const/4 v0, 0x0

    .line 314
    if-ltz p1, :cond_0

    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSize:I

    if-ge p1, v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    if-nez v1, :cond_1

    .line 322
    :cond_0
    :goto_0
    return-object v0

    .line 317
    :cond_1
    const/4 v1, 0x2

    if-ne p2, v1, :cond_0

    .line 318
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    if-eqz v1, :cond_0

    .line 319
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v0, p1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public getMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method public getThumbnailBitmap(I)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 930
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    .line 931
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v0, p1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 933
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getUnGroupSet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1048
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mUngroupIdSet:Ljava/util/Set;

    return-object v0
.end method

.method public getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "parentLayer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 327
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mNewAlbumHeader:Z

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-nez v5, :cond_1

    .line 392
    .end local p2    # "convertView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_0
    :goto_0
    return-object v1

    .line 332
    .restart local p2    # "convertView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_1
    :try_start_0
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mNewAlbumHeader:Z

    if-eqz v5, :cond_6

    .line 333
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-ge p1, v5, :cond_0

    .line 345
    :cond_2
    iput v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mViewResult:I

    .line 347
    if-eqz p2, :cond_7

    check-cast p2, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local p2    # "convertView":Lcom/sec/android/gallery3d/glcore/GlView;
    move-object v1, p2

    .line 349
    .local v1, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :goto_1
    iput v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIconLeft:I

    .line 350
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .line 351
    .local v2, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v3, 0x1

    .line 352
    .local v3, "showIcon":Z
    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->isPicasaImage(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 353
    const/4 v3, 0x0

    .line 354
    :cond_3
    invoke-direct {p0, v1, v2, v3}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->drawVideoPlayIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    .line 355
    invoke-direct {p0, v1, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->drawFocusBox(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V

    .line 356
    invoke-direct {p0, v1, p1, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->drawThumbnailImage(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/android/gallery3d/data/MediaItem;)V

    .line 358
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mDisplayBrandicon:Z

    if-eqz v5, :cond_5

    .line 359
    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->drawUnconfirmIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 360
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v5, :cond_4

    .line 361
    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->drawSecretBoxIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 363
    :cond_4
    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->draw3DImageIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    .line 365
    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->draw3DPanoramaIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v5

    if-nez v5, :cond_8

    const/4 v3, 0x1

    .line 366
    :goto_2
    invoke-direct {p0, v1, v2, v3}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->drawPanoramaIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    .line 368
    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->drawSoundSceneIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 370
    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->drawBurstShotIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 371
    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->drawCinePicIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 372
    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->drawMagicShotIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 373
    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->drawBestPhotoIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 374
    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->drawBestFaceIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 375
    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->drawEraserIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 376
    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->drawDramaShotIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 377
    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->drawPicMotionIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 378
    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->draw3DTourIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 379
    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->drawOutOfFocus(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 380
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mDisplayMediaIcon:Z

    invoke-direct {p0, v1, v2, v4}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->drawMediaTypeIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    .line 381
    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->drawSequenceIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    .line 383
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    if-eqz v4, :cond_5

    .line 384
    invoke-direct {p0, v1, v2, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->drawImageNoteIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;I)V

    .line 388
    :cond_5
    iget-wide v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mAttr:J

    const-wide/16 v6, 0x2

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    .line 389
    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->drawFrame(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V

    goto/16 :goto_0

    .line 336
    .end local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v2    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v3    # "showIcon":Z
    .restart local p2    # "convertView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_6
    :try_start_1
    iget v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSize:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-lt p1, v5, :cond_2

    goto/16 :goto_0

    .line 340
    :catch_0
    move-exception v0

    .line 341
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 347
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_7
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    goto/16 :goto_1

    .end local p2    # "convertView":Lcom/sec/android/gallery3d/glcore/GlView;
    .restart local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .restart local v2    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v3    # "showIcon":Z
    :cond_8
    move v3, v4

    .line 365
    goto :goto_2
.end method

.method public getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "parentLayer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p4, "ext"    # I
    .param p5, "obj"    # Ljava/lang/Object;

    .prologue
    .line 397
    if-eqz p5, :cond_0

    instance-of v0, p5, Lcom/sec/samsung/gallery/glview/GlTextObject;

    if-eqz v0, :cond_0

    .line 398
    const/4 v0, 0x0

    .line 400
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    goto :goto_0
.end method

.method public groupAllBurstImages()V
    .locals 1

    .prologue
    .line 1039
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mUngroupAllBurstImages:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mUngroupIdSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1040
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mUngroupAllBurstImages:Z

    .line 1041
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mUngroupIdSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1042
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->resetAdapter()V

    .line 1043
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->reloadData()V

    .line 1045
    :cond_1
    return-void
.end method

.method public isAllBurstImagesUngroup()Z
    .locals 1

    .prologue
    .line 1029
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mUngroupAllBurstImages:Z

    return v0
.end method

.method public isBurstShot(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 6
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 404
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mNewAlbumHeader:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIsGroupCameraSet:Z

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mAttr:J

    const-wide/16 v4, 0x1

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 405
    check-cast v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .line 406
    .local v0, "localMediaItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    iget-boolean v1, v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mIsGroupItem:Z

    if-eqz v1, :cond_0

    .line 407
    const/4 v1, 0x1

    .line 410
    .end local v0    # "localMediaItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isCameraSet()Z
    .locals 1

    .prologue
    .line 251
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIsGroupCameraSet:Z

    return v0
.end method

.method public isCameraSetContainsBrustShot()Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    .line 255
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAllviewBurstshot:Z

    if-eqz v0, :cond_0

    iget-wide v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mAttr:J

    and-long/2addr v2, v6

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v0, Lcom/sec/android/gallery3d/data/AllInOneAlbum;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/AllInOneAlbum;->getCameraBucketId()I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 261
    :goto_0
    return v1

    :cond_0
    iget-wide v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mAttr:J

    and-long/2addr v2, v6

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->isCameraAlbum()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->getBucketId()I

    move-result v0

    sget v2, Lcom/sec/android/gallery3d/util/MediaSetUtils;->CAMERA_BUCKET_ID:I

    if-eq v0, v2, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->getBucketId()I

    move-result v0

    sget v2, Lcom/sec/android/gallery3d/util/MediaSetUtils;->SDCARD_CAMERA_BUCKET_ID:I

    if-ne v0, v2, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->haveBurstShotImages()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->isNewAlbumHeader()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public isNewAlbumHeader()Z
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mNewAlbumHeader:Z

    return v0
.end method

.method public isSecretbox(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 1
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 414
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415
    const/4 v0, 0x1

    .line 417
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUngroup(J)Z
    .locals 3
    .param p1, "groupId"    # J

    .prologue
    .line 1025
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mUngroupIdSet:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected loadData()Z
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1171
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mNewAlbumHeader:Z

    if-eqz v1, :cond_1

    .line 1240
    :cond_0
    :goto_0
    return v4

    .line 1175
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    .line 1181
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v1, :cond_0

    .line 1184
    const-wide/16 v2, -0x1

    .line 1185
    .local v2, "version":J
    sget-object v6, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v6

    .line 1186
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v1, v1, Lcom/sec/android/gallery3d/data/FilterTypeSet;

    if-eqz v1, :cond_2

    .line 1187
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mFilterMimeType:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1188
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v1, Lcom/sec/android/gallery3d/data/FilterTypeSet;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mFilterMimeType:Ljava/lang/String;

    invoke-virtual {v1, v7}, Lcom/sec/android/gallery3d/data/FilterTypeSet;->setFilterMimeType(Ljava/lang/String;)V

    .line 1191
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    move-result-wide v2

    .line 1192
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1194
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->isCameraSetContainsBrustShot()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mUngroupAllBurstImages:Z

    if-nez v1, :cond_3

    move v1, v4

    :goto_1
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIsGroupCameraSet:Z

    .line 1195
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->isPhotoFrameSet()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIsPhotoFrameSet:Z

    .line 1197
    invoke-direct {p0, v2, v3}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->GetUpdateInfo(J)Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;

    move-result-object v0

    .line 1199
    .local v0, "info":Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;
    if-nez v0, :cond_4

    .line 1200
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v1, :cond_0

    .line 1201
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 1202
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    iget v5, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSize:I

    invoke-interface {v1, v5}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->onSizeChanged(I)V

    goto :goto_0

    .line 1192
    .end local v0    # "info":Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_3
    move v1, v5

    .line 1194
    goto :goto_1

    .line 1208
    .restart local v0    # "info":Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;
    :cond_4
    sget-object v4, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v4

    .line 1209
    :try_start_2
    iget-wide v6, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->version:J

    cmp-long v1, v6, v2

    if-eqz v1, :cond_7

    .line 1210
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v1

    iput v1, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->size:I

    .line 1211
    iget-wide v6, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->version:J

    const-wide/16 v8, -0x1

    cmp-long v1, v6, v8

    if-nez v1, :cond_5

    .line 1212
    const/4 v1, 0x0

    iput v1, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadStart:I

    .line 1213
    iget v1, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->size:I

    const/16 v6, 0x40

    invoke-static {v1, v6}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadCount:I

    .line 1215
    :cond_5
    iget v1, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadCount:I

    if-lez v1, :cond_6

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIsGroupCameraSet:Z

    if-eqz v1, :cond_6

    .line 1216
    iget v1, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->size:I

    iget v6, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mTotalRemovedCount:I

    add-int/2addr v1, v6

    const/16 v6, 0x40

    invoke-static {v1, v6}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadCount:I

    .line 1218
    :cond_6
    iput-wide v2, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->version:J

    .line 1219
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->updateContent(Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;)V

    .line 1220
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mTotalRemovedCount:I

    .line 1222
    :cond_7
    iget v1, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadCount:I

    if-lez v1, :cond_9

    .line 1223
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIsGroupCameraSet:Z

    if-eqz v1, :cond_a

    .line 1224
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getGroupIds()V

    .line 1225
    iget v1, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadStart:I

    if-nez v1, :cond_8

    .line 1226
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mTotalRemovedCount:I

    .line 1227
    :cond_8
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iget v6, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadStart:I

    iget v7, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mTotalRemovedCount:I

    add-int/2addr v6, v7

    iget v7, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadCount:I

    invoke-virtual {v1, v6, v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->procGrouping(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->items:Ljava/util/ArrayList;

    .line 1228
    iget v1, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->size:I

    iget v6, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mRemovedCount:I

    sub-int/2addr v1, v6

    iput v1, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->size:I

    .line 1237
    :cond_9
    :goto_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1239
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->updateContent(Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;)V

    move v4, v5

    .line 1240
    goto/16 :goto_0

    .line 1230
    :cond_a
    :try_start_3
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIsConnectedDevice:Z

    if-eqz v1, :cond_b

    .line 1231
    iget v1, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->size:I

    const/16 v6, 0x1388

    invoke-static {v1, v6}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadCount:I

    .line 1232
    sget-object v1, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mIsConnectedDevice = [ true ] ,info.reloadCount = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadCount:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1234
    :cond_b
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iget v6, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadStart:I

    iget v7, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->reloadCount:I

    invoke-virtual {v1, v6, v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter$UpdateInfo;->items:Ljava/util/ArrayList;

    goto :goto_2

    .line 1237
    :catchall_1
    move-exception v1

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1
.end method

.method protected removeContentListener()V
    .locals 2

    .prologue
    .line 1251
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_0

    .line 1252
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSourceListener:Lcom/sec/android/gallery3d/data/ContentListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->removeContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 1253
    :cond_0
    return-void
.end method

.method public resetAdapter()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v3, 0x0

    .line 990
    iput v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSize:I

    .line 991
    iput v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContentStart:I

    .line 992
    iput v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mContentEnd:I

    .line 993
    iput v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mThumbnailActiveStart:I

    .line 994
    iput v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mThumbnailActiveEnd:I

    .line 995
    iput v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mVisibleStart:I

    .line 996
    iput v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mVisibleEnd:I

    .line 997
    iput-wide v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSourceVersion:J

    .line 999
    const/4 v0, 0x0

    .local v0, "slotIndex":I
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mItemVersion:[J

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1000
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 1001
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mItemVersion:[J

    aput-wide v4, v1, v0

    .line 1002
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mSetVersion:[J

    aput-wide v4, v1, v0

    .line 999
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1005
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    if-eqz v1, :cond_1

    .line 1006
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->resetContentWindow()V

    .line 1008
    :cond_1
    iput v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mTotalRemovedCount:I

    .line 1009
    iput v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mRemovedCount:I

    .line 1010
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mItemIds:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    .line 1011
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mItemIds:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1012
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupIds:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    .line 1013
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupIds:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1014
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupCounts:Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    .line 1015
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mGroupCounts:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1016
    :cond_4
    return-void
.end method

.method public setAllBurstImagesUngroup(Z)V
    .locals 0
    .param p1, "ungroupAllBurstImages"    # Z

    .prologue
    .line 1052
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mUngroupAllBurstImages:Z

    .line 1053
    return-void
.end method

.method public setConnectedDevice(Z)V
    .locals 0
    .param p1, "isConnectedDevice"    # Z

    .prologue
    .line 1297
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mIsConnectedDevice:Z

    .line 1298
    return-void
.end method

.method public setDisplayBrandIcon(Z)V
    .locals 0
    .param p1, "displayIcon"    # Z

    .prologue
    .line 1256
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mDisplayBrandicon:Z

    .line 1257
    return-void
.end method

.method public setDisplayMediaIcon(Z)V
    .locals 0
    .param p1, "displayIcon"    # Z

    .prologue
    .line 1260
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mDisplayMediaIcon:Z

    .line 1261
    return-void
.end method

.method public setFilterMimeType(Ljava/lang/String;)V
    .locals 0
    .param p1, "filterMimeType"    # Ljava/lang/String;

    .prologue
    .line 1264
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mFilterMimeType:Ljava/lang/String;

    .line 1265
    return-void
.end method

.method public unGroupAllBurstImages()V
    .locals 1

    .prologue
    .line 1033
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->mUngroupAllBurstImages:Z

    .line 1034
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->resetAdapter()V

    .line 1035
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->reloadData()V

    .line 1036
    return-void
.end method
