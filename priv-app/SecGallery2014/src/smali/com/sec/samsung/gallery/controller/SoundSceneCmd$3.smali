.class Lcom/sec/samsung/gallery/controller/SoundSceneCmd$3;
.super Ljava/lang/Object;
.source "SoundSceneCmd.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/SoundSceneCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/SoundSceneCmd;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/SoundSceneCmd;)V
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/SoundSceneCmd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 3
    .param p1, "focusChange"    # I

    .prologue
    .line 201
    # getter for: Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->access$200()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAudioFocusChanged : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    packed-switch p1, :pswitch_data_0

    .line 209
    :goto_0
    return-void

    .line 204
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundSceneCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/SoundSceneCmd;

    # invokes: Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->stopPlaying()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->access$300(Lcom/sec/samsung/gallery/controller/SoundSceneCmd;)V

    goto :goto_0

    .line 202
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method
