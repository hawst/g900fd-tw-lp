.class Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;
.super Lcom/sec/samsung/gallery/glview/GlScroller;
.source "GlSearchView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlSearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItemScroller"
.end annotation


# static fields
.field private static final PADDING_BOTTOM:F = 0.7f

.field private static final PADDING_LEFT:F = 0.7f

.field private static final PADDING_TOP_H:F = 60.0f

.field private static final PADDING_TOP_PAD:F = 10.0f

.field private static final PADDING_TOP_V:F = 35.0f


# instance fields
.field private mFullVisibleCnt:I

.field private paddingLeft:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V
    .locals 0

    .prologue
    .line 906
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlScroller;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/glview/GlSearchView;Lcom/sec/samsung/gallery/glview/GlSearchView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/glview/GlSearchView$1;

    .prologue
    .line 906
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;-><init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V

    return-void
.end method


# virtual methods
.method public onFocusChange(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 1021
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    if-eqz v0, :cond_0

    .line 1022
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v1, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v2, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeyboardKeywordFocused:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$2000(Lcom/sec/samsung/gallery/glview/GlSearchView;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1, v2, p1, v0}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;->onFocusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V

    .line 1024
    :cond_0
    return-void

    .line 1022
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onMove(F)V
    .locals 1
    .param p1, "delta"    # F

    .prologue
    .line 1016
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iput p1, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mSx:F

    .line 1017
    return-void
.end method

.method public onScrollStep(I)V
    .locals 3
    .param p1, "step"    # I

    .prologue
    .line 1028
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mOnScrollListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 1029
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mOnScrollListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    sget v2, Lcom/sec/samsung/gallery/glview/GlAbsListView;->SCROLL_STATE_FLING:I

    invoke-interface {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;->onScrollStateChanged(Lcom/sec/samsung/gallery/glview/GlAbsListView;I)V

    .line 1031
    :cond_0
    return-void
.end method

.method public reset()V
    .locals 7

    .prologue
    const/high16 v3, 0x41200000    # 10.0f

    const/high16 v6, 0x40000000    # 2.0f

    .line 919
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mViewMode:I
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$1900(Lcom/sec/samsung/gallery/glview/GlSearchView;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewLeftPadding(I)F

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->paddingLeft:F

    .line 920
    const/4 v1, 0x0

    .line 921
    .local v1, "paddingTop":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlSearchView;->mWidth:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeight:I

    if-le v4, v5, :cond_1

    .line 922
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v3

    .line 927
    :goto_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeightSpace:F

    const v4, 0x3f333333    # 0.7f

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$200(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    move-result-object v4

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mScrollVisibleH:F

    sub-float/2addr v3, v4

    sub-float/2addr v3, v1

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mScrollVisibleH:F

    .line 928
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlSearchView;->mColCnt:I

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemHVisibleCnt:I

    .line 930
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mViewMode:I
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$1900(Lcom/sec/samsung/gallery/glview/GlSearchView;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewThumbnailWidth(I)F

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemW:F

    .line 931
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mViewMode:I
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$1900(Lcom/sec/samsung/gallery/glview/GlSearchView;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewThumbnailHeight(I)F

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemH:F

    .line 932
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemW:F

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mViewMode:I
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$1900(Lcom/sec/samsung/gallery/glview/GlSearchView;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewHorizontalGap(I)F

    move-result v4

    add-float/2addr v3, v4

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemGapW:F

    .line 933
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemH:F

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mViewMode:I
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$1900(Lcom/sec/samsung/gallery/glview/GlSearchView;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewVerticalGap(I)F

    move-result v4

    add-float/2addr v3, v4

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemGapH:F

    .line 935
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlSearchView;->mWidthSpace:F

    neg-float v3, v3

    div-float/2addr v3, v6

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mViewMode:I
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$1900(Lcom/sec/samsung/gallery/glview/GlSearchView;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewThumbnailWidth(I)F

    move-result v4

    div-float/2addr v4, v6

    add-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->paddingLeft:F

    add-float/2addr v3, v4

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemStartX:F

    .line 936
    const/4 v2, 0x0

    .line 937
    .local v2, "row":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlSearchView;->mColCnt:I

    if-nez v3, :cond_3

    .line 938
    const/4 v2, 0x0

    .line 943
    :goto_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeightSpace:F

    div-float/2addr v3, v6

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemGapH:F

    div-float/2addr v4, v6

    sub-float/2addr v3, v4

    sub-float/2addr v3, v1

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$200(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    move-result-object v4

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mScrollVisibleH:F

    sub-float/2addr v3, v4

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemStartY:F

    .line 945
    sget v3, Lcom/sec/samsung/gallery/glview/GlAbsListView;->DEF_DISTANCE:F

    neg-float v3, v3

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemStartZ:F

    .line 946
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeightSpace:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemGapH:F

    div-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mFullVisibleCnt:I

    .line 947
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemGapH:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mFullVisibleCnt:I

    int-to-float v4, v4

    mul-float v0, v3, v4

    .line 948
    .local v0, "fullVisibleH":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeightSpace:F

    invoke-static {v0, v3}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->isAlmostEquals(FF)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 949
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mFullVisibleCnt:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemVVisibleCnt:I

    .line 953
    :goto_2
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemHVisibleCnt:I

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemVVisibleCnt:I

    mul-int/2addr v3, v4

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemVisibleCnt:I

    .line 954
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemGapH:F

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mScrollVStart:F

    .line 955
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mScrollVisibleH:F

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mScrollVEnd:F

    .line 956
    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mScroll:F

    .line 957
    return-void

    .line 922
    .end local v0    # "fullVisibleH":F
    .end local v2    # "row":I
    :cond_0
    const/high16 v1, 0x42700000    # 60.0f

    goto/16 :goto_0

    .line 924
    :cond_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v4

    if-eqz v4, :cond_2

    move v1, v3

    :goto_3
    goto/16 :goto_0

    :cond_2
    const/high16 v1, 0x420c0000    # 35.0f

    goto :goto_3

    .line 940
    .restart local v2    # "row":I
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlSearchView;->mColCnt:I

    rem-int/2addr v3, v4

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlSearchView;->mColCnt:I

    div-int v2, v3, v4

    :goto_4
    goto :goto_1

    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlSearchView;->mColCnt:I

    div-int/2addr v3, v4

    add-int/lit8 v2, v3, 0x1

    goto :goto_4

    .line 951
    .restart local v0    # "fullVisibleH":F
    :cond_5
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mFullVisibleCnt:I

    add-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemVVisibleCnt:I

    goto :goto_2
.end method

.method public resetSet(IIZ)V
    .locals 8
    .param p1, "startIndex"    # I
    .param p2, "totalCount"    # I
    .param p3, "resetScroll"    # Z

    .prologue
    .line 978
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemHVisibleCnt:I

    add-int/2addr v5, p2

    add-int/lit8 v5, v5, -0x1

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemHVisibleCnt:I

    div-int/2addr v5, v6

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemRowCount:I

    .line 979
    const/4 v0, 0x0

    .line 980
    .local v0, "maxRange":F
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mScrollVisibleH:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemGapH:F

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemRowCount:I

    int-to-float v7, v7

    mul-float/2addr v6, v7

    cmpg-float v5, v5, v6

    if-gez v5, :cond_0

    .line 981
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemGapH:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemRowCount:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mScrollVisibleH:F

    sub-float v0, v5, v6

    .line 986
    :cond_0
    const/4 v3, 0x0

    .line 988
    .local v3, "setScroll":Z
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemHVisibleCnt:I

    div-int v2, p1, v5

    .line 989
    .local v2, "rowIndex":I
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemHVisibleCnt:I

    add-int/2addr v5, p2

    add-int/lit8 v5, v5, -0x1

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemHVisibleCnt:I

    div-int v1, v5, v6

    .line 990
    .local v1, "rowCount":I
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemVVisibleCnt:I

    if-gt v1, v5, :cond_3

    .line 991
    const/4 v2, 0x0

    .line 997
    :cond_1
    :goto_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$500(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    move-result-object v5

    int-to-float v6, v2

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemGapH:F

    mul-float/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 998
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemHVisibleCnt:I

    mul-int v4, v2, v5

    .line 1000
    .local v4, "startRowIndex":I
    const/4 v5, 0x1

    invoke-super {p0, v4, p2, v5}, Lcom/sec/samsung/gallery/glview/GlScroller;->resetSet(IIZ)V

    .line 1001
    if-eqz v3, :cond_2

    .line 1002
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->setScroll(F)V

    .line 1003
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$500(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 1005
    :cond_2
    return-void

    .line 992
    .end local v4    # "startRowIndex":I
    :cond_3
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mFullVisibleCnt:I

    add-int/2addr v5, v2

    if-lt v5, v1, :cond_1

    .line 994
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemGapH:F

    div-float v5, v0, v5

    float-to-int v2, v5

    .line 995
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public setThumbPosition(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 1
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    .line 1009
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/GlScroller;->setThumbPosition(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 1011
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mPitchFact:F
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$400(Lcom/sec/samsung/gallery/glview/GlSearchView;)F

    move-result v0

    invoke-virtual {p1, v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPitch(F)V

    .line 1012
    return-void
.end method

.method public update(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 7
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    .line 963
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mViewMode:I
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$1900(Lcom/sec/samsung/gallery/glview/GlSearchView;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewThumbnailWidthPixel(I)I

    move-result v1

    .line 964
    .local v1, "canvasWidth":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mViewMode:I
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$1900(Lcom/sec/samsung/gallery/glview/GlSearchView;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewThumbnailHeightPixel(I)I

    move-result v0

    .line 966
    .local v0, "canvasHeight":I
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->getWidth()I

    move-result v3

    if-ne v3, v1, :cond_0

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->getHeight()I

    move-result v3

    if-eq v3, v0, :cond_1

    .line 967
    :cond_0
    new-instance v3, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v4, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v3, v4, v1, v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {p1, v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 970
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v3, :cond_2

    iget v3, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-ltz v3, :cond_2

    iget v3, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->mItemCount:I

    if-ge v3, v4, :cond_2

    .line 971
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget v4, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlSearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    .line 972
    .local v2, "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {p1, v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 974
    .end local v2    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_2
    return-void
.end method
