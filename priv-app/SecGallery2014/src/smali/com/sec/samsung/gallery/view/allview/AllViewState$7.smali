.class Lcom/sec/samsung/gallery/view/allview/AllViewState$7;
.super Ljava/lang/Object;
.source "AllViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/allview/AllViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V
    .locals 0

    .prologue
    .line 398
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$7;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;I)V
    .locals 1
    .param p1, "parentView"    # Lcom/sec/samsung/gallery/glview/GlAbsListView;
    .param p2, "view"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "position"    # I

    .prologue
    .line 401
    if-gez p3, :cond_0

    .line 410
    :goto_0
    return-void

    .line 404
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$7;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # setter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mCurrentMediaItemIndex:I
    invoke-static {v0, p3}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$2402(Lcom/sec/samsung/gallery/view/allview/AllViewState;I)I

    .line 405
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$7;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$2000(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 406
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$7;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-virtual {v0, p3}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->selectMedia(I)V

    goto :goto_0

    .line 408
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$7;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # invokes: Lcom/sec/samsung/gallery/view/allview/AllViewState;->startDetailView(I)V
    invoke-static {v0, p3}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$2500(Lcom/sec/samsung/gallery/view/allview/AllViewState;I)V

    goto :goto_0
.end method
