.class public Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarView;
.source "PhotoActionBarForPick.java"


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick$1;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method private setTextOnlyButton(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 83
    const v2, 0x7f0f0287

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 84
    .local v1, "hidden_album":Landroid/view/MenuItem;
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->setTextOnlyButton(Landroid/view/MenuItem;)V

    .line 86
    const v2, 0x7f0f0288

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 87
    .local v0, "cancel":Landroid/view/MenuItem;
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->setTextOnlyButton(Landroid/view/MenuItem;)V

    .line 88
    return-void
.end method


# virtual methods
.method protected isPickMode()Z
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 56
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 57
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f120031

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 59
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->setTextOnlyButton(Landroid/view/Menu;)V

    .line 60
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 71
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 80
    :goto_0
    return-void

    .line 73
    :pswitch_0
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    sget v1, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_HIDDEN_ALBUMS:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 76
    :pswitch_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->setResult(I)V

    .line 77
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->finish()V

    goto :goto_0

    .line 71
    :pswitch_data_0
    .packed-switch 0x7f0f0287
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 64
    const v2, 0x7f0f02d0

    invoke-interface {p1, v2, v0}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 65
    const v2, 0x7f0f0287

    invoke-static {p1, v2, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 66
    const v2, 0x7f0f0288

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    invoke-static {p1, v2, v0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 67
    return-void

    :cond_0
    move v0, v1

    .line 66
    goto :goto_0
.end method

.method protected setTitle(Ljava/lang/String;)V
    .locals 2
    .param p1, "albumName"    # Ljava/lang/String;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick$2;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick$2;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 108
    return-void
.end method

.method public setTitle(Ljava/lang/String;I)V
    .locals 2
    .param p1, "albumName"    # Ljava/lang/String;
    .param p2, "numberOfItemsInAlbum"    # I

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick$3;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 125
    return-void
.end method

.method protected setupButtons()V
    .locals 1

    .prologue
    .line 49
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->showHomeButtonOnTopLeft()V

    .line 52
    :cond_0
    return-void
.end method
