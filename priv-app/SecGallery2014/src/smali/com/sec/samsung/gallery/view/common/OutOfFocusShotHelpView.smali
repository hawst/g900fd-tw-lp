.class public Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;
.super Ljava/lang/Object;
.source "OutOfFocusShotHelpView.java"


# static fields
.field public static final FOCUS_HELP_PARAM_KEY:Ljava/lang/String; = "focus_is_show"


# instance fields
.field private mCloseBtn:Landroid/widget/ImageButton;

.field private mCloseClickListener:Landroid/view/View$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

.field private mImageView:Landroid/widget/ImageView;

.field private mIsShowingHelpView:Z

.field private mMainView:Landroid/view/ViewGroup;

.field private mOkBtn:Landroid/widget/TextView;

.field private mOutOfFocusShotHelpView:Landroid/widget/RelativeLayout;

.field private mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

.field private mResource:Landroid/content/res/Resources;

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/PositionController;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "positionController"    # Lcom/sec/android/gallery3d/ui/PositionController;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mIsShowingHelpView:Z

    .line 155
    new-instance v0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$4;-><init>(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mCloseClickListener:Landroid/view/View$OnClickListener;

    .line 42
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mContext:Landroid/content/Context;

    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mResource:Landroid/content/res/Resources;

    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const v1, 0x7f0f0175

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mMainView:Landroid/view/ViewGroup;

    .line 45
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 46
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    .line 47
    return-void
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mIsShowingHelpView:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mOutOfFocusShotHelpView:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;
    .param p1, "x1"    # Landroid/widget/RelativeLayout;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mOutOfFocusShotHelpView:Landroid/widget/RelativeLayout;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mMainView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->initializeView()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->setHelpState(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->bringToFront()V

    return-void
.end method

.method private bringToFront()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mOutOfFocusShotHelpView:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    .line 76
    :goto_0
    return-void

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mMainView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mOutOfFocusShotHelpView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->bringChildToFront(Landroid/view/View;)V

    goto :goto_0
.end method

.method private initializeView()V
    .locals 10

    .prologue
    .line 102
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeightPixel()I

    move-result v0

    .line 103
    .local v0, "actionBarHeight":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v5

    .line 104
    .local v5, "imageBounds":Landroid/graphics/Rect;
    iget v6, v5, Landroid/graphics/Rect;->left:I

    .line 105
    .local v6, "left":I
    iget v7, v5, Landroid/graphics/Rect;->top:I

    .line 106
    .local v7, "top":I
    const/4 v1, 0x0

    .line 107
    .local v1, "bodyLeftMargin":I
    const/4 v3, 0x0

    .line 109
    .local v3, "bodyTopMargin":I
    if-gtz v6, :cond_0

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mPositionController:Lcom/sec/android/gallery3d/ui/PositionController;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/PositionController;->isAtMinimalScale()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 110
    :cond_0
    add-int/2addr v1, v6

    .line 113
    :cond_1
    if-ge v7, v0, :cond_3

    .line 114
    add-int/2addr v3, v0

    .line 120
    :goto_0
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewOverlayHelp:Z

    if-eqz v8, :cond_5

    .line 121
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mContext:Landroid/content/Context;

    check-cast v8, Landroid/app/Activity;

    const v9, 0x7f0f0111

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mOkBtn:Landroid/widget/TextView;

    .line 122
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mOkBtn:Landroid/widget/TextView;

    if-nez v8, :cond_4

    .line 153
    :cond_2
    :goto_1
    return-void

    .line 116
    :cond_3
    add-int/2addr v3, v7

    goto :goto_0

    .line 124
    :cond_4
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mOkBtn:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mCloseClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    :goto_2
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mOutOfFocusShotHelpView:Landroid/widget/RelativeLayout;

    const v9, 0x7f0f010f

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mImageView:Landroid/widget/ImageView;

    .line 138
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mImageView:Landroid/widget/ImageView;

    if-eqz v8, :cond_2

    .line 141
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 142
    .local v2, "bodyParams":Landroid/widget/RelativeLayout$LayoutParams;
    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 143
    iput v1, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 144
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v8, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 146
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mOutOfFocusShotHelpView:Landroid/widget/RelativeLayout;

    new-instance v9, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$3;

    invoke-direct {v9, p0}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$3;-><init>(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;)V

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_1

    .line 126
    .end local v2    # "bodyParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_5
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mContext:Landroid/content/Context;

    check-cast v8, Landroid/app/Activity;

    const v9, 0x7f0f010e

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    iput-object v8, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mCloseBtn:Landroid/widget/ImageButton;

    .line 127
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mCloseBtn:Landroid/widget/ImageButton;

    if-eqz v8, :cond_2

    .line 129
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mCloseBtn:Landroid/widget/ImageButton;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mCloseClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mCloseBtn:Landroid/widget/ImageButton;

    invoke-virtual {v8}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 132
    .local v4, "closeBtnParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mContext:Landroid/content/Context;

    check-cast v8, Landroid/app/Activity;

    invoke-virtual {v8}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/ActionBar;->getHeight()I

    move-result v8

    iput v8, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 133
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mCloseBtn:Landroid/widget/ImageButton;

    invoke-virtual {v8, v4}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2
.end method

.method private isShowHelpPopup()Z
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mContext:Landroid/content/Context;

    const-string v1, "focus_is_show"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private setHelpState(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mContext:Landroid/content/Context;

    const-string v1, "focus_is_show"

    invoke-static {v0, v1, p1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 84
    return-void
.end method


# virtual methods
.method public addView()V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->isShowHelpPopup()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mIsShowingHelpView:Z

    if-nez v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$1;-><init>(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 69
    :cond_0
    return-void
.end method

.method public isShowingHelpView()Z
    .locals 1

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mIsShowingHelpView:Z

    return v0
.end method

.method public removeView()V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mOutOfFocusShotHelpView:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_1

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$2;-><init>(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public resetLayout()V
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mOutOfFocusShotHelpView:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    .line 177
    :goto_0
    return-void

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$5;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$5;-><init>(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
