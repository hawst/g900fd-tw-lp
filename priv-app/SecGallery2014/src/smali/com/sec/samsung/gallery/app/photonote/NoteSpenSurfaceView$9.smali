.class Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$9;
.super Ljava/lang/Object;
.source "NoteSpenSurfaceView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)V
    .locals 0

    .prologue
    .line 624
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$9;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 5
    .param p1, "removerInfo"    # Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .prologue
    .line 629
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$9;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    const-string v3, "SettingInfo"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 631
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 632
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v2, "mLastRemoverSize"

    iget v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 633
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 635
    return-void
.end method
