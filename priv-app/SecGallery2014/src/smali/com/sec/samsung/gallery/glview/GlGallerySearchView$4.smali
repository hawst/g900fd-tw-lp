.class Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;
.super Ljava/lang/Object;
.source "GlGallerySearchView.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGallerySearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V
    .locals 0

    .prologue
    .line 407
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFlingEnd(F)V
    .locals 4
    .param p1, "delta"    # F

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 431
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$500(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)Lcom/sec/samsung/gallery/glview/GlBlurObject;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 432
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$500(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)Lcom/sec/samsung/gallery/glview/GlBlurObject;

    move-result-object v0

    invoke-virtual {v0, v3, v2}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->show(IF)V

    .line 433
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$500(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)Lcom/sec/samsung/gallery/glview/GlBlurObject;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->show(IF)V

    .line 435
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iput p1, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mSx:F

    .line 436
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mPitchFact:F
    invoke-static {v0, v2}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$302(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;F)F

    .line 437
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v0, v2, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setThumbnailPitchRate(FLcom/sec/samsung/gallery/glview/GlScroller;)V

    .line 438
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mSx:F

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->setScroll(F)V

    .line 439
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3, v3, v3}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setCommand(IIII)V

    .line 440
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlScroller;->hideScrollBar()V

    .line 441
    return-void
.end method

.method public onFlingProcess(FF)V
    .locals 6
    .param p1, "delta"    # F
    .param p2, "elastic"    # F

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x41f00000    # 30.0f

    const/high16 v3, -0x3e100000    # -30.0f

    const/4 v2, 0x0

    .line 410
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iput p1, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mSx:F

    .line 411
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mSx:F

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->setScroll(F)V

    .line 412
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mUsePitchBoundary:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$200(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 413
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$400(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->getSpeedRatio()F

    move-result v1

    neg-float v1, v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    # setter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mPitchFact:F
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$302(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;F)F

    .line 414
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mPitchFact:F
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$300(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)F

    move-result v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_1

    .line 415
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mPitchFact:F
    invoke-static {v0, v4}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$302(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;F)F

    .line 418
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    neg-float v1, p2

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setThumbnailPitchRate(FLcom/sec/samsung/gallery/glview/GlScroller;)V

    .line 427
    :goto_1
    return-void

    .line 416
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mPitchFact:F
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$300(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)F

    move-result v0

    cmpg-float v0, v0, v3

    if-gez v0, :cond_0

    .line 417
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mPitchFact:F
    invoke-static {v0, v3}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$302(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;F)F

    goto :goto_0

    .line 420
    :cond_2
    cmpl-float v0, p2, v2

    if-lez v0, :cond_3

    .line 421
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$500(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)Lcom/sec/samsung/gallery/glview/GlBlurObject;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p2}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->show(IF)V

    goto :goto_1

    .line 422
    :cond_3
    cmpg-float v0, p2, v2

    if-gez v0, :cond_4

    .line 423
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$500(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)Lcom/sec/samsung/gallery/glview/GlBlurObject;

    move-result-object v0

    invoke-virtual {v0, v5, p2}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->show(IF)V

    goto :goto_1

    .line 425
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$500(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)Lcom/sec/samsung/gallery/glview/GlBlurObject;

    move-result-object v0

    invoke-virtual {v0, v5, v2}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->show(IF)V

    goto :goto_1
.end method
