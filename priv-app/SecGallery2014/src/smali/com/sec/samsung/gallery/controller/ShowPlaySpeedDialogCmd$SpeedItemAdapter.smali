.class Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;
.super Landroid/widget/BaseAdapter;
.source "ShowPlaySpeedDialogCmd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SpeedItemAdapter"
.end annotation


# instance fields
.field inflater:Landroid/view/LayoutInflater;

.field mContext:Landroid/content/Context;

.field mFocused:I

.field mItemPool:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItem;",
            ">;"
        }
    .end annotation
.end field

.field mLayout:I

.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layout"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p4, "itemPool":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItem;>;"
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;->this$0:Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 89
    iput-object p2, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;->mContext:Landroid/content/Context;

    .line 90
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 91
    iput p3, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;->mLayout:I

    .line 92
    iput-object p4, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;->mItemPool:Ljava/util/ArrayList;

    .line 93
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;->mItemPool:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;->mItemPool:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 104
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 113
    if-nez p2, :cond_0

    .line 114
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;->inflater:Landroid/view/LayoutInflater;

    const v7, 0x7f0300d7

    invoke-virtual {v4, v7, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 119
    .local v3, "view":Landroid/view/View;
    :goto_0
    const v4, 0x7f0f0216

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckedTextView;

    .line 120
    .local v2, "text":Landroid/widget/CheckedTextView;
    const v4, 0x7f0f0213

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 122
    .local v0, "icon":Landroid/widget/ImageView;
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;->mItemPool:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItem;

    .line 123
    .local v1, "si":Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItem;
    iget v4, v1, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItem;->name:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/CheckedTextView;->setText(I)V

    .line 124
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getPlaySpeed(Landroid/content/Context;)I

    move-result v4

    if-ne v4, p1, :cond_1

    move v4, v5

    :goto_1
    invoke-virtual {v2, v4}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 125
    iget v4, v1, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItem;->icon:I

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 126
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f0e01ba

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    iget v8, v1, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItem;->ttsName:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 129
    return-object v3

    .line 116
    .end local v0    # "icon":Landroid/widget/ImageView;
    .end local v1    # "si":Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItem;
    .end local v2    # "text":Landroid/widget/CheckedTextView;
    .end local v3    # "view":Landroid/view/View;
    :cond_0
    move-object v3, p2

    .restart local v3    # "view":Landroid/view/View;
    goto :goto_0

    .restart local v0    # "icon":Landroid/widget/ImageView;
    .restart local v1    # "si":Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItem;
    .restart local v2    # "text":Landroid/widget/CheckedTextView;
    :cond_1
    move v4, v6

    .line 124
    goto :goto_1
.end method
