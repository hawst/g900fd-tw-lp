.class public Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "ShowCopyMoveDialogCmd.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Ljava/util/Observer;
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field private static final CLOUD_TO_CLOUD:I = 0x4

.field private static final CLOUD_TO_LOCAL:I = 0x3

.field private static final EXTERNAL_SD_DIRECTORY:Ljava/lang/String;

.field private static final LOCAL_TO_CLOUD:I = 0x2

.field private static final LOCAL_TO_LOCAL:I = 0x1

.field private static final MIXED_OR_UNKNOWN:I = 0x0

.field public static final OP_COPY:I = 0x0

.field public static final OP_COPY_TO_ALBUM:I = 0x5

.field public static final OP_DRAG_MOVE:I = 0x2

.field public static final OP_MOVE:I = 0x1

.field public static final OP_MOVE_TO_ALBUM:I = 0x6

.field public static final OP_REMOVE_KNOX:I = 0x4

.field public static final OP_REMOVE_SECRETBOX:I = 0x3

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAlbumListDialog:Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;

.field private mContext:Landroid/content/Context;

.field private mCopyMoveChioceDialog:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

.field private mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

.field private mLocalCloudCopyChioceDialog:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

.field private mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

.field private mOperationTask:Lcom/sec/samsung/gallery/util/MediaOperations;

.field private mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

.field private mResources:Landroid/content/res/Resources;

.field private mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

.field private mTopSetPath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    const-class v0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->TAG:Ljava/lang/String;

    .line 96
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->EXTERNAL_SD_DIRECTORY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mOperationTask:Lcom/sec/samsung/gallery/util/MediaOperations;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->handleMultipleFileOperation(Ljava/lang/String;IILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;Ljava/io/File;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;
    .param p1, "x1"    # Ljava/io/File;
    .param p2, "x2"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->showRenameDialog(Ljava/io/File;I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Ljava/util/LinkedList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->getSrcMediaLists()Ljava/util/LinkedList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;Ljava/util/LinkedList;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;
    .param p1, "x1"    # Ljava/util/LinkedList;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->doUpload(Ljava/util/LinkedList;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;Ljava/util/LinkedList;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;
    .param p1, "x1"    # Ljava/util/LinkedList;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->doDownload(Ljava/util/LinkedList;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Lcom/sec/samsung/gallery/util/FileUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;Lcom/sec/samsung/gallery/util/FileUtil;)Lcom/sec/samsung/gallery/util/FileUtil;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/util/FileUtil;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    return-object p1
.end method

.method static synthetic access$402(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;Lcom/sec/android/gallery3d/data/OnProgressListener;)Lcom/sec/android/gallery3d/data/OnProgressListener;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/OnProgressListener;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    return-object p1
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->EXTERNAL_SD_DIRECTORY:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Lcom/sec/samsung/gallery/util/MediaOperations;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mOperationTask:Lcom/sec/samsung/gallery/util/MediaOperations;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;Lcom/sec/samsung/gallery/util/MediaOperations;)Lcom/sec/samsung/gallery/util/MediaOperations;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/util/MediaOperations;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mOperationTask:Lcom/sec/samsung/gallery/util/MediaOperations;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/res/Resources;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->handleFilesOperation(Ljava/lang/String;I)V

    return-void
.end method

.method private checkLocalCloudCopy(Lcom/sec/android/gallery3d/data/MediaSet;)I
    .locals 15
    .param p1, "dstMediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 562
    const/4 v12, 0x0

    .line 564
    .local v12, "result":I
    const/4 v3, 0x0

    .line 565
    .local v3, "LOCAL":I
    const/4 v0, 0x1

    .line 566
    .local v0, "CLOUD":I
    const/4 v4, 0x2

    .line 569
    .local v4, "MIXED":I
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->getSrcMediaLists()Ljava/util/LinkedList;

    move-result-object v8

    .line 571
    .local v8, "mediaLists":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v6, 0x0

    .line 572
    .local v6, "bHaveLocal":Z
    const/4 v5, 0x0

    .line 574
    .local v5, "bHaveCloud":Z
    invoke-virtual {v8}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 575
    .local v9, "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v14, v9, Lcom/sec/android/gallery3d/data/LocalImage;

    if-nez v14, :cond_1

    instance-of v14, v9, Lcom/sec/android/gallery3d/data/LocalVideo;

    if-eqz v14, :cond_2

    .line 576
    :cond_1
    const/4 v6, 0x1

    goto :goto_0

    .line 578
    :cond_2
    instance-of v14, v9, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-nez v14, :cond_3

    instance-of v14, v9, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-eqz v14, :cond_0

    .line 579
    :cond_3
    const/4 v5, 0x1

    goto :goto_0

    .line 583
    .end local v9    # "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_4
    const/4 v2, 0x2

    .line 584
    .local v2, "CheckSrcLocalOrCloud":I
    if-eqz v6, :cond_6

    .line 585
    if-nez v5, :cond_5

    .line 586
    const/4 v2, 0x0

    .line 594
    :cond_5
    :goto_1
    const/4 v14, 0x2

    if-ne v2, v14, :cond_7

    move v13, v12

    .line 626
    .end local v12    # "result":I
    .local v13, "result":I
    :goto_2
    return v13

    .line 589
    .end local v13    # "result":I
    .restart local v12    # "result":I
    :cond_6
    if-eqz v5, :cond_5

    .line 590
    if-nez v6, :cond_5

    .line 591
    const/4 v2, 0x1

    goto :goto_1

    .line 599
    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v10

    .line 600
    .local v10, "path":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v11

    .line 602
    .local v11, "pathOnFileSystem":Ljava/lang/String;
    const/4 v1, 0x2

    .line 603
    .local v1, "CheckDstLocalOrCloud":I
    const-string v14, "/local"

    invoke-virtual {v10, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_9

    const-string v14, "/storage"

    invoke-virtual {v11, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 604
    const/4 v1, 0x0

    .line 609
    :cond_8
    :goto_3
    const/4 v14, 0x2

    if-ne v1, v14, :cond_a

    move v13, v12

    .line 610
    .end local v12    # "result":I
    .restart local v13    # "result":I
    goto :goto_2

    .line 606
    .end local v13    # "result":I
    .restart local v12    # "result":I
    :cond_9
    const-string v14, "/cloud"

    invoke-virtual {v10, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_8

    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_8

    .line 607
    const/4 v1, 0x1

    goto :goto_3

    .line 613
    :cond_a
    if-nez v2, :cond_c

    if-nez v1, :cond_c

    .line 614
    const/4 v12, 0x1

    :cond_b
    :goto_4
    move v13, v12

    .line 626
    .end local v12    # "result":I
    .restart local v13    # "result":I
    goto :goto_2

    .line 616
    .end local v13    # "result":I
    .restart local v12    # "result":I
    :cond_c
    if-nez v2, :cond_d

    const/4 v14, 0x1

    if-ne v1, v14, :cond_d

    .line 617
    const/4 v12, 0x2

    goto :goto_4

    .line 619
    :cond_d
    const/4 v14, 0x1

    if-ne v2, v14, :cond_e

    if-nez v1, :cond_e

    .line 620
    const/4 v12, 0x3

    goto :goto_4

    .line 622
    :cond_e
    const/4 v14, 0x1

    if-ne v2, v14, :cond_b

    const/4 v14, 0x1

    if-ne v1, v14, :cond_b

    .line 623
    const/4 v12, 0x4

    goto :goto_4
.end method

.method private dismissAlbumListDialog()V
    .locals 1

    .prologue
    .line 508
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mAlbumListDialog:Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;

    if-eqz v0, :cond_0

    .line 509
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mAlbumListDialog:Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->dismissDialog()V

    .line 510
    :cond_0
    return-void
.end method

.method private dismissCopyMoveChioseDialog()V
    .locals 1

    .prologue
    .line 556
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mCopyMoveChioceDialog:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    if-eqz v0, :cond_0

    .line 557
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mCopyMoveChioceDialog:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->dismissDialog()V

    .line 558
    :cond_0
    return-void
.end method

.method private dismissmLocalCloudCopyChioceDialog()V
    .locals 1

    .prologue
    .line 660
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mLocalCloudCopyChioceDialog:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    if-eqz v0, :cond_0

    .line 661
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mLocalCloudCopyChioceDialog:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->dismissDialog()V

    .line 663
    :cond_0
    return-void
.end method

.method private doDownload(Ljava/util/LinkedList;Ljava/lang/String;)V
    .locals 9
    .param p2, "dstAlbumPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 760
    .local p1, "srcMediaLists":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v4, Landroid/content/Intent;

    const-string v7, "com.baidu.xcloud.netdisk.ACITON_DOWNLOAD_FILES"

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 761
    .local v4, "intent":Landroid/content/Intent;
    const-string v7, "downloaddir"

    invoke-virtual {v4, v7, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 763
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 764
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    invoke-virtual {p1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 765
    .local v5, "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v7, v5, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-eqz v7, :cond_1

    move-object v6, v5

    .line 766
    check-cast v6, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    .line 767
    .local v6, "objCloud":Lcom/sec/android/gallery3d/remote/cloud/CloudImage;
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->getFilePath()Ljava/lang/String;

    move-result-object v2

    .line 768
    .local v2, "filepath":Ljava/lang/String;
    const-string v7, "/storage/emulated/0/cloudagent/cache/root/"

    const-string v8, "/"

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 770
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 771
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v7, "path"

    invoke-virtual {v1, v7, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 773
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "filepath":Ljava/lang/String;
    .end local v6    # "objCloud":Lcom/sec/android/gallery3d/remote/cloud/CloudImage;
    :cond_1
    instance-of v7, v5, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-eqz v7, :cond_0

    move-object v6, v5

    .line 774
    check-cast v6, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    .line 775
    .local v6, "objCloud":Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;->getFilePath()Ljava/lang/String;

    move-result-object v2

    .line 776
    .restart local v2    # "filepath":Ljava/lang/String;
    const-string v7, "/storage/emulated/0/cloudagent/cache/root/"

    const-string v8, "/"

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 778
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 779
    .restart local v1    # "bundle":Landroid/os/Bundle;
    const-string v7, "path"

    invoke-virtual {v1, v7, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 780
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 784
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "filepath":Ljava/lang/String;
    .end local v5    # "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v6    # "objCloud":Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;
    :cond_2
    const-string v7, "downloadfiles"

    invoke-virtual {v4, v7, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 785
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 786
    return-void
.end method

.method private doUpload(Ljava/util/LinkedList;Ljava/lang/String;)V
    .locals 8
    .param p2, "dstAlbumPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 737
    .local p1, "srcMediaLists":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v3, Landroid/content/Intent;

    const-string v7, "com.baidu.xcloud.netdisk.ACTION_UPLOAD_FILES"

    invoke-direct {v3, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 738
    .local v3, "intent":Landroid/content/Intent;
    const-string/jumbo v7, "uploaddir"

    invoke-virtual {v3, v7, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 740
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 741
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {p1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 742
    .local v4, "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v7, v4, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v7, :cond_1

    move-object v5, v4

    .line 743
    check-cast v5, Lcom/sec/android/gallery3d/data/LocalImage;

    .line 744
    .local v5, "objLocal":Lcom/sec/android/gallery3d/data/LocalImage;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/LocalImage;->getFilePath()Ljava/lang/String;

    move-result-object v1

    .line 745
    .local v1, "filepath":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v6

    .line 746
    .local v6, "uri":Landroid/net/Uri;
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 747
    .end local v1    # "filepath":Ljava/lang/String;
    .end local v5    # "objLocal":Lcom/sec/android/gallery3d/data/LocalImage;
    .end local v6    # "uri":Landroid/net/Uri;
    :cond_1
    instance-of v7, v4, Lcom/sec/android/gallery3d/data/LocalVideo;

    if-eqz v7, :cond_0

    move-object v5, v4

    .line 748
    check-cast v5, Lcom/sec/android/gallery3d/data/LocalVideo;

    .line 749
    .local v5, "objLocal":Lcom/sec/android/gallery3d/data/LocalVideo;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/LocalVideo;->getFilePath()Ljava/lang/String;

    move-result-object v1

    .line 750
    .restart local v1    # "filepath":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v6

    .line 751
    .restart local v6    # "uri":Landroid/net/Uri;
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 755
    .end local v1    # "filepath":Ljava/lang/String;
    .end local v4    # "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v5    # "objLocal":Lcom/sec/android/gallery3d/data/LocalVideo;
    .end local v6    # "uri":Landroid/net/Uri;
    :cond_2
    const-string/jumbo v7, "uploadfiles"

    invoke-virtual {v3, v7, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 756
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 757
    return-void
.end method

.method private getDstCloudPath(Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/String;
    .locals 10
    .param p1, "dstMediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 687
    const-string v5, ""

    .line 688
    .local v5, "strResult":Ljava/lang/String;
    instance-of v8, p1, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;

    if-eqz v8, :cond_2

    .line 689
    check-cast p1, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;

    .end local p1    # "dstMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;->getCoverMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    .line 690
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    const-string v7, "root/"

    .line 692
    .local v7, "strStart":Ljava/lang/String;
    instance-of v8, v1, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-eqz v8, :cond_3

    .line 693
    check-cast v1, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->getFilePath()Ljava/lang/String;

    move-result-object v4

    .line 694
    .local v4, "strPath":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 695
    const/4 v0, 0x0

    .line 696
    .local v0, "end":I
    const-string v8, "root/"

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 697
    .local v2, "nIndexRoot":I
    if-ltz v2, :cond_0

    .line 698
    const-string v8, "root/"

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    const-string v9, "root/"

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    add-int/lit8 v3, v8, -0x1

    .line 699
    .local v3, "start":I
    const-string v8, "/"

    invoke-virtual {v4, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 705
    :goto_0
    if-lez v3, :cond_1

    if-lt v3, v0, :cond_1

    .line 706
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v6, v5

    .line 733
    .end local v0    # "end":I
    .end local v2    # "nIndexRoot":I
    .end local v3    # "start":I
    .end local v4    # "strPath":Ljava/lang/String;
    .end local v5    # "strResult":Ljava/lang/String;
    .end local v7    # "strStart":Ljava/lang/String;
    .local v6, "strResult":Ljava/lang/String;
    :goto_1
    return-object v6

    .line 702
    .end local v6    # "strResult":Ljava/lang/String;
    .restart local v0    # "end":I
    .restart local v2    # "nIndexRoot":I
    .restart local v4    # "strPath":Ljava/lang/String;
    .restart local v5    # "strResult":Ljava/lang/String;
    .restart local v7    # "strStart":Ljava/lang/String;
    :cond_0
    const/4 v3, 0x0

    .line 703
    .restart local v3    # "start":I
    const-string v8, "/"

    invoke-virtual {v4, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 709
    :cond_1
    invoke-virtual {v4, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .end local v0    # "end":I
    .end local v2    # "nIndexRoot":I
    .end local v3    # "start":I
    .end local v4    # "strPath":Ljava/lang/String;
    .end local v7    # "strStart":Ljava/lang/String;
    :cond_2
    :goto_2
    move-object v6, v5

    .line 733
    .end local v5    # "strResult":Ljava/lang/String;
    .restart local v6    # "strResult":Ljava/lang/String;
    goto :goto_1

    .line 711
    .end local v6    # "strResult":Ljava/lang/String;
    .restart local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v5    # "strResult":Ljava/lang/String;
    .restart local v7    # "strStart":Ljava/lang/String;
    :cond_3
    instance-of v8, v1, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-eqz v8, :cond_2

    .line 712
    check-cast v1, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;->getFilePath()Ljava/lang/String;

    move-result-object v4

    .line 713
    .restart local v4    # "strPath":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 714
    const/4 v0, 0x0

    .line 715
    .restart local v0    # "end":I
    const-string v8, "root/"

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 716
    .restart local v2    # "nIndexRoot":I
    if-ltz v2, :cond_4

    .line 717
    const-string v8, "root/"

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    const-string v9, "root/"

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    add-int/lit8 v3, v8, -0x1

    .line 718
    .restart local v3    # "start":I
    const-string v8, "/"

    invoke-virtual {v4, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 724
    :goto_3
    if-ne v3, v0, :cond_5

    .line 725
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v6, v5

    .line 726
    .end local v5    # "strResult":Ljava/lang/String;
    .restart local v6    # "strResult":Ljava/lang/String;
    goto :goto_1

    .line 721
    .end local v3    # "start":I
    .end local v6    # "strResult":Ljava/lang/String;
    .restart local v5    # "strResult":Ljava/lang/String;
    :cond_4
    const/4 v3, 0x0

    .line 722
    .restart local v3    # "start":I
    const-string v8, "/"

    invoke-virtual {v4, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    goto :goto_3

    .line 728
    :cond_5
    invoke-virtual {v4, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    goto :goto_2
.end method

.method private getSrcMediaLists()Ljava/util/LinkedList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 666
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    .line 667
    .local v1, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v0

    .line 668
    .local v0, "mediaLists":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    return-object v0
.end method

.method private handleFilesOperation(Ljava/lang/String;I)V
    .locals 10
    .param p1, "dstAlbumPath"    # Ljava/lang/String;
    .param p2, "operationId"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 216
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    check-cast v6, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v5

    .line 218
    .local v5, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v5, v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v4

    .line 220
    .local v4, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v6, v4, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    if-eqz v6, :cond_1

    .line 221
    if-ne p2, v1, :cond_0

    .line 223
    .local v1, "deleteAfterDownload":Z
    :goto_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    invoke-virtual {v6, p1, v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->getModalDownloadIntent(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    .line 225
    .local v3, "intent":Landroid/content/Intent;
    :try_start_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 247
    .end local v1    # "deleteAfterDownload":Z
    .end local v3    # "intent":Landroid/content/Intent;
    :goto_1
    return-void

    :cond_0
    move v1, v7

    .line 221
    goto :goto_0

    .line 226
    .restart local v1    # "deleteAfterDownload":Z
    .restart local v3    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v2

    .line 227
    .local v2, "e":Landroid/content/ActivityNotFoundException;
    sget-object v6, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->TAG:Ljava/lang/String;

    const-string v7, "Activity Not Found"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    invoke-virtual {v2}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 233
    .end local v1    # "deleteAfterDownload":Z
    .end local v2    # "e":Landroid/content/ActivityNotFoundException;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_1
    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->isInCopyOrMove(Lcom/sec/android/gallery3d/ui/SelectionManager;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_3

    .line 234
    :cond_2
    sget-object v6, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "check dstAlbumPath size : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    const v8, 0x7f0e0107

    invoke-static {v6, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 236
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    check-cast v6, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v6

    const-string v8, "EXIT_SELECTION_MODE"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v8, v7}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 241
    :cond_3
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 242
    .local v0, "count":I
    if-ne v0, v1, :cond_4

    .line 243
    invoke-direct {p0, v5, p1, p2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->handleSingleFileOperation(Lcom/sec/android/gallery3d/ui/SelectionManager;Ljava/lang/String;I)V

    goto :goto_1

    .line 245
    :cond_4
    const/4 v6, 0x0

    invoke-direct {p0, p1, p2, v0, v6}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->handleMultipleFileOperation(Ljava/lang/String;IILjava/lang/String;)V

    goto :goto_1
.end method

.method private handleMultipleFileOperation(Ljava/lang/String;IILjava/lang/String;)V
    .locals 9
    .param p1, "dstAlbumPath"    # Ljava/lang/String;
    .param p2, "operationId"    # I
    .param p3, "count"    # I
    .param p4, "newFileName"    # Ljava/lang/String;

    .prologue
    const v8, 0x7f0e00ca

    const/4 v7, 0x0

    const v6, 0x7f0e01aa

    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 311
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v5

    .line 312
    .local v5, "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDialogWithMessage:Z

    if-eqz v0, :cond_5

    .line 313
    if-ne p2, v3, :cond_1

    .line 314
    new-instance v0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    .line 321
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    invoke-virtual {v0, p3}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalCount(I)V

    .line 323
    if-ne p2, v3, :cond_3

    .line 324
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3, p0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->showProgressDialog(Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 342
    :goto_1
    new-instance v0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;

    move-object v1, p0

    move v2, p2

    move-object v3, p1

    move v4, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;-><init>(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;ILjava/lang/String;ILcom/sec/android/gallery3d/data/DataManager;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    .line 480
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    invoke-virtual {v0, v7, v1}, Lcom/sec/samsung/gallery/util/FileUtil;->operateMedias(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/OnProgressListener;)Lcom/sec/samsung/gallery/util/MediaOperations;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mOperationTask:Lcom/sec/samsung/gallery/util/MediaOperations;

    .line 482
    :cond_0
    return-void

    .line 315
    :cond_1
    if-ne p2, v4, :cond_2

    .line 316
    new-instance v0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    goto :goto_0

    .line 318
    :cond_2
    new-instance v0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    goto :goto_0

    .line 325
    :cond_3
    if-ne p2, v4, :cond_4

    .line 326
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    const v2, 0x7f0e01e2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3, p0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->showProgressDialog(Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_1

    .line 328
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    const v2, 0x7f0e01ac

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3, p0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->showProgressDialog(Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_1

    .line 331
    :cond_5
    new-instance v0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    .line 332
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    invoke-virtual {v0, p3}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalCount(I)V

    .line 334
    if-ne p2, v3, :cond_6

    .line 335
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v7, v3, p0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->showProgressDialog(Ljava/lang/String;Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_1

    .line 336
    :cond_6
    if-ne p2, v4, :cond_7

    .line 337
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    const v2, 0x7f0e01e2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3, p0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->showProgressDialog(Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    goto/16 :goto_1

    .line 339
    :cond_7
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    const v2, 0x7f0e01ac

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v7, v3, p0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->showProgressDialog(Ljava/lang/String;Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    goto/16 :goto_1
.end method

.method private handleSingleFileOperation(Lcom/sec/android/gallery3d/ui/SelectionManager;Ljava/lang/String;I)V
    .locals 15
    .param p1, "selectionModeProxy"    # Lcom/sec/android/gallery3d/ui/SelectionManager;
    .param p2, "dstAlbumPath"    # Ljava/lang/String;
    .param p3, "operationId"    # I

    .prologue
    .line 271
    const/4 v7, 0x0

    .line 272
    .local v7, "showConfirmDialog":Z
    const/4 v10, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v6

    .line 273
    .local v6, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v6, :cond_0

    instance-of v10, v6, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v10, :cond_0

    move-object v5, v6

    .line 274
    check-cast v5, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 276
    .local v5, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 277
    .local v4, "dstFolder":Ljava/io/File;
    new-instance v8, Ljava/io/File;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 278
    .local v8, "srcFile":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v3, v4, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 280
    .local v3, "dstFile":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 281
    const/4 v7, 0x1

    .line 282
    iget-object v11, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    if-nez p3, :cond_2

    const v10, 0x7f0e0284

    :goto_0
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v11, v10, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 284
    .local v9, "title":Ljava/lang/String;
    new-instance v11, Landroid/app/AlertDialog$Builder;

    iget-object v10, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {v11, v10}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    if-nez p3, :cond_3

    const v10, 0x7f0e01ac

    :goto_1
    invoke-virtual {v11, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    invoke-virtual {v10, v9}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    const v11, 0x7f0e010e

    new-instance v12, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$3;

    move/from16 v0, p3

    invoke-direct {v12, p0, v3, v0}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$3;-><init>(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;Ljava/io/File;I)V

    invoke-virtual {v10, v11, v12}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    const v11, 0x7f0e0283

    new-instance v12, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$2;

    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-direct {v12, p0, v0, v1, v3}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$2;-><init>(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;Ljava/lang/String;ILjava/io/File;)V

    invoke-virtual {v10, v11, v12}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    const v11, 0x7f0e0046

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 301
    .local v2, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 305
    .end local v2    # "dialog":Landroid/app/AlertDialog;
    .end local v3    # "dstFile":Ljava/io/File;
    .end local v4    # "dstFolder":Ljava/io/File;
    .end local v5    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v8    # "srcFile":Ljava/io/File;
    .end local v9    # "title":Ljava/lang/String;
    :cond_0
    if-nez v7, :cond_1

    .line 306
    const/4 v10, 0x1

    const/4 v11, 0x0

    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-direct {p0, v0, v1, v10, v11}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->handleMultipleFileOperation(Ljava/lang/String;IILjava/lang/String;)V

    .line 308
    :cond_1
    return-void

    .line 282
    .restart local v3    # "dstFile":Ljava/io/File;
    .restart local v4    # "dstFolder":Ljava/io/File;
    .restart local v5    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v8    # "srcFile":Ljava/io/File;
    :cond_2
    const v10, 0x7f0e0285

    goto :goto_0

    .line 284
    .restart local v9    # "title":Ljava/lang/String;
    :cond_3
    const v10, 0x7f0e01a8

    goto :goto_1
.end method

.method private isBaiduAppExist()Z
    .locals 6

    .prologue
    .line 672
    new-instance v3, Landroid/content/ComponentName;

    const-string v4, "com.baidu.netdisk_ss"

    const-string v5, "com.baidu.netdisk.ui.MainActivity"

    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    .local v3, "mBaiduAppName":Landroid/content/ComponentName;
    const/4 v1, 0x0

    .line 675
    .local v1, "bExist":Z
    :try_start_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 677
    .local v0, "activityInfo":Landroid/content/pm/ActivityInfo;
    if-eqz v0, :cond_0

    .line 678
    const/4 v1, 0x1

    .line 683
    .end local v0    # "activityInfo":Landroid/content/pm/ActivityInfo;
    :cond_0
    :goto_0
    return v1

    .line 680
    :catch_0
    move-exception v2

    .line 681
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private isInCopyOrMove(Lcom/sec/android/gallery3d/ui/SelectionManager;)Z
    .locals 10
    .param p1, "selectionModeProxy"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    const-wide/16 v8, 0x0

    .line 523
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v2

    .line 524
    .local v2, "mediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 525
    .local v1, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v1, :cond_0

    .line 528
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaObject;->getSupportedOperations()J

    move-result-wide v4

    .line 530
    .local v4, "supported":J
    const-wide/high16 v6, 0x20000000000000L

    and-long/2addr v6, v4

    cmp-long v3, v6, v8

    if-eqz v3, :cond_1

    const-wide/high16 v6, 0x10000000000000L

    and-long/2addr v6, v4

    cmp-long v3, v6, v8

    if-nez v3, :cond_0

    .line 531
    :cond_1
    sget-object v3, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "supported check : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " media path : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 532
    const/4 v3, 0x0

    .line 535
    .end local v1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v4    # "supported":J
    :goto_0
    return v3

    :cond_2
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private showAlbumListDialog(Landroid/content/Context;[Lcom/sec/android/gallery3d/data/MediaSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mediaSetArray"    # [Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "operationId"    # I

    .prologue
    .line 487
    const/4 v2, 0x1

    if-ne p3, v2, :cond_0

    .line 488
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_MOVE_FILES:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    .line 489
    .local v0, "event":Lcom/sec/samsung/gallery/core/Event;
    const v1, 0x7f0e0498

    .line 501
    .local v1, "title":I
    :goto_0
    new-instance v2, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;

    invoke-direct {v2, p1, p2, v0}, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;-><init>(Landroid/content/Context;[Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/core/Event;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mAlbumListDialog:Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;

    .line 502
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mAlbumListDialog:Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->setTitle(I)V

    .line 503
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mAlbumListDialog:Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;

    invoke-virtual {v2, p0}, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->addObserver(Ljava/util/Observer;)V

    .line 504
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mAlbumListDialog:Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/common/AlbumChoiceDialog;->showDialog()V

    .line 505
    return-void

    .line 490
    .end local v0    # "event":Lcom/sec/samsung/gallery/core/Event;
    .end local v1    # "title":I
    :cond_0
    const/4 v2, 0x3

    if-ne p3, v2, :cond_1

    .line 491
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_FROM_SECRETBOX:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    .line 492
    .restart local v0    # "event":Lcom/sec/samsung/gallery/core/Event;
    const v1, 0x7f0e01e2

    .restart local v1    # "title":I
    goto :goto_0

    .line 493
    .end local v0    # "event":Lcom/sec/samsung/gallery/core/Event;
    .end local v1    # "title":I
    :cond_1
    const/4 v2, 0x4

    if-ne p3, v2, :cond_2

    .line 494
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_FROM_KNOX:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    .line 495
    .restart local v0    # "event":Lcom/sec/samsung/gallery/core/Event;
    const v1, 0x7f0e0347

    .restart local v1    # "title":I
    goto :goto_0

    .line 497
    .end local v0    # "event":Lcom/sec/samsung/gallery/core/Event;
    .end local v1    # "title":I
    :cond_2
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_COPY_FILES:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    .line 498
    .restart local v0    # "event":Lcom/sec/samsung/gallery/core/Event;
    const v1, 0x7f0e0447

    .restart local v1    # "title":I
    goto :goto_0
.end method

.method private showCopyMoveChoiceDialog(Ljava/lang/String;)V
    .locals 2
    .param p1, "dstAlbumPath"    # Ljava/lang/String;

    .prologue
    .line 539
    new-instance v0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mCopyMoveChioceDialog:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    .line 540
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mCopyMoveChioceDialog:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    const v1, 0x7f0e02e2

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->setTitle(I)V

    .line 541
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mCopyMoveChioceDialog:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    new-instance v1, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$5;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$5;-><init>(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->addObserver(Ljava/util/Observer;)V

    .line 552
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mCopyMoveChioceDialog:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->showDialog()V

    .line 553
    return-void
.end method

.method private showLocalCloudCopyMoveChioceDialog(ILjava/lang/String;)V
    .locals 2
    .param p1, "fileTo"    # I
    .param p2, "dstAlbumPath"    # Ljava/lang/String;

    .prologue
    .line 630
    new-instance v0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mLocalCloudCopyChioceDialog:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    .line 631
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mLocalCloudCopyChioceDialog:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    const v1, 0x7f0e02e2

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->setTitle(I)V

    .line 632
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mLocalCloudCopyChioceDialog:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    new-instance v1, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$6;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$6;-><init>(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->addObserver(Ljava/util/Observer;)V

    .line 656
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mLocalCloudCopyChioceDialog:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->showDialog()V

    .line 657
    return-void
.end method

.method private showRenameDialog(Ljava/io/File;I)V
    .locals 5
    .param p1, "dstFile"    # Ljava/io/File;
    .param p2, "operationId"    # I

    .prologue
    const/16 v4, 0x2e

    .line 250
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 251
    .local v1, "name":Ljava/lang/String;
    invoke-virtual {v1, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 252
    .local v0, "extention":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-virtual {v1, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 254
    new-instance v2, Lcom/sec/samsung/gallery/view/common/RenameDialog;

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/common/RenameDialog;-><init>(Landroid/content/Context;)V

    .line 255
    .local v2, "renameDialog":Lcom/sec/samsung/gallery/view/common/RenameDialog;
    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->setCurrentName(Ljava/lang/String;)V

    .line 256
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->setCurrentFilePath(Ljava/lang/String;)V

    .line 257
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->showMediaRenameDialog()V

    .line 259
    new-instance v3, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$1;

    invoke-direct {v3, p0, v0, p1, p2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;Ljava/lang/String;Ljava/io/File;I)V

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->addObserver(Ljava/util/Observer;)V

    .line 267
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 14
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 100
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, [Ljava/lang/Object;

    move-object v9, v12

    check-cast v9, [Ljava/lang/Object;

    .line 101
    .local v9, "params":[Ljava/lang/Object;
    const/4 v12, 0x0

    aget-object v12, v9, v12

    check-cast v12, Landroid/content/Context;

    iput-object v12, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    .line 102
    const/4 v12, 0x1

    aget-object v12, v9, v12

    check-cast v12, [Lcom/sec/android/gallery3d/data/MediaSet;

    move-object v5, v12

    check-cast v5, [Lcom/sec/android/gallery3d/data/MediaSet;

    .line 103
    .local v5, "mediaSetArray":[Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v12, 0x2

    aget-object v12, v9, v12

    check-cast v12, Ljava/lang/Boolean;

    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    .line 104
    .local v11, "show":Z
    const/4 v12, 0x3

    aget-object v12, v9, v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 106
    .local v8, "operationId":I
    array-length v12, v9

    const/4 v13, 0x4

    if-le v12, v13, :cond_0

    .line 107
    const/4 v12, 0x4

    aget-object v12, v9, v12

    check-cast v12, Ljava/lang/String;

    iput-object v12, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mTopSetPath:Ljava/lang/String;

    .line 110
    :cond_0
    if-nez v11, :cond_2

    .line 111
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->dismissAlbumListDialog()V

    .line 112
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->dismissCopyMoveChioseDialog()V

    .line 113
    sget-boolean v12, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaiduCloudDragDrop:Z

    if-eqz v12, :cond_1

    .line 114
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->dismissmLocalCloudCopyChioceDialog()V

    .line 179
    :cond_1
    :goto_0
    return-void

    .line 119
    :cond_2
    iget-object v12, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    iput-object v12, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mResources:Landroid/content/res/Resources;

    .line 120
    new-instance v12, Lcom/sec/samsung/gallery/util/FileUtil;

    iget-object v13, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {v12, v13}, Lcom/sec/samsung/gallery/util/FileUtil;-><init>(Landroid/content/Context;)V

    iput-object v12, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    .line 121
    iget-object v12, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    check-cast v12, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v12}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    move-result-object v12

    iput-object v12, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    .line 122
    const/4 v12, 0x2

    if-ne v8, v12, :cond_6

    .line 124
    sget-boolean v12, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaiduCloudDragDrop:Z

    if-eqz v12, :cond_5

    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->isBaiduAppExist()Z

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_5

    iget-object v12, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-static {v12}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->isCloudContentSyncOn(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 125
    const/4 v12, 0x0

    aget-object v12, v5, v12

    invoke-direct {p0, v12}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->checkLocalCloudCopy(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v10

    .line 126
    .local v10, "result":I
    const/4 v12, 0x2

    if-ne v10, v12, :cond_3

    .line 127
    const/4 v12, 0x0

    aget-object v12, v5, v12

    invoke-direct {p0, v12}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->getDstCloudPath(Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v10, v12}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->showLocalCloudCopyMoveChioceDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 128
    :cond_3
    const/4 v12, 0x3

    if-ne v10, v12, :cond_4

    .line 129
    const/4 v12, 0x0

    aget-object v12, v5, v12

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v10, v12}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->showLocalCloudCopyMoveChioceDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 131
    :cond_4
    const/4 v12, 0x0

    aget-object v12, v5, v12

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v12}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->showCopyMoveChoiceDialog(Ljava/lang/String;)V

    goto :goto_0

    .line 135
    .end local v10    # "result":I
    :cond_5
    const/4 v12, 0x0

    aget-object v12, v5, v12

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v12}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->showCopyMoveChoiceDialog(Ljava/lang/String;)V

    goto :goto_0

    .line 137
    :cond_6
    const/4 v12, 0x3

    if-ne v8, v12, :cond_b

    .line 138
    const/4 v4, 0x0

    .line 139
    .local v4, "mNewSetCount":I
    array-length v3, v5

    .line 140
    .local v3, "mMediaSetCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v3, :cond_8

    .line 141
    aget-object v12, v5, v0

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 140
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 144
    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 146
    :cond_8
    new-array v7, v4, [Lcom/sec/android/gallery3d/data/MediaSet;

    .line 147
    .local v7, "mediaSetArrayForSecretBox":[Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v1, 0x0

    .line 148
    .local v1, "j":I
    const/4 v0, 0x0

    :goto_3
    if-ge v0, v3, :cond_a

    .line 149
    aget-object v12, v5, v0

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 148
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 152
    :cond_9
    aget-object v12, v5, v0

    aput-object v12, v7, v1

    .line 153
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 156
    :cond_a
    iget-object v12, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {p0, v12, v7, v8}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->showAlbumListDialog(Landroid/content/Context;[Lcom/sec/android/gallery3d/data/MediaSet;I)V

    goto/16 :goto_0

    .line 157
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v3    # "mMediaSetCount":I
    .end local v4    # "mNewSetCount":I
    .end local v7    # "mediaSetArrayForSecretBox":[Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_b
    const/4 v12, 0x4

    if-ne v8, v12, :cond_10

    .line 158
    const/4 v4, 0x0

    .line 159
    .restart local v4    # "mNewSetCount":I
    array-length v3, v5

    .line 160
    .restart local v3    # "mMediaSetCount":I
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_5
    if-ge v0, v3, :cond_d

    .line 161
    aget-object v12, v5, v0

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isKNOXPath(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_c

    .line 160
    :goto_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 164
    :cond_c
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 166
    :cond_d
    new-array v6, v4, [Lcom/sec/android/gallery3d/data/MediaSet;

    .line 167
    .local v6, "mediaSetArrayForKNOX":[Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v0, 0x0

    const/4 v1, 0x0

    .restart local v1    # "j":I
    move v2, v1

    .end local v1    # "j":I
    .local v2, "j":I
    :goto_7
    if-ge v0, v3, :cond_f

    .line 168
    aget-object v12, v5, v0

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isKNOXPath(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_e

    move v1, v2

    .line 167
    .end local v2    # "j":I
    .restart local v1    # "j":I
    :goto_8
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    .end local v1    # "j":I
    .restart local v2    # "j":I
    goto :goto_7

    .line 171
    :cond_e
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "j":I
    .restart local v1    # "j":I
    aget-object v12, v5, v0

    aput-object v12, v6, v2

    goto :goto_8

    .line 173
    .end local v1    # "j":I
    .restart local v2    # "j":I
    :cond_f
    iget-object v12, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {p0, v12, v6, v8}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->showAlbumListDialog(Landroid/content/Context;[Lcom/sec/android/gallery3d/data/MediaSet;I)V

    goto/16 :goto_0

    .line 177
    .end local v0    # "i":I
    .end local v2    # "j":I
    .end local v3    # "mMediaSetCount":I
    .end local v4    # "mNewSetCount":I
    .end local v6    # "mediaSetArrayForKNOX":[Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_10
    iget-object v12, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {p0, v12, v5, v8}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->showAlbumListDialog(Landroid/content/Context;[Lcom/sec/android/gallery3d/data/MediaSet;I)V

    goto/16 :goto_0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 5
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    if-eqz v0, :cond_0

    .line 515
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/FileUtil;->cancelOperation()V

    .line 516
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    .line 517
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_SCAN"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->EXTERNAL_SD_DIRECTORY:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 518
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "EXIT_SELECTION_MODE"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 520
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 9
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 183
    move-object v2, p2

    check-cast v2, Lcom/sec/samsung/gallery/core/Event;

    .line 186
    .local v2, "ev":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v4

    sget v5, Lcom/sec/samsung/gallery/core/Event;->EVENT_MOVE_FILES:I

    if-ne v4, v5, :cond_1

    .line 187
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/core/Event;->getData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 188
    .local v0, "destMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v1

    .line 189
    .local v1, "dstAlbumPath":Ljava/lang/String;
    invoke-direct {p0, v1, v6}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->handleFilesOperation(Ljava/lang/String;I)V

    .line 213
    .end local v0    # "destMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v1    # "dstAlbumPath":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v4

    sget v5, Lcom/sec/samsung/gallery/core/Event;->EVENT_COPY_FILES:I

    if-ne v4, v5, :cond_2

    .line 191
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/core/Event;->getData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 192
    .restart local v0    # "destMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v1

    .line 193
    .restart local v1    # "dstAlbumPath":Ljava/lang/String;
    invoke-direct {p0, v1, v7}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->handleFilesOperation(Ljava/lang/String;I)V

    goto :goto_0

    .line 194
    .end local v0    # "destMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v1    # "dstAlbumPath":Ljava/lang/String;
    :cond_2
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v4

    sget v5, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_NEW_ALBUM_NAME_EDIT_DIALOG:I

    if-ne v4, v5, :cond_3

    .line 202
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    .line 203
    const/4 v4, 0x4

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    aput-object v4, v3, v7

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/core/Event;->getIntData()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mTopSetPath:Ljava/lang/String;

    aput-object v4, v3, v8

    .line 206
    .local v3, "params":[Ljava/lang/Object;
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v4

    const-string v5, "SHOW_NEW_ALBUM_DIALOG"

    invoke-virtual {v4, v5, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 208
    .end local v3    # "params":[Ljava/lang/Object;
    :cond_3
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v4

    sget v5, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_FROM_SECRETBOX:I

    if-ne v4, v5, :cond_0

    .line 209
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/core/Event;->getData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 210
    .restart local v0    # "destMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v1

    .line 211
    .restart local v1    # "dstAlbumPath":Ljava/lang/String;
    invoke-direct {p0, v1, v8}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->handleFilesOperation(Ljava/lang/String;I)V

    goto :goto_0
.end method
