.class Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$1;
.super Ljava/lang/Object;
.source "MusicPlayer.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 2
    .param p1, "focusChange"    # I

    .prologue
    .line 43
    packed-switch p1, :pswitch_data_0

    .line 59
    :goto_0
    :pswitch_0
    return-void

    .line 46
    :pswitch_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mForcePause:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->access$000(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mSlideShowPause:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->playMusic()V

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    const/4 v1, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsMusicAFCLoss:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->access$202(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;Z)Z

    goto :goto_0

    .line 53
    :pswitch_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    const/4 v1, 0x1

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsMusicAFCLoss:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->access$202(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;Z)Z

    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->pauseMusic()V

    goto :goto_0

    .line 43
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
