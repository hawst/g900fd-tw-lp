.class public abstract Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;
.super Landroid/view/ViewGroup;
.source "AdapterView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$1;,
        Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$SelectionNotifier;,
        Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;,
        Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterContextMenuInfo;,
        Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemSelectedListener;,
        Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemLongClickListener;,
        Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Landroid/widget/Adapter;",
        ">",
        "Landroid/view/ViewGroup;"
    }
.end annotation


# static fields
.field public static final INVALID_COL_ID:J = -0x8000000000000000L

.field public static final INVALID_POSITION:I = -0x1

.field public static final ITEM_VIEW_TYPE_HEADER_OR_FOOTER:I = -0x2

.field public static final ITEM_VIEW_TYPE_IGNORE:I = -0x1

.field static final SYNC_FIRST_POSITION:I = 0x1

.field static final SYNC_MAX_DURATION_MILLIS:I = 0x64

.field static final SYNC_SELECTED_POSITION:I


# instance fields
.field mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field protected mBlockLayoutRequests:Z

.field public mDataChanged:Z

.field private mDesiredFocusableInTouchModeState:Z

.field private mDesiredFocusableState:Z

.field private mEmptyView:Landroid/view/View;

.field protected mFirstPosition:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "scrolling"
    .end annotation
.end field

.field protected mInLayout:Z

.field protected mItemCount:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
    .end annotation
.end field

.field private mLayoutWidth:I

.field protected mNeedSync:Z

.field protected mNextSelectedColId:J

.field protected mNextSelectedPosition:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
    .end annotation
.end field

.field protected mOldItemCount:I

.field protected mOldSelectedColId:J

.field protected mOldSelectedPosition:I

.field mOnItemClickListener:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemClickListener;

.field mOnItemLongClickListener:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemLongClickListener;

.field mOnItemSelectedListener:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemSelectedListener;

.field protected mSelectedColId:J

.field protected mSelectedPosition:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
    .end annotation
.end field

.field private mSelectionNotifier:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$SelectionNotifier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView",
            "<TT;>.SelectionNotifier;"
        }
    .end annotation
.end field

.field protected mSpecificLeft:I

.field protected mSyncColId:J

.field mSyncMode:I

.field protected mSyncPosition:I

.field protected mSyncWidth:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    const/4 v1, -0x1

    const-wide/high16 v2, -0x8000000000000000L

    const/4 v0, 0x0

    .line 216
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 57
    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mFirstPosition:I

    .line 73
    iput-wide v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSyncColId:J

    .line 83
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNeedSync:Z

    .line 114
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mInLayout:Z

    .line 140
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNextSelectedPosition:I

    .line 146
    iput-wide v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNextSelectedColId:J

    .line 151
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSelectedPosition:I

    .line 157
    iput-wide v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSelectedColId:J

    .line 191
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOldSelectedPosition:I

    .line 196
    iput-wide v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOldSelectedColId:J

    .line 213
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mBlockLayoutRequests:Z

    .line 217
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    const/4 v1, -0x1

    const-wide/high16 v2, -0x8000000000000000L

    const/4 v0, 0x0

    .line 220
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mFirstPosition:I

    .line 73
    iput-wide v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSyncColId:J

    .line 83
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNeedSync:Z

    .line 114
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mInLayout:Z

    .line 140
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNextSelectedPosition:I

    .line 146
    iput-wide v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNextSelectedColId:J

    .line 151
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSelectedPosition:I

    .line 157
    iput-wide v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSelectedColId:J

    .line 191
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOldSelectedPosition:I

    .line 196
    iput-wide v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOldSelectedColId:J

    .line 213
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mBlockLayoutRequests:Z

    .line 221
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    const/4 v1, -0x1

    const-wide/high16 v2, -0x8000000000000000L

    const/4 v0, 0x0

    .line 224
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mFirstPosition:I

    .line 73
    iput-wide v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSyncColId:J

    .line 83
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNeedSync:Z

    .line 114
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mInLayout:Z

    .line 140
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNextSelectedPosition:I

    .line 146
    iput-wide v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNextSelectedColId:J

    .line 151
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSelectedPosition:I

    .line 157
    iput-wide v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSelectedColId:J

    .line 191
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOldSelectedPosition:I

    .line 196
    iput-wide v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOldSelectedColId:J

    .line 213
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mBlockLayoutRequests:Z

    .line 226
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 228
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getImportantForAccessibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 229
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->setImportantForAccessibility(I)V

    .line 233
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 234
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;Landroid/os/Parcelable;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;
    .param p1, "x1"    # Landroid/os/Parcelable;

    .prologue
    .line 42
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;)Landroid/os/Parcelable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->fireOnSelected()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->performAccessibilityActionsOnSelected()V

    return-void
.end method

.method private fireOnSelected()V
    .locals 6

    .prologue
    .line 900
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOnItemSelectedListener:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemSelectedListener;

    if-nez v0, :cond_0

    .line 911
    :goto_0
    return-void

    .line 903
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getSelectedItemPosition()I

    move-result v3

    .line 904
    .local v3, "selection":I
    if-ltz v3, :cond_1

    .line 905
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getSelectedView()Landroid/view/View;

    move-result-object v2

    .line 906
    .local v2, "v":Landroid/view/View;
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOnItemSelectedListener:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v4

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemSelectedListener;->onItemSelected(Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;Landroid/view/View;IJ)V

    goto :goto_0

    .line 909
    .end local v2    # "v":Landroid/view/View;
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOnItemSelectedListener:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemSelectedListener;

    invoke-interface {v0, p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemSelectedListener;->onNothingSelected(Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;)V

    goto :goto_0
.end method

.method private isScrollableForAccessibility()Z
    .locals 5

    .prologue
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    const/4 v2, 0x0

    .line 978
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 979
    .local v0, "adapter":Landroid/widget/Adapter;, "TT;"
    if-eqz v0, :cond_1

    .line 980
    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v1

    .line 981
    .local v1, "itemCount":I
    if-lez v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getFirstVisiblePosition()I

    move-result v3

    if-gtz v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getLastVisiblePosition()I

    move-result v3

    add-int/lit8 v4, v1, -0x1

    if-ge v3, v4, :cond_1

    :cond_0
    const/4 v2, 0x1

    .line 984
    .end local v1    # "itemCount":I
    :cond_1
    return v2
.end method

.method private performAccessibilityActionsOnSelected()V
    .locals 2

    .prologue
    .line 914
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 922
    :cond_0
    :goto_0
    return-void

    .line 917
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getSelectedItemPosition()I

    move-result v0

    .line 918
    .local v0, "position":I
    if-ltz v0, :cond_0

    .line 920
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->sendAccessibilityEvent(I)V

    goto :goto_0
.end method

.method private updateEmptyStatus(Z)V
    .locals 6
    .param p1, "empty"    # Z
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongCall"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 736
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->isInFilterMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 737
    const/4 p1, 0x0

    .line 740
    :cond_0
    if-eqz p1, :cond_3

    .line 741
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mEmptyView:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 742
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mEmptyView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 743
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->setVisibility(I)V

    .line 752
    :goto_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mDataChanged:Z

    if-eqz v0, :cond_1

    .line 753
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getRight()I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getBottom()I

    move-result v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->onLayout(ZIIII)V

    .line 759
    :cond_1
    :goto_1
    return-void

    .line 746
    :cond_2
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->setVisibility(I)V

    goto :goto_0

    .line 756
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mEmptyView:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mEmptyView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 757
    :cond_4
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .locals 2
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 448
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addView(Landroid/view/View;I)V
    .locals 2
    .param p1, "child"    # Landroid/view/View;
    .param p2, "index"    # I

    .prologue
    .line 464
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View, int) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2
    .param p1, "child"    # Landroid/view/View;
    .param p2, "index"    # I
    .param p3, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 499
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View, int, LayoutParams) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2
    .param p1, "child"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 480
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View, LayoutParams) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected canAnimate()Z
    .locals 1

    .prologue
    .line 989
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    invoke-super {p0}, Landroid/view/ViewGroup;->canAnimate()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mItemCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected checkFocus()V
    .locals 6

    .prologue
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 717
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 718
    .local v0, "adapter":Landroid/widget/Adapter;, "TT;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v3

    if-nez v3, :cond_5

    :cond_0
    move v1, v4

    .line 719
    .local v1, "empty":Z
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->isInFilterMode()Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_1
    move v2, v4

    .line 723
    .local v2, "focusable":Z
    :goto_1
    if-eqz v2, :cond_7

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mDesiredFocusableInTouchModeState:Z

    if-eqz v3, :cond_7

    move v3, v4

    :goto_2
    invoke-super {p0, v3}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    .line 724
    if-eqz v2, :cond_8

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mDesiredFocusableState:Z

    if-eqz v3, :cond_8

    move v3, v4

    :goto_3
    invoke-super {p0, v3}, Landroid/view/ViewGroup;->setFocusable(Z)V

    .line 725
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mEmptyView:Landroid/view/View;

    if-eqz v3, :cond_4

    .line 726
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/widget/Adapter;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    move v5, v4

    :cond_3
    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->updateEmptyStatus(Z)V

    .line 728
    :cond_4
    return-void

    .end local v1    # "empty":Z
    .end local v2    # "focusable":Z
    :cond_5
    move v1, v5

    .line 718
    goto :goto_0

    .restart local v1    # "empty":Z
    :cond_6
    move v2, v5

    .line 719
    goto :goto_1

    .restart local v2    # "focusable":Z
    :cond_7
    move v3, v5

    .line 723
    goto :goto_2

    :cond_8
    move v3, v5

    .line 724
    goto :goto_3
.end method

.method protected checkSelectionChanged()V
    .locals 4

    .prologue
    .line 1061
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSelectedPosition:I

    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOldSelectedPosition:I

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSelectedColId:J

    iget-wide v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOldSelectedColId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 1062
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->selectionChanged()V

    .line 1063
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSelectedPosition:I

    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOldSelectedPosition:I

    .line 1064
    iget-wide v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSelectedColId:J

    iput-wide v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOldSelectedColId:J

    .line 1066
    :cond_1
    return-void
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 926
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getSelectedView()Landroid/view/View;

    move-result-object v0

    .line 927
    .local v0, "selectedView":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 929
    const/4 v1, 0x1

    .line 931
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 797
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    .local p1, "container":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->dispatchThawSelfOnly(Landroid/util/SparseArray;)V

    .line 798
    return-void
.end method

.method protected dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 789
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    .local p1, "container":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->dispatchFreezeSelfOnly(Landroid/util/SparseArray;)V

    .line 790
    return-void
.end method

.method findSyncPosition()I
    .locals 20

    .prologue
    .line 1075
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mItemCount:I

    .line 1077
    .local v3, "count":I
    if-nez v3, :cond_1

    .line 1078
    const/4 v15, -0x1

    .line 1150
    :cond_0
    :goto_0
    return v15

    .line 1081
    :cond_1
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSyncColId:J

    .line 1082
    .local v12, "idToMatch":J
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSyncPosition:I

    .line 1085
    .local v15, "seed":I
    const-wide/high16 v16, -0x8000000000000000L

    cmp-long v16, v12, v16

    if-nez v16, :cond_2

    .line 1086
    const/4 v15, -0x1

    goto :goto_0

    .line 1090
    :cond_2
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v0, v15}, Ljava/lang/Math;->max(II)I

    move-result v15

    .line 1091
    add-int/lit8 v16, v3, -0x1

    move/from16 v0, v16

    invoke-static {v0, v15}, Ljava/lang/Math;->min(II)I

    move-result v15

    .line 1093
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v16

    const-wide/16 v18, 0x64

    add-long v6, v16, v18

    .line 1098
    .local v6, "endTime":J
    move v8, v15

    .line 1101
    .local v8, "first":I
    move v11, v15

    .line 1104
    .local v11, "last":I
    const/4 v14, 0x0

    .line 1114
    .local v14, "next":Z
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v2

    .line 1115
    .local v2, "adapter":Landroid/widget/Adapter;, "TT;"
    if-nez v2, :cond_5

    .line 1116
    const/4 v15, -0x1

    goto :goto_0

    .line 1134
    .local v4, "colId":J
    .local v9, "hitFirst":Z
    .local v10, "hitLast":Z
    :cond_3
    if-nez v9, :cond_4

    if-eqz v14, :cond_9

    if-nez v10, :cond_9

    .line 1136
    :cond_4
    add-int/lit8 v11, v11, 0x1

    .line 1137
    move v15, v11

    .line 1139
    const/4 v14, 0x0

    .line 1119
    .end local v4    # "colId":J
    .end local v9    # "hitFirst":Z
    .end local v10    # "hitLast":Z
    :cond_5
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v16

    cmp-long v16, v16, v6

    if-gtz v16, :cond_6

    .line 1120
    invoke-interface {v2, v15}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v4

    .line 1121
    .restart local v4    # "colId":J
    cmp-long v16, v4, v12

    if-eqz v16, :cond_0

    .line 1126
    add-int/lit8 v16, v3, -0x1

    move/from16 v0, v16

    if-ne v11, v0, :cond_7

    const/4 v10, 0x1

    .line 1127
    .restart local v10    # "hitLast":Z
    :goto_2
    if-nez v8, :cond_8

    const/4 v9, 0x1

    .line 1129
    .restart local v9    # "hitFirst":Z
    :goto_3
    if-eqz v10, :cond_3

    if-eqz v9, :cond_3

    .line 1150
    .end local v4    # "colId":J
    .end local v9    # "hitFirst":Z
    .end local v10    # "hitLast":Z
    :cond_6
    const/4 v15, -0x1

    goto :goto_0

    .line 1126
    .restart local v4    # "colId":J
    :cond_7
    const/4 v10, 0x0

    goto :goto_2

    .line 1127
    .restart local v10    # "hitLast":Z
    :cond_8
    const/4 v9, 0x0

    goto :goto_3

    .line 1140
    .restart local v9    # "hitFirst":Z
    :cond_9
    if-nez v10, :cond_a

    if-nez v14, :cond_5

    if-nez v9, :cond_5

    .line 1142
    :cond_a
    add-int/lit8 v8, v8, -0x1

    .line 1143
    move v15, v8

    .line 1145
    const/4 v14, 0x1

    goto :goto_1
.end method

.method public abstract getAdapter()Landroid/widget/Adapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public getCount()I
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 589
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mItemCount:I

    return v0
.end method

.method public getEmptyView()Landroid/view/View;
    .locals 1

    .prologue
    .line 678
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mEmptyView:Landroid/view/View;

    return-object v0
.end method

.method public getFirstVisiblePosition()I
    .locals 1

    .prologue
    .line 632
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mFirstPosition:I

    return v0
.end method

.method public getItemAtPosition(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 769
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 770
    .local v0, "adapter":Landroid/widget/Adapter;, "TT;"
    if-eqz v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    invoke-interface {v0, p1}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public getItemIdAtPosition(I)J
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 774
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 775
    .local v0, "adapter":Landroid/widget/Adapter;, "TT;"
    if-eqz v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    const-wide/high16 v2, -0x8000000000000000L

    :goto_0
    return-wide v2

    :cond_1
    invoke-interface {v0, p1}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v2

    goto :goto_0
.end method

.method public getLastVisiblePosition()I
    .locals 2

    .prologue
    .line 641
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mFirstPosition:I

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getChildCount()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final getOnItemClickListener()Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 272
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOnItemClickListener:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method public final getOnItemLongClickListener()Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 341
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOnItemLongClickListener:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemLongClickListener;

    return-object v0
.end method

.method public final getOnItemSelectedListener()Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemSelectedListener;
    .locals 1

    .prologue
    .line 389
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOnItemSelectedListener:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemSelectedListener;

    return-object v0
.end method

.method public getPositionForView(Landroid/view/View;)I
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    const/4 v5, -0x1

    .line 603
    move-object v3, p1

    .line 606
    .local v3, "listItem":Landroid/view/View;
    :goto_0
    :try_start_0
    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .local v4, "v":Landroid/view/View;
    invoke-virtual {v4, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-nez v6, :cond_1

    .line 607
    move-object v3, v4

    goto :goto_0

    .line 609
    .end local v4    # "v":Landroid/view/View;
    :catch_0
    move-exception v1

    .line 623
    :cond_0
    :goto_1
    return v5

    .line 615
    .restart local v4    # "v":Landroid/view/View;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getChildCount()I

    move-result v0

    .line 616
    .local v0, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, v0, :cond_0

    .line 617
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 618
    iget v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mFirstPosition:I

    add-int/2addr v5, v2

    goto :goto_1

    .line 616
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public getSelectedItem()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 574
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 575
    .local v0, "adapter":Landroid/widget/Adapter;, "TT;"
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getSelectedItemPosition()I

    move-result v1

    .line 576
    .local v1, "selection":I
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    if-ltz v1, :cond_0

    .line 577
    invoke-interface {v0, v1}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    .line 579
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getSelectedItemId()J
    .locals 2
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 562
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    iget-wide v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNextSelectedColId:J

    return-wide v0
.end method

.method public getSelectedItemPosition()I
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 554
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNextSelectedPosition:I

    return v0
.end method

.method public abstract getSelectedView()Landroid/view/View;
.end method

.method handleDataChanged()V
    .locals 10

    .prologue
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    const-wide/high16 v8, -0x8000000000000000L

    const/4 v7, 0x1

    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 993
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mItemCount:I

    .line 994
    .local v0, "count":I
    const/4 v1, 0x0

    .line 996
    .local v1, "found":Z
    if-lez v0, :cond_4

    .line 1001
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNeedSync:Z

    if-eqz v4, :cond_0

    .line 1004
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNeedSync:Z

    .line 1008
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->findSyncPosition()I

    move-result v2

    .line 1009
    .local v2, "newPos":I
    if-ltz v2, :cond_0

    .line 1011
    invoke-virtual {p0, v2, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->lookForSelectablePosition(IZ)I

    move-result v3

    .line 1012
    .local v3, "selectablePos":I
    if-ne v3, v2, :cond_0

    .line 1014
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->setNextSelectedPositionInt(I)V

    .line 1015
    const/4 v1, 0x1

    .line 1019
    .end local v2    # "newPos":I
    .end local v3    # "selectablePos":I
    :cond_0
    if-nez v1, :cond_4

    .line 1021
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getSelectedItemPosition()I

    move-result v2

    .line 1024
    .restart local v2    # "newPos":I
    if-lt v2, v0, :cond_1

    .line 1025
    add-int/lit8 v2, v0, -0x1

    .line 1027
    :cond_1
    if-gez v2, :cond_2

    .line 1028
    const/4 v2, 0x0

    .line 1032
    :cond_2
    invoke-virtual {p0, v2, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->lookForSelectablePosition(IZ)I

    move-result v3

    .line 1033
    .restart local v3    # "selectablePos":I
    if-gez v3, :cond_3

    .line 1035
    invoke-virtual {p0, v2, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->lookForSelectablePosition(IZ)I

    move-result v3

    .line 1037
    :cond_3
    if-ltz v3, :cond_4

    .line 1038
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->setNextSelectedPositionInt(I)V

    .line 1039
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->checkSelectionChanged()V

    .line 1040
    const/4 v1, 0x1

    .line 1044
    .end local v2    # "newPos":I
    .end local v3    # "selectablePos":I
    :cond_4
    if-nez v1, :cond_5

    .line 1046
    iput v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSelectedPosition:I

    .line 1047
    iput-wide v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSelectedColId:J

    .line 1048
    iput v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNextSelectedPosition:I

    .line 1049
    iput-wide v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNextSelectedColId:J

    .line 1050
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNeedSync:Z

    .line 1051
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->checkSelectionChanged()V

    .line 1058
    :cond_5
    return-void
.end method

.method isInFilterMode()Z
    .locals 1

    .prologue
    .line 687
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    const/4 v0, 0x0

    return v0
.end method

.method protected lookForSelectablePosition(IZ)I
    .locals 0
    .param p1, "position"    # I
    .param p2, "lookDown"    # Z

    .prologue
    .line 1164
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    return p1
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 857
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 858
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSelectionNotifier:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$SelectionNotifier;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 859
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 964
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 965
    const-class v1, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 966
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->isScrollableForAccessibility()Z

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    .line 967
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getSelectedView()Landroid/view/View;

    move-result-object v0

    .line 968
    .local v0, "selectedView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 969
    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    .line 971
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setCurrentItemIndex(I)V

    .line 972
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getFirstVisiblePosition()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    .line 973
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getLastVisiblePosition()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setToIndex(I)V

    .line 974
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getCount()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setItemCount(I)V

    .line 975
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 2
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 952
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 953
    const-class v1, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 954
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->isScrollableForAccessibility()Z

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setScrollable(Z)V

    .line 955
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getSelectedView()Landroid/view/View;

    move-result-object v0

    .line 956
    .local v0, "selectedView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 957
    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setEnabled(Z)V

    .line 959
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 544
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mLayoutWidth:I

    .line 545
    return-void
.end method

.method public onRequestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2
    .param p1, "child"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 937
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onRequestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 939
    invoke-static {}, Landroid/view/accessibility/AccessibilityEvent;->obtain()Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 940
    .local v0, "record":Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 942
    invoke-virtual {p1, v0}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    .line 943
    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->appendRecord(Landroid/view/accessibility/AccessibilityRecord;)V

    .line 944
    const/4 v1, 0x1

    .line 946
    .end local v0    # "record":Landroid/view/accessibility/AccessibilityEvent;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public performItemClick(Landroid/view/View;IJ)Z
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "id"    # J

    .prologue
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 287
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOnItemClickListener:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemClickListener;

    if-eqz v1, :cond_1

    .line 288
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->playSoundEffect(I)V

    .line 289
    if-eqz p1, :cond_0

    .line 290
    invoke-virtual {p1, v6}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOnItemClickListener:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemClickListener;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-interface/range {v0 .. v5}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemClickListener;->onItemClick(Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;Landroid/view/View;IJ)V

    move v0, v6

    .line 296
    :cond_1
    return v0
.end method

.method public rememberSyncState()V
    .locals 6

    .prologue
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1199
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getChildCount()I

    move-result v2

    if-lez v2, :cond_1

    .line 1200
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNeedSync:Z

    .line 1201
    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mLayoutWidth:I

    int-to-long v2, v2

    iput-wide v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSyncWidth:J

    .line 1202
    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSelectedPosition:I

    if-ltz v2, :cond_2

    .line 1204
    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSelectedPosition:I

    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mFirstPosition:I

    sub-int/2addr v2, v3

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1205
    .local v1, "v":Landroid/view/View;
    iget-wide v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNextSelectedColId:J

    iput-wide v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSyncColId:J

    .line 1206
    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNextSelectedPosition:I

    iput v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSyncPosition:I

    .line 1207
    if-eqz v1, :cond_0

    .line 1208
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSpecificLeft:I

    .line 1210
    :cond_0
    iput v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSyncMode:I

    .line 1227
    .end local v1    # "v":Landroid/view/View;
    :cond_1
    :goto_0
    return-void

    .line 1213
    :cond_2
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1214
    .restart local v1    # "v":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 1215
    .local v0, "adapter":Landroid/widget/Adapter;, "TT;"
    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mFirstPosition:I

    if-ltz v2, :cond_4

    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mFirstPosition:I

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 1216
    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mFirstPosition:I

    invoke-interface {v0, v2}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSyncColId:J

    .line 1220
    :goto_1
    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mFirstPosition:I

    iput v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSyncPosition:I

    .line 1221
    if-eqz v1, :cond_3

    .line 1222
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSpecificLeft:I

    .line 1224
    :cond_3
    iput v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSyncMode:I

    goto :goto_0

    .line 1218
    :cond_4
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSyncColId:J

    goto :goto_1
.end method

.method public removeAllViews()V
    .locals 2

    .prologue
    .line 539
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "removeAllViews() is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeView(Landroid/view/View;)V
    .locals 2
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 514
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "removeView(View) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeViewAt(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 528
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "removeViewAt(int) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method selectionChanged()V
    .locals 2

    .prologue
    .line 881
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOnItemSelectedListener:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemSelectedListener;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 883
    :cond_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mInLayout:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mBlockLayoutRequests:Z

    if-eqz v0, :cond_4

    .line 888
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSelectionNotifier:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$SelectionNotifier;

    if-nez v0, :cond_2

    .line 889
    new-instance v0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$SelectionNotifier;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$SelectionNotifier;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSelectionNotifier:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$SelectionNotifier;

    .line 891
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSelectionNotifier:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$SelectionNotifier;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->post(Ljava/lang/Runnable;)Z

    .line 897
    :cond_3
    :goto_0
    return-void

    .line 893
    :cond_4
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->fireOnSelected()V

    .line 894
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->performAccessibilityActionsOnSelected()V

    goto :goto_0
.end method

.method public abstract setAdapter(Landroid/widget/Adapter;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public setEmptyView(Landroid/view/View;)V
    .locals 4
    .param p1, "emptyView"    # Landroid/view/View;

    .prologue
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    const/4 v1, 0x1

    .line 657
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mEmptyView:Landroid/view/View;

    .line 659
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_0

    .line 661
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getImportantForAccessibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 662
    invoke-virtual {p1, v1}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 666
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 667
    .local v0, "adapter":Landroid/widget/Adapter;, "TT;"
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/widget/Adapter;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 668
    .local v1, "empty":Z
    :cond_1
    :goto_0
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->updateEmptyStatus(Z)V

    .line 669
    return-void

    .line 667
    .end local v1    # "empty":Z
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setFocusable(Z)V
    .locals 5
    .param p1, "focusable"    # Z

    .prologue
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 692
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 693
    .local v0, "adapter":Landroid/widget/Adapter;, "TT;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v4

    if-nez v4, :cond_3

    :cond_0
    move v1, v3

    .line 695
    .local v1, "empty":Z
    :goto_0
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mDesiredFocusableState:Z

    .line 696
    if-nez p1, :cond_1

    .line 697
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mDesiredFocusableInTouchModeState:Z

    .line 700
    :cond_1
    if-eqz p1, :cond_4

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->isInFilterMode()Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_2
    :goto_1
    invoke-super {p0, v3}, Landroid/view/ViewGroup;->setFocusable(Z)V

    .line 701
    return-void

    .end local v1    # "empty":Z
    :cond_3
    move v1, v2

    .line 693
    goto :goto_0

    .restart local v1    # "empty":Z
    :cond_4
    move v3, v2

    .line 700
    goto :goto_1
.end method

.method public setFocusableInTouchMode(Z)V
    .locals 5
    .param p1, "focusable"    # Z

    .prologue
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 705
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 706
    .local v0, "adapter":Landroid/widget/Adapter;, "TT;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v4

    if-nez v4, :cond_3

    :cond_0
    move v1, v3

    .line 708
    .local v1, "empty":Z
    :goto_0
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mDesiredFocusableInTouchModeState:Z

    .line 709
    if-eqz p1, :cond_1

    .line 710
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mDesiredFocusableState:Z

    .line 713
    :cond_1
    if-eqz p1, :cond_4

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->isInFilterMode()Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_2
    :goto_1
    invoke-super {p0, v3}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    .line 714
    return-void

    .end local v1    # "empty":Z
    :cond_3
    move v1, v2

    .line 706
    goto :goto_0

    .restart local v1    # "empty":Z
    :cond_4
    move v3, v2

    .line 713
    goto :goto_1
.end method

.method protected setNextSelectedPositionInt(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1185
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    iput p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNextSelectedPosition:I

    .line 1186
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getItemIdAtPosition(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNextSelectedColId:J

    .line 1188
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNeedSync:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSyncMode:I

    if-nez v0, :cond_0

    if-ltz p1, :cond_0

    .line 1189
    iput p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSyncPosition:I

    .line 1190
    iget-wide v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mNextSelectedColId:J

    iput-wide v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSyncColId:J

    .line 1192
    :cond_0
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 780
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Don\'t call setOnClickListener for an AdapterView. You probably want setOnItemClickListener instead"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setOnItemClickListener(Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemClickListener;

    .prologue
    .line 265
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOnItemClickListener:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemClickListener;

    .line 266
    return-void
.end method

.method public setOnItemLongClickListener(Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemLongClickListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemLongClickListener;

    .prologue
    .line 330
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->isLongClickable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 331
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->setLongClickable(Z)V

    .line 333
    :cond_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOnItemLongClickListener:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemLongClickListener;

    .line 334
    return-void
.end method

.method public setOnItemSelectedListener(Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemSelectedListener;

    .prologue
    .line 385
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mOnItemSelectedListener:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemSelectedListener;

    .line 386
    return-void
.end method

.method protected setSelectedPositionInt(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1174
    .local p0, "this":Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;, "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView<TT;>;"
    iput p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSelectedPosition:I

    .line 1175
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getItemIdAtPosition(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->mSelectedColId:J

    .line 1176
    return-void
.end method

.method public abstract setSelection(I)V
.end method
