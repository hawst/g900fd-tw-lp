.class public Lcom/sec/samsung/gallery/view/utils/DataPath;
.super Ljava/lang/Object;
.source "DataPath.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/utils/DataPath$1;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    return-void
.end method

.method private static checkAllTopSetPath(Landroid/content/Context;ILcom/sec/samsung/gallery/core/MediaType$MediaFilterType;Ljava/lang/StringBuilder;)Ljava/lang/String;
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "viewByTypeIndex"    # I
    .param p2, "mediaFilterType"    # Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;
    .param p3, "path"    # Ljava/lang/StringBuilder;

    .prologue
    .line 267
    const/4 v13, 0x0

    .local v13, "topPath":Ljava/lang/String;
    move-object/from16 v14, p0

    .line 268
    check-cast v14, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v3

    .local v3, "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    move-object/from16 v14, p0

    .line 269
    check-cast v14, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v11

    .line 270
    .local v11, "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    const/4 v1, 0x7

    .line 271
    .local v1, "bit":I
    sget-object v14, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->IMAGE:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-object/from16 v0, p2

    if-ne v0, v14, :cond_7

    .line 272
    const/4 v1, 0x5

    .line 276
    :cond_0
    :goto_0
    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v13

    .line 277
    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ","

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    invoke-static/range {p0 .. p0}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->isCloudSupported(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_8

    invoke-static/range {p0 .. p0}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->isCloudContentSyncOn(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_8

    const/4 v4, 0x1

    .line 281
    .local v4, "mIsDropBoxAccountActive":Z
    :goto_1
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-nez v14, :cond_9

    invoke-static/range {p0 .. p0}, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->isSNSAccountsActive(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_9

    const/4 v6, 0x1

    .line 283
    .local v6, "mIsSNSAccountActive":Z
    :goto_2
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-nez v14, :cond_a

    invoke-static/range {p0 .. p0}, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v14

    array-length v14, v14

    if-eqz v14, :cond_a

    const/4 v5, 0x1

    .line 285
    .local v5, "mIsPicasaAccountActive":Z
    :goto_3
    if-eqz v4, :cond_2

    .line 286
    const/16 v1, 0x8

    .line 287
    sget-object v14, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->IMAGE:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-object/from16 v0, p2

    if-ne v0, v14, :cond_b

    .line 288
    const/16 v1, 0x12

    .line 292
    :cond_1
    :goto_4
    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    .line 293
    .local v2, "cloud":Ljava/lang/String;
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ","

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    .end local v2    # "cloud":Ljava/lang/String;
    :cond_2
    if-eqz v6, :cond_3

    .line 297
    const/16 v14, 0x9

    invoke-virtual {v3, v14}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v10

    .line 298
    .local v10, "sns":Ljava/lang/String;
    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ","

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301
    .end local v10    # "sns":Ljava/lang/String;
    :cond_3
    if-eqz v5, :cond_5

    .line 302
    const/16 v1, 0xa

    .line 303
    sget-object v14, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->IMAGE:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-object/from16 v0, p2

    if-ne v0, v14, :cond_c

    .line 304
    const/16 v1, 0xf

    .line 308
    :cond_4
    :goto_5
    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    .line 309
    .local v8, "picasa":Ljava/lang/String;
    move-object/from16 v0, p3

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ","

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    .end local v8    # "picasa":Ljava/lang/String;
    :cond_5
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloud:Z

    if-eqz v14, :cond_6

    .line 312
    new-instance v9, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-direct {v9}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;-><init>()V

    .line 313
    .local v9, "refer":Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    move-object/from16 v0, p0

    invoke-virtual {v9, v0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->isTCloudAccoutActivated(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_d

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v14

    sget-object v15, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_NORMAL:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v14, v15, :cond_d

    const/4 v7, 0x1

    .line 315
    .local v7, "mIsTCloudAccountActive":Z
    :goto_6
    if-eqz v7, :cond_6

    .line 316
    const/16 v14, 0xc

    invoke-virtual {v3, v14}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v12

    .line 317
    .local v12, "tcloud":Ljava/lang/String;
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ","

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    .end local v7    # "mIsTCloudAccountActive":Z
    .end local v9    # "refer":Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    .end local v12    # "tcloud":Ljava/lang/String;
    :cond_6
    const-string/jumbo v14, "}"

    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 321
    invoke-virtual/range {p3 .. p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 322
    const-string v14, "gallerytest"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "===========path"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    return-object v13

    .line 273
    .end local v4    # "mIsDropBoxAccountActive":Z
    .end local v5    # "mIsPicasaAccountActive":Z
    .end local v6    # "mIsSNSAccountActive":Z
    :cond_7
    sget-object v14, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-object/from16 v0, p2

    if-ne v0, v14, :cond_0

    .line 274
    const/4 v1, 0x6

    goto/16 :goto_0

    .line 279
    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 281
    .restart local v4    # "mIsDropBoxAccountActive":Z
    :cond_9
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 283
    .restart local v6    # "mIsSNSAccountActive":Z
    :cond_a
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 289
    .restart local v5    # "mIsPicasaAccountActive":Z
    :cond_b
    sget-object v14, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-object/from16 v0, p2

    if-ne v0, v14, :cond_1

    .line 290
    const/16 v1, 0x13

    goto/16 :goto_4

    .line 305
    :cond_c
    sget-object v14, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-object/from16 v0, p2

    if-ne v0, v14, :cond_4

    .line 306
    const/16 v1, 0x10

    goto/16 :goto_5

    .line 313
    .restart local v9    # "refer":Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    :cond_d
    const/4 v7, 0x0

    goto :goto_6
.end method

.method private static getCurrentFilterType(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;
    .locals 2
    .param p0, "galleryActivity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    .line 75
    .local v1, "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v0

    .line 76
    .local v0, "currentFilterType":Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;
    return-object v0
.end method

.method public static getOneAlbumPath(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Ljava/lang/String;
    .locals 8
    .param p0, "galleryActivity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/16 v7, 0x20

    const/4 v6, 0x2

    .line 35
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    .line 36
    .local v3, "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    .line 37
    .local v1, "currentTabTag":Lcom/sec/samsung/gallery/core/TabTagType;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v4

    invoke-static {p0, v4}, Lcom/sec/samsung/gallery/view/utils/DataPath;->getViewByTopSetPath(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 39
    .local v2, "setPath":Ljava/lang/String;
    const/4 v0, 0x0

    .line 40
    .local v0, "clusterdPath":Ljava/lang/String;
    sget-object v4, Lcom/sec/samsung/gallery/view/utils/DataPath$1;->$SwitchMap$com$sec$samsung$gallery$core$TabTagType:[I

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/core/TabTagType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 58
    move-object v0, v2

    .line 62
    :cond_0
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/allinone"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 42
    :pswitch_0
    invoke-static {v2, v6}, Lcom/sec/samsung/gallery/view/utils/DataPath;->toClusterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 44
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v4

    invoke-interface {v4, v6}, Lcom/sec/android/gallery3d/app/GalleryApp;->setCurrentClusterType(I)V

    goto :goto_0

    .line 48
    :pswitch_1
    invoke-static {v2, v7}, Lcom/sec/samsung/gallery/view/utils/DataPath;->toClusterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 49
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v4

    invoke-interface {v4, v7}, Lcom/sec/android/gallery3d/app/GalleryApp;->setCurrentClusterType(I)V

    goto :goto_0

    .line 40
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getTopMediaSetPath(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Ljava/lang/String;
    .locals 4
    .param p0, "galleryActivity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    .line 29
    .local v0, "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    .line 30
    .local v2, "topSetPath":Ljava/lang/String;
    invoke-static {p0}, Lcom/sec/samsung/gallery/view/utils/DataPath;->getCurrentFilterType(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/utils/DataPath;->toFilterPath(Ljava/lang/String;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Ljava/lang/String;

    move-result-object v1

    .line 31
    .local v1, "filteredPath":Ljava/lang/String;
    return-object v1
.end method

.method public static getViewByTopSetPath(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "viewByTypeIndex"    # I

    .prologue
    .line 80
    sget-object v0, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->IMAGE_AND_VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    invoke-static {p0, p1, v0}, Lcom/sec/samsung/gallery/view/utils/DataPath;->getViewByTopSetPath(Landroid/content/Context;ILcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getViewByTopSetPath(Landroid/content/Context;ILcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "viewByTypeIndex"    # I
    .param p2, "mediaFilterType"    # Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    .prologue
    const/4 v9, 0x1

    .line 84
    move-object v7, p0

    check-cast v7, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    .local v1, "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    move-object v7, p0

    .line 85
    check-cast v7, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    .line 86
    .local v3, "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    const/4 v4, 0x0

    .line 87
    .local v4, "topPath":Ljava/lang/String;
    const/4 v6, 0x0

    .line 88
    .local v6, "totalViewByOptionSelected":I
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v7, "/combo/{"

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 89
    .local v2, "path":Ljava/lang/StringBuilder;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v7

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/util/GalleryUtils;->IsCategoryView(Lcom/sec/samsung/gallery/core/TabTagType;Ljava/lang/Boolean;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 90
    const/4 v0, 0x7

    .line 91
    .local v0, "bit":I
    sget-object v7, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->IMAGE:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-ne p2, v7, :cond_1

    .line 92
    const/4 v0, 0x5

    .line 96
    :cond_0
    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, v4

    .line 164
    .end local v0    # "bit":I
    .end local v4    # "topPath":Ljava/lang/String;
    .local v5, "topPath":Ljava/lang/String;
    :goto_1
    return-object v5

    .line 93
    .end local v5    # "topPath":Ljava/lang/String;
    .restart local v0    # "bit":I
    .restart local v4    # "topPath":Ljava/lang/String;
    :cond_1
    sget-object v7, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-ne p2, v7, :cond_0

    .line 94
    const/4 v0, 0x6

    goto :goto_0

    .line 99
    .end local v0    # "bit":I
    :cond_2
    sget-object v7, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v7, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 112
    invoke-static {p0, p1, p2, v2}, Lcom/sec/samsung/gallery/view/utils/DataPath;->checkAllTopSetPath(Landroid/content/Context;ILcom/sec/samsung/gallery/core/MediaType$MediaFilterType;Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v4

    move-object v5, v4

    .line 113
    .end local v4    # "topPath":Ljava/lang/String;
    .restart local v5    # "topPath":Ljava/lang/String;
    goto :goto_1

    .line 115
    .end local v5    # "topPath":Ljava/lang/String;
    .restart local v4    # "topPath":Ljava/lang/String;
    :cond_3
    sget-object v7, Lcom/sec/samsung/gallery/core/ViewByFilterType;->LOCAL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v7, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 116
    const/4 v0, 0x7

    .line 117
    .restart local v0    # "bit":I
    sget-object v7, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->IMAGE:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-ne p2, v7, :cond_c

    .line 118
    const/4 v0, 0x5

    .line 122
    :cond_4
    :goto_2
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    .line 123
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    add-int/lit8 v6, v6, 0x1

    .line 126
    .end local v0    # "bit":I
    :cond_5
    sget-object v7, Lcom/sec/samsung/gallery/core/ViewByFilterType;->CLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v7, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 127
    const/16 v0, 0x8

    .line 128
    .restart local v0    # "bit":I
    sget-object v7, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->IMAGE:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-ne p2, v7, :cond_d

    .line 129
    const/16 v0, 0x12

    .line 133
    :cond_6
    :goto_3
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    .line 134
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    add-int/lit8 v6, v6, 0x1

    .line 137
    .end local v0    # "bit":I
    :cond_7
    sget-object v7, Lcom/sec/samsung/gallery/core/ViewByFilterType;->PICASA:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v7, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 138
    const/16 v0, 0xa

    .line 139
    .restart local v0    # "bit":I
    sget-object v7, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->IMAGE:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-ne p2, v7, :cond_e

    .line 140
    const/16 v0, 0xf

    .line 144
    :cond_8
    :goto_4
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    .line 145
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    add-int/lit8 v6, v6, 0x1

    .line 148
    .end local v0    # "bit":I
    :cond_9
    sget-object v7, Lcom/sec/samsung/gallery/core/ViewByFilterType;->FACEBOOK:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v7, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 149
    const/16 v7, 0x9

    invoke-virtual {v1, v7}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    .line 150
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    add-int/lit8 v6, v6, 0x1

    .line 153
    :cond_a
    sget-object v7, Lcom/sec/samsung/gallery/core/ViewByFilterType;->TCLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v7, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 154
    const/16 v7, 0xc

    invoke-virtual {v1, v7}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    .line 155
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    add-int/lit8 v6, v6, 0x1

    .line 159
    :cond_b
    if-ne v6, v9, :cond_f

    move-object v5, v4

    .line 160
    .end local v4    # "topPath":Ljava/lang/String;
    .restart local v5    # "topPath":Ljava/lang/String;
    goto/16 :goto_1

    .line 119
    .end local v5    # "topPath":Ljava/lang/String;
    .restart local v0    # "bit":I
    .restart local v4    # "topPath":Ljava/lang/String;
    :cond_c
    sget-object v7, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-ne p2, v7, :cond_4

    .line 120
    const/4 v0, 0x6

    goto/16 :goto_2

    .line 130
    :cond_d
    sget-object v7, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-ne p2, v7, :cond_6

    .line 131
    const/16 v0, 0x13

    goto/16 :goto_3

    .line 141
    :cond_e
    sget-object v7, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-ne p2, v7, :cond_8

    .line 142
    const/16 v0, 0x10

    goto :goto_4

    .line 162
    .end local v0    # "bit":I
    :cond_f
    const-string/jumbo v7, "}"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, v4

    .line 164
    .end local v4    # "topPath":Ljava/lang/String;
    .restart local v5    # "topPath":Ljava/lang/String;
    goto/16 :goto_1
.end method

.method public static getViewByTopSetPath(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "viewByTypeIndex"    # I
    .param p2, "prevTopSetPath"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x0

    .line 168
    sget-object v8, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v8, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 236
    .end local p2    # "prevTopSetPath":Ljava/lang/String;
    :goto_0
    return-object p2

    .line 172
    .restart local p2    # "prevTopSetPath":Ljava/lang/String;
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 175
    .local v4, "newPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v8, ","

    invoke-virtual {p2, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 177
    .local v6, "prevPaths":[Ljava/lang/String;
    aget-object v8, v6, v11

    const-string/jumbo v9, "{"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 178
    .local v2, "index":I
    const/4 v8, -0x1

    if-eq v2, v8, :cond_1

    .line 180
    aget-object v8, v6, v11

    add-int/lit8 v9, v2, 0x1

    invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v11

    .line 182
    array-length v8, v6

    add-int/lit8 v8, v8, -0x1

    array-length v9, v6

    add-int/lit8 v9, v9, -0x1

    aget-object v9, v6, v9

    array-length v10, v6

    add-int/lit8 v10, v10, -0x1

    aget-object v10, v6, v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v9, v11, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v8

    .line 185
    :cond_1
    array-length v7, v6

    .line 186
    .local v7, "prevPathsSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v7, :cond_8

    .line 188
    sget-object v8, Lcom/sec/samsung/gallery/core/ViewByFilterType;->LOCAL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v8, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 189
    aget-object v8, v6, v0

    const-string v9, "mtp"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    aget-object v8, v6, v0

    const-string v9, "local"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 190
    :cond_2
    aget-object v8, v6, v0

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 186
    :cond_3
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 194
    :cond_4
    sget-object v8, Lcom/sec/samsung/gallery/core/ViewByFilterType;->CLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v8, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 195
    aget-object v8, v6, v0

    const-string v9, "cloud"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 196
    aget-object v8, v6, v0

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 200
    :cond_5
    sget-object v8, Lcom/sec/samsung/gallery/core/ViewByFilterType;->PICASA:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v8, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 201
    aget-object v8, v6, v0

    const-string v9, "picasa"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 202
    aget-object v8, v6, v0

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 206
    :cond_6
    sget-object v8, Lcom/sec/samsung/gallery/core/ViewByFilterType;->FACEBOOK:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v8, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 207
    aget-object v8, v6, v0

    const-string/jumbo v9, "sns"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 208
    aget-object v8, v6, v0

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 212
    :cond_7
    sget-object v8, Lcom/sec/samsung/gallery/core/ViewByFilterType;->TCLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v8, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 213
    aget-object v8, v6, v0

    const-string/jumbo v9, "tCloud"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 214
    aget-object v8, v6, v0

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 220
    :cond_8
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-nez v8, :cond_9

    .line 222
    const/4 p2, 0x0

    goto/16 :goto_0

    .line 223
    :cond_9
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_a

    .line 225
    invoke-virtual {v4, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    move-object p2, v8

    goto/16 :goto_0

    .line 229
    :cond_a
    new-instance v5, Ljava/lang/StringBuffer;

    const-string v8, "/combo/{"

    invoke-direct {v5, v8}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 230
    .local v5, "newTopSetPath":Ljava/lang/StringBuffer;
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 231
    .local v3, "newPath":Ljava/lang/String;
    invoke-virtual {v5, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 234
    .end local v3    # "newPath":Ljava/lang/String;
    :cond_b
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v5, v8}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string/jumbo v9, "}"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 236
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0
.end method

.method public static getViewByTopSetPath(Landroid/content/Context;Lcom/sec/samsung/gallery/core/ViewByFilterType;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "viewByType"    # Lcom/sec/samsung/gallery/core/ViewByFilterType;

    .prologue
    .line 240
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    .line 241
    .local v0, "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    const/4 v1, 0x0

    .line 242
    .local v1, "topPath":Ljava/lang/String;
    sget-object v2, Lcom/sec/samsung/gallery/view/utils/DataPath$1;->$SwitchMap$com$sec$samsung$gallery$core$ViewByFilterType:[I

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 260
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v1

    .line 263
    :goto_0
    return-object v1

    .line 244
    :pswitch_0
    const/4 v2, 0x7

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v1

    .line 245
    goto :goto_0

    .line 247
    :pswitch_1
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v1

    .line 248
    goto :goto_0

    .line 250
    :pswitch_2
    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v1

    .line 251
    goto :goto_0

    .line 253
    :pswitch_3
    const/16 v2, 0x9

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v1

    .line 254
    goto :goto_0

    .line 256
    :pswitch_4
    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v1

    .line 257
    goto :goto_0

    .line 242
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static toClusterPath(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p0, "setPath"    # Ljava/lang/String;
    .param p1, "clusterType"    # I

    .prologue
    .line 66
    invoke-static {p0, p1}, Lcom/sec/android/gallery3d/app/FilterUtils;->newClusterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static toFilterPath(Ljava/lang/String;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Ljava/lang/String;
    .locals 1
    .param p0, "clusterdPath"    # Ljava/lang/String;
    .param p1, "filterType"    # Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    .prologue
    .line 70
    invoke-static {p1}, Lcom/sec/android/gallery3d/app/FilterUtils;->toFilterType(Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)I

    move-result v0

    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/app/FilterUtils;->switchFilterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
