.class public Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;
.super Ljava/lang/Object;
.source "SlideshowDefaultMusicDialog.java"


# static fields
.field private static final DEFAULT_RINGTONE_LIST:[Landroid/net/Uri;

.field private static final PREFIX_RESOURCE_URI:Ljava/lang/String; = "android.resource://com.sec.android.gallery3d/"

.field private static final TAG:Ljava/lang/String;

.field private static mRingtoneNames:[Ljava/lang/String;


# instance fields
.field private index:I

.field private final mContext:Landroid/content/Context;

.field private mExtraRingtoneUri:Landroid/net/Uri;

.field private mMusicListAdapter:Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;

.field private mMusicListDialog:Landroid/app/AlertDialog;

.field private mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

.field private mOnCancelButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mOnDoneButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private mOnItemClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private mOnKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mRingtoneList:[Landroid/net/Uri;

.field private mRingtoneNameList:[Ljava/lang/String;

.field private mRingtoneUri:Landroid/net/Uri;

.field private mSelectedMusicUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    const-class v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->TAG:Ljava/lang/String;

    .line 33
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/net/Uri;

    const/4 v1, 0x0

    const-string v2, "android.resource://com.sec.android.gallery3d/2131230723"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.resource://com.sec.android.gallery3d/2131230724"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "android.resource://com.sec.android.gallery3d/2131230725"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->DEFAULT_RINGTONE_LIST:[Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ringtoneUri"    # Landroid/net/Uri;
    .param p3, "extraRingtone"    # Landroid/net/Uri;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mSelectedMusicUri:Landroid/net/Uri;

    .line 153
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$1;-><init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mOnItemClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 183
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$2;-><init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mOnCancelButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 196
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$3;-><init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mOnDoneButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 217
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$4;-><init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mOnKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 232
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$5;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$5;-><init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 60
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mContext:Landroid/content/Context;

    .line 61
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mRingtoneUri:Landroid/net/Uri;

    .line 62
    iput-object p3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mExtraRingtoneUri:Landroid/net/Uri;

    .line 63
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    .line 64
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f0300d8

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicListAdapter:Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;

    .line 65
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->createDialog()V

    .line 66
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;)Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicListAdapter:Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicListDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mSelectedMusicUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mSelectedMusicUri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->checkedItemPosition()I

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->index:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;
    .param p1, "x1"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->index:I

    return p1
.end method

.method private checkedItemPosition()I
    .locals 6

    .prologue
    .line 129
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getMusicSelected(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 130
    .local v2, "str":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicListAdapter:Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;->getCount()I

    move-result v5

    if-gt v0, v5, :cond_1

    .line 131
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicListAdapter:Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;

    invoke-virtual {v5, v0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;

    .line 132
    .local v1, "selectedItem":Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;->getUri()Landroid/net/Uri;

    move-result-object v3

    .line 133
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    .line 134
    .local v4, "uriStr":Ljava/lang/String;
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 137
    .end local v0    # "i":I
    .end local v1    # "selectedItem":Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v4    # "uriStr":Ljava/lang/String;
    :goto_1
    return v0

    .line 130
    .restart local v0    # "i":I
    .restart local v1    # "selectedItem":Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;
    .restart local v3    # "uri":Landroid/net/Uri;
    .restart local v4    # "uriStr":Ljava/lang/String;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 137
    .end local v1    # "selectedItem":Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v4    # "uriStr":Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static declared-synchronized getDefaultRingtoneNames(Landroid/content/Context;Z)[Ljava/lang/String;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "refresh"    # Z

    .prologue
    .line 90
    const-class v3, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    monitor-enter v3

    if-nez p1, :cond_0

    :try_start_0
    sget-object v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mRingtoneNames:[Ljava/lang/String;

    if-nez v2, :cond_1

    .line 91
    :cond_0
    sget-object v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->DEFAULT_RINGTONE_LIST:[Landroid/net/Uri;

    array-length v2, v2

    new-array v2, v2, [Ljava/lang/String;

    sput-object v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mRingtoneNames:[Ljava/lang/String;

    .line 93
    const/4 v0, 0x0

    .local v0, "i":I
    sget-object v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->DEFAULT_RINGTONE_LIST:[Landroid/net/Uri;

    array-length v1, v2

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 94
    sget-object v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mRingtoneNames:[Ljava/lang/String;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0281

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    add-int/lit8 v7, v0, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v0

    .line 93
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 99
    .end local v0    # "i":I
    .end local v1    # "n":I
    :cond_1
    sget-object v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mRingtoneNames:[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v3

    return-object v2

    .line 90
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method private static getDefaultRingtoneTitle(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "ringtonUri"    # Landroid/net/Uri;

    .prologue
    .line 103
    const/4 v3, 0x1

    invoke-static {p0, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->getDefaultRingtoneNames(Landroid/content/Context;Z)[Ljava/lang/String;

    move-result-object v2

    .line 104
    .local v2, "names":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    sget-object v3, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->DEFAULT_RINGTONE_LIST:[Landroid/net/Uri;

    array-length v1, v3

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 105
    sget-object v3, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->DEFAULT_RINGTONE_LIST:[Landroid/net/Uri;

    aget-object v3, v3, v0

    invoke-virtual {p1, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 106
    aget-object v3, v2, v0

    .line 109
    :goto_1
    return-object v3

    .line 104
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 109
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private static getExtraMediaTitle(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x0

    .line 114
    const/4 v7, 0x0

    .line 115
    .local v7, "mediaTitle":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 117
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    const-string/jumbo v0, "title"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 119
    .local v8, "titleColumn":I
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 122
    .end local v8    # "titleColumn":I
    :cond_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 125
    return-object v7

    .line 122
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static getMediaTitle(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 82
    const-string v0, "android.resource"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    invoke-static {p0, p1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->getDefaultRingtoneTitle(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 85
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->getExtraMediaTitle(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public createDialog()V
    .locals 4

    .prologue
    .line 142
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e0272

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicListAdapter:Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->checkedItemPosition()I

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mOnItemClickListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e0046

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mOnCancelButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e0070

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mOnDoneButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicListDialog:Landroid/app/AlertDialog;

    .line 149
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicListDialog:Landroid/app/AlertDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mOnKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 150
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicListDialog:Landroid/app/AlertDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 151
    return-void
.end method

.method public dismissDialog()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicListDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicListDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicListDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 79
    :cond_0
    return-void
.end method

.method public showDialog()V
    .locals 3

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicListDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicListDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "show music dialog! "

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicListDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 73
    :cond_0
    return-void
.end method

.method public stopMusic()V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->pauseMusic()V

    .line 242
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->destroy()V

    .line 244
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    .line 245
    return-void
.end method
