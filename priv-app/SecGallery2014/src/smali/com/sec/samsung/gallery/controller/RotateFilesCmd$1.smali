.class Lcom/sec/samsung/gallery/controller/RotateFilesCmd$1;
.super Ljava/lang/Object;
.source "RotateFilesCmd.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/OnProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/RotateFilesCmd;->handleRotateMedias(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/RotateFilesCmd;

.field final synthetic val$dataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field final synthetic val$progressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/RotateFilesCmd;Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;Lcom/sec/android/gallery3d/data/DataManager;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/RotateFilesCmd;

    iput-object p2, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd$1;->val$progressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    iput-object p3, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd$1;->val$dataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 3
    .param p1, "object"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 73
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 74
    .local v0, "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd$1;->val$dataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/RotateFilesCmd;

    iget v2, v2, Lcom/sec/samsung/gallery/controller/RotateFilesCmd;->mAngle:I

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/gallery3d/data/DataManager;->rotate(Lcom/sec/android/gallery3d/data/Path;I)V

    .line 75
    const/4 v1, 0x1

    return v1
.end method

.method public handleOperation(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 80
    .local p1, "objectList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public onCompleted(Z)V
    .locals 3
    .param p1, "result"    # Z

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd$1;->val$progressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->closeProgressDialog()V

    .line 61
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/RotateFilesCmd;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    .line 62
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/RotateFilesCmd;

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "EXIT_SELECTION_MODE"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 64
    return-void
.end method

.method public onProgress(II)V
    .locals 1
    .param p1, "progress"    # I
    .param p2, "total"    # I

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd$1;->val$progressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->updateProgress(II)V

    .line 69
    return-void
.end method
