.class public final Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;
.super Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;
.source "MoreInfoUserTagAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$UserTagLoadTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MoreInfoTagAdapter"


# instance fields
.field private mDeleteTags:Landroid/widget/Toast;

.field private mRemoveClickListener:Landroid/view/View$OnClickListener;

.field private mUserTagCountMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 28
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mRemoveClickListener:Landroid/view/View$OnClickListener;

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mUserTagCountMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;
    .param p1, "x1"    # Ljava/util/HashMap;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mUserTagCountMap:Ljava/util/HashMap;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mDeleteTags:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mDeleteTags:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->isEdited()Z

    move-result v0

    return v0
.end method

.method static synthetic access$301(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 21
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->initData(Landroid/content/Context;)V

    return-void
.end method

.method private isEdited()Z
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mInterim:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mInfos:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mInfos:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mInterim:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public editDone()V
    .locals 5

    .prologue
    .line 123
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mInfos:Ljava/util/ArrayList;

    monitor-enter v1

    .line 124
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->isDataSetChanged()Z

    move-result v0

    if-nez v0, :cond_0

    .line 125
    monitor-exit v1

    .line 133
    :goto_0
    return-void

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->getBaseUri()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mInfos:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mInterim:Ljava/util/ArrayList;

    invoke-static {v0, v2, v3, v4}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->updateUserTag(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 128
    const-string v0, "MoreInfoTagAdapter"

    const-string v2, "Error during update UserTag"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mInfos:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 131
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mInfos:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mInterim:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 132
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getNoInfoString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mContext:Landroid/content/Context;

    const v1, 0x7f0e0469

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTags()Ljava/lang/String;
    .locals 4

    .prologue
    .line 101
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mInterim:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_0

    .line 102
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->getNoInfoString()Ljava/lang/String;

    move-result-object v3

    .line 112
    :goto_0
    return-object v3

    .line 104
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mInterim:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 105
    .local v2, "size":I
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_2

    .line 107
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mInterim:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    add-int/lit8 v3, v2, -0x1

    if-ge v1, v3, :cond_1

    .line 109
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 112
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 81
    if-nez p2, :cond_0

    .line 82
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f03008c

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 86
    :cond_0
    const v2, 0x7f0f0188

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 87
    .local v1, "textView":Landroid/widget/TextView;
    if-eqz v1, :cond_1

    .line 88
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mInterim:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    :cond_1
    const v2, 0x7f0f0189

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 92
    .local v0, "deleteIcon":Landroid/widget/ImageView;
    if-eqz v0, :cond_2

    .line 93
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mRemoveClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setId(I)V

    .line 96
    :cond_2
    return-object p2
.end method

.method public initData(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$UserTagLoadTask;

    invoke-direct {v0, p0, p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$UserTagLoadTask;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;Landroid/content/Context;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$UserTagLoadTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 77
    return-void
.end method
