.class public Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "AndroidBeamCmd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$AndroidBeamUtils;,
        Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$SbeamPushCompleteCallback;,
        Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;,
        Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;
    }
.end annotation


# static fields
.field private static final ACTION_ABEAM_POPUP:Ljava/lang/String; = "com.android.nfc.AndroidBeamPopUp"

.field private static final ACTION_SBEAM_POPUP:Ljava/lang/String; = "com.sec.android.directshare.DirectSharePopUp"

.field private static final ACTION_START:Ljava/lang/String; = "com.sec.android.directshare.DIRECT_SHARE_START_ACTION"

.field private static final EXTRA_KEY_POPUP_MODE:Ljava/lang/String; = "POPUP_MODE"

.field private static final EXTRA_VAL_CLOUD_FILE:Ljava/lang/String; = "from_cloud_file"

.field private static final EXTRA_VAL_DRM_FILE:Ljava/lang/String; = "from_drm_file"

.field private static final EXTRA_VAL_NO_FILE:Ljava/lang/String; = "no_file_selected"

.field private static final KEY_SBEAM_ONOFF:Ljava/lang/String; = "SBeam_on_off"

.field private static final KEY_SBEAM_SUPPORT:Ljava/lang/String; = "SBeam_support"

.field private static final PACKAGE_NAME_SBEAM:Ljava/lang/String; = "com.sec.android.directshare"

.field private static final PACKAGE_NAME_SETTINGS:Ljava/lang/String; = "com.android.settings"

.field private static final PREP_NAME_SBEAM:Ljava/lang/String; = "pref_sbeam"

.field private static final TAG:Ljava/lang/String;

.field private static final TAGClass:Ljava/lang/String; = "SbeamHelper"


# instance fields
.field private final APP_MIME:Ljava/lang/String;

.field private final STATUS_IS_CLOUD_FILE:I

.field private final STATUS_IS_DRM:I

.field private final STATUS_NONE:I

.field private final STATUS_NO_FILE_SELECTED:I

.field private final STATUS_PUSH:I

.field private mActivity:Landroid/app/Activity;

.field private mBeamCb:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;

.field private mListener:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;

.field private mNdefStatus:I

.field private mNfcAdapter:Landroid/nfc/NfcAdapter;

.field private mSBeamPreference:Landroid/content/SharedPreferences;

.field private mSbeamCompleteCb:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$SbeamPushCompleteCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-class v0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 71
    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mSBeamPreference:Landroid/content/SharedPreferences;

    .line 419
    const-string/jumbo v0, "text/DirectShareGallery"

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->APP_MIME:Ljava/lang/String;

    .line 421
    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mBeamCb:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;

    .line 423
    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mSbeamCompleteCb:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$SbeamPushCompleteCallback;

    .line 426
    iput v2, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNdefStatus:I

    .line 428
    iput v2, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->STATUS_NONE:I

    .line 430
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->STATUS_PUSH:I

    .line 432
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->STATUS_NO_FILE_SELECTED:I

    .line 434
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->STATUS_IS_CLOUD_FILE:I

    .line 436
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->STATUS_IS_DRM:I

    .line 469
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->initBeamHelper()V

    return-void
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mListener:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->isSbeamOn()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    .prologue
    .line 62
    iget v0, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNdefStatus:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;
    .param p1, "x1"    # I

    .prologue
    .line 62
    iput p1, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNdefStatus:I

    return p1
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method private getSBeamPreference()Landroid/content/SharedPreferences;
    .locals 6

    .prologue
    .line 180
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mSBeamPreference:Landroid/content/SharedPreferences;

    if-eqz v3, :cond_0

    .line 181
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mSBeamPreference:Landroid/content/SharedPreferences;

    .line 198
    :goto_0
    return-object v1

    .line 183
    :cond_0
    const/4 v2, 0x0

    .line 185
    .local v2, "settingContext":Landroid/content/Context;
    :try_start_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mActivity:Landroid/app/Activity;

    const-string v4, "com.android.settings"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/app/Activity;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 192
    :goto_1
    if-nez v2, :cond_1

    .line 193
    const/4 v1, 0x0

    goto :goto_0

    .line 186
    :catch_0
    move-exception v0

    .line 187
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v3, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SbeamHelperBeamHelper : NameNotFoundException > "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 188
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 189
    .local v0, "e":Ljava/lang/SecurityException;
    sget-object v3, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SbeamHelperBeamHelper : SecurityException > "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 196
    .end local v0    # "e":Ljava/lang/SecurityException;
    :cond_1
    const-string v3, "pref_sbeam"

    const/4 v4, 0x5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 198
    .local v1, "sbeamPreference":Landroid/content/SharedPreferences;
    goto :goto_0
.end method

.method private initBeamHelper()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 92
    const/4 v1, 0x0

    .line 94
    .local v1, "settingContext":Landroid/content/Context;
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mActivity:Landroid/app/Activity;

    const-string v3, "com.android.settings"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/app/Activity;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v1

    .line 103
    :goto_0
    if-nez v1, :cond_0

    .line 159
    :goto_1
    return-void

    .line 95
    :catch_0
    move-exception v0

    .line 96
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v2, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SbeamHelperBeamHelper : NameNotFoundException > "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 97
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/SecurityException;
    sget-object v2, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SbeamHelperBeamHelper : SecurityException > "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 99
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_2
    move-exception v0

    .line 100
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    sget-object v2, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SbeamHelperBeamHelper : ArrayIndexOutOfBoundsException > "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 106
    .end local v0    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mActivity:Landroid/app/Activity;

    invoke-static {v2}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 107
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-nez v2, :cond_1

    .line 108
    sget-object v2, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;

    const-string v3, "SbeamHelperSbeamHelper : setBeamUris > can\'t load nfcadapter"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 112
    :cond_1
    new-instance v2, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;

    invoke-direct {v2, p0, v6}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;-><init>(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$1;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mBeamCb:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;

    .line 113
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->isSBeamSupported()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 114
    new-instance v2, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$SbeamPushCompleteCallback;

    invoke-direct {v2, p0, v6}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$SbeamPushCompleteCallback;-><init>(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$1;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mSbeamCompleteCb:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$SbeamPushCompleteCallback;

    .line 117
    :cond_2
    iput v5, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNdefStatus:I

    .line 120
    :try_start_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    new-instance v3, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$2;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$2;-><init>(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)V

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2, v3, v4}, Landroid/nfc/NfcAdapter;->setBeamPushUrisCallback(Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;Landroid/app/Activity;)V
    :try_end_1
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_5

    .line 145
    :goto_2
    :try_start_2
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->isSbeamOn()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 146
    sget-object v2, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;

    const-string v3, "setBeamUris : setSbeam"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mBeamCb:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mActivity:Landroid/app/Activity;

    const/4 v5, 0x0

    new-array v5, v5, [Landroid/app/Activity;

    invoke-virtual {v2, v3, v4, v5}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 148
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mSbeamCompleteCb:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$SbeamPushCompleteCallback;

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mActivity:Landroid/app/Activity;

    const/4 v5, 0x0

    new-array v5, v5, [Landroid/app/Activity;

    invoke-virtual {v2, v3, v4, v5}, Landroid/nfc/NfcAdapter;->setOnNdefPushCompleteCallback(Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 150
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2, v3, v4}, Landroid/nfc/NfcAdapter;->setBeamPushUrisCallback(Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;Landroid/app/Activity;)V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_3

    goto/16 :goto_1

    .line 156
    :catch_3
    move-exception v0

    .line 157
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_1

    .line 136
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_4
    move-exception v0

    .line 138
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    invoke-virtual {v0}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    goto :goto_2

    .line 139
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_5
    move-exception v0

    .line 140
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_1

    .line 152
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :cond_3
    :try_start_3
    sget-object v2, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;

    const-string v3, "setBeamUris : setAbeam"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mBeamCb:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mActivity:Landroid/app/Activity;

    const/4 v5, 0x0

    new-array v5, v5, [Landroid/app/Activity;

    invoke-virtual {v2, v3, v4, v5}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 154
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mActivity:Landroid/app/Activity;

    const/4 v5, 0x0

    new-array v5, v5, [Landroid/app/Activity;

    invoke-virtual {v2, v3, v4, v5}, Landroid/nfc/NfcAdapter;->setOnNdefPushCompleteCallback(Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;Landroid/app/Activity;[Landroid/app/Activity;)V
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_1
.end method

.method private isSBeamSupported()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 171
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->getSBeamPreference()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 172
    .local v1, "preferences":Landroid/content/SharedPreferences;
    if-nez v1, :cond_0

    .line 175
    .local v0, "isSupport":Z
    :goto_0
    sget-object v3, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SbeamHelperSbeamHelper : SBeam is "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v0, :cond_1

    const-string/jumbo v2, "supported"

    :goto_1
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    return v0

    .line 172
    .end local v0    # "isSupport":Z
    :cond_0
    const-string v2, "SBeam_support"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 175
    .restart local v0    # "isSupport":Z
    :cond_1
    const-string v2, "not supported"

    goto :goto_1
.end method

.method private isSbeamOn()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 162
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->isSBeamSupported()Z

    move-result v0

    .line 163
    .local v0, "isBeamSupported":Z
    if-nez v0, :cond_1

    .line 167
    :cond_0
    :goto_0
    return v2

    .line 166
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->getSBeamPreference()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 167
    .local v1, "preferences":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_0

    const-string v3, "SBeam_on_off"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    goto :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 4
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 79
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    .line 80
    .local v0, "params":[Ljava/lang/Object;
    const/4 v1, 0x0

    aget-object v1, v0, v1

    check-cast v1, Landroid/app/Activity;

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mActivity:Landroid/app/Activity;

    .line 81
    const/4 v1, 0x1

    aget-object v1, v0, v1

    check-cast v1, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mListener:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;

    .line 83
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$1;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)V

    const-string v3, "AndroidBeamInit"

    invoke-direct {v1, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 89
    return-void
.end method
