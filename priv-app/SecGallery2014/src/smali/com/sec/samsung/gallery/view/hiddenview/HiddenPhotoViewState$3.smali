.class Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$3;
.super Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;
.source "HiddenPhotoViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$3;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onNext()V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$3;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$600(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$3;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$700(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/glview/GlPhotoView;

    move-result-object v0

    const/16 v1, 0x1f40

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setAirMotionImageFling(I)V

    .line 152
    :cond_0
    return-void
.end method

.method public onPrevious()V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$3;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$600(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$3;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$700(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/glview/GlPhotoView;

    move-result-object v0

    const/16 v1, -0x1f40

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setAirMotionImageFling(I)V

    .line 146
    :cond_0
    return-void
.end method
