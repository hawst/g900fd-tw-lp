.class public Lcom/sec/samsung/gallery/util/FontUtil;
.super Ljava/lang/Object;
.source "FontUtil.java"


# static fields
.field public static mBoldFont:Landroid/graphics/Typeface;

.field public static mRegularFont:Landroid/graphics/Typeface;

.field public static mRobotoLightFont:Landroid/graphics/Typeface;

.field public static mRobotoRegularFont:Landroid/graphics/Typeface;


# direct methods
.method public constructor <init>(Landroid/content/res/AssetManager;)V
    .locals 0
    .param p1, "asset"    # Landroid/content/res/AssetManager;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    return-void
.end method

.method public static getBoldFont()Landroid/graphics/Typeface;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/samsung/gallery/util/FontUtil;->mBoldFont:Landroid/graphics/Typeface;

    if-nez v0, :cond_0

    .line 29
    const/4 v0, 0x1

    invoke-static {v0}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/util/FontUtil;->mBoldFont:Landroid/graphics/Typeface;

    .line 31
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/util/FontUtil;->mBoldFont:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public static getRegularFont()Landroid/graphics/Typeface;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/samsung/gallery/util/FontUtil;->mRegularFont:Landroid/graphics/Typeface;

    if-nez v0, :cond_0

    .line 22
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    sput-object v0, Lcom/sec/samsung/gallery/util/FontUtil;->mRegularFont:Landroid/graphics/Typeface;

    .line 24
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/util/FontUtil;->mRegularFont:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public static getRobotoLightFont()Landroid/graphics/Typeface;
    .locals 2

    .prologue
    .line 35
    sget-object v0, Lcom/sec/samsung/gallery/util/FontUtil;->mRobotoLightFont:Landroid/graphics/Typeface;

    if-nez v0, :cond_0

    .line 36
    const-string v0, "sec-roboto-light"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/util/FontUtil;->mRobotoLightFont:Landroid/graphics/Typeface;

    .line 38
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/util/FontUtil;->mRobotoLightFont:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public static getRobotoRegularFont()Landroid/graphics/Typeface;
    .locals 2

    .prologue
    .line 42
    sget-object v0, Lcom/sec/samsung/gallery/util/FontUtil;->mRobotoRegularFont:Landroid/graphics/Typeface;

    if-nez v0, :cond_0

    .line 43
    const-string v0, "sec-roboto-regular"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/util/FontUtil;->mRobotoRegularFont:Landroid/graphics/Typeface;

    .line 45
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/util/FontUtil;->mRobotoRegularFont:Landroid/graphics/Typeface;

    return-object v0
.end method
