.class public Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "ConvertMultiFormatToMP4Cmd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd$ConvertInfo;,
        Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd$ConversionAsyncTask;
    }
.end annotation


# static fields
.field private static final DEFAULT_OUTPUT_SIZE:I = 0x96

.field private static final IMAGE_CONVERSION_MIN_BYTES:I = 0xa00000

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/Dialog;

.field private mUseSDCard:Ljava/lang/Boolean;

.field private type:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 61
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mUseSDCard:Ljava/lang/Boolean;

    .line 408
    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->callMyFiles()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->updateItemDB(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->updateItemDBForDuration(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->addSelectionModeProxy(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->type:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$900()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private addSelectionModeProxy(Ljava/lang/String;)V
    .locals 7
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 374
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mContext:Landroid/content/Context;

    check-cast v6, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    .line 375
    .local v4, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->getUriFrom(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 376
    .local v5, "uri":Landroid/net/Uri;
    if-nez v5, :cond_0

    const-string v6, "jpg"

    invoke-virtual {p1, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 377
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    .line 379
    :cond_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 380
    .local v0, "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    .line 381
    .local v1, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    .line 382
    .local v3, "path":Lcom/sec/android/gallery3d/data/Path;
    const/4 v6, 0x0

    invoke-virtual {v1, v3, v6}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;Z)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 383
    .local v2, "media":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 384
    return-void
.end method

.method private callMyFiles()V
    .locals 4

    .prologue
    .line 201
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 202
    .local v1, "intent":Landroid/content/Intent;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->getMyFilesName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 203
    const/high16 v2, 0x16000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 206
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    :goto_0
    return-void

    .line 207
    :catch_0
    move-exception v0

    .line 208
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->TAG:Ljava/lang/String;

    const-string v3, "Activity Not Found"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private checkLowStorage(Landroid/content/Context;Ljava/lang/String;I)Z
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "outputFullFilePath"    # Ljava/lang/String;
    .param p3, "imagesCount"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 214
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    .line 215
    .local v4, "storagePath":Ljava/lang/String;
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mUseSDCard:Ljava/lang/Boolean;

    .line 217
    :try_start_0
    const-string v7, "/storage/extSdCard/"

    invoke-virtual {p2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 218
    const-string v4, "/storage/extSdCard/"

    .line 219
    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mUseSDCard:Ljava/lang/Boolean;

    .line 221
    :cond_0
    new-instance v1, Landroid/os/StatFs;

    invoke-direct {v1, v4}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 222
    .local v1, "stat":Landroid/os/StatFs;
    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v7

    int-to-long v10, v7

    mul-long v2, v8, v10

    .line 223
    .local v2, "remaining":J
    const/high16 v7, 0xa00000

    mul-int/2addr v7, p3

    int-to-long v8, v7

    cmp-long v7, v2, v8

    if-gez v7, :cond_1

    .line 224
    invoke-virtual {p0, v2, v3}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->createDialog(J)V

    .line 225
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mDialog:Landroid/app/Dialog;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v7}, Landroid/app/Dialog;->isShowing()Z

    move-result v7

    if-nez v7, :cond_1

    .line 226
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v6}, Landroid/app/Dialog;->show()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 234
    .end local v1    # "stat":Landroid/os/StatFs;
    .end local v2    # "remaining":J
    :goto_0
    return v5

    .restart local v1    # "stat":Landroid/os/StatFs;
    .restart local v2    # "remaining":J
    :cond_1
    move v5, v6

    .line 230
    goto :goto_0

    .line 231
    .end local v1    # "stat":Landroid/os/StatFs;
    .end local v2    # "remaining":J
    :catch_0
    move-exception v0

    .line 232
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " checkLowStorageforMedia() for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 233
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0
.end method

.method private checkMyfiles()Z
    .locals 6

    .prologue
    .line 177
    const/4 v2, 0x0

    .line 179
    .local v2, "myFilesExist":Z
    :try_start_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->getMyFilesName()Landroid/content/ComponentName;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 181
    .local v0, "activityInfo":Landroid/content/pm/ActivityInfo;
    if-eqz v0, :cond_0

    .line 182
    const/4 v2, 0x1

    .line 187
    .end local v0    # "activityInfo":Landroid/content/pm/ActivityInfo;
    :cond_0
    :goto_0
    return v2

    .line 184
    :catch_0
    move-exception v1

    .line 185
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private createDialogForMyFiles(J)V
    .locals 11
    .param p1, "remaining"    # J

    .prologue
    const-wide/16 v8, 0x400

    const/4 v6, 0x0

    .line 125
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 128
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mUseSDCard:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 129
    const v1, 0x7f0e0290

    .line 134
    .local v1, "stringId":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mContext:Landroid/content/Context;

    const v3, 0x7f0e021b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 135
    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 136
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    div-long v4, p1, v8

    div-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 137
    const v2, 0x7f0e00db

    new-instance v3, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd$1;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd$1;-><init>(Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 144
    const v2, 0x7f0e0046

    new-instance v3, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd$2;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd$2;-><init>(Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 150
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mDialog:Landroid/app/Dialog;

    .line 151
    return-void

    .line 131
    .end local v1    # "stringId":I
    :cond_0
    const v1, 0x7f0e028e

    .restart local v1    # "stringId":I
    goto :goto_0
.end method

.method private createDialogNoMyFiles(J)V
    .locals 11
    .param p1, "remaining"    # J

    .prologue
    const-wide/16 v8, 0x400

    const/4 v6, 0x0

    .line 154
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 157
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mUseSDCard:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 158
    const v1, 0x7f0e028f

    .line 163
    .local v1, "stringId":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mContext:Landroid/content/Context;

    const v3, 0x7f0e021b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 164
    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 165
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    div-long v4, p1, v8

    div-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 166
    const v2, 0x7f0e00db

    new-instance v3, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd$3;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd$3;-><init>(Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 173
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mDialog:Landroid/app/Dialog;

    .line 174
    return-void

    .line 160
    .end local v1    # "stringId":I
    :cond_0
    const v1, 0x7f0e028d

    .restart local v1    # "stringId":I
    goto :goto_0
.end method

.method public static delete(Ljava/io/File;)V
    .locals 7
    .param p0, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 308
    if-nez p0, :cond_1

    .line 334
    :cond_0
    :goto_0
    return-void

    .line 310
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 312
    invoke-virtual {p0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {p0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v6

    array-length v6, v6

    if-nez v6, :cond_2

    .line 313
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 316
    :cond_2
    invoke-virtual {p0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 317
    .local v2, "files":[Ljava/lang/String;
    if-eqz v2, :cond_3

    .line 318
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_3

    aget-object v5, v0, v3

    .line 320
    .local v5, "temp":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 322
    .local v1, "fileDelete":Ljava/io/File;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->delete(Ljava/io/File;)V

    .line 318
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 326
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "fileDelete":Ljava/io/File;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "temp":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v6

    array-length v6, v6

    if-nez v6, :cond_0

    .line 327
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 332
    .end local v2    # "files":[Ljava/lang/String;
    :cond_4
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method

.method private getImageType(Lcom/sec/android/gallery3d/data/MediaItem;)I
    .locals 4
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 105
    const/4 v0, 0x0

    .line 106
    .local v0, "result":I
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 107
    const/4 v0, 0x2

    .line 113
    :cond_0
    :goto_0
    return v0

    .line 108
    :cond_1
    const-wide/16 v2, 0x10

    invoke-virtual {p1, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 109
    const/4 v0, 0x1

    goto :goto_0

    .line 110
    :cond_2
    const-wide/16 v2, 0x400

    invoke-virtual {p1, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 111
    const/4 v0, 0x3

    goto :goto_0
.end method

.method private getMyFilesName()Landroid/content/ComponentName;
    .locals 3

    .prologue
    .line 192
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMyFilesTabletName:Z

    if-eqz v1, :cond_0

    .line 193
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.sec.android.app.myfiles"

    const-string v2, "com.sec.android.app.myfiles.Myfiles"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    .local v0, "myFilesName":Landroid/content/ComponentName;
    :goto_0
    return-object v0

    .line 195
    .end local v0    # "myFilesName":Landroid/content/ComponentName;
    :cond_0
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.sec.android.app.myfiles"

    const-string v2, "com.sec.android.app.myfiles.MainActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local v0    # "myFilesName":Landroid/content/ComponentName;
    goto :goto_0
.end method

.method private getOutputFilePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "originalPath"    # Ljava/lang/String;

    .prologue
    .line 337
    const-string v1, ".mp4"

    .line 338
    .local v1, "newExtension":Ljava/lang/String;
    const-string v2, "."

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 339
    .local v0, "lastDot":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 340
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 342
    :goto_0
    return-object v2

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private getUriFrom(Ljava/lang/String;)Landroid/net/Uri;
    .locals 12
    .param p1, "newFilePath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 387
    new-array v2, v3, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    .line 390
    .local v2, "projection":[Ljava/lang/String;
    new-array v4, v3, [Ljava/lang/String;

    aput-object p1, v4, v1

    .line 393
    .local v4, "args":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 394
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 396
    .local v8, "uri":Landroid/net/Uri;
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v3, "_data=?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 398
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 400
    .local v7, "id":I
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    int-to-long v10, v7

    invoke-static {v0, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 403
    .end local v7    # "id":I
    :cond_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 405
    return-object v8

    .line 403
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private isExist(Ljava/lang/String;)Z
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 239
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method private updateItemDB(Ljava/lang/String;)V
    .locals 8
    .param p1, "newFileFullPath"    # Ljava/lang/String;

    .prologue
    .line 347
    const-string v6, "/"

    invoke-virtual {p1, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {p1, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 348
    .local v2, "newFileName":Ljava/lang/String;
    const/4 v6, 0x0

    const-string v7, "."

    invoke-virtual {v2, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 349
    .local v1, "fileNameWithNoExtenison":Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getVideoDuration(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 350
    .local v0, "duration":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 352
    .local v4, "now":J
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 353
    .local v3, "values":Landroid/content/ContentValues;
    const-string/jumbo v6, "title"

    invoke-virtual {v3, v6, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    const-string v6, "mime_type"

    const-string/jumbo v7, "video/mp4"

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    const-string v6, "_data"

    invoke-virtual {v3, v6, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    const-string v6, "_display_name"

    invoke-virtual {v3, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    const-string v6, "_data"

    invoke-virtual {v3, v6, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    const-string v6, "datetaken"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 359
    const-string v6, "date_added"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 360
    const-string v6, "date_modified"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 361
    const-string v6, "duration"

    invoke-virtual {v3, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6, v7, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 363
    return-void
.end method

.method private updateItemDBForDuration(Ljava/lang/String;)V
    .locals 5
    .param p1, "newFileFullPath"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 366
    invoke-static {p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getVideoDuration(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 368
    .local v0, "duration":Ljava/lang/String;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 369
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "duration"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 371
    return-void
.end method


# virtual methods
.method public createDialog(J)V
    .locals 1
    .param p1, "remaining"    # J

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->checkMyfiles()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->createDialogForMyFiles(J)V

    .line 122
    :goto_0
    return-void

    .line 120
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->createDialogNoMyFiles(J)V

    goto :goto_0
.end method

.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 14
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 66
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/Object;

    move-object v5, v8

    check-cast v5, [Ljava/lang/Object;

    .line 67
    .local v5, "params":[Ljava/lang/Object;
    aget-object v8, v5, v11

    check-cast v8, Landroid/content/Context;

    iput-object v8, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mContext:Landroid/content/Context;

    .line 68
    aget-object v3, v5, v12

    check-cast v3, Ljava/util/ArrayList;

    .line 69
    .local v3, "mMediaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    aget-object v8, v5, v13

    check-cast v8, Ljava/lang/Boolean;

    iput-object v8, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->type:Ljava/lang/Boolean;

    .line 71
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v7

    .line 73
    .local v7, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-direct {p0, v9, v8, v10}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->checkLowStorage(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v8

    if-nez v8, :cond_0

    .line 75
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v8}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v8

    const-string v9, "EXIT_SELECTION_MODE"

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;)V

    .line 102
    :goto_0
    return-void

    .line 80
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 81
    .local v2, "mConvertInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd$ConvertInfo;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v0, v8, :cond_2

    .line 82
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/data/MediaObject;

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 84
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v1

    .line 85
    .local v1, "inFilePath":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->getOutputFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 86
    .local v4, "outFilePath":Ljava/lang/String;
    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->isExist(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 87
    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->addSelectionModeProxy(Ljava/lang/String;)V

    .line 81
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 89
    :cond_1
    new-instance v9, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd$ConvertInfo;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-direct {p0, v8}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->getImageType(Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v8

    invoke-direct {v9, p0, v1, v4, v8}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd$ConvertInfo;-><init>(Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 93
    .end local v1    # "inFilePath":Ljava/lang/String;
    .end local v4    # "outFilePath":Ljava/lang/String;
    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 94
    new-array v6, v13, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mContext:Landroid/content/Context;

    aput-object v8, v6, v11

    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->type:Ljava/lang/Boolean;

    aput-object v8, v6, v12

    .line 97
    .local v6, "params1":[Ljava/lang/Object;
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v8}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v8

    const-string v9, "SHOW_IMAGE_VIDEO_SHARE_DIALOG"

    invoke-virtual {v8, v9, v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 100
    .end local v6    # "params1":[Ljava/lang/Object;
    :cond_3
    new-instance v8, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd$ConversionAsyncTask;

    const/4 v9, 0x0

    invoke-direct {v8, p0, v9}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd$ConversionAsyncTask;-><init>(Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd$1;)V

    new-array v9, v12, [Ljava/util/ArrayList;

    aput-object v2, v9, v11

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd$ConversionAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
