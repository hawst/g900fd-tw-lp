.class Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;
.super Landroid/os/AsyncTask;
.source "EventViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/eventview/EventViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateSelectionModeTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V
    .locals 0

    .prologue
    .line 1240
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;Lcom/sec/samsung/gallery/view/eventview/EventViewState$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState$1;

    .prologue
    .line 1240
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V

    return-void
.end method

.method private updateSelectionMode()Ljava/lang/Boolean;
    .locals 15

    .prologue
    const/4 v13, 0x1

    .line 1248
    iget-object v14, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v14}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->access$000(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCloneMediaList()Ljava/util/LinkedList;

    move-result-object v8

    .line 1250
    .local v8, "mediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v6, 0x0

    .line 1251
    .local v6, "isSelectedDataChanged":Z
    invoke-virtual {v8}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 1252
    .local v10, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;->isCancelled()Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1307
    .end local v10    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    :goto_1
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    return-object v13

    .line 1254
    .restart local v10    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    instance-of v14, v10, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v14, :cond_0

    move-object v7, v10

    .line 1255
    check-cast v7, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1256
    .local v7, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v12, 0x0

    .line 1257
    .local v12, "updateSelection":Z
    instance-of v5, v7, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .line 1258
    .local v5, "isLocalItem":Z
    instance-of v14, v7, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-nez v14, :cond_3

    instance-of v14, v7, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-eqz v14, :cond_8

    :cond_3
    move v4, v13

    .line 1259
    .local v4, "isCloudItem":Z
    :goto_2
    const/4 v2, 0x0

    .line 1260
    .local v2, "filePathOfMediaSet":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1262
    .local v1, "filePathOfItem":Ljava/lang/String;
    const/4 v9, 0x0

    .line 1263
    .local v9, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaItem;->getParentSetPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v11

    .line 1264
    .local v11, "parentPath":Lcom/sec/android/gallery3d/data/Path;
    if-eqz v11, :cond_5

    .line 1265
    iget-object v14, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;
    invoke-static {v14}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->access$4300(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v14

    invoke-virtual {v14, v11}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v9

    .line 1266
    if-eqz v9, :cond_5

    instance-of v14, v9, Lcom/sec/android/gallery3d/data/LocalAlbum;

    if-nez v14, :cond_4

    instance-of v14, v9, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v14, :cond_5

    .line 1267
    :cond_4
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v2

    .line 1268
    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getParentPathOnFileSystem(Lcom/sec/android/gallery3d/data/MediaItem;)Ljava/lang/String;

    move-result-object v1

    .line 1269
    const/4 v5, 0x1

    .line 1273
    :cond_5
    if-eqz v2, :cond_9

    const-string v14, ""

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_9

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_9

    .line 1275
    const/4 v12, 0x1

    .line 1290
    :cond_6
    :goto_3
    if-eqz v12, :cond_0

    if-nez v5, :cond_7

    if-eqz v4, :cond_0

    .line 1292
    :cond_7
    iget-object v14, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v14}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->access$2500(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v14

    invoke-virtual {v14, v13}, Lcom/sec/android/gallery3d/app/StateManager;->setReloadRequiredOnResume(Z)V

    .line 1293
    iget-object v14, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;
    invoke-static {v14}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->access$100(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->dismissDialogs()V

    .line 1295
    if-eqz v5, :cond_b

    .line 1296
    const/4 v6, 0x1

    .line 1297
    iget-object v14, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v14}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->access$000(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v14

    invoke-virtual {v14, v10}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1298
    iget-object v14, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v14}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->access$000(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v14

    invoke-virtual {v14, v9, v13}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;I)V

    goto/16 :goto_0

    .line 1258
    .end local v1    # "filePathOfItem":Ljava/lang/String;
    .end local v2    # "filePathOfMediaSet":Ljava/lang/String;
    .end local v4    # "isCloudItem":Z
    .end local v9    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v11    # "parentPath":Lcom/sec/android/gallery3d/data/Path;
    :cond_8
    const/4 v4, 0x0

    goto :goto_2

    .line 1277
    .restart local v1    # "filePathOfItem":Ljava/lang/String;
    .restart local v2    # "filePathOfMediaSet":Ljava/lang/String;
    .restart local v4    # "isCloudItem":Z
    .restart local v9    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .restart local v11    # "parentPath":Lcom/sec/android/gallery3d/data/Path;
    :cond_9
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_a

    .line 1278
    new-instance v0, Ljava/io/File;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v0, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1279
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_a

    if-nez v4, :cond_a

    .line 1280
    const/4 v12, 0x1

    .line 1283
    .end local v0    # "file":Ljava/io/File;
    :cond_a
    if-eqz v4, :cond_6

    iget-object v14, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v14}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->access$4400(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v14

    invoke-static {v14, v7}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;->isCloudItemDeleted(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1285
    const/4 v12, 0x1

    goto :goto_3

    .line 1301
    :cond_b
    iget-object v13, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    # invokes: Lcom/sec/samsung/gallery/view/eventview/EventViewState;->exitSelectionMode()V
    invoke-static {v13}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->access$300(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V

    goto/16 :goto_1
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 1
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 1244
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;->updateSelectionMode()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 1240
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 1312
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1314
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1315
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    # invokes: Lcom/sec/samsung/gallery/view/eventview/EventViewState;->selectAllPostProcess()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->access$4100(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1321
    :cond_0
    :goto_0
    return-void

    .line 1317
    :catch_0
    move-exception v0

    .line 1318
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 1319
    const-string v1, "EventViewState"

    const-string v2, "onPostExecute : NullPointerException!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1240
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
