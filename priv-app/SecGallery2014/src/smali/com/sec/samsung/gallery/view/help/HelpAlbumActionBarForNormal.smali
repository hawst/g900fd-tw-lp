.class public Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal;
.super Lcom/sec/samsung/gallery/view/AlbumTabBar;
.source "HelpAlbumActionBarForNormal.java"


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AlbumTabBar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 19
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal$1;-><init>(Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method private getOptionMenuId()I
    .locals 1

    .prologue
    .line 49
    const v0, 0x7f120024

    return v0
.end method


# virtual methods
.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 33
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 34
    .local v0, "inflater":Landroid/view/MenuInflater;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal;->getOptionMenuId()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 35
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 36
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 0
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 46
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 40
    const v0, 0x7f0f02bf

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuGroupVisibility(Landroid/view/Menu;IZ)V

    .line 41
    const v0, 0x7f0f028e

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    invoke-static {p1, v0, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 42
    return-void
.end method
