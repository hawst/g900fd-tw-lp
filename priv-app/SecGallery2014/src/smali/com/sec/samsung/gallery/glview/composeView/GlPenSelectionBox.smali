.class public Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;
.super Ljava/lang/Object;
.source "GlPenSelectionBox.java"


# static fields
.field private static final BORDER_COLOR:I = -0xff6316

.field private static final BORDER_WIDTH:I = 0x8

.field private static DEF_DISTANCE:F = 0.0f

.field private static final FILL_COLOR:I = -0x662809

.field private static final PENSELECTION_END_EVT:I = 0x3

.field private static final PENSELECTION_START_ANIM:I = 0x2

.field private static final PENSELECTION_START_REQ:I = 0x1

.field private static final PENSELECTION_STATE_DESTROYING:I = 0x3

.field private static final PENSELECTION_STATE_IDLE:I = 0x0

.field private static final PENSELECTION_STATE_STARTED:I = 0x1

.field private static final TAG:Ljava/lang/String; = "GlPenSelectionBox"


# instance fields
.field private mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field private mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

.field private mHeight:I

.field private mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

.field private mPenSelectionState:I

.field private mRatioHeight:F

.field private mRatioWidth:F

.field private mRect:Landroid/graphics/Rect;

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/high16 v0, 0x44480000    # 800.0f

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->DEF_DISTANCE:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mPenSelectionState:I

    .line 34
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 35
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->initAttribute()V

    .line 36
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->createObjects()V

    .line 38
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox$1;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->getGlRoot()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox$1;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->prepareItemAnimation()V

    return-void
.end method

.method private convertWindowCoordsToOpenGLCoords(II)Landroid/graphics/PointF;
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 66
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v2, v2, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeight:I

    int-to-float v2, v2

    div-float/2addr v2, v4

    int-to-float v3, p2

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mRatioHeight:F

    mul-float v1, v2, v3

    .line 67
    .local v1, "openGlY":F
    int-to-float v2, p1

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v3, v3, Lcom/sec/android/gallery3d/glcore/GlLayer;->mWidth:I

    int-to-float v3, v3

    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mRatioWidth:F

    mul-float v0, v2, v3

    .line 68
    .local v0, "openGlX":F
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v2
.end method

.method private createObjects()V
    .locals 2

    .prologue
    .line 84
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 85
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setAlpha(F)V

    .line 86
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setUseTouchEvent(Z)V

    .line 87
    return-void
.end method

.method private prepareItemAnimation()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 97
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->removePenSelectionView()V

    .line 99
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 102
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mPenSelectionState:I

    .line 103
    return-void
.end method

.method private declared-synchronized removePenSelectionViewInter()V
    .locals 2

    .prologue
    .line 163
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlHandler;->removeAllMessage()V

    .line 165
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setAlpha(F)V

    .line 166
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mPenSelectionState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 167
    monitor-exit p0

    return-void

    .line 163
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private setBackgroundObject(II)V
    .locals 6
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    const/4 v5, 0x1

    .line 118
    if-lez p1, :cond_0

    if-gtz p2, :cond_1

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->convX(I)F

    move-result v1

    .line 122
    .local v1, "width":F
    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->convY(I)F

    move-result v0

    .line 123
    .local v0, "height":F
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v2, v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    .line 124
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    new-instance v3, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v3, v4, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 125
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    new-instance v3, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox$2;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox$2;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;)V

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 128
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const v3, -0x662809

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEmptyFillColor(I)V

    .line 129
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v2, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEmptyFill(Z)V

    .line 131
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const v3, -0xff6316

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setBorderColor(I)V

    .line 132
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/high16 v3, 0x41000000    # 8.0f

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setBorderWidth(F)V

    .line 133
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v2, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setBorderVisible(Z)V

    goto :goto_0
.end method


# virtual methods
.method public changeBackgroundSize(Landroid/graphics/Rect;)V
    .locals 11
    .param p1, "r"    # Landroid/graphics/Rect;

    .prologue
    .line 137
    iget v7, p1, Landroid/graphics/Rect;->right:I

    iget v8, p1, Landroid/graphics/Rect;->left:I

    sub-int v5, v7, v8

    .line 138
    .local v5, "w":I
    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    iget v8, p1, Landroid/graphics/Rect;->top:I

    sub-int v2, v7, v8

    .line 139
    .local v2, "h":I
    iget v7, p1, Landroid/graphics/Rect;->left:I

    iget v8, p1, Landroid/graphics/Rect;->right:I

    add-int/2addr v7, v8

    div-int/lit8 v0, v7, 0x2

    .line 140
    .local v0, "centerX":I
    iget v7, p1, Landroid/graphics/Rect;->top:I

    iget v8, p1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v7, v8

    div-int/lit8 v1, v7, 0x2

    .line 142
    .local v1, "centerY":I
    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->convX(I)F

    move-result v6

    .line 143
    .local v6, "width":F
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->convY(I)F

    move-result v3

    .line 144
    .local v3, "height":F
    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->convertWindowCoordsToOpenGLCoords(II)Landroid/graphics/PointF;

    move-result-object v4

    .line 146
    .local v4, "point":Landroid/graphics/PointF;
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v7, v6, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    .line 147
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v8, v4, Landroid/graphics/PointF;->x:F

    iget v9, v4, Landroid/graphics/PointF;->y:F

    sget v10, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->DEF_DISTANCE:F

    neg-float v10, v10

    invoke-virtual {v7, v8, v9, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    .line 148
    return-void
.end method

.method public convX(I)F
    .locals 2
    .param p1, "x"    # I

    .prologue
    .line 72
    int-to-float v0, p1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mRatioWidth:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public convY(I)F
    .locals 2
    .param p1, "y"    # I

    .prologue
    .line 76
    int-to-float v0, p1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mRatioHeight:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->removePenSelectionViewInter()V

    .line 55
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->remove()V

    .line 56
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 57
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    .line 58
    return-void
.end method

.method public exitFromPenSelectionView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 171
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlHandler;->removeAllMessage()V

    .line 172
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 173
    return-void
.end method

.method public initAttribute()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mWidthSpace:F

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v1, v1, Lcom/sec/android/gallery3d/glcore/GlLayer;->mWidth:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mRatioWidth:F

    .line 62
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeightSpace:F

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v1, v1, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeight:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mRatioHeight:F

    .line 63
    return-void
.end method

.method public isIdleState()Z
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mPenSelectionState:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removePenSelectionView()V
    .locals 2

    .prologue
    .line 151
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mPenSelectionState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 152
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlHandler;->removeAllMessage()V

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setAlpha(F)V

    .line 156
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mPenSelectionState:I

    .line 160
    :goto_0
    return-void

    .line 159
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->removePenSelectionViewInter()V

    goto :goto_0
.end method

.method public requestStart(Landroid/graphics/Rect;)V
    .locals 4
    .param p1, "r"    # Landroid/graphics/Rect;

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 90
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mRect:Landroid/graphics/Rect;

    .line 91
    iget v0, p1, Landroid/graphics/Rect;->right:I

    iget v2, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v2

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mWidth:I

    .line 92
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v2

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mHeight:I

    .line 93
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v0, v1, v3, v3, v3}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 94
    return-void

    .line 91
    :cond_0
    iget v0, p1, Landroid/graphics/Rect;->right:I

    iget v2, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v2

    goto :goto_0

    .line 92
    :cond_1
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v2

    goto :goto_1
.end method

.method public startShowAnimation()V
    .locals 7

    .prologue
    .line 106
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v4

    div-int/lit8 v1, v3, 0x2

    .line 107
    .local v1, "x":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v4

    div-int/lit8 v2, v3, 0x2

    .line 108
    .local v2, "y":I
    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->convertWindowCoordsToOpenGLCoords(II)Landroid/graphics/PointF;

    move-result-object v0

    .line 110
    .local v0, "point":Landroid/graphics/PointF;
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mWidth:I

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mHeight:I

    invoke-direct {p0, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->setBackgroundObject(II)V

    .line 112
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v4, v0, Landroid/graphics/PointF;->x:F

    iget v5, v0, Landroid/graphics/PointF;->y:F

    sget v6, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->DEF_DISTANCE:F

    neg-float v6, v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    .line 113
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/high16 v4, 0x3f000000    # 0.5f

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setAlpha(F)V

    .line 114
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->mBackground:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->moveToLast()V

    .line 115
    return-void
.end method
