.class public Lcom/sec/samsung/gallery/controller/FinishStateCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "FinishStateCmd.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 4
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 14
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Object;

    move-object v1, v3

    check-cast v1, [Ljava/lang/Object;

    .line 15
    .local v1, "params":[Ljava/lang/Object;
    const/4 v3, 0x0

    aget-object v2, v1, v3

    check-cast v2, Lcom/sec/android/gallery3d/app/ActivityState;

    .line 16
    .local v2, "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    const/4 v3, 0x1

    aget-object v0, v1, v3

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 17
    .local v0, "activity":Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    if-nez v2, :cond_0

    .line 18
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v2

    .line 20
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 21
    return-void
.end method
