.class public final enum Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;
.super Ljava/lang/Enum;
.source "Options.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SinglePointInfoWindowClickBehavior"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;

.field public static final enum HIDE_INFO_WINDOW:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;

.field public static final enum NO_OP:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 362
    new-instance v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;

    const-string v1, "HIDE_INFO_WINDOW"

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;->HIDE_INFO_WINDOW:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;

    new-instance v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;

    const-string v1, "NO_OP"

    invoke-direct {v0, v1, v3}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;->NO_OP:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;

    .line 361
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;

    sget-object v1, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;->HIDE_INFO_WINDOW:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;->NO_OP:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;->$VALUES:[Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 361
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 361
    const-class v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;

    return-object v0
.end method

.method public static values()[Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;
    .locals 1

    .prologue
    .line 361
    sget-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;->$VALUES:[Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;

    invoke-virtual {v0}, [Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;

    return-object v0
.end method
