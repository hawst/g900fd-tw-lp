.class public Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;
.super Ljava/lang/Object;
.source "DimensionConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/DimensionConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AlbumViewDimension"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;


# instance fields
.field public album_new_label_font_size:I

.field public album_new_label_height:I

.field public album_new_label_padding:I

.field public album_new_label_right_margin:I

.field public album_new_label_top_margin:I

.field public album_new_label_width:I

.field public album_view_album_font_color:I

.field public album_view_album_label_padding_left:I

.field public album_view_default_mode:I

.field public album_view_mediatype_icon_padding_bottom:I

.field public album_view_mediatype_icon_padding_left:I

.field public album_view_mode_count:I

.field public album_view_noitems_bg_height_landscape:I

.field public album_view_noitems_bg_height_portrait:I

.field public album_view_noitems_popup_padding_right:I

.field public album_view_noitems_popup_padding_right_nomenu:I

.field public album_view_noitems_popup_padding_right_picker:I

.field public album_view_noitems_popup_padding_top:I

.field public album_view_scrollbar_width:I

.field public mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

.field public noitems_screen_position_coef:I


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 94
    .local v1, "r":Landroid/content/res/Resources;
    const v2, 0x7f0c0061

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_album_font_color:I

    .line 96
    const v2, 0x7f0c0063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_default_mode:I

    .line 97
    const v2, 0x7f0c0064

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_mode_count:I

    .line 98
    const v2, 0x7f0d0376

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_album_label_padding_left:I

    .line 99
    const v2, 0x7f0d0380

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_noitems_popup_padding_top:I

    .line 100
    const v2, 0x7f0d0381

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_noitems_popup_padding_right:I

    .line 101
    const v2, 0x7f0d036b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_noitems_popup_padding_right_picker:I

    .line 102
    const v2, 0x7f0d0382

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_noitems_popup_padding_right_nomenu:I

    .line 103
    const v2, 0x7f0c0048

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->noitems_screen_position_coef:I

    .line 104
    const v2, 0x7f0d0383

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_mediatype_icon_padding_left:I

    .line 105
    const v2, 0x7f0d0384

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_mediatype_icon_padding_bottom:I

    .line 106
    const v2, 0x7f0d0377

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_new_label_height:I

    .line 107
    const v2, 0x7f0d0378

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_new_label_width:I

    .line 108
    const v2, 0x7f0d0379

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_new_label_padding:I

    .line 109
    const v2, 0x7f0d037a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_new_label_top_margin:I

    .line 110
    const v2, 0x7f0d037b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_new_label_right_margin:I

    .line 111
    const v2, 0x7f0d037c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_new_label_font_size:I

    .line 112
    const v2, 0x7f0d037d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_scrollbar_width:I

    .line 114
    const v2, 0x7f0d037f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_noitems_bg_height_portrait:I

    .line 116
    const v2, 0x7f0d037e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_noitems_bg_height_landscape:I

    .line 119
    iget v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_mode_count:I

    mul-int/lit8 v2, v2, 0x2

    new-array v2, v2, [Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    .line 120
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_mode_count:I

    mul-int/lit8 v2, v2, 0x2

    if-ge v0, v2, :cond_0

    .line 121
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    new-instance v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    invoke-direct {v3}, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;-><init>()V

    aput-object v3, v2, v0

    .line 122
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f09004d

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->album_view_album_font_size:I

    .line 123
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f09004e

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->album_view_album_count_font_size:I

    .line 124
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f09004f

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->hpadding_left:I

    .line 125
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090050

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->hpadding_right:I

    .line 126
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090051

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->hgap:I

    .line 127
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090052

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->vpadding_top:I

    .line 128
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090053

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->vgap:I

    .line 129
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090054

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->thumbnail_width:I

    .line 130
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090055

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->thumbnail_height:I

    .line 131
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090056

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->name_textbox_hoffset:I

    .line 132
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090057

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->name_textbox_voffset:I

    .line 133
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090058

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->name_textbox_width:I

    .line 134
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090059

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->name_textbox_height:I

    .line 120
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 136
    :cond_0
    return-void
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 86
    const-class v1, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    if-nez v0, :cond_0

    .line 87
    new-instance v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    .line 89
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
