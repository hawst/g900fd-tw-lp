.class Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$4;
.super Ljava/lang/Object;
.source "PhotoActionBarForNormal.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->setTitle(Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

.field final synthetic val$albumName:Ljava/lang/String;

.field final synthetic val$numberOfItemsInAlbum:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 308
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$4;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$4;->val$albumName:Ljava/lang/String;

    iput p3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$4;->val$numberOfItemsInAlbum:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private setTitleOnCustomView(Ljava/lang/String;)V
    .locals 3
    .param p1, "albumLabelCountText"    # Ljava/lang/String;

    .prologue
    .line 327
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$4;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->access$1800(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    const v2, 0x7f0f0016

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 328
    .local v0, "albumNameView":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 329
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 330
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 332
    :cond_0
    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 312
    :try_start_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$4;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->access$1600(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 313
    .local v2, "res":Landroid/content/res/Resources;
    const v3, 0x7f0e01bf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$4;->val$albumName:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$4;->val$numberOfItemsInAlbum:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 316
    .local v0, "albumLabelCountText":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 317
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$4;->setTitleOnCustomView(Ljava/lang/String;)V

    .line 324
    .end local v0    # "albumLabelCountText":Ljava/lang/String;
    .end local v2    # "res":Landroid/content/res/Resources;
    :goto_0
    return-void

    .line 319
    .restart local v0    # "albumLabelCountText":Ljava/lang/String;
    .restart local v2    # "res":Landroid/content/res/Resources;
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$4;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->access$1700(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 321
    .end local v0    # "albumLabelCountText":Ljava/lang/String;
    .end local v2    # "res":Landroid/content/res/Resources;
    :catch_0
    move-exception v1

    .line 322
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
