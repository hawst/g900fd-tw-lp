.class Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd$IdleProcess;
.super Ljava/lang/Object;
.source "StartIdleProcessCmd.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IdleProcess"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd$IdleProcess;->this$0:Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd$1;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd$IdleProcess;-><init>(Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;)V

    return-void
.end method


# virtual methods
.method public onGLIdle(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Z)Z
    .locals 9
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "renderRequested"    # Z

    .prologue
    .line 44
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 47
    .local v0, "currentTime":J
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd$IdleProcess;->this$0:Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;->mActivity:Lcom/sec/android/gallery3d/app/GalleryActivity;
    invoke-static {v6}, Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;->access$100(Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;)Lcom/sec/android/gallery3d/app/GalleryActivity;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    iget-wide v6, v6, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mAppCreateTime:J

    sub-long v2, v0, v6

    .line 48
    .local v2, "elapsedTime":J
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd$IdleProcess;->this$0:Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;->mActivity:Lcom/sec/android/gallery3d/app/GalleryActivity;
    invoke-static {v6}, Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;->access$100(Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;)Lcom/sec/android/gallery3d/app/GalleryActivity;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getActivityCreateTime()J

    move-result-wide v6

    sub-long v4, v0, v6

    .line 49
    .local v4, "elapsedTime2":J
    const-string v6, "Gallery_Performance"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Elapsed time to complete first display : Activity = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", Application = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    const/4 v6, 0x0

    return v6
.end method
