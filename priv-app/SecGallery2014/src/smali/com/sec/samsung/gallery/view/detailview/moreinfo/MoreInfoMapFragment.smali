.class public Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment;
.super Lcom/google/android/gms/maps/MapFragment;
.source "MoreInfoMapFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment$MapViewListener;
    }
.end annotation


# instance fields
.field private mListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment$MapViewListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/android/gms/maps/MapFragment;-><init>()V

    .line 16
    return-void
.end method

.method private setUpMap()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 37
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment;->getMap()Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v0

    .line 38
    .local v0, "googleMap":Lcom/google/android/gms/maps/GoogleMap;
    if-eqz v0, :cond_0

    .line 39
    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/UiSettings;->setZoomControlsEnabled(Z)V

    .line 40
    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/UiSettings;->setAllGesturesEnabled(Z)V

    .line 42
    :cond_0
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 22
    invoke-super {p0, p1}, Lcom/google/android/gms/maps/MapFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 23
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment;->setRetainInstance(Z)V

    .line 24
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/maps/MapFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 29
    .local v0, "view":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment;->mListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment$MapViewListener;

    if-eqz v1, :cond_0

    .line 30
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment;->mListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment$MapViewListener;

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment$MapViewListener;->onMapCreated()V

    .line 32
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment;->setUpMap()V

    .line 33
    return-object v0
.end method

.method public setMapViewListener(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment$MapViewListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment$MapViewListener;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment;->mListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment$MapViewListener;

    .line 46
    return-void
.end method
