.class Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$6;
.super Ljava/lang/Object;
.source "HelpAirCommandState.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->setViewAirButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

.field final synthetic val$res:Landroid/content/res/Resources;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$6;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$6;->val$res:Landroid/content/res/Resources;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$6;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$6;->val$res:Landroid/content/res/Resources;

    const v2, 0x7f0d01ce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 227
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$6;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$6;->val$res:Landroid/content/res/Resources;

    const v2, 0x7f0d01cf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 229
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$6;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$6;->val$res:Landroid/content/res/Resources;

    const v2, 0x7f0d01cc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setX(F)V

    .line 230
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$6;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$6;->val$res:Landroid/content/res/Resources;

    const v2, 0x7f0d01cd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setY(F)V

    .line 231
    return-void
.end method
