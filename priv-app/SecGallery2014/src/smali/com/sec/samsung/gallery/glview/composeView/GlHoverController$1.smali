.class Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;
.super Lcom/sec/android/gallery3d/glcore/GlHandler;
.source "GlHoverController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;Lcom/sec/android/gallery3d/glcore/GlRootView;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/glcore/GlHandler;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    return-void
.end method


# virtual methods
.method public onMessage(ILjava/lang/Object;III)V
    .locals 5
    .param p1, "cmd"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I
    .param p5, "parm3"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 165
    const-string v0, "GlHoverController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onMessage msg = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    if-ne p1, v3, :cond_3

    .line 167
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$000(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 196
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsSet:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$100(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 170
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # invokes: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->prepareSetAnimation()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$200(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V

    .line 173
    :goto_1
    invoke-static {}, Lcom/sec/samsung/gallery/util/HoverIconUtil;->setHoveringIconToMoreIcon()V

    goto :goto_0

    .line 172
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # invokes: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->prepareItemAnimation()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$300(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V

    goto :goto_1

    .line 174
    :cond_3
    const/4 v0, 0x2

    if-ne p1, v0, :cond_6

    .line 175
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$000(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$400(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlLayer;->getGlRoot()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getToolType()I

    move-result v0

    if-eq v0, v3, :cond_4

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$500(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPickMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 178
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # setter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsActionBarEnabled:Z
    invoke-static {v0, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$602(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;Z)Z

    .line 181
    :goto_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->startShowAnimation()V

    .line 182
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$500(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->AIRV:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$800(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$500(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 180
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # invokes: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->setActionBarEnabled()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$700(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V

    goto :goto_2

    .line 183
    :cond_6
    const/4 v0, 0x3

    if-ne p1, v0, :cond_8

    .line 184
    if-ne p3, v3, :cond_7

    .line 185
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # invokes: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->checkRemoveHoverView()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$900(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V

    goto :goto_0

    .line 187
    :cond_7
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->removeHoverView(Z)V

    goto/16 :goto_0

    .line 189
    :cond_8
    const/4 v0, 0x4

    if-ne p1, v0, :cond_9

    .line 190
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    const/16 v1, -0x9

    # invokes: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->prepareNextSet(I)Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1000(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;I)Z

    .line 191
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # invokes: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->exitFromHoverViewWithDelay()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1100(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V

    goto/16 :goto_0

    .line 192
    :cond_9
    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    const/16 v1, 0x9

    # invokes: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->prepareNextSet(I)Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1000(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;I)Z

    .line 194
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # invokes: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->exitFromHoverViewWithDelay()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1100(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V

    goto/16 :goto_0
.end method
