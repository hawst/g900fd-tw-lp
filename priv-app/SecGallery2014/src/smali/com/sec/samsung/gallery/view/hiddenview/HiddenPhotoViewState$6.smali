.class Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$6;
.super Ljava/lang/Object;
.source "HiddenPhotoViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$6;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;I)V
    .locals 1
    .param p1, "parentView"    # Lcom/sec/samsung/gallery/glview/GlAbsListView;
    .param p2, "view"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "position"    # I

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$6;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # setter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mScrolledPosition:I
    invoke-static {v0, p3}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$1102(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;I)I

    .line 221
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$6;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$300(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$6;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # invokes: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->selectMedia(I)V
    invoke-static {v0, p3}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$1200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;I)V

    .line 226
    :goto_0
    return-void

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$6;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # invokes: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->handleHiddenItemsLaunch(I)V
    invoke-static {v0, p3}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$1300(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;I)V

    goto :goto_0
.end method
