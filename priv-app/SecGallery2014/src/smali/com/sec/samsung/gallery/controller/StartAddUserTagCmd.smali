.class public Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "StartAddUserTagCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field public static final ADD_USER_TAG_MAX_SELECTION_LIMIT:I = 0x1f4

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final FILES_URI:Landroid/net/Uri;

.field private mAddTagDialog:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

.field private mContext:Landroid/content/Context;

.field protected mCount:I

.field protected mSelectedItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 32
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->FILES_URI:Landroid/net/Uri;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;)Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->mAddTagDialog:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;)Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->mAddTagDialog:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private dismissDialog()V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->mAddTagDialog:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->mAddTagDialog:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->dismissAddTagDialog()V

    .line 120
    :cond_0
    return-void
.end method

.method private showDialog(I)V
    .locals 10
    .param p1, "requestCode"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 55
    new-instance v7, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v7, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 56
    .local v7, "isCancelled":Ljava/util/concurrent/atomic/AtomicBoolean;
    new-instance v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;-><init>()V

    .line 58
    .local v0, "progressDialog":Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;
    new-instance v9, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$1;

    invoke-direct {v9, p0, v7, v0, p1}, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;I)V

    .line 104
    .local v9, "progressListener":Lcom/sec/android/gallery3d/data/OnProgressListener;
    new-instance v8, Lcom/sec/samsung/gallery/util/MediaOperations;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->mContext:Landroid/content/Context;

    invoke-direct {v8, v2, v9, v1}, Lcom/sec/samsung/gallery/util/MediaOperations;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/OnProgressListener;Landroid/content/Context;)V

    .line 105
    .local v8, "operations":Lcom/sec/samsung/gallery/util/MediaOperations;
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->mContext:Landroid/content/Context;

    const v5, 0x7f0e0032

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x1

    new-instance v6, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$2;

    invoke-direct {v6, p0, v7, v8}, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$2;-><init>(Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/sec/samsung/gallery/util/MediaOperations;)V

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->showProgressDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 113
    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v8, v1}, Lcom/sec/samsung/gallery/util/MediaOperations;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 114
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 4
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 43
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Object;

    move-object v1, v3

    check-cast v1, [Ljava/lang/Object;

    .line 44
    .local v1, "params":[Ljava/lang/Object;
    const/4 v3, 0x0

    aget-object v3, v1, v3

    check-cast v3, Landroid/content/Context;

    iput-object v3, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->mContext:Landroid/content/Context;

    .line 45
    const/4 v3, 0x1

    aget-object v3, v1, v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 46
    .local v2, "requestCode":I
    const/4 v3, 0x2

    aget-object v3, v1, v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 47
    .local v0, "bShow":Z
    if-eqz v0, :cond_0

    .line 48
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->showDialog(I)V

    .line 52
    :goto_0
    return-void

    .line 50
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->dismissDialog()V

    goto :goto_0
.end method

.method protected getBaseUri(I)Ljava/lang/String;
    .locals 2
    .param p1, "itemId"    # I

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->FILES_URI:Landroid/net/Uri;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
