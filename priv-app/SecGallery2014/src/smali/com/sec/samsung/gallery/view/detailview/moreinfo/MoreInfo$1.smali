.class Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$1;
.super Landroid/os/Handler;
.source "MoreInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 110
    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 112
    :pswitch_0
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v5, :cond_2

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Landroid/graphics/Bitmap;

    move-object v1, v5

    .line 113
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    :goto_1
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    if-lez v5, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    if-lez v5, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v5

    if-nez v5, :cond_0

    .line 114
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    const/16 v6, 0xb3

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    # invokes: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->getAverageColor(Landroid/graphics/Bitmap;ID)I
    invoke-static {v1, v6, v8, v9}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->access$100(Landroid/graphics/Bitmap;ID)I

    move-result v6

    # setter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mBackgorundColor:I
    invoke-static {v5, v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->access$002(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;I)I

    .line 115
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v5

    if-nez v5, :cond_1

    .line 116
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 118
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mRootView:Landroid/view/View;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->access$200(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 119
    .local v0, "background":Landroid/graphics/drawable/Drawable;
    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    .end local v0    # "background":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result v3

    .line 121
    .local v3, "colorFrom":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mIsEnabled:Z
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->access$300(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 123
    :try_start_0
    new-instance v5, Landroid/animation/ArgbEvaluator;

    invoke-direct {v5}, Landroid/animation/ArgbEvaluator;-><init>()V

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mBackgorundColor:I
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->access$000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Landroid/animation/ValueAnimator;->ofObject(Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 124
    .local v2, "colorAnimation":Landroid/animation/ValueAnimator;
    new-instance v5, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$MyAnimatorListener;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->mRootView:Landroid/view/View;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->access$200(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;)Landroid/view/View;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$MyAnimatorListener;-><init>(Landroid/view/View;)V

    invoke-virtual {v2, v5}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 125
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 126
    .end local v2    # "colorAnimation":Landroid/animation/ValueAnimator;
    :catch_0
    move-exception v4

    .line 127
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 112
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v3    # "colorFrom":I
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 133
    :pswitch_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->updateActionBar()V
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->access$400(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;)V

    goto/16 :goto_0

    .line 110
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
