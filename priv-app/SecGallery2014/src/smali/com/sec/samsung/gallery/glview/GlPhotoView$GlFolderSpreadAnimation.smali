.class public Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlPhotoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GlFolderSpreadAnimation"
.end annotation


# static fields
.field private static final DURATION:I = 0x190


# instance fields
.field private mFolderObj:Lcom/sec/samsung/gallery/glview/GlBaseObject;

.field private mRan:Ljava/util/Random;

.field private mRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3888
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 3892
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->mRan:Ljava/util/Random;

    return-void
.end method


# virtual methods
.method protected onCancel()V
    .locals 0

    .prologue
    .line 3965
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->onStop()V

    .line 3966
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 3952
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-eqz v1, :cond_1

    .line 3953
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 3954
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    aget-object v1, v1, v0

    if-nez v1, :cond_0

    .line 3953
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3956
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getDefTransAnim()Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->stop()V

    goto :goto_1

    .line 3959
    .end local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->mRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v1, :cond_2

    .line 3960
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->mRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 3962
    :cond_2
    return-void
.end method

.method public startAnimation(Lcom/sec/android/gallery3d/glcore/GlRootView;[Lcom/sec/samsung/gallery/glview/GlThumbObject;Lcom/sec/samsung/gallery/glview/GlScroller;Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 14
    .param p1, "root"    # Lcom/sec/android/gallery3d/glcore/GlRootView;
    .param p2, "objs"    # [Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .param p3, "scroller"    # Lcom/sec/samsung/gallery/glview/GlScroller;
    .param p4, "ref"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    .line 3899
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->mRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 3900
    move-object/from16 v0, p2

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 3901
    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->mFolderObj:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 3904
    :try_start_0
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->mFolderObj:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v11}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getX()F

    move-result v6

    .line 3905
    .local v6, "sx":F
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->mFolderObj:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v11}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getY()F

    move-result v7

    .line 3906
    .local v7, "sy":F
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->mFolderObj:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v11}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getZ()F

    move-result v8

    .line 3908
    .local v8, "sz":F
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    move-object/from16 v0, p3

    iget v11, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    if-ge v4, v11, :cond_0

    .line 3910
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    array-length v11, v11

    if-gt v11, v4, :cond_1

    .line 3943
    :cond_0
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->mRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v11, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 3944
    const-wide/16 v12, 0x190

    invoke-virtual {p0, v12, v13}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->setDuration(J)V

    .line 3945
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->start()V

    .line 3949
    .end local v4    # "i":I
    .end local v6    # "sx":F
    .end local v7    # "sy":F
    .end local v8    # "sz":F
    :goto_1
    return-void

    .line 3913
    .restart local v4    # "i":I
    .restart local v6    # "sx":F
    .restart local v7    # "sy":F
    .restart local v8    # "sz":F
    :cond_1
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->mThumbSet:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    aget-object v9, v11, v4

    .line 3914
    .local v9, "thumbObject":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    iget v11, v9, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    move-object/from16 v0, p3

    iget v12, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    if-ge v11, v12, :cond_2

    iget v11, v9, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    move-object/from16 v0, p3

    iget v12, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    if-lt v11, v12, :cond_2

    iget v11, v9, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    move-object/from16 v0, p3

    iget v12, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    if-le v11, v12, :cond_3

    .line 3908
    :cond_2
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 3919
    :cond_3
    invoke-virtual {v9, v6, v7, v8}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setSourcePos(FFF)V

    .line 3920
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setPosAsTarget()V

    .line 3921
    invoke-virtual {v9, v6, v7, v8}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setPos(FFF)V

    .line 3923
    const/4 v11, 0x1

    invoke-virtual {v9, v11}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getWidth(Z)F

    move-result v10

    .line 3924
    .local v10, "w":F
    const/4 v11, 0x1

    invoke-virtual {v9, v11}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getHeight(Z)F

    move-result v3

    .line 3926
    .local v3, "h":F
    const/high16 v11, 0x40000000    # 2.0f

    div-float v11, v10, v11

    const/high16 v12, 0x40000000    # 2.0f

    div-float v12, v3, v12

    invoke-virtual {v9, v11, v12}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setSourceSize(FF)V

    .line 3927
    invoke-virtual {v9, v10, v3}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setTargetSize(FF)V

    .line 3928
    const v11, 0x3dcccccd    # 0.1f

    invoke-virtual {v9, v11}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setDimEx(F)V

    .line 3929
    const/16 v11, 0x190

    invoke-virtual {v9, v11}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setDimDuration(I)V

    .line 3930
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->undim()V

    .line 3931
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderSpreadAnimation;->mRan:Ljava/util/Random;

    invoke-virtual {v11}, Ljava/util/Random;->nextInt()I

    move-result v11

    rem-int/lit8 v5, v11, 0x1e

    .line 3933
    .local v5, "roll":I
    if-gez v5, :cond_4

    .line 3934
    add-int/lit8 v5, v5, -0xf

    .line 3938
    :goto_3
    int-to-float v11, v5

    invoke-virtual {v9, v11}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setSourceRoll(F)V

    .line 3939
    const/4 v11, 0x1

    invoke-virtual {v9, v11}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setVisibility(Z)V

    .line 3940
    const/4 v11, 0x0

    const-wide/16 v12, 0x190

    invoke-virtual {v9, v11, v12, v13}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->startDefTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 3946
    .end local v3    # "h":F
    .end local v4    # "i":I
    .end local v5    # "roll":I
    .end local v6    # "sx":F
    .end local v7    # "sy":F
    .end local v8    # "sz":F
    .end local v9    # "thumbObject":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .end local v10    # "w":F
    :catch_0
    move-exception v2

    .line 3947
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 3936
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v3    # "h":F
    .restart local v4    # "i":I
    .restart local v5    # "roll":I
    .restart local v6    # "sx":F
    .restart local v7    # "sy":F
    .restart local v8    # "sz":F
    .restart local v9    # "thumbObject":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .restart local v10    # "w":F
    :cond_4
    add-int/lit8 v5, v5, 0xf

    goto :goto_3
.end method
