.class public Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarView;
.source "PhotoActionBarForSearch.java"


# instance fields
.field private mSearchView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 21
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch$1;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;)Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;->mSearchView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;->initializeSearchView()V

    return-void
.end method

.method private initializeSearchView()V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0f001a

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;->mSearchView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;->mSearchView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;->mSearchView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch$2;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->setOnQueryTextListener(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnQueryTextListener;)V

    .line 57
    return-void
.end method


# virtual methods
.method public onPause()V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;->mSearchView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->setImeVisibility(Z)V

    .line 83
    return-void
.end method

.method protected setTitle(Ljava/lang/String;)V
    .locals 2
    .param p1, "albunName"    # Ljava/lang/String;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch$3;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch$3;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 68
    return-void
.end method

.method protected setupButtons()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch$4;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch$4;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 78
    return-void
.end method
