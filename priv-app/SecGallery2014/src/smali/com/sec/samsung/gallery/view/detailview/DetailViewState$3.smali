.class Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;
.super Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
.source "DetailViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/android/gallery3d/ui/GLRoot;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/gallery3d/ui/GLRoot;

    .prologue
    .line 1019
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;-><init>(Lcom/sec/android/gallery3d/ui/GLRoot;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 1022
    iget v8, p1, Landroid/os/Message;->what:I

    sparse-switch v8, :sswitch_data_0

    .line 1168
    new-instance v8, Ljava/lang/AssertionError;

    iget v9, p1, Landroid/os/Message;->what:I

    invoke-direct {v8, v9}, Ljava/lang/AssertionError;-><init>(I)V

    throw v8

    .line 1024
    :sswitch_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->hideBars()V
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$5300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    .line 1170
    :cond_0
    :goto_0
    :sswitch_1
    return-void

    .line 1031
    :sswitch_2
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateBars()V
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$5400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    goto :goto_0

    .line 1035
    :sswitch_3
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->wantBars()V
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$5500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    goto :goto_0

    .line 1039
    :sswitch_4
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$5600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v8

    invoke-interface {v8}, Lcom/sec/android/gallery3d/ui/GLRoot;->unfreeze()V

    goto :goto_0

    .line 1043
    :sswitch_5
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeferUpdateUntil:J
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$5700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)J

    move-result-wide v8

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    sub-long v4, v8, v10

    .line 1044
    .local v4, "nextUpdate":J
    const-wide/16 v8, 0x0

    cmp-long v8, v4, v8

    if-gtz v8, :cond_1

    .line 1045
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    const/4 v9, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeferredUpdateWaiting:Z
    invoke-static {v8, v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$5802(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Z)Z

    .line 1046
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateUIForCurrentPhoto()V
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$5900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    goto :goto_0

    .line 1048
    :cond_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/os/Handler;

    move-result-object v8

    const/4 v9, 0x7

    invoke-virtual {v8, v9, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 1053
    .end local v4    # "nextUpdate":J
    :sswitch_6
    const/4 v7, 0x0

    .line 1054
    .local v7, "stayedOnCamera":Z
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/PhotoView;->getFilmMode()Z

    move-result v8

    if-nez v8, :cond_2

    .line 1055
    const/4 v7, 0x1

    .line 1063
    :goto_1
    if-eqz v7, :cond_0

    .line 1064
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getTotalMediaItemCount()I

    move-result v8

    const/4 v9, 0x1

    if-le v8, v9, :cond_6

    .line 1065
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$6000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/GalleryActivity;->isFromCamera()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1066
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->onBackPressed()V

    goto :goto_0

    .line 1056
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-gez v8, :cond_3

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getMediaItemCount()I

    move-result v8

    const/4 v9, 0x1

    if-le v8, v9, :cond_3

    .line 1058
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/ui/PhotoView;->switchToImage(I)V

    goto :goto_1

    .line 1060
    :cond_3
    const/4 v7, 0x1

    goto :goto_1

    .line 1069
    :cond_4
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v8

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v8

    if-nez v8, :cond_5

    .line 1070
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->launchCamera()V
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$6100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    .line 1078
    :cond_5
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/ui/PhotoView;->switchToImage(I)V

    goto/16 :goto_0

    .line 1080
    :cond_6
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateBars()V
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$5400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    .line 1081
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$6200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    move-result-object v9

    const/4 v10, 0x0

    invoke-interface {v9, v10}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v9

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateCurrentPhoto(Lcom/sec/android/gallery3d/data/MediaItem;)V
    invoke-static {v8, v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$6300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/android/gallery3d/data/MediaItem;)V

    goto/16 :goto_0

    .line 1087
    .end local v7    # "stayedOnCamera":Z
    :sswitch_7
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v8

    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-ne v8, v9, :cond_0

    .line 1088
    iget v8, p1, Landroid/os/Message;->arg1:I

    if-eqz v8, :cond_8

    const/4 v2, 0x1

    .line 1089
    .local v2, "isPanorama360":Z
    :goto_2
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    .line 1090
    .local v0, "contentUri":Landroid/net/Uri;
    const/4 v3, 0x0

    .line 1091
    .local v3, "panoramaIntent":Landroid/content/Intent;
    if-eqz v2, :cond_7

    .line 1092
    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->createSharePanoramaIntent(Landroid/net/Uri;)Landroid/content/Intent;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$6400(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v3

    .line 1094
    :cond_7
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v8

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->createShareIntent(Lcom/sec/android/gallery3d/data/MediaObject;)Landroid/content/Intent;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$6500(Lcom/sec/android/gallery3d/data/MediaObject;)Landroid/content/Intent;

    move-result-object v6

    .line 1096
    .local v6, "shareIntent":Landroid/content/Intent;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBar:Lcom/sec/android/gallery3d/app/GalleryActionBar;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$6600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/GalleryActionBar;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v8, v3, v6, v9}, Lcom/sec/android/gallery3d/app/GalleryActionBar;->setShareIntents(Landroid/content/Intent;Landroid/content/Intent;Landroid/widget/ShareActionProvider$OnShareTargetSelectedListener;)V

    .line 1097
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setNfcBeamPushUri(Landroid/net/Uri;)V
    invoke-static {v8, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$6700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 1088
    .end local v0    # "contentUri":Landroid/net/Uri;
    .end local v2    # "isPanorama360":Z
    .end local v3    # "panoramaIntent":Landroid/content/Intent;
    .end local v6    # "shareIntent":Landroid/content/Intent;
    :cond_8
    const/4 v2, 0x0

    goto :goto_2

    .line 1102
    :sswitch_8
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v8

    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-ne v8, v9, :cond_0

    .line 1103
    iget v8, p1, Landroid/os/Message;->arg1:I

    if-eqz v8, :cond_9

    const/4 v2, 0x1

    .line 1104
    .restart local v2    # "isPanorama360":Z
    :goto_3
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updatePanoramaUI(Z)V
    invoke-static {v8, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$6800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Z)V

    goto/16 :goto_0

    .line 1103
    .end local v2    # "isPanorama360":Z
    :cond_9
    const/4 v2, 0x0

    goto :goto_3

    .line 1110
    :sswitch_9
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setUserInteractionShowMode()V

    goto/16 :goto_0

    .line 1114
    :sswitch_a
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setDetailActionBarForNormal()V
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$6900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    .line 1115
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$7000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->onResume()V

    .line 1116
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$7100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->invalidateOptionsMenu()V

    goto/16 :goto_0

    .line 1122
    :sswitch_b
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/PositionController;->isInScale()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1123
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v8

    const v9, 0x3f866666    # 1.05f

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/ui/PhotoView;->startScale(F)V

    .line 1124
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/os/Handler;

    move-result-object v8

    const/16 v9, 0x65

    const-wide/16 v10, 0xa

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 1129
    :sswitch_c
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/PositionController;->isInScale()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1130
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v8

    const v9, 0x3f733333    # 0.95f

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/ui/PhotoView;->startScale(F)V

    .line 1131
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/os/Handler;

    move-result-object v8

    const/16 v9, 0x66

    const-wide/16 v10, 0xa

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 1136
    :sswitch_d
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/PhotoView;->endScale()V

    goto/16 :goto_0

    .line 1139
    :sswitch_e
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->showSideMirrorView(Z)Z

    goto/16 :goto_0

    .line 1143
    :sswitch_f
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 1144
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/FilmStripView;->hide()V

    goto/16 :goto_0

    .line 1148
    :sswitch_10
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 1149
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/FilmStripView;->show()V

    goto/16 :goto_0

    .line 1153
    :sswitch_11
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$7200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setPostOnAnimation(Z)V

    goto/16 :goto_0

    .line 1156
    :sswitch_12
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$7300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getActionBarView()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->selectionModeBarVisibiltiy(IZ)V

    goto/16 :goto_0

    .line 1160
    :sswitch_13
    :try_start_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateChangePlayerIcon()V

    .line 1161
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$7400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateViewModeIcons(Landroid/content/res/Configuration;)V
    invoke-static {v8, v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$7500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Landroid/content/res/Configuration;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1163
    :catch_0
    move-exception v1

    .line 1164
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_0

    .line 1022
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_4
        0x5 -> :sswitch_3
        0x6 -> :sswitch_6
        0x7 -> :sswitch_5
        0x8 -> :sswitch_7
        0x9 -> :sswitch_8
        0x64 -> :sswitch_a
        0x65 -> :sswitch_b
        0x66 -> :sswitch_c
        0x67 -> :sswitch_9
        0x68 -> :sswitch_d
        0x69 -> :sswitch_e
        0x6a -> :sswitch_f
        0x6b -> :sswitch_10
        0x6c -> :sswitch_11
        0x6d -> :sswitch_12
        0x6e -> :sswitch_13
    .end sparse-switch
.end method
