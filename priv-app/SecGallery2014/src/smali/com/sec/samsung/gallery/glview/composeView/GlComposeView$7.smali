.class Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;
.super Ljava/lang/Object;
.source "GlComposeView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 0

    .prologue
    .line 1133
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHoverEnter(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 7
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1135
    const-string v3, "GlComposeView"

    const-string v6, "onHover: Enter"

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, p1

    .line 1136
    check-cast v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v1, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 1137
    .local v1, "index":I
    shr-int/lit8 v0, v1, 0x10

    .line 1138
    .local v0, "albumIndex":I
    const v3, 0xffff

    and-int v2, v1, v3

    .line 1140
    .local v2, "photoIndex":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-nez v3, :cond_1

    :cond_0
    move v3, v4

    .line 1166
    :goto_0
    return v3

    .line 1142
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v4

    .line 1143
    goto :goto_0

    .line 1145
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->isIdle()Z

    move-result v3

    if-nez v3, :cond_3

    move v3, v4

    .line 1146
    goto :goto_0

    .line 1147
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->isEnabledQuickScroll()Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v4

    .line 1148
    goto :goto_0

    .line 1149
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->isDrawerVisible()Z

    move-result v3

    if-eqz v3, :cond_5

    move v3, v4

    .line 1150
    goto :goto_0

    .line 1151
    :cond_5
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->isIdle()Z

    move-result v3

    if-nez v3, :cond_6

    move v3, v4

    .line 1152
    goto :goto_0

    .line 1153
    :cond_6
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->isSlideShowMode:Z
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$1000(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)Z

    move-result v3

    if-eqz v3, :cond_7

    move v3, v4

    .line 1154
    goto :goto_0

    .line 1155
    :cond_7
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->getIndex()I

    move-result v3

    if-ne v3, v1, :cond_8

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->isIdleState()Z

    move-result v3

    if-nez v3, :cond_8

    move v3, v5

    .line 1156
    goto :goto_0

    .line 1157
    :cond_8
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiPickMode(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_9

    move v3, v4

    .line 1158
    goto/16 :goto_0

    .line 1159
    :cond_9
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isAlbumPickMode(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_a

    move v3, v4

    .line 1160
    goto/16 :goto_0

    .line 1162
    :cond_a
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mUseHovering:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$1100(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)I

    move-result v3

    if-ne v3, v5, :cond_b

    .line 1163
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v4, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    invoke-virtual {v3, p1, v4, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->requestStart(Lcom/sec/android/gallery3d/glcore/GlObject;Lcom/sec/android/gallery3d/data/MediaObject;I)V

    :goto_1
    move v3, v5

    .line 1166
    goto/16 :goto_0

    .line 1165
    :cond_b
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v4, v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    invoke-virtual {v3, p1, v4, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->requestStart(Lcom/sec/android/gallery3d/glcore/GlObject;Lcom/sec/android/gallery3d/data/MediaObject;I)V

    goto :goto_1
.end method

.method public onHoverExit(Lcom/sec/android/gallery3d/glcore/GlObject;)Z
    .locals 3
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/4 v0, 0x1

    .line 1176
    const-string v1, "GlComposeView"

    const-string v2, "onHover: Exit"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1177
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    if-nez v1, :cond_0

    .line 1178
    const-string v0, "GlComposeView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mHoverCtrl: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1179
    const/4 v0, 0x0

    .line 1183
    :goto_0
    return v0

    .line 1182
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->exitFromHoverView(Z)V

    goto :goto_0
.end method

.method public onHoverMove(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 2
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "dx"    # I
    .param p3, "dy"    # I

    .prologue
    const/4 v0, 0x0

    .line 1170
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    if-eqz v1, :cond_0

    .line 1171
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->isIdleState()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 1172
    :cond_0
    return v0
.end method
