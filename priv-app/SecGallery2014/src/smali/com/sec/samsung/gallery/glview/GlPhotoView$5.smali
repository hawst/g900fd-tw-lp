.class Lcom/sec/samsung/gallery/glview/GlPhotoView$5;
.super Ljava/lang/Object;
.source "GlPhotoView.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 0

    .prologue
    .line 894
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$5;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationDone()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 896
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$5;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;
    invoke-static {v0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$502(Lcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;)Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    .line 897
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$5;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnConvAnim:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$402(Lcom/sec/samsung/gallery/glview/GlPhotoView;Z)Z

    .line 898
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$5;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iput-boolean v1, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mLockPos:Z

    .line 899
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$5;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;
    invoke-static {v0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$102(Lcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;)Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    .line 900
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$5;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsResizing:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$802(Lcom/sec/samsung/gallery/glview/GlPhotoView;Z)Z

    .line 901
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$5;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # invokes: Lcom/sec/samsung/gallery/glview/GlPhotoView;->clearBoundarEffect()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$900(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    .line 902
    return-void
.end method
