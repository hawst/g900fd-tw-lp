.class public Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;
.super Ljava/lang/Object;
.source "DimensionConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/DimensionConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ThumbnailViewDimension"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;


# instance fields
.field public mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

.field public thumbnail_view_camera_icon_margin_left:I

.field public thumbnail_view_dateview_padding_left:I

.field public thumbnail_view_default_mode:I

.field public thumbnail_view_hide_split_mode_count:I

.field public thumbnail_view_hscroll_height:I

.field public thumbnail_view_icon_left_padding:I

.field public thumbnail_view_mode_count:I

.field public thumbnail_view_new_album_background_color:I

.field public thumbnail_view_new_album_font_color:I

.field public thumbnail_view_new_album_font_size:I

.field public thumbnail_view_new_album_font_size_ru:I

.field public thumbnail_view_new_album_hand_padding:I

.field public thumbnail_view_new_album_hand_size:I

.field public thumbnail_view_new_album_header_padding_left:I

.field public thumbnail_view_new_album_header_padding_top:I

.field public thumbnail_view_new_album_mode_count:I

.field public thumbnail_view_new_album_select_box_height:I

.field public thumbnail_view_new_album_select_box_width:I

.field public thumbnail_view_new_album_select_count_font_color:I

.field public thumbnail_view_new_album_select_count_font_size:I

.field public thumbnail_view_new_album_select_count_hpadding_right:I

.field public thumbnail_view_new_album_select_count_vpadding_top:I

.field public thumbnail_view_new_album_text_view_bottom_padding:I

.field public thumbnail_view_new_album_text_view_padding:I

.field public thumbnail_view_new_album_text_view_top_margin:I

.field public thumbnail_view_new_mark_height:I

.field public thumbnail_view_new_mark_right_margin:I

.field public thumbnail_view_new_mark_text_top_margin:I

.field public thumbnail_view_new_mark_top_margin:I

.field public thumbnail_view_new_mark_width:I

.field public thumbnail_view_play_icon_size:I

.field public thumbnail_view_search_result_title_height:I

.field public thumbnail_view_show_split_mode_count:I

.field public thumbnail_view_split_album_border_width:I

.field public thumbnail_view_split_album_count_font_size:I

.field public thumbnail_view_split_album_font_color:I

.field public thumbnail_view_split_album_font_size:I

.field public thumbnail_view_split_album_mode_count:I

.field public thumbnail_view_split_album_new_font_size:I

.field public thumbnail_view_split_album_new_padding:I

.field public thumbnail_view_split_album_selected_font_color:I

.field public thumbnail_view_split_album_seprator_ratio:F

.field public thumbnail_view_split_album_seprator_ratio_divider_height:F

.field public thumbnail_view_split_album_title_padding_left:I

.field public thumbnail_view_split_extra_gap:I

.field public thumbnail_view_split_focused_arrow_height:I

.field public thumbnail_view_split_focused_arrow_width:I


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 242
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 243
    .local v1, "r":Landroid/content/res/Resources;
    const v2, 0x7f0d02d6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_border_width:I

    .line 244
    const v2, 0x7f0c0065

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_font_color:I

    .line 245
    const v2, 0x7f0c0066

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_selected_font_color:I

    .line 246
    const v2, 0x7f0d0385

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_new_font_size:I

    .line 247
    const v2, 0x7f0d0386

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_new_padding:I

    .line 248
    const v2, 0x7f0d0387

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_font_size:I

    .line 249
    const v2, 0x7f0d0388

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_count_font_size:I

    .line 250
    const v2, 0x7f0d0389

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_title_padding_left:I

    .line 251
    const v2, 0x7f0e0025

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_seprator_ratio:F

    .line 252
    const v2, 0x7f0e0026

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_seprator_ratio_divider_height:F

    .line 253
    const v2, 0x7f0c0068

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_default_mode:I

    .line 254
    const v2, 0x7f0c0069

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_show_split_mode_count:I

    .line 255
    const v2, 0x7f0c006a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_hide_split_mode_count:I

    .line 256
    iget v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_show_split_mode_count:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_hide_split_mode_count:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_mode_count:I

    .line 257
    const v2, 0x7f0c0070

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_font_color:I

    .line 258
    const v2, 0x7f0d0391

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_hand_size:I

    .line 259
    const v2, 0x7f0d0392

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_hand_padding:I

    .line 260
    const v2, 0x7f0d0393

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_header_padding_top:I

    .line 261
    const v2, 0x7f0d0394

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_header_padding_left:I

    .line 262
    const v2, 0x7f0d0395

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_font_size_ru:I

    .line 263
    const v2, 0x7f0d0396

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_font_size:I

    .line 264
    const v2, 0x7f0d0397

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_text_view_padding:I

    .line 265
    const v2, 0x7f0d0398

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_text_view_bottom_padding:I

    .line 266
    const v2, 0x7f0d0399

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_text_view_top_margin:I

    .line 267
    const v2, 0x7f0d02d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_select_box_height:I

    .line 268
    const v2, 0x7f0c0071

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_select_count_font_color:I

    .line 269
    const v2, 0x7f0c0072

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_background_color:I

    .line 270
    const v2, 0x7f0d039a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_select_count_font_size:I

    .line 271
    const v2, 0x7f0d039b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_select_count_vpadding_top:I

    .line 272
    const v2, 0x7f0d039c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_select_count_hpadding_right:I

    .line 273
    const v2, 0x7f0c006c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_play_icon_size:I

    .line 275
    iget v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_mode_count:I

    mul-int/lit8 v2, v2, 0x2

    new-array v2, v2, [Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    .line 276
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_mode_count:I

    mul-int/lit8 v2, v2, 0x2

    if-ge v0, v2, :cond_0

    .line 277
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    new-instance v3, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {v3}, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;-><init>()V

    aput-object v3, v2, v0

    .line 278
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f09005a

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_hpadding_left:I

    .line 279
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f09005b

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_hpadding_right:I

    .line 280
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f09005c

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_hgap:I

    .line 281
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f09005d

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_vpadding_top:I

    .line 282
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f09005e

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_vgap:I

    .line 283
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f09005f

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_thumbnail_width:I

    .line 284
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090060

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_thumbnail_height:I

    .line 276
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 287
    :cond_0
    const v2, 0x7f0c006d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_mode_count:I

    .line 288
    const/4 v0, 0x0

    :goto_1
    iget v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_mode_count:I

    mul-int/lit8 v2, v2, 0x2

    if-ge v0, v2, :cond_1

    .line 289
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090061

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_hpadding_left:I

    .line 290
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090062

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_hgap:I

    .line 291
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090063

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_vpadding_top:I

    .line 292
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090064

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_vgap:I

    .line 293
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090065

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_thumbnail_width:I

    .line 294
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090066

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_thumbnail_height:I

    .line 295
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090067

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_name_textbox_hoffset:I

    .line 296
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090068

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_name_textbox_voffset:I

    .line 297
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090069

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_name_textbox_width:I

    .line 298
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f09006a

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_name_textbox_height:I

    .line 299
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f09006b

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_background_width:I

    .line 300
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f09006c

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_background_height:I

    .line 301
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f09006d

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_divider_height:I

    .line 288
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 304
    :cond_1
    const v2, 0x7f0c0073

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_mode_count:I

    .line 305
    const/4 v0, 0x0

    :goto_2
    iget v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_mode_count:I

    mul-int/lit8 v2, v2, 0x2

    if-ge v0, v2, :cond_2

    .line 306
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090073

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_new_album_hpadding_left:I

    .line 307
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090074

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_new_album_vpadding_top:I

    .line 308
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090075

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_new_album_vpadding_bottom:I

    .line 309
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090076

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_new_album_thumbnail_width:I

    .line 310
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090077

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_new_album_thumbnail_height:I

    .line 311
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090078

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_new_album_divider_height:I

    .line 312
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    aget-object v2, v2, v0

    const v3, 0x7f090079

    # invokes: Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I
    invoke-static {v1, v3, v0}, Lcom/sec/samsung/gallery/view/DimensionConfig;->access$000(Landroid/content/res/Resources;II)I

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_new_album_textbox_voffset:I

    .line 305
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 315
    :cond_2
    const v2, 0x7f0d0157

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_camera_icon_margin_left:I

    .line 317
    const v2, 0x7f0d015a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_mark_height:I

    .line 318
    const v2, 0x7f0d015b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_mark_width:I

    .line 319
    const v2, 0x7f0d0158

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_mark_top_margin:I

    .line 320
    const v2, 0x7f0d0159

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_mark_right_margin:I

    .line 321
    const v2, 0x7f0d015c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_mark_text_top_margin:I

    .line 323
    const v2, 0x7f0d038c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_focused_arrow_width:I

    .line 324
    const v2, 0x7f0d038d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_focused_arrow_height:I

    .line 325
    const v2, 0x7f0c006b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_extra_gap:I

    .line 326
    const v2, 0x7f0d038e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_icon_left_padding:I

    .line 327
    return-void
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 222
    const-class v1, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    if-nez v0, :cond_0

    .line 223
    new-instance v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    .line 225
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 222
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public setThumbnailViewNewAlbumSelectCountBoxWidth(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "selectedCount"    # I

    .prologue
    .line 229
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 230
    .local v0, "r":Landroid/content/res/Resources;
    const/16 v1, 0x2710

    if-lt p2, v1, :cond_0

    .line 231
    const v1, 0x7f0d02d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_select_box_width:I

    .line 239
    :goto_0
    return-void

    .line 232
    :cond_0
    const/16 v1, 0x3e8

    if-lt p2, v1, :cond_1

    .line 233
    const v1, 0x7f0d02d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_select_box_width:I

    goto :goto_0

    .line 234
    :cond_1
    const/16 v1, 0x64

    if-lt p2, v1, :cond_2

    .line 235
    const v1, 0x7f0d02d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_select_box_width:I

    goto :goto_0

    .line 237
    :cond_2
    const v1, 0x7f0d02d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_select_box_width:I

    goto :goto_0
.end method
