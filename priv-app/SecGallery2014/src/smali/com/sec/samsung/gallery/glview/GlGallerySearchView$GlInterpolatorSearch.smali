.class public Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GlInterpolatorSearch;
.super Lcom/sec/android/gallery3d/glcore/GlInterpolator;
.source "GlGallerySearchView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGallerySearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GlInterpolatorSearch"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V
    .locals 0

    .prologue
    .line 372
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GlInterpolatorSearch;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlInterpolator;-><init>()V

    .line 373
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 4
    .param p1, "ratio"    # F

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 382
    sub-float v0, v3, p1

    .line 383
    .local v0, "intVal":F
    mul-float v2, v0, v0

    sub-float v1, v3, v2

    .line 384
    .local v1, "retVal":F
    return v1
.end method

.method public reset()V
    .locals 0

    .prologue
    .line 376
    return-void
.end method
