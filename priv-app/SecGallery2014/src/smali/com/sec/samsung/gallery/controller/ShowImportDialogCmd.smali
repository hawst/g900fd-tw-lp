.class public Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "ShowImportDialogCmd.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

.field private mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

.field private mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field private title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;)Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private handleImportMedia(Lcom/sec/android/gallery3d/data/MediaObject;)V
    .locals 8
    .param p1, "mediaObject"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 69
    new-instance v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->title:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->title:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->showProgressDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 71
    const/4 v6, 0x0

    .line 72
    .local v6, "isSucceed":Z
    sget-object v0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->TAG:Ljava/lang/String;

    const-string v1, "handleImportMedia start"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    instance-of v0, p1, Lcom/sec/android/gallery3d/data/MtpImage;

    if-eqz v0, :cond_0

    .line 74
    sget-object v0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->TAG:Ljava/lang/String;

    const-string v1, "Call import"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    check-cast p1, Lcom/sec/android/gallery3d/data/MtpImage;

    .end local p1    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MtpImage;->Import()Z

    move-result v6

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->closeProgressDialog()V

    .line 80
    if-eqz v6, :cond_1

    const v7, 0x7f0e009b

    .line 81
    .local v7, "strId":I
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mContext:Landroid/content/Context;

    invoke-static {v0, v7}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 82
    return-void

    .line 80
    .end local v7    # "strId":I
    :cond_1
    const v7, 0x7f0e009c

    goto :goto_0
.end method

.method private handleImportMedias()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 85
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v6

    .line 86
    .local v6, "mediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mContext:Landroid/content/Context;

    const v1, 0x7f0e009a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 87
    .local v2, "title":Ljava/lang/String;
    new-instance v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v1

    invoke-direct {v0, v4, v1}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;-><init>(II)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    .line 90
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mContext:Landroid/content/Context;

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->showProgressDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 92
    new-instance v7, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd$1;

    invoke-direct {v7, p0}, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;)V

    .line 123
    .local v7, "onProgressListener":Lcom/sec/android/gallery3d/data/OnProgressListener;
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    invoke-virtual {v0, v3, v7}, Lcom/sec/samsung/gallery/util/FileUtil;->operateMedias(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/OnProgressListener;)Lcom/sec/samsung/gallery/util/MediaOperations;

    .line 124
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 5
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 49
    sget-object v3, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->TAG:Ljava/lang/String;

    const-string v4, "execute()"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Object;

    move-object v2, v3

    check-cast v2, [Ljava/lang/Object;

    .line 51
    .local v2, "params":[Ljava/lang/Object;
    const/4 v3, 0x0

    aget-object v3, v2, v3

    check-cast v3, Landroid/content/Context;

    iput-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mContext:Landroid/content/Context;

    .line 53
    new-instance v3, Lcom/sec/samsung/gallery/util/FileUtil;

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/samsung/gallery/util/FileUtil;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    .line 55
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 56
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 58
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 59
    .local v0, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    sget-object v3, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->TAG:Ljava/lang/String;

    const-string v4, "ShowImportDialogCmd start"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    instance-of v3, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    if-nez v3, :cond_0

    instance-of v3, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-eqz v3, :cond_2

    .line 61
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->handleImportMedias()V

    .line 66
    :cond_1
    :goto_0
    return-void

    .line 62
    :cond_2
    instance-of v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-eqz v3, :cond_1

    .line 63
    const/4 v3, 0x1

    aget-object v1, v2, v3

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 64
    .local v1, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->handleImportMedia(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/FileUtil;->cancelOperation()V

    .line 129
    return-void
.end method
