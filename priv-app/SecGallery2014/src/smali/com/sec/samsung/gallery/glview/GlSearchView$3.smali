.class Lcom/sec/samsung/gallery/glview/GlSearchView$3;
.super Ljava/lang/Object;
.source "GlSearchView.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlSearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V
    .locals 0

    .prologue
    .line 390
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFlingEnd(F)V
    .locals 4
    .param p1, "delta"    # F

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 414
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$600(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlBlurObject;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 415
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$600(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlBlurObject;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->show(IF)V

    .line 416
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$600(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlBlurObject;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->show(IF)V

    .line 418
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iput p1, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mSx:F

    .line 419
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mPitchFact:F
    invoke-static {v0, v3}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$402(Lcom/sec/samsung/gallery/glview/GlSearchView;F)F

    .line 420
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->mSx:F

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->setScroll(F)V

    .line 421
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setCommand(IIII)V

    .line 422
    return-void
.end method

.method public onFlingProcess(FF)V
    .locals 6
    .param p1, "delta"    # F
    .param p2, "elastic"    # F

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x41f00000    # 30.0f

    const/high16 v3, -0x3e100000    # -30.0f

    const/4 v2, 0x0

    .line 393
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iput p1, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mSx:F

    .line 394
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->mSx:F

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->setScroll(F)V

    .line 395
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mUsePitchBoundary:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$300(Lcom/sec/samsung/gallery/glview/GlSearchView;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 396
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$500(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->getSpeedRatio()F

    move-result v1

    neg-float v1, v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    # setter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mPitchFact:F
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$402(Lcom/sec/samsung/gallery/glview/GlSearchView;F)F

    .line 397
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mPitchFact:F
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$400(Lcom/sec/samsung/gallery/glview/GlSearchView;)F

    move-result v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_1

    .line 398
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mPitchFact:F
    invoke-static {v0, v4}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$402(Lcom/sec/samsung/gallery/glview/GlSearchView;F)F

    .line 401
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    neg-float v1, p2

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setThumbnailPitchRate(FLcom/sec/samsung/gallery/glview/GlScroller;)V

    .line 410
    :goto_1
    return-void

    .line 399
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mPitchFact:F
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$400(Lcom/sec/samsung/gallery/glview/GlSearchView;)F

    move-result v0

    cmpg-float v0, v0, v3

    if-gez v0, :cond_0

    .line 400
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mPitchFact:F
    invoke-static {v0, v3}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$402(Lcom/sec/samsung/gallery/glview/GlSearchView;F)F

    goto :goto_0

    .line 403
    :cond_2
    cmpl-float v0, p2, v2

    if-lez v0, :cond_3

    .line 404
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$600(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlBlurObject;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p2}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->show(IF)V

    goto :goto_1

    .line 405
    :cond_3
    cmpg-float v0, p2, v2

    if-gez v0, :cond_4

    .line 406
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$600(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlBlurObject;

    move-result-object v0

    invoke-virtual {v0, v5, p2}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->show(IF)V

    goto :goto_1

    .line 408
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$600(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlBlurObject;

    move-result-object v0

    invoke-virtual {v0, v5, v2}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->show(IF)V

    goto :goto_1
.end method
