.class Lcom/sec/samsung/gallery/view/eventview/EventViewState$8;
.super Ljava/lang/Object;
.source "EventViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/eventview/EventViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V
    .locals 0

    .prologue
    .line 392
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$8;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGenericMotionCancel(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 3
    .param p1, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    const/4 v2, -0x1

    .line 412
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$8;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->getGenericMotionFocus()I

    move-result v0

    .line 413
    .local v0, "oldFocusedIndex":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$8;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    invoke-virtual {v1, v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->updateBorder(II)V

    .line 414
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$8;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->setGenericMotionFocus(I)V

    .line 415
    return-void
.end method

.method public onGenericMotionEnter(Lcom/sec/android/gallery3d/glcore/GlLayer;I)V
    .locals 4
    .param p1, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "position"    # I

    .prologue
    const/4 v3, -0x1

    .line 396
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$8;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->getGenericMotionFocus()I

    move-result v0

    .line 398
    .local v0, "oldFocusedIndex":I
    if-ne v0, p2, :cond_1

    .line 408
    :cond_0
    :goto_0
    return-void

    .line 401
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$8;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    invoke-virtual {v1, v0, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->updateBorder(II)V

    .line 402
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$8;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$8;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->getGenericMotionTitleFocus()I

    move-result v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->updateTitleBorder(II)V

    .line 403
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$8;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    if-eqz v1, :cond_0

    .line 404
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$8;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v1, p2}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->setGenericMotionFocus(I)V

    .line 405
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$8;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->setGenericMotionTitleFocus(I)V

    .line 406
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$8;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$8;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->access$000(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v3

    invoke-virtual {v1, v2, p2, v3}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(Ljava/lang/Object;ILcom/sec/android/gallery3d/ui/SelectionManager;)V

    goto :goto_0
.end method
