.class public Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;
.super Ljava/lang/Object;
.source "PrintImageForPhotoNote.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field mContext:Landroid/content/Context;

.field mUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;->mContext:Landroid/content/Context;

    .line 51
    iput-object p2, p0, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;->mUri:Landroid/net/Uri;

    .line 52
    return-void
.end method

.method private isEpsonPrinter()Z
    .locals 2

    .prologue
    .line 99
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Common_AddExtMobilePrint"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Epson"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private toFileType(Ljava/lang/String;)I
    .locals 2
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 103
    const/4 v0, 0x0

    .line 105
    .local v0, "fileType":I
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 106
    const-string v1, "bmp"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 107
    const/4 v0, 0x1

    .line 115
    :cond_0
    :goto_0
    return v0

    .line 108
    :cond_1
    const-string v1, "jpg"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "jpeg"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 109
    :cond_2
    const/4 v0, 0x2

    goto :goto_0

    .line 110
    :cond_3
    const-string v1, "png"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 111
    const/4 v0, 0x3

    goto :goto_0

    .line 112
    :cond_4
    const-string/jumbo v1, "tif"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string/jumbo v1, "tiff"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 113
    :cond_5
    const/4 v0, 0x4

    goto :goto_0
.end method


# virtual methods
.method public doPrint()V
    .locals 5

    .prologue
    .line 119
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    const-string v3, "print"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/print/PrintManager;

    .line 120
    .local v1, "printManager":Landroid/print/PrintManager;
    const-string v0, "gallery"

    .line 122
    .local v0, "jobName":Ljava/lang/String;
    const-string v2, "gallery"

    new-instance v3, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote$1;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote$1;-><init>(Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;)V

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/print/PrintManager;->print(Ljava/lang/String;Landroid/print/PrintDocumentAdapter;Landroid/print/PrintAttributes;)Landroid/print/PrintJob;

    .line 201
    return-void
.end method

.method public excute()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 55
    iget-object v8, p0, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;->mUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 56
    .local v2, "filePath":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;->mUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    .line 57
    .local v1, "fileName":Ljava/lang/String;
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v8

    invoke-virtual {v8}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v4

    .line 59
    .local v4, "fileUri":Ljava/lang/String;
    const/4 v7, 0x0

    .line 61
    .local v7, "intent":Landroid/content/Intent;
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMobilePrint:Z

    if-eqz v8, :cond_2

    .line 62
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;->isEpsonPrinter()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 63
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;->toFileType(Ljava/lang/String;)I

    move-result v3

    .line 65
    .local v3, "fileType":I
    if-nez v3, :cond_0

    .line 66
    iget-object v8, p0, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;->mContext:Landroid/content/Context;

    const v9, 0x7f0e0107

    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 96
    .end local v3    # "fileType":I
    :goto_0
    return-void

    .line 70
    .restart local v3    # "fileType":I
    :cond_0
    new-instance v7, Landroid/content/Intent;

    .end local v7    # "intent":Landroid/content/Intent;
    const-string v8, "com.epson.mobilephone.samsungprint.photo"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 71
    .restart local v7    # "intent":Landroid/content/Intent;
    const-string v8, "/"

    invoke-virtual {v2, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    .line 72
    .local v6, "index":I
    const-string v8, "FILE_TYPE"

    invoke-virtual {v7, v8, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 73
    const-string v8, "FOLDER_NAME"

    invoke-virtual {v2, v10, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 75
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 76
    .local v5, "filesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    add-int/lit8 v8, v6, 0x1

    invoke-virtual {v2, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    const-string v8, "FILE_NAME"

    invoke-virtual {v7, v8, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 79
    const-string v8, "PRINT_MODE"

    invoke-virtual {v7, v8, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 80
    const-string v8, "PACKAGE_NAME"

    iget-object v9, p0, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 88
    .end local v3    # "fileType":I
    .end local v5    # "filesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v6    # "index":I
    :goto_1
    :try_start_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;->mContext:Landroid/content/Context;

    invoke-virtual {v8, v7}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v8, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;->TAG:Ljava/lang/String;

    const-string v9, "Activity Not Found"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 82
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_1
    new-instance v7, Landroid/content/Intent;

    .end local v7    # "intent":Landroid/content/Intent;
    const-string v8, "com.sec.android.app.mobileprint.PRINT_SETTING"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 83
    .restart local v7    # "intent":Landroid/content/Intent;
    const-string v8, "android.intent.extra.STREAM"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 84
    const-string v8, "android.intent.extra.TITLE"

    const-string v9, "gallery"

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 85
    const-string v8, "android.intent.extra.SUBJECT"

    invoke-virtual {v7, v8, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 94
    :cond_2
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;->doPrint()V

    goto :goto_0
.end method
