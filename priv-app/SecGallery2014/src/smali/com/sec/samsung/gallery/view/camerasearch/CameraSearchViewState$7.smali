.class Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$7;
.super Ljava/lang/Object;
.source "CameraSearchViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)V
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$7;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStatusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V
    .locals 2
    .param p1, "parentView"    # Lcom/sec/samsung/gallery/glview/GlAbsListView;
    .param p2, "status"    # I
    .param p3, "parm"    # I

    .prologue
    .line 226
    packed-switch p2, :pswitch_data_0

    .line 236
    :goto_0
    return-void

    .line 228
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$7;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->access$1300(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    const-string v1, "searchColWide"

    invoke-static {v0, v1, p3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0

    .line 232
    :pswitch_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$7;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->access$1400(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    const-string v1, "searchColPort"

    invoke-static {v0, v1, p3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0

    .line 226
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
