.class public Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;
.super Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;
.source "SlideImageItem.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/FutureListener;
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem$LargeThumbnailJob;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;",
        "Lcom/sec/android/gallery3d/util/FutureListener",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# static fields
.field private static final SLIDESHOW_BURST_IMAGE_SIZE:I = 0x320

.field private static SLIDESHOW_IMAGE_SIZE:I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mFuture:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mIndex:I

.field private mIsBurstPlay:Z

.field private mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->TAG:Ljava/lang/String;

    .line 25
    const/16 v0, 0x400

    sput v0, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->SLIDESHOW_IMAGE_SIZE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;IZ)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p3, "index"    # I
    .param p4, "isBurstPlay"    # Z

    .prologue
    .line 37
    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;-><init>(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 31
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 39
    iput p3, p0, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->mIndex:I

    move-object v2, p1

    .line 40
    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    .line 41
    iput-boolean p4, p0, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->mIsBurstPlay:Z

    .line 43
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 44
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    const-string/jumbo v2, "window"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 45
    .local v1, "wm":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 46
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0x1

    sput v2, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->SLIDESHOW_IMAGE_SIZE:I

    .line 47
    return-void
.end method


# virtual methods
.method protected cancelLoadBitmap()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->mFuture:Lcom/sec/android/gallery3d/util/Future;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->mFuture:Lcom/sec/android/gallery3d/util/Future;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 60
    :cond_0
    return-void
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->mIndex:I

    return v0
.end method

.method protected onBitmapAvailable(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 64
    sget-object v0, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onBitmapAvailable ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->mIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    return-void
.end method

.method public onFutureDone(Lcom/sec/android/gallery3d/util/Future;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 81
    .local p1, "future":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<Landroid/graphics/Bitmap;>;"
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 82
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/Future;->isCancelled()Z

    move-result v0

    .line 83
    .local v0, "isCancelled":Z
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v1, v0}, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->updateImage(Landroid/graphics/Bitmap;Z)V

    .line 84
    return-void
.end method

.method protected recycleBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 88
    return-void
.end method

.method public render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;I)I
    .locals 1
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "pass"    # I

    .prologue
    .line 69
    const/4 v0, 0x0

    return v0
.end method

.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 74
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v1

    .line 75
    .local v1, "job":Lcom/sec/android/gallery3d/util/ThreadPool$Job;, "Lcom/sec/android/gallery3d/util/ThreadPool$Job<Landroid/graphics/Bitmap;>;"
    invoke-interface {v1, p1}, Lcom/sec/android/gallery3d/util/ThreadPool$Job;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 76
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    return-object v0
.end method

.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected startLoadBitmap()V
    .locals 4

    .prologue
    .line 51
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->mIsBurstPlay:Z

    if-eqz v1, :cond_0

    const/16 v0, 0x320

    .line 52
    .local v0, "targetSize":I
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    new-instance v2, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem$LargeThumbnailJob;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-direct {v2, v3, v0}, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem$LargeThumbnailJob;-><init>(Lcom/sec/android/gallery3d/data/MediaItem;I)V

    invoke-virtual {v1, v2, p0}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->mFuture:Lcom/sec/android/gallery3d/util/Future;

    .line 53
    return-void

    .line 51
    .end local v0    # "targetSize":I
    :cond_0
    sget v0, Lcom/sec/samsung/gallery/view/image_manager/SlideImageItem;->SLIDESHOW_IMAGE_SIZE:I

    goto :goto_0
.end method
