.class public Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
.super Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;
.source "ComposeImageItem.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/FutureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;",
        "Lcom/sec/android/gallery3d/util/FutureListener",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# static fields
.field public static DEFAULT_VERSION:J = 0x0L

.field private static final TAG:Ljava/lang/String; = "ComposeImageItem"


# instance fields
.field public mBitmap:Landroid/graphics/Bitmap;

.field public mCanceled:Z

.field private mFuture:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public mGlView:Lcom/sec/android/gallery3d/glcore/GlView;

.field public mIndex:I

.field public mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mOnBitmapAvailableListener:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

.field public mThmType:I

.field private mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

.field public mType:I

.field public mUrgent:Z

.field public mVersion:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->DEFAULT_VERSION:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;IIILandroid/graphics/Bitmap;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p3, "index"    # I
    .param p4, "level"    # I
    .param p5, "type"    # I
    .param p6, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 84
    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;-><init>(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 23
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mOnBitmapAvailableListener:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

    .line 24
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 27
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mGlView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 28
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 32
    sget-wide v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->DEFAULT_VERSION:J

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mVersion:J

    .line 33
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mCanceled:Z

    .line 34
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mUrgent:Z

    .line 86
    if-ne p4, v3, :cond_1

    .line 87
    iput v5, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThmType:I

    .line 92
    :cond_0
    :goto_0
    iput p5, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mType:I

    .line 93
    iput p3, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mIndex:I

    .line 94
    iput-object p6, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 95
    check-cast p1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    .line 96
    return-void

    .line 88
    .restart local p1    # "context":Landroid/content/Context;
    :cond_1
    if-ne p4, v4, :cond_2

    .line 89
    iput v4, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThmType:I

    goto :goto_0

    .line 90
    :cond_2
    if-ne p4, v5, :cond_0

    .line 91
    iput v3, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThmType:I

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;IIILcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p3, "index"    # I
    .param p4, "level"    # I
    .param p5, "type"    # I
    .param p6, "listener"    # Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 53
    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;-><init>(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 23
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mOnBitmapAvailableListener:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

    .line 24
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 27
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mGlView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 28
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 32
    sget-wide v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->DEFAULT_VERSION:J

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mVersion:J

    .line 33
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mCanceled:Z

    .line 34
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mUrgent:Z

    .line 55
    if-ne p4, v3, :cond_1

    .line 56
    iput v5, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThmType:I

    .line 61
    :cond_0
    :goto_0
    iput p5, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mType:I

    .line 62
    iput p3, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mIndex:I

    .line 63
    check-cast p1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    .line 64
    iput-object p6, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mOnBitmapAvailableListener:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

    .line 65
    return-void

    .line 57
    .restart local p1    # "context":Landroid/content/Context;
    :cond_1
    if-ne p4, v4, :cond_2

    .line 58
    iput v4, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThmType:I

    goto :goto_0

    .line 59
    :cond_2
    if-ne p4, v5, :cond_0

    .line 60
    iput v3, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThmType:I

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;IIIZLcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p3, "index"    # I
    .param p4, "level"    # I
    .param p5, "type"    # I
    .param p6, "urgent"    # Z
    .param p7, "listener"    # Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 68
    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;-><init>(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 23
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mOnBitmapAvailableListener:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

    .line 24
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 27
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mGlView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 28
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 32
    sget-wide v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->DEFAULT_VERSION:J

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mVersion:J

    .line 33
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mCanceled:Z

    .line 34
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mUrgent:Z

    .line 70
    if-ne p4, v3, :cond_1

    .line 71
    iput v5, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThmType:I

    .line 76
    :cond_0
    :goto_0
    iput p5, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mType:I

    .line 77
    iput p3, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mIndex:I

    .line 78
    check-cast p1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    .line 79
    iput-object p7, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mOnBitmapAvailableListener:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

    .line 80
    iput-boolean p6, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mUrgent:Z

    .line 81
    return-void

    .line 72
    .restart local p1    # "context":Landroid/content/Context;
    :cond_1
    if-ne p4, v4, :cond_2

    .line 73
    iput v4, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThmType:I

    goto :goto_0

    .line 74
    :cond_2
    if-ne p4, v5, :cond_0

    .line 75
    iput v3, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThmType:I

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;IIILcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p4, "index"    # I
    .param p5, "level"    # I
    .param p6, "type"    # I
    .param p7, "listener"    # Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 37
    invoke-direct {p0, p3}, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;-><init>(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 23
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mOnBitmapAvailableListener:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

    .line 24
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 27
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mGlView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 28
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 32
    sget-wide v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->DEFAULT_VERSION:J

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mVersion:J

    .line 33
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mCanceled:Z

    .line 34
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mUrgent:Z

    .line 39
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 40
    if-ne p5, v3, :cond_1

    .line 41
    iput v5, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThmType:I

    .line 46
    :cond_0
    :goto_0
    iput p6, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mType:I

    .line 47
    iput p4, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mIndex:I

    .line 48
    check-cast p1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    .line 49
    iput-object p7, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mOnBitmapAvailableListener:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

    .line 50
    return-void

    .line 42
    .restart local p1    # "context":Landroid/content/Context;
    :cond_1
    if-ne p5, v4, :cond_2

    .line 43
    iput v4, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThmType:I

    goto :goto_0

    .line 44
    :cond_2
    if-ne p5, v5, :cond_0

    .line 45
    iput v3, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThmType:I

    goto :goto_0
.end method


# virtual methods
.method protected cancelLoadBitmap()V
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mCanceled:Z

    .line 113
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mFuture:Lcom/sec/android/gallery3d/util/Future;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mFuture:Lcom/sec/android/gallery3d/util/Future;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 116
    :cond_0
    return-void
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getItem()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method public getVersion()J
    .locals 2

    .prologue
    .line 165
    iget-wide v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mVersion:J

    return-wide v0
.end method

.method public isBitmapReady()Z
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onBitmapAvailable(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mCanceled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mOnBitmapAvailableListener:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mOnBitmapAvailableListener:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

    invoke-interface {v0, p0}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;->onBitmapAvailable(Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;)V

    .line 127
    :cond_0
    return-void
.end method

.method public onFutureDone(Lcom/sec/android/gallery3d/util/Future;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 141
    .local p1, "future":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<Landroid/graphics/Bitmap;>;"
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 142
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/Future;->isCancelled()Z

    move-result v0

    .line 143
    .local v0, "isCancelled":Z
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v1, v0}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->updateImage(Landroid/graphics/Bitmap;Z)V

    .line 144
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/util/ThreadPool;->getQueuedCount()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mOnBitmapAvailableListener:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

    if-eqz v1, :cond_0

    .line 145
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mOnBitmapAvailableListener:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;->onAllContentReady()V

    .line 146
    :cond_0
    return-void
.end method

.method protected recycleBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 131
    invoke-static {}, Lcom/sec/android/photos/data/GalleryBitmapPool;->getInstance()Lcom/sec/android/photos/data/GalleryBitmapPool;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/photos/data/GalleryBitmapPool;->put(Landroid/graphics/Bitmap;)Z

    .line 132
    return-void
.end method

.method public render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;I)I
    .locals 1
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "pass"    # I

    .prologue
    .line 136
    const/4 v0, 0x0

    return v0
.end method

.method public setThumbnailType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 99
    iput p1, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThmType:I

    .line 100
    return-void
.end method

.method public setVersion(J)V
    .locals 1
    .param p1, "version"    # J

    .prologue
    .line 161
    iput-wide p1, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mVersion:J

    .line 162
    return-void
.end method

.method protected startLoadBitmap()V
    .locals 3

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mUrgent:Z

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    iget v2, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThmType:I

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/gallery3d/util/ThreadPool;->submitUrgent(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mFuture:Lcom/sec/android/gallery3d/util/Future;

    .line 108
    :goto_0
    return-void

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    iget v2, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThmType:I

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mFuture:Lcom/sec/android/gallery3d/util/Future;

    goto :goto_0
.end method

.method protected startLoadBitmapLatency()V
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mFuture:Lcom/sec/android/gallery3d/util/Future;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/util/ThreadPool;->submitToLast(Lcom/sec/android/gallery3d/util/Future;)Lcom/sec/android/gallery3d/util/Future;

    .line 121
    return-void
.end method
