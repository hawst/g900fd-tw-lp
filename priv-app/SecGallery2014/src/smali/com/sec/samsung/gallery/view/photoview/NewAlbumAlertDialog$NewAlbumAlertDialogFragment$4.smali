.class Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$4;
.super Ljava/lang/Object;
.source "NewAlbumAlertDialog.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;)V
    .locals 0

    .prologue
    .line 255
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$4;->this$1:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/4 v1, -0x1

    .line 262
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$4;->this$1:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 267
    :goto_0
    return-void

    .line 265
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$4;->this$1:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 271
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 258
    return-void
.end method
