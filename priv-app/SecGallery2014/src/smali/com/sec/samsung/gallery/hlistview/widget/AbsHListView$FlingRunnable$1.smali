.class Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable$1;
.super Ljava/lang/Object;
.source "AbsHListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;)V
    .locals 0

    .prologue
    .line 3580
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable$1;->this$1:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 3584
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable$1;->this$1:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    iget-object v4, v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    # getter for: Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mActivePointerId:I
    invoke-static {v4}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->access$800(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)I

    move-result v0

    .line 3585
    .local v0, "activeId":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable$1;->this$1:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    iget-object v4, v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    # getter for: Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mVelocityTracker:Landroid/view/VelocityTracker;
    invoke-static {v4}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->access$900(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)Landroid/view/VelocityTracker;

    move-result-object v2

    .line 3586
    .local v2, "vt":Landroid/view/VelocityTracker;
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable$1;->this$1:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    # getter for: Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->mScroller:Lcom/sec/samsung/gallery/hlistview/widget/OverScroller;
    invoke-static {v4}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->access$1000(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;)Lcom/sec/samsung/gallery/hlistview/widget/OverScroller;

    move-result-object v1

    .line 3587
    .local v1, "scroller":Lcom/sec/samsung/gallery/hlistview/widget/OverScroller;
    if-eqz v2, :cond_0

    const/4 v4, -0x1

    if-ne v0, v4, :cond_1

    .line 3602
    :cond_0
    :goto_0
    return-void

    .line 3591
    :cond_1
    const/16 v4, 0x3e8

    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable$1;->this$1:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    iget-object v5, v5, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    # getter for: Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMaximumVelocity:I
    invoke-static {v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->access$1100(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v2, v4, v5}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 3592
    invoke-virtual {v2, v0}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v4

    neg-float v3, v4

    .line 3594
    .local v3, "xvel":F
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable$1;->this$1:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    iget-object v5, v5, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    # getter for: Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMinimumVelocity:I
    invoke-static {v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->access$1200(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)I

    move-result v5

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-ltz v4, :cond_2

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Lcom/sec/samsung/gallery/hlistview/widget/OverScroller;->isScrollingInDirection(FF)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 3596
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable$1;->this$1:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    iget-object v4, v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    const-wide/16 v6, 0x28

    invoke-virtual {v4, p0, v6, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 3598
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable$1;->this$1:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->endFling()V

    .line 3599
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable$1;->this$1:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    iget-object v4, v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    const/4 v5, 0x3

    iput v5, v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 3600
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable$1;->this$1:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    iget-object v4, v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->reportScrollStateChange(I)V

    goto :goto_0
.end method
