.class public Lcom/sec/samsung/gallery/controller/ShowContentToDisplayDialogCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "ShowContentToDisplayDialogCmd.java"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 2
    .param p1, "notification"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 18
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowContentToDisplayDialogCmd;->mContext:Landroid/content/Context;

    .line 19
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlexibleCloudAlbumFormat:Z

    if-eqz v1, :cond_0

    .line 20
    new-instance v0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowContentToDisplayDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;-><init>(Landroid/content/Context;)V

    .line 21
    .local v0, "dialog":Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->showDialog()V

    .line 26
    .end local v0    # "dialog":Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;
    :goto_0
    return-void

    .line 23
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowContentToDisplayDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;-><init>(Landroid/content/Context;)V

    .line 24
    .local v0, "dialog":Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->showDialog()V

    goto :goto_0
.end method
