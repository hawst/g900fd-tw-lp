.class public Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "MusicListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final MAX_NAME_TEXT_LENGTH:I = 0x14


# instance fields
.field private mInflater:Landroid/view/LayoutInflater;

.field private mRadioButtonResourceId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textViewResourceId"    # I

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 31
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 33
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 34
    .local v0, "value":Landroid/util/TypedValue;
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x1010219

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 35
    iget v1, v0, Landroid/util/TypedValue;->resourceId:I

    iput v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;->mRadioButtonResourceId:I

    .line 41
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;->reloadItems(Landroid/content/Context;)V

    .line 42
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;

    .line 56
    .local v3, "item":Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;->getUri()Landroid/net/Uri;

    move-result-object v7

    .line 57
    .local v7, "uri":Landroid/net/Uri;
    const/4 v5, 0x0

    .line 58
    .local v5, "title":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;->isAddMusic()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 59
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v9, 0x7f0300da

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    move-object v2, p2

    .line 94
    .end local p2    # "convertView":Landroid/view/View;
    .local v2, "convertView":Landroid/view/View;
    :goto_0
    return-object v2

    .line 63
    .end local v2    # "convertView":Landroid/view/View;
    .restart local p2    # "convertView":Landroid/view/View;
    :cond_0
    if-eqz p2, :cond_1

    instance-of v8, p2, Landroid/widget/RelativeLayout;

    if-eqz v8, :cond_2

    .line 64
    :cond_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v9, 0x7f0300d8

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 66
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0d01f6

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 67
    .local v6, "top_bottom_padding":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0d01f5

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 68
    .local v4, "start_padding":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 70
    .local v1, "config":Landroid/content/res/Configuration;
    invoke-virtual {v1}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_4

    .line 71
    const/4 v8, 0x0

    invoke-virtual {p2, v8, v6, v4, v6}, Landroid/view/View;->setPadding(IIII)V

    .end local v1    # "config":Landroid/content/res/Configuration;
    .end local v4    # "start_padding":I
    .end local v6    # "top_bottom_padding":I
    :cond_2
    :goto_1
    move-object v0, p2

    .line 77
    check-cast v0, Landroid/widget/CheckedTextView;

    .line 78
    .local v0, "cTextView":Landroid/widget/CheckedTextView;
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v8, :cond_5

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    const/16 v9, 0x14

    if-le v8, v9, :cond_5

    .line 79
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;->getTitle()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/16 v11, 0x14

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "..."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 80
    invoke-virtual {v0, v5}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    :goto_2
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;->isAddMusic()Z

    move-result v8

    if-eqz v8, :cond_6

    const v8, 0x7f02050b

    :goto_3
    invoke-virtual {v0, v8}, Landroid/widget/CheckedTextView;->setCheckMarkDrawable(I)V

    .line 89
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;->isAddMusic()Z

    move-result v8

    if-nez v8, :cond_3

    .line 90
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getMusicSelected(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    invoke-virtual {v0, v8}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    :cond_3
    move-object v2, p2

    .line 94
    .end local p2    # "convertView":Landroid/view/View;
    .restart local v2    # "convertView":Landroid/view/View;
    goto/16 :goto_0

    .line 73
    .end local v0    # "cTextView":Landroid/widget/CheckedTextView;
    .end local v2    # "convertView":Landroid/view/View;
    .restart local v1    # "config":Landroid/content/res/Configuration;
    .restart local v4    # "start_padding":I
    .restart local v6    # "top_bottom_padding":I
    .restart local p2    # "convertView":Landroid/view/View;
    :cond_4
    const/4 v8, 0x0

    invoke-virtual {p2, v4, v6, v8, v6}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_1

    .line 82
    .end local v1    # "config":Landroid/content/res/Configuration;
    .end local v4    # "start_padding":I
    .end local v6    # "top_bottom_padding":I
    .restart local v0    # "cTextView":Landroid/widget/CheckedTextView;
    :cond_5
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 84
    :cond_6
    iget v8, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;->mRadioButtonResourceId:I

    goto :goto_3
.end method

.method public reloadItems(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;->clear()V

    .line 47
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder;->createNoneItem(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;->add(Ljava/lang/Object;)V

    .line 48
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder;->createDefaultMusicItems(Landroid/content/Context;)Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;->addAll(Ljava/util/Collection;)V

    .line 49
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder;->createAddedMusicItems(Landroid/content/Context;)Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;->addAll(Ljava/util/Collection;)V

    .line 50
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder;->createAddSlideshowMusicItem(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;->add(Ljava/lang/Object;)V

    .line 51
    return-void
.end method
