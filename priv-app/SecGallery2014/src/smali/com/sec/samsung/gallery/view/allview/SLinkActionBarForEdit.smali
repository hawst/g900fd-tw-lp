.class public Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;
.source "SLinkActionBarForEdit.java"


# instance fields
.field private mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

.field private mMenu:Landroid/view/Menu;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/view/utils/EditModeHelper;I)V
    .locals 0
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "editModeHelper"    # Lcom/sec/samsung/gallery/view/utils/EditModeHelper;
    .param p3, "style"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1, p3}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;I)V

    .line 34
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;)Landroid/view/Menu;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mMenu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->setSelectAllButtonTitle(ILjava/lang/String;)V

    return-void
.end method

.method private isValidSelection()Z
    .locals 8

    .prologue
    .line 155
    const/4 v1, 0x1

    .line 156
    .local v1, "isValid":Z
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 157
    .local v0, "count":I
    if-nez v0, :cond_1

    .line 158
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v4, 0x7f0e0113

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 159
    const/4 v1, 0x0

    .line 167
    :cond_0
    :goto_0
    return v1

    .line 160
    :cond_1
    sget v3, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    if-le v0, v3, :cond_0

    .line 161
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0114

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    sget v7, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 163
    .local v2, "text":Ljava/lang/CharSequence;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 164
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 39
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 40
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f12003a

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 41
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 84
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 86
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->isValidSelection()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->chooseShareDialog()V

    goto :goto_0

    .line 91
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->isValidSelection()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 92
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAdvancedSoundSceneShare:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isASoundSceneFileSelected(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/golf/GolfMgr;->isGolfFileSelected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 94
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v2, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showImageVideoConversionShareDialog(ZZ)V

    goto :goto_0

    .line 96
    :cond_3
    new-array v0, v2, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v1, v0, v3

    .line 99
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "LAST_SHARE_APP"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 105
    .end local v0    # "params":[Ljava/lang/Object;
    :sswitch_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showDeleteDialog(ZZ)V

    goto :goto_0

    .line 108
    :sswitch_3
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_COPY_TO_ALBUM:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 111
    :sswitch_4
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_SEND_TO_OTHER_DEVICES:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 114
    :sswitch_5
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->download()V

    goto :goto_0

    .line 117
    :sswitch_6
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->download()V

    goto :goto_0

    .line 120
    :sswitch_7
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v1, v0, v3

    const/4 v1, 0x0

    aput-object v1, v0, v2

    .line 121
    .restart local v0    # "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "S_STUDIO"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 124
    .end local v0    # "params":[Ljava/lang/Object;
    :sswitch_8
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showDetailsDialog(Z)V

    goto/16 :goto_0

    .line 84
    :sswitch_data_0
    .sparse-switch
        0x7f0f026c -> :sswitch_0
        0x7f0f026d -> :sswitch_1
        0x7f0f026e -> :sswitch_2
        0x7f0f0276 -> :sswitch_6
        0x7f0f0283 -> :sswitch_7
        0x7f0f029b -> :sswitch_8
        0x7f0f02a1 -> :sswitch_5
        0x7f0f02ad -> :sswitch_4
        0x7f0f02bc -> :sswitch_3
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 10
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v9, 0x7f0f0283

    const/4 v8, 0x2

    const v5, 0x7f0f02db

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 45
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mMenu:Landroid/view/Menu;

    .line 46
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 47
    .local v0, "count":I
    if-nez v0, :cond_1

    .line 48
    invoke-interface {p1, v5, v6}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 49
    invoke-interface {p1, v5, v6}, Landroid/view/Menu;->setGroupEnabled(IZ)V

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    invoke-interface {p1, v5, v7}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 53
    invoke-interface {p1, v5, v7}, Landroid/view/Menu;->setGroupEnabled(IZ)V

    .line 55
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->getSupportedOperationForSelectedItem()J

    move-result-wide v2

    .line 56
    .local v2, "supportOperation":J
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4, p1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkLastShareAppSupport(Landroid/content/Context;Landroid/view/Menu;J)J

    move-result-wide v2

    .line 57
    invoke-static {p1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->updateMenuOperation(Landroid/view/Menu;J)V

    .line 59
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v8, :cond_2

    .line 61
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v5, 0x7f0f026c

    invoke-static {v4, v5, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 63
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v5, 0x7f0f026e

    invoke-static {v4, v5, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 66
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/controller/SStudioCmd;->isSStudioAvailable(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 67
    :cond_3
    invoke-static {p1, v9, v6}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 72
    :cond_4
    :goto_1
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLastShare:Z

    if-nez v4, :cond_5

    .line 73
    const v4, 0x7f0f026d

    invoke-static {p1, v4, v6}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 75
    :cond_5
    if-le v0, v7, :cond_0

    .line 76
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 77
    .local v1, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v4, v1, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    if-eqz v4, :cond_0

    .line 78
    const v4, 0x7f0f029b

    invoke-static {p1, v4, v6}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_0

    .line 69
    .end local v1    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_6
    const/16 v4, 0x10

    if-ge v0, v4, :cond_4

    .line 70
    invoke-static {p1, v9, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_1
.end method

.method protected setTitle(I)V
    .locals 2
    .param p1, "numberOfItemsSelected"    # I

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit$1;-><init>(Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 152
    return-void
.end method
