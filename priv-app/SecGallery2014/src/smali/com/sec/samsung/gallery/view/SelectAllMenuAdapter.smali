.class public Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SelectAllMenuAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mItems:[Ljava/lang/String;

.field private mSelectAllClickListener:Landroid/view/View$OnClickListener;

.field private mSelectAllEnable:Z

.field private mSelectedItemCountTextClickListener:Landroid/view/View$OnClickListener;

.field private mTextWidth:I

.field private mUnselectAllClickListener:Landroid/view/View$OnClickListener;

.field private mUnselectAllEnable:Z

.field private menuAll:[Ljava/lang/String;

.field private menuSelectAll:[Ljava/lang/String;

.field private menuUnselectAll:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resource"    # I

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;-><init>(Landroid/content/Context;II)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resource"    # I
    .param p3, "textWidth"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 19
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mSelectAllEnable:Z

    .line 20
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mUnselectAllEnable:Z

    .line 29
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->menuAll:[Ljava/lang/String;

    .line 31
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->menuSelectAll:[Ljava/lang/String;

    .line 33
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->menuUnselectAll:[Ljava/lang/String;

    .line 48
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mContext:Landroid/content/Context;

    .line 49
    iput p3, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mTextWidth:I

    .line 50
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mItems:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mItems:[Ljava/lang/String;

    array-length v0, v0

    .line 116
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 102
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 103
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 104
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 105
    check-cast v1, Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/samsung/gallery/util/FontUtil;->getRobotoLightFont()Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 108
    :cond_0
    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Ljava/lang/String;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mItems:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mItems:[Ljava/lang/String;

    aget-object v0, v0, p1

    .line 97
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    .line 72
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 73
    .local v0, "mLayoutInflater":Landroid/view/LayoutInflater;
    const v2, 0x7f0300cc

    invoke-virtual {v0, v2, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 74
    .local v1, "v":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mItems:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setHoverPopupType(I)V

    .line 77
    iget v2, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mTextWidth:I

    if-eqz v2, :cond_0

    .line 78
    iget v2, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mTextWidth:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setWidth(I)V

    .line 79
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mItems:[Ljava/lang/String;

    aget-object v2, v2, p1

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mContext:Landroid/content/Context;

    const v4, 0x7f0e0048

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 80
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mSelectAllClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    :goto_0
    return-object v1

    .line 81
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mItems:[Ljava/lang/String;

    aget-object v2, v2, p1

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mContext:Landroid/content/Context;

    const v4, 0x7f0e0049

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 82
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mUnselectAllClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 84
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0050

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 85
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mSelectedItemCountTextClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mItems:[Ljava/lang/String;

    aget-object v0, v0, p1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f0e0048

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mSelectAllEnable:Z

    .line 60
    :goto_0
    return v0

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mItems:[Ljava/lang/String;

    aget-object v0, v0, p1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f0e0049

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mUnselectAllEnable:Z

    goto :goto_0

    .line 60
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/ArrayAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method public setItems([Ljava/lang/String;)V
    .locals 0
    .param p1, "items"    # [Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mItems:[Ljava/lang/String;

    .line 67
    return-void
.end method

.method public setMenuEnable(ZZ)V
    .locals 1
    .param p1, "select"    # Z
    .param p2, "unselect"    # Z

    .prologue
    .line 120
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->setMenuEnable(ZZZ)V

    .line 121
    return-void
.end method

.method public setMenuEnable(ZZZ)V
    .locals 4
    .param p1, "select"    # Z
    .param p2, "unselect"    # Z
    .param p3, "isRemoveSelectAll"    # Z

    .prologue
    .line 124
    const/4 v0, 0x0

    .line 126
    .local v0, "selectedItemCountText":Ljava/lang/String;
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionBar:Z

    if-nez v2, :cond_0

    .line 127
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    const v3, 0x7f0f0018

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 129
    .local v1, "selectionButton":Landroid/widget/Button;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 130
    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 134
    .end local v1    # "selectionButton":Landroid/widget/Button;
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->setMenuString(Ljava/lang/String;)V

    .line 135
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mSelectAllEnable:Z

    .line 136
    iput-boolean p2, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mUnselectAllEnable:Z

    .line 137
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mSelectAllEnable:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mUnselectAllEnable:Z

    if-nez v2, :cond_1

    .line 138
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->menuSelectAll:[Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->setItems([Ljava/lang/String;)V

    .line 148
    :goto_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->notifyDataSetChanged()V

    .line 149
    return-void

    .line 139
    :cond_1
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mUnselectAllEnable:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mSelectAllEnable:Z

    if-nez v2, :cond_2

    .line 140
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->menuUnselectAll:[Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->setItems([Ljava/lang/String;)V

    goto :goto_0

    .line 142
    :cond_2
    if-nez p3, :cond_3

    .line 143
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->menuAll:[Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->setItems([Ljava/lang/String;)V

    goto :goto_0

    .line 145
    :cond_3
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->setItems([Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setMenuString(Ljava/lang/String;)V
    .locals 7
    .param p1, "_selectedItemCountText"    # Ljava/lang/String;

    .prologue
    const v6, 0x7f0e0049

    const v5, 0x7f0e0048

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 164
    if-eqz p1, :cond_0

    .line 165
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    aput-object p1, v0, v2

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->menuAll:[Ljava/lang/String;

    .line 166
    new-array v0, v4, [Ljava/lang/String;

    aput-object p1, v0, v2

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->menuSelectAll:[Ljava/lang/String;

    .line 167
    new-array v0, v4, [Ljava/lang/String;

    aput-object p1, v0, v2

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->menuUnselectAll:[Ljava/lang/String;

    .line 174
    :goto_0
    return-void

    .line 170
    :cond_0
    new-array v0, v4, [Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->menuAll:[Ljava/lang/String;

    .line 171
    new-array v0, v3, [Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->menuSelectAll:[Ljava/lang/String;

    .line 172
    new-array v0, v3, [Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->menuUnselectAll:[Ljava/lang/String;

    goto :goto_0
.end method

.method public setSelectAllClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mSelectAllClickListener:Landroid/view/View$OnClickListener;

    .line 153
    return-void
.end method

.method public setSelectedItemCountTextClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "_listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mSelectedItemCountTextClickListener:Landroid/view/View$OnClickListener;

    .line 161
    return-void
.end method

.method public setTextWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 177
    iput p1, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mTextWidth:I

    .line 178
    return-void
.end method

.method public setUnselectAllClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->mUnselectAllClickListener:Landroid/view/View$OnClickListener;

    .line 157
    return-void
.end method
