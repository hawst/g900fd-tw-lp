.class public Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;
.super Ljava/lang/Object;
.source "PositionControllerBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "GroupControl"
.end annotation


# static fields
.field protected static final MAX_GROUP:I = 0x12

.field protected static final MAX_GROUP_TABLET:I = 0x1c


# instance fields
.field protected mActiveObject:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;",
            ">;"
        }
    .end annotation
.end field

.field protected mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

.field protected mCount:I

.field protected mGroupObject:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

.field protected mInactiveObject:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;",
            ">;"
        }
    .end annotation
.end field

.field protected mPitch:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 1
    .param p2, "compView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .prologue
    .line 1310
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    .line 1305
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mInactiveObject:Ljava/util/ArrayList;

    .line 1308
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mPitch:F

    .line 1311
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .line 1312
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x1c

    :goto_0
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mCount:I

    .line 1313
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->createGroupObject()[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mGroupObject:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 1314
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->clearActiveGroup()V

    .line 1315
    return-void

    .line 1312
    :cond_0
    const/16 v0, 0x12

    goto :goto_0
.end method

.method private createGroupObject()[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    .locals 6

    .prologue
    .line 1320
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    .line 1323
    .local v3, "viewConfig":Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mCount:I

    new-array v0, v4, [Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 1324
    .local v0, "glGroupSet":[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mCount:I

    if-ge v2, v4, :cond_0

    .line 1325
    new-instance v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-direct {v1, v4, v5, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;-><init>(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;)V

    .line 1326
    .local v1, "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mRootObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v4, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 1327
    aput-object v1, v0, v2

    .line 1324
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1329
    .end local v1    # "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    :cond_0
    return-object v0
.end method


# virtual methods
.method public activateGroup(I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    .locals 5
    .param p1, "index"    # I

    .prologue
    const/4 v4, 0x0

    .line 1350
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mInactiveObject:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1351
    # getter for: Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->access$1000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "there is no inactive group : active = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1352
    const/4 v0, 0x0

    .line 1369
    :cond_0
    :goto_0
    return-object v0

    .line 1354
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mInactiveObject:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 1355
    .local v0, "object":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mInactiveObject:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1356
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v2, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1357
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setVisibility(Z)V

    .line 1358
    # setter for: Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mIndex:I
    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->access$702(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;I)I

    .line 1359
    iget-object v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v2, :cond_2

    .line 1360
    iget-object v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    shl-int/lit8 v3, p1, 0x10

    iput v3, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 1361
    iget-object v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mPitch:F

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPitch(F)V

    .line 1363
    :cond_2
    iget-object v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mGrpCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v2, :cond_0

    .line 1364
    iget-object v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mGrpCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    shl-int/lit8 v3, p1, 0x10

    iput v3, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 1365
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v1, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mGroupCheckBoxVisible:Z

    .line 1366
    .local v1, "visible":Z
    iget-object v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mGrpCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v2, v1, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(ZI)V

    .line 1367
    iget-object v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mGrpCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mPitch:F

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPitch(F)V

    goto :goto_0
.end method

.method public clearActiveGroup()V
    .locals 3

    .prologue
    .line 1336
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->clear()V

    .line 1337
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mInactiveObject:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 1338
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mCount:I

    if-ge v1, v2, :cond_0

    .line 1339
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mGroupObject:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    aget-object v0, v2, v1

    .line 1340
    .local v0, "glObject":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    const/4 v2, -0x1

    # setter for: Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mIndex:I
    invoke-static {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->access$702(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;I)I

    .line 1341
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setVisibility(Z)V

    .line 1342
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mInactiveObject:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1338
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1344
    .end local v0    # "glObject":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    :cond_0
    return-void
.end method

.method public inActivateGroup(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;)V
    .locals 4
    .param p1, "object"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 1373
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 1374
    # getter for: Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->access$1000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Active group is already empty"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1391
    :cond_0
    :goto_0
    return-void

    .line 1377
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAlwaysActiveGroupObjectIndex:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->access$1100(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;)I

    move-result v0

    if-eq v0, v2, :cond_2

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getIndex()I

    move-result v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAlwaysActiveGroupObjectIndex:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->access$1100(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 1380
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mIndex:I
    invoke-static {p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->access$700(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V

    .line 1381
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mInactiveObject:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1382
    invoke-virtual {p1, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setVisibility(Z)V

    .line 1383
    invoke-virtual {p1, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setFocusBorderVisible(Z)V

    .line 1384
    # setter for: Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mIndex:I
    invoke-static {p1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->access$702(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;I)I

    .line 1385
    iget-object v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v0, :cond_3

    .line 1386
    iget-object v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iput v2, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 1388
    :cond_3
    iget-object v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mGrpCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v0, :cond_0

    .line 1389
    iget-object v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mGrpCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iput v2, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    goto :goto_0
.end method

.method public setDefaultPitch(F)V
    .locals 4
    .param p1, "pitch"    # F

    .prologue
    .line 1394
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mPitch:F

    cmpl-float v3, v3, p1

    if-nez v3, :cond_1

    .line 1409
    :cond_0
    return-void

    .line 1397
    :cond_1
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mPitch:F

    .line 1399
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v1

    .line 1400
    .local v1, "itemObjSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 1401
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 1402
    .local v2, "object":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    iget-object v3, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v3, :cond_2

    .line 1403
    iget-object v3, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v3, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPitch(F)V

    .line 1405
    :cond_2
    iget-object v3, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mGrpCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v3, :cond_3

    .line 1406
    iget-object v3, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mGrpCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v3, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPitch(F)V

    .line 1400
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
