.class public Lcom/sec/samsung/gallery/util/ContextualListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ContextualListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static individual_item_view:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private contextuallist:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/util/ContextualListClass;",
            ">;"
        }
    .end annotation
.end field

.field entry:Lcom/sec/samsung/gallery/util/ContextualListClass;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->individual_item_view:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/util/ContextualListClass;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p2, "contextuallist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/util/ContextualListClass;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->entry:Lcom/sec/samsung/gallery/util/ContextualListClass;

    .line 34
    iput-object p1, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->mContext:Landroid/content/Context;

    .line 35
    iput-object p2, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->contextuallist:Ljava/util/List;

    .line 36
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->contextuallist:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->contextuallist:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 59
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 64
    iget-object v7, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 65
    .local v4, "sp":Landroid/content/SharedPreferences;
    const/4 v6, 0x0

    .line 66
    .local v6, "tvw":Landroid/widget/TextView;
    iget-object v7, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->contextuallist:Ljava/util/List;

    invoke-interface {v7, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/samsung/gallery/util/ContextualListClass;

    iput-object v7, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->entry:Lcom/sec/samsung/gallery/util/ContextualListClass;

    .line 67
    iget-object v7, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->mContext:Landroid/content/Context;

    const-string v8, "layout_inflater"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 93
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v7, 0x7f03003b

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 96
    const v7, 0x7f0f00a0

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 97
    .local v5, "tv":Landroid/widget/TextView;
    iget-object v7, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->entry:Lcom/sec/samsung/gallery/util/ContextualListClass;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/util/ContextualListClass;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    const v7, 0x7f0f00a1

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 99
    .local v1, "chk_box":Landroid/widget/CheckBox;
    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 100
    iget-object v7, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->entry:Lcom/sec/samsung/gallery/util/ContextualListClass;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/util/ContextualListClass;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 101
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-nez v7, :cond_0

    .line 102
    iget-object v7, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f02047f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 104
    :cond_0
    const-string v7, "checkstatusenabled"

    const/4 v8, 0x1

    invoke-interface {v4, v7, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 105
    .local v2, "enabled":Z
    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 106
    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 107
    iget-object v7, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->entry:Lcom/sec/samsung/gallery/util/ContextualListClass;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/util/ContextualListClass;->getName()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->mContext:Landroid/content/Context;

    const v9, 0x7f0e0234

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 108
    const-string v7, "checkstatuslocation"

    const/4 v8, 0x1

    invoke-interface {v4, v7, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 109
    .local v0, "check_box":Z
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 124
    .end local v0    # "check_box":Z
    :cond_1
    :goto_0
    sget-object v7, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->individual_item_view:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    iget-object v7, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->entry:Lcom/sec/samsung/gallery/util/ContextualListClass;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/util/ContextualListClass;->getName()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->mContext:Landroid/content/Context;

    const v9, 0x7f0e0103

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DateTag:Z

    if-eqz v7, :cond_2

    .line 128
    const v7, 0x7f03003e

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 131
    .end local p2    # "convertView":Landroid/view/View;
    :cond_2
    return-object p2

    .line 110
    .restart local p2    # "convertView":Landroid/view/View;
    :cond_3
    iget-object v7, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->entry:Lcom/sec/samsung/gallery/util/ContextualListClass;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/util/ContextualListClass;->getName()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->mContext:Landroid/content/Context;

    const v9, 0x7f0e00a9

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 111
    const-string v7, "checkstatuscategory"

    const/4 v8, 0x1

    invoke-interface {v4, v7, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 112
    .restart local v0    # "check_box":Z
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 113
    .end local v0    # "check_box":Z
    :cond_4
    iget-object v7, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->entry:Lcom/sec/samsung/gallery/util/ContextualListClass;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/util/ContextualListClass;->getName()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->mContext:Landroid/content/Context;

    const v9, 0x7f0e0103

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 114
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DateTag:Z

    if-eqz v7, :cond_5

    .line 115
    const/4 v7, 0x4

    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0

    .line 117
    :cond_5
    const-string v7, "checkstatusdate"

    const/4 v8, 0x1

    invoke-interface {v4, v7, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 118
    .restart local v0    # "check_box":Z
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 120
    .end local v0    # "check_box":Z
    :cond_6
    iget-object v7, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->entry:Lcom/sec/samsung/gallery/util/ContextualListClass;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/util/ContextualListClass;->getName()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->mContext:Landroid/content/Context;

    const v9, 0x7f0e0235

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 121
    const-string v7, "checkstatususertags"

    const/4 v8, 0x1

    invoke-interface {v4, v7, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 122
    .restart local v0    # "check_box":Z
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0
.end method

.method public isEnabled(I)Z
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->contextuallist:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/util/ContextualListClass;

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->entry:Lcom/sec/samsung/gallery/util/ContextualListClass;

    .line 41
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->entry:Lcom/sec/samsung/gallery/util/ContextualListClass;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/ContextualListClass;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f0e0103

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_DateTag:Z

    if-eqz v0, :cond_0

    .line 42
    const/4 v0, 0x0

    .line 44
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/BaseAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/ContextualListAdapter;->notifyDataSetChanged()V

    .line 137
    return-void
.end method
