.class Lcom/sec/samsung/gallery/glview/GlPhotoView$1;
.super Ljava/lang/Object;
.source "GlPhotoView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;-><init>(Landroid/content/Context;IIFF)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 0

    .prologue
    .line 240
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGenericMotionCancel(Lcom/sec/android/gallery3d/glcore/GlObject;)Z
    .locals 3
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/4 v2, 0x1

    .line 252
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnGenericMotionListener:[Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;

    aget-object v0, v0, v2

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnGenericMotionListener:[Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;

    aget-object v0, v0, v2

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;->onGenericMotionCancel(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    .line 255
    :cond_0
    return v2
.end method

.method public onGenericMotionEnter(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 5
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/4 v4, 0x1

    .line 242
    move-object v0, p1

    check-cast v0, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 243
    .local v0, "baseObj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    iget v1, v0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 245
    .local v1, "index":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnGenericMotionListener:[Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;

    aget-object v2, v2, v4

    if-eqz v2, :cond_0

    .line 246
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnGenericMotionListener:[Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;

    aget-object v2, v2, v4

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-interface {v2, v3, v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;->onGenericMotionEnter(Lcom/sec/android/gallery3d/glcore/GlLayer;I)V

    .line 248
    :cond_0
    return v4
.end method
