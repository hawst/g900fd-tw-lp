.class public abstract Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;
.super Landroid/widget/LinearLayout;
.source "GallerySearchUserTagScrollView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$3;,
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;,
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$ExpandClickListener;,
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final TAG_DELIMETER:Ljava/lang/String; = "|"

.field private static final mComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation
.end field

.field private static predefinedTimeTags:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected DEFAULT_TAG_MAX_LINE_COUNT:I

.field protected FILTER_TAG_MAX_LINE_COUNT:I

.field public MAX_WIDTH_FOR_A_LINE:I

.field protected final SEARCH_HISTORY_ON:Z

.field protected eventTagListActive:Z

.field protected locationTagListActive:Z

.field protected mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

.field protected mEventTagLine:Landroid/widget/LinearLayout;

.field protected mEventTagList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation
.end field

.field protected mEventTagRootView:Landroid/view/ViewGroup;

.field protected mExpandStates:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/ViewGroup;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field protected mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

.field final mHandler:Landroid/os/Handler;

.field private mIsFinishInit:Z

.field protected mIsResultTagViewMode:Z

.field protected mItemSelectListener:Lcom/sec/samsung/gallery/view/gallerysearch/ITagCloudItemSelectListener;

.field private mLeftMargin:I

.field protected mLocationTagLine:Landroid/widget/LinearLayout;

.field protected mLocationTagList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation
.end field

.field protected mLocationTagRootView:Landroid/view/ViewGroup;

.field private mOperation:Ljava/lang/String;

.field protected mPersonTagLine:Landroid/widget/LinearLayout;

.field protected mPersonTagList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation
.end field

.field protected mPersonTagRootView:Landroid/view/ViewGroup;

.field private mRightMargin:I

.field protected mSearchResultTagList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation
.end field

.field protected mSelectedTagList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mSelectedTagTable:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation
.end field

.field protected mServiceCollectedTagKeyList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation
.end field

.field protected mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field protected mTempList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation
.end field

.field protected mTimeTagLine:Landroid/widget/LinearLayout;

.field protected mTimeTagList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation
.end field

.field protected mTimeTagRootView:Landroid/view/ViewGroup;

.field protected mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

.field protected mUserDefTagLine:Landroid/widget/LinearLayout;

.field protected mUserDefTagRootView:Landroid/view/ViewGroup;

.field protected mUserTagTagList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation
.end field

.field protected personTagListActive:Z

.field protected userTagListActive:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 49
    const-class v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->TAG:Ljava/lang/String;

    .line 109
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    .line 112
    sget-object v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;->BEFORE_YEAR:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;

    const v2, 0x7f0e0459

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;->LAST_YEAR:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;

    const v2, 0x7f0e045a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;->PAST_SIX_MONTH:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;

    const v2, 0x7f0e045b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;->PAST_MONTH:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;

    const v2, 0x7f0e045c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;->PAST_WEEK:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;

    const v2, 0x7f0e045d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1001
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$2;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$2;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mComparator:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 134
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 53
    iput v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->DEFAULT_TAG_MAX_LINE_COUNT:I

    .line 55
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->FILTER_TAG_MAX_LINE_COUNT:I

    .line 57
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mServiceCollectedTagKeyList:Ljava/util/ArrayList;

    .line 59
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSearchResultTagList:Ljava/util/ArrayList;

    .line 61
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagTable:Ljava/util/LinkedHashMap;

    .line 63
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mIsResultTagViewMode:Z

    .line 64
    iput v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLeftMargin:I

    iput v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mRightMargin:I

    .line 65
    const/16 v0, 0x578

    iput v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->MAX_WIDTH_FOR_A_LINE:I

    .line 67
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mItemSelectListener:Lcom/sec/samsung/gallery/view/gallerysearch/ITagCloudItemSelectListener;

    .line 69
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    .line 71
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagList:Ljava/util/List;

    .line 72
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagList:Ljava/util/List;

    .line 73
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagList:Ljava/util/List;

    .line 74
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserTagTagList:Ljava/util/List;

    .line 75
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTimeTagList:Ljava/util/List;

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTempList:Ljava/util/List;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagList:Ljava/util/ArrayList;

    .line 80
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->locationTagListActive:Z

    .line 81
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->personTagListActive:Z

    .line 82
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->eventTagListActive:Z

    .line 83
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->userTagListActive:Z

    .line 84
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mIsFinishInit:Z

    .line 85
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    .line 99
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mExpandStates:Ljava/util/HashMap;

    .line 101
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->SEARCH_HISTORY_ON:Z

    .line 104
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCMRelatedCategory:Z

    if-eqz v0, :cond_0

    const-string v0, " AND "

    :goto_0
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mOperation:Ljava/lang/String;

    .line 984
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$1;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mHandler:Landroid/os/Handler;

    .line 135
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->init(Landroid/content/Context;)V

    .line 136
    return-void

    .line 104
    :cond_0
    const-string v0, " OR "

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 129
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    iput v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->DEFAULT_TAG_MAX_LINE_COUNT:I

    .line 55
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->FILTER_TAG_MAX_LINE_COUNT:I

    .line 57
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mServiceCollectedTagKeyList:Ljava/util/ArrayList;

    .line 59
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSearchResultTagList:Ljava/util/ArrayList;

    .line 61
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagTable:Ljava/util/LinkedHashMap;

    .line 63
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mIsResultTagViewMode:Z

    .line 64
    iput v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLeftMargin:I

    iput v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mRightMargin:I

    .line 65
    const/16 v0, 0x578

    iput v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->MAX_WIDTH_FOR_A_LINE:I

    .line 67
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mItemSelectListener:Lcom/sec/samsung/gallery/view/gallerysearch/ITagCloudItemSelectListener;

    .line 69
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    .line 71
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagList:Ljava/util/List;

    .line 72
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagList:Ljava/util/List;

    .line 73
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagList:Ljava/util/List;

    .line 74
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserTagTagList:Ljava/util/List;

    .line 75
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTimeTagList:Ljava/util/List;

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTempList:Ljava/util/List;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagList:Ljava/util/ArrayList;

    .line 80
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->locationTagListActive:Z

    .line 81
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->personTagListActive:Z

    .line 82
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->eventTagListActive:Z

    .line 83
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->userTagListActive:Z

    .line 84
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mIsFinishInit:Z

    .line 85
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    .line 99
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mExpandStates:Ljava/util/HashMap;

    .line 101
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->SEARCH_HISTORY_ON:Z

    .line 104
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCMRelatedCategory:Z

    if-eqz v0, :cond_0

    const-string v0, " AND "

    :goto_0
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mOperation:Ljava/lang/String;

    .line 984
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$1;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mHandler:Landroid/os/Handler;

    .line 130
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->init(Landroid/content/Context;)V

    .line 131
    return-void

    .line 104
    :cond_0
    const-string v0, " OR "

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 124
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    iput v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->DEFAULT_TAG_MAX_LINE_COUNT:I

    .line 55
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->FILTER_TAG_MAX_LINE_COUNT:I

    .line 57
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mServiceCollectedTagKeyList:Ljava/util/ArrayList;

    .line 59
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSearchResultTagList:Ljava/util/ArrayList;

    .line 61
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagTable:Ljava/util/LinkedHashMap;

    .line 63
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mIsResultTagViewMode:Z

    .line 64
    iput v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLeftMargin:I

    iput v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mRightMargin:I

    .line 65
    const/16 v0, 0x578

    iput v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->MAX_WIDTH_FOR_A_LINE:I

    .line 67
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mItemSelectListener:Lcom/sec/samsung/gallery/view/gallerysearch/ITagCloudItemSelectListener;

    .line 69
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    .line 71
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagList:Ljava/util/List;

    .line 72
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagList:Ljava/util/List;

    .line 73
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagList:Ljava/util/List;

    .line 74
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserTagTagList:Ljava/util/List;

    .line 75
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTimeTagList:Ljava/util/List;

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTempList:Ljava/util/List;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagList:Ljava/util/ArrayList;

    .line 80
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->locationTagListActive:Z

    .line 81
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->personTagListActive:Z

    .line 82
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->eventTagListActive:Z

    .line 83
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->userTagListActive:Z

    .line 84
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mIsFinishInit:Z

    .line 85
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    .line 99
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mExpandStates:Ljava/util/HashMap;

    .line 101
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->SEARCH_HISTORY_ON:Z

    .line 104
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCMRelatedCategory:Z

    if-eqz v0, :cond_0

    const-string v0, " AND "

    :goto_0
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mOperation:Ljava/lang/String;

    .line 984
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$1;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mHandler:Landroid/os/Handler;

    .line 125
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->init(Landroid/content/Context;)V

    .line 126
    return-void

    .line 104
    :cond_0
    const-string v0, " OR "

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;Landroid/view/ViewGroup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;
    .param p1, "x1"    # Landroid/view/ViewGroup;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->updateExpansionIcon(Landroid/view/ViewGroup;)V

    return-void
.end method

.method private addTag(Ljava/lang/StringBuilder;I)V
    .locals 3
    .param p1, "result"    # Ljava/lang/StringBuilder;
    .param p2, "type"    # I

    .prologue
    .line 542
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getTagFilter()Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->getExistFilterMask()I

    move-result v0

    .line 543
    .local v0, "filterMask":I
    const/4 v1, 0x1

    shl-int/2addr v1, p2

    and-int/2addr v1, v0

    if-eqz v1, :cond_0

    .line 544
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 545
    const-string v1, "("

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 549
    :goto_0
    invoke-direct {p0, p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getFilterTag(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 550
    const-string v1, " ) "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 552
    :cond_0
    return-void

    .line 547
    :cond_1
    const-string v1, " AND ("

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private addTagList(Ljava/lang/String;I)V
    .locals 4
    .param p1, "catToAdd"    # Ljava/lang/String;
    .param p2, "catType"    # I

    .prologue
    const/4 v3, 0x1

    .line 797
    packed-switch p2, :pswitch_data_0

    .line 842
    :goto_0
    return-void

    .line 799
    :pswitch_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->locationTagListActive:Z

    if-nez v0, :cond_0

    .line 800
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 801
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->locationTagListActive:Z

    .line 803
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagList:Ljava/util/List;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, p1, v3, p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 809
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->personTagListActive:Z

    if-nez v0, :cond_1

    .line 810
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 811
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->personTagListActive:Z

    .line 813
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagList:Ljava/util/List;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, p1, v3, p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 819
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->eventTagListActive:Z

    if-nez v0, :cond_2

    .line 820
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 821
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->eventTagListActive:Z

    .line 823
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagList:Ljava/util/List;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, p1, v3, p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 830
    :pswitch_3
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->userTagListActive:Z

    if-nez v0, :cond_3

    .line 831
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserTagTagList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 832
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->userTagListActive:Z

    .line 834
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserTagTagList:Ljava/util/List;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, p1, v3, p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 797
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private clearSelectedTagDatas()V
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagTable:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 342
    return-void
.end method

.method private clearTagList(I)V
    .locals 1
    .param p1, "filterMask"    # I

    .prologue
    .line 573
    and-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_0

    .line 574
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 577
    :cond_0
    and-int/lit8 v0, p1, 0x4

    if-nez v0, :cond_1

    .line 578
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 581
    :cond_1
    and-int/lit8 v0, p1, 0x8

    if-nez v0, :cond_2

    .line 582
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 585
    :cond_2
    and-int/lit8 v0, p1, 0x10

    if-nez v0, :cond_3

    .line 586
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserTagTagList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 588
    :cond_3
    return-void
.end method

.method private findItemIndex(Ljava/util/List;Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)I
    .locals 5
    .param p2, "dstData"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ")I"
        }
    .end annotation

    .prologue
    .line 294
    .local p1, "srcList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    const/4 v1, -0x1

    .line 296
    .local v1, "index":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 297
    .local v2, "orgData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    invoke-virtual {p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    .line 299
    invoke-interface {p1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 304
    .end local v2    # "orgData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    :cond_1
    return v1
.end method

.method private getAllSelectedTags()Ljava/lang/String;
    .locals 5

    .prologue
    .line 555
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 556
    .local v1, "result":Ljava/lang/StringBuilder;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getTagFilter()Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 557
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getTagFilter()Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    move-result-object v4

    iget-object v2, v4, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    .line 558
    .local v2, "selectedFilter":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;>;"
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 559
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 560
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getTagFilter()Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->getExistFilterMask()I

    move-result v0

    .line 561
    .local v0, "filterMask":I
    and-int/lit8 v4, v0, 0x1

    if-eqz v4, :cond_1

    .line 562
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getTimeTag(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564
    :cond_1
    const/4 v3, 0x1

    .local v3, "type":I
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 565
    invoke-direct {p0, v1, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->addTag(Ljava/lang/StringBuilder;I)V

    .line 564
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 567
    :cond_2
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->clearTagList(I)V

    .line 569
    .end local v0    # "filterMask":I
    .end local v2    # "selectedFilter":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;>;"
    .end local v3    # "type":I
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private getCategoryType(I)Ljava/lang/String;
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 437
    const-string v0, ""

    .line 438
    .local v0, "result":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 451
    :goto_0
    return-object v0

    .line 440
    :pswitch_0
    const-string/jumbo v0, "user_tags"

    .line 441
    goto :goto_0

    .line 443
    :pswitch_1
    const-string v0, "scene_type"

    .line 444
    goto :goto_0

    .line 446
    :pswitch_2
    const-string/jumbo v0, "subscene_type"

    .line 447
    goto :goto_0

    .line 438
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getFilterTag(I)Ljava/lang/String;
    .locals 10
    .param p1, "type"    # I

    .prologue
    .line 494
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 496
    .local v2, "result":Ljava/lang/StringBuilder;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getTagFilter()Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    move-result-object v8

    iget-object v4, v8, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    .line 497
    .local v4, "selectedFilter":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;>;"
    if-nez v4, :cond_0

    .line 498
    const-string v8, ""

    .line 521
    :goto_0
    return-object v8

    .line 500
    :cond_0
    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;

    .line 501
    .local v0, "filter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    if-eqz v0, :cond_1

    iget-object v8, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    if-nez v8, :cond_2

    .line 502
    :cond_1
    const-string v8, ""

    goto :goto_0

    .line 504
    :cond_2
    iget-object v8, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 505
    .local v6, "size":I
    if-lez v6, :cond_6

    .line 506
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v6, :cond_6

    .line 507
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 508
    .local v5, "selection":Ljava/lang/StringBuilder;
    iget-object v8, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 509
    .local v7, "tag":Ljava/lang/String;
    if-eqz v7, :cond_3

    .line 510
    const-string v8, "\'"

    const-string v9, "\\\'"

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 511
    :cond_3
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagList:Ljava/util/ArrayList;

    if-eqz v8, :cond_4

    .line 512
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 513
    :cond_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p1, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getTagName(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " = \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 514
    .local v3, "select":Ljava/lang/String;
    if-nez v1, :cond_5

    .line 515
    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 506
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 517
    :cond_5
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mOperation:Ljava/lang/String;

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 521
    .end local v1    # "i":I
    .end local v3    # "select":Ljava/lang/String;
    .end local v5    # "selection":Ljava/lang/StringBuilder;
    .end local v7    # "tag":Ljava/lang/String;
    :cond_6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0
.end method

.method private getSelectedTagDatas()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 337
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagTable:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method private getTagName(II)Ljava/lang/String;
    .locals 3
    .param p1, "type"    # I
    .param p2, "extentionType"    # I

    .prologue
    .line 525
    packed-switch p1, :pswitch_data_0

    .line 537
    const-string v2, ""

    :goto_0
    return-object v2

    .line 527
    :pswitch_0
    const-string v2, "calendar_event"

    goto :goto_0

    .line 529
    :pswitch_1
    const-string v2, "named_location"

    goto :goto_0

    .line 531
    :pswitch_2
    const-string v2, "person_names"

    goto :goto_0

    .line 533
    :pswitch_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getTagFilter()Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    move-result-object v2

    iget-object v0, v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->mFilterList:Ljava/util/ArrayList;

    .line 534
    .local v0, "selectedFilter":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;>;"
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;

    .line 535
    .local v1, "userTagFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    iget-object v2, v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mCategoryType:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getCategoryType(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 525
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getTimeTag(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 455
    .local p1, "selectedFilter":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;>;"
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 457
    .local v4, "result":Ljava/lang/StringBuilder;
    if-nez p1, :cond_0

    .line 458
    const-string v11, ""

    .line 490
    :goto_0
    return-object v11

    .line 460
    :cond_0
    const/4 v11, 0x0

    invoke-virtual {p1, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;

    .line 461
    .local v10, "timeFilter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;
    if-eqz v10, :cond_1

    iget-object v11, v10, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    if-nez v11, :cond_2

    .line 462
    :cond_1
    const-string v11, ""

    goto :goto_0

    .line 464
    :cond_2
    iget-object v11, v10, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 465
    .local v6, "size":I
    if-lez v6, :cond_7

    .line 466
    const-string v11, "("

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 468
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v6, :cond_6

    .line 470
    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 471
    .local v5, "selection":Ljava/lang/StringBuilder;
    new-instance v7, Ljava/util/StringTokenizer;

    iget-object v11, v10, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter$Filter;->mTag:Ljava/util/ArrayList;

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    const-string v12, ","

    invoke-direct {v7, v11, v12}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    .local v7, "stringtokenizer":Ljava/util/StringTokenizer;
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 473
    .local v8, "stime":J
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 474
    .local v2, "etime":J
    const-wide/16 v12, 0x0

    cmp-long v11, v8, v12

    if-gez v11, :cond_3

    const-wide/16 v8, 0x0

    .line 475
    :cond_3
    const-wide/16 v12, 0x0

    cmp-long v11, v2, v12

    if-gtz v11, :cond_4

    .line 468
    .end local v2    # "etime":J
    .end local v5    # "selection":Ljava/lang/StringBuilder;
    .end local v7    # "stringtokenizer":Ljava/util/StringTokenizer;
    .end local v8    # "stime":J
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 476
    .restart local v2    # "etime":J
    .restart local v5    # "selection":Ljava/lang/StringBuilder;
    .restart local v7    # "stringtokenizer":Ljava/util/StringTokenizer;
    .restart local v8    # "stime":J
    :cond_4
    const-string v11, "( "

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "date"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " >= "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 477
    const-string v11, " AND "

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "date"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " <= "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " )"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 479
    if-nez v1, :cond_5

    .line 480
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 484
    .end local v2    # "etime":J
    .end local v5    # "selection":Ljava/lang/StringBuilder;
    .end local v7    # "stringtokenizer":Ljava/util/StringTokenizer;
    .end local v8    # "stime":J
    :catch_0
    move-exception v0

    .line 485
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_2

    .line 482
    .end local v0    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    .restart local v2    # "etime":J
    .restart local v5    # "selection":Ljava/lang/StringBuilder;
    .restart local v7    # "stringtokenizer":Ljava/util/StringTokenizer;
    .restart local v8    # "stime":J
    :cond_5
    :try_start_1
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mOperation:Ljava/lang/String;

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 488
    .end local v2    # "etime":J
    .end local v5    # "selection":Ljava/lang/StringBuilder;
    .end local v7    # "stringtokenizer":Ljava/util/StringTokenizer;
    .end local v8    # "stime":J
    :cond_6
    const-string v11, ")"

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 490
    .end local v1    # "i":I
    :cond_7
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_0
.end method

.method private init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 139
    move-object v2, p1

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 140
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->getAdapter()Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    .line 141
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagTable:Ljava/util/LinkedHashMap;

    .line 142
    check-cast p1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    .line 143
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 144
    .local v0, "res":Landroid/content/res/Resources;
    const v2, 0x7f0d028d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLeftMargin:I

    .line 145
    const v2, 0x7f0d028e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mRightMargin:I

    .line 146
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-nez v2, :cond_0

    .line 147
    const-class v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    .line 148
    .local v1, "threadName":Ljava/lang/String;
    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    invoke-direct {v2, p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    .line 149
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;->start()V

    .line 150
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;->notifyDirty()V

    .line 152
    .end local v1    # "threadName":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private initExpandListeners()V
    .locals 4

    .prologue
    const v3, 0x7f0f0245

    .line 891
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 892
    .local v0, "expandIcon":Landroid/widget/RelativeLayout;
    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$ExpandClickListener;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagRootView:Landroid/view/ViewGroup;

    invoke-direct {v1, p0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$ExpandClickListener;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;Landroid/view/ViewGroup;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 893
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserDefTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "expandIcon":Landroid/widget/RelativeLayout;
    check-cast v0, Landroid/widget/RelativeLayout;

    .line 894
    .restart local v0    # "expandIcon":Landroid/widget/RelativeLayout;
    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$ExpandClickListener;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserDefTagRootView:Landroid/view/ViewGroup;

    invoke-direct {v1, p0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$ExpandClickListener;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;Landroid/view/ViewGroup;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 895
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "expandIcon":Landroid/widget/RelativeLayout;
    check-cast v0, Landroid/widget/RelativeLayout;

    .line 896
    .restart local v0    # "expandIcon":Landroid/widget/RelativeLayout;
    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$ExpandClickListener;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagRootView:Landroid/view/ViewGroup;

    invoke-direct {v1, p0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$ExpandClickListener;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;Landroid/view/ViewGroup;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 897
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "expandIcon":Landroid/widget/RelativeLayout;
    check-cast v0, Landroid/widget/RelativeLayout;

    .line 898
    .restart local v0    # "expandIcon":Landroid/widget/RelativeLayout;
    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$ExpandClickListener;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagRootView:Landroid/view/ViewGroup;

    invoke-direct {v1, p0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$ExpandClickListener;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;Landroid/view/ViewGroup;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 899
    return-void
.end method

.method private initTagList()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 845
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagList:Ljava/util/List;

    .line 846
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->locationTagListActive:Z

    .line 847
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagList:Ljava/util/List;

    .line 848
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->personTagListActive:Z

    .line 849
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagList:Ljava/util/List;

    .line 850
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->eventTagListActive:Z

    .line 851
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserTagTagList:Ljava/util/List;

    .line 852
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->userTagListActive:Z

    .line 854
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->updatePredefinedTime()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTimeTagList:Ljava/util/List;

    .line 856
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGallerySearchUISeperatedMode:Z

    if-eqz v0, :cond_0

    .line 857
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagList:Ljava/util/List;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e0453

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 858
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagList:Ljava/util/List;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e0454

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 859
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagList:Ljava/util/List;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e0455

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 860
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserTagTagList:Ljava/util/List;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e0452

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 862
    :cond_0
    iput-boolean v7, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mIsFinishInit:Z

    .line 863
    return-void
.end method

.method private setInitTagData()V
    .locals 11

    .prologue
    .line 744
    const/4 v8, 0x0

    .line 745
    .local v8, "cursor":Landroid/database/Cursor;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->initTagList()V

    .line 747
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->DCM_CATEGORY_TABLE_URI:Landroid/net/Uri;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "category_name"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "category_type"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "media_count"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 753
    if-nez v8, :cond_1

    .line 783
    if-eqz v8, :cond_0

    .line 784
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 786
    :cond_0
    :goto_0
    return-void

    .line 754
    :cond_1
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->getColumnCount()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-gtz v0, :cond_2

    .line 783
    if-eqz v8, :cond_0

    .line 784
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 764
    :cond_2
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 765
    const/4 v6, 0x0

    .line 766
    .local v6, "catToAdd":Ljava/lang/String;
    const/4 v7, -0x1

    .line 767
    .local v7, "catType":I
    const/4 v10, 0x0

    .line 769
    .local v10, "mediaCount":I
    :cond_3
    const/4 v10, 0x0

    .line 770
    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 771
    if-nez v10, :cond_6

    .line 778
    :cond_4
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_3

    .line 783
    .end local v6    # "catToAdd":Ljava/lang/String;
    .end local v7    # "catType":I
    .end local v10    # "mediaCount":I
    :cond_5
    if-eqz v8, :cond_0

    .line 784
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 773
    .restart local v6    # "catToAdd":Ljava/lang/String;
    .restart local v7    # "catType":I
    .restart local v10    # "mediaCount":I
    :cond_6
    const/4 v0, 0x0

    :try_start_3
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 774
    if-eqz v6, :cond_4

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 775
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 777
    invoke-direct {p0, v6, v7}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->addTagList(Ljava/lang/String;I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 780
    .end local v6    # "catToAdd":Ljava/lang/String;
    .end local v7    # "catType":I
    .end local v10    # "mediaCount":I
    :catch_0
    move-exception v9

    .line 781
    .local v9, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 783
    if-eqz v8, :cond_0

    .line 784
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 783
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_7

    .line 784
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :cond_7
    throw v0
.end method

.method private setRelativeTagData()V
    .locals 12

    .prologue
    .line 592
    const/4 v9, 0x0

    .line 593
    .local v9, "cursor":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    if-nez v0, :cond_1

    .line 652
    :cond_0
    :goto_0
    return-void

    .line 594
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getTagFilter()Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 596
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getAllSelectedTags()Ljava/lang/String;

    move-result-object v3

    .line 597
    .local v3, "preSelectedTag":Ljava/lang/String;
    const-string v0, "Relative Tag"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DCM : To get Related Category : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->DCM_IMAGE_DOC_N_CATEGORY:Landroid/net/Uri;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "title"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "--------"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string v5, "category_name"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const-string v5, "category_type"

    aput-object v5, v2, v4

    const/4 v4, 0x4

    const-string v5, "media_count"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 609
    if-nez v9, :cond_2

    .line 649
    if-eqz v9, :cond_0

    .line 650
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 610
    :cond_2
    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->getColumnCount()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-gtz v0, :cond_3

    .line 649
    if-eqz v9, :cond_0

    .line 650
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 612
    :cond_3
    :try_start_2
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 613
    .local v6, "b":Landroid/os/Bundle;
    const-string/jumbo v0, "table_name"

    const-string v1, "category"

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    invoke-interface {v9, v6}, Landroid/database/Cursor;->respond(Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 615
    invoke-interface {v9}, Landroid/database/Cursor;->requery()Z

    .line 625
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_4

    .line 626
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->setSelectedTagList()V

    .line 629
    :cond_4
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 630
    const/4 v7, 0x0

    .line 631
    .local v7, "catToAdd":Ljava/lang/String;
    const/4 v8, -0x1

    .line 632
    .local v8, "catType":I
    const/4 v11, 0x0

    .line 633
    .local v11, "mediaCount":I
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->setSelectedTagList()V

    .line 636
    :cond_5
    const/4 v11, 0x0

    .line 637
    const/4 v0, 0x2

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 638
    if-nez v11, :cond_8

    .line 644
    :cond_6
    :goto_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_5

    .line 649
    .end local v7    # "catToAdd":Ljava/lang/String;
    .end local v8    # "catType":I
    .end local v11    # "mediaCount":I
    :cond_7
    if-eqz v9, :cond_0

    .line 650
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 639
    .restart local v7    # "catToAdd":Ljava/lang/String;
    .restart local v8    # "catType":I
    .restart local v11    # "mediaCount":I
    :cond_8
    const/4 v0, 0x0

    :try_start_3
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 640
    if-eqz v7, :cond_6

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 641
    const/4 v0, 0x1

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 643
    invoke-direct {p0, v7, v8}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->addTagList(Ljava/lang/String;I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 646
    .end local v6    # "b":Landroid/os/Bundle;
    .end local v7    # "catToAdd":Ljava/lang/String;
    .end local v8    # "catType":I
    .end local v11    # "mediaCount":I
    :catch_0
    move-exception v10

    .line 647
    .local v10, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 649
    if-eqz v9, :cond_0

    .line 650
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 649
    .end local v10    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v9, :cond_9

    .line 650
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :cond_9
    throw v0
.end method

.method private setSelectedTagList()V
    .locals 7

    .prologue
    .line 655
    const/4 v2, 0x0

    .line 656
    .local v2, "searchTagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    const/4 v4, 0x0

    .line 658
    .local v4, "textTagData":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserTagTagList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTempList:Ljava/util/List;

    if-eqz v5, :cond_4

    .line 659
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTempList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 660
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagList:Ljava/util/ArrayList;

    if-eqz v5, :cond_3

    .line 661
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserTagTagList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    .line 662
    .local v3, "tagSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_3

    .line 663
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserTagTagList:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "searchTagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    check-cast v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 664
    .restart local v2    # "searchTagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v4

    .line 665
    if-nez v4, :cond_1

    .line 662
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 667
    :cond_1
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 668
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagList:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 669
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTempList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 667
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 675
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v3    # "tagSize":I
    :cond_3
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserTagTagList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 676
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserTagTagList:Ljava/util/List;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTempList:Ljava/util/List;

    invoke-interface {v5, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 679
    :cond_4
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_9

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTempList:Ljava/util/List;

    if-eqz v5, :cond_9

    .line 680
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTempList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 681
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagList:Ljava/util/ArrayList;

    if-eqz v5, :cond_8

    .line 682
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    .line 683
    .restart local v3    # "tagSize":I
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_3
    if-ge v0, v3, :cond_8

    .line 684
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagList:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "searchTagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    check-cast v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 685
    .restart local v2    # "searchTagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v4

    .line 686
    if-nez v4, :cond_6

    .line 683
    :cond_5
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 688
    :cond_6
    const/4 v1, 0x0

    .restart local v1    # "j":I
    :goto_5
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_5

    .line 689
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagList:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 690
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTempList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 688
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 696
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v3    # "tagSize":I
    :cond_8
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 697
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagList:Ljava/util/List;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTempList:Ljava/util/List;

    invoke-interface {v5, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 700
    :cond_9
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_e

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTempList:Ljava/util/List;

    if-eqz v5, :cond_e

    .line 701
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTempList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 702
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagList:Ljava/util/ArrayList;

    if-eqz v5, :cond_d

    .line 703
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    .line 704
    .restart local v3    # "tagSize":I
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_6
    if-ge v0, v3, :cond_d

    .line 705
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagList:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "searchTagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    check-cast v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 706
    .restart local v2    # "searchTagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v4

    .line 707
    if-nez v4, :cond_b

    .line 704
    :cond_a
    :goto_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 709
    :cond_b
    const/4 v1, 0x0

    .restart local v1    # "j":I
    :goto_8
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_a

    .line 710
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagList:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 711
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTempList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 709
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 717
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v3    # "tagSize":I
    :cond_d
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 718
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagList:Ljava/util/List;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTempList:Ljava/util/List;

    invoke-interface {v5, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 721
    :cond_e
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_13

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTempList:Ljava/util/List;

    if-eqz v5, :cond_13

    .line 722
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTempList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 723
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagList:Ljava/util/ArrayList;

    if-eqz v5, :cond_12

    .line 724
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    .line 725
    .restart local v3    # "tagSize":I
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_9
    if-ge v0, v3, :cond_12

    .line 726
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagList:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "searchTagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    check-cast v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 727
    .restart local v2    # "searchTagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v4

    .line 728
    if-nez v4, :cond_10

    .line 725
    :cond_f
    :goto_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 730
    :cond_10
    const/4 v1, 0x0

    .restart local v1    # "j":I
    :goto_b
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_f

    .line 731
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagList:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 732
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTempList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 730
    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    .line 738
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v3    # "tagSize":I
    :cond_12
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 739
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagList:Ljava/util/List;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTempList:Ljava/util/List;

    invoke-interface {v5, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 741
    :cond_13
    return-void
.end method

.method private updateExpansionIcon(Landroid/view/ViewGroup;)V
    .locals 2
    .param p1, "group"    # Landroid/view/ViewGroup;

    .prologue
    .line 869
    const v1, 0x7f0f0126

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 870
    .local v0, "expandIcon":Landroid/widget/ImageView;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mExpandStates:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0200bf

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 872
    return-void

    .line 870
    :cond_0
    const v1, 0x7f0200c0

    goto :goto_0
.end method


# virtual methods
.method protected addSelectedTagData(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)V
    .locals 3
    .param p1, "tagData"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagTable:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 320
    sget-object v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "add : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->setSelected(Z)V

    .line 323
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagTable:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    :cond_0
    return-void
.end method

.method protected applyFilterTagSelection(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 249
    .local p1, "allTagDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getSelectedTagDatas()Ljava/util/Collection;

    move-result-object v5

    .line 251
    .local v5, "selectedTagDatas":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_2

    .line 252
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 253
    .local v6, "tagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->setSelected(Z)V

    goto :goto_0

    .line 256
    .end local v6    # "tagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    :cond_0
    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    .line 257
    const/4 v3, 0x0

    .line 259
    .local v3, "refinedPosition":I
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 260
    .local v4, "selectedData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    const/4 v2, -0x1

    .line 262
    .local v2, "index":I
    invoke-direct {p0, p1, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->findItemIndex(Ljava/util/List;Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)I

    move-result v2

    if-ltz v2, :cond_1

    .line 263
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 265
    .local v0, "data":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->setSelected(Z)V

    .line 266
    invoke-virtual {p1, v3, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 268
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 273
    .end local v0    # "data":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "index":I
    .end local v3    # "refinedPosition":I
    .end local v4    # "selectedData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    :cond_2
    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    .line 274
    new-instance p1, Ljava/util/ArrayList;

    .end local p1    # "allTagDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 275
    .restart local p1    # "allTagDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 279
    :cond_3
    return-object p1
.end method

.method public cleanUp()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 212
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mIsFinishInit:Z

    .line 213
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->setTagDisplayMode(Z)V

    .line 214
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;->terminate()V

    .line 216
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    .line 218
    :cond_0
    return-void
.end method

.method protected clearQueryResultTags()V
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSearchResultTagList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSearchResultTagList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 373
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSearchResultTagList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 375
    :cond_0
    return-void
.end method

.method public getAllTagDatas()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 379
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mIsResultTagViewMode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSearchResultTagList:Ljava/util/ArrayList;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mServiceCollectedTagKeyList:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method protected getCategorizedfiteredTagData()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 286
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getAllTagDatas()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->applyFilterTagSelection(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 287
    .local v0, "refinedList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 288
    .local v1, "seperatedFilteredTag":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;>;"
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->seperateUserDefTag(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 289
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->seperateLocationTag(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 290
    return-object v1
.end method

.method protected getCountSelectedTagDatas()I
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagTable:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    return v0
.end method

.method protected getEventTagData()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1154
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagList:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method protected getFilteredTagDatas()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 283
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getAllTagDatas()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->applyFilterTagSelection(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected getLocationTagData()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1138
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagList:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method protected getPersonTagData()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1142
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagList:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getSelectedCount()I
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagTable:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    return v0
.end method

.method public getSelectedDataToString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 308
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getSelectedTagDatas()Ljava/util/Collection;

    move-result-object v1

    .line 309
    .local v1, "selectedTagDatas":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 311
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method protected getTimeTagData()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1150
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTimeTagList:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method protected getUserTagData()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1146
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserTagTagList:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public isTagFromSearchResult()Z
    .locals 1

    .prologue
    .line 221
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mIsResultTagViewMode:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected makeTagList()V
    .locals 14

    .prologue
    .line 902
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 904
    .local v2, "inflater":Landroid/view/LayoutInflater;
    sget-boolean v11, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGallerySearchUISeperatedMode:Z

    if-eqz v11, :cond_0

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagRootView:Landroid/view/ViewGroup;

    if-eqz v11, :cond_0

    .line 982
    :goto_0
    return-void

    .line 907
    :cond_0
    const v11, 0x7f0300e3

    const/4 v12, 0x0

    invoke-virtual {v2, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    iput-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagRootView:Landroid/view/ViewGroup;

    .line 908
    const v11, 0x7f0300e3

    const/4 v12, 0x0

    invoke-virtual {v2, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    iput-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserDefTagRootView:Landroid/view/ViewGroup;

    .line 909
    const v11, 0x7f0300e3

    const/4 v12, 0x0

    invoke-virtual {v2, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    iput-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagRootView:Landroid/view/ViewGroup;

    .line 910
    const v11, 0x7f0300e3

    const/4 v12, 0x0

    invoke-virtual {v2, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    iput-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagRootView:Landroid/view/ViewGroup;

    .line 911
    const v11, 0x7f0300e3

    const/4 v12, 0x0

    invoke-virtual {v2, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    iput-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTimeTagRootView:Landroid/view/ViewGroup;

    .line 914
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    iput-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mExpandStates:Ljava/util/HashMap;

    .line 915
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mExpandStates:Ljava/util/HashMap;

    iget-object v12, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagRootView:Landroid/view/ViewGroup;

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 916
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mExpandStates:Ljava/util/HashMap;

    iget-object v12, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserDefTagRootView:Landroid/view/ViewGroup;

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 917
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mExpandStates:Ljava/util/HashMap;

    iget-object v12, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagRootView:Landroid/view/ViewGroup;

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 918
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mExpandStates:Ljava/util/HashMap;

    iget-object v12, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagRootView:Landroid/view/ViewGroup;

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 919
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mExpandStates:Ljava/util/HashMap;

    iget-object v12, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTimeTagRootView:Landroid/view/ViewGroup;

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 921
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagRootView:Landroid/view/ViewGroup;

    const v12, 0x7f0f0124

    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 922
    .local v3, "locationTagListIcon":Landroid/widget/ImageView;
    const v11, 0x7f020464

    invoke-virtual {v3, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 923
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0e0074

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v11}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 924
    const/4 v11, 0x1

    invoke-virtual {v3, v11}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 926
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserDefTagRootView:Landroid/view/ViewGroup;

    const v12, 0x7f0f0124

    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 927
    .local v9, "userDefTagListIcon":Landroid/widget/ImageView;
    const v11, 0x7f02046a

    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 928
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0e00a8

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 929
    const/4 v11, 0x1

    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 931
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagRootView:Landroid/view/ViewGroup;

    const v12, 0x7f0f0124

    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 932
    .local v0, "eventTagListIcon":Landroid/widget/ImageView;
    const v11, 0x7f020461

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 933
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0e0441

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 934
    const/4 v11, 0x1

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 936
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagRootView:Landroid/view/ViewGroup;

    const v12, 0x7f0f0124

    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 937
    .local v5, "personTagListIcon":Landroid/widget/ImageView;
    const v11, 0x7f020467

    invoke-virtual {v5, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 938
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0e00a7

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 939
    const/4 v11, 0x1

    invoke-virtual {v5, v11}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 941
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTimeTagRootView:Landroid/view/ViewGroup;

    const v12, 0x7f0f0124

    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 942
    .local v7, "timeTagListIcon":Landroid/widget/ImageView;
    const v11, 0x7f02046d

    invoke-virtual {v7, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 943
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0e0073

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 944
    const/4 v11, 0x1

    invoke-virtual {v7, v11}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 946
    sget-boolean v11, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGallerySearchUISeperatedMode:Z

    if-eqz v11, :cond_1

    .line 948
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagRootView:Landroid/view/ViewGroup;

    const v12, 0x7f0f0226

    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 949
    .local v4, "locationTitle":Landroid/widget/TextView;
    const v11, 0x7f0e044f

    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setText(I)V

    .line 950
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserDefTagRootView:Landroid/view/ViewGroup;

    const v12, 0x7f0f0226

    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 951
    .local v10, "userDefTitle":Landroid/widget/TextView;
    const v11, 0x7f0e044e

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(I)V

    .line 952
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagRootView:Landroid/view/ViewGroup;

    const v12, 0x7f0f0226

    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 953
    .local v1, "eventTitle":Landroid/widget/TextView;
    const v11, 0x7f0e0451

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setText(I)V

    .line 954
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagRootView:Landroid/view/ViewGroup;

    const v12, 0x7f0f0226

    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 955
    .local v6, "personTitle":Landroid/widget/TextView;
    const v11, 0x7f0e0450

    invoke-virtual {v6, v11}, Landroid/widget/TextView;->setText(I)V

    .line 956
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTimeTagRootView:Landroid/view/ViewGroup;

    const v12, 0x7f0f0226

    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 957
    .local v8, "timeTitle":Landroid/widget/TextView;
    const v11, 0x7f0e044c

    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setText(I)V

    .line 959
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagRootView:Landroid/view/ViewGroup;

    invoke-direct {p0, v11}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->updateExpansionIcon(Landroid/view/ViewGroup;)V

    .line 960
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserDefTagRootView:Landroid/view/ViewGroup;

    invoke-direct {p0, v11}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->updateExpansionIcon(Landroid/view/ViewGroup;)V

    .line 961
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagRootView:Landroid/view/ViewGroup;

    invoke-direct {p0, v11}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->updateExpansionIcon(Landroid/view/ViewGroup;)V

    .line 962
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagRootView:Landroid/view/ViewGroup;

    invoke-direct {p0, v11}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->updateExpansionIcon(Landroid/view/ViewGroup;)V

    .line 963
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTimeTagRootView:Landroid/view/ViewGroup;

    invoke-direct {p0, v11}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->updateExpansionIcon(Landroid/view/ViewGroup;)V

    .line 965
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->initExpandListeners()V

    .line 968
    .end local v1    # "eventTitle":Landroid/widget/TextView;
    .end local v4    # "locationTitle":Landroid/widget/TextView;
    .end local v6    # "personTitle":Landroid/widget/TextView;
    .end local v8    # "timeTitle":Landroid/widget/TextView;
    .end local v10    # "userDefTitle":Landroid/widget/TextView;
    :cond_1
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagRootView:Landroid/view/ViewGroup;

    const v12, 0x7f0f0246

    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/LinearLayout;

    iput-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagLine:Landroid/widget/LinearLayout;

    .line 969
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagRootView:Landroid/view/ViewGroup;

    const v12, 0x7f0f0246

    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/LinearLayout;

    iput-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagLine:Landroid/widget/LinearLayout;

    .line 970
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagRootView:Landroid/view/ViewGroup;

    const v12, 0x7f0f0246

    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/LinearLayout;

    iput-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagLine:Landroid/widget/LinearLayout;

    .line 971
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserDefTagRootView:Landroid/view/ViewGroup;

    const v12, 0x7f0f0246

    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/LinearLayout;

    iput-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserDefTagLine:Landroid/widget/LinearLayout;

    .line 972
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTimeTagRootView:Landroid/view/ViewGroup;

    const v12, 0x7f0f0246

    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/LinearLayout;

    iput-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTimeTagLine:Landroid/widget/LinearLayout;

    .line 974
    sget-boolean v11, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGallerySearchUISeperatedMode:Z

    if-nez v11, :cond_2

    .line 975
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->refreshTagList()V

    .line 976
    :cond_2
    const/4 v11, 0x1

    invoke-virtual {p0, v11}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->setOrientation(I)V

    .line 977
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagLine:Landroid/widget/LinearLayout;

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 978
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagLine:Landroid/widget/LinearLayout;

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 979
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagLine:Landroid/widget/LinearLayout;

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 980
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserDefTagLine:Landroid/widget/LinearLayout;

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 981
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mTimeTagLine:Landroid/widget/LinearLayout;

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->setOrientation(I)V

    goto/16 :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 156
    move-object v0, p1

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;

    .line 157
    .local v0, "button":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->getTagData()Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    move-result-object v2

    .line 158
    .local v2, "tagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v1

    .line 159
    .local v1, "state":Lcom/sec/android/gallery3d/app/ActivityState;
    const/4 v3, 0x0

    .line 160
    .local v3, "tagDataString":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTagFilterType()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Time"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 161
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getRawTagData()Ljava/lang/String;

    move-result-object v3

    .line 166
    :goto_0
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->isSelected()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 167
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->removeSelectedTagData(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)V

    .line 168
    invoke-virtual {v0, v8}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setSelected(Z)V

    .line 169
    invoke-virtual {v2, v8}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->setSelected(Z)V

    .line 170
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getCountSelectedTagDatas()I

    move-result v5

    if-nez v5, :cond_0

    .line 171
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->clearQueryResultTags()V

    .line 174
    :cond_0
    instance-of v5, v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    if-eqz v5, :cond_1

    .line 175
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    invoke-virtual {v5, v8}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->setImeVisibility(Z)V

    .line 177
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTagFilterType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v3}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->removeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCMRelatedCategory:Z

    if-eqz v5, :cond_3

    .line 179
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    if-nez v5, :cond_2

    .line 180
    const-class v5, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    .line 181
    .local v4, "threadName":Ljava/lang/String;
    new-instance v5, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    invoke-direct {v5, p0, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;Ljava/lang/String;)V

    iput-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    .line 182
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;->start()V

    .line 184
    .end local v4    # "threadName":Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;->notifyDirty()V

    .line 186
    :cond_3
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mItemSelectListener:Lcom/sec/samsung/gallery/view/gallerysearch/ITagCloudItemSelectListener;

    if-eqz v5, :cond_4

    .line 187
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mItemSelectListener:Lcom/sec/samsung/gallery/view/gallerysearch/ITagCloudItemSelectListener;

    invoke-interface {v5, v2, v8}, Lcom/sec/samsung/gallery/view/gallerysearch/ITagCloudItemSelectListener;->OnTagCloudItemSelected(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;Z)V

    .line 209
    :cond_4
    :goto_1
    return-void

    .line 163
    :cond_5
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 190
    :cond_6
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->addSelectedTagData(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)V

    .line 191
    invoke-virtual {v0, v9}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setSelected(Z)V

    .line 192
    invoke-virtual {v2, v9}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->setSelected(Z)V

    .line 193
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTagFilterType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getCategoryType()I

    move-result v7

    invoke-virtual {v5, v6, v3, v7}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->addTag(Ljava/lang/String;Ljava/lang/String;I)V

    .line 194
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCMRelatedCategory:Z

    if-eqz v5, :cond_8

    .line 195
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    if-nez v5, :cond_7

    .line 196
    const-class v5, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    .line 197
    .restart local v4    # "threadName":Ljava/lang/String;
    new-instance v5, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    invoke-direct {v5, p0, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;Ljava/lang/String;)V

    iput-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    .line 198
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;->start()V

    .line 200
    .end local v4    # "threadName":Ljava/lang/String;
    :cond_7
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;->notifyDirty()V

    .line 202
    :cond_8
    instance-of v5, v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    if-eqz v5, :cond_4

    .line 203
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    invoke-virtual {v5, v8}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->setImeVisibility(Z)V

    .line 204
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mItemSelectListener:Lcom/sec/samsung/gallery/view/gallerysearch/ITagCloudItemSelectListener;

    if-eqz v5, :cond_4

    .line 205
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mItemSelectListener:Lcom/sec/samsung/gallery/view/gallerysearch/ITagCloudItemSelectListener;

    invoke-interface {v5, v2, v9}, Lcom/sec/samsung/gallery/view/gallerysearch/ITagCloudItemSelectListener;->OnTagCloudItemSelected(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;Z)V

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 226
    sub-int v1, p4, p2

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLeftMargin:I

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mRightMargin:I

    sub-int v0, v1, v2

    .line 227
    .local v0, "currentMaxWidth":I
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGallerySearchUISeperatedMode:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->MAX_WIDTH_FOR_A_LINE:I

    if-eq v1, v0, :cond_0

    .line 228
    iput v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->MAX_WIDTH_FOR_A_LINE:I

    .line 229
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->refreshTagList()V

    .line 233
    :goto_0
    return-void

    .line 231
    :cond_0
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    goto :goto_0
.end method

.method protected abstract refreshTagList()V
.end method

.method protected removeSelectedTagData(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)V
    .locals 3
    .param p1, "tagData"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .prologue
    .line 328
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagTable:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 329
    sget-object v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "remove : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->setSelected(Z)V

    .line 332
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSelectedTagTable:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    :cond_0
    return-void
.end method

.method public requestDataUpdate()V
    .locals 0

    .prologue
    .line 349
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->refreshTagList()V

    .line 350
    return-void
.end method

.method public resetTagList()V
    .locals 2

    .prologue
    .line 1128
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    if-nez v1, :cond_0

    .line 1129
    const-class v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 1130
    .local v0, "threadName":Ljava/lang/String;
    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    invoke-direct {v1, p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    .line 1131
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;->start()V

    .line 1133
    .end local v0    # "threadName":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;->notifyDirty()V

    .line 1135
    return-void
.end method

.method protected seperateEventTag(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 391
    .local p1, "refinedList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->seperateTagByFlag(Ljava/util/List;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected seperateLocationTag(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 387
    .local p1, "refinedList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->seperateTagByFlag(Ljava/util/List;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected seperatePersonTag(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 395
    .local p1, "refinedList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->seperateTagByFlag(Ljava/util/List;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected seperateTagByFlag(Ljava/util/List;I)Ljava/util/List;
    .locals 4
    .param p2, "tagTypeFlag"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;I)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 399
    .local p1, "refinedList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 400
    .local v1, "seperatedTagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    if-eqz p1, :cond_1

    .line 401
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 402
    .local v2, "tagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTagTypeFlags()I

    move-result v3

    and-int/2addr v3, p2

    if-eqz v3, :cond_0

    .line 403
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 406
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "tagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    :cond_1
    return-object v1
.end method

.method protected seperateUserDefTag(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 383
    .local p1, "refinedList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->seperateTagByFlag(Ljava/util/List;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public setItemSelectListener(Lcom/sec/samsung/gallery/view/gallerysearch/ITagCloudItemSelectListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/samsung/gallery/view/gallerysearch/ITagCloudItemSelectListener;

    .prologue
    .line 236
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mItemSelectListener:Lcom/sec/samsung/gallery/view/gallerysearch/ITagCloudItemSelectListener;

    .line 237
    return-void
.end method

.method public setLocationTagMaxLineCount(I)V
    .locals 0
    .param p1, "maxCount"    # I

    .prologue
    .line 246
    iput p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->DEFAULT_TAG_MAX_LINE_COUNT:I

    .line 247
    return-void
.end method

.method public abstract setLoctionTagVisibleMode(II)V
.end method

.method public setQueryResultTags(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 364
    .local p1, "tags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mSearchResultTagList:Ljava/util/ArrayList;

    .line 366
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->isTagFromSearchResult()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 367
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->refreshTagList()V

    .line 369
    :cond_0
    return-void
.end method

.method protected setTagData()V
    .locals 1

    .prologue
    .line 410
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCMRelatedCategory:Z

    if-nez v0, :cond_1

    .line 411
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->setInitTagData()V

    .line 434
    :cond_0
    :goto_0
    return-void

    .line 413
    :cond_1
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mIsFinishInit:Z

    if-nez v0, :cond_2

    .line 414
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->setInitTagData()V

    goto :goto_0

    .line 416
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    if-eqz v0, :cond_0

    .line 417
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getTagFilter()Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getTagFilter()Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagFilter;->getExistFilterMask()I

    move-result v0

    if-nez v0, :cond_3

    .line 418
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->setInitTagData()V

    goto :goto_0

    .line 425
    :cond_3
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->setRelativeTagData()V

    goto :goto_0
.end method

.method public setTagDisplayMode(Z)V
    .locals 1
    .param p1, "isResultView"    # Z

    .prologue
    .line 353
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mIsResultTagViewMode:Z

    .line 355
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mIsResultTagViewMode:Z

    if-nez v0, :cond_0

    .line 356
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->clearQueryResultTags()V

    .line 357
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->clearSelectedTagDatas()V

    .line 360
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->refreshTagList()V

    .line 361
    return-void
.end method

.method public abstract setUserDefTagVisibleMode(II)V
.end method

.method public setUserTagMaxLineCount(I)V
    .locals 0
    .param p1, "maxCount"    # I

    .prologue
    .line 242
    iput p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->FILTER_TAG_MAX_LINE_COUNT:I

    .line 243
    return-void
.end method

.method public updatePredefinedTime()Ljava/util/ArrayList;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1068
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1069
    .local v9, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    new-instance v6, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;

    invoke-direct {v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;-><init>()V

    .line 1070
    .local v6, "converter":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;
    const-string v2, ""

    .local v2, "name":Ljava/lang/String;
    const-string v10, ""

    .local v10, "start_time":Ljava/lang/String;
    const-string v7, ""

    .line 1071
    .local v7, "end_time":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1073
    .local v5, "textTTS":Ljava/lang/String;
    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 1075
    .local v8, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;>;"
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1076
    sget-object v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$3;->$SwitchMap$com$sec$samsung$gallery$view$gallerysearch$GallerySearchUserTagScrollView$PREDEFINED_TIME:[I

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    .line 1117
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1118
    .local v4, "timeRawData":Ljava/lang/String;
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1119
    .local v0, "tagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1120
    const/4 v5, 0x0

    .line 1121
    goto :goto_0

    .line 1079
    .end local v0    # "tagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    .end local v4    # "timeRawData":Ljava/lang/String;
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    sget-object v11, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;->PAST_WEEK:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;

    invoke-virtual {v1, v11}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1081
    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->startOf7DaysAgo()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    .line 1082
    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->endOfToday()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    .line 1083
    goto :goto_1

    .line 1086
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    sget-object v11, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;->PAST_MONTH:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;

    invoke-virtual {v1, v11}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1088
    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->startOf30DaysAgo()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    .line 1089
    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->endOfToday()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    .line 1090
    goto :goto_1

    .line 1093
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    sget-object v11, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;->PAST_SIX_MONTH:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;

    invoke-virtual {v1, v11}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const/4 v13, 0x6

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v3, v1, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1095
    const/4 v1, 0x6

    invoke-virtual {v6, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->startOfPastMonths(I)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    .line 1096
    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->endOfToday()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    .line 1097
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0e02b0

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const/4 v13, 0x6

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v1, v3, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1098
    goto/16 :goto_1

    .line 1101
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    sget-object v11, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;->LAST_YEAR:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;

    invoke-virtual {v1, v11}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1103
    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->startOfPastYear()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    .line 1104
    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->endOfPastYear()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    .line 1105
    goto/16 :goto_1

    .line 1108
    :pswitch_4
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->predefinedTimeTags:Ljava/util/LinkedHashMap;

    sget-object v11, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;->BEFORE_YEAR:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$PREDEFINED_TIME;

    invoke-virtual {v1, v11}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->getLastYear()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v3, v1, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1109
    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->startOfBeforeYear()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    .line 1110
    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTimeUtils;->endOfBeforeYear()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    .line 1111
    goto/16 :goto_1

    .line 1123
    :cond_0
    return-object v9

    .line 1076
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected updateTagList()V
    .locals 3

    .prologue
    .line 1012
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->setTagData()V

    .line 1013
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1014
    .local v0, "tagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagList:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1015
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagList:Ljava/util/List;

    sget-object v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mComparator:Ljava/util/Comparator;

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1016
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1018
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagList:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1019
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagList:Ljava/util/List;

    sget-object v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mComparator:Ljava/util/Comparator;

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1020
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1022
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagList:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1023
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagList:Ljava/util/List;

    sget-object v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mComparator:Ljava/util/Comparator;

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1024
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1026
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserTagTagList:Ljava/util/List;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserTagTagList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1027
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserTagTagList:Ljava/util/List;

    sget-object v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mComparator:Ljava/util/Comparator;

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1028
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserTagTagList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1030
    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1031
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->setTagList(Ljava/util/ArrayList;)V

    .line 1033
    :cond_4
    return-void
.end method
