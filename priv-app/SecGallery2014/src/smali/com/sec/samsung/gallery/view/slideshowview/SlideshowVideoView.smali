.class public Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;
.super Ljava/lang/Object;
.source "SlideshowVideoView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;
    }
.end annotation


# static fields
.field private static final DESTROY_VIDEO_SURFACEVIEW:I = 0x1

.field private static final FINISH_SLIDESHOW:I = 0x3

.field private static final GLROOTVIEW_INVISBLE:I = 0x0

.field private static final PAUSE_MEDIA_PLAYER:I = 0x2

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private audioFocusGranted:Z

.field private final mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mContext:Landroid/content/Context;

.field private mCustomSurfaceView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;

.field mHandler:Landroid/os/Handler;

.field private mHolder:Landroid/view/SurfaceHolder;

.field private mIsOffHookOccured:Z

.field private mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mMultiWindow:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

.field private mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private final mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private final mOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

.field mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private final mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

.field private mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mVideoHeight:I

.field private mVideoWidth:I

.field private mWindow:Landroid/view/Window;

.field public misVideoPlaying:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 280
    const-class v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/samsung/gallery/glview/GlSlideShowView;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 53
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->misVideoPlaying:Z

    .line 54
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->audioFocusGranted:Z

    .line 55
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mIsOffHookOccured:Z

    .line 64
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$1;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mHandler:Landroid/os/Handler;

    .line 137
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$2;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 156
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 194
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$4;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    .line 207
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$5;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$5;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 281
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$7;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$7;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 313
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$8;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$8;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 59
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mContext:Landroid/content/Context;

    .line 60
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    .line 61
    invoke-virtual {p2}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getInstance()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .line 62
    return-void
.end method

.method private abandonAudioFocus()V
    .locals 3

    .prologue
    .line 99
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->audioFocusGranted:Z

    .line 100
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 101
    .local v0, "am":Landroid/media/AudioManager;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 102
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mCustomSurfaceView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;

    return-object v0
.end method

.method static synthetic access$1000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;)Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mCustomSurfaceView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mIsOffHookOccured:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Landroid/telephony/TelephonyManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;Landroid/telephony/TelephonyManager;)Landroid/telephony/TelephonyManager;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;
    .param p1, "x1"    # Landroid/telephony/TelephonyManager;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;
    .param p1, "x1"    # Landroid/media/MediaPlayer;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->abandonAudioFocus()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/android/gallery3d/app/GalleryMultiWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMultiWindow:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;Lcom/sec/android/gallery3d/app/GalleryMultiWindow;)Lcom/sec/android/gallery3d/app/GalleryMultiWindow;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMultiWindow:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Landroid/view/Window;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mWindow:Landroid/view/Window;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;Landroid/view/Window;)Landroid/view/Window;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;
    .param p1, "x1"    # Landroid/view/Window;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mWindow:Landroid/view/Window;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Landroid/view/SurfaceHolder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mHolder:Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;
    .param p1, "x1"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mHolder:Landroid/view/SurfaceHolder;

    return-object p1
.end method

.method private gainAudioFocus()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 266
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mContext:Landroid/content/Context;

    const-string v3, "audio"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 268
    .local v0, "am":Landroid/media/AudioManager;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v3, 0x3

    const/4 v4, 0x2

    invoke-virtual {v0, v2, v3, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v1

    .line 272
    .local v1, "result":I
    if-ne v1, v5, :cond_1

    .line 273
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->audioFocusGranted:Z

    .line 278
    :cond_0
    :goto_0
    return-void

    .line 274
    :cond_1
    if-nez v1, :cond_0

    .line 276
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->audioFocusGranted:Z

    goto :goto_0
.end method


# virtual methods
.method public finishSlideshowVideoView()V
    .locals 4

    .prologue
    .line 353
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 354
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->misVideoPlaying:Z

    .line 355
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 356
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 357
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 359
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->abandonAudioFocus()V

    .line 360
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 361
    return-void
.end method

.method public initVideoView(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 4
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 106
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 108
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getInstance()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 109
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getInstance()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->pauseSlideshowMusic()V

    .line 110
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPlayUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 111
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 121
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_1

    .line 122
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 123
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 124
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 125
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 127
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mContext:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 129
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v3, 0x20

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 131
    return-void

    .line 112
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 114
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 115
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    .line 116
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_2
    move-exception v0

    .line 117
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 118
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 119
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public pauseVideo()V
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 429
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 430
    :cond_0
    return-void
.end method

.method public resumeVideoPlay()V
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 346
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->audioFocusGranted:Z

    if-nez v0, :cond_0

    .line 347
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->gainAudioFocus()V

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 350
    :cond_1
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 221
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 6
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 225
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_1

    .line 226
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->misVideoPlaying:Z

    .line 227
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 228
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->gainAudioFocus()V

    .line 229
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 230
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mVideoHeight:I

    .line 231
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mVideoWidth:I

    .line 232
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mHolder:Landroid/view/SurfaceHolder;

    iget v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mVideoWidth:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mVideoHeight:I

    invoke-interface {v1, v2, v3}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 233
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isSlideshowPauseViewEnable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 235
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    const-wide/16 v4, 0xa

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 237
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isSlideshowPauseViewEnable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 239
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->refreshSlideShowView()V

    .line 244
    :cond_1
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$6;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$6;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 260
    .local v0, "decodingThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 262
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 342
    return-void
.end method
