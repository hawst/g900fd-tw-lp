.class public Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;
.super Ljava/lang/Object;
.source "WifiDisplayUtils.java"


# static fields
.field public static final CASE_GROUP_PLAY:I = 0x7

.field public static final CASE_HDMI:I = 0x3

.field public static final CASE_HOTSPOT:I = 0x1

.field public static final CASE_NORMAL:I = 0x0

.field public static final CASE_POWER_SAVING_MODE:I = 0x5

.field public static final CASE_SYDESYNC:I = 0x4

.field public static final CASE_TOGETHER_ACTIVATE:I = 0x8

.field public static final CASE_WIFI_DIRECT:I = 0x2

.field public static final DISPLAY_INFO_DEVICE_ADDRESS:I = 0x1

.field public static final DISPLAY_INFO_DEVICE_NAME:I = 0x2

.field public static final DISPLAY_INFO_DEVICE_TYPE:I = 0x3

.field private static final TAG:Ljava/lang/String;

.field private static final sWfdImagesResIds:[I


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDetailViewRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/samsung/gallery/view/detailview/DetailViewState;",
            ">;"
        }
    .end annotation
.end field

.field private mDisplayManager:Landroid/hardware/display/DisplayManager;

.field private mIsReceiverRegistered:Z

.field private mOnDeviceChangedListenerRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/gallery3d/remote/nearby/OnDeviceChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mSettingsObserver:Landroid/database/ContentObserver;

.field private mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

.field private mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->TAG:Ljava/lang/String;

    .line 392
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->sWfdImagesResIds:[I

    return-void

    :array_0
    .array-data 4
        0x7f02035e
        0x7f020361
        0x7f020364
        0x7f02035d
        0x7f020365
        0x7f020363
        0x7f02035f
        0x7f020362
        0x7f020360
        0x7f020366
        0x7f02035c
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mIsReceiverRegistered:Z

    .line 49
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mDetailViewRef:Ljava/lang/ref/WeakReference;

    .line 51
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mOnDeviceChangedListenerRef:Ljava/lang/ref/WeakReference;

    .line 254
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils$2;-><init>(Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 80
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mContext:Landroid/content/Context;

    .line 82
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mContext:Landroid/content/Context;

    const-string v1, "display"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 83
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    .line 84
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 86
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils$1;

    new-instance v1, Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils$1;-><init>(Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mSettingsObserver:Landroid/database/ContentObserver;

    .line 93
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->update()V

    return-void
.end method

.method static synthetic access$202(Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;Landroid/hardware/display/WifiDisplayStatus;)Landroid/hardware/display/WifiDisplayStatus;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;
    .param p1, "x1"    # Landroid/hardware/display/WifiDisplayStatus;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->applyState()V

    return-void
.end method

.method private applyState()V
    .locals 3

    .prologue
    .line 150
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mDetailViewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .line 151
    .local v1, "state":Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    if-eqz v1, :cond_0

    .line 152
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateChangePlayerIcon()V

    .line 155
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mOnDeviceChangedListenerRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/nearby/OnDeviceChangedListener;

    .line 156
    .local v0, "listener":Lcom/sec/android/gallery3d/remote/nearby/OnDeviceChangedListener;
    if-eqz v0, :cond_1

    .line 157
    invoke-interface {v0}, Lcom/sec/android/gallery3d/remote/nearby/OnDeviceChangedListener;->onDeviceChanged()V

    .line 159
    :cond_1
    return-void
.end method

.method private static getDefaultIcon(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 385
    const/16 v0, 0x9

    invoke-static {p0, v0}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->getDeviceIcon(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private static getDeviceIcon(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "deviceType"    # I

    .prologue
    .line 389
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->sWfdImagesResIds:[I

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public static getWfdIcon(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "primaryDeviceType"    # Ljava/lang/String;

    .prologue
    .line 357
    if-nez p1, :cond_0

    .line 358
    invoke-static {p0}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->getDefaultIcon(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 381
    :goto_0
    return-object v3

    .line 362
    :cond_0
    const-string v3, "-"

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 363
    .local v2, "tokens":[Ljava/lang/String;
    array-length v3, v2

    if-nez v3, :cond_1

    .line 364
    invoke-static {p0}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->getDefaultIcon(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    goto :goto_0

    .line 370
    :cond_1
    const/4 v3, 0x0

    :try_start_0
    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 376
    .local v0, "deviceType":I
    const/4 v3, 0x1

    if-gt v3, v0, :cond_2

    const/16 v3, 0xc

    if-ge v0, v3, :cond_2

    .line 377
    add-int/lit8 v3, v0, -0x1

    invoke-static {p0, v3}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->getDeviceIcon(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    goto :goto_0

    .line 371
    .end local v0    # "deviceType":I
    :catch_0
    move-exception v1

    .line 372
    .local v1, "e":Ljava/lang/NumberFormatException;
    invoke-static {p0}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->getDefaultIcon(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    goto :goto_0

    .line 381
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    .restart local v0    # "deviceType":I
    :cond_2
    invoke-static {p0}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->getDefaultIcon(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    goto :goto_0
.end method

.method private update()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    .line 146
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->applyState()V

    .line 147
    return-void
.end method


# virtual methods
.method public connectWifiDisplay(Landroid/hardware/display/WifiDisplay;)V
    .locals 3
    .param p1, "wifiDisplay"    # Landroid/hardware/display/WifiDisplay;

    .prologue
    .line 126
    sget-object v0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "connect WiFi display : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/hardware/display/WifiDisplay;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    const/4 v1, 0x7

    invoke-virtual {p1}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/hardware/display/DisplayManager;->connectWifiDisplayWithMode(ILjava/lang/String;)V

    .line 129
    return-void
.end method

.method public disconnectWifiDisplay()V
    .locals 2

    .prologue
    .line 132
    sget-object v0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->TAG:Ljava/lang/String;

    const-string v1, "disconnect WiFi displays"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->disconnectWifiDisplay()V

    .line 134
    return-void
.end method

.method public getCurrentWifiDisplay()Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 245
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v2}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplay()Landroid/hardware/display/WifiDisplay;

    move-result-object v5

    .line 246
    .local v5, "wifiDisplay":Landroid/hardware/display/WifiDisplay;
    if-nez v5, :cond_0

    .line 251
    :goto_0
    return-object v1

    .line 249
    :cond_0
    new-instance v0, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;

    invoke-virtual {v5}, Landroid/hardware/display/WifiDisplay;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    move-object v4, v1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;-><init>(Landroid/net/Uri;Ljava/lang/String;ILjava/lang/String;Ljava/lang/Object;)V

    .local v0, "item":Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerItem;
    move-object v1, v0

    .line 251
    goto :goto_0
.end method

.method public getExceptionalCase()I
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->checkExceptionalCase()I

    move-result v0

    return v0
.end method

.method public getWifiDisplayAvailable()[Landroid/hardware/display/WifiDisplay;
    .locals 9

    .prologue
    .line 166
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v6}, Landroid/hardware/display/WifiDisplayStatus;->getDisplays()[Landroid/hardware/display/WifiDisplay;

    move-result-object v3

    .line 168
    .local v3, "displays":[Landroid/hardware/display/WifiDisplay;
    sget-object v6, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "get wifi display device list : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    array-length v8, v3

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 171
    .local v1, "availableList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/hardware/display/WifiDisplay;>;"
    move-object v0, v3

    .local v0, "arr$":[Landroid/hardware/display/WifiDisplay;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v2, v0, v4

    .line 172
    .local v2, "d":Landroid/hardware/display/WifiDisplay;
    invoke-virtual {v2}, Landroid/hardware/display/WifiDisplay;->isAvailable()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 173
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 177
    .end local v2    # "d":Landroid/hardware/display/WifiDisplay;
    :cond_1
    sget-object v6, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "get wifi display device available : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Landroid/hardware/display/WifiDisplay;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Landroid/hardware/display/WifiDisplay;

    return-object v6
.end method

.method public getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    return-object v0
.end method

.method public isAlertDialogOff()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 293
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->getExceptionalCase()I

    move-result v0

    .line 294
    .local v0, "exceptionCase":I
    packed-switch v0, :pswitch_data_0

    .line 311
    :pswitch_0
    const/4 v1, 0x1

    :goto_0
    :pswitch_1
    return v1

    .line 296
    :pswitch_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "wifi_display_alert_for_hotspot_is_off"

    invoke-static {v2, v3, v1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_0

    .line 298
    :pswitch_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "wifi_display_alert_for_wifi_direct_is_off"

    invoke-static {v2, v3, v1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_0

    .line 300
    :pswitch_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "wifi_display_alert_for_sydesync"

    invoke-static {v2, v3, v1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_0

    .line 302
    :pswitch_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "wifi_display_alert_for_power_saving_on"

    invoke-static {v2, v3, v1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_0

    .line 294
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isP2pConnected()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 184
    :try_start_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mContext:Landroid/content/Context;

    const-string v5, "connectivity"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 186
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    const/16 v4, 0xd

    invoke-virtual {v0, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 188
    .local v2, "netInfo":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v4

    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v4, v5, :cond_0

    .line 189
    sget-object v4, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->TAG:Ljava/lang/String;

    const-string v5, "isP2pConnected >> true"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    const/4 v3, 0x1

    .line 199
    .end local v0    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v2    # "netInfo":Landroid/net/NetworkInfo;
    :goto_0
    return v3

    .line 192
    .restart local v0    # "connectivityManager":Landroid/net/ConnectivityManager;
    .restart local v2    # "netInfo":Landroid/net/NetworkInfo;
    :cond_0
    sget-object v4, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->TAG:Ljava/lang/String;

    const-string v5, "isP2pConnected >> false"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 195
    .end local v0    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v2    # "netInfo":Landroid/net/NetworkInfo;
    :catch_0
    move-exception v1

    .line 196
    .local v1, "e":Ljava/lang/NullPointerException;
    sget-object v4, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->TAG:Ljava/lang/String;

    const-string v5, "isP2pConnected - NullPointerException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isP2pConnecting()Z
    .locals 7

    .prologue
    .line 221
    const/4 v3, 0x0

    .line 224
    .local v3, "result":Z
    :try_start_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mContext:Landroid/content/Context;

    const-string v6, "connectivity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 226
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    const/16 v5, 0xd

    invoke-virtual {v0, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 228
    .local v2, "netInfo":Landroid/net/NetworkInfo;
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v4

    .line 229
    .local v4, "state":Landroid/net/NetworkInfo$DetailedState;
    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->CONNECTING:Landroid/net/NetworkInfo$DetailedState;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v4, v5, :cond_0

    .line 230
    const/4 v3, 0x1

    .line 237
    .end local v0    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v2    # "netInfo":Landroid/net/NetworkInfo;
    .end local v4    # "state":Landroid/net/NetworkInfo$DetailedState;
    :goto_0
    return v3

    .line 232
    .restart local v0    # "connectivityManager":Landroid/net/ConnectivityManager;
    .restart local v2    # "netInfo":Landroid/net/NetworkInfo;
    .restart local v4    # "state":Landroid/net/NetworkInfo$DetailedState;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 234
    .end local v0    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v2    # "netInfo":Landroid/net/NetworkInfo;
    .end local v4    # "state":Landroid/net/NetworkInfo$DetailedState;
    :catch_0
    move-exception v1

    .line 235
    .local v1, "e":Ljava/lang/NullPointerException;
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public isP2pWorking()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 204
    :try_start_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mContext:Landroid/content/Context;

    const-string v6, "connectivity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 206
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    const/16 v5, 0xd

    invoke-virtual {v0, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 209
    .local v2, "netInfo":Landroid/net/NetworkInfo;
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v3

    .line 210
    .local v3, "state":Landroid/net/NetworkInfo$DetailedState;
    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-eq v3, v5, :cond_0

    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->CONNECTING:Landroid/net/NetworkInfo$DetailedState;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v3, v5, :cond_1

    :cond_0
    const/4 v4, 0x1

    .line 217
    .end local v0    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v2    # "netInfo":Landroid/net/NetworkInfo;
    .end local v3    # "state":Landroid/net/NetworkInfo$DetailedState;
    :cond_1
    :goto_0
    return v4

    .line 213
    :catch_0
    move-exception v1

    .line 214
    .local v1, "e":Ljava/lang/NullPointerException;
    sget-object v5, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->TAG:Ljava/lang/String;

    const-string v6, "isP2pConnected - NullPointerException"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isScanningWifiDisplay()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 241
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplayStatus;->getScanState()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWifiDisplayConnected()Z
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 283
    sget-object v1, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->TAG:Ljava/lang/String;

    const-string v2, "pause"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    sget-object v2, Landroid/hardware/display/DisplayManager$WfdAppState;->PAUSE:Landroid/hardware/display/DisplayManager$WfdAppState;

    invoke-virtual {v1, v2}, Landroid/hardware/display/DisplayManager;->setActivityState(Landroid/hardware/display/DisplayManager$WfdAppState;)V

    .line 286
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    sget-object v2, Landroid/hardware/display/DisplayManager$WfdAppState;->TEARDOWN:Landroid/hardware/display/DisplayManager$WfdAppState;

    invoke-virtual {v1, v2}, Landroid/hardware/display/DisplayManager;->setActivityState(Landroid/hardware/display/DisplayManager$WfdAppState;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    :goto_0
    return-void

    .line 287
    :catch_0
    move-exception v0

    .line 288
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 273
    sget-object v1, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->TAG:Ljava/lang/String;

    const-string v2, "resume"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    sget-object v2, Landroid/hardware/display/DisplayManager$WfdAppState;->SETUP:Landroid/hardware/display/DisplayManager$WfdAppState;

    invoke-virtual {v1, v2}, Landroid/hardware/display/DisplayManager;->setActivityState(Landroid/hardware/display/DisplayManager$WfdAppState;)V

    .line 276
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    sget-object v2, Landroid/hardware/display/DisplayManager$WfdAppState;->RESUME:Landroid/hardware/display/DisplayManager$WfdAppState;

    invoke-virtual {v1, v2}, Landroid/hardware/display/DisplayManager;->setActivityState(Landroid/hardware/display/DisplayManager$WfdAppState;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 280
    :goto_0
    return-void

    .line 277
    :catch_0
    move-exception v0

    .line 278
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0
.end method

.method public registerReceiver()V
    .locals 3

    .prologue
    .line 96
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mIsReceiverRegistered:Z

    if-nez v1, :cond_0

    .line 97
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mIsReceiverRegistered:Z

    .line 98
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 100
    .local v0, "iFilter":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 102
    .end local v0    # "iFilter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method public scanWifiDisplays()V
    .locals 2

    .prologue
    .line 113
    sget-object v0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->TAG:Ljava/lang/String;

    const-string v1, "scan WiFi displays"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->scanWifiDisplays()V

    .line 115
    return-void
.end method

.method public setAlertDialogOff()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 315
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->getExceptionalCase()I

    move-result v0

    .line 316
    .local v0, "exceptionCase":I
    packed-switch v0, :pswitch_data_0

    .line 334
    :goto_0
    :pswitch_0
    return-void

    .line 318
    :pswitch_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "wifi_display_alert_for_hotspot_is_off"

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    .line 321
    :pswitch_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "wifi_display_alert_for_wifi_direct_is_off"

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    .line 324
    :pswitch_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "wifi_display_alert_for_sydesync"

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    .line 327
    :pswitch_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "wifi_display_alert_for_power_saving_on"

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    .line 316
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setDetailViewRef(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 1
    .param p1, "state"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 137
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mDetailViewRef:Ljava/lang/ref/WeakReference;

    .line 138
    return-void
.end method

.method public setOnDeviceChangedListener(Lcom/sec/android/gallery3d/remote/nearby/OnDeviceChangedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/remote/nearby/OnDeviceChangedListener;

    .prologue
    .line 141
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mOnDeviceChangedListenerRef:Ljava/lang/ref/WeakReference;

    .line 142
    return-void
.end method

.method public setWifiBSSEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 342
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, p1}, Landroid/net/wifi/WifiManager;->setWifiIBSSEnabled(Z)Z

    .line 345
    :cond_0
    return-void
.end method

.method public setWifiEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 348
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, p1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 351
    :cond_0
    return-void
.end method

.method public stopScanWifiDisplays()V
    .locals 2

    .prologue
    .line 121
    sget-object v0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stop scan WiFi displays"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->stopScanWifiDisplays()V

    .line 123
    return-void
.end method

.method public unregisterReceiver()V
    .locals 2

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mIsReceiverRegistered:Z

    if-eqz v0, :cond_0

    .line 106
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mIsReceiverRegistered:Z

    .line 107
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 108
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mSettingsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 110
    :cond_0
    return-void
.end method
