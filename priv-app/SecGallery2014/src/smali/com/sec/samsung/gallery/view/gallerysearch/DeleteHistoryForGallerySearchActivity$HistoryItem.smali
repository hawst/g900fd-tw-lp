.class public Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;
.super Ljava/lang/Object;
.source "DeleteHistoryForGallerySearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "HistoryItem"
.end annotation


# instance fields
.field private mDate:J

.field private mDateStr:Ljava/lang/String;

.field private mIsChecked:Z

.field private mKeyword:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;Ljava/lang/String;J)V
    .locals 3
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "millis"    # J

    .prologue
    const/4 v0, 0x0

    .line 532
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 524
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->mKeyword:Ljava/lang/String;

    .line 526
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->mDateStr:Ljava/lang/String;

    .line 528
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->mDate:J

    .line 530
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->mIsChecked:Z

    .line 533
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->mKeyword:Ljava/lang/String;

    .line 534
    iput-wide p3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->mDate:J

    .line 535
    return-void
.end method

.method public constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "date"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 537
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 524
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->mKeyword:Ljava/lang/String;

    .line 526
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->mDateStr:Ljava/lang/String;

    .line 528
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->mDate:J

    .line 530
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->mIsChecked:Z

    .line 538
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->mKeyword:Ljava/lang/String;

    .line 539
    iput-object p3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->mDateStr:Ljava/lang/String;

    .line 540
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;

    .prologue
    .line 522
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->mIsChecked:Z

    return v0
.end method


# virtual methods
.method public getChecked()Z
    .locals 1

    .prologue
    .line 559
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->mIsChecked:Z

    return v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 2

    .prologue
    .line 547
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->mDateStr:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 548
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->mDateStr:Ljava/lang/String;

    .line 551
    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->mDate:J

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->getSuggestionHistoryTime(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getKeyword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 543
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->mKeyword:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionHistoryTime(J)Ljava/lang/String;
    .locals 17
    .param p1, "time"    # J

    .prologue
    .line 563
    const/4 v13, 0x0

    .line 564
    .local v13, "result":Ljava/lang/String;
    const-wide/16 v6, -0x1

    cmp-long v6, p1, v6

    if-nez v6, :cond_0

    const/4 v6, 0x0

    .line 596
    :goto_0
    return-object v6

    .line 566
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v8

    .line 567
    .local v8, "currentDate":Ljava/util/Calendar;
    const/4 v6, 0x2

    invoke-virtual {v8, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 568
    .local v4, "todayMonth":I
    const/4 v6, 0x1

    invoke-virtual {v8, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 569
    .local v3, "todayYear":I
    const/4 v6, 0x5

    invoke-virtual {v8, v6}, Ljava/util/Calendar;->get(I)I

    move-result v5

    .line 571
    .local v5, "todayDay":I
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 572
    .local v2, "today":Ljava/util/Calendar;
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Ljava/util/Calendar;->set(IIIII)V

    .line 573
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    .line 575
    .local v10, "currentTime":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "date_format"

    invoke-static {v6, v7}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 577
    .local v14, "sysDate":Ljava/lang/String;
    const/4 v12, 0x0

    .line 578
    .local v12, "dateStr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v16, "HH:mm"

    .line 580
    .local v16, "timeStr":Ljava/lang/String;
    :goto_1
    const-string v6, "dd-MM-yyyy"

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 581
    const-string v12, "dd/MM"

    .line 586
    :goto_2
    new-instance v15, Ljava/text/SimpleDateFormat;

    invoke-direct/range {v15 .. v16}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 587
    .local v15, "timeFormat":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/text/SimpleDateFormat;

    invoke-direct {v9, v12}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 589
    .local v9, "dateFormat":Ljava/text/SimpleDateFormat;
    cmp-long v6, p1, v10

    if-ltz v6, :cond_3

    const-wide/32 v6, 0x5265c00

    add-long/2addr v6, v10

    cmp-long v6, v6, p1

    if-lez v6, :cond_3

    .line 590
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x1

    move-wide/from16 v0, p1

    invoke-static {v6, v0, v1, v7}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v13

    :goto_3
    move-object v6, v13

    .line 596
    goto :goto_0

    .line 578
    .end local v9    # "dateFormat":Ljava/text/SimpleDateFormat;
    .end local v15    # "timeFormat":Ljava/text/SimpleDateFormat;
    .end local v16    # "timeStr":Ljava/lang/String;
    :cond_1
    const-string v16, "hh:mm a"

    goto :goto_1

    .line 583
    .restart local v16    # "timeStr":Ljava/lang/String;
    :cond_2
    const-string v12, "MM/dd"

    goto :goto_2

    .line 593
    .restart local v9    # "dateFormat":Ljava/text/SimpleDateFormat;
    .restart local v15    # "timeFormat":Ljava/text/SimpleDateFormat;
    :cond_3
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v9, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    goto :goto_3
.end method

.method public setChecked(Z)V
    .locals 0
    .param p1, "checked"    # Z

    .prologue
    .line 555
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->mIsChecked:Z

    .line 556
    return-void
.end method
