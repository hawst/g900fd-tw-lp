.class public Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
.super Ljava/lang/Object;
.source "LocalDatabaseManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/util/LocalDatabaseManager$LocalDatabaseHelper;
    }
.end annotation


# static fields
.field public static final ALBUM_CREATE_TIME_ENTRY:Ljava/lang/String; = "album_create_time"

.field public static final ALBUM_DISPLAY_INFO_TABLE:Ljava/lang/String; = "album_display_info_table"

.field public static final ALBUM_DISPLAY_INFO_TABLE_CREATE:Ljava/lang/String; = "CREATE TABLE album_display_info_table (_id INTEGER, bucketid_order TEXT);"

.field public static final ALBUM_NAME_ENTRY:Ljava/lang/String; = "album_name"

.field public static final BUCKET_IDS_ORDER:Ljava/lang/String; = "bucketid_order"

.field public static final BUCKET_ID_ENTRY:Ljava/lang/String; = "bucket_id"

.field public static final DATABASE_NAME:Ljava/lang/String; = "local.db"

.field public static final DATABASE_VERSION:I = 0x3

.field public static final DATA_ENTRY:Ljava/lang/String; = "_data"

.field private static final DATE_ADDED_ENTRY:Ljava/lang/String; = "date_added"

.field private static final DATE_MODIFIED_ENTRY:Ljava/lang/String; = "date_modified"

.field public static final DATE_TAKEN_ENTRY:Ljava/lang/String; = "datetaken"

.field private static final DURATION_ENTRY:Ljava/lang/String; = "duration"

.field public static EVENT_ALBUM_URI:Landroid/net/Uri; = null

.field public static final FACES_RECT_ENTRY:Ljava/lang/String; = "faces_rect"

.field private static final GROUP_ID_ENTRY:Ljava/lang/String; = "group_id"

.field private static final HEIGHT_ENTRY:Ljava/lang/String; = "height"

.field public static final HIDDEN_ALBUM_ENTRY:Ljava/lang/String; = "hidden_album"

.field public static final HIDDEN_ALBUM_INFO_TABLE:Ljava/lang/String; = "hidden_album_info"

.field public static final HIDDEN_ALBUM_INFO_TABLE_CREATE:Ljava/lang/String; = "CREATE TABLE hidden_album_info (bucket_id INTEGER UNIQUE);"

.field public static HIDDEN_ALBUM_URI:Landroid/net/Uri; = null

.field public static final ID_ENTRY:Ljava/lang/String; = "_id"

.field public static final IMAGES_HIDDEN_ALBUM_TABLE:Ljava/lang/String; = "images_hidden_album"

.field public static final IMAGES_HIDDEN_ALBUM_TABLE_CREATE:Ljava/lang/String; = "CREATE TABLE images_hidden_album (_id INTEGER, title TEXT, mime_type TEXT, latitude REAL, longitude REAL, datetaken INTEGER, date_added INTEGER, date_modified INTEGER, _data TEXT UNIQUE, orientation INTEGER, bucket_id INTEGER, _size INTEGER, width INTEGER, height INTEGER, group_id INTEGER, spherical_mosaic INTEGER, sef_file_type INTEGER, album_name TEXT, hidden_album TEXT, source_album TEXT, faces_rect BLOB );"

.field public static final IMAGE_EVENT_ALBUM_TABLE:Ljava/lang/String; = "images_event_album"

.field public static final IMAGE_EVENT_ALBUM_TABLE_CREATE:Ljava/lang/String; = "CREATE TABLE images_event_album (pkey INTEGER UNIQUE, _id INTEGER, title TEXT, mime_type TEXT, latitude REAL, longitude REAL, datetaken INTEGER, date_added INTEGER, date_modified INTEGER, _data TEXT, orientation INTEGER, bucket_id INTEGER, _size INTEGER, width INTEGER, height INTEGER, group_id INTEGER, spherical_mosaic INTEGER, sef_file_type INTEGER, bucket_display_name TEXT, suggestion INTEGER, album_create_time INTEGER);"

.field public static final LATITUDE_ENTRY:Ljava/lang/String; = "latitude"

.field public static final LOCALITY_ENTRY:Ljava/lang/String; = "locality"

.field public static final LONGITUDE_ENTRY:Ljava/lang/String; = "longitude"

.field public static final MIME_TYPE_ENTRY:Ljava/lang/String; = "mime_type"

.field private static final ORIENTATION_ENTRY:Ljava/lang/String; = "orientation"

.field public static final PKEY:Ljava/lang/String; = "pkey"

.field private static final RESOLUTION_ENTRY:Ljava/lang/String; = "resolution"

.field public static final SEARCH_HISTORY_TABLE:Ljava/lang/String; = "search_history"

.field public static final SEARCH_HISTORY_TABLE_CREATE:Ljava/lang/String; = "CREATE TABLE search_history (title TEXT UNIQUE NOT NULL, date_added INTEGER);"

.field public static SEARCH_HISTORY_URI:Landroid/net/Uri; = null

.field private static final SEF_FILE_TPYE_ENTRY:Ljava/lang/String; = "sef_file_type"

.field private static final SIZE_ENTRY:Ljava/lang/String; = "_size"

.field public static final SOURCE_DATA_ENTRY:Ljava/lang/String; = "source_album"

.field private static final SPHERICAL_MOSAIC_ENTRY:Ljava/lang/String; = "spherical_mosaic"

.field public static SUGGESTED_ALBUM_URI:Landroid/net/Uri; = null

.field public static final SUGGESTION_ENTRY:Ljava/lang/String; = "suggestion"

.field public static final SUGGEST_EVENT_INFO_TABLE:Ljava/lang/String; = "suggest_event_info"

.field public static final SUGGEST_EVENT_INFO_TABLE_CREATE:Ljava/lang/String; = "CREATE TABLE suggest_event_info (_id INTEGER, latitude REAL, longitude REAL, _data TEXT UNIQUE, datetaken INTEGER, locality TEXT, type INTEGER);"

.field private static final TAG:Ljava/lang/String; = "LocalDatabaseManager"

.field private static final TITLE_ENTRY:Ljava/lang/String; = "title"

.field public static final TYPE_ENTRY:Ljava/lang/String; = "type"

.field public static final TYPE_IMAGE:I = 0x0

.field public static final TYPE_VIDEO:I = 0x1

.field public static final VIDEO_EVENT_ALBUM_TABLE:Ljava/lang/String; = "video_event_album"

.field public static final VIDEO_EVENT_ALBUM_TABLE_CREATE:Ljava/lang/String; = "CREATE TABLE video_event_album (pkey INTEGER UNIQUE, _id INTEGER, title TEXT, mime_type TEXT, latitude REAL, longitude REAL, datetaken INTEGER, date_added INTEGER, date_modified INTEGER, _data TEXT, duration INTEGER, bucket_id INTEGER, _size INTEGER, resolution TEXT, bucket_display_name TEXT, suggestion INTEGER, album_create_time INTEGER);"

.field public static final VIDEO_HIDDEN_ALBUM_TABLE:Ljava/lang/String; = "video_hidden_album"

.field public static final VIDEO_HIDDEN_ALBUM_TABLE_CREATE:Ljava/lang/String; = "CREATE TABLE video_hidden_album (_id INTEGER, title TEXT, mime_type TEXT, latitude REAL, longitude REAL, datetaken INTEGER, date_added INTEGER, date_modified INTEGER, _data TEXT UNIQUE, duration INTEGER, bucket_id INTEGER, _size INTEGER, album_name TEXT, hidden_album TEXT, source_album TEXT, resolution TEXT);"

.field private static final WIDTH_ENTRY:Ljava/lang/String; = "width"

.field private static sInstance:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDatabaseOpenHelper:Lcom/sec/samsung/gallery/util/LocalDatabaseManager$LocalDatabaseHelper;

.field private mFileObserver:Landroid/os/FileObserver;

.field private final mResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 184
    const-string v0, "hidden://hidden"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->HIDDEN_ALBUM_URI:Landroid/net/Uri;

    .line 185
    const-string v0, "event://suggested"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->SUGGESTED_ALBUM_URI:Landroid/net/Uri;

    .line 186
    const-string v0, "event://event"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->EVENT_ALBUM_URI:Landroid/net/Uri;

    .line 187
    const-string v0, "search://history"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->SEARCH_HISTORY_URI:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 3
    .param p1, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 198
    invoke-interface {p1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->mContext:Landroid/content/Context;

    .line 199
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->mResolver:Landroid/content/ContentResolver;

    .line 200
    new-instance v1, Lcom/sec/samsung/gallery/util/LocalDatabaseManager$LocalDatabaseHelper;

    iget-object v2, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager$LocalDatabaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->mDatabaseOpenHelper:Lcom/sec/samsung/gallery/util/LocalDatabaseManager$LocalDatabaseHelper;

    .line 202
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    :goto_0
    return-void

    .line 203
    :catch_0
    move-exception v0

    .line 204
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static declared-synchronized getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
    .locals 2
    .param p0, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 211
    const-class v1, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->sInstance:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    if-nez v0, :cond_0

    .line 212
    new-instance v0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;-><init>(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    sput-object v0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->sInstance:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    .line 214
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->sInstance:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 211
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private isEmptyHiddenAlbumForType(ILcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Z
    .locals 11
    .param p1, "bucketId"    # I
    .param p2, "mediaFilterType"    # Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 348
    const/4 v6, 0x0

    .line 349
    .local v6, "c1":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 351
    .local v7, "c2":Landroid/database/Cursor;
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 352
    .local v2, "projection":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 353
    .local v3, "where":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 354
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bucket_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 356
    :cond_0
    if-eqz p2, :cond_4

    .line 357
    sget-object v0, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->IMAGE:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-ne p2, v0, :cond_2

    .line 358
    const-string v1, "images_hidden_album"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 359
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_1

    .line 386
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 387
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move v0, v9

    .line 389
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "where":Ljava/lang/String;
    :goto_0
    return v0

    .line 386
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v3    # "where":Ljava/lang/String;
    :cond_1
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 387
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move v0, v10

    goto :goto_0

    .line 364
    :cond_2
    :try_start_1
    sget-object v0, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-ne p2, v0, :cond_4

    .line 365
    const-string/jumbo v1, "video_hidden_album"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 366
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-lez v0, :cond_3

    .line 386
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 387
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move v0, v9

    goto :goto_0

    .line 386
    :cond_3
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 387
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move v0, v10

    goto :goto_0

    .line 373
    :cond_4
    if-eqz p2, :cond_5

    :try_start_2
    sget-object v0, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->IMAGE_AND_VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-ne p2, v0, :cond_7

    .line 374
    :cond_5
    const-string v1, "images_hidden_album"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 375
    if-eqz v6, :cond_6

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-lez v0, :cond_6

    .line 386
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 387
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move v0, v9

    goto :goto_0

    .line 378
    :cond_6
    :try_start_3
    const-string/jumbo v1, "video_hidden_album"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 379
    if-eqz v7, :cond_7

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    if-lez v0, :cond_7

    .line 386
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 387
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move v0, v9

    goto :goto_0

    .line 386
    :cond_7
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 387
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "where":Ljava/lang/String;
    :goto_1
    move v0, v10

    .line 389
    goto :goto_0

    .line 383
    :catch_0
    move-exception v8

    .line 384
    .local v8, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 386
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 387
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .line 386
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 387
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method


# virtual methods
.method public addAlbumDisplayInfo(Ljava/lang/String;)Z
    .locals 11
    .param p1, "order"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 514
    const/4 v0, 0x0

    .line 515
    .local v0, "database":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 516
    .local v4, "values":Landroid/content/ContentValues;
    const-string v7, "_id"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 517
    const-string v7, "bucketid_order"

    invoke-virtual {v4, v7, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    const-wide/16 v2, -0x1

    .line 520
    .local v2, "id":J
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 521
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 522
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->deleteAlbumDisplayInfo(I)Z

    .line 524
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 525
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 526
    const-string v7, "album_display_info_table"

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 528
    :cond_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->mResolver:Landroid/content/ContentResolver;

    const-string v8, "content://album_order"

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v9, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 536
    :goto_0
    const-wide/16 v8, 0x0

    cmp-long v7, v2, v8

    if-lez v7, :cond_2

    :goto_1
    return v5

    .line 530
    :catch_0
    move-exception v1

    .line 531
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 532
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    throw v5

    :cond_2
    move v5, v6

    .line 536
    goto :goto_1
.end method

.method public addEventInfo(Ljava/lang/String;[Landroid/content/ContentValues;)I
    .locals 1
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "values"    # [Landroid/content/ContentValues;

    .prologue
    .line 556
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->addEventInfo(Ljava/lang/String;[Landroid/content/ContentValues;Z)I

    move-result v0

    return v0
.end method

.method public addEventInfo(Ljava/lang/String;[Landroid/content/ContentValues;Z)I
    .locals 9
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "values"    # [Landroid/content/ContentValues;
    .param p3, "needNotify"    # Z

    .prologue
    const/4 v8, 0x0

    .line 560
    const/4 v2, 0x0

    .line 561
    .local v2, "database":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 562
    .local v0, "count":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 563
    if-nez v2, :cond_0

    move v1, v0

    .line 589
    .end local v0    # "count":I
    .local v1, "count":I
    :goto_0
    return v1

    .line 566
    .end local v1    # "count":I
    .restart local v0    # "count":I
    :cond_0
    monitor-enter v2

    .line 568
    :try_start_0
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 569
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 570
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    array-length v5, p2

    if-ge v4, v5, :cond_2

    .line 571
    aget-object v5, p2, v4

    if-eqz v5, :cond_1

    .line 572
    const/4 v5, 0x0

    aget-object v6, p2, v4

    const/4 v7, 0x5

    invoke-virtual {v2, p1, v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 573
    add-int/lit8 v0, v0, 0x1

    .line 570
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 576
    :cond_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 582
    .end local v4    # "i":I
    :cond_3
    if-eqz v2, :cond_4

    .line 583
    :try_start_1
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 586
    :cond_4
    :goto_2
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 587
    if-eqz p3, :cond_5

    if-lez v0, :cond_5

    const-string/jumbo v5, "suggest_event_info"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 588
    iget-object v5, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->mResolver:Landroid/content/ContentResolver;

    sget-object v6, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->SUGGESTED_ALBUM_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_5
    move v1, v0

    .line 589
    .end local v0    # "count":I
    .restart local v1    # "count":I
    goto :goto_0

    .line 578
    .end local v1    # "count":I
    .restart local v0    # "count":I
    :catch_0
    move-exception v3

    .line 579
    .local v3, "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    .line 580
    :try_start_2
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 582
    if-eqz v2, :cond_4

    .line 583
    :try_start_3
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_2

    .line 586
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v5

    .line 582
    :catchall_1
    move-exception v5

    if-eqz v2, :cond_6

    .line 583
    :try_start_4
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_6
    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public addHiddenAlbumInfo(I)Z
    .locals 6
    .param p1, "bucketId"    # I

    .prologue
    .line 310
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 311
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "bucket_id"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 312
    const-string v1, "hidden_album_info"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addHiddenItem(Ljava/lang/String;Landroid/content/ContentValues;)J
    .locals 4
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "initialValues"    # Landroid/content/ContentValues;

    .prologue
    const/4 v3, 0x0

    .line 287
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->mFileObserver:Landroid/os/FileObserver;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/android/gallery3d/data/HiddenSource;->ISLAND_HIDE_PATH:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->isEmptyHiddenAlbum(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->mFileObserver:Landroid/os/FileObserver;

    if-eqz v0, :cond_1

    .line 289
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->mFileObserver:Landroid/os/FileObserver;

    invoke-virtual {v0}, Landroid/os/FileObserver;->stopWatching()V

    .line 290
    iput-object v3, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->mFileObserver:Landroid/os/FileObserver;

    .line 293
    :cond_1
    new-instance v0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager$1;

    sget-object v1, Lcom/sec/android/gallery3d/data/HiddenSource;->ISLAND_HIDE_PATH:Ljava/lang/String;

    const/16 v2, 0x240

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager$1;-><init>(Lcom/sec/samsung/gallery/util/LocalDatabaseManager;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->mFileObserver:Landroid/os/FileObserver;

    .line 305
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->mFileObserver:Landroid/os/FileObserver;

    invoke-virtual {v0}, Landroid/os/FileObserver;->startWatching()V

    .line 306
    invoke-virtual {p0, p1, v3, p2}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public close()V
    .locals 3

    .prologue
    .line 218
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 219
    .local v1, "writableDatabase":Landroid/database/sqlite/SQLiteDatabase;
    if-eqz v1, :cond_0

    .line 220
    monitor-enter v1

    .line 221
    :try_start_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 222
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 225
    .local v0, "readableDatabase":Landroid/database/sqlite/SQLiteDatabase;
    if-eqz v0, :cond_1

    .line 226
    monitor-enter v0

    .line 227
    :try_start_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 228
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 230
    :cond_1
    return-void

    .line 222
    .end local v0    # "readableDatabase":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 228
    .restart local v0    # "readableDatabase":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_1
    move-exception v2

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2
.end method

.method public delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "args"    # [Ljava/lang/String;

    .prologue
    .line 450
    const/4 v1, 0x0

    .line 451
    .local v1, "database":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, -0x1

    .line 453
    .local v0, "count":I
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 454
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 455
    invoke-virtual {v1, p1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 456
    :cond_0
    if-lez v0, :cond_2

    const-string v3, "hidden_album_info"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "images_hidden_album"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string/jumbo v3, "video_hidden_album"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 458
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->mResolver:Landroid/content/ContentResolver;

    sget-object v4, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->HIDDEN_ALBUM_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 461
    :cond_2
    if-lez v0, :cond_4

    const-string/jumbo v3, "suggest_event_info"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "images_event_album"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string/jumbo v3, "video_event_album"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 462
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->mResolver:Landroid/content/ContentResolver;

    sget-object v4, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->EVENT_ALBUM_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 465
    :cond_4
    if-ltz v0, :cond_5

    const-string v3, "search_history"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 466
    iget-object v3, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->mResolver:Landroid/content/ContentResolver;

    sget-object v4, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->SEARCH_HISTORY_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 474
    :cond_5
    :goto_0
    return v0

    .line 468
    :catch_0
    move-exception v2

    .line 469
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 470
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    throw v3
.end method

.method public deleteAlbumDisplayInfo(I)Z
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 551
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 552
    .local v0, "selection":Ljava/lang/String;
    const-string v1, "album_display_info_table"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public deleteHiddenAlbumInfo(I)Z
    .locals 3
    .param p1, "bucketId"    # I

    .prologue
    .line 316
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bucket_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 317
    .local v0, "selection":Ljava/lang/String;
    const-string v1, "hidden_album_info"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public deleteHiddenAlbumItems(Ljava/lang/String;I)V
    .locals 8
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "bucketId"    # I

    .prologue
    const/4 v1, 0x0

    .line 321
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v2, v1

    .line 322
    .local v2, "projections":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bucket_id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 323
    .local v3, "selection":Ljava/lang/String;
    const/4 v6, 0x0

    .line 325
    .local v6, "c":Landroid/database/Cursor;
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 326
    if-eqz v6, :cond_1

    .line 328
    :cond_0
    new-instance v0, Ljava/io/File;

    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 329
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 331
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v3, v0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 335
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 337
    :goto_0
    return-void

    .line 332
    :catch_0
    move-exception v7

    .line 333
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 335
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public deleteHistory(Ljava/lang/String;)V
    .locals 6
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 478
    const/4 v0, 0x0

    .line 481
    .local v0, "database":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 482
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 483
    const-string v2, "search_history"

    const-string/jumbo v3, "title=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 484
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->mResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->SEARCH_HISTORY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 492
    :goto_0
    return-void

    .line 485
    :catch_0
    move-exception v1

    .line 486
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 487
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    throw v2
.end method

.method public getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->mDatabaseOpenHelper:Lcom/sec/samsung/gallery/util/LocalDatabaseManager$LocalDatabaseHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager$LocalDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->mDatabaseOpenHelper:Lcom/sec/samsung/gallery/util/LocalDatabaseManager$LocalDatabaseHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager$LocalDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    .locals 10
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "nullColumnHack"    # Ljava/lang/String;
    .param p3, "initialValues"    # Landroid/content/ContentValues;

    .prologue
    const-wide/16 v8, 0x0

    .line 424
    const/4 v0, 0x0

    .line 425
    .local v0, "database":Landroid/database/sqlite/SQLiteDatabase;
    const-wide/16 v2, -0x1

    .line 427
    .local v2, "id":J
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 428
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 429
    monitor-enter v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 430
    :try_start_1
    invoke-virtual {v0, p1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 431
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 433
    :cond_0
    cmp-long v4, v2, v8

    if-ltz v4, :cond_2

    :try_start_2
    const-string v4, "hidden_album_info"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "images_hidden_album"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string/jumbo v4, "video_hidden_album"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 435
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->mResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->HIDDEN_ALBUM_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 437
    :cond_2
    cmp-long v4, v2, v8

    if-ltz v4, :cond_3

    const-string v4, "search_history"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 438
    iget-object v4, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->mResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->SEARCH_HISTORY_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 446
    :cond_3
    :goto_0
    return-wide v2

    .line 431
    :catchall_0
    move-exception v4

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v4
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 440
    :catch_0
    move-exception v1

    .line 441
    .local v1, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0

    .line 442
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v4

    throw v4
.end method

.method public isEmptyHiddenAlbum(I)Z
    .locals 1
    .param p1, "bucketId"    # I

    .prologue
    .line 340
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->isEmptyHiddenAlbumForType(ILcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Z

    move-result v0

    return v0
.end method

.method public isEmptyHiddenAlbum(ILcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Z
    .locals 1
    .param p1, "bucketId"    # I
    .param p2, "mediaFilterType"    # Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    .prologue
    .line 344
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->isEmptyHiddenAlbumForType(ILcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Z

    move-result v0

    return v0
.end method

.method public query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "columns"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 394
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "columns"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .param p6, "groupBy"    # Ljava/lang/String;

    .prologue
    .line 400
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 401
    .local v0, "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    invoke-virtual {v0, p1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 402
    const/4 v8, 0x0

    .line 403
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v1, 0x0

    .line 405
    .local v1, "database":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 406
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 407
    const/4 v6, 0x0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p6

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 408
    :cond_0
    if-nez v8, :cond_1

    .line 409
    const/4 v2, 0x0

    .line 420
    :goto_0
    return-object v2

    .line 410
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_2

    .line 411
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 412
    const/4 v2, 0x0

    goto :goto_0

    .line 414
    :catch_0
    move-exception v9

    .line 415
    .local v9, "e":Ljava/lang/Exception;
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 416
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    .line 417
    const/4 v2, 0x0

    goto :goto_0

    .end local v9    # "e":Ljava/lang/Exception;
    :cond_2
    move-object v2, v8

    .line 420
    goto :goto_0
.end method

.method public update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "updateValues"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "args"    # [Ljava/lang/String;

    .prologue
    .line 495
    const/4 v0, -0x1

    .line 496
    .local v0, "count":I
    const/4 v1, 0x0

    .line 498
    .local v1, "database":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 499
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 500
    invoke-virtual {v1, p1, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 501
    :cond_0
    if-lez v0, :cond_1

    const-string v3, "hidden_album_info"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 502
    iget-object v3, p0, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->mResolver:Landroid/content/ContentResolver;

    sget-object v4, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->HIDDEN_ALBUM_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 510
    :cond_1
    :goto_0
    return v0

    .line 504
    :catch_0
    move-exception v2

    .line 505
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 506
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    throw v3
.end method

.method public updateDataBase(Ljava/lang/String;)Z
    .locals 1
    .param p1, "table"    # Ljava/lang/String;

    .prologue
    .line 240
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->updateDataBase(Ljava/lang/String;Ljava/lang/Integer;)Z

    move-result v0

    return v0
.end method

.method public updateDataBase(Ljava/lang/String;Ljava/lang/Integer;)Z
    .locals 13
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "bucketId"    # Ljava/lang/Integer;

    .prologue
    .line 244
    const/4 v6, 0x0

    .line 245
    .local v6, "c":Landroid/database/Cursor;
    const/4 v11, 0x0

    .line 248
    .local v11, "isUpdated":Z
    const/4 v3, 0x0

    .line 249
    .local v3, "selection":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 250
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bucket_id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 252
    :cond_0
    const-string v5, "datetaken DESC, _id DESC"

    .line 253
    .local v5, "order":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "_data"

    aput-object v1, v2, v0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 254
    if-nez v6, :cond_1

    .line 255
    const-string v0, "LocalDatabaseManager"

    const-string v1, "cursor is null!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 280
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move v12, v11

    .line 283
    .end local v5    # "order":Ljava/lang/String;
    .end local v11    # "isUpdated":Z
    .local v12, "isUpdated":I
    :goto_0
    return v12

    .line 259
    .end local v12    # "isUpdated":I
    .restart local v5    # "order":Ljava/lang/String;
    .restart local v11    # "isUpdated":Z
    :cond_1
    :try_start_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 261
    .local v7, "deleteWhere":Ljava/lang/StringBuilder;
    :cond_2
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 262
    .local v10, "id":I
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 263
    .local v9, "filepath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_4

    .line 264
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 265
    const-string v0, " OR "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    :cond_3
    const-string v0, "_id"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 270
    const/4 v11, 0x1

    .line 272
    :cond_4
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 274
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_5

    .line 275
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 280
    :cond_5
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .end local v5    # "order":Ljava/lang/String;
    .end local v7    # "deleteWhere":Ljava/lang/StringBuilder;
    .end local v9    # "filepath":Ljava/lang/String;
    .end local v10    # "id":I
    :goto_1
    move v12, v11

    .line 283
    .restart local v12    # "isUpdated":I
    goto :goto_0

    .line 277
    .end local v12    # "isUpdated":I
    :catch_0
    move-exception v8

    .line 278
    .local v8, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 280
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method
