.class Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;
.super Ljava/lang/Object;
.source "Clusterkraf.java"

# interfaces
.implements Lcom/google/android/gms/maps/GoogleMap$OnInfoWindowClickListener;
.implements Lcom/google/android/gms/maps/GoogleMap$OnMarkerClickListener;
.implements Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$Host;
.implements Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener$Host;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InnerCallbackListener"
.end annotation


# instance fields
.field private final clusteringOnCameraChangeListener:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;

.field private final clusterkrafRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;",
            ">;"
        }
    .end annotation
.end field

.field private final handler:Landroid/os/Handler;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)V
    .locals 2
    .param p1, "clusterkraf"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    .prologue
    .line 339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 337
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;->handler:Landroid/os/Handler;

    .line 340
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;->clusterkrafRef:Ljava/lang/ref/WeakReference;

    .line 341
    new-instance v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$600()Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;-><init>(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener$Host;Lcom/sec/samsung/gallery/mapfragment/clustering/Options;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;->clusteringOnCameraChangeListener:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;

    .line 342
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$1;

    .prologue
    .line 332
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;-><init>(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;)Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;

    .prologue
    .line 332
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;->clusteringOnCameraChangeListener:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;

    .prologue
    .line 332
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;->handler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public onClusterTransitionFinished()V
    .locals 4

    .prologue
    .line 391
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;->clusterkrafRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    .line 392
    .local v0, "clusterkraf":Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;
    if-eqz v0, :cond_0

    .line 393
    # invokes: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->drawMarkers()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$1200(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)V

    .line 397
    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->transitionsAnimation:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$900(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->onHostPlottedDestinationClusterPoints()V

    .line 399
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;->clusteringOnCameraChangeListener:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;->setDirty(J)V

    .line 400
    return-void
.end method

.method public onClusterTransitionStarted()V
    .locals 2

    .prologue
    .line 377
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;->clusterkrafRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    .line 378
    .local v0, "clusterkraf":Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;
    if-eqz v0, :cond_0

    .line 382
    # invokes: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->removePreviousMarkers()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$1100(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)V

    .line 384
    :cond_0
    return-void
.end method

.method public onClusterTransitionStarting()V
    .locals 4

    .prologue
    .line 369
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;->clusteringOnCameraChangeListener:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;->setDirty(J)V

    .line 370
    return-void
.end method

.method public onClusteringCameraChange()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 348
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;->clusterkrafRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    .line 349
    .local v0, "clusterkraf":Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->hideLocationThumbnail(I)V

    .line 350
    if-eqz v0, :cond_2

    .line 351
    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusteringTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$700(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 352
    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusteringTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$700(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;->cancel()V

    .line 353
    # setter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusteringTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;
    invoke-static {v0, v2}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$702(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;)Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;

    .line 355
    :cond_0
    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusterTransitionsBuildingTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$800(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 356
    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusterTransitionsBuildingTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$800(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;->cancel()V

    .line 357
    # setter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusterTransitionsBuildingTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;
    invoke-static {v0, v2}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$802(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;)Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;

    .line 359
    :cond_1
    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->transitionsAnimation:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$900(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->cancel()V

    .line 360
    # invokes: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->updateClustersAndTransition()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$1000(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)V

    .line 362
    :cond_2
    return-void
.end method

.method public onInfoWindowClick(Lcom/google/android/gms/maps/model/Marker;)V
    .locals 6
    .param p1, "marker"    # Lcom/google/android/gms/maps/model/Marker;

    .prologue
    .line 468
    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;->clusterkrafRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    .line 469
    .local v1, "clusterkraf":Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;
    if-eqz v1, :cond_1

    .line 470
    const/4 v3, 0x0

    .line 471
    .local v3, "handled":Z
    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentClusterPointsByMarker:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$1300()Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    .line 472
    .local v0, "clusterPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$600()Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->getOnInfoWindowClickDownstreamListener()Lcom/sec/samsung/gallery/mapfragment/clustering/OnInfoWindowClickDownstreamListener;

    move-result-object v2

    .line 473
    .local v2, "downstreamListener":Lcom/sec/samsung/gallery/mapfragment/clustering/OnInfoWindowClickDownstreamListener;
    if-eqz v2, :cond_0

    .line 474
    invoke-interface {v2, p1, v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/OnInfoWindowClickDownstreamListener;->onInfoWindowClick(Lcom/google/android/gms/maps/model/Marker;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)Z

    move-result v3

    .line 476
    :cond_0
    if-nez v3, :cond_1

    if-eqz v0, :cond_1

    .line 477
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_2

    .line 478
    sget-object v4, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$3;->$SwitchMap$com$sec$samsung$gallery$mapfragment$clustering$Options$ClusterInfoWindowClickBehavior:[I

    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$600()Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->getClusterInfoWindowClickBehavior()Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 503
    .end local v0    # "clusterPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    .end local v2    # "downstreamListener":Lcom/sec/samsung/gallery/mapfragment/clustering/OnInfoWindowClickDownstreamListener;
    .end local v3    # "handled":Z
    :cond_1
    :goto_0
    return-void

    .line 480
    .restart local v0    # "clusterPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    .restart local v2    # "downstreamListener":Lcom/sec/samsung/gallery/mapfragment/clustering/OnInfoWindowClickDownstreamListener;
    .restart local v3    # "handled":Z
    :pswitch_0
    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->zoomToBounds(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)V

    goto :goto_0

    .line 483
    :pswitch_1
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/Marker;->hideInfoWindow()V

    goto :goto_0

    .line 490
    :cond_2
    sget-object v4, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$3;->$SwitchMap$com$sec$samsung$gallery$mapfragment$clustering$Options$SinglePointInfoWindowClickBehavior:[I

    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$600()Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->getSinglePointInfoWindowClickBehavior()Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    goto :goto_0

    .line 492
    :pswitch_2
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/Marker;->hideInfoWindow()V

    goto :goto_0

    .line 478
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 490
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method public onMarkerClick(Lcom/google/android/gms/maps/model/Marker;)Z
    .locals 11
    .param p1, "marker"    # Lcom/google/android/gms/maps/model/Marker;

    .prologue
    const/4 v8, 0x1

    .line 407
    const/4 v4, 0x0

    .line 408
    .local v4, "handled":Z
    const/4 v3, 0x0

    .line 409
    .local v3, "exempt":Z
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;->clusterkrafRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    .line 410
    .local v1, "clusterkraf":Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;
    if-eqz v1, :cond_3

    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentClusterPointsByMarker:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$1300()Ljava/util/HashMap;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 411
    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentClusterPointsByMarker:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$1300()Ljava/util/HashMap;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    .line 412
    .local v0, "clusterPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    if-nez v0, :cond_0

    .line 413
    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->transitionsAnimation:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;
    invoke-static {v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$900(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;

    move-result-object v7

    invoke-virtual {v7, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->getAnimatedDestinationClusterPoint(Lcom/google/android/gms/maps/model/Marker;)Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    move-result-object v7

    if-eqz v7, :cond_5

    .line 414
    const/4 v3, 0x1

    .line 420
    :cond_0
    :goto_0
    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$600()Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->getOnMarkerClickDownstreamListener()Lcom/sec/samsung/gallery/mapfragment/clustering/OnMarkerClickDownstreamListener;

    move-result-object v2

    .line 421
    .local v2, "downstreamListener":Lcom/sec/samsung/gallery/mapfragment/clustering/OnMarkerClickDownstreamListener;
    if-nez v3, :cond_1

    if-eqz v2, :cond_1

    .line 422
    invoke-interface {v2, p1, v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/OnMarkerClickDownstreamListener;->onMarkerClick(Lcom/google/android/gms/maps/model/Marker;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)Z

    move-result v4

    .line 424
    :cond_1
    if-nez v3, :cond_2

    if-nez v4, :cond_2

    if-eqz v0, :cond_2

    .line 425
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->size()I

    move-result v7

    if-le v7, v8, :cond_6

    .line 426
    sget-object v7, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$3;->$SwitchMap$com$sec$samsung$gallery$mapfragment$clustering$Options$ClusterClickBehavior:[I

    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$600()Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->getClusterClickBehavior()Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterClickBehavior;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterClickBehavior;->ordinal()I

    move-result v9

    aget v7, v7, v9

    packed-switch v7, :pswitch_data_0

    .line 452
    :cond_2
    :goto_1
    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$1400()Landroid/content/Context;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v7

    const/16 v9, 0xa

    invoke-virtual {v7, v9}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 454
    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$1400()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 455
    .local v6, "res":Landroid/content/res/Resources;
    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$600()Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->getMarkerOptionsChooser()Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;

    .line 456
    .local v5, "moc":Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;
    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$1400()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f0203a2

    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentClusterPointsByMarker:Ljava/util/HashMap;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$1300()Ljava/util/HashMap;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    invoke-virtual {v5, v9, v6, v10, v7}, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->getClusterBitmap(Landroid/content/Context;Landroid/content/res/Resources;ILcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->fromBitmap(Landroid/graphics/Bitmap;)Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v7

    invoke-virtual {p1, v7}, Lcom/google/android/gms/maps/model/Marker;->setIcon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)V

    .line 458
    # invokes: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->showMarkerThumbnail(Lcom/google/android/gms/maps/model/Marker;)V
    invoke-static {v1, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$1500(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;Lcom/google/android/gms/maps/model/Marker;)V

    .line 460
    .end local v0    # "clusterPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    .end local v2    # "downstreamListener":Lcom/sec/samsung/gallery/mapfragment/clustering/OnMarkerClickDownstreamListener;
    .end local v5    # "moc":Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;
    .end local v6    # "res":Landroid/content/res/Resources;
    :cond_3
    if-nez v4, :cond_4

    if-eqz v3, :cond_7

    :cond_4
    move v7, v8

    :goto_2
    return v7

    .line 417
    .restart local v0    # "clusterPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    :cond_5
    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->transitionsAnimation:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;
    invoke-static {v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$900(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;

    move-result-object v7

    invoke-virtual {v7, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->getStationaryClusterPoint(Lcom/google/android/gms/maps/model/Marker;)Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    move-result-object v0

    goto :goto_0

    .line 429
    .restart local v2    # "downstreamListener":Lcom/sec/samsung/gallery/mapfragment/clustering/OnMarkerClickDownstreamListener;
    :pswitch_0
    const/4 v4, 0x1

    .line 430
    goto :goto_1

    .line 433
    :pswitch_1
    const/4 v4, 0x1

    .line 434
    goto :goto_1

    .line 440
    :cond_6
    sget-object v7, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$3;->$SwitchMap$com$sec$samsung$gallery$mapfragment$clustering$Options$SinglePointClickBehavior:[I

    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$600()Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->getSinglePointClickBehavior()Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;->ordinal()I

    move-result v9

    aget v7, v7, v9

    packed-switch v7, :pswitch_data_1

    goto :goto_1

    .line 443
    :pswitch_2
    const/4 v4, 0x1

    .line 444
    goto :goto_1

    .line 460
    .end local v0    # "clusterPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    .end local v2    # "downstreamListener":Lcom/sec/samsung/gallery/mapfragment/clustering/OnMarkerClickDownstreamListener;
    :cond_7
    const/4 v7, 0x0

    goto :goto_2

    .line 426
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 440
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method
