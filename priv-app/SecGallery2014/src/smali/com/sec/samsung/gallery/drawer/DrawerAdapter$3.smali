.class Lcom/sec/samsung/gallery/drawer/DrawerAdapter$3;
.super Ljava/lang/Object;
.source "DrawerAdapter.java"

# interfaces
.implements Lcom/sec/samsung/gallery/drawer/IDrawerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/drawer/DrawerAdapter;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$3;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdate()V
    .locals 3

    .prologue
    .line 89
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$3;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupRemote:Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;
    invoke-static {v2}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$300(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->getGroupForDevices()Lcom/sec/samsung/gallery/drawer/Group;

    move-result-object v0

    .line 90
    .local v0, "groupForDevices":Lcom/sec/samsung/gallery/drawer/Group;
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$3;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupRemote:Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;
    invoke-static {v2}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$300(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->getGroupForNearby()Lcom/sec/samsung/gallery/drawer/Group;

    move-result-object v1

    .line 92
    .local v1, "groupForNearby":Lcom/sec/samsung/gallery/drawer/Group;
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    .line 93
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/Group;->getChildrenCount()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$3;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$100(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 94
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$3;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$100(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 95
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$3;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$100(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 96
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$3;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$100(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$3;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$100(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    :cond_0
    :goto_0
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/Group;->getChildrenCount()I

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$3;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$100(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 103
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$3;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$100(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 106
    :cond_1
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/Group;->getChildrenCount()I

    move-result v2

    if-lez v2, :cond_2

    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$3;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$100(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 107
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$3;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$100(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    :cond_2
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/Group;->getChildrenCount()I

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$3;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$100(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 111
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$3;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$100(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 114
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$3;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->notifyDataSetChanged()V

    .line 115
    return-void

    .line 99
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$3;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$100(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
