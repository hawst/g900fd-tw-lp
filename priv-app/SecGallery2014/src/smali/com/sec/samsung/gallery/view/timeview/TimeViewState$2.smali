.class Lcom/sec/samsung/gallery/view/timeview/TimeViewState$2;
.super Lorg/puremvc/java/patterns/mediator/Mediator;
.source "TimeViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/timeview/TimeViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/Object;

    .prologue
    .line 219
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$2;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    invoke-direct {p0, p2, p3}, Lorg/puremvc/java/patterns/mediator/Mediator;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public handleNotification(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 9
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const/4 v8, 0x0

    .line 235
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getName()Ljava/lang/String;

    move-result-object v3

    .line 236
    .local v3, "notiName":Ljava/lang/String;
    const-string v7, "VIEW_MODE_SWITCH"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 272
    :cond_0
    :goto_0
    return-void

    .line 238
    :cond_1
    const-string v7, "UPDATE_CONFIRM_MENU"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 239
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$2;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # invokes: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->updateConfirmMenu()V
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$200(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    goto :goto_0

    .line 240
    :cond_2
    const-string v7, "PREPARE_SLIDE_SHOW_DATA"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 241
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$2;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # invokes: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->addToSildeProxy()V
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$300(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    goto :goto_0

    .line 242
    :cond_3
    const-string v7, "VIEW_BY_TYPE_UPDATED"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 243
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlexibleCloudAlbumFormat:Z

    if-eqz v7, :cond_4

    .line 244
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$2;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$400(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v6

    .line 245
    .local v6, "viewType":I
    sget-object v7, Lcom/sec/samsung/gallery/core/ViewByFilterType;->CLOUDALBUM_FORMAT:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v7, v6}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 246
    invoke-static {v8}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->changeCloudAlbumFormat(Z)V

    .line 251
    .end local v6    # "viewType":I
    :cond_4
    :goto_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$2;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->handleFilter()V

    .line 252
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$2;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->getActionBarManager()Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v0

    .line 253
    .local v0, "actionBar":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    instance-of v7, v0, Lcom/sec/samsung/gallery/view/AlbumTabBar;

    if-eqz v7, :cond_0

    .line 254
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$2;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$500(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v1

    .line 255
    .local v1, "currentViewByType":I
    check-cast v0, Lcom/sec/samsung/gallery/view/AlbumTabBar;

    .end local v0    # "actionBar":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->changeViewByType(I)V

    goto :goto_0

    .line 248
    .end local v1    # "currentViewByType":I
    .restart local v6    # "viewType":I
    :cond_5
    const/4 v7, 0x1

    invoke-static {v7}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->changeCloudAlbumFormat(Z)V

    goto :goto_1

    .line 257
    .end local v6    # "viewType":I
    :cond_6
    const-string v7, "SECRET_MODE_CHANGED"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 258
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$2;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # invokes: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->updateSelectionMode()V
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$600(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    .line 259
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$2;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # invokes: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->onTabChanged()V
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$700(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    goto :goto_0

    .line 260
    :cond_7
    const-string v7, "DOWNLOAD_NEARBY"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 262
    :try_start_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$2;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # invokes: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->downloadNearby(Lorg/puremvc/java/interfaces/INotification;)V
    invoke-static {v7, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$800(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Lorg/puremvc/java/interfaces/INotification;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 263
    :catch_0
    move-exception v2

    .line 264
    .local v2, "e":Ljava/lang/NullPointerException;
    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$900()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 266
    .end local v2    # "e":Ljava/lang/NullPointerException;
    :cond_8
    const-string v7, "DOWNLOAD_CLOUDBY"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 267
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/Object;

    move-object v5, v7

    check-cast v5, [Ljava/lang/Object;

    .line 269
    .local v5, "params":[Ljava/lang/Object;
    aget-object v4, v5, v8

    check-cast v4, Ljava/util/ArrayList;

    .line 270
    .local v4, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$2;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$1000(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v7

    invoke-static {v7, v4}, Lcom/sec/samsung/gallery/util/DownloadUtil;->downloadEnqueue(Landroid/content/Context;Ljava/util/ArrayList;)V

    goto/16 :goto_0
.end method

.method public listNotificationInterests()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 222
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "VIEW_MODE_SWITCH"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "UPDATE_CONFIRM_MENU"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "PREPARE_SLIDE_SHOW_DATA"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "VIEW_BY_TYPE_UPDATED"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "DOWNLOAD_NEARBY"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "DOWNLOAD_CLOUDBY"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "SECRET_MODE_CHANGED"

    aput-object v2, v0, v1

    return-object v0
.end method
