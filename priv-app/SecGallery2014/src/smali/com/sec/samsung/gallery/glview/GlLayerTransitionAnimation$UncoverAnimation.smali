.class Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$UncoverAnimation;
.super Ljava/lang/Object;
.source "GlLayerTransitionAnimation.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$IApplyTransform;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UncoverAnimation"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$UncoverAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$1;

    .prologue
    .line 207
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$UncoverAnimation;-><init>(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)V

    return-void
.end method


# virtual methods
.method public applyTransform(F)V
    .locals 9
    .param p1, "ratio"    # F

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    .line 210
    sub-float v1, v8, p1

    .line 211
    .local v1, "inRatio":F
    mul-float v4, v1, v1

    sub-float v3, v8, v4

    .line 215
    .local v3, "proRatio":F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$UncoverAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldCount:I
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$500(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 216
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$UncoverAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldObjList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$600(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 217
    .local v2, "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$UncoverAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldAlphaHnd:[I
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$700(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)[I

    move-result-object v4

    aget v4, v4, v0

    invoke-virtual {v2, v1, v4}, Lcom/sec/android/gallery3d/glcore/GlObject;->setAlpha(FI)V

    .line 218
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$UncoverAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldObjHnd:[I
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$800(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)[I

    move-result-object v4

    aget v4, v4, v0

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$UncoverAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mDeltaX:F
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$400(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)F

    move-result v5

    mul-float/2addr v5, v3

    invoke-virtual {v2, v4, v5, v7, v7}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPos(IFFF)V

    .line 215
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 220
    .end local v2    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$UncoverAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewCount:I
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$900(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 221
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$UncoverAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewObjList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$1000(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 222
    .restart local v2    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$UncoverAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewAlphaHnd:[I
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$1100(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)[I

    move-result-object v4

    aget v4, v4, v0

    invoke-virtual {v2, p1, v4}, Lcom/sec/android/gallery3d/glcore/GlObject;->setAlpha(FI)V

    .line 223
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$UncoverAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewObjHnd:[I
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$1200(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)[I

    move-result-object v4

    aget v4, v4, v0

    const/high16 v5, -0x3c6a0000    # -300.0f

    sub-float v6, v8, v3

    mul-float/2addr v5, v6

    invoke-virtual {v2, v4, v7, v7, v5}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPos(IFFF)V

    .line 220
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 225
    .end local v2    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_1
    return-void
.end method
