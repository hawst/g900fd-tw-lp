.class public Lcom/sec/samsung/gallery/util/SettingsUtil;
.super Ljava/lang/Object;
.source "SettingsUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAirButton(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    const-string v0, "air_button_onoff"

    invoke-static {p0, v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getAirMotionEngine(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    const-string v0, "air_motion_engine"

    invoke-static {p0, v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getAirMotionScroll(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    const-string v0, "air_motion_scroll"

    invoke-static {p0, v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getAirMotionScrollAlbumAndPhoto(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    const-string v0, "air_motion_scroll_album_and_photo"

    invoke-static {p0, v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getAirMotionTurn(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    const-string v0, "air_motion_turn"

    invoke-static {p0, v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getAirMotionTurnSinglePhotoView(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const-string v0, "air_motion_turn_single_photo_view"

    invoke-static {p0, v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getAirViewFeedback(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 106
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v4, "air_view_mode"

    invoke-static {v2, v4, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 109
    .local v0, "airViewMode":I
    invoke-static {p0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getFingerAirView(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    if-ne v0, v5, :cond_1

    .line 110
    :cond_0
    const-string v2, "finger_air_view_sound_and_haptic_feedback"

    invoke-static {p0, v2}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    .line 125
    :goto_0
    return v2

    .line 111
    :cond_1
    if-nez v0, :cond_2

    .line 113
    const-string/jumbo v2, "spen_feedback_sound_air_view"

    invoke-static {p0, v2}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    goto :goto_0

    .line 114
    :cond_2
    if-ne v0, v6, :cond_5

    move-object v2, p0

    .line 115
    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGlRootView()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getToolType()I

    move-result v1

    .line 116
    .local v1, "toolType":I
    if-ne v1, v6, :cond_3

    .line 118
    const-string/jumbo v2, "spen_feedback_sound_air_view"

    invoke-static {p0, v2}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    goto :goto_0

    .line 119
    :cond_3
    if-ne v1, v5, :cond_4

    .line 120
    const-string v2, "finger_air_view_sound_and_haptic_feedback"

    invoke-static {p0, v2}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    goto :goto_0

    :cond_4
    move v2, v3

    .line 122
    goto :goto_0

    .line 125
    .end local v1    # "toolType":I
    :cond_5
    const-string v2, "finger_air_view_sound_and_haptic_feedback"

    invoke-static {p0, v2}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    goto :goto_0
.end method

.method private static getBool(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "item"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 100
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 101
    .local v0, "cr":Landroid/content/ContentResolver;
    invoke-static {v0, p1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public static getEasyMode(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 79
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "easy_mode_switch"

    invoke-static {v3, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "easy_mode_gallery"

    invoke-static {v3, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_3

    move v0, v1

    .line 81
    .local v0, "result":Z
    :goto_0
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOldEasyUx:Z

    if-ne v3, v1, :cond_0

    .line 82
    const/4 v0, 0x0

    .line 84
    :cond_0
    if-eqz v0, :cond_4

    .line 85
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFaceTagAvailable(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 86
    invoke-static {p0, v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setFaceTagStatusForNormalMode(Landroid/content/Context;Z)V

    .line 88
    :cond_1
    invoke-static {p0, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setFaceTagStatus(Landroid/content/Context;Z)V

    .line 95
    :cond_2
    :goto_1
    return v0

    .end local v0    # "result":Z
    :cond_3
    move v0, v2

    .line 79
    goto :goto_0

    .line 90
    .restart local v0    # "result":Z
    :cond_4
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getFaceTagStatusForNormalMode(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 91
    invoke-static {p0, v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setFaceTagStatus(Landroid/content/Context;Z)V

    .line 92
    invoke-static {p0, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setFaceTagStatusForNormalMode(Landroid/content/Context;Z)V

    goto :goto_1
.end method

.method public static getFingerAirView(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    const-string v0, "finger_air_view"

    invoke-static {p0, v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getFingerAirViewInformationPreview(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    const-string v0, "finger_air_view_information_preview"

    invoke-static {p0, v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getHapticExtraFeedback(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 153
    const-string v0, "haptic_feedback_extra"

    invoke-static {p0, v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getModeAirViewSoundAndHaptic(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 130
    const/4 v1, 0x0

    .line 131
    .local v1, "supportSound":Z
    const/4 v0, 0x0

    .line 133
    .local v0, "supportHaptic":Z
    invoke-static {p0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getAirViewFeedback(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 134
    const/4 v1, 0x1

    .line 138
    :cond_0
    const-string/jumbo v2, "spen_feedback_haptic"

    invoke-static {p0, v2}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 140
    const-string/jumbo v2, "spen_feedback_haptic_air_view"

    invoke-static {p0, v2}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 143
    :cond_1
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 144
    const/4 v2, 0x1

    .line 149
    :goto_0
    return v2

    .line 145
    :cond_2
    if-eqz v1, :cond_3

    if-nez v0, :cond_3

    .line 146
    const/4 v2, 0x2

    goto :goto_0

    .line 147
    :cond_3
    if-nez v1, :cond_4

    if-eqz v0, :cond_4

    .line 148
    const/4 v2, 0x3

    goto :goto_0

    .line 149
    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getMouseHovering(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    const-string v0, "mouse_hovering"

    invoke-static {p0, v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getMouseHoveringInformationPreview(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    const-string v0, "mouse_hovering_information_preview"

    invoke-static {p0, v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getMouseHoveringListScroll(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    const-string v0, "mouse_hovering_list_scroll"

    invoke-static {p0, v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getPenHovering(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    const-string v0, "pen_hovering"

    invoke-static {p0, v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getPenHoveringIconLabels(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    const-string v0, "pen_hovering_icon_label"

    invoke-static {p0, v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getPenHoveringInformationPreview(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    const-string v0, "pen_hovering_information_preview"

    invoke-static {p0, v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getPenHoveringListScroll(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    const-string v0, "pen_hovering_list_scroll"

    invoke-static {p0, v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getBool(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
