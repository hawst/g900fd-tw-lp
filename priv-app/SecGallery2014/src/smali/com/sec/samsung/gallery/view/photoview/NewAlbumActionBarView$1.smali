.class Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView$1;
.super Ljava/lang/Object;
.source "NewAlbumActionBarView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->setTitle(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;

.field final synthetic val$numberOfItemsSelected:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;I)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;

    iput p2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView$1;->val$numberOfItemsSelected:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 63
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->access$000(Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->access$100(Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    const/4 v1, 0x0

    .line 66
    .local v1, "res":Landroid/content/res/Resources;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->access$200(Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 68
    const/4 v0, 0x0

    .line 69
    .local v0, "numberOfSelectedItems":Ljava/lang/String;
    iget v2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView$1;->val$numberOfItemsSelected:I

    if-ne v2, v3, :cond_2

    .line 70
    const v2, 0x7f0e004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView$1;->val$numberOfItemsSelected:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 74
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;

    iget v3, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView$1;->val$numberOfItemsSelected:I

    # invokes: Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->setSelectedItemCount(I)V
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->access$300(Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;I)V

    .line 75
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;

    iget v3, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView$1;->val$numberOfItemsSelected:I

    # invokes: Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->setSelectAllButtonTitle(ILjava/lang/String;)V
    invoke-static {v2, v3, v0}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->access$400(Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;ILjava/lang/String;)V

    goto :goto_0

    .line 72
    :cond_2
    const v2, 0x7f0e004e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView$1;->val$numberOfItemsSelected:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
