.class Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$1;
.super Ljava/lang/Object;
.source "CameraSearchActionBar.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 33
    const/16 v1, 0x11

    .line 34
    .local v1, "flags":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->access$000(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ActionBar;->getDisplayOptions()I

    move-result v3

    or-int v0, v3, v1

    .line 35
    .local v0, "change":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->access$100(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 36
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->access$200(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030008

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 38
    .local v2, "view":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->access$300(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 39
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->access$400(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)Landroid/app/ActionBar;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 40
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->access$500(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ActionBar;->removeAllTabs()V

    .line 41
    return-void
.end method
