.class Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;
.super Ljava/lang/Object;
.source "ShowCopyMoveDialogCmd.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/OnProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->handleMultipleFileOperation(Ljava/lang/String;IILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mErrorReason:Ljava/lang/String;

.field private mMediaSize:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

.field final synthetic val$count:I

.field final synthetic val$dataProxy:Lcom/sec/android/gallery3d/data/DataManager;

.field final synthetic val$dstAlbumPath:Ljava/lang/String;

.field final synthetic val$newFileName:Ljava/lang/String;

.field final synthetic val$operationId:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;ILjava/lang/String;ILcom/sec/android/gallery3d/data/DataManager;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 342
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    iput p2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$operationId:I

    iput-object p3, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$dstAlbumPath:Ljava/lang/String;

    iput p4, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$count:I

    iput-object p5, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$dataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iput-object p6, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$newFileName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 343
    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->mErrorReason:Ljava/lang/String;

    .line 344
    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->mMediaSize:Ljava/util/HashMap;

    return-void
.end method

.method private operation(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 14
    .param p1, "object"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 464
    const/4 v4, 0x0

    .line 465
    .local v4, "result":Z
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->mMediaSize:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->hashCode()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 466
    .local v0, "fileSize":J
    new-instance v5, Landroid/os/StatFs;

    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$dstAlbumPath:Ljava/lang/String;

    invoke-direct {v5, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 467
    .local v5, "statFs":Landroid/os/StatFs;
    invoke-virtual {v5}, Landroid/os/StatFs;->getFreeBlocks()I

    move-result v6

    int-to-long v10, v6

    invoke-virtual {v5}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v12, v6

    mul-long v2, v10, v12

    .line 468
    .local v2, "freeSize":J
    cmp-long v6, v0, v2

    if-lez v6, :cond_0

    .line 469
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0e0139

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->mErrorReason:Ljava/lang/String;

    move v6, v8

    .line 477
    :goto_0
    return v6

    .line 472
    :cond_0
    iget v6, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$operationId:I

    if-nez v6, :cond_2

    .line 473
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$dataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v10, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$dstAlbumPath:Ljava/lang/String;

    iget v6, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$count:I

    if-ne v6, v8, :cond_1

    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$newFileName:Ljava/lang/String;

    :goto_1
    invoke-virtual {v9, p1, v10, v6}, Lcom/sec/android/gallery3d/data/DataManager;->copyItem(Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    :goto_2
    move v6, v4

    .line 477
    goto :goto_0

    :cond_1
    move-object v6, v7

    .line 473
    goto :goto_1

    .line 475
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$dataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$dstAlbumPath:Ljava/lang/String;

    iget v10, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$count:I

    if-ne v10, v8, :cond_3

    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$newFileName:Ljava/lang/String;

    :cond_3
    invoke-virtual {v6, p1, v9, v7}, Lcom/sec/android/gallery3d/data/DataManager;->moveItem(Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    goto :goto_2
.end method


# virtual methods
.method public handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 14
    .param p1, "object"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    const/4 v9, 0x0

    .line 424
    const/4 v6, 0x0

    .line 425
    .local v6, "result":Z
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->mMediaSize:Ljava/util/HashMap;

    if-nez v8, :cond_1

    .line 426
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;
    invoke-static {v8}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$300(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Lcom/sec/samsung/gallery/util/FileUtil;

    move-result-object v8

    if-nez v8, :cond_0

    .line 427
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    new-instance v10, Lcom/sec/samsung/gallery/util/FileUtil;

    iget-object v11, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v11

    invoke-direct {v10, v11}, Lcom/sec/samsung/gallery/util/FileUtil;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;
    invoke-static {v8, v10}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$302(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;Lcom/sec/samsung/gallery/util/FileUtil;)Lcom/sec/samsung/gallery/util/FileUtil;

    .line 429
    :cond_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;
    invoke-static {v8}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$300(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Lcom/sec/samsung/gallery/util/FileUtil;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/FileUtil;->getSelectedMediaSize()Ljava/util/HashMap;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->mMediaSize:Ljava/util/HashMap;

    .line 430
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v8}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$200(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v10

    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->mMediaSize:Ljava/util/HashMap;

    sget-object v11, Lcom/sec/samsung/gallery/util/FileUtil;->TOTAL_FILE_SIZE_KEY:Ljava/lang/Integer;

    invoke-virtual {v8, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-virtual {v10, v12, v13}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalSize(J)V

    .line 434
    :cond_1
    :try_start_0
    instance-of v8, p1, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v8, :cond_4

    .line 435
    move-object v0, p1

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    move-object v7, v0

    .line 436
    .local v7, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v5, 0x0

    .line 437
    .local v5, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    sget-object v10, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v10
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 438
    const/4 v8, 0x0

    :try_start_1
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v11

    invoke-virtual {v7, v8, v11}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v5

    .line 439
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 440
    :try_start_2
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 441
    .local v4, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->operation(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result v6

    .line 442
    if-nez v6, :cond_2

    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v5    # "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v7    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_3
    :goto_0
    move v8, v6

    .line 454
    :goto_1
    return v8

    .line 439
    .restart local v5    # "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .restart local v7    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :catchall_0
    move-exception v8

    :try_start_3
    monitor-exit v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v8
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 449
    .end local v5    # "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v7    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :catch_0
    move-exception v2

    .line 450
    .local v2, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v2}, Ljava/lang/NullPointerException;->printStackTrace()V

    move v8, v9

    .line 451
    goto :goto_1

    .line 446
    .end local v2    # "e":Ljava/lang/NullPointerException;
    :cond_4
    :try_start_5
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->operation(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    move-result v6

    goto :goto_0

    .line 452
    :catch_1
    move-exception v2

    .line 453
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    move v8, v9

    .line 454
    goto :goto_1
.end method

.method public handleOperation(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 460
    .local p1, "objectList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public onCompleted(Z)V
    .locals 12
    .param p1, "result"    # Z

    .prologue
    const v11, 0x7f0e013b

    const/4 v10, 0x3

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 347
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$200(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->closeCustomProgressDialog()V

    .line 348
    iput-object v8, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->mMediaSize:Ljava/util/HashMap;

    .line 349
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # setter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;
    invoke-static {v2, v8}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$302(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;Lcom/sec/samsung/gallery/util/FileUtil;)Lcom/sec/samsung/gallery/util/FileUtil;

    .line 350
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # setter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;
    invoke-static {v2, v8}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$402(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;Lcom/sec/android/gallery3d/data/OnProgressListener;)Lcom/sec/android/gallery3d/data/OnProgressListener;

    .line 352
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.MEDIA_SCAN"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "file://"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->EXTERNAL_SD_DIRECTORY:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$500()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v2, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 353
    iget v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$operationId:I

    if-ne v2, v10, :cond_1

    if-eqz p1, :cond_1

    .line 354
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "EXIT_SELECTION_MODE"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 360
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mOperationTask:Lcom/sec/samsung/gallery/util/MediaOperations;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$700(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Lcom/sec/samsung/gallery/util/MediaOperations;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mOperationTask:Lcom/sec/samsung/gallery/util/MediaOperations;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$700(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Lcom/sec/samsung/gallery/util/MediaOperations;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/util/MediaOperations;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 361
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # setter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mOperationTask:Lcom/sec/samsung/gallery/util/MediaOperations;
    invoke-static {v2, v8}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$702(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;Lcom/sec/samsung/gallery/util/MediaOperations;)Lcom/sec/samsung/gallery/util/MediaOperations;

    .line 408
    :cond_0
    :goto_1
    return-void

    .line 357
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "EXIT_SELECTION_MODE"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 364
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # setter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mOperationTask:Lcom/sec/samsung/gallery/util/MediaOperations;
    invoke-static {v2, v8}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$702(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;Lcom/sec/samsung/gallery/util/MediaOperations;)Lcom/sec/samsung/gallery/util/MediaOperations;

    .line 365
    if-nez p1, :cond_5

    .line 366
    const-string v0, ""

    .line 367
    .local v0, "errorMsg":Ljava/lang/String;
    iget v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$operationId:I

    if-ne v2, v7, :cond_3

    .line 368
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mResources:Landroid/content/res/Resources;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$800(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e01c9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 369
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mResources:Landroid/content/res/Resources;
    invoke-static {v4}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$800(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 370
    :cond_3
    iget v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$operationId:I

    if-nez v2, :cond_4

    .line 371
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mResources:Landroid/content/res/Resources;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$800(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e01c8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 372
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mResources:Landroid/content/res/Resources;
    invoke-static {v4}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$800(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 373
    :cond_4
    iget v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$operationId:I

    if-ne v2, v10, :cond_0

    .line 374
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mResources:Landroid/content/res/Resources;
    invoke-static {v3}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$800(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e01ef

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 382
    .end local v0    # "errorMsg":Ljava/lang/String;
    :cond_5
    const-string v1, ""

    .line 383
    .local v1, "resultMsg":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->mErrorReason:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 384
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->mErrorReason:Ljava/lang/String;

    .line 406
    :cond_6
    :goto_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 385
    :cond_7
    iget v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$operationId:I

    if-ne v2, v7, :cond_a

    .line 386
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$dstAlbumPath:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$dstAlbumPath:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getSecretBoxPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 387
    iget v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$count:I

    if-ne v2, v7, :cond_8

    .line 388
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e01e3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 389
    :cond_8
    iget v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$count:I

    if-le v2, v7, :cond_6

    .line 390
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e01e4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    iget v4, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$count:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v9

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 393
    :cond_9
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e01c7

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 395
    :cond_a
    iget v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$operationId:I

    if-ne v2, v10, :cond_d

    .line 396
    iget v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$count:I

    if-ne v2, v7, :cond_b

    .line 397
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mResources:Landroid/content/res/Resources;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$800(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e01e9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 398
    :cond_b
    iget v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$count:I

    if-le v2, v7, :cond_c

    .line 399
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mResources:Landroid/content/res/Resources;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$800(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e01ea

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    iget v4, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->val$count:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v9

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 401
    :cond_c
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e01c7

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 404
    :cond_d
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e01c6

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2
.end method

.method public onProgress(II)V
    .locals 4
    .param p1, "progress"    # I
    .param p2, "flag"    # I

    .prologue
    .line 418
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    const/4 v0, 0x1

    .line 419
    .local v0, "isUpdateFileSize":Z
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd$4;->this$0:Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;->access$200(Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v1

    int-to-long v2, p1

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->increaseProgress(JZ)V

    .line 420
    return-void

    .line 418
    .end local v0    # "isUpdateFileSize":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
