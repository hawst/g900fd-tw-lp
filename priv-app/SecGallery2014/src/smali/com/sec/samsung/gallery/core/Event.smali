.class public Lcom/sec/samsung/gallery/core/Event;
.super Ljava/lang/Object;
.source "Event.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/core/Event$Builder;
    }
.end annotation


# static fields
.field public static final EVENT_ACCOUNT_LOGOUT_NOTI:I

.field public static final EVENT_ACTION_DELETE:I

.field public static final EVENT_ADD_SUGGESTION:I

.field public static final EVENT_BURST_PLAY:I

.field public static final EVENT_BURST_SHOT_PLAY:I

.field public static final EVENT_CHANGE_PLAYER:I

.field public static final EVENT_CONFIRM_LOCATION_SHARE:I

.field public static final EVENT_COPY_FILES:I

.field public static final EVENT_COPY_TO_ALBUM:I

.field public static final EVENT_CREATE_GIF:I

.field public static final EVENT_CREATE_NEW_ALBUM:I

.field public static final EVENT_CREATE_VIDEO_ALBUM:I

.field public static final EVENT_CROP_MEDIA:I

.field public static final EVENT_DELAY_HIDE_BARS:I

.field public static final EVENT_DELETE_CLOUD_MEDIA:I

.field public static final EVENT_DELETE_MEDIA:I

.field public static final EVENT_DELETE_SOUND_IN_PICTURE:I

.field public static final EVENT_DELETE_SOUND_IN_PICTURE_CONFIRM:I

.field public static final EVENT_EDIT_CATEGORY:I

.field public static final EVENT_ENTERKEYWORD_SEARCH:I

.field public static final EVENT_ENTER_CAMERA_MODE:I

.field public static final EVENT_ENTER_DELETE_SELECTION_MODE:I

.field public static final EVENT_ENTER_GALLERY_SEARCH_MODE:I

.field public static final EVENT_ENTER_SEARCH_MODE:I

.field public static final EVENT_ENTER_SELECTION_MODE:I

.field public static final EVENT_ENTER_SELECTION_MODE_FOR_EDIT:I

.field public static final EVENT_ENTER_SELECTION_MODE_FOR_MAKE_COLLAGE:I

.field public static final EVENT_ENTER_SELECTION_MODE_FOR_STORY_ALBUM:I

.field public static final EVENT_EXIT_SELECTION_MODE:I

.field public static final EVENT_FILTER_DATA:I

.field public static final EVENT_GOTO_UP:I

.field public static final EVENT_HIDE_ITEMS_FOR_NORMAL_PHOTO_VIEW:I

.field public static final EVENT_HOME_ICON:I

.field public static final EVENT_IMAGE_NOTE:I

.field public static final EVENT_IMAGE_ROTATION_CCW:I

.field public static final EVENT_IMAGE_ROTATION_CW:I

.field public static final EVENT_IMPORT:I

.field public static final EVENT_LAST_SHARE_APP:I

.field public static final EVENT_LOCK_TOUCH:I

.field public static final EVENT_MAKE_BUSRTSHOT_PICTURE:I

.field public static final EVENT_MAKE_MOTION_PICTURE:I

.field public static final EVENT_MANUAL_DETECT:I

.field public static final EVENT_MOVE_FILES:I

.field public static final EVENT_MOVE_FILES_TO:I

.field public static final EVENT_MOVE_TO_ALBUM:I

.field public static final EVENT_MOVE_TO_SECRETBOX:I

.field public static final EVENT_NEW_ALBUM_CANCEL:I

.field public static final EVENT_NEW_ALBUM_DONE:I

.field public static final EVENT_NEW_EVENT_ALBUM:I

.field public static final EVENT_PLAY_VIDEO:I

.field public static final EVENT_POPUP_DIALOG_VISIBLE:I

.field public static final EVENT_PRESS_SEARCH:I

.field public static final EVENT_PRESS_VOICE_SEARCH:I

.field public static final EVENT_REFRESH_SLINK:I

.field public static final EVENT_REMOVE_CATEGORY:I

.field public static final EVENT_REMOVE_FROM_KNOX:I

.field public static final EVENT_REMOVE_FROM_SECRETBOX:I

.field public static final EVENT_RENAME_MEDIA:I

.field public static final EVENT_REORDER_ALBUMS:I

.field public static final EVENT_ROTATE_MEDIA:I

.field public static final EVENT_SCAN_NEARBY_DEVICES:I

.field public static final EVENT_SEARCH_FIELD_EMPTY:I

.field public static final EVENT_SELECT_ALL:I

.field public static final EVENT_SEND_TO_OTHER_DEVICES:I

.field public static final EVENT_SHOW_DELETE_DIALOG:I

.field public static final EVENT_SHOW_HIDDEN_ALBUMS:I

.field public static final EVENT_SHOW_NEW_ALBUM_NAME_EDIT_DIALOG:I

.field public static final EVENT_SHOW_NEXT_IMAGE:I

.field public static final EVENT_SHOW_ON_MAP:I

.field public static final EVENT_SHOW_PREVIOUS_IMAGE:I

.field public static final EVENT_SHOW_SLIDESHOW_SETTING:I

.field public static final EVENT_SORT_BY_LATEST:I

.field public static final EVENT_SORT_BY_OLDEST:I

.field public static final EVENT_START_NAVIGATION:I

.field public static final EVENT_START_NEW_ALBUM:I

.field public static final EVENT_START_SELECTED_APP:I

.field public static final EVENT_STOP_SLIDESHOW:I

.field public static final EVENT_TAG_LIST_UPDATE_SEARCH:I

.field public static final EVENT_UNSELECT_ALL:I

.field private static index:I


# instance fields
.field private mData:Ljava/lang/Object;

.field private mIntVal:I

.field private mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    const/4 v0, 0x0

    sput v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    .line 21
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_EXIT_SELECTION_MODE:I

    .line 23
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_DELETE_DIALOG:I

    .line 25
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_NEW_ALBUM_NAME_EDIT_DIALOG:I

    .line 27
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_SELECT_ALL:I

    .line 29
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_UNSELECT_ALL:I

    .line 31
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_START_SELECTED_APP:I

    .line 33
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELETE_MEDIA:I

    .line 35
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELETE_CLOUD_MEDIA:I

    .line 37
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_CREATE_NEW_ALBUM:I

    .line 39
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_RENAME_MEDIA:I

    .line 41
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_COPY_FILES:I

    .line 43
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_MOVE_FILES:I

    .line 45
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_MOVE_FILES_TO:I

    .line 47
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_NEW_ALBUM_DONE:I

    .line 49
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_NEW_ALBUM_CANCEL:I

    .line 51
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_FILTER_DATA:I

    .line 53
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_START_NEW_ALBUM:I

    .line 55
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_SLIDESHOW_SETTING:I

    .line 57
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SELECTION_MODE:I

    .line 59
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_HIDDEN_ALBUMS:I

    .line 61
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_CAMERA_MODE:I

    .line 63
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SEARCH_MODE:I

    .line 65
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_PRESS_SEARCH:I

    .line 67
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTERKEYWORD_SEARCH:I

    .line 69
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_GALLERY_SEARCH_MODE:I

    .line 71
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_PRESS_VOICE_SEARCH:I

    .line 73
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_ADD_SUGGESTION:I

    .line 75
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_IMAGE_ROTATION_CW:I

    .line 77
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_IMAGE_ROTATION_CCW:I

    .line 79
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_NEXT_IMAGE:I

    .line 81
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_PREVIOUS_IMAGE:I

    .line 83
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_BURST_SHOT_PLAY:I

    .line 85
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_GOTO_UP:I

    .line 87
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_CROP_MEDIA:I

    .line 89
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_HOME_ICON:I

    .line 91
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_ROTATE_MEDIA:I

    .line 93
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_SCAN_NEARBY_DEVICES:I

    .line 95
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_BURST_PLAY:I

    .line 97
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELAY_HIDE_BARS:I

    .line 99
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_POPUP_DIALOG_VISIBLE:I

    .line 101
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_ON_MAP:I

    .line 103
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_START_NAVIGATION:I

    .line 105
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_IMAGE_NOTE:I

    .line 107
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_CHANGE_PLAYER:I

    .line 109
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_SEARCH_FIELD_EMPTY:I

    .line 111
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_STOP_SLIDESHOW:I

    .line 113
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_IMPORT:I

    .line 115
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_CONFIRM_LOCATION_SHARE:I

    .line 117
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_PLAY_VIDEO:I

    .line 119
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_LAST_SHARE_APP:I

    .line 121
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SELECTION_MODE_FOR_STORY_ALBUM:I

    .line 123
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SELECTION_MODE_FOR_EDIT:I

    .line 125
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SELECTION_MODE_FOR_MAKE_COLLAGE:I

    .line 127
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_SORT_BY_OLDEST:I

    .line 129
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_SORT_BY_LATEST:I

    .line 131
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_MAKE_MOTION_PICTURE:I

    .line 133
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_MOVE_TO_SECRETBOX:I

    .line 135
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_FROM_SECRETBOX:I

    .line 137
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_CREATE_VIDEO_ALBUM:I

    .line 139
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_HIDE_ITEMS_FOR_NORMAL_PHOTO_VIEW:I

    .line 141
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_ACCOUNT_LOGOUT_NOTI:I

    .line 143
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_LOCK_TOUCH:I

    .line 145
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_MAKE_BUSRTSHOT_PICTURE:I

    .line 147
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_MANUAL_DETECT:I

    .line 149
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_FROM_KNOX:I

    .line 151
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_REORDER_ALBUMS:I

    .line 153
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_NEW_EVENT_ALBUM:I

    .line 155
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_TAG_LIST_UPDATE_SEARCH:I

    .line 157
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_SEND_TO_OTHER_DEVICES:I

    .line 159
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_CATEGORY:I

    .line 161
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_EDIT_CATEGORY:I

    .line 163
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_ACTION_DELETE:I

    .line 165
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_COPY_TO_ALBUM:I

    .line 167
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_MOVE_TO_ALBUM:I

    .line 169
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELETE_SOUND_IN_PICTURE:I

    .line 171
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_REFRESH_SLINK:I

    .line 173
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_DELETE_SELECTION_MODE:I

    .line 175
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_CREATE_GIF:I

    .line 177
    sget v0, Lcom/sec/samsung/gallery/core/Event;->index:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/samsung/gallery/core/Event;->index:I

    sput v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELETE_SOUND_IN_PICTURE_CONFIRM:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    return-void
.end method


# virtual methods
.method public getData()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/Event;->mData:Ljava/lang/Object;

    return-object v0
.end method

.method public getIntData()I
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Lcom/sec/samsung/gallery/core/Event;->mIntVal:I

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lcom/sec/samsung/gallery/core/Event;->mType:I

    return v0
.end method

.method public setData(Ljava/lang/Object;)Lcom/sec/samsung/gallery/core/Event;
    .locals 0
    .param p1, "eventData"    # Ljava/lang/Object;

    .prologue
    .line 197
    iput-object p1, p0, Lcom/sec/samsung/gallery/core/Event;->mData:Ljava/lang/Object;

    .line 198
    return-object p0
.end method

.method public setIntData(I)Lcom/sec/samsung/gallery/core/Event;
    .locals 0
    .param p1, "intVal"    # I

    .prologue
    .line 202
    iput p1, p0, Lcom/sec/samsung/gallery/core/Event;->mIntVal:I

    .line 203
    return-object p0
.end method

.method public setType(I)Lcom/sec/samsung/gallery/core/Event;
    .locals 0
    .param p1, "eventType"    # I

    .prologue
    .line 192
    iput p1, p0, Lcom/sec/samsung/gallery/core/Event;->mType:I

    .line 193
    return-object p0
.end method
