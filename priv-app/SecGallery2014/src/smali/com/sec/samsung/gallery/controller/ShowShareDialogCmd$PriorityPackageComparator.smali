.class Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd$PriorityPackageComparator;
.super Ljava/lang/Object;
.source "ShowShareDialogCmd.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PriorityPackageComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/content/pm/ResolveInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private mComparator:Landroid/content/pm/ResolveInfo$DisplayNameComparator;

.field private final mPrioritySharePkg_ChatON:Ljava/lang/String;

.field private final mPrioritySharePkg_GroupPlay:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;Landroid/content/pm/PackageManager;)V
    .locals 1
    .param p2, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd$PriorityPackageComparator;->this$0:Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;

    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207
    const-string v0, "com.sec.chaton"

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd$PriorityPackageComparator;->mPrioritySharePkg_ChatON:Ljava/lang/String;

    .line 208
    const-string v0, "com.samsung.groupcast"

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd$PriorityPackageComparator;->mPrioritySharePkg_GroupPlay:Ljava/lang/String;

    .line 213
    new-instance v0, Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    invoke-direct {v0, p2}, Landroid/content/pm/ResolveInfo$DisplayNameComparator;-><init>(Landroid/content/pm/PackageManager;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd$PriorityPackageComparator;->mComparator:Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    .line 214
    return-void
.end method

.method private isPrioritySharePkg(Landroid/content/pm/ResolveInfo;)Z
    .locals 3
    .param p1, "info"    # Landroid/content/pm/ResolveInfo;

    .prologue
    const/4 v0, 0x1

    .line 230
    iget-object v1, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    const-string v2, "com.sec.chaton"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 235
    :cond_0
    :goto_0
    return v0

    .line 232
    :cond_1
    iget-object v1, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    const-string v2, "com.samsung.groupcast"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 235
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public compare(Landroid/content/pm/ResolveInfo;Landroid/content/pm/ResolveInfo;)I
    .locals 1
    .param p1, "info0"    # Landroid/content/pm/ResolveInfo;
    .param p2, "info1"    # Landroid/content/pm/ResolveInfo;

    .prologue
    .line 218
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd$PriorityPackageComparator;->isPrioritySharePkg(Landroid/content/pm/ResolveInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p2}, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd$PriorityPackageComparator;->isPrioritySharePkg(Landroid/content/pm/ResolveInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd$PriorityPackageComparator;->mComparator:Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    invoke-virtual {v0, p1, p2}, Landroid/content/pm/ResolveInfo$DisplayNameComparator;->compare(Landroid/content/pm/ResolveInfo;Landroid/content/pm/ResolveInfo;)I

    move-result v0

    .line 225
    :goto_0
    return v0

    .line 220
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd$PriorityPackageComparator;->isPrioritySharePkg(Landroid/content/pm/ResolveInfo;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p2}, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd$PriorityPackageComparator;->isPrioritySharePkg(Landroid/content/pm/ResolveInfo;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 221
    const/4 v0, -0x1

    goto :goto_0

    .line 222
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd$PriorityPackageComparator;->isPrioritySharePkg(Landroid/content/pm/ResolveInfo;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, p2}, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd$PriorityPackageComparator;->isPrioritySharePkg(Landroid/content/pm/ResolveInfo;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 223
    const/4 v0, 0x1

    goto :goto_0

    .line 225
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd$PriorityPackageComparator;->mComparator:Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    invoke-virtual {v0, p1, p2}, Landroid/content/pm/ResolveInfo$DisplayNameComparator;->compare(Landroid/content/pm/ResolveInfo;Landroid/content/pm/ResolveInfo;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 206
    check-cast p1, Landroid/content/pm/ResolveInfo;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/content/pm/ResolveInfo;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd$PriorityPackageComparator;->compare(Landroid/content/pm/ResolveInfo;Landroid/content/pm/ResolveInfo;)I

    move-result v0

    return v0
.end method
