.class Lcom/sec/samsung/gallery/view/utils/EditModeHelper$1;
.super Ljava/lang/Object;
.source "EditModeHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showSetAsDialog(Lcom/sec/android/gallery3d/data/MediaItem;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

.field final synthetic val$checkView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/utils/EditModeHelper;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 337
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper$1;->this$0:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper$1;->val$checkView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    const/4 v2, 0x1

    .line 340
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper$1;->val$checkView:Landroid/view/View;

    const v1, 0x7f0f001d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper$1;->this$0:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    # getter for: Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->access$000(Lcom/sec/samsung/gallery/view/utils/EditModeHelper;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "private_move_do_not_show"

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 342
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper$1;->this$0:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showSetAsDialog(Z)V

    .line 343
    return-void
.end method
