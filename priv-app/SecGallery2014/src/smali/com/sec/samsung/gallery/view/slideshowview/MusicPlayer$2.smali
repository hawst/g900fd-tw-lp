.class Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$2;
.super Landroid/telephony/PhoneStateListener;
.source "MusicPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 5
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 64
    packed-switch p1, :pswitch_data_0

    .line 93
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mCallState:I
    invoke-static {v1, p1}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->access$602(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;I)I

    .line 94
    return-void

    .line 66
    :pswitch_0
    const-string v1, "MusicPlayer"

    const-string v2, "Slideshow started -> Call DUT -> Slideshow and Slideshow Music should stop"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    const-string v1, "MusicPlayer"

    const-string v2, "Accept call in DUT -> Start Slideshow -> End call -> Slideshow music should start"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    const/4 v0, 0x0

    .line 69
    .local v0, "isGettingFinished":Z
    sget-boolean v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isGettingFinished:Z

    .line 70
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsMusicStartPended:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsMusicPausedByCall:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->access$400(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    if-nez v0, :cond_2

    .line 71
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_2

    .line 72
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsMusicAFCLoss:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->access$200(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 73
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 74
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsMusicStartPended:Z
    invoke-static {v1, v3}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->access$302(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;Z)Z

    .line 75
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsMusicPausedByCall:Z
    invoke-static {v1, v3}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->access$402(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;Z)Z

    .line 78
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsOffHookOccured:Z
    invoke-static {v1, v3}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->access$502(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;Z)Z

    goto :goto_0

    .line 81
    .end local v0    # "isGettingFinished":Z
    :pswitch_1
    const-string v1, "MusicPlayer"

    const-string/jumbo v2, "stop music at ringing"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsOffHookOccured:Z
    invoke-static {v1, v4}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->access$502(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;Z)Z

    .line 83
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->pauseMusic()V

    goto :goto_0

    .line 86
    :pswitch_2
    const-string v1, "MusicPlayer"

    const-string v2, "music don\'t start during call"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mIsOffHookOccured:Z
    invoke-static {v1, v4}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->access$502(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;Z)Z

    .line 88
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->pauseMusic()V

    goto :goto_0

    .line 64
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
