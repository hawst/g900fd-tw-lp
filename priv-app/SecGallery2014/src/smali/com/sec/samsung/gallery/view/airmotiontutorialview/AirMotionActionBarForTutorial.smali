.class public Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarView;
.source "AirMotionActionBarForTutorial.java"


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 17
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 19
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;->mMainActionBar:Landroid/app/ActionBar;

    const v1, 0x7f020311

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setIcon(I)V

    .line 21
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial$1;-><init>(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 30
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;->setTitle()V

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method private setTitle()V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial$2;-><init>(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 52
    return-void
.end method
