.class Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPick;
.super Ljava/lang/Object;
.source "AlbumViewEventHandle.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$IAlbumViewMode;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AlbumViewPick"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPick;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$1;

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPick;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;)V

    return-void
.end method


# virtual methods
.method public disableView()V
    .locals 0

    .prologue
    .line 86
    return-void
.end method

.method public enableView()V
    .locals 0

    .prologue
    .line 114
    return-void
.end method

.method public initializeView()V
    .locals 3

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPick;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mAlbumViewState:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->access$400(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;)Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->getActionBarManager()Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v0

    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPick;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->access$300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 82
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 107
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPick;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->access$300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 108
    .local v0, "topState":Lcom/sec/android/gallery3d/app/ActivityState;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPick;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->access$300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 109
    return-void
.end method

.method public onHoverClick(Lcom/sec/android/gallery3d/data/MediaObject;)V
    .locals 3
    .param p1, "media"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 99
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPick;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->access$300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 102
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPick;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->access$300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "PICKER_ITEM_SELECTED"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 103
    return-void
.end method

.method public onItemClick(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlView;I)V
    .locals 4
    .param p1, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "view"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "position"    # I

    .prologue
    .line 90
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPick;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mAlbumViewState:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->access$400(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;)Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v2, p3}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 91
    .local v0, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPick;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->access$300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v0, v1, v2

    .line 94
    .local v1, "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPick;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->access$300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "PICKER_ITEM_SELECTED"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 95
    return-void
.end method
