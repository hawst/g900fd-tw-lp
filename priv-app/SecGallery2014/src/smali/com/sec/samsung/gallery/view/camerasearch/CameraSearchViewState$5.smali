.class Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$5;
.super Ljava/lang/Object;
.source "CameraSearchViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$5;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGenericMotionCancel(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 3
    .param p1, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    .line 198
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$5;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->access$600(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getGenericMotionFocus()I

    move-result v0

    .line 199
    .local v0, "genericMotionFocus":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$5;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->access$600(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->setGenericMotionFocus(I)V

    .line 200
    if-ltz v0, :cond_0

    .line 201
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$5;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->access$600(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->notifyDataSetChanged(II)V

    .line 203
    :cond_0
    return-void
.end method

.method public onGenericMotionEnter(Lcom/sec/android/gallery3d/glcore/GlLayer;I)V
    .locals 1
    .param p1, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "position"    # I

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$5;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    invoke-virtual {v0, p2}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->setItemFocus(I)V

    .line 194
    return-void
.end method
