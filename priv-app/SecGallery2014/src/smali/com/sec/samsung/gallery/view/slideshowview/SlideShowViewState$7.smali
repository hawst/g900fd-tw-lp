.class Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$7;
.super Landroid/content/BroadcastReceiver;
.source "SlideShowViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->registerBroadcastReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V
    .locals 0

    .prologue
    .line 781
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$7;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 784
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 785
    .local v0, "action":Ljava/lang/String;
    const-string v4, "android.intent.action.PALM_DOWN"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 786
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$7;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->onBackPressed()V

    .line 829
    :cond_0
    :goto_0
    return-void

    .line 787
    :cond_1
    const-string v4, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 788
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$300()Ljava/lang/String;

    move-result-object v4

    const-string v5, "MEDIA_UNMOUNTED"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 789
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$7;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$1300(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->isDataContainsSDcardPath()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 790
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$7;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-virtual {v4, v7}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setIndicatorVisibility(Z)V

    .line 791
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$300()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Stop slideshow by media unmounted"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 792
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$7;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->finishCurrentViewState()V

    goto :goto_0

    .line 794
    :cond_2
    const-string v4, "android.intent.action.DOCK_EVENT"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 797
    const-string v4, "android.intent.extra.DOCK_STATE"

    invoke-virtual {p2, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 799
    .local v1, "dockStateReceived":I
    const-string v4, "android.intent.extra.SMART_DOCK_STATE"

    invoke-virtual {p2, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 802
    .local v3, "smartDockStateReceived":I
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mDockStatePrevious:I
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$1400()I

    move-result v4

    if-eqz v4, :cond_3

    if-eqz v1, :cond_4

    :cond_3
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSmartDockStatePrevious:I
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$1500()I

    move-result v4

    if-ne v4, v7, :cond_5

    if-nez v1, :cond_5

    .line 804
    :cond_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$7;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-virtual {v4, v7}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setIndicatorVisibility(Z)V

    .line 805
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$300()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Stop slideshow by dock event"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 806
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$7;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->finishCurrentViewState()V

    .line 809
    :cond_5
    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mDockStatePrevious:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$1402(I)I

    .line 810
    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSmartDockStatePrevious:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$1502(I)I

    goto :goto_0

    .line 813
    .end local v1    # "dockStateReceived":I
    .end local v3    # "smartDockStateReceived":I
    :cond_6
    const-string v4, "android.hardware.usb.action.USB_DEVICE_DETACHED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 814
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$7;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$1300(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->isDataContainsMTPpath()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 815
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$300()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Stop slideshow by MTP media unmounted"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 816
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$7;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isSlideshowPauseViewEnable()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 817
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$7;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # invokes: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setSlideshowPauseViewEnable(Z)V
    invoke-static {v4, v6}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$1600(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;Z)V

    .line 818
    :cond_7
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$7;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$900(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 819
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$7;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$900(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->setIsShowing(Z)V

    .line 820
    :cond_8
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$7;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 822
    :cond_9
    const-string v4, "request"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 823
    .local v2, "request":Ljava/lang/String;
    if-eqz v2, :cond_0

    const-string/jumbo v4, "stop"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 824
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$7;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-virtual {v4, v7}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setIndicatorVisibility(Z)V

    .line 825
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$300()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Stop slideshow by request"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 826
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$7;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->finishCurrentViewState()V

    goto/16 :goto_0
.end method
