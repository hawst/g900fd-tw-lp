.class Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$7;
.super Ljava/lang/Object;
.source "NewAlbumAlertDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

.field final synthetic val$editText:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 378
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$7;->this$1:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$7;->val$editText:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 381
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$7;->this$1:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$400(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$7;->val$editText:Landroid/widget/EditText;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 383
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$7;->this$1:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mClipVisible:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->access$1600(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 384
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$7;->this$1:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->mCtx:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->access$800(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "clipboardEx"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/sec/clipboard/ClipboardExManager;

    .line 385
    .local v0, "clipEx":Landroid/sec/clipboard/ClipboardExManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->isShowing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 386
    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->showUIDataDialog()V

    .line 389
    .end local v0    # "clipEx":Landroid/sec/clipboard/ClipboardExManager;
    :cond_0
    return-void
.end method
