.class Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$3;
.super Ljava/lang/Object;
.source "NoteActionBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$3;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v5, 0x7f0f0143

    const v4, 0x7f0f0142

    const/16 v3, 0x8

    .line 182
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v2, v5, :cond_1

    .line 183
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$3;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->isUndoable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 184
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$3;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->updateUndo()V

    .line 192
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$3;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$100(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 193
    .local v1, "undoBtn":Landroid/widget/ImageButton;
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$3;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$100(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 194
    .local v0, "redoBtn":Landroid/widget/ImageButton;
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$3;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->isUndoable()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 195
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$3;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->isRedoable()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 196
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$3;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setPenSettingVisibility(I)V

    .line 197
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$3;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setRemoverSettingVisibility(I)V

    .line 198
    return-void

    .line 186
    .end local v0    # "redoBtn":Landroid/widget/ImageButton;
    .end local v1    # "undoBtn":Landroid/widget/ImageButton;
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v2, v4, :cond_0

    .line 187
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$3;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->isRedoable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 188
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$3;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->updateRedo()V

    goto :goto_0
.end method
