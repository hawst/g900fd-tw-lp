.class public Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;
.super Ljava/lang/Object;
.source "DimensionConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Spec"
.end annotation


# instance fields
.field public thumbnail_view_hgap:I

.field public thumbnail_view_hpadding_left:I

.field public thumbnail_view_hpadding_right:I

.field public thumbnail_view_new_album_divider_height:I

.field public thumbnail_view_new_album_hpadding_left:I

.field public thumbnail_view_new_album_textbox_voffset:I

.field public thumbnail_view_new_album_thumbnail_height:I

.field public thumbnail_view_new_album_thumbnail_width:I

.field public thumbnail_view_new_album_vpadding_bottom:I

.field public thumbnail_view_new_album_vpadding_top:I

.field public thumbnail_view_split_album_background_height:I

.field public thumbnail_view_split_album_background_width:I

.field public thumbnail_view_split_album_divider_height:I

.field public thumbnail_view_split_album_hgap:I

.field public thumbnail_view_split_album_hpadding_left:I

.field public thumbnail_view_split_album_name_textbox_height:I

.field public thumbnail_view_split_album_name_textbox_hoffset:I

.field public thumbnail_view_split_album_name_textbox_voffset:I

.field public thumbnail_view_split_album_name_textbox_width:I

.field public thumbnail_view_split_album_thumbnail_height:I

.field public thumbnail_view_split_album_thumbnail_width:I

.field public thumbnail_view_split_album_vgap:I

.field public thumbnail_view_split_album_vpadding_top:I

.field public thumbnail_view_thumbnail_height:I

.field public thumbnail_view_thumbnail_width:I

.field public thumbnail_view_vgap:I

.field public thumbnail_view_vpadding_top:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
