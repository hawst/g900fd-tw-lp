.class Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$SBeamStateObserver;
.super Landroid/database/ContentObserver;
.source "DetailViewBeam.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SBeamStateObserver"
.end annotation


# instance fields
.field private mResolver:Landroid/content/ContentResolver;

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 1
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$SBeamStateObserver;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;

    .line 84
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 85
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$SBeamStateObserver;->mResolver:Landroid/content/ContentResolver;

    .line 86
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .prologue
    .line 100
    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onChange()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 102
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$SBeamStateObserver;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->setBeamListener()V

    .line 103
    return-void
.end method

.method public register()V
    .locals 3

    .prologue
    .line 89
    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "register setting observer"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$SBeamStateObserver;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "sbeam_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 91
    return-void
.end method

.method public unregister()V
    .locals 2

    .prologue
    .line 94
    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "unregister setting observer"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$SBeamStateObserver;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 96
    return-void
.end method
