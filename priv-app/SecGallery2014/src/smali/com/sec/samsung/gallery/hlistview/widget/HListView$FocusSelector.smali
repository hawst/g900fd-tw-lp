.class Lcom/sec/samsung/gallery/hlistview/widget/HListView$FocusSelector;
.super Ljava/lang/Object;
.source "HListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/hlistview/widget/HListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FocusSelector"
.end annotation


# instance fields
.field private mPosition:I

.field private mPositionLeft:I

.field final synthetic this$0:Lcom/sec/samsung/gallery/hlistview/widget/HListView;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/hlistview/widget/HListView;)V
    .locals 0

    .prologue
    .line 1051
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FocusSelector;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/HListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/hlistview/widget/HListView;Lcom/sec/samsung/gallery/hlistview/widget/HListView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/HListView;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/hlistview/widget/HListView$1;

    .prologue
    .line 1051
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FocusSelector;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/HListView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FocusSelector;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/HListView;

    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FocusSelector;->mPosition:I

    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FocusSelector;->mPositionLeft:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setSelectionFromLeft(II)V

    .line 1065
    return-void
.end method

.method public setup(II)Lcom/sec/samsung/gallery/hlistview/widget/HListView$FocusSelector;
    .locals 0
    .param p1, "position"    # I
    .param p2, "left"    # I

    .prologue
    .line 1057
    iput p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FocusSelector;->mPosition:I

    .line 1058
    iput p2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FocusSelector;->mPositionLeft:I

    .line 1059
    return-object p0
.end method
