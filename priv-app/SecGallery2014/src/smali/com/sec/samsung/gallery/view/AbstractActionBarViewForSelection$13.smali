.class Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$13;
.super Ljava/lang/Object;
.source "AbstractActionBarViewForSelection.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)V
    .locals 0

    .prologue
    .line 612
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$13;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 615
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$13;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mUsingSelectionModeBar:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionOnActionBar:Z

    if-nez v0, :cond_0

    .line 626
    :goto_0
    return-void

    .line 618
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$13;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    if-nez v0, :cond_1

    .line 619
    # getter for: Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->access$600()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mSelectionModeBar is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 625
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$13;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->setVisibility(I)V

    goto :goto_0
.end method
