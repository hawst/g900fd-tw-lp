.class Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerListOnKeyListener;
.super Ljava/lang/Object;
.source "NavigationDrawer.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/drawer/NavigationDrawer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DrawerListOnKeyListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)V
    .locals 0

    .prologue
    .line 563
    iput-object p1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerListOnKeyListener;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;

    .prologue
    .line 563
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerListOnKeyListener;-><init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v2, 0x14

    .line 567
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerListOnKeyListener;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerList:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$2300(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v0

    .line 569
    .local v0, "selectedPosition":I
    if-ne p2, v2, :cond_1

    .line 570
    add-int/lit8 v0, v0, 0x1

    .line 575
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerListOnKeyListener;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$100(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->isEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 576
    if-ne p2, v2, :cond_2

    .line 577
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerListOnKeyListener;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerList:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$2300(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Landroid/widget/ListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 582
    :goto_1
    return v1

    .line 571
    :cond_1
    const/16 v1, 0x13

    if-ne p2, v1, :cond_0

    .line 572
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 579
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerListOnKeyListener;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerList:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$2300(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Landroid/widget/ListView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_1

    .line 582
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method
