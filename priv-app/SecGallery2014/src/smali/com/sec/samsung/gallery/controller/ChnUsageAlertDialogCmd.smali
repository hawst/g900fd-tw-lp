.class public Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "ChnUsageAlertDialogCmd.java"

# interfaces
.implements Ljava/util/Observer;
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field public static final SEND_MESSAGE_DELAY_TIME:I = 0xfa

.field private static final TAG:Ljava/lang/String;

.field public static final TYPE_CHANGE_PLAYER:I = 0x6

.field public static final TYPE_MAP_VIEW_VIA_NETWORK:I = 0x5

.field public static final TYPE_PERMISSION_FACETAG:I = 0xe

.field public static final TYPE_PERMISSION_TAG_BUDDY:I = 0xd

.field public static final TYPE_SCAN_NEARBY_DEVICE:I = 0x7

.field public static execNearbyPopup:Z

.field private static mHandler:Landroid/os/Handler;

.field public static popupType:Ljava/lang/Integer;

.field public static position:Ljava/lang/Integer;


# instance fields
.field private bodyText:Ljava/lang/CharSequence;

.field private dataAlertDialog:Landroid/app/AlertDialog;

.field private justSelectedKeyOK:Z

.field private mContext:Landroid/content/Context;

.field private permissionAlertDialog:Landroid/app/AlertDialog;

.field private titleID:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-class v0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->TAG:Ljava/lang/String;

    .line 64
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->execNearbyPopup:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 61
    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->dataAlertDialog:Landroid/app/AlertDialog;

    .line 62
    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->permissionAlertDialog:Landroid/app/AlertDialog;

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->justSelectedKeyOK:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->justSelectedKeyOK:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->justSelectedKeyOK:Z

    return p1
.end method

.method static synthetic access$200()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private dismissdialog()V
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->dataAlertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 236
    sget-object v0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->TAG:Ljava/lang/String;

    const-string v1, "Dismiss dataAlertDialog."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->dataAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->permissionAlertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 240
    sget-object v0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->TAG:Ljava/lang/String;

    const-string v1, "Dismiss permissionAlertDialog."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->permissionAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 243
    :cond_1
    return-void
.end method

.method private showDialog()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 134
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->dismissdialog()V

    .line 135
    sget-object v5, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->TAG:Ljava/lang/String;

    const-string v6, "Show dialog."

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 137
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 139
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f030031

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 140
    .local v3, "layout":Landroid/view/View;
    const v5, 0x7f0f0050

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 141
    .local v4, "message1":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->bodyText:Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v5, :cond_0

    .line 143
    const/high16 v5, -0x1000000

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 145
    :cond_0
    const v5, 0x7f0f001d

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 146
    .local v1, "checkBox":Landroid/widget/CheckBox;
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->titleID:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 147
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 148
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setSoundEffectsEnabled(Z)V

    .line 149
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 151
    new-instance v5, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$1;

    invoke-direct {v5, p0}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;)V

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    const v5, 0x104000a

    new-instance v6, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$3;

    invoke-direct {v6, p0, v1}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$3;-><init>(Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const/high16 v6, 0x1040000

    new-instance v7, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$2;

    invoke-direct {v7, p0}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$2;-><init>(Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 205
    sget-object v5, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v6, 0x5

    if-eq v5, v6, :cond_1

    sget-object v5, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v6, 0x6

    if-eq v5, v6, :cond_1

    sget-object v5, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v6, 0x7

    if-ne v5, v6, :cond_3

    .line 207
    :cond_1
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->dataAlertDialog:Landroid/app/AlertDialog;

    .line 208
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->dataAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->show()V

    .line 210
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->dataAlertDialog:Landroid/app/AlertDialog;

    new-instance v6, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$4;

    invoke-direct {v6, p0}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$4;-><init>(Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;)V

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 232
    :cond_2
    :goto_0
    return-void

    .line 218
    :cond_3
    sget-object v5, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/16 v6, 0xd

    if-eq v5, v6, :cond_4

    sget-object v5, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/16 v6, 0xe

    if-ne v5, v6, :cond_2

    .line 219
    :cond_4
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->permissionAlertDialog:Landroid/app/AlertDialog;

    .line 220
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->permissionAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->show()V

    .line 222
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->permissionAlertDialog:Landroid/app/AlertDialog;

    new-instance v6, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$5;

    invoke-direct {v6, p0}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$5;-><init>(Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;)V

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 9
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const/16 v8, 0xe

    const/16 v7, 0xd

    const/4 v6, 0x7

    const/4 v5, 0x6

    const/4 v4, 0x1

    .line 73
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Object;

    move-object v0, v2

    check-cast v0, [Ljava/lang/Object;

    .line 74
    .local v0, "params":[Ljava/lang/Object;
    const/4 v2, 0x0

    aget-object v2, v0, v2

    check-cast v2, Landroid/content/Context;

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;

    .line 75
    aget-object v2, v0, v4

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 76
    .local v1, "show":Z
    const/4 v2, 0x2

    aget-object v2, v0, v2

    check-cast v2, Ljava/lang/Integer;

    sput-object v2, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    .line 78
    sget-object v2, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, v5, :cond_0

    sget-object v2, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, v6, :cond_0

    sget-object v2, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, v7, :cond_0

    sget-object v2, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v8, :cond_1

    .line 80
    :cond_0
    const/4 v2, 0x3

    aget-object v2, v0, v2

    check-cast v2, Landroid/os/Handler;

    sput-object v2, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mHandler:Landroid/os/Handler;

    .line 83
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->dataAlertDialog:Landroid/app/AlertDialog;

    if-eqz v2, :cond_2

    .line 84
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->dataAlertDialog:Landroid/app/AlertDialog;

    .line 85
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->permissionAlertDialog:Landroid/app/AlertDialog;

    if-eqz v2, :cond_3

    .line 86
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->permissionAlertDialog:Landroid/app/AlertDialog;

    .line 88
    :cond_3
    if-nez v1, :cond_4

    .line 89
    sget-object v2, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->TAG:Ljava/lang/String;

    const-string v3, "Remove dialog command is occured."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->dismissdialog()V

    .line 126
    :goto_0
    return-void

    .line 92
    :cond_4
    sget-object v2, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x5

    if-eq v2, v3, :cond_5

    sget-object v2, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, v5, :cond_5

    sget-object v2, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v6, :cond_6

    :cond_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->dataAlertDialog:Landroid/app/AlertDialog;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->dataAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v2

    if-ne v2, v4, :cond_6

    .line 95
    sget-object v2, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->TAG:Ljava/lang/String;

    const-string v3, "dataAlertDialog is isShowing. No more excute dialog."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 98
    :cond_6
    sget-object v2, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, v7, :cond_7

    sget-object v2, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v8, :cond_8

    :cond_7
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->permissionAlertDialog:Landroid/app/AlertDialog;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->permissionAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v2

    if-ne v2, v4, :cond_8

    .line 100
    sget-object v2, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->TAG:Ljava/lang/String;

    const-string v3, "permissionAlertDialog is isShowing. No more excute dialog."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 104
    :cond_8
    sget-object v2, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_a

    .line 105
    const v2, 0x7f0e0300

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->titleID:Ljava/lang/Integer;

    .line 106
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0301

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0302

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0303

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0304

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e02fd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->bodyText:Ljava/lang/CharSequence;

    .line 125
    :cond_9
    :goto_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->showDialog()V

    goto/16 :goto_0

    .line 111
    :cond_a
    sget-object v2, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, v5, :cond_b

    sget-object v2, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v6, :cond_c

    .line 112
    :cond_b
    const v2, 0x7f0e02f6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->titleID:Ljava/lang/Integer;

    .line 113
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e02f7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->bodyText:Ljava/lang/CharSequence;

    goto :goto_1

    .line 114
    :cond_c
    sget-object v2, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v7, :cond_d

    .line 115
    const v2, 0x7f0e02f8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->titleID:Ljava/lang/Integer;

    .line 116
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e02f9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\n - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e02fc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->bodyText:Ljava/lang/CharSequence;

    goto :goto_1

    .line 118
    :cond_d
    sget-object v2, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v8, :cond_9

    .line 119
    const v2, 0x7f0e02f8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->titleID:Ljava/lang/Integer;

    .line 120
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e02f9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\n - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e02fb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e02fc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->bodyText:Ljava/lang/CharSequence;

    goto/16 :goto_1
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 1
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 130
    move-object v0, p2

    check-cast v0, Lcom/sec/samsung/gallery/core/Event;

    .line 131
    .local v0, "ev":Lcom/sec/samsung/gallery/core/Event;
    return-void
.end method
