.class Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3$1;
.super Ljava/lang/Object;
.source "ContentToDisplayDialogForCloudAlbum.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;

.field final synthetic val$cView:Landroid/view/View;

.field final synthetic val$colorOnClick:I

.field final synthetic val$viewType:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;Landroid/view/View;II)V
    .locals 0

    .prologue
    .line 328
    iput-object p1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;

    iput-object p2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3$1;->val$cView:Landroid/view/View;

    iput p3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3$1;->val$colorOnClick:I

    iput p4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3$1;->val$viewType:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 332
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .local v0, "action":I
    move-object v1, p1

    .line 333
    check-cast v1, Landroid/widget/CheckedTextView;

    .line 334
    .local v1, "ctv":Landroid/widget/CheckedTextView;
    if-nez v0, :cond_1

    .line 335
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;

    iget-object v2, v2, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$200(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 336
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3$1;->val$cView:Landroid/view/View;

    iget v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3$1;->val$colorOnClick:I

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 350
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;

    iget-object v2, v2, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # invokes: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->updateSaveButton()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$600(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)V

    .line 351
    return v4

    .line 337
    :cond_1
    const/4 v2, 0x3

    if-ne v0, v2, :cond_2

    .line 338
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3$1;->val$cView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 339
    :cond_2
    if-ne v0, v4, :cond_0

    .line 340
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3$1;->val$cView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 341
    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->toggle()V

    .line 342
    const v2, 0x8000

    invoke-virtual {v1, v2}, Landroid/widget/CheckedTextView;->sendAccessibilityEvent(I)V

    .line 343
    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 344
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;

    iget-object v2, v2, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    iget v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3$1;->val$viewType:I

    # |= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$076(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)I

    .line 348
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;

    iget-object v2, v2, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mFormatListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$700(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->invalidateViews()V

    goto :goto_0

    .line 346
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;

    iget-object v2, v2, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    iget v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3$1;->val$viewType:I

    xor-int/lit8 v3, v3, -0x1

    # &= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$072(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)I

    goto :goto_1
.end method
