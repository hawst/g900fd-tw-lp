.class public Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarView;
.source "GallerySearchActionBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$OnUpdateHistoryListener;,
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$HistoryAdapter;
    }
.end annotation


# static fields
.field private static final NAME:Ljava/lang/String; = "name"

.field private static final RID:Ljava/lang/String; = "person_id"

.field private static final TIME:Ljava/lang/String; = "time"


# instance fields
.field private mCursor:Landroid/database/MatrixCursor;

.field private mIsDoingVoiceSearch:Z

.field private mOldFilterText:Ljava/lang/String;

.field private mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

.field private mShowHistoryListListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$OnShowHistoryListListener;

.field public mUpdateHistory:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$OnUpdateHistoryListener;

.field private suggestionAdapter:Landroid/widget/SimpleCursorAdapter;


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 73
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;I)V

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mIsDoingVoiceSearch:Z

    .line 524
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$11;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$11;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mUpdateHistory:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$OnUpdateHistoryListener;

    .line 74
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$1;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 96
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->setSuggestionAdapter(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Landroid/database/MatrixCursor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mCursor:Landroid/database/MatrixCursor;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->initializeSearchView()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$OnShowHistoryListListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mShowHistoryListListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$OnShowHistoryListListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mOldFilterText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mOldFilterText:Ljava/lang/String;

    return-object p1
.end method

.method private addSuggestion(Ljava/lang/String;)V
    .locals 10
    .param p1, "filterText"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 195
    new-instance v2, Landroid/database/MatrixCursor;

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "_id"

    aput-object v4, v3, v7

    const-string v4, "person_id"

    aput-object v4, v3, v6

    const-string v4, "name"

    aput-object v4, v3, v8

    const-string/jumbo v4, "time"

    aput-object v4, v3, v9

    const/4 v4, 0x4

    const-string v5, "keyword"

    aput-object v5, v3, v4

    invoke-direct {v2, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mCursor:Landroid/database/MatrixCursor;

    .line 199
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 200
    .local v0, "isOnlyHistory":Ljava/lang/Integer;
    if-nez p1, :cond_0

    .line 201
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 212
    :goto_0
    new-array v1, v9, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mCursor:Landroid/database/MatrixCursor;

    aput-object v2, v1, v7

    aput-object p1, v1, v6

    aput-object v0, v1, v8

    .line 215
    .local v1, "object":[Ljava/lang/Object;
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_ADD_SUGGESTION:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/core/Event;->setData(Ljava/lang/Object;)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->notifyObservers(Ljava/lang/Object;)V

    .line 216
    return-void

    .line 203
    .end local v1    # "object":[Ljava/lang/Object;
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 204
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-eqz v2, :cond_1

    .line 205
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 207
    :cond_1
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 209
    :cond_2
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method private getOptionMenuId()I
    .locals 1

    .prologue
    .line 99
    const v0, 0x7f120037

    return v0
.end method

.method private initializeSearchView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 219
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    if-nez v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0f0019

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getSearchText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mOldFilterText:Ljava/lang/String;

    .line 223
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mOldFilterText:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 224
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mOldFilterText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 225
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setImeVisibility(Z)V

    .line 227
    :cond_1
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGallerySearchUISeperatedMode:Z

    if-nez v0, :cond_2

    .line 228
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e01f9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 231
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$2;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setOnQueryTextListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnQueryTextListener;)V

    .line 254
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$3;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$3;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setOnUpdateTagListListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnUpdateTagListListener;)V

    .line 264
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setVoiceButtonEnabled(Z)V

    .line 265
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$4;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$4;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setOnVoiceRecognitionListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnVoiceRecognitionListener;)V

    .line 273
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$5;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$5;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setOnSuggestionListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnSuggestionListener;)V

    .line 288
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-eqz v0, :cond_3

    .line 289
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mUpdateHistory:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$OnUpdateHistoryListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setUpdateHistoryListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$OnUpdateHistoryListener;)V

    .line 290
    :cond_3
    return-void
.end method

.method private setSuggestionAdapter(Ljava/lang/String;)V
    .locals 8
    .param p1, "filterText"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x3

    .line 177
    if-eqz p1, :cond_0

    .line 178
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 179
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->addSuggestion(Ljava/lang/String;)V

    .line 181
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->suggestionAdapter:Landroid/widget/SimpleCursorAdapter;

    if-nez v0, :cond_1

    .line 182
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$HistoryAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f030067

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mCursor:Landroid/database/MatrixCursor;

    new-array v4, v7, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "person_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "name"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string/jumbo v6, "time"

    aput-object v6, v4, v5

    new-array v5, v7, [I

    fill-array-data v5, :array_0

    const/high16 v6, -0x80000000

    invoke-direct/range {v0 .. v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$HistoryAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->suggestionAdapter:Landroid/widget/SimpleCursorAdapter;

    .line 187
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->suggestionAdapter:Landroid/widget/SimpleCursorAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setSuggestionsAdapter(Landroid/widget/CursorAdapter;)V

    .line 192
    :goto_0
    return-void

    .line 190
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->suggestionAdapter:Landroid/widget/SimpleCursorAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mCursor:Landroid/database/MatrixCursor;

    invoke-virtual {v0, v1}, Landroid/widget/SimpleCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    goto :goto_0

    .line 182
    nop

    :array_0
    .array-data 4
        0x7f0f011b
        0x7f0f011d
        0x7f0f011c
    .end array-data
.end method


# virtual methods
.method public checkHistoryList()Z
    .locals 18

    .prologue
    .line 103
    const/4 v10, 0x0

    .line 104
    .local v10, "cursor":Landroid/database/Cursor;
    const-string v7, "date_added DESC"

    .line 105
    .local v7, "orderClause":Ljava/lang/String;
    const/4 v14, 0x0

    .line 106
    .local v14, "name":Ljava/lang/String;
    const-wide/16 v16, 0x0

    .line 107
    .local v16, "time":J
    const/4 v15, 0x0

    .line 108
    .local v15, "result":Z
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 109
    .local v8, "currentTime":J
    const-wide v4, 0x9a7ec800L

    sub-long v12, v8, v4

    .line 110
    .local v12, "limit_30_Day":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v2

    .line 112
    .local v2, "dbManager":Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
    :try_start_0
    const-string v3, "search_history"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "title"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "date_added"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 115
    if-eqz v10, :cond_1

    .line 116
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 118
    :cond_0
    const/4 v3, 0x0

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 119
    const/4 v3, 0x1

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 120
    cmp-long v3, v16, v12

    if-gez v3, :cond_2

    .line 121
    invoke-virtual {v2, v14}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->deleteHistory(Ljava/lang/String;)V

    .line 126
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    .line 132
    :cond_1
    :goto_0
    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 134
    :goto_1
    return v15

    .line 124
    :cond_2
    const/4 v15, 0x1

    .line 125
    goto :goto_0

    .line 129
    :catch_0
    move-exception v11

    .line 130
    .local v11, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 132
    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v11    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v3
.end method

.method public getOldSearchText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mOldFilterText:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchKeyword()Ljava/lang/String;
    .locals 2

    .prologue
    .line 346
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    if-eqz v1, :cond_0

    .line 347
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->getQueryText()Ljava/lang/String;

    move-result-object v0

    .line 350
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVoiceSearchState()Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mIsDoingVoiceSearch:Z

    return v0
.end method

.method public hideProgressBar()V
    .locals 2

    .prologue
    .line 398
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$10;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$10;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 406
    :cond_0
    return-void
.end method

.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 139
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-eqz v1, :cond_0

    .line 144
    :goto_0
    return-void

    .line 141
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 142
    .local v0, "inflater":Landroid/view/MenuInflater;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->getOptionMenuId()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 143
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->onCreateOptionsMenu(Landroid/view/Menu;)V

    goto :goto_0
.end method

.method public onDeleteKeyword(Ljava/lang/String;)V
    .locals 3
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 323
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->getQueryText()Ljava/lang/String;

    move-result-object v0

    .line 324
    .local v0, "key":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 325
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$7;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$7;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 332
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 163
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 169
    :goto_0
    return-void

    .line 165
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-class v2, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 166
    .local v0, "intent1":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 163
    nop

    :pswitch_data_0
    .packed-switch 0x7f0f02d8
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 305
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setImeVisibility(Z)V

    .line 307
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mOldFilterText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setSearchText(Ljava/lang/String;)V

    .line 308
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v1, 0x7f0f02d7

    const/4 v2, 0x1

    .line 148
    invoke-interface {p1, v1, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 149
    invoke-interface {p1, v1, v2}, Landroid/view/Menu;->setGroupEnabled(IZ)V

    .line 151
    const v1, 0x7f0f02d8

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 152
    .local v0, "deleteHistoryMenu":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 153
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->checkHistoryList()Z

    move-result v1

    if-nez v1, :cond_1

    .line 154
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 159
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 312
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->onResume()V

    .line 313
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 314
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 315
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 316
    return-void
.end method

.method public onVoiceResult(Ljava/lang/String;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 354
    if-eqz p1, :cond_0

    .line 355
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 357
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->setVoiceSearchState(Z)V

    .line 358
    return-void
.end method

.method public removeKeyword()V
    .locals 2

    .prologue
    .line 335
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    if-eqz v0, :cond_0

    .line 336
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$8;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$8;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 343
    :cond_0
    return-void
.end method

.method public setDropDownIcon(Z)V
    .locals 1
    .param p1, "isShow"    # Z

    .prologue
    .line 373
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setDropDownIcon(Z)V

    .line 375
    :cond_0
    return-void
.end method

.method public setHistoryQuery(Ljava/lang/String;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 538
    if-eqz p1, :cond_0

    .line 539
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 541
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->setImeVisibility(Z)V

    .line 542
    return-void
.end method

.method public setImeVisibility(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 369
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setImeVisibility(Z)V

    .line 370
    return-void
.end method

.method public setShowHistoryListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$OnShowHistoryListListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$OnShowHistoryListListener;

    .prologue
    .line 534
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mShowHistoryListListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$OnShowHistoryListListener;

    .line 535
    return-void
.end method

.method protected setSuggestionAdapter()V
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->setSuggestionAdapter(Ljava/lang/String;)V

    .line 174
    return-void
.end method

.method public setVoiceSearchState(Z)V
    .locals 0
    .param p1, "isActive"    # Z

    .prologue
    .line 361
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mIsDoingVoiceSearch:Z

    .line 362
    return-void
.end method

.method public setVoiceTextonResume()V
    .locals 2

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mOldFilterText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setSearchText(Ljava/lang/String;)V

    .line 320
    return-void
.end method

.method protected setupButtons()V
    .locals 2

    .prologue
    .line 295
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$6;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$6;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 301
    return-void
.end method

.method public showDropDownIcon(Z)V
    .locals 1
    .param p1, "isShow"    # Z

    .prologue
    .line 378
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->showDropDownIcon(Z)V

    .line 380
    :cond_0
    return-void
.end method

.method public showProgressBar()V
    .locals 2

    .prologue
    .line 387
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-eqz v0, :cond_0

    .line 388
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$9;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$9;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 395
    :cond_0
    return-void
.end method
