.class public Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;
.super Landroid/app/Activity;
.source "DeleteHistoryForGallerySearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;,
        Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItemViewHolder;,
        Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;
    }
.end annotation


# instance fields
.field private final DAY_MSEC:J

.field private mActionBar:Landroid/app/ActionBar;

.field private mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;

.field private mCheckBox:Landroid/widget/CheckBox;

.field private mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

.field private mDropDownButton:Landroid/widget/Button;

.field private mHeaderView:Landroid/view/View;

.field private mHistoryListView:Landroid/widget/ListView;

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;",
            ">;"
        }
    .end annotation
.end field

.field private mMenu:Landroid/view/Menu;

.field private mPopupMenu:Landroid/widget/ListPopupWindow;

.field private mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

.field private mSelectedCount:I

.field protected mSelectionActionBarView:Landroid/view/View;

.field private mSelectionButton:Landroid/widget/TextView;

.field private mSelectionModeOnActionBarLayout:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 50
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 52
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mItems:Ljava/util/ArrayList;

    .line 54
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;

    .line 56
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mHistoryListView:Landroid/widget/ListView;

    .line 58
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mActionBar:Landroid/app/ActionBar;

    .line 60
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mDropDownButton:Landroid/widget/Button;

    .line 62
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mPopupMenu:Landroid/widget/ListPopupWindow;

    .line 64
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mMenu:Landroid/view/Menu;

    .line 66
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectedCount:I

    .line 70
    const-wide/32 v0, 0x5265c00

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->DAY_MSEC:J

    .line 72
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mHeaderView:Landroid/view/View;

    .line 522
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->updateSelectAll()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->deleteSelectedItems()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->isAllChecked()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->updateCheckbox(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->setCheckedAll(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)Landroid/widget/ListPopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mPopupMenu:Landroid/widget/ListPopupWindow;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->adjustDropDownPopupOffset()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    .prologue
    .line 50
    iget v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectedCount:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    return-object v0
.end method

.method private addSelectAllHeader()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 246
    const v0, 0x7f0f020a

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mHeaderView:Landroid/view/View;

    .line 247
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mHeaderView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 248
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mHeaderView:Landroid/view/View;

    const v1, 0x7f0f020c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mCheckBox:Landroid/widget/CheckBox;

    .line 249
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 250
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setDuplicateParentStateEnabled(Z)V

    .line 251
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 253
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mHeaderView:Landroid/view/View;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$8;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 268
    return-void
.end method

.method private adjustDropDownPopupOffset()V
    .locals 4

    .prologue
    .line 602
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0259

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 603
    .local v0, "horizontalOffset":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d025b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 604
    .local v1, "verticalOffset":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0251

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getActionBarSize(Landroid/content/Context;)I

    move-result v3

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    .line 605
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mPopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v2, v1}, Landroid/widget/ListPopupWindow;->setVerticalOffset(I)V

    .line 606
    if-eqz v0, :cond_0

    .line 607
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mPopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v2, v0}, Landroid/widget/ListPopupWindow;->setHorizontalOffset(I)V

    .line 609
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mPopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v2}, Landroid/widget/ListPopupWindow;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 610
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mPopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v2}, Landroid/widget/ListPopupWindow;->show()V

    .line 612
    :cond_1
    return-void
.end method

.method private deleteSelectedItems()V
    .locals 5

    .prologue
    .line 389
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->getCount()I

    move-result v0

    .line 391
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 392
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;

    invoke-virtual {v3, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;

    .line 394
    .local v2, "item":Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;
    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->mIsChecked:Z
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->access$1000(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 395
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->getKeyword()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->deleteHistory(Ljava/lang/String;)V

    .line 391
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 397
    .end local v2    # "item":Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;
    :cond_1
    return-void
.end method

.method private getCustomActionView()Landroid/view/View;
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 159
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v1, :cond_1

    .line 160
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/ColorDrawable;-><init>()V

    .line 161
    .local v0, "colorDrawable":Landroid/graphics/drawable/ColorDrawable;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0033

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 162
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 167
    .end local v0    # "colorDrawable":Landroid/graphics/drawable/ColorDrawable;
    :goto_0
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionBar:Z

    if-eqz v1, :cond_2

    .line 168
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030010

    invoke-virtual {v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectionActionBarView:Landroid/view/View;

    .line 242
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectionActionBarView:Landroid/view/View;

    return-object v1

    .line 164
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200f3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 169
    :cond_2
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionOnActionBar:Z

    if-eqz v1, :cond_3

    .line 170
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030011

    invoke-virtual {v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectionActionBarView:Landroid/view/View;

    .line 171
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectionActionBarView:Landroid/view/View;

    const v2, 0x7f0f001f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectionModeOnActionBarLayout:Landroid/widget/LinearLayout;

    .line 172
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectionActionBarView:Landroid/view/View;

    const v2, 0x7f0f0020

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mCheckBox:Landroid/widget/CheckBox;

    .line 173
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mCheckBox:Landroid/widget/CheckBox;

    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$2;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectionActionBarView:Landroid/view/View;

    const v2, 0x7f0f0021

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectionButton:Landroid/widget/TextView;

    .line 180
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectionModeOnActionBarLayout:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$3;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->setEnableSelectionOnActionBar(Z)V

    goto :goto_1

    .line 188
    :cond_3
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03000f

    invoke-virtual {v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectionActionBarView:Landroid/view/View;

    .line 189
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectionActionBarView:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 190
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectionActionBarView:Landroid/view/View;

    const v2, 0x7f0f0018

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mDropDownButton:Landroid/widget/Button;

    .line 191
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mDropDownButton:Landroid/widget/Button;

    const v2, 0x7f0e004e

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectedCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 193
    new-instance v1, Landroid/widget/ListPopupWindow;

    invoke-direct {v1, p0}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mPopupMenu:Landroid/widget/ListPopupWindow;

    .line 194
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mPopupMenu:Landroid/widget/ListPopupWindow;

    if-eqz v1, :cond_0

    .line 195
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mPopupMenu:Landroid/widget/ListPopupWindow;

    const/4 v2, -0x2

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    .line 196
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mPopupMenu:Landroid/widget/ListPopupWindow;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mDropDownButton:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 197
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->adjustDropDownPopupOffset()V

    .line 198
    new-instance v1, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    const v2, 0x7f0300cc

    invoke-direct {v1, p0, v2}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    .line 199
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mPopupMenu:Landroid/widget/ListPopupWindow;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 200
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mPopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1, v6}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    .line 201
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$4;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->setSelectAllClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$5;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$5;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->setUnselectAllClickListener(Landroid/view/View$OnClickListener;)V

    .line 219
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mDropDownButton:Landroid/widget/Button;

    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$6;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$6;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 229
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mPopupMenu:Landroid/widget/ListPopupWindow;

    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$7;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$7;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto/16 :goto_1
.end method

.method private init()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 105
    const v0, 0x7f0f020f

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mHistoryListView:Landroid/widget/ListView;

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mItems:Ljava/util/ArrayList;

    .line 107
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getSuggestionNames()V

    .line 108
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mItems:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;

    .line 110
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mActionBar:Landroid/app/ActionBar;

    .line 111
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f11002a

    invoke-virtual {v0, v1}, Landroid/content/Context;->setTheme(I)V

    .line 112
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mActionBar:Landroid/app/ActionBar;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v2, -0x1000000

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 113
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getCustomActionView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 115
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 116
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 118
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 121
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionBar:Z

    if-eqz v0, :cond_2

    .line 122
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 131
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mHistoryListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 132
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mHistoryListView:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$1;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 152
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionBar:Z

    if-eqz v0, :cond_1

    .line 153
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->addSelectAllHeader()V

    .line 155
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->updateSelectAll()V

    .line 156
    return-void

    .line 124
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 125
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionOnActionBar:Z

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 127
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    goto :goto_0
.end method

.method private isAllChecked()Z
    .locals 5

    .prologue
    .line 271
    const/4 v0, 0x1

    .line 273
    .local v0, "all":Z
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;

    if-eqz v4, :cond_1

    .line 274
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->getCount()I

    move-result v1

    .line 276
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 277
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;

    invoke-virtual {v4, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;

    .line 279
    .local v3, "item":Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->getChecked()Z

    move-result v4

    if-nez v4, :cond_2

    .line 280
    const/4 v0, 0x0

    .line 285
    .end local v3    # "item":Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->notifyDataSetChanged()V

    .line 288
    .end local v1    # "count":I
    .end local v2    # "i":I
    :cond_1
    return v0

    .line 276
    .restart local v1    # "count":I
    .restart local v2    # "i":I
    .restart local v3    # "item":Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private setCheckedAll(Z)V
    .locals 4
    .param p1, "checked"    # Z

    .prologue
    .line 319
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;

    if-eqz v3, :cond_1

    .line 320
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->getCount()I

    move-result v0

    .line 322
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 323
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;

    invoke-virtual {v3, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;

    .line 324
    .local v2, "item":Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;
    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->setChecked(Z)V

    .line 322
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 327
    .end local v2    # "item":Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->notifyDataSetChanged()V

    .line 329
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_1
    return-void
.end method

.method private setEnableSelectionOnActionBar(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 624
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mCheckBox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 625
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 626
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 627
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 629
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectionButton:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 630
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectionButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 632
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectionModeOnActionBarLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 633
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectionModeOnActionBarLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 635
    :cond_2
    return-void
.end method

.method private showDeleteAlertDialog()V
    .locals 9

    .prologue
    const v8, 0x7f0e0041

    const/4 v5, 0x1

    .line 400
    const/4 v0, 0x0

    .line 401
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->getCount()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 402
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;

    invoke-virtual {v4, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;

    .line 403
    .local v1, "hItem":Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;
    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->mIsChecked:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->access$1000(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 404
    add-int/lit8 v0, v0, 0x1

    .line 401
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 408
    .end local v1    # "hItem":Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;
    :cond_1
    const/4 v3, 0x0

    .line 409
    .local v3, "message":Ljava/lang/String;
    if-ne v0, v5, :cond_2

    .line 410
    const v4, 0x7f0e0042

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 415
    :goto_1
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    new-instance v5, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$10;

    invoke-direct {v5, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$10;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)V

    invoke-virtual {v4, v8, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0e0046

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 427
    return-void

    .line 412
    :cond_2
    const v4, 0x7f0e0043

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {p0, v4, v5}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method private updateCheckbox(Z)V
    .locals 1
    .param p1, "bUpdate"    # Z

    .prologue
    .line 638
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 639
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 640
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->setCheckedAll(Z)V

    .line 641
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->updateSelectAll()V

    .line 643
    :cond_0
    return-void
.end method

.method private updateSelectAll()V
    .locals 11

    .prologue
    const v10, 0x7f0e004e

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 332
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->getCount()I

    move-result v0

    .line 333
    .local v0, "count":I
    iput v8, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectedCount:I

    .line 336
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 337
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;

    invoke-virtual {v5, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;

    .line 339
    .local v2, "item":Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->getChecked()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 340
    iget v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectedCount:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectedCount:I

    .line 336
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 344
    .end local v2    # "item":Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;
    :cond_1
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionBar:Z

    if-eqz v5, :cond_3

    .line 345
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mActionBar:Landroid/app/ActionBar;

    new-array v6, v9, [Ljava/lang/Object;

    iget v7, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectedCount:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p0, v10, v6}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 377
    :goto_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mMenu:Landroid/view/Menu;

    if-eqz v5, :cond_2

    .line 378
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mMenu:Landroid/view/Menu;

    const v6, 0x7f0f0024

    invoke-interface {v5, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 380
    .local v3, "menuItem":Landroid/view/MenuItem;
    iget v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectedCount:I

    if-gtz v5, :cond_6

    .line 381
    invoke-interface {v3, v8}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 386
    .end local v3    # "menuItem":Landroid/view/MenuItem;
    :cond_2
    :goto_2
    return-void

    .line 346
    :cond_3
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionOnActionBar:Z

    if-eqz v5, :cond_5

    .line 347
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e004f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v9, [Ljava/lang/Object;

    iget v7, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectedCount:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 348
    .local v4, "selectedString":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectionButton:Landroid/widget/TextView;

    if-eqz v5, :cond_4

    .line 349
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectionButton:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 351
    :cond_4
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectionActionBarView:Landroid/view/View;

    const v6, 0x7f0f0021

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectionButton:Landroid/widget/TextView;

    .line 352
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectionButton:Landroid/widget/TextView;

    if-eqz v5, :cond_2

    .line 355
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectionButton:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 359
    .end local v4    # "selectedString":Ljava/lang/String;
    :cond_5
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mDropDownButton:Landroid/widget/Button;

    new-array v6, v9, [Ljava/lang/Object;

    iget v7, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mSelectedCount:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p0, v10, v6}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 362
    new-instance v5, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$9;

    invoke-direct {v5, p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$9;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;I)V

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 383
    .restart local v3    # "menuItem":Landroid/view/MenuItem;
    :cond_6
    invoke-interface {v3, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_2
.end method


# virtual methods
.method public getSuggestionNames()V
    .locals 12

    .prologue
    .line 292
    const/4 v6, 0x0

    .line 293
    .local v6, "cursor":Landroid/database/Cursor;
    const-string v5, "date_added DESC"

    .line 294
    .local v5, "orderClause":Ljava/lang/String;
    const/4 v8, 0x0

    .line 295
    .local v8, "name":Ljava/lang/String;
    const-wide/16 v10, 0x0

    .line 297
    .local v10, "time":J
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string v1, "search_history"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "title"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "date_added"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 300
    if-eqz v6, :cond_2

    .line 301
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 303
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 304
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 305
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mItems:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;

    invoke-direct {v1, p0, v8, v10, v11}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 306
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 308
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313
    :cond_2
    if-eqz v6, :cond_3

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 315
    :cond_3
    :goto_0
    return-void

    .line 310
    :catch_0
    move-exception v7

    .line 311
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 313
    if-eqz v6, :cond_3

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :cond_4
    throw v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 617
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 618
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionBar:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionOnActionBar:Z

    if-nez v0, :cond_0

    .line 619
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->adjustDropDownPopupOffset()V

    .line 621
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v1, 0x400

    .line 85
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 86
    const v0, 0x7f0300cf

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->setContentView(I)V

    .line 87
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFullScreen(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 91
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    .line 92
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->init()V

    .line 94
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 431
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 432
    .local v0, "inflater":Landroid/view/MenuInflater;
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionBar:Z

    if-eqz v1, :cond_0

    .line 433
    const v1, 0x7f12001d

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 436
    :goto_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mMenu:Landroid/view/Menu;

    .line 437
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1

    .line 435
    :cond_0
    const v1, 0x7f12001e

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 442
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 444
    .local v0, "itemID":I
    sparse-switch v0, :sswitch_data_0

    .line 458
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1

    .line 446
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->showDeleteAlertDialog()V

    goto :goto_0

    .line 451
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->finish()V

    goto :goto_0

    .line 444
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_1
        0x7f0f0022 -> :sswitch_1
        0x7f0f0024 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 98
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 100
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionBar:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionOnActionBar:Z

    if-eqz v0, :cond_1

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mCheckBox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->isAllChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 102
    :cond_1
    return-void
.end method
