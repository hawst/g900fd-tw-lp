.class Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;
.super Ljava/lang/Object;
.source "MoreInfoItemUserTag.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 71
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    if-nez v2, :cond_1

    .line 72
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->hasFocus()Z

    move-result v3

    # setter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditTextHasFocus:Z
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$002(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;Z)Z

    .line 73
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->clearFocus()V

    .line 74
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->setCursorVisible(Z)V

    .line 75
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mAdapters:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$200(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Ljava/util/ArrayList;

    move-result-object v3

    # invokes: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->runUserTagEditView(Ljava/util/ArrayList;)V
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$300(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;Ljava/util/ArrayList;)V

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v7, v2, :cond_0

    .line 77
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 78
    .local v1, "tag":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$400(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->getAllTags()Ljava/util/ArrayList;

    move-result-object v0

    .line 79
    .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v1, :cond_4

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 80
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mDuplicatedToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$500(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Landroid/widget/Toast;

    move-result-object v2

    if-nez v2, :cond_2

    .line 81
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mContext:Landroid/content/Context;

    const-string v4, ""

    invoke-static {v3, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    # setter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mDuplicatedToast:Landroid/widget/Toast;
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$502(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 83
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mDuplicatedToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$500(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Landroid/widget/Toast;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e046d

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mDuplicatedToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$500(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 93
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;

    if-eqz v2, :cond_0

    .line 94
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;

    invoke-interface {v2, v7}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;->onEdit(Z)V

    goto :goto_0

    .line 86
    :cond_4
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    .line 87
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$400(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->addTag(Ljava/lang/String;)V

    .line 88
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mRunEditButton:Landroid/widget/ImageButton;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$600(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Landroid/widget/ImageButton;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 90
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mAddTagButton:Landroid/widget/ImageButton;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$700(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Landroid/widget/ImageButton;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 91
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e04a8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1
.end method
