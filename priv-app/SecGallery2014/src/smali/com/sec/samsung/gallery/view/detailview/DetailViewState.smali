.class public Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
.super Lcom/sec/android/gallery3d/app/ActivityState;
.source "DetailViewState.java"

# interfaces
.implements Landroid/widget/ShareActionProvider$OnShareTargetSelectedListener;
.implements Lcom/sec/android/gallery3d/app/GalleryActionBar$OnAlbumModeSelectedListener;
.implements Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$IOnPlayerStateListener;
.implements Lcom/sec/android/gallery3d/ui/PhotoView$Listener;
.implements Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowbarUpdateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/detailview/DetailViewState$InitAGIFJob;,
        Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;,
        Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;,
        Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorViewOnShowListener;,
        Lcom/sec/samsung/gallery/view/detailview/DetailViewState$MyMenuVisibilityListener;,
        Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;
    }
.end annotation


# static fields
.field private static final BACK_KEY_INVALID_TIME:J = 0x3e8L

.field private static final BURST_PLAY_DURATOIN_DEFAULT:I = 0x2

.field private static final CAMERA_SWITCH_CUTOFF_TIME:J = 0x0L

.field private static final DEFERRED_UPDATE_MS:J = 0xfaL

.field private static final DOUBLE_TAP_TIMEOUT:I

.field private static final HIDE_BARS_TIMEOUT:I = 0xdac

.field private static final HIDE_SIDE_MIRROR_THRESHOLD:I = 0x3e8

.field private static final HIDE_SIDE_MIRROR_TIMEOUT:I = 0xdac

.field public static final KEY_ADD_CAMERA_SHORTCUT:Ljava/lang/String; = "add_camera_shortcut"

.field public static final KEY_EXPANSION_DETAIL_VIEW:Ljava/lang/String; = "expansion_detail_view"

.field public static final KEY_FACE_ID:Ljava/lang/String; = "face_id"

.field public static final KEY_IN_CAMERA_ROLL:Ljava/lang/String; = "in_camera_roll"

.field public static final KEY_IS_FROM_CAMERA:Ljava/lang/String; = "is_from_camera"

.field public static final KEY_MEDIA_ITEM_PATH:Ljava/lang/String; = "KEY_MEDIA_ITEM_PATH"

.field public static final KEY_MEDIA_SET_PATH:Ljava/lang/String; = "KEY_MEDIA_SET_PATH"

.field public static final KEY_MIME_TYPE:Ljava/lang/String; = "mime-type"

.field private static final KEY_RETURN_INDEX_HINT:Ljava/lang/String; = "return-index-hint"

.field private static final KEY_START_IN_FILMSTRIP:Ljava/lang/String; = "start-in-filmstrip"

.field private static final KEY_TREAT_BACK_AS_UP:Ljava/lang/String; = "treat-back-as-up"

.field public static final KEY_VIEW_MODE:Ljava/lang/String; = "view-mode"

.field private static final MIMETYPE_FOR_EDITOR:Ljava/lang/String; = "image/*"

.field private static final MSG_CHANGE_ACTION_BAR:I = 0x64

.field private static final MSG_END_SCALE:I = 0x68

.field private static final MSG_EXIT_SELECTION_MODE_AND_RESET:I = 0x1

.field private static final MSG_HIDE_BARS:I = 0x1

.field private static final MSG_HIDE_FILMSTRIP_VIEW:I = 0x6a

.field private static final MSG_HIDE_SIDE_MIRROR:I = 0x69

.field private static final MSG_ON_CAMERA_CENTER:I = 0x6

.field private static final MSG_ON_FULL_SCREEN_CHANGED:I = 0x2

.field private static final MSG_PLAYER_STATE_CHANGE:I = 0x6e

.field private static final MSG_SET_BARS_DELAY:I = 0x67

.field private static final MSG_SHOW_FILMSTRIP_VIEW:I = 0x6b

.field private static final MSG_SHOW_SELECTION_MODE_BAR:I = 0x6d

.field private static final MSG_SMOOTH_ZOOM_IN:I = 0x65

.field private static final MSG_SMOOTH_ZOOM_OUT:I = 0x66

.field private static final MSG_UNFREEZE_GLROOT:I = 0x4

.field private static final MSG_UNUSE_POSTONANIMATION:I = 0x6c

.field private static final MSG_UPDATE_ACTION_BAR:I = 0x3

.field private static final MSG_UPDATE_DEFERRED:I = 0x7

.field private static final MSG_UPDATE_PANORAMA_UI:I = 0x9

.field private static final MSG_UPDATE_SHARE_URI:I = 0x8

.field private static final MSG_WANT_BARS:I = 0x5

.field public static final REQUEST_ASSIGN_NAME:I = 0x304

.field public static final REQUEST_CONTACT_INFO_CHANGED:I = 0x309

.field public static final REQUEST_CROP:I = 0x301

.field public static final REQUEST_CROP_MANUAL_FD:I = 0x306

.field public static final REQUEST_CROP_PICASA:I = 0x302

.field public static final REQUEST_CROP_SET_AS_CALLER_ID:I = 0x305

.field public static final REQUEST_EDIT:I = 0x303

.field public static final REQUEST_EDIT_LOCATION_TAG:I = 0x30a

.field public static final REQUEST_OCR_GET_TEXT:I = 0x308

.field public static final REQUEST_OCR_TEXT:I = 0x307

.field public static final REQUEST_SLINK_ACTION:I = 0x30b

.field public static final REQUEST_SLINK_ACTION_CROP:I = 0x30c

.field private static final RESULT_BACK:I = 0x65

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private AIRB:Ljava/lang/String;

.field private VIEW:Ljava/lang/String;

.field private isLongPressKey:Z

.field private mActionBar:Lcom/sec/android/gallery3d/app/GalleryActionBar;

.field private volatile mActionBarAllowed:Z

.field private mAgifVersion:J

.field private mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

.field private mAirMotionSettingsReceiver:Landroid/content/BroadcastReceiver;

.field private mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

.field private mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

.field private mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private mBackByZoomOut:Z

.field private mBrokenImage:Landroid/graphics/Bitmap;

.field private mCameraQuickViewTimer:Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;

.field private mChangePlayerDialog:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

.field private mCheckSideSyncNeeded:Z

.field private mClusterAlbumName:Ljava/lang/String;

.field private mContentObserver:Landroid/database/ContentObserver;

.field private mCurrentIndex:I

.field private mCurrentMediaSetIndex:I

.field private mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

.field private mDeferUpdateUntil:J

.field private mDeferredUpdateWaiting:Z

.field private mDeleteIsFocus:Z

.field private mDeleteList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field private mDeletePath:Lcom/sec/android/gallery3d/data/Path;

.field private mDetailViewBeam:Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;

.field private mDnieDisableTimeout:I

.field private final mDrmPopupListener:Lcom/sec/android/gallery3d/util/ImageDRMUtil$DrmPopupListener;

.field private mDrmUtil:Lcom/sec/android/gallery3d/util/ImageDRMUtil;

.field private mEAMReceiver:Landroid/content/BroadcastReceiver;

.field private mEVFHandler:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

.field private mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

.field private mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

.field private mExpansionDetailViewMode:Z

.field private mFSAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

.field private mFSBottom:I

.field private mFSLeft:I

.field private mFSRight:I

.field private mFSTop:I

.field private mFaceTagFeature:Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;

.field private mFilmStripCurrentProgress:F

.field private final mFilmStripListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

.field private mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

.field private mFirstTimeLaunch:Z

.field private mFromGalleryWidget:Z

.field private mFromSetupWidzard:Z

.field private mFromWeTogether:Z

.field private mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

.field private mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

.field private mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

.field private mGlHoverGestureListener:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$SimpleOnGestureListener;

.field private mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mGroupIndex:J

.field private mHandler:Landroid/os/Handler;

.field private mHasCameraScreennailOrPlaceholder:Z

.field private mHaveImageEditor:Z

.field private mHdmi3D:Lcom/sec/android/gallery3d/ui/Hdmi3D;

.field private mHdmiListener:Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;

.field private mHelpMode:I

.field private mHoverGestureDetector:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

.field private mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

.field private mIndexEnabled:Z

.field private mInitialItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mIsActive:Z

.field private mIsBack:Z

.field private mIsConnectedSideSync:Z

.field private mIsEmergencyMode:Z

.field private mIsFinishingState:Z

.field private mIsHiddenItem:Z

.field private mIsInteracting:Z

.field private mIsKnox2Enabled:Z

.field private mIsLongPressFinished:Z

.field private mIsMenuVisible:Z

.field private mIsPopupDialogDisplayed:Z

.field private mIsResumeManualFaceTag:Z

.field private mIsStartPhotoNote:Z

.field private mIsUpAvailableForNormal:Z

.field private mKeyBoardManager:Lcom/sec/android/gallery3d/util/KeyBoardManager;

.field private mLockTouchEvent:Z

.field private mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

.field private mMenu:Landroid/view/Menu;

.field private mMenuExecutor:Lcom/sec/android/gallery3d/ui/MenuExecutor;

.field private final mMenuVisibilityListener:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$MyMenuVisibilityListener;

.field private mMimeType:Ljava/lang/String;

.field private mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

.field public mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

.field private mMyFilesAndAll:Z

.field private mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

.field private mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mNfcPushUris:[Landroid/net/Uri;

.field private final mOnGenericMotionListener:Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;

.field private final mOnSystemGenericMotionListener:Landroid/view/View$OnGenericMotionListener;

.field private mOrientationManager:Lcom/sec/android/gallery3d/app/OrientationManager;

.field private mOriginalSetPathString:Ljava/lang/String;

.field private mOutOfFocusShotHelpView:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

.field private mPhotoPageViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

.field private mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

.field private mPlayVideoWhenLocked:Z

.field private mPreviousViewState:Lcom/sec/android/gallery3d/app/ActivityState;

.field private mRcsUri:Z

.field private final mResultIntent:Landroid/content/Intent;

.field private final mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

.field private mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

.field private mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mSetPathString:Ljava/lang/String;

.field private mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

.field private mShowBars:Z

.field private mShowSideMirrorStartTime:J

.field private mShowSlideshowsettingsMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mShowSpinner:Z

.field mSideMirrorAnim:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;

.field private final mSideMirrorListener:Lcom/sec/android/gallery3d/ui/SideMirrorView$Listener;

.field private mSideMirrorMotionView:Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;

.field private final mSideMirrorUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

.field private mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

.field private mSideMirrorViewOnShowListener:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorViewOnShowListener;

.field private mSideMirrorX:F

.field private mSingleMode:Z

.field private mSlideShowPlaySpeed:I

.field private mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

.field private mStartInFilmstrip:Z

.field private mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

.field private mTagType:Lcom/sec/samsung/gallery/core/TabTagType;

.field private mTempUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;

.field private mTreatBackAsUp:Z

.field private mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;

.field private final mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

.field private mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

.field private mWindowHasFocus:Z

.field private mWindowSize:Landroid/graphics/Point;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 248
    const-class v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TAG:Ljava/lang/String;

    .line 283
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v0

    sput v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->DOUBLE_TAP_TIMEOUT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 243
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ActivityState;-><init>()V

    .line 319
    const-string v0, "VIEW"

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->VIEW:Ljava/lang/String;

    .line 320
    const-string v0, "AIRB"

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->AIRB:Ljava/lang/String;

    .line 335
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    .line 348
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    .line 349
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHdmi3D:Lcom/sec/android/gallery3d/ui/Hdmi3D;

    .line 354
    iput-wide v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowSideMirrorStartTime:J

    .line 355
    iput v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I

    .line 356
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBars:Z

    .line 357
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarAllowed:Z

    .line 360
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 361
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mInitialItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 374
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeferUpdateUntil:J

    .line 380
    new-array v0, v4, [Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNfcPushUris:[Landroid/net/Uri;

    .line 381
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$MyMenuVisibilityListener;

    invoke-direct {v0, p0, v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$MyMenuVisibilityListener;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMenuVisibilityListener:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$MyMenuVisibilityListener;

    .line 384
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsBack:Z

    .line 385
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsLongPressFinished:Z

    .line 386
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mLockTouchEvent:Z

    .line 388
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mResultIntent:Landroid/content/Intent;

    .line 389
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsHiddenItem:Z

    .line 390
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentMediaSetIndex:I

    .line 391
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFirstTimeLaunch:Z

    .line 396
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsPopupDialogDisplayed:Z

    .line 397
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mContentObserver:Landroid/database/ContentObserver;

    .line 398
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 400
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMenu:Landroid/view/Menu;

    .line 401
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPreviousViewState:Lcom/sec/android/gallery3d/app/ActivityState;

    .line 403
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSingleMode:Z

    .line 404
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPlayVideoWhenLocked:Z

    .line 405
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mBrokenImage:Landroid/graphics/Bitmap;

    .line 410
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mBackByZoomOut:Z

    .line 411
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    .line 412
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFromGalleryWidget:Z

    .line 418
    iput-wide v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGroupIndex:J

    .line 419
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mClusterAlbumName:Ljava/lang/String;

    .line 421
    iput v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHelpMode:I

    .line 425
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowPlaySpeed:I

    .line 428
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    .line 429
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsFinishingState:Z

    .line 431
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    .line 433
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsStartPhotoNote:Z

    .line 440
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsConnectedSideSync:Z

    .line 441
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCheckSideSyncNeeded:Z

    .line 442
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsEmergencyMode:Z

    .line 443
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFromWeTogether:Z

    .line 444
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;

    invoke-direct {v0, p0, v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mTempUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;

    .line 454
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isLongPressKey:Z

    .line 460
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 462
    const/16 v0, 0x12c

    iput v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDnieDisableTimeout:I

    .line 463
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mWindowHasFocus:Z

    .line 464
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripCurrentProgress:F

    .line 505
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

    .line 2994
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$8;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$8;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorViewOnShowListener:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorViewOnShowListener;

    .line 3005
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$9;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$9;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    .line 3025
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$10;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$10;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAirMotionSettingsReceiver:Landroid/content/BroadcastReceiver;

    .line 3038
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$11;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$11;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlHoverGestureListener:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$SimpleOnGestureListener;

    .line 3095
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;

    const-string v1, "PHOTO_PAGE"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$12;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoPageViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 3205
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$13;

    const-string v1, "SLIDESHOW_SETTINGS"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$13;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowSlideshowsettingsMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 3230
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$14;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$14;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDrmPopupListener:Lcom/sec/android/gallery3d/util/ImageDRMUtil$DrmPopupListener;

    .line 3248
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$15;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$15;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOnGenericMotionListener:Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;

    .line 3269
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$16;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$16;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOnSystemGenericMotionListener:Landroid/view/View$OnGenericMotionListener;

    .line 3300
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$17;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$17;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    .line 3343
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$18;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$18;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    .line 3367
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$19;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$19;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    .line 3431
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$20;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$20;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorListener:Lcom/sec/android/gallery3d/ui/SideMirrorView$Listener;

    .line 5066
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mWindowSize:Landroid/graphics/Point;

    .line 5353
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$28;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$28;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHdmiListener:Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;

    .line 5585
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$30;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    .line 5761
    return-void
.end method

.method private TTSPhotoReader()V
    .locals 27

    .prologue
    .line 5882
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 5955
    :cond_0
    :goto_0
    return-void

    .line 5884
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    if-eqz v3, :cond_0

    .line 5885
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v20

    .line 5886
    .local v20, "mContext":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v18

    .line 5887
    .local v18, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    const-string v10, ""

    .line 5888
    .local v10, "mimeType":Ljava/lang/String;
    const-string v11, ""

    .line 5889
    .local v11, "date":Ljava/lang/String;
    const-string v12, ""

    .line 5890
    .local v12, "location":Ljava/lang/String;
    const-string v13, ""

    .line 5891
    .local v13, "weather":Ljava/lang/String;
    const-string v14, ""

    .line 5892
    .local v14, "category":Ljava/lang/String;
    const-string v15, ""

    .line 5893
    .local v15, "faces":Ljava/lang/String;
    const-string v21, ""

    .line 5895
    .local v21, "photoReader":Ljava/lang/String;
    if-eqz v18, :cond_0

    .line 5897
    if-eqz v10, :cond_2

    .line 5898
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x2f

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v23

    .line 5899
    .local v23, "slashPos":I
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move/from16 v0, v23

    invoke-virtual {v3, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 5901
    .end local v23    # "slashPos":I
    :cond_2
    if-nez v10, :cond_3

    const v3, 0x7f0e00ff

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 5905
    :cond_3
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v4

    move-object/from16 v0, v20

    invoke-static {v0, v4, v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v11

    .line 5909
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatitude()D

    move-result-wide v4

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaItem;->getLongitude()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 5910
    new-instance v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder;

    move-object/from16 v0, v20

    invoke-direct {v8, v0}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;-><init>(Landroid/content/Context;)V

    .line 5911
    .local v8, "geocoder":Lcom/sec/android/gallery3d/util/ReverseGeocoder;
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatitude()D

    move-result-wide v4

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaItem;->getLongitude()D

    move-result-wide v6

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->generateName(DDLcom/sec/android/gallery3d/util/ReverseGeocoder;)Ljava/lang/String;

    move-result-object v12

    .line 5918
    .end local v8    # "geocoder":Lcom/sec/android/gallery3d/util/ReverseGeocoder;
    :cond_4
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v3, :cond_8

    .line 5919
    const-string v3, "external"

    invoke-static {v3}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 5920
    .local v2, "FILES_URI":Landroid/net/Uri;
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    .line 5921
    .local v24, "strBuilder":Ljava/lang/StringBuilder;
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v19

    .line 5922
    .local v19, "itemUri":Ljava/lang/String;
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getCategoryType(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 5923
    .local v16, "categoryType":Ljava/lang/String;
    if-eqz v16, :cond_8

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_8

    .line 5924
    const/16 v17, 0x0

    .line 5925
    .local v17, "count":I
    new-instance v25, Ljava/util/StringTokenizer;

    const-string v3, ","

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 5926
    .local v25, "token":Ljava/util/StringTokenizer;
    :goto_1
    invoke-virtual/range {v25 .. v25}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 5927
    invoke-virtual/range {v25 .. v25}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v26

    .line 5928
    .local v26, "type":Ljava/lang/String;
    if-eqz v17, :cond_5

    .line 5929
    const-string v3, ","

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5931
    :cond_5
    sget-object v3, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->CATEGORY_STRING_MAP:Ljava/util/LinkedHashMap;

    move-object/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v22

    .line 5932
    .local v22, "resId":I
    if-eqz v22, :cond_6

    .line 5933
    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5935
    :cond_6
    add-int/lit8 v17, v17, 0x1

    .line 5936
    goto :goto_1

    .line 5937
    .end local v22    # "resId":I
    .end local v26    # "type":Ljava/lang/String;
    :cond_7
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 5942
    .end local v2    # "FILES_URI":Landroid/net/Uri;
    .end local v16    # "categoryType":Ljava/lang/String;
    .end local v17    # "count":I
    .end local v19    # "itemUri":Ljava/lang/String;
    .end local v24    # "strBuilder":Ljava/lang/StringBuilder;
    .end local v25    # "token":Ljava/util/StringTokenizer;
    :cond_8
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaItem;->getFaces()[Lcom/sec/android/gallery3d/data/Face;

    move-result-object v3

    if-eqz v3, :cond_9

    .line 5943
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaItem;->getFaces()[Lcom/sec/android/gallery3d/data/Face;

    move-result-object v3

    array-length v3, v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_a

    .line 5944
    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e00cb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    :cond_9
    :goto_2
    move-object/from16 v9, p0

    .line 5950
    invoke-direct/range {v9 .. v15}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setPhotoReader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 5952
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5945
    :cond_a
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaItem;->getFaces()[Lcom/sec/android/gallery3d/data/Face;

    move-result-object v3

    array-length v3, v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_9

    .line 5946
    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e00cc

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaItem;->getFaces()[Lcom/sec/android/gallery3d/data/Face;

    move-result-object v7

    array-length v7, v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    goto :goto_2
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    return-object v0
.end method

.method static synthetic access$10000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$10100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    .prologue
    .line 243
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->postGallery(Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V

    return-void
.end method

.method static synthetic access$10200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$10300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$10400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$10500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$10600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 243
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->startSoundScene(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$10700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$10800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->initSideMirrorMotionView()V

    return-void
.end method

.method static synthetic access$10900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$11000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$11100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$11200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->AIRB:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$11300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$11400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$11500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$11600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    return-object v0
.end method

.method static synthetic access$11700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    return-object v0
.end method

.method static synthetic access$11800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$11900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/FilterDeleteSet;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    return-object v0
.end method

.method static synthetic access$12000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 243
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->addItemsToSlideshowProxy(Z)V

    return-void
.end method

.method static synthetic access$1202(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/android/gallery3d/data/FilterDeleteSet;)Lcom/sec/android/gallery3d/data/FilterDeleteSet;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    .prologue
    .line 243
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    return-object p1
.end method

.method static synthetic access$12100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateSaveButtonState()V

    return-void
.end method

.method static synthetic access$12200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->exitSelectionMode()V

    return-void
.end method

.method static synthetic access$12300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$12400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isActionbarShowing()Z

    move-result v0

    return v0
.end method

.method static synthetic access$12500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$12600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoViewIcon;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    return-object v0
.end method

.method static synthetic access$12702(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # I

    .prologue
    .line 243
    iput p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowPlaySpeed:I

    return p1
.end method

.method static synthetic access$12800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$12900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->handleDismissDialogs()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$13000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->startMoreInfo()V

    return-void
.end method

.method static synthetic access$13100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->stopSoundScene()V

    return-void
.end method

.method static synthetic access$13200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOriginalSetPathString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$13300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$13400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # Ljava/lang/Boolean;

    .prologue
    .line 243
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateSelectionMode(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$13502(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 243
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsPopupDialogDisplayed:Z

    return p1
.end method

.method static synthetic access$13600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # I

    .prologue
    .line 243
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->refreshHidingMessage(I)V

    return-void
.end method

.method static synthetic access$13700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$13800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$13900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSLeft:I

    return v0
.end method

.method static synthetic access$14000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    return-object v0
.end method

.method static synthetic access$14002(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    .prologue
    .line 243
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    return-object p1
.end method

.method static synthetic access$1402(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # I

    .prologue
    .line 243
    iput p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSLeft:I

    return p1
.end method

.method static synthetic access$14100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$14200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/GalleryApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    return-object v0
.end method

.method static synthetic access$14300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$14400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    return-object v0
.end method

.method static synthetic access$14500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mBrokenImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$14502(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 243
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mBrokenImage:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$14600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$14700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TTSPhotoReader()V

    return-void
.end method

.method static synthetic access$14802(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 243
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsInteracting:Z

    return p1
.end method

.method static synthetic access$14900()I
    .locals 1

    .prologue
    .line 243
    sget v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->DOUBLE_TAP_TIMEOUT:I

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSTop:I

    return v0
.end method

.method static synthetic access$15000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/utils/EditModeHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # I

    .prologue
    .line 243
    iput p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSTop:I

    return p1
.end method

.method static synthetic access$15100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->removeHideSideMirrorMessage()V

    return-void
.end method

.method static synthetic access$15200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->refreshHideSideMirrorMessage()V

    return-void
.end method

.method static synthetic access$15300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    return v0
.end method

.method static synthetic access$15400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$15500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$15600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$15700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$15800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$15900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSRight:I

    return v0
.end method

.method static synthetic access$16000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # I

    .prologue
    .line 243
    iput p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSRight:I

    return p1
.end method

.method static synthetic access$16100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHasCameraScreennailOrPlaceholder:Z

    return v0
.end method

.method static synthetic access$16200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$16300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$16400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateMenuOperations()V

    return-void
.end method

.method static synthetic access$16500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$16600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AlbumReloader;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    return-object v0
.end method

.method static synthetic access$16700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->showChangePlayerDialog()V

    return-void
.end method

.method static synthetic access$16800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$16900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSBottom:I

    return v0
.end method

.method static synthetic access$17002(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # I

    .prologue
    .line 243
    iput p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDnieDisableTimeout:I

    return p1
.end method

.method static synthetic access$1702(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # I

    .prologue
    .line 243
    iput p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSBottom:I

    return p1
.end method

.method static synthetic access$17100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$17200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$17300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$17400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$17500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->startBurstPlay()V

    return-void
.end method

.method static synthetic access$17600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->startSlideShow()V

    return-void
.end method

.method static synthetic access$17700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/GLView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

    return-object v0
.end method

.method static synthetic access$17800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$17900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->startDelete()V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/SideMirrorView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    return-object v0
.end method

.method static synthetic access$18000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$18100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$18200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$18300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$18400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$18500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$18600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$18700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$18800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$18900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$19000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$19100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$19200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$19300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$19400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$19500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorX:F

    return v0
.end method

.method static synthetic access$2002(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # F

    .prologue
    .line 243
    iput p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorX:F

    return p1
.end method

.method static synthetic access$202(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 243
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsMenuVisible:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorMotionView:Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOutOfFocusShotHelpView:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsStartPhotoNote:Z

    return v0
.end method

.method static synthetic access$2802(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 243
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsStartPhotoNote:Z

    return p1
.end method

.method static synthetic access$2900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/anim/FloatAnimation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/android/gallery3d/anim/FloatAnimation;)Lcom/sec/android/gallery3d/anim/FloatAnimation;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/anim/FloatAnimation;

    .prologue
    .line 243
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->showBarsForAShortTime()V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripCurrentProgress:F

    return v0
.end method

.method static synthetic access$3002(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # F

    .prologue
    .line 243
    iput p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripCurrentProgress:F

    return p1
.end method

.method static synthetic access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 243
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->playCurrentMedia(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isLongPressKey:Z

    return v0
.end method

.method static synthetic access$4102(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 243
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isLongPressKey:Z

    return p1
.end method

.method static synthetic access$4200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I

    return v0
.end method

.method static synthetic access$4202(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # I

    .prologue
    .line 243
    iput p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I

    return p1
.end method

.method static synthetic access$4300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$4700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHoverGestureDetector:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    return-object v0
.end method

.method static synthetic access$4800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mLockTouchEvent:Z

    return v0
.end method

.method static synthetic access$4802(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 243
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mLockTouchEvent:Z

    return p1
.end method

.method static synthetic access$4900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/DataManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$5000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 243
    sget-object v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$5200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOnGenericMotionListener:Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;

    return-object v0
.end method

.method static synthetic access$5300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->hideBars()V

    return-void
.end method

.method static synthetic access$5400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateBars()V

    return-void
.end method

.method static synthetic access$5500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->wantBars()V

    return-void
.end method

.method static synthetic access$5600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$5700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-wide v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeferUpdateUntil:J

    return-wide v0
.end method

.method static synthetic access$5802(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 243
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeferredUpdateWaiting:Z

    return p1
.end method

.method static synthetic access$5900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateUIForCurrentPhoto()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$6000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$6100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->launchCamera()V

    return-void
.end method

.method static synthetic access$6200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    return-object v0
.end method

.method static synthetic access$6300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 243
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateCurrentPhoto(Lcom/sec/android/gallery3d/data/MediaItem;)V

    return-void
.end method

.method static synthetic access$6400(Landroid/net/Uri;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Landroid/net/Uri;

    .prologue
    .line 243
    invoke-static {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->createSharePanoramaIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6500(Lcom/sec/android/gallery3d/data/MediaObject;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 243
    invoke-static {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->createShareIntent(Lcom/sec/android/gallery3d/data/MediaObject;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/GalleryActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBar:Lcom/sec/android/gallery3d/app/GalleryActionBar;

    return-object v0
.end method

.method static synthetic access$6700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Landroid/net/Uri;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 243
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setNfcBeamPushUri(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$6800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 243
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updatePanoramaUI(Z)V

    return-void
.end method

.method static synthetic access$6900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setDetailActionBarForNormal()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/ImageNoteView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    return-object v0
.end method

.method static synthetic access$7000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$7100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$7200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$7300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$7400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$7500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Landroid/content/res/Configuration;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # Landroid/content/res/Configuration;

    .prologue
    .line 243
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateViewModeIcons(Landroid/content/res/Configuration;)V

    return-void
.end method

.method static synthetic access$7600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mResultIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$7700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->refreshHidingMessage()V

    return-void
.end method

.method static synthetic access$7802(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # J

    .prologue
    .line 243
    iput-wide p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAgifVersion:J

    return-wide p1
.end method

.method static synthetic access$7900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIndexEnabled:Z

    return v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$8000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$8100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/GalleryCoverMode;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    return-object v0
.end method

.method static synthetic access$8200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;ILandroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/content/Intent;

    .prologue
    .line 243
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setStateResult(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$8300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->finishDetailView()V

    return-void
.end method

.method static synthetic access$8400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/StateManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    return-object v0
.end method

.method static synthetic access$8500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 243
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->initAgif(Lcom/sec/android/gallery3d/data/MediaItem;)V

    return-void
.end method

.method static synthetic access$8600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$8700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsResumeManualFaceTag:Z

    return v0
.end method

.method static synthetic access$8800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setManualFaceTag()V

    return-void
.end method

.method static synthetic access$8900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$9000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$9100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$9200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/core/GalleryFacade;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    return-object v0
.end method

.method static synthetic access$9300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateCountOnActionBar()V

    return-void
.end method

.method static synthetic access$9400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsActive:Z

    return v0
.end method

.method static synthetic access$9500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBars:Z

    return v0
.end method

.method static synthetic access$9600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    return-object v0
.end method

.method static synthetic access$9700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->initSideMirrorView()V

    return-void
.end method

.method static synthetic access$9800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorViewOnShowListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorViewOnShowListener:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorViewOnShowListener;

    return-object v0
.end method

.method static synthetic access$9900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCameraQuickViewTimer:Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;

    return-object v0
.end method

.method private addItemsToSlideshowProxy(Z)V
    .locals 14
    .param p1, "isBurst"    # Z

    .prologue
    .line 4251
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSlideShowSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v9

    .line 4252
    .local v9, "slideShowProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    const/4 v0, 0x0

    .line 4253
    .local v0, "count":I
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v10, :cond_9

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v10

    if-eqz v10, :cond_9

    .line 4254
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 4256
    if-gtz v0, :cond_0

    .line 4257
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v11, 0x7f0e006d

    invoke-static {v10, v11}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 4258
    const/4 v10, 0x0

    invoke-static {v10}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->setBurstMode(Z)V

    .line 4362
    :goto_0
    return-void

    .line 4262
    :cond_0
    const/4 v8, 0x0

    .local v8, "position":I
    :goto_1
    if-ge v8, v0, :cond_4

    .line 4264
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v10, v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 4266
    .local v4, "media":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v10, v4, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v10, :cond_1

    .line 4267
    check-cast v4, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    .end local v4    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;

    move-result-object v4

    .line 4270
    .restart local v4    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    instance-of v10, v4, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-nez v10, :cond_2

    instance-of v10, v4, Lcom/sec/android/gallery3d/data/ActionImage;

    if-nez v10, :cond_2

    instance-of v10, v4, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-eqz v10, :cond_3

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v10

    const/4 v11, 0x4

    if-ne v10, v11, :cond_3

    .line 4262
    :cond_2
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 4274
    :cond_3
    invoke-virtual {v9, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_2

    .line 4277
    .end local v4    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_4
    if-eqz p1, :cond_8

    .line 4278
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v10

    invoke-direct {v3, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 4280
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 4281
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v10}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v10

    long-to-int v1, v10

    .line 4282
    .local v1, "groupNum":I
    const/4 v2, 0x0

    .local v2, "i":I
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    .local v6, "n":I
    :goto_3
    if-ge v2, v6, :cond_7

    .line 4283
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 4285
    .local v7, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v7, :cond_5

    instance-of v10, v7, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v10, :cond_5

    move-object v5, v7

    .line 4286
    check-cast v5, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 4287
    .local v5, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v10, v5, Lcom/sec/android/gallery3d/data/ActionImage;

    if-eqz v10, :cond_6

    .line 4282
    .end local v5    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_5
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 4290
    .restart local v5    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_6
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v10

    int-to-long v12, v1

    cmp-long v10, v10, v12

    if-nez v10, :cond_5

    .line 4291
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/gallery3d/data/MediaObject;

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_4

    .line 4296
    .end local v5    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v7    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_7
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->startBurstPlay()V

    goto :goto_0

    .line 4298
    .end local v1    # "groupNum":I
    .end local v2    # "i":I
    .end local v3    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    .end local v6    # "n":I
    :cond_8
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->startSlideShow()V

    goto :goto_0

    .line 4301
    .end local v8    # "position":I
    :cond_9
    new-instance v10, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v12, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;

    invoke-direct {v12, p0, p1, v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;ZLcom/sec/android/gallery3d/ui/SelectionManager;)V

    iget-object v13, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getTotalMediaItemCount()I

    move-result v13

    invoke-direct {v10, v11, v12, v13}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;I)V

    const/4 v11, 0x1

    new-array v11, v11, [Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    aput-object v13, v11, v12

    invoke-virtual {v10, v11}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0
.end method

.method private canShowBars()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1916
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarAllowed:Z

    if-nez v3, :cond_1

    .line 1924
    :cond_0
    :goto_0
    return v1

    .line 1919
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 1920
    .local v0, "config":Landroid/content/res/Configuration;
    iget v3, v0, Landroid/content/res/Configuration;->touchscreen:I

    if-eq v3, v2, :cond_0

    move v1, v2

    .line 1924
    goto :goto_0
.end method

.method private checkIndexShouldShow()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4467
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;

    if-eqz v2, :cond_1

    .line 4468
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 4469
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setIndexEnabled(Z)V

    .line 4475
    :goto_0
    return v0

    .line 4472
    :cond_0
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setIndexEnabled(Z)V

    :cond_1
    move v0, v1

    .line 4475
    goto :goto_0
.end method

.method private clearFlagShowWhenLocked()V
    .locals 2

    .prologue
    .line 5778
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPlayVideoWhenLocked:Z

    if-eqz v0, :cond_0

    .line 5779
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPlayVideoWhenLocked:Z

    .line 5784
    :goto_0
    return-void

    .line 5782
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 5783
    iget v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFlags:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFlags:I

    goto :goto_0
.end method

.method private closeMoreInfo()V
    .locals 3

    .prologue
    .line 5634
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    if-nez v0, :cond_0

    .line 5642
    :goto_0
    return-void

    .line 5636
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->onDestroy()V

    .line 5637
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    .line 5638
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->initActionBar()V

    .line 5639
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isActionbarShowing()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->setCurrentPhotoForCA(Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    .line 5640
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->registerEAM(Landroid/content/BroadcastReceiver;)V

    .line 5641
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->onResume()V

    goto :goto_0
.end method

.method private closeSlideShowSetting()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 5836
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->setSlideshowSettingEnable(Z)V

    .line 5837
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1, v4}, Lcom/sec/android/gallery3d/app/GalleryApp;->setSlideShowMode(Z)V

    .line 5838
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->registerEAM(Landroid/content/BroadcastReceiver;)V

    .line 5839
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5840
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v2, v3, v4}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Z)V

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 5841
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    sget v2, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_DEFAULT:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->setShowBarMode(I)V

    .line 5842
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateCountOnActionBar()V

    .line 5857
    :goto_0
    return-void

    .line 5843
    :cond_0
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    if-eqz v1, :cond_3

    .line 5844
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    .line 5845
    .local v0, "currentLunchMode":Lcom/sec/samsung/gallery/core/LaunchModeType;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v1, :cond_2

    .line 5847
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 5848
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    sget v2, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_DEFAULT:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->setShowBarMode(I)V

    .line 5849
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    goto :goto_0

    .line 5851
    :cond_2
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    .line 5852
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setDetailActionBarForNormal()V

    goto :goto_0

    .line 5855
    .end local v0    # "currentLunchMode":Lcom/sec/samsung/gallery/core/LaunchModeType;
    :cond_3
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setDetailActionBarForNormal()V

    goto :goto_0
.end method

.method private cloudSync()V
    .locals 1

    .prologue
    .line 4365
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCloud:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->isCloudContentSyncOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4366
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->cloudAlbumsSync(Landroid/content/Context;)V

    .line 4368
    :cond_0
    return-void
.end method

.method private computeAddressCA(Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;Lcom/sec/android/gallery3d/util/ReverseGeocoder;)Landroid/location/Address;
    .locals 20
    .param p1, "set"    # Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;
    .param p2, "geocoder"    # Lcom/sec/android/gallery3d/util/ReverseGeocoder;

    .prologue
    .line 4900
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLatitude:D

    .line 4901
    .local v4, "setMinLatitude":D
    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLongitude:D

    .line 4902
    .local v6, "setMinLongitude":D
    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLatitude:D

    .line 4903
    .local v10, "setMaxLatitude":D
    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLongitude:D

    .line 4904
    .local v12, "setMaxLongitude":D
    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLatitude:D

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLatitude:D

    move-wide/from16 v16, v0

    sub-double v8, v8, v16

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLongitude:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLongitude:D

    move-wide/from16 v18, v0

    sub-double v16, v16, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->abs(D)D

    move-result-wide v16

    cmpg-double v3, v8, v16

    if-gez v3, :cond_0

    .line 4906
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLatitude:D

    .line 4907
    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLongitude:D

    .line 4908
    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLatitude:D

    .line 4909
    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLongitude:D

    .line 4911
    :cond_0
    const/4 v8, 0x1

    move-object/from16 v3, p2

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->lookupAddress(DDZ)Landroid/location/Address;

    move-result-object v2

    .line 4912
    .local v2, "addr1":Landroid/location/Address;
    const/4 v14, 0x1

    move-object/from16 v9, p2

    invoke-virtual/range {v9 .. v14}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->lookupAddress(DDZ)Landroid/location/Address;

    move-result-object v15

    .line 4913
    .local v15, "addr2":Landroid/location/Address;
    if-nez v2, :cond_2

    move-object v2, v15

    .line 4918
    .end local v2    # "addr1":Landroid/location/Address;
    :cond_1
    :goto_0
    return-object v2

    .line 4915
    .restart local v2    # "addr1":Landroid/location/Address;
    :cond_2
    if-nez v15, :cond_1

    goto :goto_0
.end method

.method private static createShareIntent(Lcom/sec/android/gallery3d/data/MediaObject;)Landroid/content/Intent;
    .locals 4
    .param p0, "mediaObject"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 1653
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaObject;->getMediaType()I

    move-result v0

    .line 1654
    .local v0, "type":I
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->getMimeType(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method private static createSharePanoramaIntent(Landroid/net/Uri;)Landroid/content/Intent;
    .locals 2
    .param p0, "contentUri"    # Landroid/net/Uri;

    .prologue
    .line 1661
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "application/vnd.google.panorama360+jpg"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private editBurstShot()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 5322
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->makeBurstShotList()Ljava/util/ArrayList;

    move-result-object v0

    .line 5324
    .local v0, "burstShotList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v3, v1, v2

    aput-object v0, v1, v4

    .line 5327
    .local v1, "params":[Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 5328
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gt v2, v4, :cond_1

    .line 5329
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0e02e4

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 5333
    :cond_0
    :goto_0
    return-void

    .line 5331
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v3, "SHOW_EDIT_BUSRT_SHOT_DIALOG"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private enableDnieMode(ZZ)V
    .locals 6
    .param p1, "isEnable"    # Z
    .param p2, "delayed"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 4409
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 4410
    .local v0, "handler":Landroid/os/Handler;
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/Object;

    if-eqz p1, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v1, v3

    .line 4414
    .local v1, "params":[Ljava/lang/Object;
    if-eqz p2, :cond_1

    .line 4415
    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$24;

    invoke-direct {v2, p0, p1, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$24;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;ZLjava/lang/Object;)V

    iget v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDnieDisableTimeout:I

    int-to-long v4, v3

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 4426
    :goto_1
    return-void

    .end local v1    # "params":[Ljava/lang/Object;
    :cond_0
    move v2, v4

    .line 4410
    goto :goto_0

    .line 4423
    .restart local v1    # "params":[Ljava/lang/Object;
    :cond_1
    sget-object v2, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "enableDnieMode ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] with no delay"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4424
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v3, "SET_DNIE_MODE"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private exitSelectionMode()V
    .locals 3

    .prologue
    .line 3818
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v1, :cond_0

    .line 3819
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 3820
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->clearSelectedCount()V

    .line 3821
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 3823
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v1, :cond_1

    .line 3824
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->exitSelectionMode()V

    .line 3827
    :cond_1
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v1, :cond_2

    .line 3828
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->unregisterOrientationListener()V

    .line 3830
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getActionBarView()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v0

    .line 3831
    .local v0, "actionbarView":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->selectionModeBarVisibiltiy(IZ)V

    .line 3833
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->initActionBar()V

    .line 3834
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v1, :cond_3

    .line 3835
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->updateBurstShotIcon(Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowPlaySpeed:I

    .line 3836
    :cond_3
    const/16 v1, 0xdac

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->refreshHidingMessage(I)V

    .line 3837
    return-void
.end method

.method private finishDetailView()V
    .locals 2

    .prologue
    .line 4073
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsFinishingState:Z

    .line 4079
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/HelpImage;

    if-eqz v0, :cond_0

    .line 4080
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->finish()V

    .line 4101
    :goto_0
    return-void

    .line 4084
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$22;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$22;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private generateName(DDLcom/sec/android/gallery3d/util/ReverseGeocoder;)Ljava/lang/String;
    .locals 13
    .param p1, "lat"    # D
    .param p3, "lng"    # D
    .param p5, "geocoder"    # Lcom/sec/android/gallery3d/util/ReverseGeocoder;

    .prologue
    .line 4864
    new-instance v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;

    invoke-direct {v8}, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;-><init>()V

    .line 4865
    .local v8, "set":Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;
    move-wide v4, p1

    .line 4866
    .local v4, "itemLatitude":D
    move-wide/from16 v6, p3

    .line 4867
    .local v6, "itemLongitude":D
    const/4 v3, 0x0

    .line 4869
    .local v3, "name":Ljava/lang/String;
    iget-wide v10, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLatitude:D

    cmpl-double v9, v10, v4

    if-lez v9, :cond_0

    .line 4870
    iput-wide v4, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLatitude:D

    .line 4871
    iput-wide v6, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLongitude:D

    .line 4873
    :cond_0
    iget-wide v10, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLatitude:D

    cmpg-double v9, v10, v4

    if-gez v9, :cond_1

    .line 4874
    iput-wide v4, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLatitude:D

    .line 4875
    iput-wide v6, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLongitude:D

    .line 4877
    :cond_1
    iget-wide v10, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLongitude:D

    cmpl-double v9, v10, v6

    if-lez v9, :cond_2

    .line 4878
    iput-wide v4, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLatitude:D

    .line 4879
    iput-wide v6, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLongitude:D

    .line 4881
    :cond_2
    iget-wide v10, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLongitude:D

    cmpg-double v9, v10, v6

    if-gez v9, :cond_3

    .line 4882
    iput-wide v4, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLatitude:D

    .line 4883
    iput-wide v6, v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLongitude:D

    .line 4885
    :cond_3
    move-object/from16 v0, p5

    invoke-direct {p0, v8, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->computeAddressCA(Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;Lcom/sec/android/gallery3d/util/ReverseGeocoder;)Landroid/location/Address;

    move-result-object v2

    .line 4887
    .local v2, "addr":Landroid/location/Address;
    if-eqz v2, :cond_4

    .line 4888
    invoke-virtual {v2}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_5

    .line 4889
    invoke-virtual {v2}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    move-result-object v3

    .line 4896
    :cond_4
    :goto_0
    return-object v3

    .line 4890
    :cond_5
    invoke-virtual {v2}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_6

    .line 4891
    invoke-virtual {v2}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 4893
    :cond_6
    invoke-virtual {v2}, Landroid/location/Address;->getCountryName()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private static getMediaTypeString(Lcom/sec/android/gallery3d/data/MediaItem;)Ljava/lang/String;
    .locals 2
    .param p0, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 2980
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 2981
    const-string v0, "Video"

    .line 2985
    :goto_0
    return-object v0

    .line 2982
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 2983
    const-string v0, "Photo"

    goto :goto_0

    .line 2985
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getNearbyClient()Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 5375
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNearby:Z

    if-nez v2, :cond_1

    .line 5382
    :cond_0
    :goto_0
    return-object v0

    .line 5377
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const-string v3, "nearby"

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/data/DataManager;->getSource(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSource;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    .line 5378
    .local v1, "nearbySource":Lcom/sec/android/gallery3d/remote/nearby/NearbySource;
    if-eqz v1, :cond_0

    .line 5380
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->getNearbyContext()Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->getNearbyClient()Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v0

    .line 5381
    .local v0, "nearbyClient":Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->addPlayerStateListener(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$IOnPlayerStateListener;)V

    goto :goto_0
.end method

.method private getTopSetPathByFilter()Ljava/lang/String;
    .locals 5

    .prologue
    .line 4140
    const-string v0, ""

    .line 4141
    .local v0, "basePath":Ljava/lang/String;
    const/4 v2, 0x0

    .line 4142
    .local v2, "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    .line 4143
    .local v3, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v3, :cond_0

    .line 4144
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    .line 4145
    :cond_0
    if-eqz v2, :cond_1

    .line 4146
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    .line 4148
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-static {v4}, Lcom/sec/android/gallery3d/app/FilterUtils;->toClusterType(Lcom/sec/samsung/gallery/core/TabTagType;)I

    move-result v1

    .line 4149
    .local v1, "clusterType":I
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/app/FilterUtils;->newClusterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private handle3DTour(Lcom/sec/android/gallery3d/data/MediaItem;Z)V
    .locals 3
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "withSound"    # Z

    .prologue
    .line 5130
    if-eqz p2, :cond_0

    .line 5131
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 5133
    :cond_0
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 5136
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "PLAY_3DTOUR"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 5137
    return-void
.end method

.method private handleBurstPlaySetting()V
    .locals 3

    .prologue
    .line 5107
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    .line 5110
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "SHOW_BURST_SHOT_PLAY_SETTING_DIALOG"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 5111
    return-void
.end method

.method private handleCrop(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 3
    .param p1, "current"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 4168
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v1, v1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v1, :cond_0

    .line 4169
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v1, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalImage;->removeManualFD()V

    .line 4173
    :cond_0
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 4176
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "CROP_IMAGE"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 4177
    return-void
.end method

.method private handleCropSamsungLink(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 4180
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/controller/CropImageCmd;->startCropAndDeleteActivity(Landroid/app/Activity;Landroid/net/Uri;)V

    .line 4181
    return-void
.end method

.method private handleDismissDialogs()V
    .locals 5

    .prologue
    .line 4390
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-nez v4, :cond_1

    .line 4406
    :cond_0
    return-void

    .line 4392
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCloneMediaList()Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 4393
    .local v3, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v4, v3, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v4, :cond_2

    move-object v2, v3

    .line 4394
    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 4395
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 4396
    new-instance v0, Ljava/io/File;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 4397
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_2

    .line 4398
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->dismissDialogs()V

    .line 4399
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 4400
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4, v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_0
.end method

.method private handleGoUpButton()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 5010
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    const-class v7, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/app/StateManager;->setPreviousViewState(Ljava/lang/Class;)V

    .line 5011
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    .line 5012
    .local v0, "activity":Lcom/sec/android/gallery3d/app/GalleryActivity;
    iget-boolean v6, v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mNeedFirstUpOfDetailView:Z

    if-eqz v6, :cond_1

    .line 5013
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 5014
    iput-boolean v5, v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mNeedFirstUpOfDetailView:Z

    .line 5016
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isBackNeeded()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 5017
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->onBackPressed()V

    .line 5059
    :cond_0
    :goto_0
    return-void

    .line 5022
    :cond_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->isEnabled()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 5023
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/StateManager;->getStateCount()I

    move-result v5

    if-ne v5, v4, :cond_0

    .line 5024
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->onBackPressed()V

    goto :goto_0

    .line 5028
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 5030
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->onBackPressed()V

    goto :goto_0

    .line 5034
    :cond_3
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getStateCount()I

    move-result v6

    if-le v6, v4, :cond_4

    .line 5035
    sget-object v6, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v6, v7}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 5039
    :cond_4
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewMode()Ljava/lang/Class;

    move-result-object v3

    .line 5040
    .local v3, "viewState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    const-class v6, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-ne v3, v6, :cond_5

    .line 5041
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    const-class v5, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0

    .line 5042
    :cond_5
    const-class v6, Lcom/sec/samsung/gallery/view/allview/AllViewState;

    if-ne v3, v6, :cond_6

    .line 5043
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->onBackPressed()V

    goto :goto_0

    .line 5044
    :cond_6
    const-class v6, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    if-eq v3, v6, :cond_0

    .line 5047
    const/4 v1, 0x0

    .line 5048
    .local v1, "isAllViewSet":Z
    const/4 v2, 0x0

    .line 5049
    .local v2, "setPath":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    if-eqz v6, :cond_7

    .line 5050
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    .line 5051
    if-eqz v2, :cond_8

    const-string v6, "allinone"

    invoke-virtual {v2, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_8

    move v1, v4

    .line 5053
    :cond_7
    :goto_1
    if-eqz v1, :cond_9

    .line 5054
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->startAllView(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    move v1, v5

    .line 5051
    goto :goto_1

    .line 5056
    :cond_9
    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->startPhotoView(I)V

    goto/16 :goto_0
.end method

.method private handleImageRotation(ILcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 7
    .param p1, "action"    # I
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const v6, 0x7f0f0281

    const/4 v2, 0x1

    .line 4193
    const/4 v0, 0x0

    .line 4194
    .local v0, "bPrepared":Z
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseRotationAnimaionInDetailView:Z

    if-ne v1, v2, :cond_0

    .line 4196
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-ne p1, v6, :cond_3

    move v1, v2

    :goto_0
    const/4 v5, 0x0

    invoke-virtual {v3, v4, v1, v5}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->prepareAnimation(Lcom/sec/android/gallery3d/ui/PhotoView;ZLjava/lang/Object;)Z

    move-result v0

    .line 4197
    if-nez v0, :cond_0

    .line 4198
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetToFullViewNoAnimation()V

    .line 4202
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->setIsRotated(Z)V

    .line 4203
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->hideFaceIndicatorView()V

    .line 4204
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SEARCH:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v1, v3, :cond_1

    .line 4205
    instance-of v1, p2, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v1, :cond_1

    .line 4206
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v1, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/LocalImage;->setRotateForSearch(Z)V

    .line 4210
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    if-ne p1, v6, :cond_4

    const/16 v1, 0x5a

    :goto_1
    invoke-virtual {v2, v3, v1}, Lcom/sec/android/gallery3d/data/DataManager;->rotate(Lcom/sec/android/gallery3d/data/Path;I)V

    .line 4213
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    if-eqz v1, :cond_2

    .line 4214
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryId()I

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->playImageContinuously(ILcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 4216
    :cond_2
    return-void

    .line 4196
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 4210
    :cond_4
    const/16 v1, -0x5a

    goto :goto_1
.end method

.method private handleOutOfFocusImage(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 3
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 5152
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 5155
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "PLAY_OUTOFFOCUS_IMAGE"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 5156
    return-void
.end method

.method private handlePlaySoundScene(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 2
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 5147
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 5148
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->startSoundScene(Ljava/lang/String;)V

    .line 5149
    return-void
.end method

.method private handlePlayVideo(Lcom/sec/android/gallery3d/data/MediaItem;Z)V
    .locals 4
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "withSound"    # Z

    .prologue
    const/4 v3, 0x1

    .line 5114
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSingleMode:Z

    if-eqz v1, :cond_1

    .line 5115
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5127
    :cond_0
    :goto_0
    return-void

    .line 5118
    :cond_1
    if-eqz p2, :cond_2

    .line 5119
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 5121
    :cond_2
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    aput-object p1, v0, v3

    .line 5125
    .local v0, "params":[Ljava/lang/Object;
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPlayVideoWhenLocked:Z

    .line 5126
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "VIDEO_PLAY"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private handleResultCropManualFD(Landroid/content/Intent;)V
    .locals 7
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    .line 5250
    if-eqz p1, :cond_2

    .line 5251
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-nez v4, :cond_0

    .line 5252
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 5253
    .local v3, "uri":Landroid/net/Uri;
    if-eqz v3, :cond_0

    .line 5254
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    .line 5255
    .local v2, "path":Lcom/sec/android/gallery3d/data/Path;
    if-eqz v2, :cond_0

    .line 5256
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;Z)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 5257
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 5261
    .end local v2    # "path":Lcom/sec/android/gallery3d/data/Path;
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_0
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsResumeManualFaceTag:Z

    .line 5263
    const-string v4, "data"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 5264
    .local v0, "bmp":Landroid/graphics/Bitmap;
    const-string v4, "cropped-rect"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    .line 5265
    .local v1, "croppedRect":Landroid/graphics/Rect;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFaceTagFeature:Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;

    invoke-interface {v4, v0, v1}, Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;->cropManualFD(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V

    .line 5267
    if-eqz v0, :cond_1

    .line 5268
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 5269
    const/4 v0, 0x0

    .line 5272
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v4, :cond_2

    .line 5273
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->hideContactPopupView()V

    .line 5275
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    .end local v1    # "croppedRect":Landroid/graphics/Rect;
    :cond_2
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsLongPressFinished:Z

    .line 5276
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v4, :cond_3

    .line 5277
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v4, v6}, Lcom/sec/android/gallery3d/ui/PhotoView;->setIsManualFDResumed(Z)V

    .line 5278
    :cond_3
    return-void
.end method

.method private handleResultGetText(ILandroid/content/Intent;)V
    .locals 1
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 5281
    if-nez p1, :cond_0

    if-eqz p2, :cond_0

    .line 5282
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0, p2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCR;->showOCRFailedDialog(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Landroid/content/Intent;)V

    .line 5284
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v0, :cond_1

    .line 5285
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0, p2}, Lcom/sec/android/gallery3d/ui/PhotoView;->onOCRRecognizeComplete(Landroid/content/Intent;)V

    .line 5286
    :cond_1
    return-void
.end method

.method private handleSequenceCmd(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 3
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 5140
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 5143
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "PLAY_SEQUENCE_IMAGE"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 5144
    return-void
.end method

.method private hideBars()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1871
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1872
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getActionBarView()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->getPopUpMenu()Landroid/widget/ListPopupWindow;

    move-result-object v0

    .line 1873
    .local v0, "listPopupWindow":Landroid/widget/ListPopupWindow;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1874
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 1879
    .end local v0    # "listPopupWindow":Landroid/widget/ListPopupWindow;
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->isDisabledAutoHide()Z

    move-result v3

    if-nez v3, :cond_1

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBars:Z

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsActive:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isExploreByTouchEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1901
    :cond_1
    :goto_0
    return-void

    .line 1884
    :cond_2
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBars:Z

    .line 1885
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->isActionBarShowAuto()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1886
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->hide()V

    .line 1887
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/sec/android/gallery3d/ui/GLRoot;->setLightsOutMode(Z)V

    .line 1888
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    if-eqz v3, :cond_4

    .line 1889
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1890
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBars:Z

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->setShowBarState(Z)V

    .line 1891
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBars:Z

    if-nez v3, :cond_7

    :goto_1
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setFullScreenMode(Z)V

    .line 1893
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getActionBarView()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 1894
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getActionBarView()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v1

    const/16 v3, 0x8

    invoke-virtual {v1, v3, v2}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->selectionModeBarVisibiltiy(IZ)V

    .line 1897
    :cond_5
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->isFilmShowAuto()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1898
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->hide()V

    .line 1900
    :cond_6
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->hideFaceIndicatorView()V

    goto :goto_0

    :cond_7
    move v1, v2

    .line 1891
    goto :goto_1
.end method

.method private initActionBar()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 3896
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionTutorial(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3897
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 3898
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    sget v2, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_MOTION_TUTORIAL:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->setShowBarMode(I)V

    .line 3951
    :goto_0
    return-void

    .line 3899
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionPreview(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3900
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionPreview;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 3901
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    sget v2, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_MOTION_PREVIEW:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->setShowBarMode(I)V

    goto :goto_0

    .line 3902
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionHelp(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3903
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v2, Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 3904
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    sget v2, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_MOTION_HELP:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->setShowBarMode(I)V

    goto :goto_0

    .line 3905
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isCoverMode()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3906
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    sget v2, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_COVER:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->setShowBarMode(I)V

    goto :goto_0

    .line 3907
    :cond_3
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSingleMode:Z

    if-eqz v1, :cond_4

    .line 3908
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 3909
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    sget v2, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_DEFAULT:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->setShowBarMode(I)V

    goto :goto_0

    .line 3910
    :cond_4
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    if-eqz v1, :cond_7

    .line 3911
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    .line 3912
    .local v0, "currentLunchMode":Lcom/sec/samsung/gallery/core/LaunchModeType;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v1

    if-nez v1, :cond_5

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_5

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v1, :cond_6

    .line 3914
    :cond_5
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 3915
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 3920
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    sget v2, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_DEFAULT:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->setShowBarMode(I)V

    goto/16 :goto_0

    .line 3917
    :cond_6
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    .line 3918
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setDetailActionBarForNormal()V

    goto :goto_1

    .line 3921
    .end local v0    # "currentLunchMode":Lcom/sec/samsung/gallery/core/LaunchModeType;
    :cond_7
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 3922
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->initActionBar()V

    .line 3923
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    sget v2, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_MOREINFO:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->setShowBarMode(I)V

    goto/16 :goto_0

    .line 3924
    :cond_8
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCameraQuickViewOnLockscreen(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 3925
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForCameraQuickViewOnLockscreen;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForCameraQuickViewOnLockscreen;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 3926
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    sget v2, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_DEFAULT:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->setShowBarMode(I)V

    goto/16 :goto_0

    .line 3928
    :cond_9
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 3929
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 3931
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 3933
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    sget v2, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_SLIDESHOW_SETTING:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->setShowBarMode(I)V

    goto/16 :goto_0

    .line 3935
    :cond_a
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v2, v3, v4}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Z)V

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 3937
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateCountOnActionBar()V

    .line 3938
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    sget v2, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_DEFAULT:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->setShowBarMode(I)V

    goto/16 :goto_0

    .line 3941
    :cond_b
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 3943
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 3945
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    sget v2, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_SLIDESHOW_SETTING:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->setShowBarMode(I)V

    goto/16 :goto_0

    .line 3947
    :cond_c
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setDetailActionBarForNormal()V

    goto/16 :goto_0
.end method

.method private initAgif(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 7
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v6, 0x0

    .line 4524
    if-eqz p1, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getQuramAGIF()Lcom/quramsoft/agif/QuramAGIF;

    move-result-object v3

    if-nez v3, :cond_1

    .line 4563
    :cond_0
    :goto_0
    return-void

    .line 4527
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getQuramAGIF()Lcom/quramsoft/agif/QuramAGIF;

    move-result-object v3

    invoke-virtual {v3}, Lcom/quramsoft/agif/QuramAGIF;->getQURAMWINKUTIL()Lcom/quramsoft/agif/QURAMWINKUTIL;

    move-result-object v2

    .line 4529
    .local v2, "mQURAMWINKUTIL":Lcom/quramsoft/agif/QURAMWINKUTIL;
    instance-of v3, p1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-nez v3, :cond_2

    instance-of v3, p1, Lcom/sec/android/gallery3d/data/UriImage;

    if-eqz v3, :cond_4

    .line 4530
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v3

    const-string v4, "gif"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 4531
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2, v4, p1, v6}, Lcom/quramsoft/agif/QURAMWINKUTIL;->initAGIF(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;Z)Z

    move-result v4

    invoke-virtual {v3, v4, p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetAgifMode(ZLcom/sec/android/gallery3d/data/MediaItem;)V

    .line 4532
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->invalidate()V

    goto :goto_0

    .line 4534
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMimeType:Ljava/lang/String;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMimeType:Ljava/lang/String;

    const-string v4, "gif"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 4535
    instance-of v3, p1, Lcom/sec/android/gallery3d/data/UriImage;

    if-eqz v3, :cond_7

    .line 4536
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2, v4, p1, v6}, Lcom/quramsoft/agif/QURAMWINKUTIL;->initAGIF(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;Z)Z

    move-result v4

    invoke-virtual {v3, v4, p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetAgifMode(ZLcom/sec/android/gallery3d/data/MediaItem;)V

    goto :goto_0

    .line 4541
    :cond_4
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_8

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v3

    const-string v4, "gif"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 4542
    instance-of v3, p1, Lcom/sec/android/gallery3d/remote/cloud/CloudMediaItem;

    if-eqz v3, :cond_6

    move-object v3, p1

    .line 4543
    check-cast v3, Lcom/sec/android/gallery3d/remote/cloud/CloudMediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/remote/cloud/CloudMediaItem;->getCachedPath()Ljava/lang/String;

    move-result-object v1

    .line 4544
    .local v1, "filePath":Ljava/lang/String;
    if-eqz v1, :cond_5

    .line 4545
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 4546
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 4547
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2, v4, p1, v6}, Lcom/quramsoft/agif/QURAMWINKUTIL;->initAGIF(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;Z)Z

    move-result v4

    invoke-virtual {v3, v4, p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetAgifMode(ZLcom/sec/android/gallery3d/data/MediaItem;)V

    goto/16 :goto_0

    .line 4551
    .end local v0    # "file":Ljava/io/File;
    :cond_5
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3, v6, p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetAgifMode(ZLcom/sec/android/gallery3d/data/MediaItem;)V

    goto/16 :goto_0

    .line 4554
    .end local v1    # "filePath":Ljava/lang/String;
    :cond_6
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    instance-of v5, p1, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    invoke-virtual {v2, v4, p1, v5}, Lcom/quramsoft/agif/QURAMWINKUTIL;->initAGIF(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;Z)Z

    move-result v4

    invoke-virtual {v3, v4, p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetAgifMode(ZLcom/sec/android/gallery3d/data/MediaItem;)V

    .line 4562
    :cond_7
    :goto_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3, v6, p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetAgifMode(ZLcom/sec/android/gallery3d/data/MediaItem;)V

    goto/16 :goto_0

    .line 4556
    :cond_8
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMimeType:Ljava/lang/String;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMimeType:Ljava/lang/String;

    const-string v4, "gif"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 4557
    instance-of v3, p1, Lcom/sec/android/gallery3d/data/UriImage;

    if-eqz v3, :cond_7

    .line 4558
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2, v4, p1, v6}, Lcom/quramsoft/agif/QURAMWINKUTIL;->initAGIF(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;Z)Z

    move-result v4

    invoke-virtual {v3, v4, p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetAgifMode(ZLcom/sec/android/gallery3d/data/MediaItem;)V

    goto :goto_1
.end method

.method private initCameraQuickViewTimer(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    .line 5180
    const-string v2, "Quick_Time"

    const-wide/16 v4, 0x0

    invoke-virtual {p1, v2, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 5182
    .local v0, "duration":J
    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;

    invoke-direct {v2}, Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;-><init>()V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCameraQuickViewTimer:Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;

    .line 5183
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCameraQuickViewTimer:Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;

    invoke-virtual {v2, v0, v1}, Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;->setReviewDuration(J)V

    .line 5184
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCameraQuickViewTimer:Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;

    new-instance v3, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$27;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$27;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;->setOnFinishedListener(Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer$OnFinishedListener;)V

    .line 5190
    return-void
.end method

.method private initFilmStripView()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 3840
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    if-nez v0, :cond_1

    .line 3861
    :cond_0
    :goto_0
    return-void

    .line 3843
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCameraQuickViewOnLockscreen(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v5, v7

    .line 3845
    .local v5, "disableSelectMode":Z
    :goto_1
    new-instance v0, Lcom/sec/android/gallery3d/ui/FilmStripView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mKeyBoardManager:Lcom/sec/android/gallery3d/util/KeyBoardManager;

    iget v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHelpMode:I

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/gallery3d/ui/FilmStripView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/util/KeyBoardManager;IZLcom/sec/android/gallery3d/app/AlbumReloader;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    .line 3848
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/GLView;->addComponent(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 3849
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    iget-wide v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGroupIndex:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setGroupIndex(J)V

    .line 3850
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setListener(Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;)V

    .line 3851
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setUserInteractionListener(Lcom/sec/android/gallery3d/ui/UserInteractionListener;)V

    .line 3852
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOnGenericMotionListener:Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setOnGenericMotionListener(Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;)V

    .line 3853
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    iget v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setFocusIndex(I)V

    .line 3854
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    iget v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setStartIndex(I)V

    .line 3855
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/GLView;->requestLayout()V

    .line 3857
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0, v7}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setVisibility(I)V

    .line 3858
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->isFromCamera()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    if-nez v0, :cond_0

    .line 3859
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->setInitialFilmRender(Z)V

    goto :goto_0

    .line 3843
    .end local v5    # "disableSelectMode":Z
    :cond_2
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    goto :goto_1
.end method

.method private initImageNoteView()V
    .locals 4

    .prologue
    .line 5159
    new-instance v0, Lcom/sec/android/gallery3d/ui/ImageNoteView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/ui/ImageNoteView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    .line 5160
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    new-instance v1, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$26;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$26;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->setImageNoteViewListener(Lcom/sec/android/gallery3d/ui/ImageNoteView$ImageNoteViewListener;)V

    .line 5175
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->setPhotoViewIcon(Lcom/sec/android/gallery3d/ui/PhotoViewIcon;)V

    .line 5176
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/GLView;->addComponent(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 5177
    return-void
.end method

.method private initSideMirrorMotionView()V
    .locals 2

    .prologue
    .line 3888
    new-instance v0, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorMotionView:Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;

    .line 3889
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorMotionView:Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/GLView;->addComponent(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 3890
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorMotionView:Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->setUserInteractionListener(Lcom/sec/android/gallery3d/ui/UserInteractionListener;)V

    .line 3891
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/GLView;->requestLayout()V

    .line 3892
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorMotionView:Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->setVisibility(I)V

    .line 3893
    return-void
.end method

.method private initSideMirrorView()V
    .locals 15

    .prologue
    .line 3864
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/app/Config$SideMirror;->get(Landroid/content/Context;)Lcom/sec/android/gallery3d/app/Config$SideMirror;

    move-result-object v14

    .line 3866
    .local v14, "config":Lcom/sec/android/gallery3d/app/Config$SideMirror;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 3867
    .local v2, "topMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    new-instance v0, Lcom/sec/android/gallery3d/ui/SideMirrorView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget v3, v14, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorTopMargin:I

    iget v4, v14, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorMidMargin:I

    iget v5, v14, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorBottomMargin:I

    iget v6, v14, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorContentSize:I

    iget v7, v14, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorThumbWidth:I

    iget v8, v14, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorThumbWidth:I

    iget v9, v14, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorBarSize:I

    iget v10, v14, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorGripSize:I

    iget v11, v14, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorGripWidth:I

    iget-object v12, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mKeyBoardManager:Lcom/sec/android/gallery3d/util/KeyBoardManager;

    iget v13, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHelpMode:I

    invoke-direct/range {v0 .. v13}, Lcom/sec/android/gallery3d/ui/SideMirrorView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaSet;IIIIIIIIILcom/sec/android/gallery3d/util/KeyBoardManager;I)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    .line 3872
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/GLView;->addComponent(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 3873
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    iget-wide v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGroupIndex:J

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->setGroupIndex(J)V

    .line 3874
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorListener:Lcom/sec/android/gallery3d/ui/SideMirrorView$Listener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->setListener(Lcom/sec/android/gallery3d/ui/SideMirrorView$Listener;)V

    .line 3875
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->setUserInteractionListener(Lcom/sec/android/gallery3d/ui/UserInteractionListener;)V

    .line 3876
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOnGenericMotionListener:Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->setOnGenericMotionListener(Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;)V

    .line 3877
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    iget v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentMediaSetIndex:I

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->setFocusIndex(I)V

    .line 3878
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    iget v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentMediaSetIndex:I

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->setStartIndex(I)V

    .line 3881
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/GLView;->requestLayout()V

    .line 3882
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsActive:Z

    if-eqz v0, :cond_0

    .line 3883
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->resume()V

    .line 3884
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->setVisibility(I)V

    .line 3885
    return-void
.end method

.method private isActionbarShowing()Z
    .locals 4

    .prologue
    .line 3974
    const/4 v2, 0x1

    .line 3976
    .local v2, "isShowing":Z
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    if-nez v3, :cond_1

    .line 3987
    .end local v2    # "isShowing":Z
    :cond_0
    :goto_0
    return v2

    .line 3979
    .restart local v2    # "isShowing":Z
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v1

    .line 3980
    .local v1, "actionBarView":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    if-eqz v1, :cond_0

    .line 3983
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 3984
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_0

    .line 3987
    invoke-virtual {v0}, Landroid/app/ActionBar;->isShowing()Z

    move-result v2

    goto :goto_0
.end method

.method private isAvailableEAM()Z
    .locals 1

    .prologue
    .line 5990
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mInitialItem:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/UriImage;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCameraQuickViewOnLockscreen(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 5994
    :cond_2
    const/4 v0, 0x0

    .line 5996
    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isBackNeeded()Z
    .locals 2

    .prologue
    .line 5062
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->isSLinkIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method private isFromMyFilesAndUpKeyDisable()Z
    .locals 2

    .prologue
    .line 5645
    const/4 v0, 0x0

    .line 5646
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->isFromMyFiles()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5650
    const/4 v0, 0x1

    .line 5652
    :cond_0
    return v0
.end method

.method private isFromUriList()Z
    .locals 3

    .prologue
    .line 5656
    const/4 v0, 0x0

    .line 5657
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5658
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "uriList"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 5661
    :cond_0
    return v0
.end method

.method private isPlayerChanged()Z
    .locals 2

    .prologue
    .line 4626
    const/4 v0, 0x0

    .line 4628
    .local v0, "isWifiDisplayConnected":Z
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWfd:Z

    if-eqz v1, :cond_0

    .line 4629
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->isWifiDisplayConnected()Z

    move-result v0

    .line 4631
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->isOnPlaying()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    if-eqz v0, :cond_3

    invoke-static {}, Lcom/sec/android/gallery3d/util/HdmiUtils;->isPlugged()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isSideSyncConnected()Z
    .locals 1

    .prologue
    .line 5805
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCheckSideSyncNeeded:Z

    if-eqz v0, :cond_0

    .line 5806
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isConnetedSideSync(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsConnectedSideSync:Z

    .line 5807
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCheckSideSyncNeeded:Z

    .line 5809
    :cond_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsConnectedSideSync:Z

    return v0
.end method

.method private isViewByFilterLocal()Z
    .locals 5

    .prologue
    .line 5665
    const/4 v2, 0x0

    .line 5667
    .local v2, "result":Z
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    .line 5668
    .local v0, "activity":Lcom/sec/android/gallery3d/app/GalleryActivity;
    iget-boolean v4, v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mNeedFirstUpOfDetailView:Z

    if-nez v4, :cond_0

    move v3, v2

    .line 5677
    .end local v2    # "result":Z
    .local v3, "result":I
    :goto_0
    return v3

    .line 5672
    .end local v3    # "result":I
    .restart local v2    # "result":Z
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v1

    .line 5673
    .local v1, "currentViewByType":I
    sget-object v4, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v4, v1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v4

    if-nez v4, :cond_1

    sget-object v4, Lcom/sec/samsung/gallery/core/ViewByFilterType;->LOCAL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v4, v1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 5675
    const/4 v2, 0x1

    :cond_1
    move v3, v2

    .line 5677
    .restart local v3    # "result":I
    goto :goto_0
.end method

.method private launchCamera()V
    .locals 1

    .prologue
    .line 1668
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->startCameraActivity(Landroid/content/Context;)V

    .line 1669
    return-void
.end method

.method private makeBurstShotList()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5289
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentIndex()I

    move-result v1

    .line 5290
    .local v1, "currentIndex":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 5291
    .local v2, "currentItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v4

    .line 5293
    .local v4, "groupId":J
    :goto_0
    if-lez v1, :cond_0

    .line 5294
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    add-int/lit8 v7, v1, -0x1

    invoke-interface {v6, v7}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->moveTo(I)V

    .line 5295
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .line 5296
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v6

    cmp-long v6, v4, v6

    if-nez v6, :cond_0

    .line 5297
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 5302
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 5303
    .local v0, "burstShotList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    move v3, v1

    .local v3, "i":I
    :goto_1
    add-int/lit8 v6, v1, 0x14

    if-ge v3, v6, :cond_1

    .line 5304
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v6, v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->moveTo(I)V

    .line 5305
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .line 5306
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v6

    cmp-long v6, v4, v6

    if-nez v6, :cond_1

    .line 5307
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5303
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 5312
    :cond_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v6, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->moveTo(I)V

    .line 5313
    return-object v0
.end method

.method private makeMotionPicture()V
    .locals 2

    .prologue
    .line 5317
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->makeBurstShotList()Ljava/util/ArrayList;

    move-result-object v0

    .line 5318
    .local v0, "burstShotList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->makeMotionPicture(Ljava/util/ArrayList;)V

    .line 5319
    return-void
.end method

.method private onUpPressed()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2047
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStartInFilmstrip:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getFilmMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2048
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->setFilmMode(Z)V

    .line 2060
    :cond_0
    :goto_0
    return-void

    .line 2052
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getStateCount()I

    move-result v0

    if-le v0, v1, :cond_2

    .line 2053
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setResult()V

    .line 2054
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->onBackPressed()V

    goto :goto_0

    .line 2058
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOriginalSetPathString:Ljava/lang/String;

    if-nez v0, :cond_0

    goto :goto_0
.end method

.method private playCurrentMedia(Z)Z
    .locals 10
    .param p1, "withSound"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 4988
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v5}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 4989
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v0, :cond_1

    .line 5002
    :cond_0
    :goto_0
    return v4

    .line 4994
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v6

    const-wide/16 v8, 0x80

    and-long/2addr v6, v8

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_2

    move v1, v3

    .line 4995
    .local v1, "playVideo":Z
    :goto_1
    const-wide/16 v6, 0x400

    invoke-virtual {v0, v6, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    .line 4996
    .local v2, "support3DTour":Z
    if-eqz v1, :cond_3

    .line 4997
    invoke-direct {p0, v0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->handlePlayVideo(Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    move v4, v3

    .line 4998
    goto :goto_0

    .end local v1    # "playVideo":Z
    .end local v2    # "support3DTour":Z
    :cond_2
    move v1, v4

    .line 4994
    goto :goto_1

    .line 4999
    .restart local v1    # "playVideo":Z
    .restart local v2    # "support3DTour":Z
    :cond_3
    if-eqz v2, :cond_0

    .line 5000
    invoke-direct {p0, v0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->handle3DTour(Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    goto :goto_0
.end method

.method private postGallery(Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V
    .locals 3
    .param p1, "cmd"    # Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    .prologue
    .line 6001
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 6004
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "POST_GALLERY"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 6005
    return-void
.end method

.method private refreshHideSideMirrorMessage()V
    .locals 4

    .prologue
    .line 5436
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 5437
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->removeHideSideMirrorMessage()V

    .line 5438
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x69

    const-wide/16 v2, 0xdac

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 5440
    :cond_0
    return-void
.end method

.method private refreshHidingMessage()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1904
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1905
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1907
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsMenuVisible:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getFilmMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1908
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 1909
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xdac

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1912
    :cond_1
    return-void
.end method

.method private refreshHidingMessage(I)V
    .locals 4
    .param p1, "timeout"    # I

    .prologue
    const/4 v1, 0x1

    .line 4018
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 4019
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 4022
    :cond_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsPopupDialogDisplayed:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->enableShowBar()Z

    move-result v0

    if-nez v0, :cond_2

    .line 4034
    :cond_1
    :goto_0
    return-void

    .line 4026
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 4030
    :cond_3
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsMenuVisible:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsInteracting:Z

    if-nez v0, :cond_1

    .line 4031
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 4032
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private removeHideSideMirrorMessage()V
    .locals 2

    .prologue
    const/16 v1, 0x69

    .line 5443
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5444
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 5446
    :cond_0
    return-void
.end method

.method private requestDeferredUpdate()V
    .locals 4

    .prologue
    const-wide/16 v2, 0xfa

    .line 1672
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeferUpdateUntil:J

    .line 1673
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeferredUpdateWaiting:Z

    if-nez v0, :cond_0

    .line 1674
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeferredUpdateWaiting:Z

    .line 1675
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1677
    :cond_0
    return-void
.end method

.method private selectAllProcess(Lcom/sec/android/gallery3d/data/MediaSet;I)V
    .locals 1
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "realCount"    # I

    .prologue
    .line 4754
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->selectAllProcess(Lcom/sec/android/gallery3d/data/MediaSet;II)V

    .line 4755
    return-void
.end method

.method private selectAllProcess(Lcom/sec/android/gallery3d/data/MediaSet;II)V
    .locals 11
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "realCount"    # I
    .param p3, "maxAddCount"    # I

    .prologue
    const v10, 0x7f0e0117

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 4757
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v8}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiPickMode(Landroid/content/Context;)Z

    move-result v1

    .line 4758
    .local v1, "isMultiPick":Z
    invoke-virtual {p1, v7, p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v3

    .line 4759
    .local v3, "mMediaItem":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->checkGifMode()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 4760
    const/16 p3, 0x14

    .line 4761
    :cond_0
    const/4 v8, -0x1

    if-eq p3, v8, :cond_2

    move v0, v6

    .line 4762
    .local v0, "isLimited":Z
    :goto_0
    const/4 v4, 0x0

    .local v4, "position":I
    :goto_1
    if-ge v4, p2, :cond_1

    .line 4763
    if-eqz v0, :cond_4

    if-gtz p3, :cond_4

    .line 4764
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v8, :cond_3

    iget-boolean v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFromGalleryWidget:Z

    if-eqz v8, :cond_3

    .line 4765
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v9, 0x3e8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v7

    invoke-virtual {v8, v10, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 4767
    .local v5, "text":Ljava/lang/CharSequence;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 4801
    .end local v5    # "text":Ljava/lang/CharSequence;
    :cond_1
    :goto_2
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateCountOnActionBar()V

    .line 4802
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/ui/FilmStripView;->selectAll(I)V

    .line 4803
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateMenuOperations()V

    .line 4804
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/FilmStripView;->invalidate()V

    .line 4805
    return-void

    .end local v0    # "isLimited":Z
    .end local v4    # "position":I
    :cond_2
    move v0, v7

    .line 4761
    goto :goto_0

    .line 4768
    .restart local v0    # "isLimited":Z
    .restart local v4    # "position":I
    :cond_3
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->checkGifMode()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 4769
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v9, 0x14

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v7

    invoke-virtual {v8, v10, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 4771
    .restart local v5    # "text":Ljava/lang/CharSequence;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_2

    .line 4775
    .end local v5    # "text":Ljava/lang/CharSequence;
    :cond_4
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 4776
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v8, v2, Lcom/sec/android/gallery3d/data/ActionImage;

    if-eqz v8, :cond_6

    .line 4762
    :cond_5
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 4778
    :cond_6
    iget-boolean v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    if-eqz v8, :cond_7

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 4780
    :cond_7
    if-eqz v1, :cond_b

    .line 4781
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v8, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isAvailableDrm(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v8

    if-nez v8, :cond_9

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v8

    if-eqz v8, :cond_9

    .line 4782
    :cond_8
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0e0107

    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_3

    .line 4785
    :cond_9
    if-eqz v0, :cond_a

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 4787
    :cond_a
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8, p1, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 4788
    if-eqz v0, :cond_5

    .line 4789
    add-int/lit8 p3, p3, -0x1

    goto :goto_3

    .line 4792
    :cond_b
    if-eqz v0, :cond_c

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 4794
    :cond_c
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8, p1, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 4795
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 4796
    if-eqz v0, :cond_5

    .line 4797
    add-int/lit8 p3, p3, -0x1

    goto :goto_3
.end method

.method private selectAllProcessTask(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 4
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 4725
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$25;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$25;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;I)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 4751
    return-void
.end method

.method private selectLimitedAllProcess(Lcom/sec/android/gallery3d/data/MediaSet;II)V
    .locals 16
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "realCount"    # I
    .param p3, "maxPickCount"    # I

    .prologue
    .line 4808
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v11}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiPickMode(Landroid/content/Context;)Z

    move-result v3

    .line 4810
    .local v3, "isMultiPick":Z
    const/4 v5, 0x0

    .line 4811
    .local v5, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v10

    .line 4812
    .local v10, "totalSelectedCount":I
    sub-int v6, p3, v10

    .line 4814
    .local v6, "maxAddCnt":I
    div-int v8, p2, p3

    .line 4816
    .local v8, "step":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-gt v2, v8, :cond_0

    .line 4817
    if-gtz v6, :cond_1

    .line 4855
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateCountOnActionBar()V

    .line 4856
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    invoke-virtual {v11, v12}, Lcom/sec/android/gallery3d/ui/FilmStripView;->selectAll(I)V

    .line 4857
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateMenuOperations()V

    .line 4858
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/FilmStripView;->invalidate()V

    .line 4859
    return-void

    .line 4819
    :cond_1
    mul-int v11, v2, p3

    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v11, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v5

    .line 4821
    const/4 v7, 0x0

    .local v7, "position":I
    :goto_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-ge v7, v11, :cond_2

    .line 4822
    if-gtz v6, :cond_3

    if-lez p3, :cond_3

    .line 4823
    sget-boolean v11, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v11, :cond_2

    .line 4824
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0e0117

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 4826
    .local v9, "text":Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 4816
    .end local v9    # "text":Ljava/lang/CharSequence;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 4830
    :cond_3
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 4831
    .local v4, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v11, v4, Lcom/sec/android/gallery3d/data/ActionImage;

    if-eqz v11, :cond_5

    .line 4821
    :cond_4
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 4833
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    if-eqz v11, :cond_6

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 4835
    :cond_6
    if-eqz v3, :cond_9

    .line 4836
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v11, v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isAvailableDrm(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v11

    if-nez v11, :cond_8

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v11

    if-eqz v11, :cond_8

    .line 4837
    :cond_7
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v12, 0x7f0e0107

    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_2

    .line 4840
    :cond_8
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 4842
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 4843
    add-int/lit8 v6, v6, -0x1

    goto :goto_2

    .line 4846
    :cond_9
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 4848
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 4849
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 4850
    add-int/lit8 v6, v6, -0x1

    goto :goto_2
.end method

.method private setCheckSideSyncConnection(Z)V
    .locals 0
    .param p1, "bNeeded"    # Z

    .prologue
    .line 5801
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCheckSideSyncNeeded:Z

    .line 5802
    return-void
.end method

.method private setDetailActionBarForNormal()V
    .locals 4

    .prologue
    .line 3954
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsUpAvailableForNormal:Z

    invoke-direct {v1, v2, v3}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Z)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 3955
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    sget v1, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_DEFAULT:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->setShowBarMode(I)V

    .line 3956
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFirstTimeLaunch:Z

    if-eqz v0, :cond_1

    .line 3957
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFirstTimeLaunch:Z

    .line 3958
    const/16 v0, 0xdac

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->refreshHidingMessage(I)V

    .line 3964
    :cond_0
    :goto_0
    return-void

    .line 3961
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->checkIndexShouldShow()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIndexEnabled:Z

    if-eqz v0, :cond_0

    .line 3962
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    const-string v1, ""

    iget v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getMediaItemCount()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(Ljava/lang/String;II)V

    goto :goto_0
.end method

.method private setFullScreenMode(Z)V
    .locals 4
    .param p1, "isFullMode"    # Z

    .prologue
    .line 3991
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHWKey:Z

    if-eqz v2, :cond_1

    .line 4008
    :cond_0
    :goto_0
    return-void

    .line 3994
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionTutorialNone(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3997
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v0

    .line 3998
    .local v0, "mw":Lcom/sec/android/gallery3d/app/GalleryMultiWindow;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4001
    const-class v2, Landroid/view/View;

    const-string v3, "SYSTEM_UI_FLAG_REMOVE_NAVIGATION"

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDeclaredIntField(Ljava/lang/Class;Ljava/lang/String;)I

    move-result v1

    .line 4003
    .local v1, "systemUiFlagRemoveNavigation":I
    if-eqz p1, :cond_2

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 4004
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setSystemUiVisibility(I)V

    goto :goto_0

    .line 4006
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/16 v3, 0x200

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setSystemUiVisibility(I)V

    goto :goto_0
.end method

.method private setIndexEnabled(Z)V
    .locals 7
    .param p1, "enabled"    # Z

    .prologue
    .line 4479
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v6, 0x7f0f0014

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 4480
    .local v4, "view":Landroid/widget/TextView;
    if-eqz v4, :cond_0

    .line 4481
    if-nez p1, :cond_1

    .line 4482
    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 4493
    :cond_0
    :goto_0
    return-void

    .line 4485
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 4486
    .local v3, "res":Landroid/content/res/Resources;
    const v5, 0x7f0e001a

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 4487
    .local v0, "dx":F
    const v5, 0x7f0e001b

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 4488
    .local v1, "dy":F
    const v5, 0x7f0e001c

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 4489
    .local v2, "radius":F
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 4490
    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 4491
    const/high16 v5, -0x1000000

    invoke-virtual {v4, v2, v0, v1, v5}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto :goto_0
.end method

.method private setManualFaceTag()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 4372
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v1, :cond_1

    .line 4373
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFaceTagFeature:Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;->getManualFaceTagUri()Landroid/net/Uri;

    move-result-object v0

    .line 4374
    .local v0, "manualFaceTagUri":Landroid/net/Uri;
    if-eqz v0, :cond_1

    .line 4375
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->setManualFace(Landroid/net/Uri;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 4376
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsResumeManualFaceTag:Z

    .line 4377
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    sget-object v2, Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;->INCLUDE_PROFILE:Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->assignName(Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;)V

    .line 4378
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFaceTagFeature:Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;

    invoke-interface {v1, v4}, Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;->setManualFaceTagUri(Landroid/net/Uri;)V

    .line 4380
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->setIsManualFDResumed(Z)V

    .line 4383
    .end local v0    # "manualFaceTagUri":Landroid/net/Uri;
    :cond_1
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsResumeManualFaceTag:Z

    if-nez v1, :cond_2

    .line 4384
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFaceTagFeature:Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;

    invoke-interface {v1, v4}, Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;->setManualFaceTagUri(Landroid/net/Uri;)V

    .line 4385
    :cond_2
    return-void
.end method

.method private setNfcBeamPushUri(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1649
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNfcPushUris:[Landroid/net/Uri;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 1650
    return-void
.end method

.method private setPhotoReader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "mimeType"    # Ljava/lang/String;
    .param p2, "date"    # Ljava/lang/String;
    .param p3, "location"    # Ljava/lang/String;
    .param p4, "weather"    # Ljava/lang/String;
    .param p5, "category"    # Ljava/lang/String;
    .param p6, "faces"    # Ljava/lang/String;

    .prologue
    .line 5958
    const-string v0, ""

    .line 5960
    .local v0, "TTSText":Ljava/lang/String;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5961
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5962
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 5963
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5964
    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 5965
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5966
    :cond_2
    if-eqz p4, :cond_3

    invoke-virtual {p4}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 5967
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5968
    :cond_3
    if-eqz p5, :cond_4

    invoke-virtual {p5}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 5969
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5970
    :cond_4
    if-eqz p6, :cond_5

    invoke-virtual {p6}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 5971
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5973
    :cond_5
    return-object v0
.end method

.method private setResult()V
    .locals 3

    .prologue
    .line 2065
    const/4 v0, 0x0

    .line 2066
    .local v0, "result":Landroid/content/Intent;
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "result":Landroid/content/Intent;
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2067
    .restart local v0    # "result":Landroid/content/Intent;
    const-string v1, "return-index-hint"

    iget v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2068
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setStateResult(ILandroid/content/Intent;)V

    .line 2069
    return-void
.end method

.method private showBars()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1842
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsActive:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBars:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->enableShowBar()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1868
    :cond_0
    :goto_0
    return-void

    .line 1845
    :cond_1
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBars:Z

    .line 1846
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOrientationManager:Lcom/sec/android/gallery3d/app/OrientationManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/OrientationManager;->unlockOrientation()V

    .line 1847
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/sec/android/gallery3d/ui/GLRoot;->setLightsOutMode(Z)V

    .line 1848
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->refreshHidingMessage()V

    .line 1849
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBars:Z

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->setShowBarState(Z)V

    .line 1850
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBars:Z

    if-nez v2, :cond_2

    move v0, v1

    :cond_2
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setFullScreenMode(Z)V

    .line 1851
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->setInitialFilmRender(Z)V

    .line 1852
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->enableActioBarShow()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1853
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->show()V

    .line 1855
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_4

    .line 1856
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x6d

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1859
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->enableFilmShow()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1860
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    if-nez v0, :cond_5

    .line 1861
    new-instance v0, Lcom/sec/android/gallery3d/anim/FloatAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/16 v3, 0xfa

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/anim/FloatAnimation;-><init>(FFI)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    .line 1862
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGlRootView()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->registerLaunchedAnimation(Lcom/sec/android/gallery3d/anim/CanvasAnimation;)V

    .line 1863
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/anim/FloatAnimation;->start()V

    .line 1865
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->show()V

    .line 1867
    :cond_6
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->showFaceIndicatorView()V

    goto :goto_0
.end method

.method private showBarsForAShortTime()V
    .locals 1

    .prologue
    .line 4037
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->showBars()V

    .line 4038
    const/16 v0, 0xdac

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->refreshHidingMessage(I)V

    .line 4039
    return-void
.end method

.method private showChangePlayerDialog()V
    .locals 5

    .prologue
    .line 3789
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mChangePlayerDialog:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    if-eqz v1, :cond_0

    .line 3790
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mChangePlayerDialog:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->dismiss()V

    .line 3793
    :cond_0
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWfd:Z

    if-eqz v1, :cond_2

    .line 3794
    const/4 v0, 0x0

    .line 3795
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    if-eqz v1, :cond_1

    .line 3796
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getBaseSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 3799
    :cond_1
    new-instance v1, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaSet;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mChangePlayerDialog:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    .line 3803
    .end local v0    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->isAlertDialogOff()Z

    move-result v1

    if-nez v1, :cond_4

    .line 3804
    new-instance v1, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mChangePlayerDialog:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;)V

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/WifiDisplayAlertDialog;->show()V

    .line 3809
    :cond_3
    :goto_0
    return-void

    .line 3806
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mChangePlayerDialog:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    if-eqz v1, :cond_3

    .line 3807
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mChangePlayerDialog:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->show()V

    goto :goto_0
.end method

.method private showGuideDialog(Z)V
    .locals 1
    .param p1, "isImage"    # Z

    .prologue
    .line 3967
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/HelpImage;

    if-nez v0, :cond_0

    .line 3968
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->showAdaptDisplayDialog(Z)V

    .line 3969
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->showMotionDialog(Z)V

    .line 3971
    :cond_0
    return-void
.end method

.method private showOnMap(DD)V
    .locals 3
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    .line 4160
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p3, p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    .line 4163
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "SHOW_ON_MAP"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 4164
    return-void
.end method

.method private startAllView(Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 4134
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 4135
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4136
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    const-class v2, Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 4137
    return-void
.end method

.method private startBurstPlay()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 4184
    const/4 v1, 0x6

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v1, v0, v3

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v2

    const/4 v1, 0x2

    aput-object v4, v0, v1

    const/4 v1, 0x3

    aput-object v4, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowPlaySpeed:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 4187
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "START_SLIDESHOW"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 4188
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->setBurstMode(Z)V

    .line 4189
    return-void
.end method

.method private startDelete()V
    .locals 2

    .prologue
    .line 5576
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5577
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->onDeleteMultipleImages()V

    .line 5578
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->exitSelectionMode()V

    .line 5582
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->showUndoBar(Z)V

    .line 5583
    return-void

    .line 5580
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->deleteCurrentImage()V

    goto :goto_0
.end method

.method private startManualDetect()V
    .locals 3

    .prologue
    .line 5006
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->startMFDByMenu(II)V

    .line 5007
    return-void
.end method

.method private startMoreInfo()V
    .locals 4

    .prologue
    .line 5628
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/samsung/gallery/view/ActionBarManager;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    .line 5629
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    sget v1, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_MOREINFO:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->setShowBarMode(I)V

    .line 5630
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->unregisterEAM(Landroid/content/BroadcastReceiver;)V

    .line 5631
    return-void
.end method

.method private startNavigation(DD)V
    .locals 3
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    .line 4153
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p3, p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    .line 4156
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "START_NAVIGATION"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 4157
    return-void
.end method

.method private startNavigation(DDLjava/lang/String;)V
    .locals 3
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D
    .param p5, "name"    # Ljava/lang/String;

    .prologue
    .line 2073
    const/4 v1, 0x4

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p3, p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    aput-object p5, v0, v1

    .line 2076
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "START_NAVIGATION"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2077
    return-void
.end method

.method private startPhotoView(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 4104
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 4105
    .local v0, "bundle":Landroid/os/Bundle;
    iget v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentMediaSetIndex:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 4106
    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentMediaSetIndex:I

    .line 4107
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->getTopSetPathByFilter()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 4108
    .local v2, "topMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    if-eqz v3, :cond_0

    .line 4109
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    .line 4110
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 4111
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 4112
    iput v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentMediaSetIndex:I

    .line 4118
    .end local v1    # "i":I
    .end local v2    # "topMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    const-string v3, "KEY_MEDIA_SET_PATH"

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->getTopSetPathByFilter()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4119
    const-string v3, "KEY_MEDIA_SET_POSITION"

    iget v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentMediaSetIndex:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 4120
    const-string v3, "KEY_ITEM_POSITION"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 4121
    const-string v4, "is_from_camera"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v3, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/GalleryActivity;->isFromCamera()Z

    move-result v3

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 4122
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    sget-object v4, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_EVENT:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v3, v4, :cond_1

    .line 4123
    const-string v3, "KEY_MEDIA_SET_PATH"

    const-string v4, "/local/event/all"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4124
    const-string v3, "KEY_NO_SPLIT_MODE"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 4126
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    if-eqz v3, :cond_2

    .line 4127
    const-string v3, "KEY_MEDIA_ITEM_PATH"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOriginalSetPathString:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4129
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    const-class v4, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/gallery3d/app/StateManager;->pushStateAtBottom(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 4130
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    const-class v4, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/app/StateManager;->pushStateAtBottom(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 4131
    return-void

    .line 4110
    .restart local v1    # "i":I
    .restart local v2    # "topMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private startSlideShow()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4219
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->hideContactPopupView()V

    .line 4220
    const/4 v1, 0x0

    .line 4221
    .local v1, "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4222
    const/4 v2, 0x6

    new-array v1, v2, [Ljava/lang/Object;

    .end local v1    # "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v1, v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v5

    aput-object v3, v1, v6

    aput-object v3, v1, v7

    const/4 v2, 0x4

    iget v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowPlaySpeed:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 4231
    .restart local v1    # "params":[Ljava/lang/Object;
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v3, "START_SLIDESHOW"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 4232
    return-void

    .line 4226
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentIndex()I

    move-result v0

    .line 4227
    .local v0, "imageIndex":I
    const/4 v2, 0x6

    new-array v1, v2, [Ljava/lang/Object;

    .end local v1    # "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v1, v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v5

    aput-object v3, v1, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    const/4 v2, 0x4

    iget v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowPlaySpeed:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .restart local v1    # "params":[Ljava/lang/Object;
    goto :goto_0
.end method

.method private startSoundScene(Ljava/lang/String;)V
    .locals 6
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 5199
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsFinishingState:Z

    if-eqz v2, :cond_1

    .line 5211
    :cond_0
    :goto_0
    return-void

    .line 5201
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v3, "phone"

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 5202
    .local v1, "telephonymanager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v2

    if-ne v2, v5, :cond_2

    invoke-static {p1}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isSoundScene(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 5203
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0e0066

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 5206
    :cond_2
    const/4 v2, 0x4

    new-array v0, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v4

    const/4 v2, 0x1

    sget-object v3, Lcom/sec/samsung/gallery/controller/SoundSceneCmd$SoundSceneCmdType;->START_PLAY_SOUND:Lcom/sec/samsung/gallery/controller/SoundSceneCmd$SoundSceneCmdType;

    aput-object v3, v0, v2

    aput-object p1, v0, v5

    const/4 v2, 0x3

    const/4 v3, 0x0

    aput-object v3, v0, v2

    .line 5209
    .local v0, "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    if-eqz v2, :cond_0

    .line 5210
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v3, "SOUND_SCENE"

    invoke-virtual {v2, v3, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private stopSoundScene()V
    .locals 3

    .prologue
    .line 5214
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/controller/SoundSceneCmd$SoundSceneCmdType;->STOP_PLAY_SOUND:Lcom/sec/samsung/gallery/controller/SoundSceneCmd$SoundSceneCmdType;

    aput-object v2, v0, v1

    .line 5217
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "SOUND_SCENE"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 5218
    return-void
.end method

.method private updateBars()V
    .locals 1

    .prologue
    .line 1933
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->canShowBars()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1934
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->hideBars()V

    .line 1936
    :cond_0
    return-void
.end method

.method private updateCountOnActionBar()V
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 4958
    iget-boolean v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    if-eqz v10, :cond_3

    .line 4959
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSourceMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    .line 4960
    .local v3, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v5

    .line 4961
    .local v5, "numberOfItemsInAlbum":I
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v10, v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v0

    .line 4962
    .local v0, "checkedCount":I
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v10, v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v4

    .line 4963
    .local v4, "newAlbumCheckCount":I
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v7

    .line 4964
    .local v7, "totalCheckedCount":I
    add-int v10, v0, v4

    if-ne v5, v10, :cond_0

    move v6, v8

    .line 4965
    .local v6, "selectAll":Z
    :goto_0
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v10, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 4967
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v10}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 4968
    .local v1, "intent":Landroid/content/Intent;
    const-string v10, "pick-max-item"

    const/4 v11, -0x1

    invoke-virtual {v1, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 4969
    .local v2, "maxPickCount":I
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v10}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiPickMode(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_1

    if-lez v2, :cond_1

    if-ne v2, v7, :cond_1

    .line 4970
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v11, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v10, v11, v9, v8}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 4985
    .end local v0    # "checkedCount":I
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "maxPickCount":I
    .end local v4    # "newAlbumCheckCount":I
    :goto_1
    return-void

    .end local v6    # "selectAll":Z
    .restart local v0    # "checkedCount":I
    .restart local v4    # "newAlbumCheckCount":I
    :cond_0
    move v6, v9

    .line 4964
    goto :goto_0

    .line 4971
    .restart local v1    # "intent":Landroid/content/Intent;
    .restart local v2    # "maxPickCount":I
    .restart local v6    # "selectAll":Z
    :cond_1
    if-ne v5, v4, :cond_2

    .line 4972
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v10, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    const/16 v11, 0x8

    invoke-virtual {v9, v10, v11, v8}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    goto :goto_1

    .line 4974
    :cond_2
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v10, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v8, v10, v9, v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    goto :goto_1

    .line 4977
    .end local v0    # "checkedCount":I
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "maxPickCount":I
    .end local v3    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v4    # "newAlbumCheckCount":I
    .end local v5    # "numberOfItemsInAlbum":I
    .end local v6    # "selectAll":Z
    .end local v7    # "totalCheckedCount":I
    :cond_3
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSourceMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    .line 4978
    .restart local v3    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v5

    .line 4979
    .restart local v5    # "numberOfItemsInAlbum":I
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v7

    .line 4980
    .restart local v7    # "totalCheckedCount":I
    if-ne v7, v5, :cond_4

    move v6, v8

    .line 4981
    .restart local v6    # "selectAll":Z
    :goto_2
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v8, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 4982
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v8, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    .line 4983
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v10, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v8, v10, v9, v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    goto :goto_1

    .end local v6    # "selectAll":Z
    :cond_4
    move v6, v9

    .line 4980
    goto :goto_2
.end method

.method private updateCurrentPhoto(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 8
    .param p1, "photo"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v6, 0x0

    .line 1755
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v4, :cond_0

    .line 1756
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v5}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentIndex()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setFocusIndex(I)V

    .line 1759
    :cond_0
    if-nez p1, :cond_2

    .line 1760
    sget-object v4, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TAG:Ljava/lang/String;

    const-string v5, "Photo is null"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1828
    :cond_1
    :goto_0
    return-void

    .line 1764
    :cond_2
    const/4 v0, 0x0

    .line 1765
    .local v0, "forceUpdateDrmImage":Z
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    instance-of v4, v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-eqz v4, :cond_3

    .line 1766
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    check-cast v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->forceUpdateDrmImage()Z

    move-result v0

    .line 1770
    :cond_3
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    if-eqz v4, :cond_5

    .line 1771
    move-object v1, p1

    .line 1772
    .local v1, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    sget-object v5, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v4, v5, :cond_4

    .line 1773
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSourceMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    .line 1774
    .local v3, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v4}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentIndex()I

    move-result v4

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v2

    .line 1775
    .local v2, "mediaItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_4

    .line 1776
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1779
    .end local v2    # "mediaItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v3    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .restart local v1    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addExpMediaItem(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1780
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->invalidateOptionsMenu()V

    .line 1785
    .end local v1    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_5
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-ne v4, p1, :cond_7

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->getIsRotated()Z

    move-result v4

    if-nez v4, :cond_7

    if-nez v0, :cond_7

    .line 1786
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v4, v6}, Lcom/sec/android/gallery3d/ui/PhotoView;->setIsRotated(Z)V

    .line 1787
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v4, :cond_6

    .line 1788
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->invalidateFaceIndicatorView(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 1790
    :cond_6
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v4, :cond_1

    .line 1791
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->showIcon(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 1792
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->updateBurstShotIcon(Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowPlaySpeed:I

    goto :goto_0

    .line 1797
    :cond_7
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->getFilmMode()Z

    move-result v4

    if-nez v4, :cond_8

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eq v4, p1, :cond_8

    .line 1798
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->stopSoundScene()V

    .line 1799
    if-eqz p1, :cond_8

    const-wide/16 v4, 0x10

    invoke-virtual {p1, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isSoundShotAutoPlay(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1801
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->startSoundScene(Ljava/lang/String;)V

    .line 1805
    :cond_8
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v4, v6}, Lcom/sec/android/gallery3d/ui/PhotoView;->setIsRotated(Z)V

    .line 1806
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->hideContactPopupView()V

    .line 1808
    if-eqz p1, :cond_9

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getDataVersion()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAgifVersion:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_9

    .line 1809
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v4

    new-instance v5, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$InitAGIFJob;

    invoke-direct {v5, p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$InitAGIFJob;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/android/gallery3d/data/MediaItem;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;)Lcom/sec/android/gallery3d/util/Future;

    .line 1812
    :cond_9
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eq v4, p1, :cond_a

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v4, v4, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v4, :cond_a

    .line 1813
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v4, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/LocalImage;->removeManualFD()V

    .line 1817
    :cond_a
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 1818
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->closeMoreInfo()V

    .line 1821
    :cond_b
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1822
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->getFilmMode()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1823
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->requestDeferredUpdate()V

    goto/16 :goto_0

    .line 1825
    :cond_c
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateUIForCurrentPhoto()V

    goto/16 :goto_0
.end method

.method private updateLastSharedIconAndTitle()V
    .locals 1

    .prologue
    .line 5193
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->isLastShareAppExisted(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5194
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    .line 5196
    :cond_0
    return-void
.end method

.method private updateMaximumSelectionNumberExceeded(I)V
    .locals 6
    .param p1, "maxCount"    # I

    .prologue
    .line 4648
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0117

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 4651
    .local v0, "text":Ljava/lang/CharSequence;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 4652
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateCountOnActionBar()V

    .line 4653
    return-void
.end method

.method private updateMenuOperations()V
    .locals 2

    .prologue
    .line 1832
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setPostOnAnimation(Z)V

    .line 1833
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 1835
    return-void
.end method

.method private updatePanoramaUI(Z)V
    .locals 7
    .param p1, "isPanorama360"    # Z

    .prologue
    const v6, 0x7f0f026c

    .line 2938
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBar:Lcom/sec/android/gallery3d/app/GalleryActionBar;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryActionBar;->getMenu()Landroid/view/Menu;

    move-result-object v1

    .line 2941
    .local v1, "menu":Landroid/view/Menu;
    if-nez v1, :cond_1

    .line 2960
    :cond_0
    :goto_0
    return-void

    .line 2945
    :cond_1
    invoke-static {v1, p1, p1}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->updateMenuForPanorama(Landroid/view/Menu;ZZ)V

    .line 2947
    if-eqz p1, :cond_2

    .line 2948
    invoke-interface {v1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 2949
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 2950
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 2951
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0367

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_0

    .line 2953
    .end local v0    # "item":Landroid/view/MenuItem;
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v2

    const-wide/16 v4, 0x4

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 2954
    invoke-interface {v1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 2955
    .restart local v0    # "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 2956
    const/4 v2, 0x1

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 2957
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0047

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private updateSaveButtonState()V
    .locals 3

    .prologue
    .line 4566
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMenu:Landroid/view/Menu;

    const v2, 0x7f0f02de

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 4567
    .local v0, "save_Item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v1, :cond_0

    .line 4568
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 4570
    :cond_0
    return-void
.end method

.method private updateSelectionMode(Ljava/lang/Boolean;)V
    .locals 1
    .param p1, "params"    # Ljava/lang/Boolean;

    .prologue
    .line 3812
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3813
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->onBackPressed()V

    .line 3815
    :cond_0
    return-void
.end method

.method private updateShowHiddenIconVisibility()V
    .locals 4

    .prologue
    .line 4635
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMenu:Landroid/view/Menu;

    const v3, 0x7f0f02a2

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 4636
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 4637
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsHiddenItem:Z

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 4639
    :cond_0
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v2, :cond_1

    .line 4640
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMenu:Landroid/view/Menu;

    const v3, 0x7f0f0271

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 4641
    .local v1, "item2":Landroid/view/MenuItem;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    .line 4642
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsHiddenItem:Z

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 4645
    .end local v1    # "item2":Landroid/view/MenuItem;
    :cond_1
    return-void
.end method

.method private updateUIForCurrentPhoto()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1680
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-nez v2, :cond_1

    .line 1751
    :cond_0
    :goto_0
    return-void

    .line 1686
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v2

    const-wide/16 v4, 0x4000

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->getFilmMode()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1688
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->setWantPictureCenterCallbacks(Z)V

    .line 1691
    :cond_2
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateMenuOperations()V

    .line 1694
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    if-eqz v2, :cond_3

    .line 1695
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryId()I

    move-result v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->playImageContinuously(ILcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 1697
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_d

    :goto_1
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->showGuideDialog(Z)V

    .line 1698
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v0, :cond_4

    .line 1699
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->showIcon(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 1700
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->updateBurstShotIcon(Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowPlaySpeed:I

    .line 1703
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOutOfFocusShotHelpView:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/32 v2, 0x20000

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1704
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOutOfFocusShotHelpView:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->addView()V

    .line 1706
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1707
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDrmUtil:Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->isValidRights(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1708
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mBrokenImage:Landroid/graphics/Bitmap;

    if-nez v0, :cond_6

    .line 1709
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200a7

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mBrokenImage:Landroid/graphics/Bitmap;

    .line 1710
    :cond_6
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mBrokenImage:Landroid/graphics/Bitmap;

    invoke-interface {v0, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->updateBrokenImage(Landroid/graphics/Bitmap;)V

    .line 1711
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v0, :cond_7

    .line 1712
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    iget v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/FilmStripView;->updateItem(I)V

    .line 1715
    :cond_7
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDrmUtil:Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->verifyRights(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1716
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDrmUtil:Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->consume(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 1717
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    instance-of v0, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-eqz v0, :cond_8

    .line 1718
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    check-cast v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->resetBrokenImage()V

    .line 1724
    :cond_8
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v0, :cond_a

    .line 1725
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->setCurrentPhoto(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 1726
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    if-eqz v0, :cond_9

    .line 1727
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getKey()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->setCurMediaSetKey(Ljava/lang/String;Ljava/lang/String;)V

    .line 1730
    :cond_9
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFaceTagAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1731
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFaceTagFeature:Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-interface {v0, v2}, Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;->setScanPriority(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 1735
    :cond_a
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseContextualAwareness:Z

    if-eqz v0, :cond_b

    .line 1736
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isActionbarShowing()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->setCurrentPhotoForCA(Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    .line 1745
    :cond_b
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isExploreByTouchEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1746
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->setShowContextualTag(Z)V

    .line 1749
    :cond_c
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoReader:Z

    if-eqz v0, :cond_0

    .line 1750
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TTSPhotoReader()V

    goto/16 :goto_0

    :cond_d
    move v0, v1

    .line 1697
    goto/16 :goto_1
.end method

.method private updateViewModeIcons(IZ)V
    .locals 4
    .param p1, "action"    # I
    .param p2, "useSelector"    # Z

    .prologue
    const v3, 0x7f0f026e

    .line 4510
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMenu:Landroid/view/Menu;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isCoverMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4521
    :cond_0
    :goto_0
    return-void

    .line 4512
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMenu:Landroid/view/Menu;

    const v2, 0x7f0f0299

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 4513
    .local v0, "nearbyMenuItem":Landroid/view/MenuItem;
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFromSetupWidzard:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4514
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMenu:Landroid/view/Menu;

    invoke-static {v1, v3, p1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 4515
    if-nez p1, :cond_2

    .line 4516
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMenu:Landroid/view/Menu;

    const v2, 0x7f020023

    invoke-static {v1, v3, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIcon(Landroid/view/Menu;II)V

    goto :goto_0

    .line 4518
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMenu:Landroid/view/Menu;

    const v2, 0x7f02032f

    invoke-static {v1, v3, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIcon(Landroid/view/Menu;II)V

    goto :goto_0
.end method

.method private updateViewModeIcons(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 4498
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4499
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->isFromCamera()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsInViewMode:Z

    if-eqz v0, :cond_1

    .line 4500
    :cond_0
    if-eqz p1, :cond_2

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_2

    .line 4501
    invoke-direct {p0, v1, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateViewModeIcons(IZ)V

    .line 4507
    :cond_1
    :goto_0
    return-void

    .line 4503
    :cond_2
    const/4 v0, 0x2

    invoke-direct {p0, v0, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateViewModeIcons(IZ)V

    goto :goto_0
.end method

.method private wantBars()V
    .locals 1

    .prologue
    .line 1928
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->canShowBars()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1929
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->showBars()V

    .line 1930
    :cond_0
    return-void
.end method


# virtual methods
.method public doCommitDeleteImage(Z)V
    .locals 7
    .param p1, "waitForDone"    # Z

    .prologue
    const/4 v6, 0x0

    .line 2235
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeletePath:Lcom/sec/android/gallery3d/data/Path;

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    if-nez v4, :cond_1

    .line 2261
    :cond_0
    :goto_0
    return-void

    .line 2238
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeletePath:Lcom/sec/android/gallery3d/data/Path;

    if-eqz v4, :cond_3

    .line 2239
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    if-nez v4, :cond_2

    .line 2240
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    .line 2241
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeletePath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2242
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeletePath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2244
    :cond_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2247
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v2

    .line 2248
    .local v2, "manager":Lcom/sec/android/gallery3d/data/DataManager;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/Path;

    .line 2249
    .local v3, "path":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/data/DataManager;->delete(Lcom/sec/android/gallery3d/data/Path;)V

    goto :goto_1

    .line 2251
    .end local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->notifyContentChanged()V

    .line 2252
    if-eqz p1, :cond_5

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    .line 2254
    const-wide/16 v4, 0x64

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2259
    :cond_5
    :goto_2
    iput-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    .line 2260
    iput-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeletePath:Lcom/sec/android/gallery3d/data/Path;

    goto :goto_0

    .line 2255
    :catch_0
    move-exception v0

    .line 2256
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2
.end method

.method protected getBackgroundColorId()I
    .locals 1

    .prologue
    .line 502
    const v0, 0x7f0b000b

    return v0
.end method

.method public getContentsForDetailsDialog()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4236
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v3

    .line 4239
    .local v3, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 4240
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaObject;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v1

    .line 4241
    .local v1, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    new-instance v4, Lcom/sec/samsung/gallery/controller/DetailModel;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v4, v5, v1}, Lcom/sec/samsung/gallery/controller/DetailModel;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaDetails;)V

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/controller/DetailModel;->toStringList()Ljava/util/ArrayList;

    move-result-object v0

    .line 4242
    .local v0, "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    return-object v0
.end method

.method public getMediaForDetails()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1

    .prologue
    .line 4247
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method public getSelectonModeBarHeight()I
    .locals 1

    .prologue
    .line 5794
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBars:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getActionBarView()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5795
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getActionBarView()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->getSelectionModeBarHeight()I

    move-result v0

    .line 5797
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLongPressFinished()Z
    .locals 1

    .prologue
    .line 5473
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsLongPressFinished:Z

    return v0
.end method

.method public isRcsUri()Z
    .locals 1

    .prologue
    .line 5977
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mRcsUri:Z

    return v0
.end method

.method public lockNotify()V
    .locals 1

    .prologue
    .line 4580
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v0, :cond_0

    .line 4581
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->lockNotify()V

    .line 4583
    :cond_0
    return-void
.end method

.method public notifyFaceTagUpdate(I)V
    .locals 7
    .param p1, "id"    # I

    .prologue
    .line 5813
    if-gtz p1, :cond_1

    .line 5833
    :cond_0
    :goto_0
    return-void

    .line 5817
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v4, :cond_0

    .line 5818
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    .line 5820
    .local v3, "uri":Landroid/net/Uri;
    if-eqz v3, :cond_0

    .line 5821
    invoke-virtual {v3}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    .line 5823
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 5824
    .local v0, "_id":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v4, :cond_0

    if-ne v0, p1, :cond_0

    .line 5825
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->setCurrentPhoto(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 5826
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->invalidate()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 5830
    .end local v0    # "_id":I
    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v1

    .line 5831
    .local v1, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "notifyFaceTagUpdate error. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onActionBarAllowed(Z)V
    .locals 2
    .param p1, "allowed"    # Z

    .prologue
    .line 2160
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarAllowed:Z

    .line 2161
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2162
    return-void
.end method

.method public onActionBarWanted()V
    .locals 2

    .prologue
    .line 2166
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2167
    return-void
.end method

.method public onAlbumModeSelected(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 2935
    return-void
.end method

.method public onBackPressed()V
    .locals 13

    .prologue
    const/4 v1, 0x0

    const/4 v12, 0x1

    .line 1941
    const/4 v10, 0x1

    .line 1943
    .local v10, "needShrinkAnim":Z
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->isFromCamera()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1944
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->cameraSwitchingBoost(Landroid/content/Context;)V

    .line 1946
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1948
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->closeSlideShowSetting()V

    .line 2043
    :cond_1
    :goto_0
    return-void

    .line 1951
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1952
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->onBackPressed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1953
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->closeMoreInfo()V

    goto :goto_0

    .line 1958
    :cond_3
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setResult()V

    .line 1959
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStartInFilmstrip:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getFilmMode()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1960
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0, v12}, Lcom/sec/android/gallery3d/ui/PhotoView;->setFilmMode(Z)V

    .line 1967
    :cond_4
    :goto_1
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->showSideMirrorView(Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1971
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1972
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->exitSelectionMode()V

    goto :goto_0

    .line 1961
    :cond_5
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mTreatBackAsUp:Z

    if-eqz v0, :cond_4

    .line 1962
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->onUpPressed()V

    goto :goto_1

    .line 1976
    :cond_6
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setFullScreenMode(Z)V

    .line 1978
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->stopRotateOutAnimation()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1982
    :cond_7
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_8

    .line 1983
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 1986
    :cond_8
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->onBackPressed()V

    .line 1988
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionPreview(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1989
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/16 v2, 0x65

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setResult(I)V

    .line 1992
    :cond_9
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOCR:Z

    if-eqz v0, :cond_a

    .line 1993
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->removeOcrPopup()V

    .line 1997
    :cond_a
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v0, :cond_d

    .line 1998
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v0, :cond_b

    .line 1999
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalImage;->removeManualFD()V

    .line 2002
    :cond_b
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->removeContactPopupView()V

    .line 2003
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->isArcMode()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2004
    const/4 v11, 0x0

    .line 2005
    .local v11, "originalSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOriginalSetPathString:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 2006
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOriginalSetPathString:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v11

    .line 2008
    :cond_c
    if-eqz v11, :cond_d

    instance-of v0, v11, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    if-eqz v0, :cond_d

    .line 2009
    iput-boolean v12, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsBack:Z

    .line 2010
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getAssignedName()Ljava/lang/String;

    move-result-object v6

    .line 2011
    .local v6, "assignedName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getMediaSetKey()Ljava/lang/String;

    move-result-object v9

    .line 2012
    .local v9, "mediaSetKey":Ljava/lang/String;
    if-eqz v6, :cond_d

    if-eqz v9, :cond_d

    .line 2013
    check-cast v11, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    .end local v11    # "originalSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v11, v6}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->setName(Ljava/lang/String;)V

    .line 2019
    .end local v6    # "assignedName":Ljava/lang/String;
    .end local v9    # "mediaSetKey":Ljava/lang/String;
    :cond_d
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPreviousViewState:Lcom/sec/android/gallery3d/app/ActivityState;

    if-eqz v0, :cond_11

    .line 2020
    const/4 v7, 0x0

    .line 2021
    .local v7, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v0, v2, :cond_e

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPreviousViewState:Lcom/sec/android/gallery3d/app/ActivityState;

    instance-of v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-eqz v0, :cond_e

    .line 2023
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOriginalSetPathString:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v8

    .line 2024
    .local v8, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v8, :cond_e

    .line 2025
    iget v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I

    invoke-virtual {v8, v0, v12}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    check-cast v7, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 2028
    .end local v8    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .restart local v7    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_e
    if-nez v7, :cond_f

    .line 2029
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v7

    .line 2031
    :cond_f
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->isManualRotateIconEnabled()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 2032
    const/4 v10, 0x0

    .line 2034
    :cond_10
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPreviousViewState:Lcom/sec/android/gallery3d/app/ActivityState;

    instance-of v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-eqz v0, :cond_14

    .line 2035
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPreviousViewState:Lcom/sec/android/gallery3d/app/ActivityState;

    check-cast v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-ne v10, v12, :cond_12

    move v2, v12

    :goto_2
    if-nez v7, :cond_13

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mInitialItem:Lcom/sec/android/gallery3d/data/MediaItem;

    :goto_3
    iget v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentMediaSetIndex:I

    iget v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->setPromptRequest(IILjava/lang/Object;II)V

    .line 2040
    .end local v7    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_11
    :goto_4
    invoke-virtual {p0, v12}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->doCommitDeleteImage(Z)V

    .line 2041
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->finishDetailView()V

    goto/16 :goto_0

    .restart local v7    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_12
    move v2, v1

    .line 2035
    goto :goto_2

    :cond_13
    move-object v3, v7

    goto :goto_3

    .line 2037
    :cond_14
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPreviousViewState:Lcom/sec/android/gallery3d/app/ActivityState;

    if-ne v10, v12, :cond_16

    move v0, v12

    :goto_5
    if-nez v7, :cond_15

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mInitialItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .end local v7    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_15
    invoke-virtual {v2, v1, v0, v7}, Lcom/sec/android/gallery3d/app/ActivityState;->setPromptRequest(IILjava/lang/Object;)V

    goto :goto_4

    .restart local v7    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_16
    move v0, v1

    goto :goto_5
.end method

.method public onCommitDeleteImage()V
    .locals 3

    .prologue
    .line 2212
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeletePath:Lcom/sec/android/gallery3d/data/Path;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 2222
    :goto_0
    return-void

    .line 2215
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2216
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->onCommitDeleteMultipleImages()V

    goto :goto_0

    .line 2220
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMenuExecutor:Lcom/sec/android/gallery3d/ui/MenuExecutor;

    const v1, 0x7f0f026e

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeletePath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->startSingleItemAction(ILcom/sec/android/gallery3d/data/Path;)V

    .line 2221
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeletePath:Lcom/sec/android/gallery3d/data/Path;

    goto :goto_0
.end method

.method public onCommitDeleteMultipleImages()V
    .locals 5

    .prologue
    .line 5547
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5573
    :cond_0
    :goto_0
    return-void

    .line 5551
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMenuExecutor:Lcom/sec/android/gallery3d/ui/MenuExecutor;

    const v1, 0x7f0f026e

    const v2, 0x7f0e0041

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    new-instance v4, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$29;

    invoke-direct {v4, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$29;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->startAction(IILjava/util/ArrayList;Lcom/sec/android/gallery3d/ui/MenuExecutor$ProgressListener;)V

    .line 5572
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 4430
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 4431
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 4432
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 4434
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    .line 4435
    .local v0, "size":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v3, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->setScreenSize(II)V

    .line 4439
    .end local v0    # "size":Landroid/graphics/Point;
    :cond_0
    if-eqz p1, :cond_1

    .line 4440
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onConfigChanged(Landroid/content/res/Configuration;)V

    .line 4442
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 4444
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateViewModeIcons(Landroid/content/res/Configuration;)V

    .line 4446
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->checkIndexShouldShow()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIndexEnabled:Z

    if-eqz v1, :cond_2

    .line 4447
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    const-string v2, ""

    iget v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getMediaItemCount()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(Ljava/lang/String;II)V

    .line 4450
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    if-eqz v1, :cond_3

    .line 4451
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->showSideMirrorView(Z)Z

    .line 4454
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorMotionView:Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;

    if-eqz v1, :cond_4

    .line 4455
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorMotionView:Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 4457
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 4458
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 4461
    :cond_5
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v1, :cond_6

    .line 4462
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->resetLayout()V

    .line 4464
    :cond_6
    return-void
.end method

.method public onCoverModeChanged()V
    .locals 0

    .prologue
    .line 5466
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->initActionBar()V

    .line 5467
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateMenuOperations()V

    .line 5468
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->showBars()V

    .line 5469
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 33
    .param p1, "data"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    .line 917
    const-string v3, "Gallery_Performance"

    const-string v4, "DetailViewState onCreate Start"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 920
    if-nez p1, :cond_0

    .line 921
    new-instance p1, Landroid/os/Bundle;

    .end local p1    # "data":Landroid/os/Bundle;
    invoke-direct/range {p1 .. p1}, Landroid/os/Bundle;-><init>()V

    .line 923
    .restart local p1    # "data":Landroid/os/Bundle;
    :cond_0
    new-instance v4, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v3, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-direct {v4, v3}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;-><init>(Lcom/sec/android/gallery3d/app/GalleryActivity;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    .line 924
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryActionBar()Lcom/sec/android/gallery3d/app/GalleryActionBar;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBar:Lcom/sec/android/gallery3d/app/GalleryActionBar;

    .line 925
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 926
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getNewAlbumSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 927
    new-instance v3, Lcom/sec/android/gallery3d/ui/MenuExecutor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-direct {v3, v4, v5}, Lcom/sec/android/gallery3d/ui/MenuExecutor;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMenuExecutor:Lcom/sec/android/gallery3d/ui/MenuExecutor;

    .line 930
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    .line 931
    new-instance v3, Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v0, p0

    invoke-direct {v3, v4, v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/app/ActivityState;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 932
    new-instance v3, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v3, v4}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 933
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    .line 934
    new-instance v3, Lcom/sec/android/gallery3d/util/KeyBoardManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/sec/android/gallery3d/util/KeyBoardManager;-><init>(Lcom/sec/android/gallery3d/app/GalleryContext;Z)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mKeyBoardManager:Lcom/sec/android/gallery3d/util/KeyBoardManager;

    .line 935
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGlRootView()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 936
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryCoverMode()Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    .line 937
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 938
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFromGalleryWidget(Landroid/content/Context;)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFromGalleryWidget:Z

    .line 939
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v21

    .line 940
    .local v21, "intent":Landroid/content/Intent;
    const-string v3, "from-WeTogether"

    const/4 v4, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFromWeTogether:Z

    .line 942
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    if-eqz v3, :cond_1d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isCoverMode()Z

    move-result v3

    if-eqz v3, :cond_1d

    const/16 v16, 0x1

    .line 943
    .local v16, "bCoverMode":Z
    :goto_0
    if-nez v16, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCameraQuickViewOnLockscreen(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 944
    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFlags:I

    or-int/lit8 v3, v3, 0x20

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFlags:I

    .line 947
    :cond_2
    const-string v3, "mime-type"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMimeType:Ljava/lang/String;

    .line 948
    const-string v3, "KEY_GROUP_INDEX"

    const-wide/16 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGroupIndex:J

    .line 949
    const-string v3, "KEY_ITEM_RCS"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mRcsUri:Z

    .line 951
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setFullScreenMode(Z)V

    .line 952
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAirBrowse:Z

    if-eqz v3, :cond_3

    .line 953
    new-instance v3, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAirMotionSettingsReceiver:Landroid/content/BroadcastReceiver;

    const/4 v7, 0x1

    invoke-direct {v3, v4, v5, v7}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;-><init>(Landroid/content/Context;Landroid/content/BroadcastReceiver;I)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    .line 958
    :cond_3
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHoveringUI:Z

    if-eqz v3, :cond_4

    .line 959
    new-instance v3, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHoverGestureDetector:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    .line 963
    :cond_4
    new-instance v3, Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    .line 964
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->setListener(Lcom/sec/android/gallery3d/ui/PhotoView$Listener;)V

    .line 965
    new-instance v3, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/PositionController;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    .line 966
    new-instance v3, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/PositionController;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOutOfFocusShotHelpView:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    .line 967
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v3, :cond_5

    .line 968
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    new-instance v4, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$2;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$2;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->setListener(Lcom/sec/android/gallery3d/ui/PhotoViewIcon$UpdateInterface;)V

    .line 977
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->setPhotoViewIcon(Lcom/sec/android/gallery3d/ui/PhotoViewIcon;)V

    .line 978
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/GLView;->addComponent(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 980
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-nez v16, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCameraQuickViewOnLockscreen(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_1e

    :cond_6
    const/4 v3, 0x1

    :goto_1
    invoke-virtual {v4, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->setOffContextualTag(Z)V

    .line 982
    const-string v3, "face_id"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v17

    .line 983
    .local v17, "curFaceId":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->setCurrentFaceId(I)V

    .line 984
    const-string v3, "KEY_MEDIA_ITEM_PATH"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    .line 986
    .local v6, "itemPath":Lcom/sec/android/gallery3d/data/Path;
    const/16 v24, 0x0

    .line 987
    .local v24, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v3, v6}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v26

    .line 988
    .local v26, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v26, :cond_7

    move-object/from16 v0, v26

    instance-of v3, v0, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v3, :cond_7

    move-object/from16 v24, v26

    .line 989
    check-cast v24, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 992
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->setUserInteractionListener(Lcom/sec/android/gallery3d/ui/UserInteractionListener;)V

    .line 993
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOnSystemGenericMotionListener:Landroid/view/View$OnGenericMotionListener;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->setOnGenericMotionListener(Landroid/view/View$OnGenericMotionListener;)V

    .line 994
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-static {v3, v4}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeatureFactory;->getDefualt(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/PhotoView;)Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFaceTagFeature:Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;

    .line 995
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFaceTagFeature:Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->setFaceFeature(Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;)V

    .line 996
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->isLaunchFromSetupWidzard()Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFromSetupWidzard:Z

    .line 998
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v3, :cond_8

    .line 999
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBars:Z

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->setShowBarState(Z)V

    .line 1002
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/app/GalleryApp;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 1003
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getOrientationManager()Lcom/sec/android/gallery3d/app/OrientationManager;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOrientationManager:Lcom/sec/android/gallery3d/app/OrientationManager;

    .line 1004
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOrientationManager:Lcom/sec/android/gallery3d/app/OrientationManager;

    invoke-interface {v3, v4}, Lcom/sec/android/gallery3d/ui/GLRoot;->setOrientationSource(Lcom/sec/android/gallery3d/ui/OrientationSource;)V

    .line 1007
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDrmUtil:Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    .line 1008
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->getNearbyClient()Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    .line 1010
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWfd:Z

    if-eqz v3, :cond_9

    .line 1011
    new-instance v3, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v3, v4}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    .line 1012
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->setDetailViewRef(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    .line 1013
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->registerReceiver()V

    .line 1016
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    new-instance v4, Lcom/sec/samsung/gallery/util/SmartClipUtil$SmartClipListener;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v0, p0

    invoke-direct {v4, v5, v7, v0}, Lcom/sec/samsung/gallery/util/SmartClipUtil$SmartClipListener;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    invoke-static {v3, v4}, Lcom/samsung/android/smartclip/SmartClipMetaUtils;->setDataExtractionListener(Landroid/view/View;Lcom/samsung/android/smartclip/SmartClipDataExtractionListener;)Z

    .line 1019
    new-instance v3, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$3;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/android/gallery3d/ui/GLRoot;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    .line 1173
    const-string v3, "KEY_MEDIA_SET_PATH"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSetPathString:Ljava/lang/String;

    .line 1174
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSetPathString:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOriginalSetPathString:Ljava/lang/String;

    .line 1175
    const-string v3, "KEY_MEDIA_ITEM_PATH"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 1176
    .local v23, "itemPathString":Ljava/lang/String;
    if-eqz v23, :cond_1f

    const-string v3, "KEY_MEDIA_ITEM_PATH"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    .line 1177
    :goto_2
    const-string/jumbo v3, "treat-back-as-up"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mTreatBackAsUp:Z

    .line 1178
    const-string/jumbo v3, "start-in-filmstrip"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStartInFilmstrip:Z

    .line 1179
    const-string v3, "in_camera_roll"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v20

    .line 1181
    .local v20, "inCameraRoll":Z
    const-string v3, "add_camera_shortcut"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v15

    .line 1182
    .local v15, "addCameraShortCut":Z
    const-string v3, "expansion_detail_view"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    .line 1183
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    if-eqz v3, :cond_a

    .line 1184
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->setOffContextualTag(Z)V

    .line 1187
    :cond_a
    new-instance v3, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;-><init>(Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowbarUpdateListener;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    .line 1189
    const-string v3, "KEY_ITEM_POSITION"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I

    .line 1190
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSetPathString:Ljava/lang/String;

    if-eqz v3, :cond_24

    .line 1191
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowSpinner:Z

    .line 1192
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseShowCameraShortCut:Z

    if-eqz v3, :cond_c

    if-nez v20, :cond_b

    if-eqz v15, :cond_c

    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCameraAvailable(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 1193
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/combo/item/{/filter/camera_shortcut,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSetPathString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "}"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSetPathString:Ljava/lang/String;

    .line 1194
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I

    .line 1195
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHasCameraScreennailOrPlaceholder:Z

    .line 1198
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSetPathString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v28

    .line 1199
    .local v28, "originalSet":Lcom/sec/android/gallery3d/data/MediaSet;
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHasCameraScreennailOrPlaceholder:Z

    if-eqz v3, :cond_d

    move-object/from16 v0, v28

    instance-of v3, v0, Lcom/sec/android/gallery3d/data/ComboAlbum;

    if-eqz v3, :cond_d

    move-object/from16 v3, v28

    .line 1202
    check-cast v3, Lcom/sec/android/gallery3d/data/ComboAlbum;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/data/ComboAlbum;->useNameOfChild(I)V

    .line 1206
    :cond_d
    move-object/from16 v0, v28

    instance-of v3, v0, Lcom/sec/android/gallery3d/data/UriListSet;

    if-eqz v3, :cond_e

    move-object/from16 v31, v28

    .line 1207
    check-cast v31, Lcom/sec/android/gallery3d/data/UriListSet;

    .line 1208
    .local v31, "uriListSet":Lcom/sec/android/gallery3d/data/UriListSet;
    const-string v3, "android.intent.extra.STREAM"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v32

    .line 1209
    .local v32, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual/range {v31 .. v32}, Lcom/sec/android/gallery3d/data/UriListSet;->initUris(Ljava/util/ArrayList;)V

    .line 1213
    .end local v31    # "uriListSet":Lcom/sec/android/gallery3d/data/UriListSet;
    .end local v32    # "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v0, v28

    invoke-virtual {v3, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setSourceMediaSet(Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 1214
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/filter/delete/{"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSetPathString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "}"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSetPathString:Ljava/lang/String;

    .line 1215
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSetPathString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    .line 1216
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    if-nez v3, :cond_20

    .line 1217
    sget-object v3, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed to restore "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSetPathString:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1224
    :goto_3
    if-nez v6, :cond_10

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    if-eqz v3, :cond_10

    .line 1225
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getMediaItemCount()I

    move-result v25

    .line 1226
    .local v25, "mediaItemCount":I
    if-lez v25, :cond_1c

    .line 1227
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I

    move/from16 v0, v25

    if-lt v3, v0, :cond_f

    .line 1228
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I

    .line 1229
    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    .line 1237
    .end local v25    # "mediaItemCount":I
    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v3, v6}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v27

    .line 1238
    .local v27, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    move-object/from16 v0, v27

    instance-of v3, v0, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v3, :cond_21

    .line 1239
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v3, v6}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mInitialItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1244
    :goto_4
    const-string v3, "KEY_MEDIA_SET_POSITION"

    const/4 v4, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentMediaSetIndex:I

    .line 1246
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    if-eqz v3, :cond_14

    .line 1247
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 1248
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    if-nez v3, :cond_22

    const/4 v3, 0x1

    :goto_5
    invoke-virtual {v4, v3}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->setUseBurstShotGrouping(Z)V

    .line 1249
    new-instance v3, Lcom/sec/android/gallery3d/app/AlbumReloader;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-direct {v3, v4, v5}, Lcom/sec/android/gallery3d/app/AlbumReloader;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaSet;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    .line 1250
    new-instance v2, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I

    const/4 v8, -0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    invoke-direct/range {v2 .. v11}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/Path;IIZZLcom/sec/android/gallery3d/app/AlbumReloader;)V

    .line 1251
    .local v2, "pda":Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->initFilmStripView()V

    .line 1254
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    .line 1255
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->setModel(Lcom/sec/android/gallery3d/ui/PhotoView$Model;)V

    .line 1257
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getPreviousActivityState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v29

    .line 1258
    .local v29, "prevActivity":Lcom/sec/android/gallery3d/app/ActivityState;
    move-object/from16 v0, v29

    instance-of v3, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-nez v3, :cond_11

    move-object/from16 v0, v29

    instance-of v3, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    if-eqz v3, :cond_12

    .line 1259
    :cond_11
    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPreviousViewState:Lcom/sec/android/gallery3d/app/ActivityState;

    .line 1260
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPreviousViewState:Lcom/sec/android/gallery3d/app/ActivityState;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v5, v7}, Lcom/sec/android/gallery3d/app/ActivityState;->setPromptRequest(IILjava/lang/Object;)V

    .line 1261
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->isUpButtonVisible()Z

    move-result v3

    if-nez v3, :cond_23

    const/4 v3, 0x1

    :goto_6
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mBackByZoomOut:Z

    .line 1263
    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getPreviousBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    if-eqz v3, :cond_13

    .line 1264
    new-instance v3, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getPreviousBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;-><init>(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getPreviousRotation()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->setInitialScreenNail(Lcom/sec/android/gallery3d/ui/ScreenNail;I)V

    .line 1266
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/app/StateManager;->setPreviousActivityState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 1267
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/app/StateManager;->setPreviousBitmap(Landroid/graphics/Bitmap;I)V

    .line 1269
    new-instance v3, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$4;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->setDataListener(Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;)V

    .line 1549
    .end local v2    # "pda":Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .end local v27    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v28    # "originalSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v29    # "prevActivity":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_14
    :goto_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStartInFilmstrip:Z

    if-eqz v3, :cond_28

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    if-eqz v3, :cond_28

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getMediaItemCount()I

    move-result v3

    const/4 v5, 0x1

    if-le v3, v5, :cond_28

    const/4 v3, 0x1

    :goto_8
    invoke-virtual {v4, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->setFilmMode(Z)V

    .line 1552
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v3, v6}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v22

    check-cast v22, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1553
    .local v22, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isFromMyFilesAndUpKeyDisable()Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMyFilesAndAll:Z

    .line 1554
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isFromUriList()Z

    move-result v3

    if-nez v3, :cond_15

    move-object/from16 v0, v22

    instance-of v3, v0, Lcom/sec/android/gallery3d/data/UriImage;

    if-nez v3, :cond_15

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMyFilesAndAll:Z

    if-nez v3, :cond_15

    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isViewByFilterLocal()Z

    move-result v3

    if-eqz v3, :cond_29

    :cond_15
    const/4 v3, 0x0

    :goto_9
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsUpAvailableForNormal:Z

    .line 1556
    const-string v3, "KEY_ITEM_IS_HIDDEN"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsHiddenItem:Z

    .line 1558
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    if-eqz v3, :cond_16

    .line 1559
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->initImageNoteView()V

    .line 1562
    :cond_16
    invoke-static {}, Landroid/util/GateConfig;->isGateEnabled()Z

    move-result v3

    if-eqz v3, :cond_17

    .line 1563
    const-string v3, "GATE"

    const-string v4, "<GATE-M>PICTURE_OPENED: filePath is removed due to security </GATE-M>"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1567
    :cond_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v3, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/GalleryActivity;->isFromCamera()Z

    move-result v19

    .line 1568
    .local v19, "fromCamera":Z
    if-eqz v19, :cond_19

    .line 1569
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isWrongOrientationFromCamera()Z

    move-result v3

    if-eqz v3, :cond_18

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v3, :cond_18

    .line 1570
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->setRequestedOrientation(I)V

    .line 1573
    :cond_18
    invoke-direct/range {p0 .. p1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->initCameraQuickViewTimer(Landroid/os/Bundle;)V

    .line 1575
    :cond_19
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFirstTimeLaunch:Z

    if-nez v3, :cond_1a

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v3

    if-nez v3, :cond_1a

    .line 1576
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFirstTimeLaunch:Z

    .line 1579
    :cond_1a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAnimationInterface()Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    .line 1581
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDecoderInterface()Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    .line 1583
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v3

    if-eqz v3, :cond_1b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v3, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/GalleryActivity;->isFromCamera()Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 1584
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v14

    .line 1585
    .local v14, "actionBar":Landroid/app/ActionBar;
    const v3, 0x7f02000e

    invoke-virtual {v14, v3}, Landroid/app/ActionBar;->setIcon(I)V

    .line 1586
    const/4 v3, 0x0

    invoke-virtual {v14, v3}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 1587
    new-instance v8, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$7;

    const-wide/16 v10, 0x3e8

    const-wide/16 v12, 0x3e8

    move-object/from16 v9, p0

    invoke-direct/range {v8 .. v14}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$7;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;JJLandroid/app/ActionBar;)V

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$7;->start()Landroid/os/CountDownTimer;

    .line 1617
    .end local v14    # "actionBar":Landroid/app/ActionBar;
    :cond_1b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v4, "PHOTO_PAGE"

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 1619
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoPageViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 1624
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    .line 1626
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHelpMode:I

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->setHelpMode(I)V

    .line 1627
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v4, "image/*"

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEditorAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHaveImageEditor:Z

    .line 1628
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->VIEW:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3, v4, v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 1630
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKnoxEnabledStateOfKnox2(Landroid/content/Context;)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsKnox2Enabled:Z

    .line 1631
    invoke-static {}, Lcom/sec/samsung/gallery/util/HoverIconUtil;->setHoveringIconToDefault()V

    .line 1633
    const-string v3, "Gallery_Performance"

    const-string v4, "DetailViewState onCreate End"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1635
    .end local v19    # "fromCamera":Z
    .end local v22    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1c
    return-void

    .line 942
    .end local v6    # "itemPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v15    # "addCameraShortCut":Z
    .end local v16    # "bCoverMode":Z
    .end local v17    # "curFaceId":I
    .end local v20    # "inCameraRoll":Z
    .end local v23    # "itemPathString":Ljava/lang/String;
    .end local v24    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v26    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1d
    const/16 v16, 0x0

    goto/16 :goto_0

    .line 980
    .restart local v16    # "bCoverMode":Z
    :cond_1e
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 1176
    .restart local v6    # "itemPath":Lcom/sec/android/gallery3d/data/Path;
    .restart local v17    # "curFaceId":I
    .restart local v23    # "itemPathString":Ljava/lang/String;
    .restart local v24    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v26    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1f
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 1221
    .restart local v15    # "addCameraShortCut":Z
    .restart local v20    # "inCameraRoll":Z
    .restart local v28    # "originalSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_20
    const/4 v3, -0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mResultIntent:Landroid/content/Intent;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setStateResult(ILandroid/content/Intent;)V

    goto/16 :goto_3

    .line 1241
    .restart local v27    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_21
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mInitialItem:Lcom/sec/android/gallery3d/data/MediaItem;

    goto/16 :goto_4

    .line 1248
    :cond_22
    const/4 v3, 0x0

    goto/16 :goto_5

    .line 1261
    .restart local v2    # "pda":Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .restart local v29    # "prevActivity":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_23
    const/4 v3, 0x0

    goto/16 :goto_6

    .line 1461
    .end local v2    # "pda":Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .end local v27    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v28    # "originalSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v29    # "prevActivity":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_24
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v3, v6}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v27

    .line 1462
    .restart local v27    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v27, :cond_1c

    .line 1463
    check-cast v27, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local v27    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mInitialItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1464
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mInitialItem:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v3, v3, Lcom/sec/android/gallery3d/data/UriImage;

    if-eqz v3, :cond_25

    .line 1465
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mInitialItem:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v3, Lcom/sec/android/gallery3d/data/UriImage;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMimeType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/data/UriImage;->setMimeType(Ljava/lang/String;)V

    .line 1467
    :cond_25
    new-instance v3, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mInitialItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-direct {v3, v4, v5, v7}, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/data/MediaItem;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    .line 1468
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->setModel(Lcom/sec/android/gallery3d/ui/PhotoView$Model;)V

    .line 1469
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mInitialItem:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateCurrentPhoto(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 1470
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowSpinner:Z

    .line 1473
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSingleMode:Z

    .line 1474
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    if-eqz v3, :cond_26

    .line 1475
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryId()I

    move-result v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mInitialItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3, v4, v5, v7}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->playPendedPlayer(ILcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 1476
    :cond_26
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v18

    .line 1477
    .local v18, "currentMediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v18, :cond_27

    move-object/from16 v0, v18

    instance-of v3, v0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v3, :cond_27

    .line 1478
    new-instance v3, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$5;

    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$5;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mContentObserver:Landroid/database/ContentObserver;

    .line 1509
    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    check-cast v18, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v18    # "currentMediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    move-object/from16 v0, v18

    iget v4, v0, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v30

    .line 1511
    .local v30, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mContentObserver:Landroid/database/ContentObserver;

    move-object/from16 v0, v30

    invoke-virtual {v3, v0, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1516
    .end local v30    # "uri":Landroid/net/Uri;
    :cond_27
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    check-cast v3, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;

    new-instance v4, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$6;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$6;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/app/SinglePhotoDataAdapter;->setLoadingListener(Lcom/sec/android/gallery3d/app/LoadingListener;)V

    goto/16 :goto_7

    .line 1549
    :cond_28
    const/4 v3, 0x0

    goto/16 :goto_8

    .line 1554
    .restart local v22    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_29
    const/4 v3, 0x1

    goto/16 :goto_9
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 3458
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMenu:Landroid/view/Menu;

    .line 3459
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 3460
    return-void
.end method

.method public onCurrentImageUpdated()V
    .locals 1

    .prologue
    .line 2573
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/ui/GLRoot;->unfreeze()V

    .line 2574
    return-void
.end method

.method public onDeleteImage(Lcom/sec/android/gallery3d/data/Path;I)V
    .locals 2
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "offset"    # I

    .prologue
    .line 2186
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->onCommitDeleteImage()V

    .line 2187
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeletePath:Lcom/sec/android/gallery3d/data/Path;

    .line 2188
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteIsFocus:Z

    .line 2189
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    iget v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I

    add-int/2addr v1, p2

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->addDeletion(Lcom/sec/android/gallery3d/data/Path;I)V

    .line 2190
    return-void

    .line 2188
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDeleteMultipleImages()V
    .locals 7

    .prologue
    .line 5520
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->onCommitDeleteMultipleImages()V

    .line 5521
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 5522
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCloneMediaList()Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 5523
    .local v3, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4, v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedIndex(Lcom/sec/android/gallery3d/data/MediaObject;)I

    move-result v1

    .line 5524
    .local v1, "index":I
    if-ltz v1, :cond_0

    .line 5527
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHasCameraScreennailOrPlaceholder:Z

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    :goto_1
    sub-int v4, v1, v4

    invoke-virtual {v5, v6, v4}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->addDeletion(Lcom/sec/android/gallery3d/data/Path;I)V

    .line 5528
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 5527
    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 5530
    .end local v1    # "index":I
    .end local v3    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    .line 5532
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2839
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/sec/android/gallery3d/ui/GLRoot;->setOrientationSource(Lcom/sec/android/gallery3d/ui/OrientationSource;)V

    .line 2841
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->clearFlagShowWhenLocked()V

    .line 2843
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWfd:Z

    if-eqz v0, :cond_0

    .line 2844
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->setDetailViewRef(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    .line 2845
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mWifiDisplayUtils:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->unregisterReceiver()V

    .line 2849
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->stopSoundScene()V

    .line 2850
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setSmartClipDataExtractionListener(Lcom/samsung/android/smartclip/SmartClipDataExtractionListener;)Z

    .line 2851
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 2853
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v0, :cond_1

    .line 2854
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->destroy()V

    .line 2857
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_2

    .line 2859
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2862
    :cond_2
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHWKey:Z

    if-nez v0, :cond_3

    .line 2863
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setSystemUiVisibility(I)V

    .line 2866
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    if-eqz v0, :cond_4

    .line 2867
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->destroy()V

    .line 2870
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mBrokenImage:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_5

    .line 2871
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mBrokenImage:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 2872
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mBrokenImage:Landroid/graphics/Bitmap;

    .line 2875
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mContentObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_6

    .line 2876
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 2877
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mContentObserver:Landroid/database/ContentObserver;

    .line 2880
    :cond_6
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    if-eqz v0, :cond_7

    .line 2881
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->removePlayerStateListener(Lcom/sec/android/gallery3d/remote/nearby/NearbyClient$IOnPlayerStateListener;)V

    .line 2882
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->isOnPlaying()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2883
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->disconnectWithPlayDevice()V

    .line 2886
    :cond_7
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFaceTagFeature:Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;->disableBuddyShare()V

    .line 2888
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    if-nez v0, :cond_8

    .line 2889
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->leaveSelectionMode()V

    .line 2890
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 2892
    :cond_8
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v1, "PHOTO_PAGE"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 2894
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    if-eqz v0, :cond_9

    .line 2895
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->destroy()V

    .line 2897
    :cond_9
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    if-eqz v0, :cond_a

    .line 2898
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->removeRotationAnimation()V

    .line 2900
    :cond_a
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v0, :cond_b

    .line 2901
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->removeContactPopupView()V

    .line 2904
    :cond_b
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    if-eqz v0, :cond_c

    .line 2905
    invoke-static {}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->deleteBGimage()V

    .line 2910
    :cond_c
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v0, :cond_d

    .line 2911
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->destoryAirMotionDetector()V

    .line 2914
    :cond_d
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mChangePlayerDialog:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    if-eqz v0, :cond_e

    .line 2915
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mChangePlayerDialog:Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListDialog;->dismiss()V

    .line 2918
    :cond_e
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    if-eqz v0, :cond_f

    .line 2919
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->onDestroy()V

    .line 2922
    :cond_f
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOutOfFocusShotHelpView:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    if-eqz v0, :cond_10

    .line 2923
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOutOfFocusShotHelpView:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->removeView()V

    .line 2925
    :cond_10
    invoke-static {}, Lcom/sec/samsung/gallery/view/utils/ContextualTagConts$ContextualTagView;->releaseInstance()V

    .line 2926
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->clearExpMediaItem()V

    .line 2928
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->onDestroy()V

    .line 2929
    return-void
.end method

.method public onDirty()V
    .locals 3

    .prologue
    .line 2226
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOriginalSetPathString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 2227
    .local v0, "originalMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    if-eqz v1, :cond_0

    .line 2228
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->IsCategoryView(Lcom/sec/samsung/gallery/core/TabTagType;Ljava/lang/Boolean;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2229
    check-cast v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    .end local v0    # "originalMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->updateMediaSet()V

    .line 2231
    :cond_0
    return-void
.end method

.method public onDown()V
    .locals 1

    .prologue
    .line 5483
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCameraQuickViewTimer:Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;

    if-eqz v0, :cond_0

    .line 5484
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCameraQuickViewTimer:Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/CameraQuickViewTimer;->cancelTimer()V

    .line 5485
    :cond_0
    return-void
.end method

.method public onFilmModeChanged(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    const/4 v2, 0x1

    .line 2578
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowSpinner:Z

    if-eqz v0, :cond_0

    .line 2579
    if-eqz p1, :cond_2

    .line 2580
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBar:Lcom/sec/android/gallery3d/app/GalleryActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/gallery3d/app/GalleryActionBar;->enableAlbumModeMenu(ILcom/sec/android/gallery3d/app/GalleryActionBar$OnAlbumModeSelectedListener;)V

    .line 2586
    :cond_0
    :goto_0
    if-eqz p1, :cond_3

    .line 2587
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 2588
    const-string v0, "Gallery"

    const-string v1, "FilmstripPage"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/util/UsageStatistics;->onContentViewChanged(Ljava/lang/String;Ljava/lang/String;)V

    .line 2591
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v0, :cond_1

    .line 2592
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->hide()V

    .line 2603
    :cond_1
    :goto_1
    return-void

    .line 2583
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBar:Lcom/sec/android/gallery3d/app/GalleryActionBar;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/app/GalleryActionBar;->disableAlbumModeMenu(Z)V

    goto :goto_0

    .line 2596
    :cond_3
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->refreshHidingMessage()V

    .line 2598
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v0, :cond_1

    .line 2599
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->show()V

    goto :goto_1
.end method

.method public onFullScreenChanged(Z)V
    .locals 5
    .param p1, "full"    # Z

    .prologue
    const/4 v2, 0x0

    .line 2171
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 2172
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x2

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v4, v1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 2174
    .local v0, "m":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 2176
    .end local v0    # "m":Landroid/os/Message;
    :cond_0
    return-void

    :cond_1
    move v1, v2

    .line 2172
    goto :goto_0
.end method

.method public onLongPress()V
    .locals 1

    .prologue
    .line 5478
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsLongPressFinished:Z

    .line 5479
    return-void
.end method

.method public onMWLayoutChanged()V
    .locals 5

    .prologue
    .line 5070
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v2, :cond_0

    .line 5071
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v0

    .line 5072
    .local v0, "multiWindow":Lcom/sec/android/gallery3d/app/GalleryMultiWindow;
    if-nez v0, :cond_1

    .line 5103
    .end local v0    # "multiWindow":Lcom/sec/android/gallery3d/app/GalleryMultiWindow;
    :cond_0
    :goto_0
    return-void

    .line 5074
    .restart local v0    # "multiWindow":Lcom/sec/android/gallery3d/app/GalleryMultiWindow;
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->updateMultiWindowSize(Landroid/content/Context;)V

    .line 5075
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMenu:Landroid/view/Menu;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->updateMenuOperation(Landroid/view/Menu;)V

    .line 5080
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v1

    .line 5082
    .local v1, "size":Landroid/graphics/Point;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v2, :cond_2

    .line 5083
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->onMWLayoutChanged()V

    .line 5085
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mWindowSize:Landroid/graphics/Point;

    iget v3, v1, Landroid/graphics/Point;->x:I

    iget v4, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Point;->equals(II)Z

    move-result v2

    if-nez v2, :cond_0

    .line 5088
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mWindowSize:Landroid/graphics/Point;

    iget v3, v1, Landroid/graphics/Point;->x:I

    iget v4, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Point;->set(II)V

    .line 5090
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v2, :cond_3

    .line 5091
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mWindowSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mWindowSize:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->setScreenSize(II)V

    .line 5092
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->invalidate()V

    .line 5095
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v2, :cond_4

    .line 5096
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/FilmStripView;->updateItemSize()V

    .line 5098
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setMultiWindow(Landroid/content/Context;)V

    .line 5099
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    if-eqz v2, :cond_0

    .line 5100
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->updateLayout()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 10
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const v9, 0x7f0f0281

    const v8, 0x7f0f0280

    const/4 v7, 0x1

    const v6, 0x102002c

    .line 3490
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v4

    if-nez v4, :cond_1

    .line 3491
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 3492
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 3493
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 3496
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 3498
    .local v2, "itemId":I
    if-ne v2, v6, :cond_4

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v4, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/GalleryActivity;->isFromCamera()Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v4, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/GalleryActivity;->isFromMyFiles()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 3501
    :cond_2
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->handleGoUpButton()V

    .line 3555
    :cond_3
    :goto_0
    return-void

    .line 3505
    :cond_4
    if-ne v2, v6, :cond_5

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getStateCount()I

    move-result v4

    if-ne v4, v7, :cond_5

    .line 3507
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->handleGoUpButton()V

    goto :goto_0

    .line 3512
    :cond_5
    if-ne v2, v6, :cond_9

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v4

    if-nez v4, :cond_9

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->isUpButtonVisible()Z

    move-result v4

    if-eqz v4, :cond_9

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsHiddenItem:Z

    if-nez v4, :cond_9

    .line 3515
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v4

    if-nez v4, :cond_7

    .line 3516
    :cond_6
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->handleGoUpButton()V

    goto :goto_0

    .line 3517
    :cond_7
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMyFilesAndAll:Z

    if-eqz v4, :cond_8

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsUpAvailableForNormal:Z

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v4, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/GalleryActivity;->isFromCamera()Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_8
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 3518
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->onBackPressed()V

    goto :goto_0

    .line 3523
    :cond_9
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v4, v4, Lcom/sec/android/gallery3d/data/HelpImage;

    if-nez v4, :cond_3

    .line 3527
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v4, p1}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->isModalDownloadNeeded(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/view/MenuItem;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 3528
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->enqueueMenuItemAfterModalDownload(I)V

    .line 3529
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaItemArrayList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->getModalDownloadIntent(Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    .line 3531
    .local v1, "intent":Landroid/content/Intent;
    :try_start_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/16 v5, 0x30b

    invoke-virtual {v4, v1, v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 3532
    :catch_0
    move-exception v0

    .line 3533
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v4, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TAG:Ljava/lang/String;

    const-string v5, "Activity Not Found"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3534
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 3539
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_a
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onOptionsItemSelected(Landroid/view/MenuItem;)V

    .line 3540
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    if-eqz v4, :cond_c

    if-eq v2, v8, :cond_b

    if-ne v2, v9, :cond_c

    .line 3541
    :cond_b
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    instance-of v4, v4, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-eqz v4, :cond_c

    .line 3542
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    check-cast v3, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .line 3543
    .local v3, "mediaItemAdapter":Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    sget-object v5, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SEARCH:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v4, v5, :cond_c

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v4, :cond_c

    .line 3544
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->reloadForRotate()V

    .line 3550
    .end local v3    # "mediaItemAdapter":Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    :cond_c
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    if-eqz v4, :cond_3

    if-eq v2, v8, :cond_d

    if-ne v2, v9, :cond_3

    .line 3552
    :cond_d
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    invoke-virtual {v4, v7}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->setNeedtoUpdateImageNoteIcon(Z)V

    .line 3553
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public onPause()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2451
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->onPause()V

    .line 2452
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsActive:Z

    .line 2454
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/gallery3d/ui/GLRoot;->unfreeze()V

    .line 2455
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 2457
    invoke-static {}, Lcom/sec/android/gallery3d/ui/DetailsHelper;->pause()V

    .line 2459
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    if-eqz v1, :cond_0

    .line 2460
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AlbumReloader;->pause()V

    .line 2462
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    if-eqz v1, :cond_1

    .line 2463
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->pause()V

    .line 2465
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->pause()V

    .line 2466
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 2468
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 2471
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBar:Lcom/sec/android/gallery3d/app/GalleryActionBar;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMenuVisibilityListener:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$MyMenuVisibilityListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/GalleryActionBar;->removeOnMenuVisibilityListener(Landroid/app/ActionBar$OnMenuVisibilityListener;)V

    .line 2472
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowSpinner:Z

    if-eqz v1, :cond_2

    .line 2473
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBar:Lcom/sec/android/gallery3d/app/GalleryActionBar;

    invoke-virtual {v1, v6}, Lcom/sec/android/gallery3d/app/GalleryActionBar;->disableAlbumModeMenu(Z)V

    .line 2475
    :cond_2
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->onCommitDeleteImage()V

    .line 2476
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMenuExecutor:Lcom/sec/android/gallery3d/ui/MenuExecutor;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/MenuExecutor;->pause()V

    .line 2481
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAgifVersion:J

    .line 2482
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->stopSoundScene()V

    .line 2483
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHdmi3D:Lcom/sec/android/gallery3d/ui/Hdmi3D;

    if-eqz v1, :cond_3

    .line 2484
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->setHdmi3D(Lcom/sec/android/gallery3d/ui/Hdmi3D;)V

    .line 2485
    iput-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHdmi3D:Lcom/sec/android/gallery3d/ui/Hdmi3D;

    .line 2487
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    if-eqz v1, :cond_4

    .line 2488
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->onPause()V

    .line 2491
    :cond_4
    invoke-static {}, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->cancelTask()V

    .line 2492
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    if-eqz v1, :cond_5

    .line 2493
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->pause()V

    .line 2496
    :cond_5
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v1, :cond_6

    .line 2497
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->pause()V

    .line 2498
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAnimation(Lcom/sec/android/gallery3d/anim/Animation;)V

    .line 2499
    iput-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;

    .line 2502
    :cond_6
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v1, :cond_7

    .line 2503
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFaceTagFeature:Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;

    invoke-interface {v1, v4}, Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;->setManualFaceTagUri(Landroid/net/Uri;)V

    .line 2506
    :cond_7
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 2507
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDrmUtil:Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->setDrmPopupListener(Lcom/sec/android/gallery3d/util/ImageDRMUtil$DrmPopupListener;)V

    .line 2508
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v1, :cond_8

    .line 2509
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    .line 2512
    :cond_8
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHoverGestureDetector:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    if-eqz v1, :cond_9

    .line 2513
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHoverGestureDetector:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->releaseHoverListenerAndEvent()V

    .line 2516
    :cond_9
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    if-eqz v1, :cond_a

    .line 2517
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->unsetPhotoView()V

    .line 2518
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->unsetPhotoDataAdapter()V

    .line 2521
    :cond_a
    invoke-direct {p0, v5, v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->enableDnieMode(ZZ)V

    .line 2523
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEVFHandler:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    if-eqz v1, :cond_b

    .line 2524
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEVFHandler:Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/CameraViewFinderHandler;->onPause()V

    .line 2527
    :cond_b
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDetailViewBeam:Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;

    if-eqz v1, :cond_c

    .line 2528
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDetailViewBeam:Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->unsetBeamListener()V

    .line 2529
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDetailViewBeam:Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->unregisterSettingObserver()V

    .line 2532
    :cond_c
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOutOfFocusShotHelpView:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    if-eqz v1, :cond_d

    .line 2533
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOutOfFocusShotHelpView:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->removeView()V

    .line 2535
    :cond_d
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHdmiListener:Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/HdmiUtils;->removeHdmiConnectListener(Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;)V

    .line 2543
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isAvailableEAM()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 2544
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->unregisterEAM(Landroid/content/BroadcastReceiver;)V

    .line 2548
    :cond_e
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->isFromCamera()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 2549
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    if-eqz v1, :cond_f

    .line 2550
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->isOnPlaying()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 2551
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->disconnectWithPlayDevice()V

    .line 2557
    :cond_f
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1, v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->setIsPhotoPage(Z)V

    .line 2558
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsBack:Z

    if-eqz v1, :cond_11

    .line 2559
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsBack:Z

    .line 2560
    const/4 v0, 0x0

    .line 2561
    .local v0, "originalSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOriginalSetPathString:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 2562
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOriginalSetPathString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 2564
    :cond_10
    if-eqz v0, :cond_11

    instance-of v1, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    if-eqz v1, :cond_11

    .line 2565
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->notifyContentChanged()V

    .line 2569
    .end local v0    # "originalSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_11
    return-void
.end method

.method public onPictureCenter(Z)V
    .locals 3
    .param p1, "isCamera"    # Z

    .prologue
    const/4 v2, 0x6

    const/4 v0, 0x0

    .line 1641
    if-nez p1, :cond_0

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHasCameraScreennailOrPlaceholder:Z

    if-eqz v1, :cond_2

    :cond_0
    const/4 p1, 0x1

    .line 1642
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->setWantPictureCenterCallbacks(Z)V

    .line 1643
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1644
    if-eqz p1, :cond_1

    .line 1645
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1646
    :cond_1
    return-void

    :cond_2
    move p1, v0

    .line 1641
    goto :goto_0
.end method

.method public onPlayerStateChanged()V
    .locals 2

    .prologue
    .line 5788
    sget-object v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TAG:Ljava/lang/String;

    const-string v1, "onPlayerStateChanged()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5789
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x6e

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 5790
    return-void
.end method

.method public onPrepareOptionMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 3464
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 3465
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x6c

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 3467
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    if-nez v1, :cond_2

    .line 3486
    :cond_1
    :goto_0
    return-void

    .line 3470
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 3471
    .local v0, "current":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_1

    .line 3474
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v1

    if-nez v1, :cond_4

    .line 3476
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 3477
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 3478
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 3480
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 3481
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFromSetupWidzard:Z

    if-nez v1, :cond_5

    .line 3482
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateChangePlayerIcon()V

    .line 3484
    :cond_5
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateShowHiddenIconVisibility()V

    .line 3485
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateViewModeIcons(Landroid/content/res/Configuration;)V

    goto :goto_0
.end method

.method public onRefresh()V
    .locals 1

    .prologue
    .line 5245
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v0, :cond_0

    .line 5246
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->updateAllItem()V

    .line 5247
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 2607
    const-string v6, "Gallery_Performance"

    const-string v9, "DetailViewState onResume Start"

    invoke-static {v6, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2608
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->onResume()V

    .line 2609
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplication()Landroid/app/Application;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->isFestivalMode()Z

    move-result v6

    if-nez v6, :cond_0

    .line 2610
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-static {v9}, Lcom/sec/android/gallery3d/app/FilterUtils;->toClusterType(Lcom/sec/samsung/gallery/core/TabTagType;)I

    move-result v9

    invoke-interface {v6, v9}, Lcom/sec/android/gallery3d/app/GalleryApp;->setCurrentClusterType(I)V

    .line 2612
    :cond_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v6, v7}, Lcom/sec/android/gallery3d/app/GalleryApp;->setIsPhotoPage(Z)V

    .line 2614
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    if-nez v6, :cond_1

    .line 2615
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v6

    invoke-virtual {v6, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 2833
    :goto_0
    return-void

    .line 2618
    :cond_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    .line 2619
    .local v3, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v3, :cond_2

    instance-of v6, v3, Lcom/sec/android/gallery3d/data/UriImage;

    if-eqz v6, :cond_2

    move-object v6, v3

    .line 2620
    check-cast v6, Lcom/sec/android/gallery3d/data/UriImage;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/UriImage;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    .line 2621
    .local v5, "uri":Landroid/net/Uri;
    if-eqz v5, :cond_2

    const-string v6, "file"

    invoke-virtual {v5}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    check-cast v3, Lcom/sec/android/gallery3d/data/UriImage;

    .end local v3    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/UriImage;->getFilePath()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_2

    .line 2624
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v6

    invoke-virtual {v6, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    goto :goto_0

    .line 2628
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v6

    invoke-interface {v6}, Lcom/sec/android/gallery3d/ui/GLRoot;->freeze()V

    .line 2629
    iput-boolean v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsActive:Z

    .line 2630
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setContentPane(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 2633
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v6

    invoke-virtual {v6, v8}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 2636
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->resume()V

    .line 2637
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/PhotoView;->resume()V

    .line 2638
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v6, :cond_3

    .line 2639
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/FilmStripView;->resume()V

    .line 2641
    :cond_3
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    if-eqz v6, :cond_4

    .line 2642
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AlbumReloader;->resume()V

    .line 2645
    :cond_4
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v9, "SLIDESHOW_SETTINGS"

    invoke-virtual {v6, v9}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 2646
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowSlideshowsettingsMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v6, v9}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 2648
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2649
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v6, v7}, Lcom/sec/android/gallery3d/app/GalleryApp;->setSlideShowMode(Z)V

    .line 2651
    :cond_5
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBar:Lcom/sec/android/gallery3d/app/GalleryActionBar;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMenuVisibilityListener:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$MyMenuVisibilityListener;

    invoke-virtual {v6, v9}, Lcom/sec/android/gallery3d/app/GalleryActionBar;->addOnMenuVisibilityListener(Landroid/app/ActionBar$OnMenuVisibilityListener;)V

    .line 2652
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowSpinner:Z

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/PhotoView;->getFilmMode()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2653
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBar:Lcom/sec/android/gallery3d/app/GalleryActionBar;

    invoke-virtual {v6, v8, p0}, Lcom/sec/android/gallery3d/app/GalleryActionBar;->enableAlbumModeMenu(ILcom/sec/android/gallery3d/app/GalleryActionBar$OnAlbumModeSelectedListener;)V

    .line 2657
    :cond_6
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v9, "image/*"

    invoke-static {v6, v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEditorAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    iget-boolean v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHaveImageEditor:Z

    if-eq v6, v9, :cond_7

    .line 2658
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHaveImageEditor:Z

    if-nez v6, :cond_21

    move v6, v7

    :goto_1
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHaveImageEditor:Z

    .line 2659
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateMenuOperations()V

    .line 2661
    :cond_7
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2662
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v6

    invoke-virtual {v6, v10}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->isSelectionMode(Ljava/lang/String;)V

    .line 2665
    :cond_8
    iput-boolean v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBars:Z

    .line 2666
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->initActionBar()V

    .line 2667
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onResume()V

    .line 2669
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    if-eqz v6, :cond_a

    .line 2670
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v6

    if-lez v6, :cond_a

    .line 2671
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;

    if-eqz v6, :cond_9

    .line 2672
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->cancel(Z)Z

    .line 2674
    :cond_9
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mTempUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;

    monitor-enter v9

    .line 2675
    :try_start_0
    new-instance v6, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;

    const/4 v10, 0x0

    invoke-direct {v6, p0, v10}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;)V

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;

    .line 2676
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Void;

    invoke-virtual {v6, v10}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2677
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2681
    :cond_a
    iput-boolean v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsStartPhotoNote:Z

    .line 2683
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    if-eqz v6, :cond_b

    .line 2684
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    const/16 v9, 0x8

    invoke-virtual {v6, v9}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->setVisibility(I)V

    .line 2687
    :cond_b
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v6, :cond_c

    .line 2688
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setMultiWindow(Landroid/content/Context;)V

    .line 2690
    :cond_c
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->cloudSync()V

    .line 2691
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDelayedDnieSetting:Z

    if-eqz v6, :cond_22

    move v6, v7

    :goto_2
    invoke-direct {p0, v7, v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->enableDnieMode(ZZ)V

    .line 2693
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/PhotoView;->isCurFaceExistName()Z

    move-result v6

    if-nez v6, :cond_d

    .line 2694
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFaceTagFeature:Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;

    invoke-interface {v6}, Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;->dismissAlertDialog()V

    .line 2697
    :cond_d
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDrmUtil:Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDrmPopupListener:Lcom/sec/android/gallery3d/util/ImageDRMUtil$DrmPopupListener;

    invoke-virtual {v6, v9}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->setDrmPopupListener(Lcom/sec/android/gallery3d/util/ImageDRMUtil$DrmPopupListener;)V

    .line 2698
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v6, :cond_e

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    if-eqz v6, :cond_e

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getMediaItemCount()I

    move-result v6

    const/4 v9, 0x2

    if-lt v6, v9, :cond_e

    .line 2699
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    invoke-virtual {v6, v9}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    .line 2702
    :cond_e
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHoverGestureDetector:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    if-eqz v6, :cond_f

    .line 2703
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHoverGestureDetector:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlHoverGestureListener:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$SimpleOnGestureListener;

    invoke-virtual {v6, v9}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->setListener(Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector$OnListScrollListner;)V

    .line 2707
    :cond_f
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v6, :cond_10

    .line 2708
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v6, :cond_10

    .line 2709
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v6, v9}, Lcom/sec/android/gallery3d/ui/PhotoView;->setCurrentPhoto(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 2712
    :cond_10
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMpo3DDisplay:Z

    if-eqz v6, :cond_11

    .line 2717
    new-instance v6, Lcom/sec/android/gallery3d/ui/Hdmi3D;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v6, v9}, Lcom/sec/android/gallery3d/ui/Hdmi3D;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHdmi3D:Lcom/sec/android/gallery3d/ui/Hdmi3D;

    .line 2718
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHdmi3D:Lcom/sec/android/gallery3d/ui/Hdmi3D;

    invoke-virtual {v6, v9}, Lcom/sec/android/gallery3d/ui/PhotoView;->setHdmi3D(Lcom/sec/android/gallery3d/ui/Hdmi3D;)V

    .line 2720
    :cond_11
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    if-eqz v6, :cond_23

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isCoverMode()Z

    move-result v6

    if-eqz v6, :cond_23

    move v0, v7

    .line 2721
    .local v0, "bCoverMode":Z
    :goto_3
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseContextualAwareness:Z

    if-eqz v6, :cond_12

    if-nez v0, :cond_12

    .line 2722
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v6, :cond_12

    .line 2723
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v10}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getActionBarView()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->getActionBar()Landroid/app/ActionBar;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/ActionBar;->isShowing()Z

    move-result v10

    invoke-virtual {v6, v9, v10}, Lcom/sec/android/gallery3d/ui/PhotoView;->setCurrentPhotoForCA(Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    .line 2727
    :cond_12
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    if-eqz v6, :cond_14

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->isEnabled()Z

    move-result v6

    if-eqz v6, :cond_14

    .line 2728
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->isVisiable()Z

    move-result v6

    if-nez v6, :cond_13

    .line 2729
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->setVisible(Z)V

    .line 2731
    :cond_13
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->refresh()V

    .line 2734
    :cond_14
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHasCameraScreennailOrPlaceholder:Z

    if-eqz v6, :cond_16

    .line 2735
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v6, v6, Lcom/sec/android/gallery3d/data/CameraShortcutImage;

    if-eqz v6, :cond_16

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    if-eqz v6, :cond_16

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getMediaItemCount()I

    move-result v6

    if-le v6, v7, :cond_16

    .line 2736
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v6, v8}, Lcom/sec/android/gallery3d/ui/PhotoView;->setWantPictureCenterCallbacks(Z)V

    .line 2737
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v6, v7}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    .line 2738
    .local v4, "photo":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v4, :cond_15

    .line 2739
    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateCurrentPhoto(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 2740
    :cond_15
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->switchToImage(I)V

    .line 2744
    .end local v4    # "photo":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_16
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    if-eqz v6, :cond_17

    .line 2745
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->resume()V

    .line 2747
    :cond_17
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    .line 2748
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    if-nez v6, :cond_18

    if-nez v0, :cond_18

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCameraQuickViewOnLockscreen(Landroid/app/Activity;)Z

    move-result v6

    if-eqz v6, :cond_24

    .line 2749
    :cond_18
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v6, v8}, Lcom/sec/android/gallery3d/ui/PhotoView;->setFaceTagToggle(Z)V

    .line 2754
    :goto_4
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    if-eqz v6, :cond_19

    .line 2755
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v6, v9}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setPhotoView(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    .line 2756
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    instance-of v6, v6, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-eqz v6, :cond_25

    .line 2757
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    check-cast v6, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v8, v6}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setPhotoDataAdapter(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)V

    .line 2758
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setMultiViewState(Z)V

    .line 2768
    :cond_19
    :goto_5
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v8, "PHOTO_PAGE"

    invoke-virtual {v6, v8}, Lcom/sec/samsung/gallery/core/GalleryFacade;->hasMediator(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1a

    .line 2769
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoPageViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v6, v8}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 2782
    :cond_1a
    new-instance v6, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v6, v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDetailViewBeam:Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;

    .line 2783
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDetailViewBeam:Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->setBeamListener()V

    .line 2784
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDetailViewBeam:Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->registerSettingObserver()V

    .line 2786
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    if-eqz v6, :cond_1b

    .line 2787
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->dismissProgressDialog()V

    .line 2789
    :cond_1b
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v6

    if-nez v6, :cond_1c

    .line 2790
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setManualFaceTag()V

    .line 2792
    :cond_1c
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateLastSharedIconAndTitle()V

    .line 2793
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v6, :cond_1d

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v6, v6, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v6, :cond_1d

    .line 2794
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v2

    .line 2795
    .local v2, "filePath":Ljava/lang/String;
    if-eqz v2, :cond_1d

    .line 2796
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2797
    .local v1, "file":Ljava/io/File;
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v6, :cond_1d

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_1d

    .line 2798
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->setBroken(Z)V

    .line 2802
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "filePath":Ljava/lang/String;
    :cond_1d
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    if-eqz v6, :cond_1e

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v6, v6, Lcom/sec/android/gallery3d/data/UriImage;

    if-eqz v6, :cond_1e

    .line 2804
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getMediaItemCount()I

    move-result v6

    const/4 v8, -0x1

    if-ne v6, v8, :cond_1e

    .line 2805
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Landroid/app/Activity;->finish()V

    .line 2809
    :cond_1e
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    if-eqz v6, :cond_1f

    .line 2810
    const/16 v6, 0xdac

    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->refreshHidingMessage(I)V

    .line 2813
    :cond_1f
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHdmiListener:Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/HdmiUtils;->addHdmiConnectListener(Lcom/sec/android/gallery3d/util/HdmiUtils$OnHdmiConnectListener;)V

    .line 2815
    invoke-direct {p0, v7}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setCheckSideSyncConnection(Z)V

    .line 2816
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEmergencyMode(Landroid/app/Activity;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsEmergencyMode:Z

    .line 2821
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isAvailableEAM()Z

    move-result v6

    if-eqz v6, :cond_20

    .line 2822
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v6

    if-eqz v6, :cond_26

    .line 2823
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mWindowHasFocus:Z

    if-eqz v6, :cond_20

    .line 2824
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->registerEAM(Landroid/content/BroadcastReceiver;)V

    .line 2829
    :cond_20
    :goto_6
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v6

    invoke-interface {v6}, Lcom/sec/android/gallery3d/ui/GLRoot;->unfreeze()V

    .line 2830
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mStateManager:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-static {v6, v7, p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->checkViewStateByFilterBySetting(Landroid/content/Context;Lcom/sec/android/gallery3d/app/StateManager;Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 2831
    const-string v6, "Gallery_Performance"

    const-string v7, "DetailViewState onResume End"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .end local v0    # "bCoverMode":Z
    :cond_21
    move v6, v8

    .line 2658
    goto/16 :goto_1

    .line 2677
    :catchall_0
    move-exception v6

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6

    :cond_22
    move v6, v8

    .line 2691
    goto/16 :goto_2

    :cond_23
    move v0, v8

    .line 2720
    goto/16 :goto_3

    .line 2751
    .restart local v0    # "bCoverMode":Z
    :cond_24
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFaceTagAvailable(Landroid/content/Context;)Z

    move-result v9

    invoke-virtual {v6, v9}, Lcom/sec/android/gallery3d/ui/PhotoView;->setFaceTagToggle(Z)V

    goto/16 :goto_4

    .line 2760
    :cond_25
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v6, v8}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setMultiViewState(Z)V

    goto/16 :goto_5

    .line 2826
    :cond_26
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->registerEAM(Landroid/content/BroadcastReceiver;)V

    goto :goto_6
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 5337
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v0, :cond_0

    .line 5338
    const-string v0, "KEY_MEDIA_ITEM_PATH"

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 5339
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v0, :cond_1

    .line 5340
    const-string v0, "face_id"

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->getCurFaceId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 5341
    :cond_1
    return-void
.end method

.method protected onSaveState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 5346
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v0, :cond_0

    .line 5347
    const-string v0, "KEY_MEDIA_ITEM_PATH"

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 5348
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v0, :cond_1

    .line 5349
    const-string v0, "face_id"

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->getCurFaceId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 5350
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onSaveState(Landroid/os/Bundle;)V

    .line 5351
    return-void
.end method

.method public onScaleMin()Z
    .locals 9

    .prologue
    const/4 v2, 0x2

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 5489
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mBackByZoomOut:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    if-eqz v0, :cond_1

    .line 5510
    :cond_0
    :goto_0
    return v1

    .line 5492
    :cond_1
    const/4 v6, 0x0

    .line 5493
    .local v6, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPreviousViewState:Lcom/sec/android/gallery3d/app/ActivityState;

    instance-of v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-eqz v0, :cond_2

    .line 5495
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOriginalSetPathString:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v7

    .line 5496
    .local v7, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v7, :cond_2

    .line 5497
    iget v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I

    invoke-virtual {v7, v0, v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    check-cast v6, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 5500
    .end local v7    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .restart local v6    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_2
    if-nez v6, :cond_3

    .line 5501
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v6

    .line 5503
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPreviousViewState:Lcom/sec/android/gallery3d/app/ActivityState;

    instance-of v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-eqz v0, :cond_5

    .line 5504
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPreviousViewState:Lcom/sec/android/gallery3d/app/ActivityState;

    check-cast v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-nez v6, :cond_4

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mInitialItem:Lcom/sec/android/gallery3d/data/MediaItem;

    :goto_1
    iget v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentMediaSetIndex:I

    iget v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->setPromptRequest(IILjava/lang/Object;II)V

    .line 5508
    .end local v6    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :goto_2
    invoke-virtual {p0, v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->doCommitDeleteImage(Z)V

    .line 5509
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->finishDetailView()V

    move v1, v8

    .line 5510
    goto :goto_0

    .restart local v6    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_4
    move-object v3, v6

    .line 5504
    goto :goto_1

    .line 5506
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPreviousViewState:Lcom/sec/android/gallery3d/app/ActivityState;

    if-nez v6, :cond_6

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mInitialItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .end local v6    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_6
    invoke-virtual {v0, v1, v2, v6}, Lcom/sec/android/gallery3d/app/ActivityState;->setPromptRequest(IILjava/lang/Object;)V

    goto :goto_2
.end method

.method public onShareTargetSelected(Landroid/widget/ShareActionProvider;Landroid/content/Intent;)Z
    .locals 7
    .param p1, "source"    # Landroid/widget/ShareActionProvider;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2968
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v2

    .line 2969
    .local v2, "timestampMillis":J
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->getMediaTypeString(Lcom/sec/android/gallery3d/data/MediaItem;)Ljava/lang/String;

    move-result-object v0

    .line 2970
    .local v0, "mediaType":Ljava/lang/String;
    const-string v1, "Gallery"

    const-string v6, "Share"

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v2

    :goto_0
    invoke-static {v1, v6, v0, v4, v5}, Lcom/sec/android/gallery3d/util/UsageStatistics;->onEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 2976
    const/4 v1, 0x0

    return v1

    .line 2970
    :cond_0
    const-wide/16 v4, -0x1

    goto :goto_0
.end method

.method public onSingleTapUp(II)V
    .locals 13
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 2087
    const/4 v5, 0x0

    .line 2088
    .local v5, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    if-eqz v0, :cond_0

    .line 2089
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    .line 2090
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v3

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v4

    move v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->checkPlayButton(IIIILcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2091
    const-wide/16 v0, 0x400

    invoke-virtual {v5, v0, v1}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2092
    const/4 v0, 0x1

    invoke-direct {p0, v5, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->handle3DTour(Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    .line 2156
    :cond_1
    :goto_0
    return-void

    .line 2094
    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, v5, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->handlePlayVideo(Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    goto :goto_0

    .line 2096
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->checkPlaySoundScene(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2097
    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->handlePlaySoundScene(Lcom/sec/android/gallery3d/data/MediaItem;)V

    goto :goto_0

    .line 2098
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isBurstShotPlayIconPressed()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2099
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 2100
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->setBurstMode(Z)V

    .line 2102
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->addItemsToSlideshowProxy(Z)V

    goto :goto_0

    .line 2103
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isBurstShotSettingIconPressed()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2104
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 2106
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->handleBurstPlaySetting()V

    goto :goto_0

    .line 2107
    :cond_6
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isPanoramaIconPressed()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2108
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->panoramaPlayBoost(Landroid/content/Context;)V

    .line 2109
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->hideBars()V

    .line 2110
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v9

    .line 2111
    .local v9, "posController":Lcom/sec/android/gallery3d/ui/PositionController;
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageWidth()I

    move-result v0

    int-to-float v7, v0

    .line 2112
    .local v7, "imageW":F
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageHeight()I

    move-result v0

    int-to-float v6, v0

    .line 2113
    .local v6, "imageH":F
    cmpl-float v0, v6, v7

    if-lez v0, :cond_8

    .line 2114
    const/4 v0, 0x0

    cmpl-float v0, v7, v0

    if-lez v0, :cond_7

    div-float v0, v6, v7

    const/high16 v1, 0x3f800000    # 1.0f

    mul-float v10, v0, v1

    .line 2115
    .local v10, "ratio":F
    :goto_1
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/PositionController;->fitViewToImageTop()V

    .line 2116
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageScale()F

    move-result v11

    .line 2117
    .local v11, "scale":F
    mul-float v0, v6, v11

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    float-to-int v12, v0

    .line 2118
    .local v12, "slideAmount":I
    const/4 v0, 0x0

    neg-int v1, v12

    const/16 v2, 0xc

    invoke-virtual {v9, v0, v1, v2, v10}, Lcom/sec/android/gallery3d/ui/PositionController;->panoramaScroll(IIIF)V

    goto/16 :goto_0

    .line 2114
    .end local v10    # "ratio":F
    .end local v11    # "scale":F
    .end local v12    # "slideAmount":I
    :cond_7
    const v10, 0x40b9999a    # 5.8f

    goto :goto_1

    .line 2120
    :cond_8
    const/4 v0, 0x0

    cmpl-float v0, v6, v0

    if-lez v0, :cond_9

    div-float v0, v7, v6

    const/high16 v1, 0x3f800000    # 1.0f

    mul-float v10, v0, v1

    .line 2121
    .restart local v10    # "ratio":F
    :goto_2
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/PositionController;->fitViewToImageLeft()V

    .line 2122
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/PositionController;->getImageScale()F

    move-result v11

    .line 2123
    .restart local v11    # "scale":F
    mul-float v0, v7, v11

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    float-to-int v12, v0

    .line 2124
    .restart local v12    # "slideAmount":I
    neg-int v0, v12

    const/4 v1, 0x0

    const/16 v2, 0xc

    invoke-virtual {v9, v0, v1, v2, v10}, Lcom/sec/android/gallery3d/ui/PositionController;->panoramaScroll(IIIF)V

    goto/16 :goto_0

    .line 2120
    .end local v10    # "ratio":F
    .end local v11    # "scale":F
    .end local v12    # "slideAmount":I
    :cond_9
    const v10, 0x40b9999a    # 5.8f

    goto :goto_2

    .line 2126
    .end local v6    # "imageH":F
    .end local v7    # "imageW":F
    .end local v9    # "posController":Lcom/sec/android/gallery3d/ui/PositionController;
    :cond_a
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->is3DPanoramaIconPressed()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2127
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0e02eb

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 2130
    :cond_b
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0, v5}, Lcom/sec/samsung/gallery/util/Panorama3DUtil;->launch3DPanorama(Landroid/app/Activity;Lcom/sec/android/gallery3d/data/MediaItem;)V

    goto/16 :goto_0

    .line 2132
    :cond_c
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isOutOfFocusIconPressed()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2134
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.ofviewer"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->checkPackageExist(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2135
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v8

    .line 2136
    .local v8, "mAudioManager":Landroid/media/AudioManager;
    if-eqz v8, :cond_d

    .line 2137
    const/16 v0, 0xa

    invoke-virtual {v8, v0}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 2139
    .end local v8    # "mAudioManager":Landroid/media/AudioManager;
    :cond_d
    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->handleOutOfFocusImage(Lcom/sec/android/gallery3d/data/MediaItem;)V

    goto/16 :goto_0

    .line 2140
    :cond_e
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isMagicShotIconPressed()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 2141
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMagicShotIconEnter:Z

    if-eqz v0, :cond_1

    .line 2142
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v8

    .line 2143
    .restart local v8    # "mAudioManager":Landroid/media/AudioManager;
    if-eqz v8, :cond_f

    .line 2144
    const/16 v0, 0xa

    invoke-virtual {v8, v0}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 2145
    :cond_f
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->startMagicShotStudio()V

    goto/16 :goto_0

    .line 2147
    .end local v8    # "mAudioManager":Landroid/media/AudioManager;
    :cond_10
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->isSequenceIconPressed()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 2148
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v8

    .line 2149
    .restart local v8    # "mAudioManager":Landroid/media/AudioManager;
    if-eqz v8, :cond_11

    .line 2150
    const/16 v0, 0xa

    invoke-virtual {v8, v0}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 2151
    :cond_11
    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->handleSequenceCmd(Lcom/sec/android/gallery3d/data/MediaItem;)V

    goto/16 :goto_0

    .line 2153
    .end local v8    # "mAudioManager":Landroid/media/AudioManager;
    :cond_12
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->onUserInteractionTap()V

    goto/16 :goto_0
.end method

.method public onSizeChanged()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 5682
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 5683
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    if-lez v2, :cond_1

    .line 5684
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;

    if-eqz v2, :cond_0

    .line 5685
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->cancel(Z)Z

    .line 5687
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mTempUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;

    monitor-enter v3

    .line 5688
    :try_start_0
    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;

    const/4 v4, 0x0

    invoke-direct {v2, p0, v4}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;

    .line 5689
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v4}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$UpdateSelectionModeTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 5690
    monitor-exit v3

    .line 5703
    :cond_1
    :goto_0
    return-void

    .line 5690
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 5693
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-eqz v2, :cond_1

    .line 5694
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 5695
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_1

    .line 5696
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v1

    .line 5697
    .local v1, "path":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 5698
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->dismissDialogs()V

    goto :goto_0
.end method

.method public onStateResult(IILandroid/content/Intent;)V
    .locals 26
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 2268
    sparse-switch p1, :sswitch_data_0

    .line 2447
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 2272
    :sswitch_1
    if-eqz p3, :cond_0

    const/16 v22, -0x1

    move/from16 v0, p2

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    .line 2273
    const/16 v18, 0x0

    .line 2274
    .local v18, "path":Lcom/sec/android/gallery3d/data/Path;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEventViewMode(Landroid/content/Context;)Z

    move-result v22

    if-eqz v22, :cond_1

    .line 2275
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/EventAlbumManager;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOriginalSetPathString:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    move-object/from16 v3, p3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->handleCropEvent(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v18

    .line 2279
    :goto_1
    if-eqz v18, :cond_0

    .line 2280
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I

    move/from16 v23, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    move/from16 v2, v23

    invoke-interface {v0, v1, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->setCurrentPhoto(Lcom/sec/android/gallery3d/data/Path;I)V

    goto :goto_0

    .line 2277
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v22

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v23

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v22 .. v24}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v18

    goto :goto_1

    .line 2284
    .end local v18    # "path":Lcom/sec/android/gallery3d/data/Path;
    :sswitch_2
    const/16 v22, -0x1

    move/from16 v0, p2

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 2295
    :sswitch_3
    const/4 v15, 0x1

    .line 2296
    .local v15, "mCheckUser":Z
    sget-boolean v22, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v22, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v22, v0

    if-eqz v22, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 2297
    const/4 v15, 0x0

    .line 2299
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFaceTagFeature:Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;

    move-object/from16 v23, v0

    if-eqz p3, :cond_4

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v22

    :goto_2
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-interface {v0, v1, v15}, Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;->assignName(Landroid/net/Uri;Z)V

    .line 2301
    if-eqz p3, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v22, v0

    if-eqz v22, :cond_3

    .line 2302
    const-string v22, "external"

    invoke-static/range {v22 .. v22}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 2303
    .local v4, "FILES_URI":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-static {v4, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 2305
    .local v6, "baseUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v22, v0

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getCategoryType(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 2306
    .local v19, "types":Ljava/lang/String;
    if-eqz v19, :cond_3

    const-string v22, "People"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_3

    .line 2307
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2308
    .local v5, "addTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v22, "People"

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2310
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v22, v0

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-static {v0, v1, v5}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->addCategoryType(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 2311
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->requestUpdateScreenByCategoryChange(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 2317
    .end local v4    # "FILES_URI":Landroid/net/Uri;
    .end local v5    # "addTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v6    # "baseUri":Landroid/net/Uri;
    .end local v19    # "types":Ljava/lang/String;
    :cond_3
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v22, v0

    if-eqz v22, :cond_0

    .line 2318
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/gallery3d/ui/PhotoView;->resumeFaceIndicatorView()V

    goto/16 :goto_0

    .line 2299
    :cond_4
    const/16 v22, 0x0

    goto/16 :goto_2

    .line 2313
    .restart local v4    # "FILES_URI":Landroid/net/Uri;
    .restart local v5    # "addTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v6    # "baseUri":Landroid/net/Uri;
    .restart local v19    # "types":Ljava/lang/String;
    :cond_5
    sget-object v22, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TAG:Ljava/lang/String;

    const-string v23, "Error during add category"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 2321
    .end local v4    # "FILES_URI":Landroid/net/Uri;
    .end local v5    # "addTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v6    # "baseUri":Landroid/net/Uri;
    .end local v15    # "mCheckUser":Z
    .end local v19    # "types":Ljava/lang/String;
    :sswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v22, v0

    if-eqz v22, :cond_0

    .line 2322
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v23, v0

    const/16 v22, -0x1

    move/from16 v0, p2

    move/from16 v1, v22

    if-ne v0, v1, :cond_6

    const/16 v22, 0x1

    :goto_4
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->nameChanged(Z)V

    goto/16 :goto_0

    :cond_6
    const/16 v22, 0x0

    goto :goto_4

    .line 2326
    :sswitch_5
    const/16 v22, -0x1

    move/from16 v0, p2

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    if-eqz p3, :cond_0

    .line 2327
    const-string v22, "data"

    move-object/from16 v0, p3

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Landroid/graphics/Bitmap;

    .line 2328
    .local v7, "bmp":Landroid/graphics/Bitmap;
    const-string v22, "lookupKey"

    move-object/from16 v0, p3

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 2329
    .local v14, "lookUpKeyFromCrop":Ljava/lang/String;
    if-eqz v14, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v22, v0

    if-eqz v22, :cond_7

    .line 2330
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Lcom/sec/android/gallery3d/ui/PhotoView;->setCurContactLookupKey(Ljava/lang/String;)V

    .line 2331
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFaceTagFeature:Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-interface {v0, v7}, Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;->setAsCallerId(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 2335
    .end local v7    # "bmp":Landroid/graphics/Bitmap;
    .end local v14    # "lookUpKeyFromCrop":Ljava/lang/String;
    :sswitch_6
    const/16 v22, -0x1

    move/from16 v0, p2

    move/from16 v1, v22

    if-ne v0, v1, :cond_8

    .line 2336
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->handleResultCropManualFD(Landroid/content/Intent;)V

    .line 2340
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v22, v0

    if-eqz v22, :cond_0

    .line 2341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/gallery3d/ui/PhotoView;->resumeFaceIndicatorView()V

    goto/16 :goto_0

    .line 2338
    :cond_8
    const/16 v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsLongPressFinished:Z

    goto :goto_5

    .line 2346
    :sswitch_7
    if-nez p2, :cond_0

    if-eqz p3, :cond_0

    .line 2347
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCR;->showOCRFailedDialog(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 2352
    :sswitch_8
    move-object/from16 v0, p0

    move/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->handleResultGetText(ILandroid/content/Intent;)V

    goto/16 :goto_0

    .line 2360
    :sswitch_9
    const/16 v22, -0x1

    move/from16 v0, p2

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    .line 2361
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v22

    const-string v23, "lat"

    invoke-virtual/range {v22 .. v23}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    .line 2362
    .local v10, "lat":Ljava/lang/Double;
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v22

    const-string v23, "log"

    invoke-virtual/range {v22 .. v23}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v13

    .line 2363
    .local v13, "log":Ljava/lang/Double;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    move-object/from16 v22, v0

    if-eqz v22, :cond_0

    .line 2364
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMoreInfo:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v10, v13}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo;->setLatLng(Ljava/lang/Double;Ljava/lang/Double;)V

    goto/16 :goto_0

    .line 2370
    .end local v10    # "lat":Ljava/lang/Double;
    .end local v13    # "log":Ljava/lang/Double;
    :sswitch_a
    sget-object v22, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TAG:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "SLink Download Complete "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2371
    const/16 v22, -0x1

    move/from16 v0, p2

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    .line 2375
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getLocalUrisFromSuccessfulModalDownloadResult(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v11

    .line 2378
    .local v11, "list":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    const/16 v16, 0x0

    .line 2379
    .local v16, "mediaStoreUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    instance-of v0, v11, Ljava/util/ArrayList;

    move/from16 v22, v0

    if-eqz v22, :cond_9

    move-object/from16 v16, v11

    .line 2380
    check-cast v16, Ljava/util/ArrayList;

    .line 2383
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->pollItemAfterModalDownload()I

    move-result v9

    .line 2384
    .local v9, "itemId":I
    if-nez v9, :cond_a

    .line 2385
    sget-object v22, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TAG:Ljava/lang/String;

    const-string v23, "no action after modal download"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2389
    :cond_a
    sparse-switch v9, :sswitch_data_1

    goto/16 :goto_0

    .line 2391
    :sswitch_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->startChooser(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/List;)V

    goto/16 :goto_0

    .line 2394
    :sswitch_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v22, v0

    const-string v23, "image/*"

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->launchLastShareApp(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2397
    :sswitch_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v24, v0

    const/16 v22, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/net/Uri;

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getFilePathFrom(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->copyToClipboard(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2401
    :sswitch_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMimeType:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->imageEdit(Ljava/util/ArrayList;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2404
    :sswitch_f
    const-string v22, "deviceId"

    const-wide/16 v24, -0x1

    move-object/from16 v0, p3

    move-object/from16 v1, v22

    move-wide/from16 v2, v24

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v20

    .line 2405
    .local v20, "selectedDeviceId":J
    const-wide/16 v22, -0x1

    cmp-long v22, v20, v22

    if-eqz v22, :cond_0

    .line 2408
    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v8, v0, [J

    .line 2409
    .local v8, "ids":[J
    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v22, v0

    check-cast v22, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->getSlinkObjectId()J

    move-result-wide v24

    aput-wide v24, v8, v23

    .line 2411
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v20

    invoke-static {v0, v8, v1, v2}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->transferFiles(Landroid/content/Context;[JJ)V

    goto/16 :goto_0

    .line 2414
    .end local v8    # "ids":[J
    .end local v20    # "selectedDeviceId":J
    :sswitch_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-object/from16 v22, v0

    const/16 v23, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v23

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showSetAsDialog(ZLjava/util/ArrayList;)V

    goto/16 :goto_0

    .line 2417
    :sswitch_11
    const/16 v22, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/net/Uri;

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->handleCropSamsungLink(Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2420
    :sswitch_12
    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v23, v0

    aput-object v23, v17, v22

    const/16 v22, 0x1

    aput-object v16, v17, v22

    .line 2423
    .local v17, "params":[Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-object/from16 v22, v0

    const-string v23, "S_STUDIO"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 2431
    .end local v9    # "itemId":I
    .end local v11    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    .end local v16    # "mediaStoreUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .end local v17    # "params":[Ljava/lang/Object;
    :sswitch_13
    if-nez p3, :cond_b

    .line 2432
    const-string v22, "SlinkCrop"

    const-string v23, "DetailViewState : data is null"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2435
    :cond_b
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v12

    .line 2436
    .local v12, "localUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOriginalSetPathString:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    const/16 v25, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v0, v1, v12, v2, v3}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->uploadFiles(Ljava/lang/String;Landroid/net/Uri;ZZ)Z

    goto/16 :goto_0

    .line 2440
    .end local v12    # "localUri":Landroid/net/Uri;
    :sswitch_14
    const/16 v22, 0x3

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v23, v0

    aput-object v23, v17, v22

    const/16 v22, 0x1

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v17, v22

    const/16 v22, 0x2

    aput-object p3, v17, v22

    .line 2443
    .restart local v17    # "params":[Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-object/from16 v22, v0

    const-string v23, "ADD_SLIDESHOW_MUSIC"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 2268
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_14
        0x301 -> :sswitch_1
        0x302 -> :sswitch_2
        0x303 -> :sswitch_0
        0x304 -> :sswitch_3
        0x305 -> :sswitch_5
        0x306 -> :sswitch_6
        0x307 -> :sswitch_7
        0x308 -> :sswitch_8
        0x309 -> :sswitch_4
        0x30a -> :sswitch_9
        0x30b -> :sswitch_a
        0x30c -> :sswitch_13
    .end sparse-switch

    .line 2389
    :sswitch_data_1
    .sparse-switch
        0x7f0f026c -> :sswitch_b
        0x7f0f026d -> :sswitch_c
        0x7f0f0283 -> :sswitch_12
        0x7f0f029a -> :sswitch_10
        0x7f0f029c -> :sswitch_e
        0x7f0f02a7 -> :sswitch_d
        0x7f0f02a8 -> :sswitch_11
        0x7f0f02ad -> :sswitch_f
    .end sparse-switch
.end method

.method public onUndoBarVisibilityChanged(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 2964
    return-void
.end method

.method public onUndoDeleteImage()V
    .locals 2

    .prologue
    .line 2194
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeletePath:Lcom/sec/android/gallery3d/data/Path;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 2208
    :goto_0
    return-void

    .line 2197
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2198
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->onUndoDeleteMultipleImages()V

    goto :goto_0

    .line 2204
    :cond_1
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteIsFocus:Z

    if-eqz v0, :cond_2

    .line 2205
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeletePath:Lcom/sec/android/gallery3d/data/Path;

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->setFocusHintPath(Lcom/sec/android/gallery3d/data/Path;)V

    .line 2206
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeletePath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->removeDeletion(Lcom/sec/android/gallery3d/data/Path;)V

    .line 2207
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeletePath:Lcom/sec/android/gallery3d/data/Path;

    goto :goto_0
.end method

.method public onUndoDeleteMultipleImages()V
    .locals 3

    .prologue
    .line 5535
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    .line 5544
    :goto_0
    return-void

    .line 5540
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/Path;

    .line 5541
    .local v1, "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->removeDeletion(Lcom/sec/android/gallery3d/data/Path;)V

    goto :goto_1

    .line 5543
    .end local v1    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_1
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDeleteList:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public onUserInteractionTap()V
    .locals 5

    .prologue
    const/16 v4, 0x67

    .line 4042
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->enableShowBar()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4056
    :goto_0
    return-void

    .line 4045
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowSideMirrorStartTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->showSideMirrorView(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4046
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowSideMirrorStartTime:J

    goto :goto_0

    .line 4050
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_2

    .line 4051
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 4052
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    sget v1, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->DOUBLE_TAP_TIMEOUT:I

    int-to-long v2, v1

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 4054
    :cond_2
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->setUserInteractionShowMode()V

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .param p1, "hasFocus"    # Z

    .prologue
    .line 5222
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-eqz v0, :cond_1

    .line 5223
    if-eqz p1, :cond_2

    .line 5224
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mWindowHasFocus:Z

    .line 5225
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getMediaItemCount()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 5226
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    .line 5228
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isAvailableEAM()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5229
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->registerEAM(Landroid/content/BroadcastReceiver;)V

    .line 5241
    :cond_1
    :goto_0
    return-void

    .line 5232
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mWindowHasFocus:Z

    .line 5233
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v0, :cond_3

    .line 5234
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    .line 5236
    :cond_3
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isAvailableEAM()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5237
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->unregisterEAM(Landroid/content/BroadcastReceiver;)V

    goto :goto_0
.end method

.method public removeHidingMessage()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 4011
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4012
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 4013
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xdac

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 4015
    :cond_0
    return-void
.end method

.method protected selectAll()V
    .locals 13

    .prologue
    const/4 v12, -0x1

    .line 4656
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    check-cast v6, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .line 4657
    .local v6, "mediaItemAdapter":Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    const/4 v7, 0x0

    .line 4658
    .local v7, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-boolean v11, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    if-eqz v11, :cond_1

    .line 4659
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSourceMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v7

    .line 4664
    :goto_0
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v9

    .line 4666
    .local v9, "realCount":I
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v11}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 4667
    .local v3, "intent":Landroid/content/Intent;
    const/4 v4, -0x1

    .line 4668
    .local v4, "maxAddCount":I
    const-string v11, "pick-max-item"

    invoke-virtual {v3, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 4669
    .local v5, "maxPickCount":I
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11, v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v2

    .line 4671
    .local v2, "currentSelectedCount":I
    if-eq v5, v12, :cond_3

    .line 4672
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v10

    .line 4673
    .local v10, "totalSelectedCount":I
    sub-int v4, v5, v10

    .line 4674
    sget-boolean v11, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v11, :cond_2

    .line 4675
    if-nez v4, :cond_5

    .line 4676
    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateMaximumSelectionNumberExceeded(I)V

    .line 4722
    .end local v10    # "totalSelectedCount":I
    :cond_0
    :goto_1
    return-void

    .line 4661
    .end local v2    # "currentSelectedCount":I
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "maxAddCount":I
    .end local v5    # "maxPickCount":I
    .end local v9    # "realCount":I
    :cond_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    goto :goto_0

    .line 4680
    .restart local v2    # "currentSelectedCount":I
    .restart local v3    # "intent":Landroid/content/Intent;
    .restart local v4    # "maxAddCount":I
    .restart local v5    # "maxPickCount":I
    .restart local v9    # "realCount":I
    .restart local v10    # "totalSelectedCount":I
    :cond_2
    sub-int v1, v9, v2

    .line 4681
    .local v1, "currentRemainCount":I
    if-le v1, v4, :cond_5

    .line 4682
    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateMaximumSelectionNumberExceeded(I)V

    goto :goto_1

    .line 4686
    .end local v1    # "currentRemainCount":I
    .end local v10    # "totalSelectedCount":I
    :cond_3
    sget-boolean v11, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v11, :cond_4

    iget-boolean v11, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFromGalleryWidget:Z

    if-eqz v11, :cond_4

    .line 4687
    const/16 v5, 0x3e8

    .line 4688
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v10

    .line 4689
    .restart local v10    # "totalSelectedCount":I
    sub-int v4, v5, v10

    .line 4690
    if-nez v4, :cond_5

    .line 4691
    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateMaximumSelectionNumberExceeded(I)V

    goto :goto_1

    .line 4694
    .end local v10    # "totalSelectedCount":I
    :cond_4
    sget-boolean v11, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v11, :cond_5

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v11}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiPickMode(Landroid/content/Context;)Z

    move-result v11

    if-eqz v11, :cond_5

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v11}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFromInsideGallery(Landroid/content/Context;)Z

    move-result v11

    if-nez v11, :cond_5

    .line 4695
    sget v5, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    .line 4696
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v10

    .line 4697
    .restart local v10    # "totalSelectedCount":I
    sub-int v4, v5, v10

    .line 4698
    if-nez v4, :cond_5

    .line 4699
    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateMaximumSelectionNumberExceeded(I)V

    goto :goto_1

    .line 4704
    .end local v10    # "totalSelectedCount":I
    :cond_5
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getCurrentCacheSize()I

    move-result v0

    .line 4705
    .local v0, "count":I
    sget-boolean v11, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v11, :cond_6

    if-eq v5, v12, :cond_6

    .line 4706
    invoke-direct {p0, v7, v9, v5}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->selectLimitedAllProcess(Lcom/sec/android/gallery3d/data/MediaSet;II)V

    .line 4713
    :goto_2
    sget-boolean v11, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v11, :cond_0

    .line 4714
    const-string v11, "pick-max-item"

    invoke-virtual {v3, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 4715
    .local v8, "pickItemCount":I
    if-eq v8, v12, :cond_0

    .line 4716
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v10

    .line 4717
    .restart local v10    # "totalSelectedCount":I
    if-lt v10, v8, :cond_0

    .line 4718
    invoke-direct {p0, v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateMaximumSelectionNumberExceeded(I)V

    goto :goto_1

    .line 4707
    .end local v8    # "pickItemCount":I
    .end local v10    # "totalSelectedCount":I
    :cond_6
    if-ge v0, v9, :cond_7

    .line 4708
    invoke-direct {p0, v7}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->selectAllProcessTask(Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_2

    .line 4710
    :cond_7
    invoke-direct {p0, v7, v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->selectAllProcess(Lcom/sec/android/gallery3d/data/MediaSet;I)V

    goto :goto_2
.end method

.method public setUnnamedFaceCandidates(I[I)V
    .locals 1
    .param p1, "faceId"    # I
    .param p2, "ids"    # [I

    .prologue
    .line 4574
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v0, :cond_0

    .line 4575
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView;->setUnnamedCandidates(I[I)V

    .line 4577
    :cond_0
    return-void
.end method

.method public setUserInteractionShowMode()V
    .locals 2

    .prologue
    .line 4059
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsActive:Z

    if-nez v0, :cond_1

    .line 4070
    :cond_0
    :goto_0
    return-void

    .line 4063
    :cond_1
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBars:Z

    if-eqz v0, :cond_2

    .line 4064
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->hideBars()V

    .line 4065
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 4066
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 4068
    :cond_2
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->showBarsForAShortTime()V

    goto :goto_0
.end method

.method public declared-synchronized showSideMirrorView(Z)Z
    .locals 5
    .param p1, "show"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 5386
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    if-eqz v2, :cond_5

    .line 5387
    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->isShowing()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    if-nez p1, :cond_2

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->isShowing()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_2

    .line 5432
    :cond_1
    :goto_0
    monitor-exit p0

    return v0

    .line 5391
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->setIsShowing(Z)V

    .line 5392
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorAnim:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAnimation(Lcom/sec/android/gallery3d/anim/Animation;)V

    .line 5393
    if-eqz p1, :cond_4

    .line 5394
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->getWidth()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    const/4 v3, 0x0

    const/16 v4, 0x12c

    invoke-direct {v0, p0, v2, v3, v4}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;FFI)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorAnim:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;

    .line 5395
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->refreshHideSideMirrorMessage()V

    .line 5396
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowSideMirrorStartTime:J

    .line 5402
    :goto_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorAnim:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;->start()V

    .line 5403
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorAnim:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->registerLaunchedAnimation(Lcom/sec/android/gallery3d/anim/CanvasAnimation;)V

    .line 5405
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_3

    .line 5406
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->setVisibility(I)V

    :cond_3
    move v0, v1

    .line 5407
    goto :goto_0

    .line 5398
    :cond_4
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->getWidth()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    const/16 v4, 0x12c

    invoke-direct {v0, p0, v2, v3, v4}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;FFI)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorAnim:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;

    .line 5399
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->removeHideSideMirrorMessage()V

    .line 5400
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowSideMirrorStartTime:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 5386
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 5410
    :cond_5
    :try_start_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorMotionView:Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;

    if-eqz v2, :cond_1

    .line 5411
    if-eqz p1, :cond_6

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorMotionView:Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->isShowing()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_6
    if-nez p1, :cond_7

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorMotionView:Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 5415
    :cond_7
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorMotionView:Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->setIsShowing(Z)V

    .line 5416
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorAnim:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAnimation(Lcom/sec/android/gallery3d/anim/Animation;)V

    .line 5417
    if-eqz p1, :cond_9

    .line 5418
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorMotionView:Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->getWidth()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    const/4 v3, 0x0

    const/16 v4, 0x12c

    invoke-direct {v0, p0, v2, v3, v4}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;FFI)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorAnim:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;

    .line 5419
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->refreshHideSideMirrorMessage()V

    .line 5424
    :goto_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorAnim:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;->start()V

    .line 5425
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorAnim:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->registerLaunchedAnimation(Lcom/sec/android/gallery3d/anim/CanvasAnimation;)V

    .line 5427
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorMotionView:Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_8

    .line 5428
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorMotionView:Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->setVisibility(I)V

    :cond_8
    move v0, v1

    .line 5429
    goto/16 :goto_0

    .line 5421
    :cond_9
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorMotionView:Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->getWidth()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    const/16 v4, 0x12c

    invoke-direct {v0, p0, v2, v3, v4}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;FFI)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorAnim:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;

    .line 5422
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->removeHideSideMirrorMessage()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method protected unselectAll()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 4926
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mExpansionDetailViewMode:Z

    if-eqz v1, :cond_0

    .line 4927
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSourceMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 4928
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAllItemInMediaSet(Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 4929
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1, v0, v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;I)V

    .line 4930
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 4931
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    .line 4939
    .end local v0    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateCountOnActionBar()V

    .line 4940
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->unselectAll()V

    .line 4941
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateMenuOperations()V

    .line 4942
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->invalidate()V

    .line 4943
    return-void

    .line 4933
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 4934
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;I)V

    .line 4935
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 4936
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    goto :goto_0
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 29
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 3559
    move-object/from16 v19, p2

    check-cast v19, Lcom/sec/samsung/gallery/core/Event;

    .line 3560
    .local v19, "ev":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual/range {v19 .. v19}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v20

    .line 3562
    .local v20, "eventType":I
    sget-object v3, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Event update ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3564
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v17

    .line 3565
    .local v17, "current":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v17, :cond_1

    .line 3786
    :cond_0
    :goto_0
    return-void

    .line 3571
    :cond_1
    :try_start_0
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_SELECT_ALL:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_2

    .line 3572
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->selectAll()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3783
    :catch_0
    move-exception v18

    .line 3784
    .local v18, "e":Ljava/lang/Exception;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 3573
    .end local v18    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_1
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_UNSELECT_ALL:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_3

    .line 3574
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->unselectAll()V

    goto :goto_0

    .line 3575
    :cond_3
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELAY_HIDE_BARS:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_4

    .line 3576
    const/16 v3, 0xdac

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->refreshHidingMessage(I)V

    goto :goto_0

    .line 3578
    :cond_4
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_SLIDESHOW_SETTING:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_8

    .line 3579
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isIncomingCall(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 3580
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0e0066

    invoke-static {v3, v9}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 3583
    :cond_5
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideSettingPopup:Z

    if-nez v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isExternalDisplayAvailable(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 3584
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 3585
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v9, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v9, v10}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v3, v9}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 3586
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mShowBarManager:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;

    sget v9, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_SLIDESHOW_SETTING:I

    invoke-virtual {v3, v9}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->setShowBarMode(I)V

    .line 3587
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v9, "SLIDE_SHOW_SETTING_VIEW"

    invoke-virtual {v3, v9}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 3588
    new-instance v3, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    const-string v9, "SLIDE_SHOW_SETTING_VIEW"

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v3, v9, v10}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;-><init>(Ljava/lang/String;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    .line 3589
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    const/4 v9, 0x1

    invoke-virtual {v3, v9}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->setSlideshowSettingEnable(Z)V

    .line 3590
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;

    const/4 v9, 0x1

    invoke-interface {v3, v9}, Lcom/sec/android/gallery3d/app/GalleryApp;->setSlideShowMode(Z)V

    .line 3595
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v9}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->unregisterEAM(Landroid/content/BroadcastReceiver;)V

    goto/16 :goto_0

    .line 3592
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v9, "PREPARE_SLIDE_SHOW_DATA"

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3, v9, v10}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 3597
    :cond_8
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_CROP_MEDIA:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_9

    .line 3598
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->handleCrop(Lcom/sec/android/gallery3d/data/MediaItem;)V

    goto/16 :goto_0

    .line 3601
    :cond_9
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_ROTATE_MEDIA:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_a

    .line 3602
    invoke-virtual/range {v19 .. v19}, Lcom/sec/samsung/gallery/core/Event;->getIntData()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->handleImageRotation(ILcom/sec/android/gallery3d/data/MediaItem;)V

    goto/16 :goto_0

    .line 3604
    :cond_a
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_MANUAL_DETECT:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_b

    .line 3605
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->startManualDetect()V

    goto/16 :goto_0

    .line 3607
    :cond_b
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_SCAN_NEARBY_DEVICES:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_c

    .line 3608
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    if-eqz v3, :cond_0

    .line 3609
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->refresh()V

    .line 3610
    invoke-static {}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->scanForNearbyPlayerDevices()V

    goto/16 :goto_0

    .line 3613
    :cond_c
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_BURST_PLAY:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_d

    .line 3614
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->addItemsToSlideshowProxy(Z)V

    goto/16 :goto_0

    .line 3616
    :cond_d
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_ON_MAP:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_e

    .line 3617
    const/4 v3, 0x2

    new-array v0, v3, [D

    move-object/from16 v22, v0

    .line 3618
    .local v22, "latlng":[D
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatLong([D)V

    .line 3619
    const/4 v3, 0x0

    aget-wide v4, v22, v3

    .line 3620
    .local v4, "latitude":D
    const/4 v3, 0x1

    aget-wide v6, v22, v3

    .line 3622
    .local v6, "longitude":D
    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3623
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->showOnMap(DD)V

    goto/16 :goto_0

    .line 3624
    .end local v4    # "latitude":D
    .end local v6    # "longitude":D
    .end local v22    # "latlng":[D
    :cond_e
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_START_NAVIGATION:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_10

    .line 3625
    const/4 v3, 0x2

    new-array v0, v3, [D

    move-object/from16 v26, v0

    .line 3626
    .local v26, "photolatlng":[D
    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatLong([D)V

    .line 3627
    const/4 v3, 0x0

    aget-wide v4, v26, v3

    .line 3628
    .restart local v4    # "latitude":D
    const/4 v3, 0x1

    aget-wide v6, v26, v3

    .line 3630
    .restart local v6    # "longitude":D
    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3631
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaiduPositioning:Z

    if-eqz v3, :cond_f

    .line 3632
    new-instance v8, Lcom/sec/android/gallery3d/util/ReverseGeocoder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v8, v3}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;-><init>(Landroid/content/Context;)V

    .local v8, "geocoder":Lcom/sec/android/gallery3d/util/ReverseGeocoder;
    move-object/from16 v3, p0

    .line 3633
    invoke-direct/range {v3 .. v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->generateName(DDLcom/sec/android/gallery3d/util/ReverseGeocoder;)Ljava/lang/String;

    move-result-object v14

    .local v14, "name":Ljava/lang/String;
    move-object/from16 v9, p0

    move-wide v10, v4

    move-wide v12, v6

    .line 3634
    invoke-direct/range {v9 .. v14}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->startNavigation(DDLjava/lang/String;)V

    goto/16 :goto_0

    .line 3636
    .end local v8    # "geocoder":Lcom/sec/android/gallery3d/util/ReverseGeocoder;
    .end local v14    # "name":Ljava/lang/String;
    :cond_f
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->startNavigation(DD)V

    goto/16 :goto_0

    .line 3640
    .end local v4    # "latitude":D
    .end local v6    # "longitude":D
    .end local v26    # "photolatlng":[D
    :cond_10
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_IMAGE_NOTE:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_12

    .line 3641
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v10, 0x10

    invoke-virtual {v3, v10, v11}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 3642
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    if-eqz v3, :cond_0

    .line 3643
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->runPhotoNoteDialog()V

    goto/16 :goto_0

    .line 3646
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    if-eqz v3, :cond_0

    .line 3647
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->startRotateOutAnim()V

    goto/16 :goto_0

    .line 3650
    :cond_12
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_HOME_ICON:I

    move/from16 v0, v20

    if-eq v0, v3, :cond_13

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_GOTO_UP:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_14

    .line 3651
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->stopDecoder(Lcom/sec/samsung/gallery/decoder/DecoderInterface;)V

    .line 3652
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->handleGoUpButton()V

    goto/16 :goto_0

    .line 3653
    :cond_14
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_CHANGE_PLAYER:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_17

    .line 3654
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseChnUsageAlertPopup:Z

    const/4 v9, 0x1

    if-ne v3, v9, :cond_16

    .line 3655
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v9, "ChangePlayerWifiDataAlertDialogOff"

    const/4 v10, 0x0

    invoke-static {v3, v9, v10}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isWifiConnected(Landroid/content/Context;)Z

    move-result v3

    const/4 v9, 0x1

    if-ne v3, v9, :cond_15

    .line 3657
    const/4 v3, 0x4

    new-array v0, v3, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v9, v25, v3

    const/4 v3, 0x1

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v25, v3

    const/4 v3, 0x2

    const/4 v9, 0x6

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v25, v3

    const/4 v3, 0x3

    new-instance v9, Landroid/os/Handler;

    new-instance v10, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$21;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$21;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    invoke-direct {v9, v10}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    aput-object v9, v25, v3

    .line 3674
    .local v25, "params":[Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v3

    const-string v9, "SHOW_USAGE_ALERT_DIALOG"

    move-object/from16 v0, v25

    invoke-virtual {v3, v9, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 3676
    .end local v25    # "params":[Ljava/lang/Object;
    :cond_15
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->showChangePlayerDialog()V

    goto/16 :goto_0

    .line 3679
    :cond_16
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->showChangePlayerDialog()V

    goto/16 :goto_0

    .line 3681
    :cond_17
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_NEXT_IMAGE:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_18

    .line 3682
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 3683
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetToFullView()V

    .line 3684
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->nextImage()Z

    goto/16 :goto_0

    .line 3685
    :cond_18
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_PREVIOUS_IMAGE:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_19

    .line 3686
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 3687
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetToFullView()V

    .line 3688
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->previousImage()Z

    goto/16 :goto_0

    .line 3689
    :cond_19
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_IMPORT:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_1a

    .line 3690
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v9, "SHOW_IMPORT_DIALOG"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v12, v10, v11

    const/4 v11, 0x1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    aput-object v12, v10, v11

    invoke-virtual {v3, v9, v10}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 3693
    :cond_1a
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_PLAY_VIDEO:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_1b

    .line 3694
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->playCurrentMedia(Z)Z

    goto/16 :goto_0

    .line 3695
    :cond_1b
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_LAST_SHARE_APP:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_1c

    .line 3696
    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v9, v25, v3

    .line 3699
    .restart local v25    # "params":[Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v9, "LAST_SHARE_APP"

    move-object/from16 v0, v25

    invoke-virtual {v3, v9, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 3700
    .end local v25    # "params":[Ljava/lang/Object;
    :cond_1c
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_MAKE_MOTION_PICTURE:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_1d

    .line 3701
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->makeMotionPicture()V

    goto/16 :goto_0

    .line 3702
    :cond_1d
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_MAKE_BUSRTSHOT_PICTURE:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_1e

    .line 3703
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->editBurstShot()V

    goto/16 :goto_0

    .line 3704
    :cond_1e
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_FROM_SECRETBOX:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_1f

    .line 3705
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v10, 0x7

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v23

    .line 3707
    .local v23, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v23, :cond_0

    .line 3708
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    .line 3709
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentMediaSetIndex:I

    const/4 v10, 0x1

    const/4 v11, 0x3

    move-object/from16 v0, v23

    invoke-virtual {v3, v0, v9, v10, v11}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAlbumListDialogToCopyMove(Lcom/sec/android/gallery3d/data/MediaSet;IZI)V

    goto/16 :goto_0

    .line 3712
    .end local v23    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1f
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_FROM_KNOX:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_20

    .line 3713
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v10, 0x7

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v23

    .line 3715
    .restart local v23    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v23, :cond_0

    .line 3716
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    .line 3717
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentMediaSetIndex:I

    const/4 v10, 0x1

    const/4 v11, 0x4

    move-object/from16 v0, v23

    invoke-virtual {v3, v0, v9, v10, v11}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAlbumListDialogToCopyMove(Lcom/sec/android/gallery3d/data/MediaSet;IZI)V

    goto/16 :goto_0

    .line 3720
    .end local v23    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_20
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_LOCK_TOUCH:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_21

    .line 3721
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mLockTouchEvent:Z

    .line 3722
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    const/4 v9, 0x1

    invoke-virtual {v3, v9}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setLockTouchEvent(Z)V

    goto/16 :goto_0

    .line 3723
    :cond_21
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_SEND_TO_OTHER_DEVICES:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_22

    .line 3724
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    const v9, 0x7f0f02ad

    invoke-virtual {v3, v9}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->enqueueMenuItemAfterModalDownload(I)V

    .line 3725
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v28, v0

    check-cast v28, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    .line 3726
    .local v28, "sLinkImage":Lcom/sec/android/gallery3d/remote/slink/SLinkImage;
    const/4 v3, 0x1

    new-array v0, v3, [J

    move-object/from16 v21, v0

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v3, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->getSlinkObjectId()J

    move-result-wide v10

    aput-wide v10, v21, v9

    .line 3729
    .local v21, "ids":[J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual/range {v28 .. v28}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->getDeviceId()I

    move-result v9

    move-object/from16 v0, v21

    invoke-static {v3, v9, v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->createDeviceChooserActivityIntent(Landroid/content/Context;I[J)Landroid/content/Intent;

    move-result-object v27

    .line 3731
    .local v27, "sIntent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/16 v9, 0x30b

    move-object/from16 v0, v27

    invoke-virtual {v3, v0, v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 3732
    .end local v21    # "ids":[J
    .end local v27    # "sIntent":Landroid/content/Intent;
    .end local v28    # "sLinkImage":Lcom/sec/android/gallery3d/remote/slink/SLinkImage;
    :cond_22
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_ACTION_DELETE:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_23

    .line 3733
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->startDelete()V

    goto/16 :goto_0

    .line 3734
    :cond_23
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_CATEGORY:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_28

    .line 3735
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 3736
    .local v16, "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const/16 v24, 0x0

    .line 3737
    .local v24, "originalSet":Lcom/sec/android/gallery3d/data/MediaSet;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOriginalSetPathString:Ljava/lang/String;

    if-eqz v3, :cond_24

    .line 3738
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOriginalSetPathString:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v24

    .line 3739
    :cond_24
    move-object/from16 v0, v16

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3741
    const/4 v15, 0x0

    .line 3742
    .local v15, "actionType":Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v3

    if-eqz v3, :cond_26

    .line 3743
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSourceMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 3744
    .local v2, "SelectedMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3745
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    sget-object v9, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v3, v9, :cond_25

    .line 3746
    sget-object v15, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;->REMOVE_PEOPLE_TAG:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;

    .line 3750
    :goto_2
    const/4 v3, 0x5

    new-array v0, v3, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v9, v25, v3

    const/4 v3, 0x1

    aput-object v16, v25, v3

    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaItemArrayList()Ljava/util/ArrayList;

    move-result-object v9

    aput-object v9, v25, v3

    const/4 v3, 0x3

    aput-object v15, v25, v3

    const/4 v3, 0x4

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v25, v3

    .line 3753
    .restart local v25    # "params":[Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v9, "START_CATEGORY_TAG"

    move-object/from16 v0, v25

    invoke-virtual {v3, v9, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 3748
    .end local v25    # "params":[Ljava/lang/Object;
    :cond_25
    sget-object v15, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;->REMOVE_PHOTO_TAG:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;

    goto :goto_2

    .line 3755
    .end local v2    # "SelectedMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_26
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3756
    sget-object v15, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;->REMOVE_DETAIL_TAG:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;

    .line 3757
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    sget-object v9, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v3, v9, :cond_27

    .line 3758
    sget-object v15, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;->REMOVE_DETAIL_PEOPLE_TAG:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;

    .line 3759
    :cond_27
    const/4 v3, 0x5

    new-array v0, v3, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v9, v25, v3

    const/4 v3, 0x1

    aput-object v16, v25, v3

    const/4 v3, 0x2

    const/4 v9, 0x0

    aput-object v9, v25, v3

    const/4 v3, 0x3

    aput-object v15, v25, v3

    const/4 v3, 0x4

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v25, v3

    .line 3762
    .restart local v25    # "params":[Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v9, "START_CATEGORY_TAG"

    move-object/from16 v0, v25

    invoke-virtual {v3, v9, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 3765
    .end local v15    # "actionType":Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;
    .end local v16    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v24    # "originalSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v25    # "params":[Ljava/lang/Object;
    :cond_28
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELETE_SOUND_IN_PICTURE:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_29

    .line 3766
    const/4 v3, 0x3

    new-array v0, v3, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v9, v25, v3

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    aput-object v9, v25, v3

    const/4 v3, 0x2

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v25, v3

    .line 3769
    .restart local v25    # "params":[Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v9, "SOUNDSCENE_EDITOR"

    move-object/from16 v0, v25

    invoke-virtual {v3, v9, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 3770
    .end local v25    # "params":[Ljava/lang/Object;
    :cond_29
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELETE_SOUND_IN_PICTURE_CONFIRM:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_2b

    .line 3771
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v10, 0x10

    const/4 v9, 0x0

    invoke-virtual {v3, v10, v11, v9}, Lcom/sec/android/gallery3d/data/MediaItem;->setAttribute(JZ)V

    .line 3772
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->updateMenuOperations()V

    .line 3773
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    if-eqz v3, :cond_2a

    .line 3774
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoViewIcon:Lcom/sec/android/gallery3d/ui/PhotoViewIcon;

    const/4 v9, 0x0

    invoke-virtual {v3, v9}, Lcom/sec/android/gallery3d/ui/PhotoViewIcon;->showSoundSceneIcon(Z)V

    .line 3775
    :cond_2a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->invalidate()V

    goto/16 :goto_0

    .line 3776
    :cond_2b
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_POPUP_DIALOG_VISIBLE:I

    move/from16 v0, v20

    if-ne v0, v3, :cond_0

    .line 3777
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsPopupDialogDisplayed:Z

    .line 3778
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    if-eqz v3, :cond_0

    .line 3780
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;

    const/4 v9, 0x1

    invoke-virtual {v3, v9}, Landroid/os/Handler;->removeMessages(I)V

    .line 3781
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->showBars()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public updateChangePlayerIcon()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4587
    sget-object v1, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "update change player icon"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4588
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMenu:Landroid/view/Menu;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFromSetupWidzard:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGalleryCoverMode:Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isCoverMode()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsEmergencyMode:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFromWeTogether:Z

    if-eqz v1, :cond_2

    .line 4623
    :cond_1
    :goto_0
    return-void

    .line 4593
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMenu:Landroid/view/Menu;

    const v2, 0x7f0f0299

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 4594
    .local v0, "menuItem":Landroid/view/MenuItem;
    if-eqz v0, :cond_1

    .line 4598
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isSideSyncConnected()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4599
    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4600
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 4605
    :cond_3
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isPlayerChanged()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 4606
    sget-object v1, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TAG:Ljava/lang/String;

    const-string v2, "player is changed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4607
    const v1, 0x7f02001c

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 4608
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 4617
    :goto_1
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsKnox2Enabled:Z

    if-eqz v1, :cond_6

    .line 4618
    sget-object v1, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "updateChangePlayerIcon : KNOX mode is enabled. All share icon would be hidden"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4619
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 4609
    :cond_4
    invoke-static {}, Lcom/sec/android/gallery3d/util/HdmiUtils;->isPlugged()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 4610
    const v1, 0x7f02001b

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 4611
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 4613
    :cond_5
    sget-object v1, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TAG:Ljava/lang/String;

    const-string v2, "player is not changed. normal icon"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4614
    const v1, 0x7f0203b2

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 4615
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 4621
    :cond_6
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->drawChangePlayerIcon(Landroid/view/MenuItem;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    goto :goto_0
.end method

.method public updateShowBar()V
    .locals 2

    .prologue
    .line 5861
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$31;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$31;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 5879
    return-void
.end method
