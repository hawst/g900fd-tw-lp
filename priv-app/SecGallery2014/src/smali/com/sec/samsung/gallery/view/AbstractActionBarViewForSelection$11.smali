.class Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$11;
.super Ljava/lang/Object;
.source "AbstractActionBarViewForSelection.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->setupPopupMenu(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)V
    .locals 0

    .prologue
    .line 503
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$11;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 509
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$11;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    # getter for: Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->access$300(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$11;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0e0048

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 510
    .local v0, "selectAllItem":Z
    if-eqz v0, :cond_0

    .line 511
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$11;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$11;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    # getter for: Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mNumberOfItemsSelected:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->access$500(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->processSelectAllMenuItemClick(II)V

    .line 514
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$11;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 515
    return-void

    .line 513
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$11;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$11;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    # getter for: Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mNumberOfItemsSelected:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->access$500(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->processSelectAllMenuItemClick(II)V

    goto :goto_0
.end method
