.class Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;
.super Lcom/sec/android/gallery3d/ui/GLView;
.source "HelpDetailViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/GLView;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 345
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const-wide/16 v6, 0xc8

    const-wide/16 v4, 0x10

    const/16 v3, 0x9

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 211
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    if-nez v2, :cond_1

    .line 309
    :cond_0
    :goto_0
    return v0

    .line 214
    :cond_1
    sparse-switch p1, :sswitch_data_0

    .line 307
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setFocusable(Z)V

    .line 308
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestFocus()Z

    goto :goto_0

    .line 217
    :sswitch_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->isFocusable()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/GLView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 224
    :sswitch_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 225
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    .line 226
    goto :goto_0

    .line 228
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setFocusable(Z)V

    .line 229
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1800(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 230
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 235
    :sswitch_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->showBarsForAShortTime()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$2000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)V

    .line 236
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$2100(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 237
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setFocusable(Z)V

    .line 238
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestFocus()Z

    .line 239
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0

    .line 241
    :sswitch_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 242
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->showBarsForAShortTime()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$2000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)V

    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$2200(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 244
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetToFullView()V

    .line 245
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->previousImage()Z

    move v0, v1

    .line 246
    goto/16 :goto_0

    .line 251
    :sswitch_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 252
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->showBarsForAShortTime()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$2000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)V

    .line 253
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$2300(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 254
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetToFullView()V

    .line 255
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->nextImage()Z

    move v0, v1

    .line 256
    goto/16 :goto_0

    .line 262
    :sswitch_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-nez v2, :cond_0

    .line 265
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->isInScale()Z

    move-result v0

    if-nez v0, :cond_5

    .line 266
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    const v2, 0x3f866666    # 1.05f

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->startScale(F)V

    .line 267
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Landroid/os/Handler;

    move-result-object v0

    const/4 v2, 0x7

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 268
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Landroid/os/Handler;

    move-result-object v0

    const/4 v2, 0x6

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_5
    move v0, v1

    .line 270
    goto/16 :goto_0

    .line 272
    :sswitch_6
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-nez v2, :cond_0

    .line 275
    :cond_6
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    const/high16 v2, 0x3fa00000    # 1.25f

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->startScale(F)V

    .line 276
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 277
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    move v0, v1

    .line 278
    goto/16 :goto_0

    .line 281
    :sswitch_7
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-nez v2, :cond_0

    .line 284
    :cond_7
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->isInScale()Z

    move-result v0

    if-nez v0, :cond_8

    .line 285
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    const v2, 0x3f733333    # 0.95f

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->startScale(F)V

    .line 286
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Landroid/os/Handler;

    move-result-object v0

    const/4 v2, 0x6

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 287
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Landroid/os/Handler;

    move-result-object v0

    const/4 v2, 0x7

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_8
    move v0, v1

    .line 289
    goto/16 :goto_0

    .line 291
    :sswitch_8
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-nez v2, :cond_0

    .line 294
    :cond_9
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    const/high16 v2, 0x3f400000    # 0.75f

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->startScale(F)V

    .line 295
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 296
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    move v0, v1

    .line 297
    goto/16 :goto_0

    .line 299
    :sswitch_9
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->toggleScale()V

    .line 300
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 301
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    move v0, v1

    .line 302
    goto/16 :goto_0

    .line 305
    :sswitch_a
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0

    .line 214
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_1
        0x14 -> :sswitch_2
        0x15 -> :sswitch_3
        0x16 -> :sswitch_4
        0x17 -> :sswitch_0
        0x25 -> :sswitch_5
        0x2b -> :sswitch_7
        0x42 -> :sswitch_0
        0x71 -> :sswitch_a
        0x72 -> :sswitch_a
        0xa8 -> :sswitch_5
        0xa9 -> :sswitch_7
        0x117 -> :sswitch_8
        0x118 -> :sswitch_6
        0x119 -> :sswitch_9
    .end sparse-switch
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 329
    const/16 v1, 0xa8

    if-eq p1, v1, :cond_0

    const/16 v1, 0x25

    if-eq p1, v1, :cond_0

    const/16 v1, 0xa9

    if-eq p1, v1, :cond_0

    const/16 v1, 0x2b

    if-ne p1, v1, :cond_1

    .line 341
    :cond_0
    :goto_0
    return v0

    .line 332
    :cond_1
    const/16 v1, 0x42

    if-eq p1, v1, :cond_2

    const/16 v1, 0x17

    if-ne p1, v1, :cond_4

    .line 333
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->isFocusable()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v1

    if-nez v1, :cond_3

    .line 335
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->enterSelectionMode()V

    goto :goto_0

    .line 338
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 341
    :cond_4
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/GLView;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 315
    const/16 v0, 0x118

    if-eq p1, v0, :cond_0

    const/16 v0, 0x117

    if-eq p1, v0, :cond_0

    const/16 v0, 0xa8

    if-eq p1, v0, :cond_0

    const/16 v0, 0x25

    if-eq p1, v0, :cond_0

    const/16 v0, 0xa9

    if-eq p1, v0, :cond_0

    const/16 v0, 0x2b

    if-ne p1, v0, :cond_2

    .line 318
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    const-wide/16 v2, 0x10

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 319
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/GLView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 324
    :goto_0
    return v0

    .line 321
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->endScale()V

    .line 322
    const/4 v0, 0x1

    goto :goto_0

    .line 324
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/GLView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 14
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 169
    const/4 v7, 0x0

    .line 171
    .local v7, "modifiedGapForFilmStripView":I
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v9

    if-eqz v9, :cond_0

    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHWKey:Z

    if-nez v9, :cond_0

    .line 172
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1100(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 173
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1200(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 174
    const/16 v7, 0x30

    .line 179
    :cond_0
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    sub-int v12, p4, p2

    sub-int v13, p5, p3

    invoke-virtual {v9, v10, v11, v12, v13}, Lcom/sec/android/gallery3d/ui/PhotoView;->layout(IIII)V

    .line 181
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1300(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/gallery3d/ui/PositionRepository;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/android/gallery3d/ui/PositionRepository;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/gallery3d/ui/PositionRepository;->setOffset(II)V

    .line 182
    const/4 v4, 0x0

    .line 183
    .local v4, "filmStripHeight":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 184
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1400(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->get(Landroid/content/Context;)Lcom/sec/android/gallery3d/app/Config$PhotoPage;

    move-result-object v2

    .line 185
    .local v2, "config":Lcom/sec/android/gallery3d/app/Config$PhotoPage;
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v9

    sub-int v10, p4, p2

    const/high16 v11, 0x40000000    # 2.0f

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/gallery3d/ui/FilmStripView;->measure(II)V

    .line 188
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/FilmStripView;->getMeasuredHeight()I

    move-result v4

    .line 189
    sub-int v8, p4, p2

    .line 190
    .local v8, "width":I
    iget v9, v2, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->filmstripContentSize:I

    iget v10, v2, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->filmstripMidMargin:I

    add-int/2addr v9, v10

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-static {v10}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1500(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v10

    mul-int v3, v9, v10

    .line 192
    .local v3, "contentWidth":I
    if-ge v8, v3, :cond_2

    move v0, v8

    .line 194
    .local v0, "barSize":I
    :goto_0
    const/4 v1, 0x2

    .line 195
    .local v1, "barStart":I
    add-int/lit8 v6, v8, -0x4

    .line 197
    .local v6, "maxBarSize":I
    if-lt v0, v6, :cond_3

    .line 198
    move v0, v6

    .line 202
    :goto_1
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1600(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0d0113

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 203
    .local v5, "filmstripBottomPadding":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v9

    sub-int v10, p5, p3

    sub-int/2addr v10, v4

    sub-int/2addr v10, v7

    add-int/2addr v10, v5

    add-int v11, v1, v0

    sub-int v12, p5, p3

    sub-int/2addr v12, v7

    invoke-virtual {v9, v1, v10, v11, v12}, Lcom/sec/android/gallery3d/ui/FilmStripView;->layout(IIII)V

    .line 206
    .end local v0    # "barSize":I
    .end local v1    # "barStart":I
    .end local v2    # "config":Lcom/sec/android/gallery3d/app/Config$PhotoPage;
    .end local v3    # "contentWidth":I
    .end local v5    # "filmstripBottomPadding":I
    .end local v6    # "maxBarSize":I
    .end local v8    # "width":I
    :cond_1
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v9

    sub-int v10, p5, p3

    sub-int/2addr v10, v4

    sub-int/2addr v10, v7

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/ui/PhotoView;->setAboveFilmStripHeight(I)V

    .line 207
    return-void

    .restart local v2    # "config":Lcom/sec/android/gallery3d/app/Config$PhotoPage;
    .restart local v3    # "contentWidth":I
    .restart local v8    # "width":I
    :cond_2
    move v0, v3

    .line 192
    goto :goto_0

    .line 200
    .restart local v0    # "barSize":I
    .restart local v1    # "barStart":I
    .restart local v6    # "maxBarSize":I
    :cond_3
    sub-int v9, v8, v0

    div-int/lit8 v1, v9, 0x2

    goto :goto_1
.end method

.method public render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 0
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 159
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 160
    return-void
.end method

.method protected renderBackground(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 0
    .param p1, "view"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 164
    invoke-interface {p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->clearBuffer()V

    .line 165
    return-void
.end method
