.class public Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;
.super Ljava/lang/Object;
.source "NoteActionBar.java"


# static fields
.field private static final BTN_IDS:[I


# instance fields
.field private button_text_padding:I

.field private mActionBtnClickListener:Landroid/view/View$OnClickListener;

.field private final mActivity:Landroid/app/Activity;

.field private mBtnLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mDoneCancelClickListener:Landroid/view/View$OnClickListener;

.field private mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

.field private mPenPosition:F

.field public mPrevButtonId:I

.field private mPrevStrokeColorChange:Z

.field private mRemoverPosition:F

.field private mSwitchBtnClickListener:Landroid/view/View$OnClickListener;

.field private mSwitchBtnKeyListener:Landroid/view/View$OnKeyListener;

.field private mUndoRedoBtnClickListener:Landroid/view/View$OnClickListener;

.field private final mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->BTN_IDS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0f013f
        0x7f0f0141
        0x7f0f0144
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mPrevStrokeColorChange:Z

    .line 35
    iput v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mPenPosition:F

    .line 36
    iput v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mRemoverPosition:F

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mPrevButtonId:I

    .line 114
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$1;-><init>(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActionBtnClickListener:Landroid/view/View$OnClickListener;

    .line 167
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$2;-><init>(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mDoneCancelClickListener:Landroid/view/View$OnClickListener;

    .line 179
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$3;-><init>(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mUndoRedoBtnClickListener:Landroid/view/View$OnClickListener;

    .line 201
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$4;-><init>(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mSwitchBtnClickListener:Landroid/view/View$OnClickListener;

    .line 214
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$5;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$5;-><init>(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mBtnLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 222
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$6;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$6;-><init>(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mSwitchBtnKeyListener:Landroid/view/View$OnKeyListener;

    .line 41
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    .line 42
    iput-object p2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    .line 43
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->initResource()V

    .line 44
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->initActionBarView()V

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->openToolBar()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;
    .param p1, "x1"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->requestFocus(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->closeToolBar()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->getToolBarHeight()I

    move-result v0

    return v0
.end method

.method private activate()V
    .locals 4

    .prologue
    .line 60
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    const v3, 0x7f0f013d

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 61
    .local v1, "openToolBar":Landroid/widget/LinearLayout;
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    const v3, 0x7f0f014b

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 62
    .local v0, "closeToolBar":Landroid/view/View;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 63
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 66
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setPenMode()V

    .line 67
    const v2, 0x7f0f013f

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setCheckedButton(I)V

    .line 68
    return-void
.end method

.method private closeToolBar()V
    .locals 4

    .prologue
    .line 108
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    const v3, 0x7f0f014b

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 109
    .local v0, "closeToolBar":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    const v3, 0x7f0f013d

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 111
    .local v1, "openToolBar":Landroid/view/View;
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 112
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 113
    return-void
.end method

.method private getToolBarHeight()I
    .locals 9

    .prologue
    .line 256
    iget-object v7, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    const v8, 0x7f0f014b

    invoke-virtual {v7, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 257
    .local v0, "closeToolBar":Landroid/view/View;
    iget-object v7, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    const v8, 0x7f0f013d

    invoke-virtual {v7, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 258
    .local v2, "openToolBar":Landroid/view/View;
    const/4 v4, 0x0

    .line 259
    .local v4, "screenWidth":I
    const/4 v3, 0x0

    .line 261
    .local v3, "screenHeight":I
    const/4 v5, 0x0

    .line 262
    .local v5, "topPosition":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 263
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 264
    .local v1, "displayMetrics":Landroid/util/DisplayMetrics;
    iget-object v7, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    const-string/jumbo v8, "window"

    invoke-virtual {v7, v8}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/WindowManager;

    .line 265
    .local v6, "wm":Landroid/view/WindowManager;
    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 266
    iget v4, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 267
    iget v3, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 268
    if-ge v4, v3, :cond_0

    .line 269
    iget-object v7, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v7

    iget v5, v7, Landroid/graphics/Rect;->top:I

    .line 273
    .end local v1    # "displayMetrics":Landroid/util/DisplayMetrics;
    .end local v6    # "wm":Landroid/view/WindowManager;
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-nez v7, :cond_1

    .line 274
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v7

    add-int/2addr v7, v5

    .line 278
    :goto_0
    return v7

    .line 275
    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-nez v7, :cond_2

    .line 276
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v7

    add-int/2addr v7, v5

    goto :goto_0

    .line 278
    :cond_2
    const/4 v7, 0x0

    goto :goto_0
.end method

.method private initActionBarView()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->initToolBar()V

    .line 55
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->initButtons()V

    .line 56
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->activate()V

    .line 57
    return-void
.end method

.method private initButtons()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 154
    const v0, 0x7f0f013f

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActionBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setButtonProperty(ILandroid/view/View$OnClickListener;Z)V

    .line 155
    const v0, 0x7f0f0141

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActionBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setButtonProperty(ILandroid/view/View$OnClickListener;Z)V

    .line 156
    const v0, 0x7f0f0144

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActionBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setButtonProperty(ILandroid/view/View$OnClickListener;Z)V

    .line 157
    const v0, 0x7f0f0143

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mUndoRedoBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0, v0, v1, v3}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setButtonProperty(ILandroid/view/View$OnClickListener;Z)V

    .line 158
    const v0, 0x7f0f0142

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mUndoRedoBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0, v0, v1, v3}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setButtonProperty(ILandroid/view/View$OnClickListener;Z)V

    .line 160
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->updateButtonsTextVisibiltiy()V

    .line 161
    const v0, 0x7f0f0148

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mDoneCancelClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setButtonProperty(ILandroid/view/View$OnClickListener;Z)V

    .line 162
    const v0, 0x7f0f0146

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mDoneCancelClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setButtonProperty(ILandroid/view/View$OnClickListener;Z)V

    .line 163
    const v0, 0x7f0f014d

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mDoneCancelClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setButtonProperty(ILandroid/view/View$OnClickListener;Z)V

    .line 164
    const v0, 0x7f0f014e

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mDoneCancelClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setButtonProperty(ILandroid/view/View$OnClickListener;Z)V

    .line 165
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->updateButtonState(Z)V

    .line 166
    return-void
.end method

.method private initResource()V
    .locals 2

    .prologue
    .line 47
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 48
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0d0145

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mPenPosition:F

    .line 49
    const v1, 0x7f0d0146

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mRemoverPosition:F

    .line 50
    const v1, 0x7f0d0143

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->button_text_padding:I

    .line 51
    return-void
.end method

.method private initToolBar()V
    .locals 4

    .prologue
    .line 74
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    const v3, 0x7f0f014c

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 75
    .local v1, "openBtn":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    const v3, 0x7f0f013e

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 77
    .local v0, "closeBtn":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 78
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mSwitchBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mBtnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 80
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mSwitchBtnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 82
    :cond_0
    if-eqz v0, :cond_1

    .line 83
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mSwitchBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mBtnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 85
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mSwitchBtnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 87
    :cond_1
    return-void
.end method

.method private openToolBar()V
    .locals 4

    .prologue
    .line 89
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    const v3, 0x7f0f014b

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 90
    .local v0, "closeToolBar":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    const v3, 0x7f0f013d

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 92
    .local v1, "openToolBar":Landroid/view/View;
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 93
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 95
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->isRemoverMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 96
    const v2, 0x7f0f0141

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setCheckedButton(I)V

    .line 97
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setRemoverMode()V

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 98
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->isPenMode()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 99
    const v2, 0x7f0f013f

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setCheckedButton(I)V

    .line 100
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setPenMode()V

    goto :goto_0

    .line 101
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->isMovingMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    const v2, 0x7f0f0144

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setCheckedButton(I)V

    .line 103
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setMovingMode()V

    goto :goto_0
.end method

.method private requestFocus(I)V
    .locals 2
    .param p1, "viewId"    # I

    .prologue
    .line 249
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 250
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 251
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 253
    :cond_0
    return-void
.end method

.method private setButtonProperty(ILandroid/view/View$OnClickListener;Z)V
    .locals 2
    .param p1, "resouceID"    # I
    .param p2, "listener"    # Landroid/view/View$OnClickListener;
    .param p3, "enabled"    # Z

    .prologue
    .line 350
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 351
    .local v0, "btn":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 352
    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 353
    invoke-virtual {v0, p3}, Landroid/view/View;->setEnabled(Z)V

    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setButtonHintPopup(I)V

    .line 356
    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setHoverProperty(Landroid/view/View;I)V

    .line 357
    return-void
.end method

.method private setButtonText(IIZ)V
    .locals 4
    .param p1, "resouceID"    # I
    .param p2, "textResourceID"    # I
    .param p3, "visible"    # Z

    .prologue
    const/4 v3, 0x0

    .line 301
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 302
    .local v1, "btnView":Landroid/view/View;
    instance-of v2, v1, Landroid/widget/Button;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 303
    check-cast v0, Landroid/widget/Button;

    .line 304
    .local v0, "btn":Landroid/widget/Button;
    if-eqz v0, :cond_0

    .line 305
    if-eqz p3, :cond_1

    .line 306
    invoke-virtual {v0, p2}, Landroid/widget/Button;->setText(I)V

    .line 307
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTextColor(I)V

    .line 308
    iget v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->button_text_padding:I

    invoke-virtual {v0, v3, v3, v2, v3}, Landroid/widget/Button;->setPadding(IIII)V

    .line 315
    .end local v0    # "btn":Landroid/widget/Button;
    :cond_0
    :goto_0
    return-void

    .line 310
    .restart local v0    # "btn":Landroid/widget/Button;
    :cond_1
    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 311
    iget v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->button_text_padding:I

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v2, v3, v3, v3}, Landroid/widget/Button;->setPadding(IIII)V

    goto :goto_0
.end method

.method private setHoverProperty(Landroid/view/View;I)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "type"    # I

    .prologue
    .line 385
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.feature.hovering_ui"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 387
    :try_start_0
    const-class v1, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;

    const-string v2, "setHoverPopupType"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 392
    :cond_0
    :goto_0
    return-void

    .line 388
    :catch_0
    move-exception v0

    .line 389
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private updateButton(Landroid/widget/Button;Z)V
    .locals 3
    .param p1, "btn"    # Landroid/widget/Button;
    .param p2, "enable"    # Z

    .prologue
    .line 340
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->getNoteType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 341
    if-eqz p2, :cond_1

    const/4 v0, -0x1

    .line 342
    .local v0, "color":I
    :goto_0
    if-eqz p1, :cond_0

    .line 343
    invoke-virtual {p1, p2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 344
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setTextColor(I)V

    .line 347
    .end local v0    # "color":I
    :cond_0
    return-void

    .line 341
    :cond_1
    const v0, -0x736055

    goto :goto_0
.end method

.method private updateImageButton(Landroid/widget/ImageButton;Z)V
    .locals 2
    .param p1, "btn"    # Landroid/widget/ImageButton;
    .param p2, "enable"    # Z

    .prologue
    .line 334
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->getNoteType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 335
    invoke-virtual {p1, p2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 337
    :cond_0
    return-void
.end method


# virtual methods
.method public getPenPosition()F
    .locals 1

    .prologue
    .line 431
    iget v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mPenPosition:F

    return v0
.end method

.method public getPrevStrokeColoerChange()Z
    .locals 1

    .prologue
    .line 406
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mPrevStrokeColorChange:Z

    return v0
.end method

.method public getRemoverPosition()F
    .locals 1

    .prologue
    .line 435
    iget v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mRemoverPosition:F

    return v0
.end method

.method public setButtonHintPopup(I)V
    .locals 4
    .param p1, "resouceID"    # I

    .prologue
    .line 360
    const/4 v2, 0x1

    .line 361
    .local v2, "displayHintPopup":Z
    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 362
    .local v0, "btn":Landroid/view/View;
    if-eqz v0, :cond_2

    .line 363
    const v3, 0x7f0f0148

    if-eq p1, v3, :cond_0

    const v3, 0x7f0f0146

    if-ne p1, v3, :cond_1

    .line 364
    :cond_0
    instance-of v3, v0, Landroid/widget/Button;

    if-eqz v3, :cond_1

    move-object v1, v0

    .line 365
    check-cast v1, Landroid/widget/Button;

    .line 366
    .local v1, "button":Landroid/widget/Button;
    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-nez v3, :cond_3

    const/4 v2, 0x1

    .line 370
    .end local v1    # "button":Landroid/widget/Button;
    :cond_1
    :goto_0
    if-eqz v2, :cond_4

    .line 371
    new-instance v3, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$7;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$7;-><init>(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 382
    :cond_2
    :goto_1
    return-void

    .line 366
    .restart local v1    # "button":Landroid/widget/Button;
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 379
    .end local v1    # "button":Landroid/widget/Button;
    :cond_4
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto :goto_1
.end method

.method public setCheckedButton(I)V
    .locals 6
    .param p1, "btnId"    # I

    .prologue
    .line 282
    sget-object v0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->BTN_IDS:[I

    .local v0, "arr$":[I
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v4, :cond_1

    aget v3, v0, v2

    .line 283
    .local v3, "id":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;

    .line 284
    .local v1, "btn":Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;
    if-ne v3, p1, :cond_0

    .line 285
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;->setChecked(Z)V

    .line 282
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 287
    :cond_0
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;->setChecked(Z)V

    goto :goto_1

    .line 290
    .end local v1    # "btn":Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;
    .end local v3    # "id":I
    :cond_1
    return-void
.end method

.method public setMultiWindow(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;)V
    .locals 0
    .param p1, "multiWindow"    # Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 72
    return-void
.end method

.method public setPrevStrokeColorChange(Z)V
    .locals 0
    .param p1, "change"    # Z

    .prologue
    .line 402
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mPrevStrokeColorChange:Z

    .line 403
    return-void
.end method

.method public setRedoable(Z)V
    .locals 3
    .param p1, "isRedoable"    # Z

    .prologue
    .line 425
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    const v2, 0x7f0f0142

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 426
    .local v0, "redoBtn":Landroid/widget/ImageButton;
    if-eqz v0, :cond_0

    .line 427
    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 429
    :cond_0
    return-void
.end method

.method public setUndoable(Z)V
    .locals 3
    .param p1, "isUndoable"    # Z

    .prologue
    .line 418
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    const v2, 0x7f0f0143

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 419
    .local v0, "undoBtn":Landroid/widget/ImageButton;
    if-eqz v0, :cond_0

    .line 420
    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 422
    :cond_0
    return-void
.end method

.method public updateBrushIcon(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;)V
    .locals 4
    .param p1, "brush"    # Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    .prologue
    .line 410
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    const v3, 0x7f0f013f

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;

    .line 411
    .local v0, "penButton":Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;
    if-eqz v0, :cond_0

    .line 412
    const v2, 0x7f0f014a

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 413
    .local v1, "penIcon":Landroid/widget/ImageView;
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageLevel(I)V

    .line 415
    .end local v1    # "penIcon":Landroid/widget/ImageView;
    :cond_0
    return-void
.end method

.method public updateButtonState(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 318
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    const v3, 0x7f0f014e

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 319
    .local v0, "closeBtn":Landroid/view/View;
    instance-of v2, v0, Landroid/widget/ImageButton;

    if-eqz v2, :cond_0

    .line 320
    check-cast v0, Landroid/widget/ImageButton;

    .end local v0    # "closeBtn":Landroid/view/View;
    invoke-direct {p0, v0, p1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->updateImageButton(Landroid/widget/ImageButton;Z)V

    .line 325
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    const v3, 0x7f0f0146

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 326
    .local v1, "openBtn":Landroid/view/View;
    instance-of v2, v1, Landroid/widget/ImageButton;

    if-eqz v2, :cond_1

    .line 327
    check-cast v1, Landroid/widget/ImageButton;

    .end local v1    # "openBtn":Landroid/view/View;
    invoke-direct {p0, v1, p1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->updateImageButton(Landroid/widget/ImageButton;Z)V

    .line 331
    :goto_1
    return-void

    .line 322
    .restart local v0    # "closeBtn":Landroid/view/View;
    :cond_0
    check-cast v0, Landroid/widget/Button;

    .end local v0    # "closeBtn":Landroid/view/View;
    invoke-direct {p0, v0, p1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->updateButton(Landroid/widget/Button;Z)V

    goto :goto_0

    .line 329
    .restart local v1    # "openBtn":Landroid/view/View;
    :cond_1
    check-cast v1, Landroid/widget/Button;

    .end local v1    # "openBtn":Landroid/view/View;
    invoke-direct {p0, v1, p1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->updateButton(Landroid/widget/Button;Z)V

    goto :goto_1
.end method

.method public updateButtonsTextVisibiltiy()V
    .locals 5

    .prologue
    const v4, 0x7f0e020a

    const v3, 0x7f0e0046

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 293
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v2

    if-nez v2, :cond_1

    .line 294
    .local v0, "textVisible":Z
    :cond_0
    :goto_0
    const v2, 0x7f0f0148

    invoke-direct {p0, v2, v3, v0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setButtonText(IIZ)V

    .line 295
    const v2, 0x7f0f0146

    invoke-direct {p0, v2, v4, v0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setButtonText(IIZ)V

    .line 296
    const v2, 0x7f0f014d

    invoke-direct {p0, v2, v3, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setButtonText(IIZ)V

    .line 297
    const v2, 0x7f0f014e

    invoke-direct {p0, v2, v4, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setButtonText(IIZ)V

    .line 298
    return-void

    .end local v0    # "textVisible":Z
    :cond_1
    move v0, v1

    .line 293
    goto :goto_0
.end method

.method public updatePenColorView(I)V
    .locals 3
    .param p1, "color"    # I

    .prologue
    .line 395
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;

    const v2, 0x7f0f0140

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 396
    .local v0, "penColorView":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 397
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 399
    :cond_0
    return-void
.end method
