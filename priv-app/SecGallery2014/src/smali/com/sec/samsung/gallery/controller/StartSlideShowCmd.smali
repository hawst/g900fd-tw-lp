.class public Lcom/sec/samsung/gallery/controller/StartSlideShowCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "StartSlideShowCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 12
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const/4 v11, 0x1

    .line 25
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/Object;

    move-object v8, v9

    check-cast v8, [Ljava/lang/Object;

    .line 26
    .local v8, "params":[Ljava/lang/Object;
    const/4 v9, 0x0

    aget-object v0, v8, v9

    check-cast v0, Landroid/app/Activity;

    .line 27
    .local v0, "activity":Landroid/app/Activity;
    aget-object v9, v8, v11

    check-cast v9, Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 28
    .local v2, "burstPlay":Z
    const/4 v9, 0x2

    aget-object v7, v8, v9

    check-cast v7, Ljava/lang/String;

    .line 29
    .local v7, "mediaPath":Ljava/lang/String;
    const/4 v9, 0x3

    aget-object v5, v8, v9

    check-cast v5, Ljava/lang/Integer;

    .line 30
    .local v5, "imageIndex":Ljava/lang/Integer;
    const/4 v9, 0x4

    aget-object v3, v8, v9

    check-cast v3, Ljava/lang/Integer;

    .line 31
    .local v3, "burstPlaySpeed":Ljava/lang/Integer;
    const/4 v9, 0x5

    aget-object v9, v8, v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 33
    .local v6, "isFromEditmode":Z
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 35
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v10, "GALLERY_ACTIVITY_ID"

    move-object v9, v0

    check-cast v9, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryId()I

    move-result v9

    invoke-virtual {v1, v10, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 36
    const-string v9, "burstplay"

    invoke-virtual {v1, v9, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 37
    const-string v9, "editmode"

    invoke-virtual {v1, v9, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 38
    const-string v9, "path"

    invoke-virtual {v1, v9, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    if-eqz v5, :cond_0

    .line 40
    const-string v9, "index"

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v1, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 41
    :cond_0
    if-eqz v3, :cond_1

    .line 42
    const-string v9, "burstplayspeed"

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v1, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 43
    :cond_1
    const-string v10, "clusterType"

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v9}, Lcom/sec/android/gallery3d/app/GalleryApp;->getCurrentClusterType()I

    move-result v9

    invoke-virtual {v1, v10, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move-object v9, v0

    .line 46
    check-cast v9, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v4

    .line 48
    .local v4, "currentState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v9, v4, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-eqz v9, :cond_2

    .line 49
    const-string v9, "KEY_FROM_ALBUM_VIEW_STATE"

    invoke-virtual {v1, v9, v11}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 51
    :cond_2
    instance-of v9, v4, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    if-nez v9, :cond_3

    .line 52
    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local v0    # "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v9

    const-class v10, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-virtual {v9, v10, v1}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 53
    :cond_3
    return-void
.end method
