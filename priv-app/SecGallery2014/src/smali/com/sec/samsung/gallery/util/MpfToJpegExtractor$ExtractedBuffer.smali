.class Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;
.super Ljava/lang/Object;
.source "MpfToJpegExtractor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ExtractedBuffer"
.end annotation


# static fields
.field private static final DEFAULTOUTPUTSIZE:I = 0x100000

.field private static final MAXOUTPUTSIZE:I = 0x500000


# instance fields
.field private mFactor:F

.field private mHeight:I

.field private mOutBuf:[B

.field private mValid:Z

.field private mWidth:I

.field private mWrite:I

.field final synthetic this$0:Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;)V
    .locals 1

    .prologue
    .line 273
    iput-object p1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->this$0:Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 278
    const/high16 v0, 0x100000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mOutBuf:[B

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$1;

    .prologue
    .line 273
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;-><init>(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    .prologue
    .line 273
    invoke-direct {p0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->reset()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 273
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->write(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    .prologue
    .line 273
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mValid:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    .prologue
    .line 273
    iget v0, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mWidth:I

    return v0
.end method

.method static synthetic access$402(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;
    .param p1, "x1"    # I

    .prologue
    .line 273
    iput p1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mWidth:I

    return p1
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    .prologue
    .line 273
    iget v0, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mHeight:I

    return v0
.end method

.method static synthetic access$502(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;
    .param p1, "x1"    # I

    .prologue
    .line 273
    iput p1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mHeight:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    .prologue
    .line 273
    iget v0, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mFactor:F

    return v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)[B
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    .prologue
    .line 273
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mOutBuf:[B

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    .prologue
    .line 273
    iget v0, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mWrite:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;

    .prologue
    .line 273
    invoke-direct {p0}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->determineFactor()V

    return-void
.end method

.method private determineFactor()V
    .locals 4

    .prologue
    .line 291
    const/high16 v2, 0x44f00000    # 1920.0f

    iget v3, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mWidth:I

    int-to-float v3, v3

    div-float v1, v2, v3

    .line 292
    .local v1, "wFact":F
    const/high16 v2, 0x44870000    # 1080.0f

    iget v3, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mHeight:I

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 293
    .local v0, "hFact":F
    cmpg-float v2, v1, v0

    if-gez v2, :cond_0

    .end local v1    # "wFact":F
    :goto_0
    iput v1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mFactor:F

    .line 294
    return-void

    .restart local v1    # "wFact":F
    :cond_0
    move v1, v0

    .line 293
    goto :goto_0
.end method

.method private reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 285
    iput v0, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mWrite:I

    .line 286
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mValid:Z

    .line 287
    return-void
.end method

.method private write(II)Z
    .locals 6
    .param p1, "start"    # I
    .param p2, "count"    # I

    .prologue
    const/high16 v5, 0x500000

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 297
    iget v3, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mWrite:I

    add-int/2addr v3, p2

    iget-object v4, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mOutBuf:[B

    array-length v4, v4

    if-le v3, v4, :cond_1

    .line 298
    iget v3, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mWrite:I

    add-int/2addr v3, p2

    if-le v3, v5, :cond_0

    .line 299
    const-string v2, "MpfToJpegExtractor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ExtractedBuffer.write() is skipped : written = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mWrite:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", newCount = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mValid:Z

    .line 311
    :goto_0
    return v1

    .line 304
    :cond_0
    iget v1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mWrite:I

    add-int/2addr v1, p2

    int-to-float v1, v1

    const v3, 0x3f8ccccd    # 1.1f

    mul-float/2addr v1, v3

    float-to-int v1, v1

    invoke-static {v1, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 305
    .local v0, "newSize":I
    new-array v1, v0, [B

    iput-object v1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mOutBuf:[B

    .line 308
    .end local v0    # "newSize":I
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->this$0:Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;

    # getter for: Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->mAccessArray:[B
    invoke-static {v1}, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;->access$1000(Lcom/sec/samsung/gallery/util/MpfToJpegExtractor;)[B

    move-result-object v1

    iget-object v3, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mOutBuf:[B

    iget v4, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mWrite:I

    invoke-static {v1, p1, v3, v4, p2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 309
    iget v1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mWrite:I

    add-int/2addr v1, p2

    iput v1, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mWrite:I

    .line 310
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/util/MpfToJpegExtractor$ExtractedBuffer;->mValid:Z

    move v1, v2

    .line 311
    goto :goto_0
.end method
