.class public Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;
.super Ljava/lang/Object;
.source "EditCategoryDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;,
        Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$Result;
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;

.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private mItem:Ljava/lang/String;

.field private mResult:Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$Result;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mItem:Ljava/lang/String;

    .line 36
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mContext:Landroid/content/Context;

    .line 37
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mItem:Ljava/lang/String;

    .line 38
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->initDialog()V

    .line 39
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mItem:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mItem:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;)Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$Result;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mResult:Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$Result;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;)Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mAdapter:Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;

    return-object v0
.end method

.method private initDialog()V
    .locals 7

    .prologue
    const v6, 0x7f0e00a9

    .line 46
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    .line 47
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f03008a

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 48
    .local v2, "dialoglayout":Landroid/view/View;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 49
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 50
    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 51
    const v4, 0x7f0e00db

    new-instance v5, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$1;

    invoke-direct {v5, p0}, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$1;-><init>(Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 61
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mDialog:Landroid/app/AlertDialog;

    .line 62
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    const v5, 0x10100

    invoke-virtual {v4, v5}, Landroid/view/Window;->addFlags(I)V

    .line 63
    const v4, 0x7f0f0186

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 64
    .local v1, "categoryList":Landroid/widget/ListView;
    new-instance v4, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mContext:Landroid/content/Context;

    invoke-direct {v4, p0, v5}, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;-><init>(Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mAdapter:Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;

    .line 65
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mAdapter:Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 66
    new-instance v4, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$2;

    invoke-direct {v4, p0, v1}, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$2;-><init>(Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;Landroid/widget/ListView;)V

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 74
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mDialog:Landroid/app/AlertDialog;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 75
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4, v6}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 76
    return-void
.end method


# virtual methods
.method public dimiss()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 88
    :cond_0
    return-void
.end method

.method public registerResultCallBack(Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$Result;)V
    .locals 0
    .param p1, "result"    # Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$Result;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mResult:Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$Result;

    .line 43
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 82
    :cond_0
    return-void
.end method
