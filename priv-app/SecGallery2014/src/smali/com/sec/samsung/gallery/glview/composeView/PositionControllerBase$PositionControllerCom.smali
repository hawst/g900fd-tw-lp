.class public Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;
.super Ljava/lang/Object;
.source "PositionControllerBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PositionControllerCom"
.end annotation


# instance fields
.field public isGroupCheckBoxFocused:Z

.field private mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

.field protected mBrokenMovieCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

.field protected mBrokenPictureCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

.field protected mCanvChecked:Lcom/sec/android/gallery3d/glcore/GlCanvas;

.field protected mCanvUnchecked:Lcom/sec/android/gallery3d/glcore/GlCanvas;

.field private mCanvasMgr:Lcom/sec/samsung/gallery/glview/composeView/GlCanvasManager;

.field public mCheckBoxVisible:Z

.field private mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

.field protected mContext:Landroid/content/Context;

.field protected mExpCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

.field protected mFocused:I

.field public mGroupCheckBoxVisible:Z

.field private mGroupCom:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;

.field private mGroupCount:I

.field private mGrpCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

.field protected mIsFocusVisible:Z

.field public mIsRTL:Z

.field private mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

.field private mPitchRate:F

.field protected mPortraitMode:Z

.field private mPosCtrl1:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

.field private mPosCtrl2:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

.field private mScaleRate:F

.field protected mScrollTopMargine:F

.field protected mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 5
    .param p2, "composeView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1681
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1662
    iput-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mBrokenPictureCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 1663
    iput-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mBrokenMovieCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 1666
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mGroupCount:I

    .line 1667
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mScrollTopMargine:F

    .line 1668
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPitchRate:F

    .line 1669
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mScaleRate:F

    .line 1672
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsFocusVisible:Z

    .line 1674
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mCheckBoxVisible:Z

    .line 1675
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mGroupCheckBoxVisible:Z

    .line 1676
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->isGroupCheckBoxFocused:Z

    .line 1677
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsRTL:Z

    .line 1679
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    .line 1684
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .line 1685
    iget-object v1, p2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mContext:Landroid/content/Context;

    .line 1686
    iget-object v1, p2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    .line 1687
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mIsRTL:Z

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsRTL:Z

    .line 1688
    new-instance v1, Lcom/sec/samsung/gallery/glview/composeView/GlCanvasManager;

    iget-object v2, p2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlCanvasManager;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mCanvasMgr:Lcom/sec/samsung/gallery/glview/composeView/GlCanvasManager;

    .line 1690
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v1, :cond_0

    const v1, 0x7f020122

    :goto_0
    invoke-static {v2, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1692
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v2, p2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v1, v2, v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mCanvChecked:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 1693
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mCanvChecked:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->setManualRecycle(Z)V

    .line 1695
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v1, :cond_1

    const v1, 0x7f020121

    :goto_1
    invoke-static {v2, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1697
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v2, p2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v1, v2, v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mCanvUnchecked:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 1698
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mCanvUnchecked:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->setManualRecycle(Z)V

    .line 1700
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02024e

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1701
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v2, p2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v1, v2, v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mExpCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 1702
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mExpCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->setManualRecycle(Z)V

    .line 1704
    new-instance v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    invoke-direct {v1, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;-><init>(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mGrpCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    .line 1705
    new-instance v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    invoke-direct {v1, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;-><init>(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    .line 1706
    return-void

    .line 1690
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    const v1, 0x7f0204ad

    goto :goto_0

    .line 1695
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    const v1, 0x7f0204a6

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    .prologue
    .line 1649
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mGrpCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    .prologue
    .line 1649
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;)Lcom/sec/samsung/gallery/glview/composeView/GlCanvasManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    .prologue
    .line 1649
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mCanvasMgr:Lcom/sec/samsung/gallery/glview/composeView/GlCanvasManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;)Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    .prologue
    .line 1649
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;)[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    .prologue
    .line 1649
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mGroupCom:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    .prologue
    .line 1649
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mGroupCount:I

    return v0
.end method


# virtual methods
.method public clean()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1755
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mCanvasMgr:Lcom/sec/samsung/gallery/glview/composeView/GlCanvasManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlCanvasManager;->recycle()V

    .line 1756
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mCanvUnchecked:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->recycle()V

    .line 1757
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mCanvChecked:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->recycle()V

    .line 1758
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPosCtrl1:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .line 1759
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPosCtrl2:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .line 1760
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    .line 1761
    return-void
.end method

.method protected getBrokenItemCanvas(I)Lcom/sec/android/gallery3d/glcore/GlCanvas;
    .locals 4
    .param p1, "brokenType"    # I

    .prologue
    const/4 v3, 0x1

    .line 1711
    if-ne p1, v3, :cond_1

    .line 1712
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mBrokenPictureCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    if-nez v1, :cond_0

    .line 1713
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/ResourceManager;->getBrokenPictureThumbnail(Landroid/content/res/Resources;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1714
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v1, v2, v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mBrokenPictureCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 1715
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mBrokenPictureCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->setManualRecycle(Z)V

    .line 1717
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mBrokenPictureCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 1724
    :goto_0
    return-object v1

    .line 1719
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mBrokenMovieCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    if-nez v1, :cond_2

    .line 1720
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->this$0:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/util/ResourceManager;->getBrokenMovieThumbnail(Landroid/content/res/Resources;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1721
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v1, v2, v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mBrokenMovieCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 1722
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mBrokenMovieCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->setManualRecycle(Z)V

    .line 1724
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mBrokenMovieCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    goto :goto_0
.end method

.method public getPitchRate()F
    .locals 1

    .prologue
    .line 1837
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPitchRate:F

    return v0
.end method

.method public getScaleRate()F
    .locals 1

    .prologue
    .line 1841
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mScaleRate:F

    return v0
.end method

.method public resetCount()V
    .locals 5

    .prologue
    .line 1767
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-nez v4, :cond_1

    .line 1768
    const/4 v4, 0x0

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mGroupCount:I

    .line 1788
    :cond_0
    return-void

    .line 1771
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getCount()I

    move-result v2

    .line 1772
    .local v2, "groupCount":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mGroupCom:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;

    if-eqz v4, :cond_2

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mGroupCount:I

    if-eq v4, v2, :cond_3

    .line 1773
    :cond_2
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mGroupCount:I

    .line 1774
    new-array v4, v2, [Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;

    iput-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mGroupCom:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;

    .line 1776
    :cond_3
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v2, :cond_0

    .line 1777
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mGroupCom:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;

    aget-object v1, v4, v3

    .line 1778
    .local v1, "group":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;
    if-nez v1, :cond_4

    .line 1779
    new-instance v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;

    .end local v1    # "group":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;
    invoke-direct {v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;-><init>()V

    .line 1780
    .restart local v1    # "group":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mGroupCom:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;

    aput-object v1, v4, v3

    .line 1782
    :cond_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v4, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getCount(I)I

    move-result v0

    .line 1783
    .local v0, "count":I
    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;->setItemCount(I)V

    .line 1785
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v4, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getItemRatio(I)[B

    move-result-object v4

    iput-object v4, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;->mRatio:[B

    .line 1786
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v4, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getLineCount(I)I

    move-result v4

    iput v4, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;->mLineCount:I

    .line 1776
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public setAdapter(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    .prologue
    .line 1744
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    .line 1745
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPosCtrl1:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    if-eqz v0, :cond_0

    .line 1746
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPosCtrl1:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iput-object p1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    .line 1748
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPosCtrl2:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    if-eqz v0, :cond_1

    .line 1749
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPosCtrl2:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iput-object p1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    .line 1751
    :cond_1
    return-void
.end method

.method public setPitchRate(FZ)V
    .locals 10
    .param p1, "rate"    # F
    .param p2, "force"    # Z

    .prologue
    .line 1795
    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPitchRate:F

    cmpl-float v8, p1, v8

    if-nez v8, :cond_1

    if-nez p2, :cond_1

    .line 1816
    :cond_0
    return-void

    .line 1798
    :cond_1
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPitchRate:F

    .line 1799
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v6, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    .line 1800
    .local v6, "objs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;>;"
    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v4

    .line 1802
    .local v4, "itemObjSize":I
    iget-boolean v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPortraitMode:Z

    if-eqz v8, :cond_2

    .line 1803
    const/high16 v7, 0x41c00000    # 24.0f

    .line 1807
    .local v7, "pitch":F
    :goto_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_0

    .line 1808
    invoke-virtual {v6, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    mul-float v9, v7, p1

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setPitch(F)V

    .line 1809
    invoke-virtual {v6, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getAllChildObject()[Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/android/gallery3d/glcore/GlObject;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_2
    if-ge v3, v5, :cond_4

    aget-object v1, v0, v3

    .line 1810
    .local v1, "childObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    if-nez v1, :cond_3

    .line 1809
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1805
    .end local v0    # "arr$":[Lcom/sec/android/gallery3d/glcore/GlObject;
    .end local v1    # "childObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    .end local v2    # "i":I
    .end local v3    # "i$":I
    .end local v5    # "len$":I
    .end local v7    # "pitch":F
    :cond_2
    const/high16 v7, 0x41400000    # 12.0f

    .restart local v7    # "pitch":F
    goto :goto_0

    .line 1813
    .restart local v0    # "arr$":[Lcom/sec/android/gallery3d/glcore/GlObject;
    .restart local v1    # "childObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    .restart local v2    # "i":I
    .restart local v3    # "i$":I
    .restart local v5    # "len$":I
    :cond_3
    mul-float v8, v7, p1

    invoke-virtual {v1, v8}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPitch(F)V

    goto :goto_3

    .line 1807
    .end local v1    # "childObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public setReferCtrl(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;)V
    .locals 1
    .param p1, "refer1"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
    .param p2, "refer2"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .prologue
    .line 1729
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPosCtrl1:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .line 1730
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPosCtrl2:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .line 1731
    if-eqz p1, :cond_0

    .line 1732
    iput-object p2, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mRefer:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .line 1733
    iput-object p0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    .line 1734
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    iput-object v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    .line 1736
    :cond_0
    if-eqz p2, :cond_1

    .line 1737
    iput-object p1, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mRefer:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .line 1738
    iput-object p0, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    .line 1739
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    iput-object v0, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    .line 1741
    :cond_1
    return-void
.end method

.method public setScaleRate(F)V
    .locals 8
    .param p1, "rate"    # F

    .prologue
    .line 1819
    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mScaleRate:F

    cmpl-float v7, p1, v7

    if-nez v7, :cond_1

    .line 1834
    :cond_0
    return-void

    .line 1822
    :cond_1
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mScaleRate:F

    .line 1823
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v6, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    .line 1824
    .local v6, "objs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;>;"
    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v4

    .line 1825
    .local v4, "itemObjSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v4, :cond_0

    .line 1826
    invoke-virtual {v6, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v7, p1, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setScale(FF)V

    .line 1827
    invoke-virtual {v6, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getAllChildObject()[Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/android/gallery3d/glcore/GlObject;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v5, :cond_3

    aget-object v1, v0, v3

    .line 1828
    .local v1, "childObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    if-nez v1, :cond_2

    .line 1827
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1831
    :cond_2
    invoke-virtual {v1, p1, p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->setScale(FF)V

    goto :goto_2

    .line 1825
    .end local v1    # "childObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public setScrollTopMargine(F)V
    .locals 0
    .param p1, "height"    # F

    .prologue
    .line 1791
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mScrollTopMargine:F

    .line 1792
    return-void
.end method
