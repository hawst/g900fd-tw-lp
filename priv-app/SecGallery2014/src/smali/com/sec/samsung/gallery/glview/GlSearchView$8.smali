.class Lcom/sec/samsung/gallery/glview/GlSearchView$8;
.super Ljava/lang/Object;
.source "GlSearchView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlSearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V
    .locals 0

    .prologue
    .line 502
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$8;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGenericMotionCancel(Lcom/sec/android/gallery3d/glcore/GlObject;)Z
    .locals 2
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/4 v1, 0x2

    .line 514
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$8;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mOnGenericMotionListener:[Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    .line 515
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$8;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mOnGenericMotionListener:[Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$8;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;->onGenericMotionCancel(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    .line 517
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onGenericMotionEnter(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 4
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/4 v3, 0x2

    .line 504
    move-object v0, p1

    check-cast v0, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 505
    .local v0, "baseObj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    iget v1, v0, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 507
    .local v1, "index":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$8;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlSearchView;->mOnGenericMotionListener:[Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;

    aget-object v2, v2, v3

    if-eqz v2, :cond_0

    .line 508
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$8;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlSearchView;->mOnGenericMotionListener:[Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$8;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlSearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-interface {v2, v3, v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;->onGenericMotionEnter(Lcom/sec/android/gallery3d/glcore/GlLayer;I)V

    .line 510
    :cond_0
    const/4 v2, 0x1

    return v2
.end method
