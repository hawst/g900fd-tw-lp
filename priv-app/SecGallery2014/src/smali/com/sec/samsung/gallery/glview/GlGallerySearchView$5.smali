.class Lcom/sec/samsung/gallery/glview/GlGallerySearchView$5;
.super Ljava/lang/Object;
.source "GlGallerySearchView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGallerySearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V
    .locals 0

    .prologue
    .line 444
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$5;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMove(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 1
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "dx"    # I
    .param p3, "dy"    # I

    .prologue
    .line 450
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$5;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-virtual {v0, p2, p3}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->onMoved(II)Z

    move-result v0

    return v0
.end method

.method public onPress(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 1
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 446
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$5;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-virtual {v0, p2, p3}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->onPressed(II)Z

    move-result v0

    return v0
.end method

.method public onRelease(Lcom/sec/android/gallery3d/glcore/GlObject;IIII)Z
    .locals 1
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "dx"    # I
    .param p3, "dy"    # I
    .param p4, "speedX"    # I
    .param p5, "speedY"    # I

    .prologue
    .line 454
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$5;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->onReleased(IIII)Z

    move-result v0

    return v0
.end method
