.class Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$4;
.super Landroid/telephony/PhoneStateListener;
.source "SlideshowPresentationVideoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$4;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 5
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 189
    packed-switch p1, :pswitch_data_0

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 191
    :pswitch_0
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Call state Idle: UnMute the Sound"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$4;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$4;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 193
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$4;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 194
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$4;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mIsOffHookOccured:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$702(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;Z)Z

    goto :goto_0

    .line 197
    :pswitch_1
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Call state Ringing: Mute the Sound"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$4;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mIsOffHookOccured:Z
    invoke-static {v0, v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$702(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;Z)Z

    .line 199
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$4;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$4;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$4;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto :goto_0

    .line 203
    :pswitch_2
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Call state Offhook: Mute the Sound"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$4;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mIsOffHookOccured:Z
    invoke-static {v0, v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$702(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;Z)Z

    .line 205
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$4;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$4;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$4;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto/16 :goto_0

    .line 189
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
