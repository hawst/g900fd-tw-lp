.class public Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$AndroidBeamUtils;
.super Ljava/lang/Object;
.source "AndroidBeamCmd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AndroidBeamUtils"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 469
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertToFileUri(Landroid/content/Context;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contentUri"    # Landroid/net/Uri;

    .prologue
    .line 489
    const/4 v1, 0x0

    .line 491
    .local v1, "uri":Landroid/net/Uri;
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-static {p0, p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getFilePathFrom(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 495
    :goto_0
    return-object v1

    .line 492
    :catch_0
    move-exception v0

    .line 493
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getDummyCloudUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 480
    const-string v0, "http://"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static isBeamSupported(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 1
    .param p0, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 473
    instance-of v0, p0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMmsItem(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 475
    :cond_0
    const/4 v0, 0x0

    .line 476
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isRemoteContents(Landroid/net/Uri;)Z
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 484
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "content://com.sec.android.gallery3d.provider/picasa"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static toUriPathArray(Landroid/content/Context;Ljava/util/List;)[Landroid/net/Uri;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)[",
            "Landroid/net/Uri;"
        }
    .end annotation

    .prologue
    .local p1, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 499
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 501
    .local v3, "uriPathList":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaObject;

    .local v1, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    move-object v4, v1

    .line 502
    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    .line 503
    .local v2, "uri":Landroid/net/Uri;
    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local v1    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$AndroidBeamUtils;->isBeamSupported(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 504
    new-array v4, v5, [Landroid/net/Uri;

    invoke-static {}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$AndroidBeamUtils;->getDummyCloudUri()Landroid/net/Uri;

    move-result-object v5

    aput-object v5, v4, v6

    .line 513
    .end local v2    # "uri":Landroid/net/Uri;
    :goto_1
    return-object v4

    .line 505
    .restart local v2    # "uri":Landroid/net/Uri;
    :cond_0
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$AndroidBeamUtils;->isRemoteContents(Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 507
    new-array v4, v5, [Landroid/net/Uri;

    aput-object v2, v4, v6

    goto :goto_1

    .line 509
    :cond_1
    invoke-static {p0, v2}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$AndroidBeamUtils;->convertToFileUri(Landroid/content/Context;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    .line 510
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 513
    .end local v2    # "uri":Landroid/net/Uri;
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x0

    goto :goto_1

    :cond_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Landroid/net/Uri;

    invoke-interface {v3, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Landroid/net/Uri;

    goto :goto_1
.end method
