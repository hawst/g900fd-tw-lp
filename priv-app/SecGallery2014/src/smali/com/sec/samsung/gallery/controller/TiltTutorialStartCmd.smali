.class public Lcom/sec/samsung/gallery/controller/TiltTutorialStartCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "TiltTutorialStartCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/samsung/gallery/controller/TiltTutorialStartCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/TiltTutorialStartCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method private displayAirMotionPreview(Landroid/os/Bundle;Lcom/sec/android/gallery3d/data/DataManager;)V
    .locals 2
    .param p1, "data"    # Landroid/os/Bundle;
    .param p2, "dm"    # Lcom/sec/android/gallery3d/data/DataManager;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/TiltTutorialStartCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    const-class v1, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 98
    return-void
.end method

.method private displayMotionPreview(Landroid/os/Bundle;Lcom/sec/android/gallery3d/data/DataManager;)V
    .locals 7
    .param p1, "data"    # Landroid/os/Bundle;
    .param p2, "dm"    # Lcom/sec/android/gallery3d/data/DataManager;

    .prologue
    .line 71
    const/4 v1, 0x0

    .line 72
    .local v1, "file":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/TiltTutorialStartCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "DOWNLOADABLE_HELP"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 73
    .local v2, "isDownloadableMode":Ljava/lang/Boolean;
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 74
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/sec/samsung/gallery/view/help/GalleryHelp;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "motion.jpg"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 79
    :goto_0
    sget-object v4, Lcom/sec/samsung/gallery/controller/TiltTutorialStartCmd;->TAG:Ljava/lang/String;

    const-string v5, "MotionDefault"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {p2, v4, v5}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    .line 81
    .local v3, "itemPath":Lcom/sec/android/gallery3d/data/Path;
    if-nez v3, :cond_1

    .line 82
    sget-object v4, Lcom/sec/samsung/gallery/controller/TiltTutorialStartCmd;->TAG:Ljava/lang/String;

    const-string v5, "displayMotionPreview - itemPath is null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    :goto_1
    return-void

    .line 76
    .end local v3    # "itemPath":Lcom/sec/android/gallery3d/data/Path;
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/TiltTutorialStartCmd;->initMotionTestFile()Z

    .line 77
    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/TiltTutorialStartCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/motion_reference.jpg"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 85
    .restart local v3    # "itemPath":Lcom/sec/android/gallery3d/data/Path;
    :cond_1
    invoke-virtual {p2, v3}, Lcom/sec/android/gallery3d/data/DataManager;->getDefaultSetOf(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 87
    .local v0, "albumPath":Lcom/sec/android/gallery3d/data/Path;
    if-eqz v0, :cond_2

    .line 88
    const-string v4, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    :cond_2
    const-string v4, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const-string v4, "from-motiontilt"

    const/4 v5, 0x1

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 93
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/TiltTutorialStartCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    const-class v5, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v4, v5, p1}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method private initMotionTestFile()Z
    .locals 14

    .prologue
    .line 101
    const/4 v7, 0x0

    .line 102
    .local v7, "fos":Ljava/io/FileOutputStream;
    const/4 v3, 0x0

    .line 103
    .local v3, "bos":Ljava/io/BufferedOutputStream;
    const/4 v9, 0x0

    .line 104
    .local v9, "is":Ljava/io/InputStream;
    const/4 v1, 0x0

    .line 106
    .local v1, "bis":Ljava/io/BufferedInputStream;
    iget-object v12, p0, Lcom/sec/samsung/gallery/controller/TiltTutorialStartCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 107
    .local v0, "am":Landroid/content/res/AssetManager;
    new-instance v10, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v13, p0, Lcom/sec/samsung/gallery/controller/TiltTutorialStartCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v13

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/motion_reference.jpg"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 110
    .local v10, "mfile":Ljava/io/File;
    :try_start_0
    const-string v12, "motionReference/motion_reference.jpg"

    invoke-virtual {v0, v12}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v9

    .line 111
    new-instance v2, Ljava/io/BufferedInputStream;

    invoke-direct {v2, v9}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .local v2, "bis":Ljava/io/BufferedInputStream;
    :try_start_1
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_1

    .line 113
    new-instance v8, Ljava/io/FileOutputStream;

    invoke-direct {v8, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 114
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .local v8, "fos":Ljava/io/FileOutputStream;
    :try_start_2
    new-instance v4, Ljava/io/BufferedOutputStream;

    invoke-direct {v4, v8}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 116
    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .local v4, "bos":Ljava/io/BufferedOutputStream;
    const/4 v11, -0x1

    .line 117
    .local v11, "read":I
    const/16 v12, 0x2000

    :try_start_3
    new-array v5, v12, [B

    .line 118
    .local v5, "buffer":[B
    :goto_0
    invoke-virtual {v2, v5}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v11

    const/4 v12, -0x1

    if-eq v11, v12, :cond_0

    .line 119
    const/4 v12, 0x0

    invoke-virtual {v4, v5, v12, v11}, Ljava/io/BufferedOutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_0

    .line 124
    .end local v5    # "buffer":[B
    :catch_0
    move-exception v6

    move-object v1, v2

    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    move-object v3, v4

    .end local v4    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    move-object v7, v8

    .line 125
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .end local v11    # "read":I
    .local v6, "e":Ljava/io/IOException;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :goto_1
    :try_start_4
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 127
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 128
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 129
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 130
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 137
    .end local v6    # "e":Ljava/io/IOException;
    :goto_2
    const/4 v12, 0x1

    return v12

    .line 121
    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v4    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v5    # "buffer":[B
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v11    # "read":I
    :cond_0
    :try_start_5
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->flush()V

    .line 122
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->flush()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move-object v3, v4

    .end local v4    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    move-object v7, v8

    .line 127
    .end local v5    # "buffer":[B
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .end local v11    # "read":I
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :cond_1
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 128
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 129
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 130
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object v1, v2

    .line 131
    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    goto :goto_2

    .line 127
    :catchall_0
    move-exception v12

    :goto_3
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 128
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 129
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 130
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v12

    .line 127
    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v2    # "bis":Ljava/io/BufferedInputStream;
    :catchall_1
    move-exception v12

    move-object v1, v2

    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    goto :goto_3

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v12

    move-object v1, v2

    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v4    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v11    # "read":I
    :catchall_3
    move-exception v12

    move-object v1, v2

    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    move-object v3, v4

    .end local v4    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    move-object v7, v8

    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 124
    .end local v11    # "read":I
    :catch_1
    move-exception v6

    goto :goto_1

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v2    # "bis":Ljava/io/BufferedInputStream;
    :catch_2
    move-exception v6

    move-object v1, v2

    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    goto :goto_1

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v6

    move-object v1, v2

    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    goto :goto_1
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 6
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 42
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/Object;

    move-object v4, v5

    check-cast v4, [Ljava/lang/Object;

    .line 43
    .local v4, "params":[Ljava/lang/Object;
    const/4 v5, 0x0

    aget-object v1, v4, v5

    check-cast v1, Landroid/os/Bundle;

    .line 44
    .local v1, "extras":Landroid/os/Bundle;
    const/4 v5, 0x1

    aget-object v5, v4, v5

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iput-object v5, p0, Lcom/sec/samsung/gallery/controller/TiltTutorialStartCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 46
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/TiltTutorialStartCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryMotion()Lcom/sec/android/gallery3d/app/GalleryMotion;

    move-result-object v2

    .line 47
    .local v2, "galleryMotion":Lcom/sec/android/gallery3d/app/GalleryMotion;
    if-eqz v2, :cond_0

    if-nez v1, :cond_1

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v3

    .line 51
    .local v3, "motionTutorialType":Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    sget-object v5, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->AIR_MOTION_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v3, v5, :cond_2

    sget-object v5, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->AIR_MOTION_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v3, v5, :cond_3

    .line 52
    :cond_2
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryMotion;->setAirMotionTutorialView()V

    .line 53
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 54
    .local v0, "data":Landroid/os/Bundle;
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/TiltTutorialStartCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v5

    invoke-direct {p0, v0, v5}, Lcom/sec/samsung/gallery/controller/TiltTutorialStartCmd;->displayAirMotionPreview(Landroid/os/Bundle;Lcom/sec/android/gallery3d/data/DataManager;)V

    goto :goto_0

    .line 55
    .end local v0    # "data":Landroid/os/Bundle;
    :cond_3
    sget-object v5, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v3, v5, :cond_4

    sget-object v5, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v3, v5, :cond_4

    sget-object v5, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v3, v5, :cond_5

    .line 58
    :cond_4
    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/GalleryMotion;->setMotionHelpView(Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;)V

    .line 59
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 60
    .restart local v0    # "data":Landroid/os/Bundle;
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/TiltTutorialStartCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v5

    invoke-direct {p0, v0, v5}, Lcom/sec/samsung/gallery/controller/TiltTutorialStartCmd;->displayMotionPreview(Landroid/os/Bundle;Lcom/sec/android/gallery3d/data/DataManager;)V

    goto :goto_0

    .line 61
    .end local v0    # "data":Landroid/os/Bundle;
    :cond_5
    sget-object v5, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_NONE:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v3, v5, :cond_0

    .line 62
    sget-object v5, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v3, v5, :cond_6

    sget-object v5, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v3, v5, :cond_6

    .line 64
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryMotion;->setMotionTutorialView()V

    .line 65
    :cond_6
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 66
    .restart local v0    # "data":Landroid/os/Bundle;
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/TiltTutorialStartCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v5

    invoke-direct {p0, v0, v5}, Lcom/sec/samsung/gallery/controller/TiltTutorialStartCmd;->displayMotionPreview(Landroid/os/Bundle;Lcom/sec/android/gallery3d/data/DataManager;)V

    goto :goto_0
.end method
