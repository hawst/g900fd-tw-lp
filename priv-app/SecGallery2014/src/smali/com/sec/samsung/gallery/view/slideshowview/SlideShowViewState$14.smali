.class Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$14;
.super Landroid/os/Handler;
.source "SlideShowViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V
    .locals 0

    .prologue
    .line 1208
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$14;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1211
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1221
    :cond_0
    :goto_0
    return-void

    .line 1213
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$14;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isSlideshowPauseViewEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1214
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$14;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # invokes: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->finishSlideshowPauseView()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$2900(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V

    .line 1215
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$14;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # invokes: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->initSlideshowPauseView()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$3000(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V

    goto :goto_0

    .line 1211
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
