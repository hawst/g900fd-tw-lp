.class public Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarView;
.source "SlideShowSettingsActionbar.java"


# static fields
.field public static misSlideshowRunning:Z


# instance fields
.field mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

.field private mSelectionButton:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->misSlideshowRunning:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 26
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar$1;-><init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mSelectionButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;Landroid/widget/Button;)Landroid/widget/Button;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;
    .param p1, "x1"    # Landroid/widget/Button;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mSelectionButton:Landroid/widget/Button;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method


# virtual methods
.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 53
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 54
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f120038

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 55
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 5
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 59
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 60
    .local v0, "id":I
    packed-switch v0, :pswitch_data_0

    .line 70
    :goto_0
    return-void

    .line 62
    :pswitch_0
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v1, v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v4

    .line 64
    .local v1, "params":[Ljava/lang/Object;
    sput-boolean v4, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->misSlideshowRunning:Z

    .line 65
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "PREPARE_SLIDE_SHOW_DATA"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 60
    nop

    :pswitch_data_0
    .packed-switch 0x7f0f02d9
        :pswitch_0
    .end packed-switch
.end method
