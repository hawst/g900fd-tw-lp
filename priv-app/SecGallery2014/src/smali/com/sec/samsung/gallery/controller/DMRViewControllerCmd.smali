.class public Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "DMRViewControllerCmd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;,
        Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRZoomListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mDMRViewController:Lcom/sec/android/gallery3d/homesync/DMRViewController;

.field private mDMRZoomListener:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRZoomListener;

.field private mIDMRControllerActionHandler:Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 30
    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->mDMRViewController:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    .line 31
    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->mIDMRControllerActionHandler:Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;)Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRZoomListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->mDMRZoomListener:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRZoomListener;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method private startViewController()V
    .locals 2

    .prologue
    .line 53
    invoke-static {}, Lcom/sec/android/gallery3d/homesync/DMRViewController;->GetInstance()Lcom/sec/android/gallery3d/homesync/DMRViewController;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->mDMRViewController:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    .line 54
    new-instance v0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->mIDMRControllerActionHandler:Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;

    .line 78
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->mDMRViewController:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->mDMRViewController:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->mIDMRControllerActionHandler:Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/homesync/DMRViewController;->startViewControllerServer(Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;)V

    .line 81
    :cond_0
    return-void
.end method

.method private stopViewController()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 84
    sget-object v0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->TAG:Ljava/lang/String;

    const-string v1, "PhotoView stopViewControllerServer()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    invoke-static {}, Lcom/sec/android/gallery3d/homesync/DMRViewController;->isDetailViewDMRViewControllerInstance()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    invoke-static {}, Lcom/sec/android/gallery3d/homesync/DMRViewController;->GetInstance()Lcom/sec/android/gallery3d/homesync/DMRViewController;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 87
    invoke-static {}, Lcom/sec/android/gallery3d/homesync/DMRViewController;->GetInstance()Lcom/sec/android/gallery3d/homesync/DMRViewController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/homesync/DMRViewController;->stopViewControllerServer()V

    .line 88
    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->mDMRViewController:Lcom/sec/android/gallery3d/homesync/DMRViewController;

    .line 89
    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->mIDMRControllerActionHandler:Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;

    .line 92
    :cond_0
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 3
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 37
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v2, :cond_0

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-nez v2, :cond_1

    .line 50
    :cond_0
    :goto_0
    return-void

    .line 40
    :cond_1
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Object;

    move-object v1, v2

    check-cast v1, [Ljava/lang/Object;

    .line 41
    .local v1, "params":[Ljava/lang/Object;
    const/4 v2, 0x0

    aget-object v0, v1, v2

    check-cast v0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

    .line 42
    .local v0, "cmdType":Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;
    const/4 v2, 0x1

    aget-object v2, v1, v2

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 43
    const/4 v2, 0x2

    aget-object v2, v1, v2

    check-cast v2, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRZoomListener;

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->mDMRZoomListener:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRZoomListener;

    .line 45
    sget-object v2, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;->START_VIEW_CONTROLLER:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

    if-ne v0, v2, :cond_2

    .line 46
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->startViewController()V

    goto :goto_0

    .line 47
    :cond_2
    sget-object v2, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;->STOP_VIEW_CONTROLLER:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

    if-ne v0, v2, :cond_0

    .line 48
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->stopViewController()V

    goto :goto_0
.end method
