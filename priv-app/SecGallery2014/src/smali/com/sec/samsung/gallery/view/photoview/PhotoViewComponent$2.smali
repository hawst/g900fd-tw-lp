.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;
.super Lorg/puremvc/java/patterns/mediator/Mediator;
.source "PhotoViewComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/Object;

    .prologue
    .line 558
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-direct {p0, p2, p3}, Lorg/puremvc/java/patterns/mediator/Mediator;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public handleNotification(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 18
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 572
    invoke-interface/range {p1 .. p1}, Lorg/puremvc/java/interfaces/INotification;->getName()Ljava/lang/String;

    move-result-object v7

    .line 574
    .local v7, "notiName":Ljava/lang/String;
    const-string v13, "VIEW_MODE_SWITCH"

    invoke-virtual {v7, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 575
    invoke-interface/range {p1 .. p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 576
    .local v2, "direction":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v13}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/app/StateManager;->getPreviousViewState()Ljava/lang/Class;

    move-result-object v11

    .line 577
    .local v11, "previousViewState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    const/4 v13, -0x1

    if-ne v2, v13, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v13, v13, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsNoSplitMode:Z

    if-nez v13, :cond_1

    .line 639
    .end local v2    # "direction":I
    .end local v11    # "previousViewState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    :cond_0
    :goto_0
    return-void

    .line 585
    .restart local v2    # "direction":I
    .restart local v11    # "previousViewState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    :cond_1
    const/16 v13, 0x201

    if-ne v2, v13, :cond_2

    .line 586
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget v14, v14, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaItemIndex:I

    # invokes: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->startDetailViewState(I)V
    invoke-static {v13, v14}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$700(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;I)V

    goto :goto_0

    .line 587
    :cond_2
    const/16 v13, 0x202

    if-ne v2, v13, :cond_3

    .line 590
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget v14, v14, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaItemIndex:I

    # invokes: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->startDetailViewState(I)V
    invoke-static {v13, v14}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$700(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;I)V

    goto :goto_0

    .line 591
    :cond_3
    const/16 v13, 0x203

    if-ne v2, v13, :cond_4

    .line 592
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # invokes: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->startCollageViewer()V
    invoke-static {v13}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V

    goto :goto_0

    .line 594
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v13}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 595
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->exitSelectionMode()V

    goto :goto_0

    .line 597
    .end local v2    # "direction":I
    .end local v11    # "previousViewState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    :cond_5
    const-string v13, "START_NEW_ALBUM_MODE"

    invoke-virtual {v7, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 598
    invoke-interface/range {p1 .. p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 599
    .local v1, "albumName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # invokes: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->enterNewAlbumMode(Ljava/lang/String;)V
    invoke-static {v13, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1000(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Ljava/lang/String;)V

    goto :goto_0

    .line 600
    .end local v1    # "albumName":Ljava/lang/String;
    :cond_6
    const-string v13, "UPDATE_CONFIRM_MENU"

    invoke-virtual {v7, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 601
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateConfirmMenu()V

    goto :goto_0

    .line 602
    :cond_7
    const-string v13, "PREPARE_SLIDE_SHOW_DATA"

    invoke-virtual {v7, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 604
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    const/4 v14, 0x0

    # invokes: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->addToSlideshowProxy(Z)V
    invoke-static {v13, v14}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1100(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Z)V

    goto :goto_0

    .line 605
    :cond_8
    const-string v13, "DOWNLOAD_NEARBY"

    invoke-virtual {v7, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 606
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v13}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v3

    .line 607
    .local v3, "manager":Lcom/sec/android/gallery3d/data/DataManager;
    const-string v13, "nearby"

    invoke-virtual {v3, v13}, Lcom/sec/android/gallery3d/data/DataManager;->getSource(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSource;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    .line 609
    .local v6, "nearbySource":Lcom/sec/android/gallery3d/remote/nearby/NearbySource;
    if-eqz v6, :cond_0

    .line 611
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->getNearbyContext()Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->getNearbyClient()Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v5

    .line 613
    .local v5, "nearbyClient":Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    const-string v12, ""

    .line 614
    .local v12, "serverName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;
    invoke-static {v13}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$100(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget v14, v14, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-virtual {v13, v14}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    .line 615
    .local v4, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v4, :cond_9

    .line 616
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v12

    .line 619
    :cond_9
    invoke-interface/range {p1 .. p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Ljava/lang/Object;

    move-object v10, v13

    check-cast v10, [Ljava/lang/Object;

    .line 621
    .local v10, "params":[Ljava/lang/Object;
    const/4 v13, 0x0

    aget-object v9, v10, v13

    check-cast v9, Ljava/util/ArrayList;

    .line 622
    .local v9, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-virtual {v5, v12, v9}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->download(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 623
    .end local v3    # "manager":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v4    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v5    # "nearbyClient":Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    .end local v6    # "nearbySource":Lcom/sec/android/gallery3d/remote/nearby/NearbySource;
    .end local v9    # "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    .end local v10    # "params":[Ljava/lang/Object;
    .end local v12    # "serverName":Ljava/lang/String;
    :cond_a
    const-string v13, "DOWNLOAD_CLOUDBY"

    invoke-virtual {v7, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_b

    .line 624
    invoke-interface/range {p1 .. p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Ljava/lang/Object;

    move-object v10, v13

    check-cast v10, [Ljava/lang/Object;

    .line 626
    .restart local v10    # "params":[Ljava/lang/Object;
    const/4 v13, 0x0

    aget-object v8, v10, v13

    check-cast v8, Ljava/util/ArrayList;

    .line 627
    .local v8, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v13}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v13

    invoke-static {v13, v8}, Lcom/sec/samsung/gallery/util/DownloadUtil;->downloadEnqueue(Landroid/content/Context;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 628
    .end local v8    # "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v10    # "params":[Ljava/lang/Object;
    :cond_b
    const-string v13, "REFRESH_SELECTION"

    invoke-virtual {v7, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 629
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mHandleDismissDialogsTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;
    invoke-static {v13}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1300(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    move-result-object v13

    if-eqz v13, :cond_c

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mHandleDismissDialogsTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;
    invoke-static {v13}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1300(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v13

    sget-object v14, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v13, v14, :cond_0

    .line 633
    :cond_c
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->tempTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;
    invoke-static {v13}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1400(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    move-result-object v14

    monitor-enter v14

    .line 634
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    new-instance v15, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-direct/range {v15 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;)V

    # setter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mHandleDismissDialogsTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;
    invoke-static {v13, v15}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1302(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;)Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    .line 635
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mHandleDismissDialogsTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;
    invoke-static {v13}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1300(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    move-result-object v13

    const/4 v15, 0x0

    new-array v15, v15, [Ljava/lang/Void;

    invoke-virtual {v13, v15}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 636
    monitor-exit v14

    goto/16 :goto_0

    :catchall_0
    move-exception v13

    monitor-exit v14
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v13
.end method

.method public listNotificationInterests()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 561
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "VIEW_MODE_SWITCH"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "START_NEW_ALBUM_MODE"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "UPDATE_CONFIRM_MENU"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "PREPARE_SLIDE_SHOW_DATA"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "DOWNLOAD_NEARBY"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "DOWNLOAD_CLOUDBY"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "REFRESH_SELECTION"

    aput-object v2, v0, v1

    return-object v0
.end method
