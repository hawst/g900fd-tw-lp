.class public Lcom/sec/samsung/gallery/glview/GlGridView;
.super Lcom/sec/samsung/gallery/glview/GlAbsListView;
.source "GlGridView.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;
.implements Lcom/sec/samsung/gallery/glview/GlReorderHelper$OnDragEndListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/GlGridView$GlDynamicGridBoundaryAnim;,
        Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;,
        Lcom/sec/samsung/gallery/glview/GlGridView$OnGetViewListener;,
        Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;,
        Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;,
        Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;,
        Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;
    }
.end annotation


# static fields
.field private static ALBUM_ITEM_MAX:I = 0x0

.field private static final CONV_ANIM_GRID:I = 0x3

.field private static final CONV_ANIM_PHOTO:I = 0x1

.field private static final CONV_ANIM_RESIZE:I = 0x2

.field private static FIRST_THUMBNAIL_HEIGHT_MULTIPLE:I = 0x0

.field private static FIRST_THUMBNAIL_WIDTH_MULTIPLE:I = 0x0

.field public static final STATUS_ITEM_MOVE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "GlGridView"

.field private static final VISIABLE_POSITION_INIT:I


# instance fields
.field private heightMultiple:I

.field public mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

.field private mClickable:Z

.field public mColCnt:I

.field private mConfig:Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;

.field mConvListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

.field private mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

.field mConvSetListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

.field public mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

.field private mFadeOutAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;

.field private mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

.field private mFocused:I

.field private mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

.field private mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

.field mHoverCtrlListener:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;

.field private mIsDisplayedPenSelectionBox:Z

.field private mIsDragEnabled:Z

.field private mIsFocusVisible:Z

.field private mIsPressed:Z

.field public mLastVisibleFirst:I

.field public mLastVisibleLast:I

.field mListenerAlbumClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

.field mListenerAlbumLongClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

.field private mListenerAlbumMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

.field mListenerConv2:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

.field private mListenerDrop:Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;

.field mListenerHover:Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;

.field mListenerPenSelection:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

.field private mObjEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

.field private mObjEstimatorEx:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

.field private mOnConvAnim:Z

.field mOnGetViewListener:Lcom/sec/samsung/gallery/glview/GlGridView$OnGetViewListener;

.field public mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

.field private mPenSelectionObj:Lcom/sec/android/gallery3d/glcore/GlObject;

.field private mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

.field public mRowCnt:I

.field private mSizeChangedDurationAnimation:Z

.field private mSpreadAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;

.field mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field private mSx:F

.field private mViewMode:I

.field private widthMultiple:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 68
    const/16 v0, 0x23

    sput v0, Lcom/sec/samsung/gallery/glview/GlGridView;->ALBUM_ITEM_MAX:I

    .line 75
    sput v1, Lcom/sec/samsung/gallery/glview/GlGridView;->FIRST_THUMBNAIL_WIDTH_MULTIPLE:I

    .line 76
    sput v1, Lcom/sec/samsung/gallery/glview/GlGridView;->FIRST_THUMBNAIL_HEIGHT_MULTIPLE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 119
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;-><init>(Landroid/content/Context;)V

    .line 81
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    .line 82
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    .line 84
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFadeOutAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;

    .line 85
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSpreadAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;

    .line 86
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .line 87
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    .line 89
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnConvAnim:Z

    .line 92
    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mColCnt:I

    .line 93
    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mRowCnt:I

    .line 94
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mClickable:Z

    .line 96
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSx:F

    .line 98
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnGetViewListener:Lcom/sec/samsung/gallery/glview/GlGridView$OnGetViewListener;

    .line 99
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mConfig:Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;

    .line 100
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mLastVisibleFirst:I

    .line 101
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mLastVisibleLast:I

    .line 108
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mIsPressed:Z

    .line 111
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mIsFocusVisible:Z

    .line 113
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mIsDragEnabled:Z

    .line 640
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGridView$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGridView$1;-><init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mObjEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    .line 655
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGridView$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGridView$2;-><init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mObjEstimatorEx:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    .line 762
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGridView$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGridView$4;-><init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mConvListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

    .line 804
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGridView$5;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGridView$5;-><init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mConvSetListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

    .line 837
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGridView$6;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGridView$6;-><init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mListenerConv2:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 987
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGridView$7;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGridView$7;-><init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mListenerDrop:Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;

    .line 1008
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGridView$8;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGridView$8;-><init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mListenerAlbumMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    .line 1031
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGridView$9;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGridView$9;-><init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mListenerAlbumClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .line 1054
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGridView$10;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGridView$10;-><init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mListenerAlbumLongClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

    .line 1074
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mIsDisplayedPenSelectionBox:Z

    .line 1076
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGridView$11;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGridView$11;-><init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mListenerPenSelection:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

    .line 1148
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGridView$12;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGridView$12;-><init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mListenerHover:Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;

    .line 1179
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGridView$13;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGridView$13;-><init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mHoverCtrlListener:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;

    .line 120
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    const/16 v0, 0x2a

    sput v0, Lcom/sec/samsung/gallery/glview/GlGridView;->ALBUM_ITEM_MAX:I

    .line 123
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;F)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "scroll"    # F

    .prologue
    .line 126
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlGridView;-><init>(Landroid/content/Context;)V

    .line 127
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSx:F

    .line 128
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;FII)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "scroll"    # F
    .param p3, "firstVisible"    # I
    .param p4, "lastVisible"    # I

    .prologue
    .line 131
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlGridView;-><init>(Landroid/content/Context;)V

    .line 132
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSx:F

    .line 133
    iput p3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mLastVisibleFirst:I

    .line 134
    iput p4, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mLastVisibleLast:I

    .line 135
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;FLcom/sec/samsung/gallery/glview/GlBaseAdapter;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "scroll"    # F
    .param p3, "adapter"    # Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .prologue
    .line 138
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlGridView;-><init>(Landroid/content/Context;F)V

    .line 139
    invoke-virtual {p0, p3}, Lcom/sec/samsung/gallery/glview/GlGridView;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V

    .line 140
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;FZ)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "scroll"    # F
    .param p3, "isUpdateUpperBorder"    # Z

    .prologue
    .line 143
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlGridView;-><init>(Landroid/content/Context;F)V

    .line 144
    iput-boolean p3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mUpdateUpperBorder:Z

    .line 145
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/glview/GlGridView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mIsDisplayedPenSelectionBox:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/samsung/gallery/glview/GlGridView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;
    .param p1, "x1"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mIsDisplayedPenSelectionBox:Z

    return p1
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/glview/GlGridView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;
    .param p1, "x1"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnConvAnim:Z

    return p1
.end method

.method static synthetic access$1100()I
    .locals 1

    .prologue
    .line 65
    sget v0, Lcom/sec/samsung/gallery/glview/GlGridView;->ALBUM_ITEM_MAX:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/glview/GlGridView;)[Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/samsung/gallery/glview/GlGridView;Lcom/sec/android/gallery3d/glcore/GlObject;)Lcom/sec/android/gallery3d/glcore/GlObject;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPenSelectionObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFadeOutAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/sec/samsung/gallery/glview/GlGridView;Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;)Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFadeOutAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mConfig:Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSpreadAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/sec/samsung/gallery/glview/GlGridView;Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;)Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSpreadAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/sec/samsung/gallery/glview/GlGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/samsung/gallery/glview/GlGridView;Lcom/sec/android/gallery3d/app/GalleryActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/app/GalleryActivity;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlGridView;->isTabletPickerMode(Lcom/sec/android/gallery3d/app/GalleryActivity;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$202(Lcom/sec/samsung/gallery/glview/GlGridView;Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;)Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/sec/samsung/gallery/glview/GlGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->heightMultiple:I

    return v0
.end method

.method static synthetic access$2200(Lcom/sec/samsung/gallery/glview/GlGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->widthMultiple:I

    return v0
.end method

.method static synthetic access$2302(Lcom/sec/samsung/gallery/glview/GlGridView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;
    .param p1, "x1"    # F

    .prologue
    .line 65
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSx:F

    return p1
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/glview/GlGridView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSizeChangedDurationAnimation:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sec/samsung/gallery/glview/GlGridView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;
    .param p1, "x1"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSizeChangedDurationAnimation:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/glview/GlGridView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;
    .param p1, "x1"    # Z

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlGridView;->loadFolders(Z)V

    return-void
.end method

.method static synthetic access$502(Lcom/sec/samsung/gallery/glview/GlGridView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;
    .param p1, "x1"    # I

    .prologue
    .line 65
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFocused:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlReorderHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/glview/GlGridView;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlGridView;->onDrag(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/glview/GlGridView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mClickable:Z

    return v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/glview/GlGridView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mIsDragEnabled:Z

    return v0
.end method

.method private convAnimEx()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 778
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    array-length v4, v6

    .line 779
    .local v4, "objCount":I
    new-array v0, v4, [Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 780
    .local v0, "glObject":[Lcom/sec/android/gallery3d/glcore/GlObject;
    new-array v3, v4, [I

    .line 781
    .local v3, "indexes":[I
    const/4 v5, 0x0

    .line 782
    .local v5, "validCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v4, :cond_0

    .line 783
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    aget-object v1, v6, v2

    .line 784
    .local v1, "glObjectCur":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemVisibleCnt:I

    if-lt v2, v6, :cond_1

    .line 794
    .end local v1    # "glObjectCur":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mObjEstimatorEx:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    invoke-interface {v6}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;->saveState()V

    .line 795
    new-instance v6, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    .line 796
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mIndexs:[I

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget v10, v10, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCount:I

    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->setSrc([Lcom/sec/android/gallery3d/glcore/GlObject;[ILcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;I)V

    .line 797
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mObjEstimatorEx:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    invoke-virtual {v6, v0, v3, v7, v5}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->setDst([Lcom/sec/android/gallery3d/glcore/GlObject;[ILcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;I)V

    .line 798
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mConvSetListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->setAnimationDoneListener(Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;)V

    .line 799
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->start()V

    .line 800
    iput-boolean v11, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnConvAnim:Z

    .line 801
    const/4 v6, 0x0

    iput v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAnimType:I

    .line 802
    return-void

    .line 786
    .restart local v1    # "glObjectCur":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_1
    iget v6, v1, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemCount:I

    if-ge v6, v7, :cond_2

    .line 787
    invoke-virtual {v1, v11}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setVisibility(Z)V

    .line 788
    aput-object v1, v0, v5

    .line 789
    iget v6, v1, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    aput v6, v3, v5

    .line 790
    add-int/lit8 v5, v5, 0x1

    .line 782
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private convAnimGrid()V
    .locals 3

    .prologue
    .line 825
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    if-eqz v0, :cond_0

    .line 826
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;-><init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFadeOutAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;

    .line 827
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFadeOutAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 828
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFadeOutAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->startAnimation([Lcom/sec/android/gallery3d/glcore/GlObject;I)V

    .line 831
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;-><init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSpreadAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;

    .line 832
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSpreadAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 833
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSpreadAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mListenerConv2:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 834
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSpreadAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->startChange()V

    .line 835
    return-void
.end method

.method private convAnimPhoto()V
    .locals 12

    .prologue
    .line 720
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-nez v7, :cond_0

    .line 760
    :goto_0
    return-void

    .line 729
    :cond_0
    new-instance v7, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;

    invoke-direct {v7, p0}, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;-><init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V

    iput-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFadeOutAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;

    .line 730
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFadeOutAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/glview/GlGridView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 733
    :try_start_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFadeOutAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    const/4 v9, 0x1

    invoke-virtual {v7, v8, v9}, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->startAnimation([Lcom/sec/android/gallery3d/glcore/GlObject;I)V

    .line 735
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    array-length v5, v7

    .line 736
    .local v5, "objCount":I
    new-array v1, v5, [Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 737
    .local v1, "glObject":[Lcom/sec/android/gallery3d/glcore/GlObject;
    new-array v4, v5, [I

    .line 738
    .local v4, "indexes":[I
    const/4 v6, 0x0

    .line 739
    .local v6, "validCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v5, :cond_2

    .line 740
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    aget-object v2, v7, v3

    .line 741
    .local v2, "glObjectCur":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    iget v7, v2, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemCount:I

    if-ge v7, v8, :cond_1

    .line 742
    const/4 v7, 0x1

    invoke-virtual {v2, v7}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setVisibility(Z)V

    .line 743
    aput-object v2, v1, v6

    .line 744
    iget v7, v2, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    aput v7, v4, v6

    .line 745
    add-int/lit8 v6, v6, 0x1

    .line 739
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 748
    .end local v2    # "glObjectCur":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_2
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mObjEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    invoke-interface {v7}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;->saveState()V

    .line 750
    new-instance v7, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    .line 751
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectsEx:[Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mIndexsEx:[I

    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v10, v10, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget v11, v11, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCountEx:I

    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->setSrc([Lcom/sec/android/gallery3d/glcore/GlObject;[ILcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;I)V

    .line 752
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mObjEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    invoke-virtual {v7, v1, v4, v8, v6}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->setDst([Lcom/sec/android/gallery3d/glcore/GlObject;[ILcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;I)V

    .line 753
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mConvSetListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

    invoke-virtual {v7, v8}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->setAnimationDoneListener(Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;)V

    .line 754
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->start()V

    .line 755
    const/4 v7, 0x0

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAnimType:I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 757
    .end local v1    # "glObject":[Lcom/sec/android/gallery3d/glcore/GlObject;
    .end local v3    # "i":I
    .end local v4    # "indexes":[I
    .end local v5    # "objCount":I
    .end local v6    # "validCount":I
    :catch_0
    move-exception v0

    .line 758
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method private createRectangles()[Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .locals 14

    .prologue
    const/4 v13, 0x1

    .line 520
    sget v1, Lcom/sec/samsung/gallery/glview/GlGridView;->ALBUM_ITEM_MAX:I

    new-array v8, v1, [Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 521
    .local v8, "glThumbSet":[Lcom/sec/samsung/gallery/glview/GlThumbObject;
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 522
    .local v7, "dimensionUtil":Lcom/sec/samsung/gallery/util/DimensionUtil;
    iget v12, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I

    .line 524
    .local v12, "viewMode":I
    invoke-virtual {v7, v12}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewThumbnailWidthPixel(I)I

    move-result v3

    .line 525
    .local v3, "thumbWidth":I
    invoke-virtual {v7, v12}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewThumbnailHeightPixel(I)I

    move-result v4

    .line 526
    .local v4, "thumbHeight":I
    invoke-virtual {v7, v12}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNameTextboxWidthPixel(I)I

    move-result v5

    .line 527
    .local v5, "textWidth":I
    invoke-virtual {v7, v12}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNameTextboxHeightPixel(I)I

    move-result v6

    .line 528
    .local v6, "textHeight":I
    const/4 v9, 0x0

    .local v9, "i":I
    sget v10, Lcom/sec/samsung/gallery/glview/GlGridView;->ALBUM_ITEM_MAX:I

    .local v10, "n":I
    :goto_0
    if-ge v9, v10, :cond_1

    .line 529
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-direct/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;-><init>(Lcom/sec/samsung/gallery/glview/GlReorderHelper;Lcom/sec/android/gallery3d/glcore/GlLayer;IIII)V

    .line 530
    .local v0, "glThumb":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    aput-object v0, v8, v9

    .line 532
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mListenerAlbumMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V

    .line 533
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mListenerAlbumClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)V

    .line 534
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mListenerAlbumLongClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setLongClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;)V

    .line 535
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mListenerHover:Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setHoverListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;)V

    .line 536
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mListenerGenericMotion:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setGenericMotionListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;)V

    .line 537
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mListenerDrop:Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setDragListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;)V

    .line 538
    invoke-virtual {v0, v13}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setTouchAnimEnable(Z)V

    .line 539
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mListenerPenSelection:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setPenSelectListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;)V

    .line 541
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v11

    .line 542
    .local v11, "txtObj":Lcom/sec/samsung/gallery/glview/GlTextObject;
    if-eqz v11, :cond_0

    .line 543
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mListenerPenSelection:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

    invoke-virtual {v11, v1}, Lcom/sec/samsung/gallery/glview/GlTextObject;->setPenSelectListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;)V

    .line 528
    :cond_0
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 545
    .end local v0    # "glThumb":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .end local v11    # "txtObj":Lcom/sec/samsung/gallery/glview/GlTextObject;
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    invoke-virtual {v1, v3, v4, v5, v6}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->initDragObject(IIII)V

    .line 546
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->getDragObject()Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mListenerAlbumMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V

    .line 547
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->getDragObject()Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mListenerAlbumClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)V

    .line 548
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->getDragObject()Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mListenerAlbumLongClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setLongClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;)V

    .line 549
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->getDragObject()Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mListenerDrop:Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setDragListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;)V

    .line 550
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->getDragObject()Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v1

    invoke-virtual {v1, v13}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setTouchAnimEnable(Z)V

    .line 551
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->getScrollBar()Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->moveToLast()V

    .line 552
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->getScrollBar()Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mListenerPenSelection:Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->setPenSelectListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;)V

    .line 553
    return-object v8
.end method

.method private freeRectangles()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 558
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-nez v1, :cond_0

    .line 567
    :goto_0
    return-void

    .line 562
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 563
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->remove()V

    .line 564
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    aput-object v2, v1, v0

    .line 562
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 566
    :cond_1
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    goto :goto_0
.end method

.method private getHorizontalCount()V
    .locals 2

    .prologue
    .line 1214
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewColumnItemCount(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mColCnt:I

    .line 1215
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewRowItemCount(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mRowCnt:I

    .line 1216
    return-void
.end method

.method private isTabletPickerMode(Lcom/sec/android/gallery3d/app/GalleryActivity;)Z
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/GalleryActivity;

    .prologue
    .line 2070
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v0, v1, Landroid/content/res/Configuration;->orientation:I

    .line 2071
    .local v0, "o":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2072
    const/4 v1, 0x0

    .line 2074
    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTabletPickerMode(Landroid/content/Intent;)Z

    move-result v1

    goto :goto_0
.end method

.method private loadFolders(Z)V
    .locals 5
    .param p1, "resizing"    # Z

    .prologue
    .line 859
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    .line 860
    .local v0, "albumScroller":Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    if-nez v2, :cond_1

    .line 878
    :cond_0
    :goto_0
    return-void

    .line 863
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v1

    .line 865
    .local v1, "count":I
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAnimType:I

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mInitVisible:Z

    .line 866
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->reset()V

    .line 868
    if-eqz p1, :cond_3

    .line 869
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFocused:I

    invoke-virtual {v0, v2, v1}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->resetSet(II)V

    .line 874
    :goto_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    const/4 v3, 0x0

    iget v4, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScrollRange:F

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setRange(FF)V

    .line 875
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    iget v3, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScroll:F

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 876
    if-lez v1, :cond_0

    .line 877
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget v3, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mVisibleFirst:I

    iget v4, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mVisibleLast:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->updateVisiableRange(II)V

    goto :goto_0

    .line 865
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 871
    :cond_3
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSx:F

    iget v3, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemGapH:F

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget v3, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemHVisibleCnt:I

    mul-int/2addr v2, v3

    invoke-virtual {v0, v2, v1}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->resetSet(II)V

    goto :goto_2
.end method

.method private onDrag(II)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 2053
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mIsPressed:Z

    .line 2055
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->isScreenLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2056
    const/4 v0, 0x1

    .line 2059
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->onDrag(II)Z

    move-result v0

    goto :goto_0
.end method

.method private popObjects(II)V
    .locals 2
    .param p1, "visibleFirst"    # I
    .param p2, "visibleLast"    # I

    .prologue
    const/4 v1, 0x0

    .line 498
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->restoreStatus()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    .line 499
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    if-eqz v0, :cond_3

    .line 500
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mSelectedEx:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFocused:I

    .line 501
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFocused:I

    if-lt v0, p1, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFocused:I

    if-le v0, p2, :cond_1

    .line 502
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFocused:I

    iput v1, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScrollStep:I

    .line 503
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFocused:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemHVisibleCnt:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemGapH:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSx:F

    .line 505
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mViewType:Ljava/lang/String;

    const-class v1, Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 507
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAnimType:I

    .line 517
    :goto_0
    return-void

    .line 510
    :cond_2
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAnimType:I

    goto :goto_0

    .line 514
    :cond_3
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFocused:I

    .line 515
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAnimType:I

    goto :goto_0
.end method

.method private pushCurrentListInfo()V
    .locals 7

    .prologue
    .line 575
    :try_start_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mVisibleLast:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mVisibleFirst:I

    sub-int/2addr v4, v5

    add-int/lit8 v3, v4, 0x1

    .line 576
    .local v3, "visibleCount":I
    if-gtz v3, :cond_0

    .line 577
    const-string v4, "GlGridView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "pushCurrentListInfo visibleCount = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", First = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mVisibleFirst:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", Last = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mVisibleLast:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 606
    .end local v3    # "visibleCount":I
    :goto_0
    return-void

    .line 581
    .restart local v3    # "visibleCount":I
    :cond_0
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    invoke-direct {v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;-><init>()V

    .line 582
    .local v2, "listInfo":Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;
    const-class v4, Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mViewType:Ljava/lang/String;

    .line 583
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mVisibleFirst:I

    iput v4, v2, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mFirst:I

    .line 584
    iput v3, v2, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCount:I

    .line 585
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFocused:I

    iput v4, v2, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mSelected:I

    .line 586
    new-array v4, v3, [I

    iput-object v4, v2, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mIndexs:[I

    .line 587
    new-array v4, v3, [Lcom/sec/android/gallery3d/glcore/GlObject;

    iput-object v4, v2, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 588
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_2

    .line 589
    iget-object v4, v2, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mIndexs:[I

    iget v5, v2, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mFirst:I

    add-int/2addr v5, v1

    aput v5, v4, v1

    .line 590
    iget-object v4, v2, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v5, v2, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mFirst:I

    add-int/2addr v5, v1

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/glview/GlGridView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v5

    aput-object v5, v4, v1

    .line 591
    iget-object v4, v2, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v4, v4, v1

    if-eqz v4, :cond_1

    iget-object v4, v2, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v4, v4, v1

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlObject;->getChildCount()I

    move-result v4

    if-lez v4, :cond_1

    .line 592
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v5, v2, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v5, v5, v1

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/glcore/GlObject;->getAllChildObject()[Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/glcore/GlRootView;->addObjectNotToDetach([Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 588
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 595
    :cond_2
    const/4 v1, 0x0

    :goto_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    array-length v4, v4

    if-ge v1, v4, :cond_4

    .line 596
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    aget-object v4, v4, v1

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mAttr:I

    sget v5, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    and-int/2addr v4, v5

    if-nez v4, :cond_3

    .line 597
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    aget-object v4, v4, v1

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->remove()V

    .line 595
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 599
    :cond_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mObjEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    iput-object v4, v2, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    .line 600
    iget-object v4, v2, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    invoke-interface {v4}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;->saveState()V

    .line 601
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v5, v2, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/glcore/GlRootView;->addObjectNotToDetach([Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 602
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->saveStatus(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 603
    .end local v1    # "i":I
    .end local v2    # "listInfo":Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;
    .end local v3    # "visibleCount":I
    :catch_0
    move-exception v0

    .line 604
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private reload()V
    .locals 8

    .prologue
    .line 440
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-nez v1, :cond_0

    .line 495
    :goto_0
    return-void

    .line 443
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 444
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->freeRectangles()V

    .line 445
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->updateVisiableRange(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 492
    :catch_0
    move-exception v0

    .line 493
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 449
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    instance-of v1, v1, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v1, v2, :cond_2

    .line 458
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->setAlbumViewMode(I)V

    .line 459
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->setColumn()V

    .line 468
    :cond_2
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnConvAnim:Z

    if-eqz v1, :cond_3

    .line 469
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSizeChangedDurationAnimation:Z

    .line 472
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-nez v1, :cond_4

    .line 473
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->createRectangles()[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 476
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    if-eqz v1, :cond_5

    .line 477
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->reset()V

    .line 478
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->register([Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 479
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    const/4 v2, 0x0

    iput v2, v1, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScrollStep:I

    .line 482
    :cond_5
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->loadFolders(Z)V

    .line 484
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAnimType:I

    if-lez v1, :cond_6

    .line 485
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnConvAnim:Z

    .line 486
    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->removeCommand(I)V

    .line 487
    const/4 v2, 0x5

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0xc8

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, Lcom/sec/samsung/gallery/glview/GlGridView;->setCommandDelayed(IIIIJ)V

    .line 491
    :goto_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->getGlRoot()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    goto :goto_0

    .line 489
    :cond_6
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mLockPos:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private remainObjectForLayerAnimation()V
    .locals 5

    .prologue
    .line 216
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mVisibleLast:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mVisibleFirst:I

    sub-int/2addr v3, v4

    add-int/lit8 v2, v3, 0x1

    .line 217
    .local v2, "visibleCount":I
    new-array v1, v2, [Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 219
    .local v1, "objList":[Lcom/sec/android/gallery3d/glcore/GlObject;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 220
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mVisibleFirst:I

    add-int/2addr v3, v0

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/GlGridView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v3

    aput-object v3, v1, v0

    .line 219
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 222
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->addObjectNotToDetach([Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 223
    return-void
.end method

.method private setScaleFactor(ZI)V
    .locals 0
    .param p1, "wide"    # Z
    .param p2, "colCount"    # I

    .prologue
    .line 1219
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->getHorizontalCount()V

    .line 1220
    return-void
.end method

.method private setThumbImageFlingSpeed(I)V
    .locals 2
    .param p1, "speed"    # I

    .prologue
    .line 1302
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->startFling(F)V

    .line 1303
    return-void
.end method

.method private updateAll()V
    .locals 1

    .prologue
    .line 908
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->update()V

    .line 909
    return-void
.end method


# virtual methods
.method public attachScrollBar(Lcom/sec/samsung/gallery/glview/GlScroller;)V
    .locals 6
    .param p1, "scroller"    # Lcom/sec/samsung/gallery/glview/GlScroller;

    .prologue
    const/4 v5, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    .line 1876
    if-nez p1, :cond_0

    .line 1881
    :goto_0
    return-void

    .line 1879
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;

    move-object v1, p0

    move-object v2, p0

    move v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;-><init>(Lcom/sec/samsung/gallery/glview/GlGridView;Lcom/sec/android/gallery3d/glcore/GlLayer;FFI)V

    .line 1880
    .local v0, "scrollBar":Lcom/sec/samsung/gallery/glview/GlScrollBar;
    invoke-virtual {p1, v0, v5}, Lcom/sec/samsung/gallery/glview/GlScroller;->setScrollBarObject(Lcom/sec/samsung/gallery/glview/GlScrollBar;Z)V

    goto :goto_0
.end method

.method protected doResize()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1223
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 1224
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->pushCurrentListInfo()V

    .line 1225
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->restoreStatus()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    .line 1227
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    if-nez v0, :cond_0

    .line 1246
    :goto_0
    return-void

    .line 1232
    :cond_0
    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAnimType:I

    .line 1233
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;

    if-eqz v0, :cond_1

    .line 1234
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mWideMode:Z

    if-eqz v0, :cond_2

    .line 1235
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mColCnt:I

    invoke-interface {v0, v1, v3, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;->onStatusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V

    .line 1239
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->createRectangles()[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 1240
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->reset()V

    .line 1241
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->register([Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 1243
    invoke-direct {p0, v3}, Lcom/sec/samsung/gallery/glview/GlGridView;->loadFolders(Z)V

    .line 1244
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->convAnimEx()V

    .line 1245
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->hideScrollBarImmediately()V

    goto :goto_0

    .line 1237
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mColCnt:I

    invoke-interface {v0, v1, v4, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;->onStatusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V

    goto :goto_1
.end method

.method public getAlbumViewMode()I
    .locals 1

    .prologue
    .line 1390
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I

    return v0
.end method

.method protected getColCntThreshold()[F
    .locals 2

    .prologue
    .line 2040
    const/4 v1, 0x4

    new-array v0, v1, [F

    fill-array-data v0, :array_0

    .line 2041
    .local v0, "threshold":[F
    return-object v0

    .line 2040
    nop

    :array_0
    .array-data 4
        0x3e4ccccd    # 0.2f
        0x3ecccccd    # 0.4f
        0x3f19999a    # 0.6f
        0x3f4ccccd    # 0.8f
    .end array-data
.end method

.method public getFirstVisiblePosition()I
    .locals 1

    .prologue
    .line 1323
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    if-eqz v0, :cond_0

    .line 1324
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mVisibleFirst:I

    .line 1326
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLastVisiblePosition()I
    .locals 1

    .prologue
    .line 1330
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    if-eqz v0, :cond_0

    .line 1331
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mVisibleLast:I

    .line 1333
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getObjectIndex(I)Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .locals 4
    .param p1, "index"    # I

    .prologue
    const/4 v2, 0x0

    .line 673
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-nez v3, :cond_1

    move-object v0, v2

    .line 681
    :cond_0
    :goto_0
    return-object v0

    .line 676
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemVisibleCnt:I

    if-ge v1, v3, :cond_3

    .line 677
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    aget-object v0, v3, v1

    .line 678
    .local v0, "glObject":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    if-eqz v0, :cond_2

    iget v3, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    if-eq v3, p1, :cond_0

    .line 676
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v0    # "glObject":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_3
    move-object v0, v2

    .line 681
    goto :goto_0
.end method

.method public getScroll()F
    .locals 1

    .prologue
    .line 685
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSx:F

    return v0
.end method

.method public getScrollBar(Lcom/sec/samsung/gallery/glview/GlScroller;)Lcom/sec/samsung/gallery/glview/GlScrollBar;
    .locals 1
    .param p1, "scroller"    # Lcom/sec/samsung/gallery/glview/GlScroller;

    .prologue
    .line 1890
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->getScrollBar(Lcom/sec/samsung/gallery/glview/GlScroller;)Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v0

    return-object v0
.end method

.method public isDragEnabled()Z
    .locals 1

    .prologue
    .line 2049
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mIsDragEnabled:Z

    return v0
.end method

.method protected onCommand(ILjava/lang/Object;III)V
    .locals 3
    .param p1, "cmd"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I
    .param p5, "parm3"    # I

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x1

    .line 881
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->isCreated()Z

    move-result v0

    if-nez v0, :cond_1

    .line 899
    :cond_0
    :goto_0
    return-void

    .line 885
    :cond_1
    if-ne p1, v2, :cond_2

    .line 886
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->updateAll()V

    goto :goto_0

    .line 887
    :cond_2
    if-ne p1, v1, :cond_3

    .line 888
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->reload()V

    goto :goto_0

    .line 889
    :cond_3
    const/4 v0, 0x4

    if-ne p1, v0, :cond_4

    .line 890
    invoke-virtual {p0, p3}, Lcom/sec/samsung/gallery/glview/GlGridView;->updateItem(I)V

    goto :goto_0

    .line 891
    :cond_4
    const/4 v0, 0x5

    if-ne p1, v0, :cond_6

    .line 892
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAnimType:I

    if-ne v0, v1, :cond_5

    .line 893
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->convAnimPhoto()V

    goto :goto_0

    .line 894
    :cond_5
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAnimType:I

    if-ne v0, v2, :cond_0

    .line 895
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->convAnimGrid()V

    goto :goto_0

    .line 897
    :cond_6
    invoke-super/range {p0 .. p5}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->onCommand(ILjava/lang/Object;III)V

    goto :goto_0
.end method

.method protected onCreate()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 149
    new-instance v1, Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    invoke-direct {v1, p0, p0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/samsung/gallery/glview/GlReorderHelper$OnDragEndListener;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    .line 151
    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/glview/GlGridView;->setClearByColor(Z)V

    .line 152
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->resetLayout()V

    .line 153
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 154
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    if-nez v1, :cond_0

    .line 155
    new-instance v1, Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 157
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mWidthSpace:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mHeightSpace:F

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->getHeight()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->setDimensionUtil(FFII)V

    .line 158
    const/4 v0, 0x0

    .line 159
    .local v0, "mode":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 160
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v1, v2, :cond_1

    .line 161
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    const-string v2, "peopleViewMode"

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewDefaultMode()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .line 165
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->setAlbumViewMode(I)V

    .line 166
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->getHorizontalCount()V

    .line 168
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->freeRectangles()V

    .line 169
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;->get(Landroid/content/Context;)Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mConfig:Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;

    .line 171
    new-instance v1, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;-><init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    .line 172
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->attachScrollBar(Lcom/sec/samsung/gallery/glview/GlScroller;)V

    .line 173
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->createRectangles()[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 174
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->register([Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 175
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->reset()V

    .line 177
    new-instance v1, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    .line 178
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setDuration(J)V

    .line 179
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v1, p0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setBoundaryAnimationListener(Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;)V

    .line 180
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 182
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->setScroller(Lcom/sec/samsung/gallery/glview/GlScroller;)V

    .line 183
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->setFlingAnim(Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;)V

    .line 185
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mLastVisibleFirst:I

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mLastVisibleLast:I

    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlGridView;->popObjects(II)V

    .line 186
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd;->initMultiCore(Landroid/content/Context;)V

    .line 187
    new-instance v1, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mHoverCtrlListener:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;

    invoke-direct {v1, v2, p0, v3, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;Z)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    .line 188
    new-instance v1, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    .line 189
    return-void

    .line 163
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    const-string v2, "albumViewMode"

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewDefaultMode()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 193
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    array-length v1, v1

    if-nez v1, :cond_1

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mTransitionAnimationType:I

    if-ltz v1, :cond_3

    .line 198
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->remainObjectForLayerAnimation()V

    .line 199
    new-instance v1, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mTransitionAnimationType:I

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;-><init>(I)V

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->setPendingLayerAnimation(Lcom/sec/android/gallery3d/glcore/GlPendingLayerAnimation;)V

    .line 206
    :goto_1
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlags:I

    .line 207
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 208
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    .line 209
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->getScrollBar(Lcom/sec/samsung/gallery/glview/GlScroller;)Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v0

    .line 210
    .local v0, "sb":Lcom/sec/samsung/gallery/glview/GlScrollBar;
    if-eqz v0, :cond_2

    .line 211
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->remove()V

    .line 212
    :cond_2
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/GlGridView;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V

    goto :goto_0

    .line 200
    .end local v0    # "sb":Lcom/sec/samsung/gallery/glview/GlScrollBar;
    :cond_3
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlags:I

    sget v2, Lcom/sec/samsung/gallery/glview/GlGridView;->SAVE_OBJINFO_FORANIM:I

    and-int/2addr v1, v2

    if-lez v1, :cond_4

    .line 201
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->pushCurrentListInfo()V

    goto :goto_1

    .line 203
    :cond_4
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->freeRectangles()V

    goto :goto_1
.end method

.method public onDragEvent(Landroid/view/DragEvent;)Z
    .locals 9
    .param p1, "event"    # Landroid/view/DragEvent;

    .prologue
    const/4 v4, 0x0

    .line 227
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnDropItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnDropItemClickListener;

    if-nez v5, :cond_1

    .line 245
    :cond_0
    :goto_0
    return v4

    .line 230
    :cond_1
    invoke-virtual {p1}, Landroid/view/DragEvent;->getClipData()Landroid/content/ClipData;

    move-result-object v0

    .line 231
    .local v0, "clipData":Landroid/content/ClipData;
    if-eqz v0, :cond_0

    .line 234
    invoke-virtual {p1}, Landroid/view/DragEvent;->getClipDescription()Landroid/content/ClipDescription;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ClipDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v5

    const-string v6, "cropUri"

    invoke-virtual {v5, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 235
    invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I

    move-result v1

    .line 236
    .local v1, "count":I
    if-lez v1, :cond_0

    .line 237
    const/4 v2, 0x0

    .line 238
    .local v2, "index":I
    invoke-virtual {v0, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v3

    .line 239
    .local v3, "item":Landroid/content/ClipData$Item;
    invoke-virtual {v3}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 240
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnDropItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnDropItemClickListener;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v6, 0x0

    const/4 v7, -0x1

    invoke-virtual {v3}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v8

    invoke-interface {v4, v5, v6, v7, v8}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnDropItemClickListener;->onDropItemClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;ILandroid/net/Uri;)V

    .line 241
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public onDragFinished(I)V
    .locals 3
    .param p1, "combinedIndex"    # I

    .prologue
    .line 2064
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;

    if-eqz v0, :cond_0

    .line 2065
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v2, 0x4

    invoke-interface {v0, v1, v2, p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;->onStatusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V

    .line 2067
    :cond_0
    return-void
.end method

.method public onFlingEnd(F)V
    .locals 3
    .param p1, "delta"    # F

    .prologue
    const/4 v2, 0x0

    .line 1201
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    if-nez v0, :cond_0

    .line 1211
    :goto_0
    return-void

    .line 1204
    :cond_0
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSx:F

    .line 1205
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    if-eqz v0, :cond_1

    .line 1206
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSx:F

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->setScroll(F)V

    .line 1207
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->setAlbumPitchRate(F)V

    .line 1209
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v2, v2, v2}, Lcom/sec/samsung/gallery/glview/GlGridView;->setCommand(IIII)V

    .line 1210
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->hideScrollBar()V

    goto :goto_0
.end method

.method public onFlingProcess(FF)V
    .locals 2
    .param p1, "delta"    # F
    .param p2, "elastic"    # F

    .prologue
    .line 1193
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSx:F

    .line 1194
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScrollRange:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 1195
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSx:F

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->setScroll(F)V

    .line 1196
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    neg-float v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->setAlbumPitchRate(F)V

    .line 1197
    return-void
.end method

.method protected onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 258
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->isSlideShowMode:Z

    if-eqz v3, :cond_1

    .line 319
    :cond_0
    :goto_0
    return v1

    .line 260
    :cond_1
    const/16 v3, 0xa8

    if-eq p1, v3, :cond_2

    const/16 v3, 0xa9

    if-eq p1, v3, :cond_2

    const/16 v3, 0x118

    if-eq p1, v3, :cond_2

    const/16 v3, 0x117

    if-ne p1, v3, :cond_4

    :cond_2
    move v0, v2

    .line 262
    .local v0, "isZoomKey":Z
    :goto_1
    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->isScreenLocked()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    if-eqz v3, :cond_5

    :cond_3
    move v1, v2

    .line 263
    goto :goto_0

    .end local v0    # "isZoomKey":Z
    :cond_4
    move v0, v1

    .line 260
    goto :goto_1

    .line 266
    .restart local v0    # "isZoomKey":Z
    :cond_5
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->isScreenLocked()Z

    move-result v3

    if-nez v3, :cond_0

    .line 269
    const/16 v3, 0x42

    if-eq p1, v3, :cond_6

    const/16 v3, 0x17

    if-ne p1, v3, :cond_7

    .line 270
    :cond_6
    invoke-virtual {p2}, Landroid/view/KeyEvent;->startTracking()V

    move v1, v2

    .line 271
    goto :goto_0

    .line 274
    :cond_7
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    if-nez v3, :cond_8

    move v1, v2

    .line 275
    goto :goto_0

    .line 278
    :cond_8
    sparse-switch p1, :sswitch_data_0

    .line 315
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnKeyListenner:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;

    if-eqz v2, :cond_0

    .line 316
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnKeyListenner:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    invoke-interface {v1, p1, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;->onKeyEvent(II)Z

    move-result v1

    goto :goto_0

    .line 280
    :sswitch_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 281
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 282
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mIsFocusVisible:Z

    .line 283
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->jumpTo(I)Z

    move-result v1

    goto :goto_0

    .line 285
    :sswitch_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 286
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 287
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mIsFocusVisible:Z

    .line 288
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->jumpTo(I)Z

    move-result v1

    goto/16 :goto_0

    .line 290
    :sswitch_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 291
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 292
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->heightMultiple:I

    if-le v3, v2, :cond_9

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemFocused:I

    if-nez v3, :cond_9

    .line 293
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    const/4 v3, -0x1

    iput v3, v2, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemFocused:I

    .line 294
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mIsFocusVisible:Z

    goto/16 :goto_0

    .line 297
    :cond_9
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mIsFocusVisible:Z

    .line 298
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->jumpTo(I)Z

    move-result v1

    goto/16 :goto_0

    .line 300
    :sswitch_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 301
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 302
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mIsFocusVisible:Z

    .line 303
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->jumpTo(I)Z

    move-result v1

    goto/16 :goto_0

    :sswitch_4
    move v1, v2

    .line 308
    goto/16 :goto_0

    :sswitch_5
    move v1, v2

    .line 313
    goto/16 :goto_0

    .line 278
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_2
        0x14 -> :sswitch_3
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0xa8 -> :sswitch_4
        0xa9 -> :sswitch_5
        0x117 -> :sswitch_5
        0x118 -> :sswitch_4
    .end sparse-switch
.end method

.method protected onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 358
    const/16 v0, 0xa8

    if-eq p1, v0, :cond_0

    const/16 v0, 0xa9

    if-ne p1, v0, :cond_1

    .line 359
    :cond_0
    const/4 v0, 0x1

    .line 368
    :goto_0
    return v0

    .line 362
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->isScreenLocked()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 363
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 365
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnKeyListenner:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;

    if-eqz v0, :cond_4

    .line 366
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnKeyListenner:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;

    const/16 v1, 0x80

    invoke-interface {v0, p1, v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;->onKeyEvent(II)Z

    move-result v0

    goto :goto_0

    .line 368
    :cond_4
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 324
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->isSlideShowMode:Z

    if-eqz v3, :cond_1

    .line 353
    :cond_0
    :goto_0
    return v1

    .line 326
    :cond_1
    const/16 v3, 0xa8

    if-eq p1, v3, :cond_2

    const/16 v3, 0xa9

    if-eq p1, v3, :cond_2

    const/16 v3, 0x118

    if-eq p1, v3, :cond_2

    const/16 v3, 0x117

    if-ne p1, v3, :cond_3

    :cond_2
    move v1, v2

    .line 328
    goto :goto_0

    .line 331
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->isScreenLocked()Z

    move-result v3

    if-nez v3, :cond_0

    .line 335
    const/16 v3, 0x42

    if-eq p1, v3, :cond_4

    const/16 v3, 0x17

    if-ne p1, v3, :cond_6

    .line 336
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemFocused:I

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFocused:I

    .line 337
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFocused:I

    if-ltz v3, :cond_5

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFocused:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemCount:I

    if-ge v3, v4, :cond_5

    .line 338
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;

    if-eqz v3, :cond_5

    .line 339
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFocused:I

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->getThumbByIndex(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v0

    .line 340
    .local v0, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 341
    if-eqz v0, :cond_0

    .line 342
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v5

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFocused:I

    invoke-interface {v3, v4, v5, v6}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;->onItemClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 343
    const/4 v3, 0x4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFocused:I

    invoke-virtual {p0, v3, v4, v1, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->setCommand(IIII)V

    .end local v0    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_5
    move v1, v2

    .line 346
    goto :goto_0

    .line 348
    :cond_6
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mIsFocusVisible:Z

    if-nez v2, :cond_7

    const/16 v2, 0x15

    if-eq p1, v2, :cond_8

    :cond_7
    const/16 v2, 0x16

    if-eq p1, v2, :cond_8

    const/16 v2, 0x13

    if-eq p1, v2, :cond_8

    const/16 v2, 0x14

    if-ne p1, v2, :cond_9

    .line 350
    :cond_8
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mIsFocusVisible:Z

    .line 351
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemFocused:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->onFocusChange(I)V

    .line 353
    :cond_9
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto/16 :goto_0
.end method

.method protected onMoved(II)Z
    .locals 4
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    const/4 v3, 0x1

    .line 937
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->isScreenLocked()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mLockFlingAtMaxZoomLevel:Z

    if-eqz v0, :cond_1

    .line 960
    :cond_0
    :goto_0
    return v3

    .line 958
    :cond_1
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v1, 0x2d

    if-le v0, v1, :cond_2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mClickable:Z

    .line 959
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    neg-int v2, p2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWorldCoordinateValue(F)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setMovement(F)V

    goto :goto_0
.end method

.method protected onPressed(II)Z
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v2, 0x1

    .line 913
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    if-eqz v0, :cond_0

    .line 914
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->exitFromHoverView(Z)V

    .line 916
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->isScreenLocked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 932
    :goto_0
    return v2

    .line 925
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd;->useMultiCore(Landroid/content/Context;)V

    .line 926
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 927
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 930
    :cond_2
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mClickable:Z

    .line 931
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mIsPressed:Z

    goto :goto_0
.end method

.method protected onReleased(IIII)Z
    .locals 3
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "speedX"    # I
    .param p4, "speedY"    # I

    .prologue
    const/4 v2, 0x1

    .line 970
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mIsPressed:Z

    .line 971
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->isScreenLocked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 984
    :cond_0
    :goto_0
    return v2

    .line 975
    :cond_1
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mLockFlingAtMaxZoomLevel:Z

    if-eqz v0, :cond_2

    .line 977
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    if-eqz v0, :cond_0

    .line 978
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->setAlbumPitchRate(F)V

    goto :goto_0

    .line 983
    :cond_2
    neg-int v0, p4

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->setThumbImageFlingSpeed(I)V

    goto :goto_0
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)V
    .locals 0
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 1299
    return-void
.end method

.method protected onScrolled(IIII)Z
    .locals 2
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "x"    # I
    .param p4, "y"    # I

    .prologue
    const/4 v1, 0x0

    .line 965
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlGridView;->onMoved(II)Z

    move-result v0

    invoke-virtual {p0, v1, v1, v1, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->onReleased(IIII)Z

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method protected onTouch(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 250
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mIsFocusVisible:Z

    .line 251
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->onFocusChange(I)V

    .line 253
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->onTouch(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public reconstructView(I)V
    .locals 3
    .param p1, "animation"    # I

    .prologue
    .line 1250
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 1251
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->pushCurrentListInfo()V

    .line 1253
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->restoreStatus()Ljava/lang/Object;

    move-result-object v0

    .line 1254
    .local v0, "object":Ljava/lang/Object;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    if-eqz v1, :cond_0

    .line 1255
    check-cast v0, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    .end local v0    # "object":Ljava/lang/Object;
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    .line 1258
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mLockPos:Z

    .line 1260
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->createRectangles()[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 1261
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->reset()V

    .line 1262
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->register([Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 1264
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->loadFolders(Z)V

    .line 1267
    const/4 v1, 0x3

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAnimType:I

    .line 1268
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->convAnimGrid()V

    .line 1269
    return-void
.end method

.method public refreshView(Z)V
    .locals 2
    .param p1, "reload"    # Z

    .prologue
    const/4 v0, 0x3

    const/4 v1, 0x0

    .line 1312
    if-eqz p1, :cond_0

    .line 1314
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1, v1, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->setCommand(IIII)V

    .line 1320
    :goto_0
    return-void

    .line 1317
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->removeCommand(I)V

    .line 1318
    invoke-virtual {p0, v0, v1, v1, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->setCommand(IIII)V

    goto :goto_0
.end method

.method public reloadObjects()V
    .locals 2

    .prologue
    .line 1139
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    if-eqz v0, :cond_0

    .line 1140
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->freeRectangles()V

    .line 1141
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->createRectangles()[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 1142
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->reset()V

    .line 1143
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->register([Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 1145
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->loadFolders(Z)V

    .line 1146
    return-void
.end method

.method protected resetLayout()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 373
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->resetLayout()V

    .line 374
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mWidthSpace:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mHeightSpace:F

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->getHeight()I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/sec/samsung/gallery/util/DimensionUtil;->setDimensionUtil(FFII)V

    .line 375
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v2

    if-nez v2, :cond_1

    .line 376
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getRotation()I

    move-result v1

    .line 378
    .local v1, "orientation":I
    if-eq v1, v8, :cond_0

    if-ne v1, v9, :cond_4

    .line 379
    :cond_0
    iput-boolean v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mWideMode:Z

    .line 384
    .end local v1    # "orientation":I
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->getHorizontalCount()V

    .line 385
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    if-eqz v2, :cond_2

    .line 386
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    invoke-virtual {v2, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->exitFromHoverView(Z)V

    .line 387
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->initAttribute()V

    .line 389
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    if-eqz v2, :cond_3

    .line 390
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->exitFromPenSelectionView()V

    .line 391
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->initAttribute()V

    .line 394
    :cond_3
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->isCreated()Z

    move-result v2

    if-nez v2, :cond_5

    .line 436
    :goto_1
    return-void

    .line 381
    .restart local v1    # "orientation":I
    :cond_4
    iput-boolean v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mWideMode:Z

    goto :goto_0

    .line 398
    .end local v1    # "orientation":I
    :cond_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    if-eqz v2, :cond_6

    .line 399
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 401
    :cond_6
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSpreadAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;

    if-eqz v2, :cond_7

    .line 402
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mSpreadAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;

    invoke-virtual {v2, v8}, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->stop(Z)V

    .line 404
    :cond_7
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFadeOutAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;

    if-eqz v2, :cond_8

    .line 405
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFadeOutAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->stop()V

    .line 407
    :cond_8
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    if-eqz v2, :cond_9

    .line 408
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->stop()V

    .line 410
    :cond_9
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    if-eqz v2, :cond_a

    .line 411
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mVisibleFirst:I

    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFocused:I

    .line 412
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->reset()V

    .line 415
    :cond_a
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    if-eqz v2, :cond_b

    .line 416
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->resetDragInfo()V

    .line 419
    :cond_b
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAnimType:I

    if-ne v2, v9, :cond_d

    .line 422
    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAnimType:I

    .line 423
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v2, :cond_d

    .line 424
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    array-length v2, v2

    if-ge v0, v2, :cond_d

    .line 425
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v2, v2, v0

    if-eqz v2, :cond_c

    .line 426
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->remove()V

    .line 424
    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 432
    .end local v0    # "i":I
    :cond_d
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->reloadObjects()V

    .line 435
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->getGlRoot()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestLayout()V

    goto :goto_1
.end method

.method public setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V
    .locals 5
    .param p1, "adapter"    # Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .prologue
    const/4 v4, 0x0

    .line 690
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    if-eqz v1, :cond_0

    .line 691
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->unregisterObserver(Ljava/lang/Object;)V

    .line 693
    :cond_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .line 694
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v1, :cond_1

    .line 695
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->setAlbumViewMode(I)V

    .line 696
    new-instance v1, Lcom/sec/samsung/gallery/glview/GlGridView$3;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/GlGridView$3;-><init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    .line 709
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->registerObserver(Lcom/sec/samsung/gallery/glview/GContentObserver;)V

    .line 711
    const-wide/16 v2, 0x8c

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 715
    :goto_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v4, v4, v4}, Lcom/sec/samsung/gallery/glview/GlGridView;->setCommand(IIII)V

    .line 717
    :cond_1
    return-void

    .line 712
    :catch_0
    move-exception v0

    .line 713
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAirMotionImageFling(I)V
    .locals 2
    .param p1, "speed"    # I

    .prologue
    .line 1306
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mIsPressed:Z

    if-nez v0, :cond_0

    .line 1307
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->startFlingtoEnd(F)V

    .line 1308
    :cond_0
    return-void
.end method

.method public setAlbumViewMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 1364
    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewModeCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-le p1, v1, :cond_1

    .line 1387
    :cond_0
    :goto_0
    return-void

    .line 1369
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEmergencyMode(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1370
    :cond_2
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I

    .line 1374
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    if-eqz v1, :cond_3

    .line 1375
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->setViewMode(I)V

    .line 1378
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    .line 1379
    .local v0, "mStatusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v1, v2, :cond_5

    .line 1380
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    const-string v2, "peopleViewMode"

    invoke-static {v1, v2, p1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 1384
    :goto_2
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/glview/GlGridView;->setFirstThumbnailMultiple(I)V

    .line 1385
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v1, :cond_0

    .line 1386
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->setAlbumViewMode(I)V

    goto :goto_0

    .line 1372
    .end local v0    # "mStatusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    :cond_4
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I

    goto :goto_1

    .line 1382
    .restart local v0    # "mStatusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    :cond_5
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    const-string v2, "albumViewMode"

    invoke-static {v1, v2, p1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_2
.end method

.method public setAttrs(II)V
    .locals 1
    .param p1, "attr"    # I
    .param p2, "parm"    # I

    .prologue
    const/4 v0, 0x1

    .line 1347
    if-ne p1, v0, :cond_1

    .line 1348
    invoke-direct {p0, v0, p2}, Lcom/sec/samsung/gallery/glview/GlGridView;->setScaleFactor(ZI)V

    .line 1351
    :cond_0
    :goto_0
    return-void

    .line 1349
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 1350
    const/4 v0, 0x0

    invoke-direct {p0, v0, p2}, Lcom/sec/samsung/gallery/glview/GlGridView;->setScaleFactor(ZI)V

    goto :goto_0
.end method

.method public setColumn()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1354
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mColCnt:I

    .line 1355
    .local v0, "oldColCount":I
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGridView;->getHorizontalCount()V

    .line 1357
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mColCnt:I

    if-eq v1, v0, :cond_0

    .line 1358
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mVisibleFirst:I

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mFocused:I

    .line 1359
    const/4 v1, 0x6

    invoke-virtual {p0, v1, v2, v2, v2}, Lcom/sec/samsung/gallery/glview/GlGridView;->setCommand(IIII)V

    .line 1361
    :cond_0
    return-void
.end method

.method public setDragEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 2045
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mIsDragEnabled:Z

    .line 2046
    return-void
.end method

.method public setFirstThumbnailMultiple(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    const/4 v1, 0x1

    .line 1394
    if-eq p1, v1, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEmergencyMode(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1395
    :cond_1
    sget v0, Lcom/sec/samsung/gallery/glview/GlGridView;->FIRST_THUMBNAIL_WIDTH_MULTIPLE:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->widthMultiple:I

    .line 1396
    sget v0, Lcom/sec/samsung/gallery/glview/GlGridView;->FIRST_THUMBNAIL_HEIGHT_MULTIPLE:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->heightMultiple:I

    .line 1401
    :goto_0
    return-void

    .line 1398
    :cond_2
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->widthMultiple:I

    .line 1399
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->heightMultiple:I

    goto :goto_0
.end method

.method public setFocusIndex(IZ)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "isAlbum"    # Z

    .prologue
    .line 1870
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    if-nez v0, :cond_0

    .line 1873
    :goto_0
    return-void

    .line 1872
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iput p1, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemFocused:I

    goto :goto_0
.end method

.method public setMode(IILjava/lang/Object;)V
    .locals 3
    .param p1, "mode"    # I
    .param p2, "parm"    # I
    .param p3, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x0

    .line 1337
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mMode:I

    if-ne p1, v0, :cond_0

    .line 1344
    :goto_0
    return-void

    .line 1339
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->setMode(IILjava/lang/Object;)V

    .line 1341
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->loadFolders(Z)V

    .line 1342
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlGridView;->removeCommand(I)V

    .line 1343
    invoke-virtual {p0, v2, v1, v1, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->setCommand(IIII)V

    goto :goto_0
.end method

.method public setOnGetViewListener(Lcom/sec/samsung/gallery/glview/GlGridView$OnGetViewListener;)V
    .locals 0
    .param p1, "onGetViewListener"    # Lcom/sec/samsung/gallery/glview/GlGridView$OnGetViewListener;

    .prologue
    .line 1861
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnGetViewListener:Lcom/sec/samsung/gallery/glview/GlGridView$OnGetViewListener;

    .line 1862
    return-void
.end method

.method public setScrollBarMaxHeight(Lcom/sec/samsung/gallery/glview/GlScroller;F)V
    .locals 0
    .param p1, "scroller"    # Lcom/sec/samsung/gallery/glview/GlScroller;
    .param p2, "max"    # F

    .prologue
    .line 1885
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->setScrollBarMaxHeight(Lcom/sec/samsung/gallery/glview/GlScroller;F)V

    .line 1886
    return-void
.end method

.method protected updateItem(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 902
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mVisibleFirst:I

    if-lt p1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mVisibleLast:I

    if-le p1, v0, :cond_1

    .line 905
    :cond_0
    :goto_0
    return-void

    .line 904
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->update(I)V

    goto :goto_0
.end method
