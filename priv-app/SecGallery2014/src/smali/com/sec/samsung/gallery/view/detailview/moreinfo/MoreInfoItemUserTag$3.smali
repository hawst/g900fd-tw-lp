.class Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$3;
.super Ljava/lang/Object;
.source "MoreInfoItemUserTag.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText$onKeyPreImeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKeyPreIme(ILandroid/view/KeyEvent;)V
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v2, 0x8

    .line 112
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mRunEditButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$600(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mRunEditButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$600(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 115
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mAddTagButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$700(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 118
    :cond_0
    return-void
.end method
