.class public Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;
.super Lcom/sec/samsung/gallery/view/help/HelpViewState;
.source "HelpFaceTagState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContentDescription:Ljava/lang/String;

.field private mNameListener:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mContentDescription:Ljava/lang/String;

    .line 155
    new-instance v0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState$1;-><init>(Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mNameListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->onClickFaceTagging()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method private onClickFaceTagging()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x7d0

    const/4 v4, 0x5

    const/4 v2, 0x4

    const/4 v3, 0x0

    .line 163
    iget v0, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mViewMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 164
    iput v2, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mViewMode:I

    .line 165
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 166
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->setHelpView()V

    .line 167
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->startBubbleAnimation()V

    .line 168
    invoke-super {p0, v3}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->startTapAnimation(I)V

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mViewMode:I

    if-ne v0, v2, :cond_2

    .line 170
    iput v4, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mViewMode:I

    .line 171
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->setHelpView()V

    .line 172
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->startBubbleAnimation()V

    .line 173
    invoke-super {p0, v3}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->startTapAnimation(I)V

    goto :goto_0

    .line 174
    :cond_2
    iget v0, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mViewMode:I

    if-ne v0, v4, :cond_0

    .line 175
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mViewMode:I

    .line 176
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->setHelpView()V

    .line 177
    invoke-virtual {p0, v6, v7, v6, v7}, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->startFinishActivity(JJ)V

    goto :goto_0
.end method

.method private setViewFaceTagging()V
    .locals 10

    .prologue
    const v5, 0x7f0202d6

    const v6, 0x7f0202d4

    const/4 v9, 0x3

    const/4 v4, 0x0

    const/4 v8, 0x4

    .line 60
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 61
    .local v2, "res":Landroid/content/res/Resources;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v7, 0x2

    if-ne v3, v7, :cond_2

    const/4 v0, 0x1

    .line 62
    .local v0, "isLand":Z
    :goto_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mItem:Landroid/widget/ImageView;

    if-nez v3, :cond_0

    .line 63
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v7, 0x7f0f0133

    invoke-virtual {v3, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mItem:Landroid/widget/ImageView;

    .line 66
    :cond_0
    iget v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mViewMode:I

    if-ne v3, v9, :cond_f

    .line 67
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v5, 0x7f0e02c9

    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mContentDescription:Ljava/lang/String;

    .line 68
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v3, :cond_4

    .line 69
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mBG:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    const-string v3, "gallery_help_face_photo_land"

    :goto_1
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 72
    :goto_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mText:Landroid/widget/TextView;

    const v5, 0x7f0e02c9

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(I)V

    .line 73
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mText:Landroid/widget/TextView;

    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setGravity(I)V

    .line 74
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mHelpPopup:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_6

    const v3, 0x7f0d017c

    :goto_3
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v5, v3}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 76
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mHelpPopup:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_7

    const v3, 0x7f0d017b

    :goto_4
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v5, v3}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 78
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mTap:Landroid/widget/ImageView;

    if-eqz v0, :cond_8

    const v3, 0x7f0d0178

    :goto_5
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 80
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mTap:Landroid/widget/ImageView;

    if-eqz v0, :cond_9

    const v3, 0x7f0d0177

    :goto_6
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setY(F)V

    .line 82
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mHead:Landroid/widget/ImageView;

    if-eqz v0, :cond_a

    const v3, 0x7f0d0180

    :goto_7
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 84
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mItem:Landroid/widget/ImageView;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 85
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mItem:Landroid/widget/ImageView;

    if-eqz v0, :cond_b

    const v3, 0x7f0d0184

    :goto_8
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 87
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mItem:Landroid/widget/ImageView;

    if-eqz v0, :cond_c

    const v3, 0x7f0d0183

    :goto_9
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setY(F)V

    .line 89
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mItem:Landroid/widget/ImageView;

    if-eqz v0, :cond_d

    const v3, 0x7f0d0187

    :goto_a
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 91
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mItem:Landroid/widget/ImageView;

    if-eqz v0, :cond_e

    const v3, 0x7f0d0188

    :goto_b
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 151
    :cond_1
    :goto_c
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mTap:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mContentDescription:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 152
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mItem:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mContentDescription:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 153
    return-void

    .end local v0    # "isLand":Z
    :cond_2
    move v0, v4

    .line 61
    goto/16 :goto_0

    .line 69
    .restart local v0    # "isLand":Z
    :cond_3
    const-string v3, "gallery_help_face_photo"

    goto/16 :goto_1

    .line 71
    :cond_4
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mBG:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    const v3, 0x7f0202d9

    :goto_d
    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    :cond_5
    const v3, 0x7f0202d8

    goto :goto_d

    .line 74
    :cond_6
    const v3, 0x7f0d017a

    goto/16 :goto_3

    .line 76
    :cond_7
    const v3, 0x7f0d0179

    goto/16 :goto_4

    .line 78
    :cond_8
    const v3, 0x7f0d0176

    goto/16 :goto_5

    .line 80
    :cond_9
    const v3, 0x7f0d0175

    goto/16 :goto_6

    .line 82
    :cond_a
    const v3, 0x7f0d017e

    goto/16 :goto_7

    .line 85
    :cond_b
    const v3, 0x7f0d0182

    goto :goto_8

    .line 87
    :cond_c
    const v3, 0x7f0d0181

    goto :goto_9

    .line 89
    :cond_d
    const v3, 0x7f0d0185

    goto :goto_a

    .line 91
    :cond_e
    const v3, 0x7f0d0186

    goto :goto_b

    .line 93
    :cond_f
    iget v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mViewMode:I

    if-ne v3, v8, :cond_19

    .line 94
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v4, 0x7f0e02cb

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mContentDescription:Ljava/lang/String;

    .line 95
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v3, :cond_11

    .line 96
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mBG:Landroid/widget/ImageView;

    if-eqz v0, :cond_10

    const-string v3, "gallery_help_face_detail_land_1"

    :goto_e
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 100
    :goto_f
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v3

    if-eqz v3, :cond_13

    .line 101
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mText:Landroid/widget/TextView;

    const v4, 0x7f0e02ca

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 105
    :goto_10
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mText:Landroid/widget/TextView;

    const v4, 0x7f0d0199

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 106
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mHelpPopup:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_14

    const v3, 0x7f0d0190

    :goto_11
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v4, v3}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 108
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mHelpPopup:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_15

    const v3, 0x7f0d018f

    :goto_12
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v4, v3}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 110
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mTap:Landroid/widget/ImageView;

    if-eqz v0, :cond_16

    const v3, 0x7f0d018c

    :goto_13
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 112
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mTap:Landroid/widget/ImageView;

    if-eqz v0, :cond_17

    const v3, 0x7f0d018b

    :goto_14
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setY(F)V

    .line 114
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mHead:Landroid/widget/ImageView;

    if-eqz v0, :cond_18

    const v3, 0x7f0d0192

    :goto_15
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 116
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mItem:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_c

    .line 96
    :cond_10
    const-string v3, "gallery_help_face_detail_1"

    goto :goto_e

    .line 98
    :cond_11
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mBG:Landroid/widget/ImageView;

    if-eqz v0, :cond_12

    move v3, v5

    :goto_16
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_f

    :cond_12
    move v3, v6

    goto :goto_16

    .line 103
    :cond_13
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mText:Landroid/widget/TextView;

    const v4, 0x7f0e02cb

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_10

    .line 106
    :cond_14
    const v3, 0x7f0d018e

    goto :goto_11

    .line 108
    :cond_15
    const v3, 0x7f0d018d

    goto :goto_12

    .line 110
    :cond_16
    const v3, 0x7f0d018a

    goto :goto_13

    .line 112
    :cond_17
    const v3, 0x7f0d0189

    goto :goto_14

    .line 114
    :cond_18
    const v3, 0x7f0d0191

    goto :goto_15

    .line 117
    :cond_19
    iget v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mViewMode:I

    const/4 v7, 0x5

    if-ne v3, v7, :cond_1e

    .line 118
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v7, 0x7f0e02cc

    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mContentDescription:Ljava/lang/String;

    .line 119
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v3, :cond_1c

    .line 120
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mBG:Landroid/widget/ImageView;

    if-eqz v0, :cond_1b

    const-string v3, "gallery_help_face_detail_land_1"

    :goto_17
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 123
    :goto_18
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mHelpPopup:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 124
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mTap:Landroid/widget/ImageView;

    const/16 v5, 0x80

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/widget/ImageView;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    .line 125
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->clearAnimation()V

    .line 126
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 128
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mHelpFaceListView:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 129
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v5, 0x7f0f012c

    invoke-virtual {v3, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object v1, v3

    check-cast v1, Landroid/widget/TextView;

    .line 130
    .local v1, "name1":Landroid/widget/TextView;
    if-eqz v1, :cond_1a

    .line 131
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mNameListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v5, 0x7f0e02d3

    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 134
    :cond_1a
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v5, 0x7f0f012d

    invoke-virtual {v3, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mTap:Landroid/widget/ImageView;

    .line 135
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 136
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v4, 0x7f0f012e

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mText:Landroid/widget/TextView;

    .line 137
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mText:Landroid/widget/TextView;

    const v4, 0x7f0d0198

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 138
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v4, 0x7f0f012f

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mHead:Landroid/widget/ImageView;

    goto/16 :goto_c

    .line 120
    .end local v1    # "name1":Landroid/widget/TextView;
    :cond_1b
    const-string v3, "gallery_help_face_detail_1"

    goto/16 :goto_17

    .line 122
    :cond_1c
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mBG:Landroid/widget/ImageView;

    if-eqz v0, :cond_1d

    :goto_19
    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_18

    :cond_1d
    move v5, v6

    goto :goto_19

    .line 139
    :cond_1e
    iget v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mViewMode:I

    const/4 v4, 0x6

    if-ne v3, v4, :cond_1

    .line 140
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v3, :cond_20

    .line 141
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mBG:Landroid/widget/ImageView;

    if-eqz v0, :cond_1f

    const-string v3, "gallery_help_face_detail_land_2"

    :goto_1a
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 144
    :goto_1b
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mHelpPopup:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 145
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mText:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 146
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 147
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mHead:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 148
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mHelpFaceListView:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 149
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->showCompletedPopup()V

    goto/16 :goto_c

    .line 141
    :cond_1f
    const-string v3, "gallery_help_face_detail_2"

    goto :goto_1a

    .line 143
    :cond_20
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mBG:Landroid/widget/ImageView;

    if-eqz v0, :cond_21

    const v3, 0x7f0202d7

    :goto_1c
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1b

    :cond_21
    const v3, 0x7f0202d5

    goto :goto_1c
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 32
    const-string v0, "Help"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mHelpMode:I

    .line 33
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mViewMode:I

    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpPhotoActionBarForNormal;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/help/HelpPhotoActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 36
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->setHelpView()V

    .line 37
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-super {p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->animationInit(Landroid/content/Context;)V

    .line 38
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->startBubbleAnimation()V

    .line 39
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->setOnClickListener()V

    .line 40
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 44
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->onResume()V

    .line 45
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v1, :cond_0

    .line 46
    const/4 v0, 0x0

    .line 47
    .local v0, "multiWindow":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getMultiWindowActivity()Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 52
    .end local v0    # "multiWindow":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    :cond_0
    return-void
.end method

.method protected setHelpView()V
    .locals 0

    .prologue
    .line 55
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->setHelpView()V

    .line 56
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->setViewFaceTagging()V

    .line 57
    return-void
.end method

.method protected setOnClickListener()V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mBG:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mBG:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSoundEffectsEnabled(Z)V

    .line 187
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mBG:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState$2;-><init>(Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mText:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 197
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mText:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setImportantForAccessibility(I)V

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mItem:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 200
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mItem:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState$3;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState$3;-><init>(Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 207
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mTap:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 208
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;->mTap:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState$4;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/help/HelpFaceTagState$4;-><init>(Lcom/sec/samsung/gallery/view/help/HelpFaceTagState;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 216
    :cond_3
    return-void
.end method
