.class public Lcom/sec/samsung/gallery/util/HoverIconUtil;
.super Ljava/lang/Object;
.source "HoverIconUtil.java"


# static fields
.field public static final HOVERING_PENSELECT_POINTER_01:I = 0x15

.field public static final HOVERING_SCROLLICON_DOWN:I = 0xf

.field public static final HOVERING_SCROLLICON_LEFT:I = 0x11

.field public static final HOVERING_SCROLLICON_RIGHT:I = 0xd

.field public static final HOVERING_SCROLLICON_UP:I = 0xb

.field public static final HOVERING_SPENICON_DEFAULT:I = 0x1

.field public static final HOVERING_SPENICON_MORE:I = 0xa

.field private static mHoveringIcon:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x1

    sput v0, Lcom/sec/samsung/gallery/util/HoverIconUtil;->mHoveringIcon:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isHoveringIconDefault()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 27
    sget v1, Lcom/sec/samsung/gallery/util/HoverIconUtil;->mHoveringIcon:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isHoveringIconPenSelect()Z
    .locals 2

    .prologue
    .line 31
    sget v0, Lcom/sec/samsung/gallery/util/HoverIconUtil;->mHoveringIcon:I

    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isHoveringIconPenSelectView(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 95
    const/4 v1, 0x1

    .line 96
    .local v1, "result":Z
    instance-of v2, p0, Lcom/sec/android/gallery3d/app/CropImage;

    if-eqz v2, :cond_1

    .line 97
    const/4 v1, 0x0

    .line 104
    .end local p0    # "context":Landroid/content/Context;
    :cond_0
    :goto_0
    return v1

    .line 98
    .restart local p0    # "context":Landroid/content/Context;
    :cond_1
    instance-of v2, p0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    if-eqz v2, :cond_0

    .line 99
    check-cast p0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 100
    .local v0, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v2, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-nez v2, :cond_2

    instance-of v2, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    if-eqz v2, :cond_0

    .line 101
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static setHoveringIconTo(I)V
    .locals 4
    .param p0, "hoveringType"    # I

    .prologue
    .line 68
    sget v1, Lcom/sec/samsung/gallery/util/HoverIconUtil;->mHoveringIcon:I

    .line 70
    .local v1, "newIcon":I
    packed-switch p0, :pswitch_data_0

    .line 84
    :goto_0
    sget v2, Lcom/sec/samsung/gallery/util/HoverIconUtil;->mHoveringIcon:I

    if-ne v2, v1, :cond_0

    .line 92
    :goto_1
    return-void

    .line 72
    :pswitch_0
    const/16 v1, 0xb

    .line 73
    goto :goto_0

    .line 75
    :pswitch_1
    const/16 v1, 0xf

    .line 76
    goto :goto_0

    .line 78
    :pswitch_2
    const/16 v1, 0x11

    .line 79
    goto :goto_0

    .line 81
    :pswitch_3
    const/16 v1, 0xd

    goto :goto_0

    .line 86
    :cond_0
    sput v1, Lcom/sec/samsung/gallery/util/HoverIconUtil;->mHoveringIcon:I

    .line 88
    :try_start_0
    sget v2, Lcom/sec/samsung/gallery/util/HoverIconUtil;->mHoveringIcon:I

    const/4 v3, -0x1

    invoke-static {v2, v3}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 89
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 70
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static setHoveringIconToDefault()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 35
    sget v1, Lcom/sec/samsung/gallery/util/HoverIconUtil;->mHoveringIcon:I

    if-ne v1, v2, :cond_0

    .line 43
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return-void

    .line 37
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    sput v2, Lcom/sec/samsung/gallery/util/HoverIconUtil;->mHoveringIcon:I

    .line 39
    const/4 v1, 0x1

    const/4 v2, -0x1

    :try_start_0
    invoke-static {v1, v2}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 40
    :catch_0
    move-exception v0

    .line 41
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setHoveringIconToMoreIcon()V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 57
    sget v1, Lcom/sec/samsung/gallery/util/HoverIconUtil;->mHoveringIcon:I

    if-ne v1, v2, :cond_0

    .line 65
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return-void

    .line 59
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    sput v2, Lcom/sec/samsung/gallery/util/HoverIconUtil;->mHoveringIcon:I

    .line 61
    const/16 v1, 0xa

    const/4 v2, -0x1

    :try_start_0
    invoke-static {v1, v2}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 62
    :catch_0
    move-exception v0

    .line 63
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setHoveringIconToPenSelect()V
    .locals 3

    .prologue
    const/16 v2, 0x15

    .line 46
    sget v1, Lcom/sec/samsung/gallery/util/HoverIconUtil;->mHoveringIcon:I

    if-ne v1, v2, :cond_0

    .line 54
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return-void

    .line 48
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    sput v2, Lcom/sec/samsung/gallery/util/HoverIconUtil;->mHoveringIcon:I

    .line 50
    const/16 v1, 0x15

    const/4 v2, -0x1

    :try_start_0
    invoke-static {v1, v2}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 51
    :catch_0
    move-exception v0

    .line 52
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
