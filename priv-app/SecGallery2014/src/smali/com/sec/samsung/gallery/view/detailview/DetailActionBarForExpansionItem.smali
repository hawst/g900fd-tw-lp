.class public Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;
.source "DetailActionBarForExpansionItem.java"


# instance fields
.field private mDivider:Landroid/widget/ImageView;

.field private mFromGifMaker:Z

.field private mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

.field private mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mTwCheckBox:Landroid/widget/CheckBox;

.field private maxPickCount:I

.field private totalSelectedCount:I


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 8
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v3, -0x1

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 53
    invoke-direct {p0, p1, v7}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Z)V

    .line 42
    iput-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mTwCheckBox:Landroid/widget/CheckBox;

    .line 45
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mFromGifMaker:Z

    .line 48
    iput-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mDivider:Landroid/widget/ImageView;

    .line 49
    iput v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->maxPickCount:I

    .line 50
    iput v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->totalSelectedCount:I

    .line 56
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionActionBarView:Landroid/view/View;

    const v4, 0x7f0f0020

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 57
    .local v0, "checkBox":Landroid/widget/CheckBox;
    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 59
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v4, 0x7f0f0021

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 60
    .local v1, "mSelectionButton":Landroid/widget/TextView;
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 61
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setClickable(Z)V

    .line 62
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0262

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-virtual {v1, v3, v5, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 63
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionActionBarView:Landroid/view/View;

    const v4, 0x7f0f001f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 64
    .local v2, "mSelectionModeOnActionBarLayout":Landroid/widget/LinearLayout;
    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 65
    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getNewAlbumSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 68
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 69
    new-instance v3, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v3, v4, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;-><init>(Landroid/content/Context;Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    .line 70
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionActionBarView:Landroid/view/View;

    const v4, 0x7f0f001d

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mTwCheckBox:Landroid/widget/CheckBox;

    .line 71
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->updateCheckBox()V

    .line 72
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v3, :cond_0

    .line 73
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionActionBarView:Landroid/view/View;

    const v4, 0x7f0f001c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mDivider:Landroid/widget/ImageView;

    .line 74
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mDivider:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 76
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mTwCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v7}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 77
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mTwCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v7}, Landroid/widget/CheckBox;->setSoundEffectsEnabled(Z)V

    .line 78
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFromGifMaker(Landroid/content/Context;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mFromGifMaker:Z

    .line 80
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mTwCheckBox:Landroid/widget/CheckBox;

    new-instance v4, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;

    invoke-direct {v4, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)V

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->initActionBarBg()V

    .line 147
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->totalSelectedCount:I

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;
    .param p1, "x1"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->totalSelectedCount:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->isMultiPickMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->updatePopupMenuButtonState()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->isMultiPickMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->updatePopupMenuButtonState()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->isMultiPickMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2100(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->updatePopupMenuButtonState()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2200(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->getMinCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$2800(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;Lcom/sec/android/gallery3d/data/MediaSet;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "x2"    # Z

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->updateSelectionButton(Lcom/sec/android/gallery3d/data/MediaSet;Z)V

    return-void
.end method

.method static synthetic access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->setupPopupMenu(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$3400(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;IILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->setSelectAllButtonTitle(IILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$4200(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;IILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->setSelectAllButtonTitle(IILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$4300(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->setSelectAllButtonTTS(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$4400(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->isMultiPickMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->maxPickCount:I

    return v0
.end method

.method static synthetic access$802(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;
    .param p1, "x1"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->maxPickCount:I

    return p1
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method private checkFaceImageType(Lcom/sec/android/gallery3d/data/MediaObject;)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 3
    .param p1, "media"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 329
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->getTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v1, v2, :cond_0

    .line 330
    instance-of v1, p1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v1, :cond_0

    move-object v1, p1

    .line 331
    check-cast v1, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalImage;->getFaceImagePathForExpansion()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 332
    .local v0, "facePath":Lcom/sec/android/gallery3d/data/Path;
    if-eqz v0, :cond_0

    .line 333
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object p1

    .line 337
    .end local v0    # "facePath":Lcom/sec/android/gallery3d/data/Path;
    :cond_0
    return-object p1
.end method

.method private getMinCount()I
    .locals 3

    .prologue
    .line 301
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 302
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "pick-min-item"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method private getTagType()Lcom/sec/samsung/gallery/core/TabTagType;
    .locals 2

    .prologue
    .line 341
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSourceMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 342
    .local v0, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    goto :goto_0
.end method

.method private initActionBarBg()V
    .locals 4

    .prologue
    .line 150
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mMainActionBar:Landroid/app/ActionBar;

    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200ab

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 153
    :cond_0
    return-void
.end method

.method private updateCheckBox()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 307
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mTwCheckBox:Landroid/widget/CheckBox;

    if-nez v2, :cond_0

    .line 326
    :goto_0
    return-void

    .line 310
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getExpMediaItem()Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    .line 313
    .local v1, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    const/4 v0, 0x0

    .line 315
    .local v0, "isSelected":Z
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->isSinglePickMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 316
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mTwCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0

    .line 317
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 318
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mTwCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0

    .line 320
    :cond_2
    if-eqz v1, :cond_3

    .line 321
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v0

    .line 323
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mTwCheckBox:Landroid/widget/CheckBox;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 324
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mTwCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method private updateSelectionButton(Lcom/sec/android/gallery3d/data/MediaSet;Z)V
    .locals 10
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "updataTitle"    # Z

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 156
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v3

    .line 157
    .local v3, "numberOfItemsInAlbum":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v0

    .line 158
    .local v0, "checkedCount":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v2

    .line 159
    .local v2, "newAlbumCheckCount":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v5

    .line 160
    .local v5, "totalCheckedCount":I
    add-int v8, v0, v2

    if-ne v3, v8, :cond_2

    move v4, v6

    .line 162
    .local v4, "selectAll":Z
    :goto_0
    if-nez v3, :cond_0

    .line 163
    const/4 v4, 0x0

    .line 165
    :cond_0
    if-eqz p2, :cond_1

    .line 166
    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->setTitle(I)V

    .line 168
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->getMaxCount()I

    move-result v1

    .line 169
    .local v1, "maxPickCount":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->isMultiPickMode()Z

    move-result v8

    if-eqz v8, :cond_3

    if-lez v1, :cond_3

    if-ne v1, v5, :cond_3

    .line 170
    sget-object v8, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {p0, v8, v7, v6}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 176
    :goto_1
    return-void

    .end local v1    # "maxPickCount":I
    .end local v4    # "selectAll":Z
    :cond_2
    move v4, v7

    .line 160
    goto :goto_0

    .line 171
    .restart local v1    # "maxPickCount":I
    .restart local v4    # "selectAll":Z
    :cond_3
    if-ne v3, v2, :cond_4

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->getTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v8

    sget-object v9, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v8, v9, :cond_4

    .line 172
    sget-object v7, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    const/16 v8, 0x8

    invoke-virtual {p0, v7, v8, v6}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    goto :goto_1

    .line 174
    :cond_4
    sget-object v6, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {p0, v6, v7, v4}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    goto :goto_1
.end method


# virtual methods
.method protected final hide()V
    .locals 0

    .prologue
    .line 215
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->hide()V

    .line 216
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->dismissPopupMenu()V

    .line 217
    return-void
.end method

.method public isManualRotateIconEnabled()Z
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->isRotationMenuItemEnabled()Z

    move-result v0

    .line 350
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v2, 0x7f0f0298

    .line 180
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 181
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 182
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f120017

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 184
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v1, :cond_0

    .line 185
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->setRotationMenuItem(Landroid/view/MenuItem;)V

    .line 190
    :goto_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->updateCheckBox()V

    .line 192
    return-void

    .line 187
    :cond_0
    invoke-interface {p1, v2}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 2
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 201
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    sget v1, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELAY_HIDE_BARS:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->notifyObservers(Ljava/lang/Object;)V

    .line 202
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 209
    :cond_0
    :goto_0
    return-void

    .line 204
    :pswitch_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->handleScreenRotation()V

    goto :goto_0

    .line 202
    nop

    :pswitch_data_0
    .packed-switch 0x7f0f0298
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 294
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->onPause()V

    .line 295
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->onPause()V

    .line 298
    :cond_0
    return-void
.end method

.method protected onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 196
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->updateCheckBox()V

    .line 197
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 285
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->onResume()V

    .line 286
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->onResume()V

    .line 289
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->setupButtons()V

    .line 290
    return-void
.end method

.method protected setTitle(I)V
    .locals 1
    .param p1, "numberOfItemsSelected"    # I

    .prologue
    .line 238
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->setTitle(II)V

    .line 239
    return-void
.end method

.method protected setTitle(II)V
    .locals 2
    .param p1, "numberOfItemsSelected"    # I
    .param p2, "countOfMediaItem"    # I

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;II)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 281
    :cond_0
    return-void
.end method

.method protected setupButtons()V
    .locals 2

    .prologue
    .line 221
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->setupButtons()V

    .line 222
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$2;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 234
    :cond_0
    return-void
.end method
