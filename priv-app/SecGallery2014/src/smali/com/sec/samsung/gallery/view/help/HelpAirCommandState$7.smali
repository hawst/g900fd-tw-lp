.class Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$7;
.super Ljava/lang/Object;
.source "HelpAirCommandState.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->setViewAirButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

.field final synthetic val$helpY:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;I)V
    .locals 0

    .prologue
    .line 251
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$7;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iput p2, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$7;->val$helpY:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const-wide/high16 v4, 0x3fe8000000000000L    # 0.75

    .line 254
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$7;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$7;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget v1, v1, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mWidth:I

    int-to-double v2, v1

    mul-double/2addr v2, v4

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setMaxWidth(I)V

    .line 255
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$7;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$7;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget v1, v1, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mWidth:I

    int-to-double v2, v1

    mul-double/2addr v2, v4

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setMaxHeight(I)V

    .line 256
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$7;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$7;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget v1, v1, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mWidth:I

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$7;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setX(F)V

    .line 257
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$7;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    iget v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$7;->val$helpY:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$7;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setY(F)V

    .line 258
    return-void
.end method
