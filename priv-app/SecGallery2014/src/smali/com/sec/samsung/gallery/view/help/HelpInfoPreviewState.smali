.class public Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;
.super Lcom/sec/samsung/gallery/view/help/HelpViewState;
.source "HelpInfoPreviewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;


# static fields
.field private static final TAG:Ljava/lang/String; = "HelpInfoPreviewState"


# instance fields
.field private INVALID_POSITION:F

.field private IsShowCompletePopup:Z

.field contentDescription:Ljava/lang/String;

.field private mHelpHoverListener:Landroid/view/View$OnHoverListener;

.field private mHelpTouchListener:Landroid/view/View$OnTouchListener;

.field private mX:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->contentDescription:Ljava/lang/String;

    .line 191
    new-instance v0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$2;-><init>(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHelpHoverListener:Landroid/view/View$OnHoverListener;

    .line 242
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->INVALID_POSITION:F

    .line 243
    iget v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->INVALID_POSITION:F

    iput v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mX:F

    .line 244
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->IsShowCompletePopup:Z

    .line 246
    new-instance v0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;-><init>(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHelpTouchListener:Landroid/view/View$OnTouchListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mX:F

    return v0
.end method

.method static synthetic access$502(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;
    .param p1, "x1"    # F

    .prologue
    .line 28
    iput p1, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mX:F

    return p1
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->INVALID_POSITION:F

    return v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->IsShowCompletePopup:Z

    return v0
.end method

.method static synthetic access$802(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->IsShowCompletePopup:Z

    return p1
.end method

.method private setViewInformationPreview()V
    .locals 15

    .prologue
    const v14, 0x7f0f0135

    const v13, 0x7f0e02d0

    const v12, 0x7f0e02cd

    const/4 v11, 0x4

    const/4 v10, 0x0

    .line 54
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 55
    .local v5, "res":Landroid/content/res/Resources;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mTap:Landroid/widget/ImageView;

    const v9, 0x7f0202ef

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 56
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mTap:Landroid/widget/ImageView;

    new-instance v9, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$1;

    invoke-direct {v9, p0}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$1;-><init>(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)V

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->orientation:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_5

    .line 65
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v8, :cond_1

    .line 66
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mBG:Landroid/widget/ImageView;

    const-string v9, "gallery_help_album_land"

    invoke-virtual {p0, v9}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 69
    :goto_0
    iget v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mViewMode:I

    const/4 v9, 0x7

    if-ne v8, v9, :cond_2

    .line 70
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mText:Landroid/widget/TextView;

    invoke-virtual {v8, v12}, Landroid/widget/TextView;->setText(I)V

    .line 71
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8, v12}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->contentDescription:Ljava/lang/String;

    .line 72
    const v8, 0x7f0d01da

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 73
    .local v6, "tapY":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHelpPopup:Landroid/widget/RelativeLayout;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v6

    int-to-float v9, v9

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 74
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mTap:Landroid/widget/ImageView;

    int-to-float v9, v6

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setY(F)V

    .line 75
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mTap:Landroid/widget/ImageView;

    iget v9, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mWidth:I

    div-int/lit8 v9, v9, 0x2

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v10}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    sub-int/2addr v9, v10

    int-to-float v9, v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setX(F)V

    .line 76
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHead:Landroid/widget/ImageView;

    iget v9, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mWidth:I

    div-int/lit8 v9, v9, 0x2

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHead:Landroid/widget/ImageView;

    invoke-virtual {v10}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    sub-int/2addr v9, v10

    int-to-float v9, v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setX(F)V

    .line 77
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->startBubbleAnimation()V

    .line 188
    .end local v6    # "tapY":I
    :cond_0
    :goto_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mTap:Landroid/widget/ImageView;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->contentDescription:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 189
    return-void

    .line 68
    :cond_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mBG:Landroid/widget/ImageView;

    const v9, 0x7f0202de

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 78
    :cond_2
    iget v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mViewMode:I

    const/16 v9, 0x8

    if-ne v8, v9, :cond_3

    .line 79
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->clearAnimation()V

    .line 80
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mTap:Landroid/widget/ImageView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 82
    const/4 v8, 0x1

    const/4 v9, -0x1

    :try_start_0
    invoke-static {v8, v9}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    :goto_2
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverView:Landroid/widget/ImageView;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHelpHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 88
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v8, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 89
    const v8, 0x7f0d0165

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 91
    .local v7, "top_margin":I
    const v8, 0x7f0d0166

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 93
    .local v1, "bottom_margin":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverView:Landroid/widget/ImageView;

    int-to-float v9, v7

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setY(F)V

    .line 94
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverView:Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 95
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverView:Landroid/widget/ImageView;

    invoke-virtual {v8, v10, v10}, Landroid/widget/ImageView;->measure(II)V

    .line 96
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8, v14}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    move-object v0, v8

    check-cast v0, Landroid/widget/LinearLayout;

    .line 98
    .local v0, "arrow":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v10, v10}, Landroid/widget/LinearLayout;->measure(II)V

    .line 99
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverView:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v8, v7

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    sub-int/2addr v8, v9

    int-to-float v8, v8

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setY(F)V

    .line 101
    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 102
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mText:Landroid/widget/TextView;

    invoke-virtual {v8, v13}, Landroid/widget/TextView;->setText(I)V

    .line 103
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8, v13}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->contentDescription:Ljava/lang/String;

    .line 104
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHelpPopup:Landroid/widget/RelativeLayout;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverView:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v7

    sub-int/2addr v9, v1

    int-to-float v9, v9

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 105
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHead:Landroid/widget/ImageView;

    iget v9, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mWidth:I

    div-int/lit8 v9, v9, 0x2

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHead:Landroid/widget/ImageView;

    invoke-virtual {v10}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    sub-int/2addr v9, v10

    int-to-float v9, v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setX(F)V

    .line 106
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverView:Landroid/widget/ImageView;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHelpTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 107
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->startBubbleAnimation()V

    goto/16 :goto_1

    .line 83
    .end local v0    # "arrow":Landroid/widget/LinearLayout;
    .end local v1    # "bottom_margin":I
    .end local v7    # "top_margin":I
    :catch_0
    move-exception v3

    .line 84
    .local v3, "e":Landroid/os/RemoteException;
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_2

    .line 108
    .end local v3    # "e":Landroid/os/RemoteException;
    :cond_3
    iget v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mViewMode:I

    const/16 v9, 0x9

    if-ne v8, v9, :cond_4

    .line 109
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f0134

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    move-object v4, v8

    check-cast v4, Landroid/widget/ImageView;

    .line 110
    .local v4, "hover":Landroid/widget/ImageView;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverView:Landroid/widget/ImageView;

    const v9, 0x7f0d0165

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setY(F)V

    .line 112
    invoke-virtual {v4, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 113
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8, v14}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    move-object v0, v8

    check-cast v0, Landroid/widget/LinearLayout;

    .line 115
    .restart local v0    # "arrow":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 116
    const v8, 0x7f0202bc

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 117
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHelpPopup:Landroid/widget/RelativeLayout;

    invoke-virtual {v8, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 118
    .end local v0    # "arrow":Landroid/widget/LinearLayout;
    .end local v4    # "hover":Landroid/widget/ImageView;
    :cond_4
    iget v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mViewMode:I

    const/16 v9, 0xa

    if-ne v8, v9, :cond_0

    .line 119
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f013c

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    move-object v2, v8

    check-cast v2, Landroid/widget/TextView;

    .line 121
    .local v2, "completePopup":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f040002

    invoke-static {v8, v9}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 123
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0e02d1

    invoke-virtual {v8, v9}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 125
    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 128
    .end local v2    # "completePopup":Landroid/widget/TextView;
    :cond_5
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v8, :cond_6

    .line 129
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mBG:Landroid/widget/ImageView;

    const-string v9, "gallery_help_album"

    invoke-virtual {p0, v9}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 132
    :goto_3
    iget v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mViewMode:I

    const/4 v9, 0x7

    if-ne v8, v9, :cond_7

    .line 133
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mText:Landroid/widget/TextView;

    invoke-virtual {v8, v12}, Landroid/widget/TextView;->setText(I)V

    .line 134
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8, v12}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->contentDescription:Ljava/lang/String;

    .line 135
    const v8, 0x7f0d01db

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 136
    .restart local v6    # "tapY":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHelpPopup:Landroid/widget/RelativeLayout;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v6

    int-to-float v9, v9

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 137
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mTap:Landroid/widget/ImageView;

    int-to-float v9, v6

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setY(F)V

    .line 138
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mTap:Landroid/widget/ImageView;

    iget v9, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mWidth:I

    div-int/lit8 v9, v9, 0x2

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v10}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    sub-int/2addr v9, v10

    int-to-float v9, v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setX(F)V

    .line 139
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHead:Landroid/widget/ImageView;

    iget v9, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mWidth:I

    div-int/lit8 v9, v9, 0x2

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHead:Landroid/widget/ImageView;

    invoke-virtual {v10}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    sub-int/2addr v9, v10

    int-to-float v9, v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setX(F)V

    .line 140
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->startBubbleAnimation()V

    goto/16 :goto_1

    .line 131
    .end local v6    # "tapY":I
    :cond_6
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mBG:Landroid/widget/ImageView;

    const v9, 0x7f0202dd

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    .line 141
    :cond_7
    iget v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mViewMode:I

    const/16 v9, 0x8

    if-ne v8, v9, :cond_8

    .line 142
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->clearAnimation()V

    .line 143
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v8, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 144
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mTap:Landroid/widget/ImageView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 146
    const/4 v8, 0x1

    const/4 v9, -0x1

    :try_start_1
    invoke-static {v8, v9}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 150
    :goto_4
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverView:Landroid/widget/ImageView;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHelpHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 151
    const v8, 0x7f0d0164

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 153
    .restart local v7    # "top_margin":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverView:Landroid/widget/ImageView;

    int-to-float v9, v7

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setY(F)V

    .line 154
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverView:Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 155
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverView:Landroid/widget/ImageView;

    invoke-virtual {v8, v10, v10}, Landroid/widget/ImageView;->measure(II)V

    .line 156
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8, v14}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    move-object v0, v8

    check-cast v0, Landroid/widget/LinearLayout;

    .line 158
    .restart local v0    # "arrow":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v10, v10}, Landroid/widget/LinearLayout;->measure(II)V

    .line 159
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverView:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v8, v7

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    sub-int/2addr v8, v9

    int-to-float v8, v8

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setY(F)V

    .line 161
    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 162
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mText:Landroid/widget/TextView;

    invoke-virtual {v8, v13}, Landroid/widget/TextView;->setText(I)V

    .line 163
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8, v13}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->contentDescription:Ljava/lang/String;

    .line 164
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHead:Landroid/widget/ImageView;

    iget v9, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mWidth:I

    div-int/lit8 v9, v9, 0x2

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHead:Landroid/widget/ImageView;

    invoke-virtual {v10}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    sub-int/2addr v9, v10

    int-to-float v9, v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setX(F)V

    .line 165
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHelpPopup:Landroid/widget/RelativeLayout;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverView:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v7

    int-to-float v9, v9

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 166
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverView:Landroid/widget/ImageView;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHelpTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 167
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->startBubbleAnimation()V

    goto/16 :goto_1

    .line 147
    .end local v0    # "arrow":Landroid/widget/LinearLayout;
    .end local v7    # "top_margin":I
    :catch_1
    move-exception v3

    .line 148
    .restart local v3    # "e":Landroid/os/RemoteException;
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_4

    .line 168
    .end local v3    # "e":Landroid/os/RemoteException;
    :cond_8
    iget v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mViewMode:I

    const/16 v9, 0x9

    if-ne v8, v9, :cond_9

    .line 169
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f0134

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverView:Landroid/widget/ImageView;

    .line 170
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverView:Landroid/widget/ImageView;

    const v9, 0x7f0d0164

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setY(F)V

    .line 172
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverView:Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 173
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8, v14}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    move-object v0, v8

    check-cast v0, Landroid/widget/LinearLayout;

    .line 175
    .restart local v0    # "arrow":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 176
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverView:Landroid/widget/ImageView;

    const v9, 0x7f0202bc

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 177
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHelpPopup:Landroid/widget/RelativeLayout;

    invoke-virtual {v8, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 178
    .end local v0    # "arrow":Landroid/widget/LinearLayout;
    :cond_9
    iget v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mViewMode:I

    const/16 v9, 0xa

    if-ne v8, v9, :cond_0

    .line 179
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0f013c

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    move-object v2, v8

    check-cast v2, Landroid/widget/TextView;

    .line 181
    .restart local v2    # "completePopup":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f040002

    invoke-static {v8, v9}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 183
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0e02d1

    invoke-virtual {v8, v9}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 185
    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    .line 34
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 36
    const-string v0, "Help"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHelpMode:I

    .line 37
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mViewMode:I

    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/help/HelpAlbumActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 39
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mTap:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHelpHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 41
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->setHelpView()V

    .line 42
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->animationInit(Landroid/content/Context;)V

    .line 43
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->setOnClickListener()V

    .line 45
    return-void
.end method

.method protected setHelpView()V
    .locals 0

    .prologue
    .line 48
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->setHelpView()V

    .line 49
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->setViewInformationPreview()V

    .line 50
    return-void
.end method
