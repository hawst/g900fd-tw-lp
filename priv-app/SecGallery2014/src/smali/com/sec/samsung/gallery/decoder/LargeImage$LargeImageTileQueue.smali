.class Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;
.super Ljava/lang/Object;
.source "LargeImage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/decoder/LargeImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LargeImageTileQueue"
.end annotation


# instance fields
.field private mHead:Lcom/sec/samsung/gallery/decoder/LargeImageTile;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 465
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/decoder/LargeImage$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/decoder/LargeImage$1;

    .prologue
    .line 465
    invoke-direct {p0}, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;-><init>()V

    return-void
.end method


# virtual methods
.method public clean()V
    .locals 1

    .prologue
    .line 489
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;->mHead:Lcom/sec/samsung/gallery/decoder/LargeImageTile;

    .line 490
    return-void
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;->mHead:Lcom/sec/samsung/gallery/decoder/LargeImageTile;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pop()Lcom/sec/samsung/gallery/decoder/LargeImageTile;
    .locals 2

    .prologue
    .line 469
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;->mHead:Lcom/sec/samsung/gallery/decoder/LargeImageTile;

    .line 470
    .local v0, "tile":Lcom/sec/samsung/gallery/decoder/LargeImageTile;
    if-eqz v0, :cond_0

    .line 471
    iget-object v1, v0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mNext:Lcom/sec/samsung/gallery/decoder/LargeImageTile;

    iput-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;->mHead:Lcom/sec/samsung/gallery/decoder/LargeImageTile;

    .line 472
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mNext:Lcom/sec/samsung/gallery/decoder/LargeImageTile;

    .line 474
    :cond_0
    return-object v0
.end method

.method public push(Lcom/sec/samsung/gallery/decoder/LargeImageTile;)Z
    .locals 2
    .param p1, "tile"    # Lcom/sec/samsung/gallery/decoder/LargeImageTile;

    .prologue
    .line 478
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;->mHead:Lcom/sec/samsung/gallery/decoder/LargeImageTile;

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 479
    .local v0, "wasEmpty":Z
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;->mHead:Lcom/sec/samsung/gallery/decoder/LargeImageTile;

    iput-object v1, p1, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mNext:Lcom/sec/samsung/gallery/decoder/LargeImageTile;

    .line 480
    iput-object p1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;->mHead:Lcom/sec/samsung/gallery/decoder/LargeImageTile;

    .line 481
    return v0

    .line 478
    .end local v0    # "wasEmpty":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
