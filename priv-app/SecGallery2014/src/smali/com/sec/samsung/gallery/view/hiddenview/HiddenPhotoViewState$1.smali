.class Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$1;
.super Lorg/puremvc/java/patterns/mediator/Mediator;
.source "HiddenPhotoViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/Object;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    invoke-direct {p0, p2, p3}, Lorg/puremvc/java/patterns/mediator/Mediator;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public handleNotification(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 4
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const/4 v3, 0x1

    .line 103
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getName()Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, "notiName":Ljava/lang/String;
    const-string v2, "EXIT_SELECTION_MODE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 105
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # invokes: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->exitSelectionMode(Z)V
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$000(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;Z)V

    .line 106
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 107
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 108
    .local v1, "reset":I
    if-ne v1, v3, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$100(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 109
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$100(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resetContentWindow()V

    .line 110
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$100(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resetAllThumbnails()V

    .line 111
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->notifyDataSetChanged()V

    .line 115
    .end local v1    # "reset":I
    :cond_0
    return-void
.end method

.method public listNotificationInterests()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 96
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "EXIT_SELECTION_MODE"

    aput-object v2, v0, v1

    return-object v0
.end method
