.class Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerItemClickListener;
.super Ljava/lang/Object;
.source "NavigationDrawer.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/drawer/NavigationDrawer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DrawerItemClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)V
    .locals 0

    .prologue
    .line 537
    iput-object p1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerItemClickListener;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;

    .prologue
    .line 537
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerItemClickListener;-><init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 540
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerItemClickListener;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;
    invoke-static {v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$100(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    move-result-object v3

    invoke-virtual {v3, p3}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->isEnabled(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 542
    :try_start_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerItemClickListener;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;
    invoke-static {v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$100(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    move-result-object v3

    invoke-virtual {v3, p3}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->getChild(I)Lcom/sec/samsung/gallery/drawer/DrawerItem;

    move-result-object v1

    .line 543
    .local v1, "item":Lcom/sec/samsung/gallery/drawer/DrawerItem;
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDrawerExpanderMode:Z

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->isGroupNameItem()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getName()Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerItemClickListener;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$300(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e04cb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 545
    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerItemClickListener;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;
    invoke-static {v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$100(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->updateFilterByExpandMode()V

    .line 560
    .end local v1    # "item":Lcom/sec/samsung/gallery/drawer/DrawerItem;
    :cond_0
    :goto_0
    return-void

    .line 548
    .restart local v1    # "item":Lcom/sec/samsung/gallery/drawer/DrawerItem;
    :cond_1
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 549
    .local v2, "msg":Landroid/os/Message;
    const/4 v3, 0x1

    iput v3, v2, Landroid/os/Message;->what:I

    .line 550
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 551
    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerItemClickListener;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$800(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Landroid/os/Handler;

    move-result-object v3

    const-wide/16 v4, 0x12c

    invoke-virtual {v3, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 552
    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerItemClickListener;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;
    invoke-static {v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$2500(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Landroid/support/v4/widget/DrawerLayout;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$DrawerItemClickListener;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mLinearlayout:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$1200(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 554
    .end local v1    # "item":Lcom/sec/samsung/gallery/drawer/DrawerItem;
    .end local v2    # "msg":Landroid/os/Message;
    :catch_0
    move-exception v0

    .line 555
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    .line 556
    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$1500()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ignore selected item, "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " drawer adapter size is changed"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
