.class Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;
.super Landroid/support/v4/app/ActionBarDrawerToggle;
.source "NavigationDrawer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->initDrawerToggle()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V
    .locals 6
    .param p2, "x0"    # Landroid/app/Activity;
    .param p3, "x1"    # Landroid/support/v4/widget/DrawerLayout;
    .param p4, "x2"    # I
    .param p5, "x3"    # I
    .param p6, "x4"    # I

    .prologue
    .line 436
    iput-object p1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/app/ActionBarDrawerToggle;-><init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V

    return-void
.end method

.method private bringDrawerToFront()V
    .locals 1

    .prologue
    .line 517
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;
    invoke-static {v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$2500(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Landroid/support/v4/widget/DrawerLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->bringToFront()V

    .line 518
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;
    invoke-static {v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$2500(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Landroid/support/v4/widget/DrawerLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->requestLayout()V

    .line 519
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 512
    invoke-super {p0, p1}, Landroid/support/v4/app/ActionBarDrawerToggle;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 513
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # invokes: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->adjustLayout()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$700(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)V

    .line 514
    return-void
.end method

.method public onDrawerClosed(Landroid/view/View;)V
    .locals 2
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 504
    invoke-super {p0, p1}, Landroid/support/v4/app/ActionBarDrawerToggle;->onDrawerClosed(Landroid/view/View;)V

    .line 505
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$500(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$500(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v1, :cond_0

    .line 506
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    const/4 v1, 0x1

    # invokes: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setSelectAllFocused(Z)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$2400(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;Z)V

    .line 508
    :cond_0
    return-void
.end method

.method public onDrawerOpened(Landroid/view/View;)V
    .locals 2
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 496
    invoke-super {p0, p1}, Landroid/support/v4/app/ActionBarDrawerToggle;->onDrawerOpened(Landroid/view/View;)V

    .line 497
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$500(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$500(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v1, :cond_0

    .line 498
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    const/4 v1, 0x0

    # invokes: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setSelectAllFocused(Z)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$2400(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;Z)V

    .line 500
    :cond_0
    return-void
.end method

.method public onDrawerSlide(Landroid/view/View;F)V
    .locals 7
    .param p1, "drawerView"    # Landroid/view/View;
    .param p2, "slideOffset"    # F

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 440
    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$1500()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onDrawerSlide start: slideOffset = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mTouched = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mTouched:Z
    invoke-static {v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$1600(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mToggle = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mToggle:Z
    invoke-static {v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$1700(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$300(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->closeOptionsMenu()V

    .line 443
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseChnUsageAlertPopup:Z

    if-ne v1, v4, :cond_2

    .line 444
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$300(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isWifiConnected(Landroid/content/Context;)Z

    move-result v1

    if-eq v1, v4, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$300(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isWifiP2pConnected(Landroid/content/Context;)Z

    move-result v1

    if-ne v1, v4, :cond_2

    .line 445
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$300(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    const-string v2, "ScanNearbyDeviceWifiDataAlertDialogOff"

    invoke-static {v1, v2, v5}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_2

    sget-boolean v1, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->execNearbyPopup:Z

    if-nez v1, :cond_2

    .line 448
    sput-boolean v4, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->execNearbyPopup:Z

    .line 449
    new-array v0, v6, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$300(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 452
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$300(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "SHOW_USAGE_ALERT_DIALOG"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 492
    .end local v0    # "params":[Ljava/lang/Object;
    :cond_1
    :goto_0
    return-void

    .line 458
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->checkGifMode()Z

    move-result v1

    if-nez v1, :cond_1

    .line 461
    invoke-direct {p0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->bringDrawerToFront()V

    .line 462
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ActionBarDrawerToggle;->onDrawerSlide(Landroid/view/View;F)V

    .line 464
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # setter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mSlideOffset:F
    invoke-static {v1, p2}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$1802(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;F)F

    .line 465
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mTouched:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$1600(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mToggle:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$1700(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mSlideOffset:F
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$1800(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_5

    .line 466
    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$1500()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onDrawerSlide drawerClosed ******** "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    invoke-super {p0, p1}, Landroid/support/v4/app/ActionBarDrawerToggle;->onDrawerClosed(Landroid/view/View;)V

    .line 468
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # invokes: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->drawerClosed()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$1900(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)V

    .line 469
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # setter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mToggle:Z
    invoke-static {v1, v5}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$1702(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;Z)Z

    .line 470
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseChnUsageAlertPopup:Z

    if-ne v1, v4, :cond_4

    .line 471
    sput-boolean v5, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->execNearbyPopup:Z

    .line 491
    :cond_4
    :goto_1
    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$1500()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onDrawerSlide end: isDrawerVisible = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->isDrawerVisible()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 473
    :cond_5
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mTouched:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$1600(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mToggle:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$1700(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_6
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mSlideOffset:F
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$1800(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_7

    .line 474
    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$1500()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onDrawerSlide drawerOpened ******** "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    invoke-super {p0, p1}, Landroid/support/v4/app/ActionBarDrawerToggle;->onDrawerOpened(Landroid/view/View;)V

    .line 477
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # invokes: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->drawerOpened()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$2000(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)V

    .line 478
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$100(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->hasFilterBy()Z

    move-result v1

    if-eqz v1, :cond_4

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v1, :cond_4

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOverlayHelp:Z

    if-eqz v1, :cond_4

    .line 479
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerHelp:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$2100(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->addView()V

    .line 480
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerHelp:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$2100(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->isShowingHelpView()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 481
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerHelp:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$2100(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->bringToFront()V

    goto :goto_1

    .line 485
    :cond_7
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$2200(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-ne v1, v6, :cond_4

    .line 486
    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$1500()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onDrawerSlide ******** "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerList:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$2300(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Landroid/widget/ListView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;
    invoke-static {v2}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$100(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 488
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$12;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$2200(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1
.end method
