.class public Lcom/sec/samsung/gallery/controller/AddSlideshowMusicCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "AddSlideshowMusicCmd.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method private addMusic(Landroid/content/Context;ILandroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    .line 34
    const/4 v4, -0x1

    if-ne p2, v4, :cond_0

    .line 35
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 36
    .local v1, "musicUri":Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 37
    .local v0, "music":Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->load(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v3

    .line 38
    .local v3, "settings":Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    const/4 v4, 0x3

    invoke-virtual {v3, v4, v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->addSetting(ILjava/lang/Object;)V

    .line 39
    const/4 v4, 0x5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->addSetting(ILjava/lang/Object;)V

    .line 40
    const/4 v4, 0x6

    invoke-virtual {v3, v4, v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->addSetting(ILjava/lang/Object;)V

    .line 41
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getMusicsAdded()Ljava/util/Set;

    move-result-object v2

    .line 42
    .local v2, "musicsAdded":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 43
    invoke-static {p1, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->save(Landroid/content/Context;Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;)V

    .line 45
    check-cast p1, Lcom/sec/android/gallery3d/app/GalleryActivity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v4

    const-string v5, "SLIDESHOW_MUSIC_UPDATE"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 51
    .end local v0    # "music":Ljava/lang/String;
    .end local v1    # "musicUri":Landroid/net/Uri;
    .end local v2    # "musicsAdded":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v3    # "settings":Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    :goto_0
    return-void

    .line 49
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    sput-boolean v5, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->LOAD_MUSIC_NONE_SELECTED:Z

    goto :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 5
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 25
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    move-object v2, v4

    check-cast v2, [Ljava/lang/Object;

    .line 26
    .local v2, "params":[Ljava/lang/Object;
    const/4 v4, 0x0

    aget-object v0, v2, v4

    check-cast v0, Landroid/content/Context;

    .line 27
    .local v0, "context":Landroid/content/Context;
    const/4 v4, 0x1

    aget-object v4, v2, v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 28
    .local v3, "resultCode":I
    const/4 v4, 0x2

    aget-object v1, v2, v4

    check-cast v1, Landroid/content/Intent;

    .line 30
    .local v1, "data":Landroid/content/Intent;
    invoke-direct {p0, v0, v3, v1}, Lcom/sec/samsung/gallery/controller/AddSlideshowMusicCmd;->addMusic(Landroid/content/Context;ILandroid/content/Intent;)V

    .line 31
    return-void
.end method
