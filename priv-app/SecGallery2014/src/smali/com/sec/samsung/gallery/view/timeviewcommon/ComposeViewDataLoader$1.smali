.class Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$1;
.super Ljava/lang/Object;
.source "ComposeViewDataLoader.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;)V
    .locals 0

    .prologue
    .line 1187
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$1;->this$0:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAllContentReady()V
    .locals 2

    .prologue
    .line 1214
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$1;->this$0:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstReloadSkip:Z

    .line 1215
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$1;->this$0:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mModelListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

    if-eqz v0, :cond_0

    .line 1216
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$1;->this$0:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mModelListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;->onAllContentReady()V

    .line 1218
    :cond_0
    return-void
.end method

.method public onBitmapAvailable(Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;)V
    .locals 6
    .param p1, "item"    # Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    .prologue
    .line 1190
    :try_start_0
    iget-object v2, p1, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 1191
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$1;->this$0:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    # getter for: Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->access$000(Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;)Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    move-result-object v3

    if-nez v3, :cond_1

    .line 1210
    .end local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    :goto_0
    return-void

    .line 1193
    .restart local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$1;->this$0:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    # getter for: Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->access$000(Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;)Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    move-result-object v3

    iget-object v4, p1, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3, v2, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->drawDecorViewOnRequest(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v3

    iput-object v3, p1, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mGlView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 1195
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$1;->this$0:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    # getter for: Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mIsActive:Z
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->access$100(Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1198
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$1;->this$0:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    # getter for: Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstItemOnly:Z
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->access$200(Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1199
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$1;->this$0:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    iget v4, p1, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mIndex:I

    shr-int/lit8 v4, v4, 0x10

    aget-object v0, v3, v4

    .line 1200
    .local v0, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    if-eqz v2, :cond_2

    .line 1201
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v3

    iput v3, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemCount:I

    .line 1204
    .end local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$1;->this$0:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mViewListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;

    if-eqz v3, :cond_0

    .line 1205
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$1;->this$0:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mViewListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;

    iget v4, p1, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mIndex:I

    iget v5, p1, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mType:I

    invoke-interface {v3, v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;->onContentUpdated(II)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1207
    .end local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :catch_0
    move-exception v1

    .line 1208
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method
