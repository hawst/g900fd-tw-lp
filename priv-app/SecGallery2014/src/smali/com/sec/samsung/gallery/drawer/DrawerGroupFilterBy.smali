.class public Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;
.super Lcom/sec/samsung/gallery/drawer/AbstractDrawerGroup;
.source "DrawerGroupFilterBy.java"


# static fields
.field public static FILTERBY_ICON_MAP:Ljava/util/LinkedHashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final MSG_ADD_FILTERBY_NOTIFY_DATA_SET_CHANGED:I = 0x2

.field private static final MSG_NOTIFY_DATA_SET_CHANGED:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field private static final sFilterUpdateThread:Landroid/os/HandlerThread;


# instance fields
.field private final CATEGORY_VISIBLE_VALUE:I

.field private mCategoryMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mCategoryObserver:Landroid/database/ContentObserver;

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mDrawerMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field public mEventAlbumVisible:Z

.field private mEventMgr:Lcom/sec/android/gallery3d/data/EventAlbumManager;

.field private mEventObserver:Landroid/database/ContentObserver;

.field private mFilterByHandler:Landroid/os/Handler;

.field private mFilterUpdateHandler:Landroid/os/Handler;

.field private mFilterbyGroup:Lcom/sec/samsung/gallery/drawer/Group;

.field private mFilterbySettingMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

.field public mPeopleAlbumVisible:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 43
    const-class v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->TAG:Ljava/lang/String;

    .line 60
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "DrawerCategoryThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->sFilterUpdateThread:Landroid/os/HandlerThread;

    .line 64
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->FILTERBY_ICON_MAP:Ljava/util/LinkedHashMap;

    .line 65
    sget-object v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->FILTERBY_ICON_MAP:Ljava/util/LinkedHashMap;

    const-string v1, "People"

    const v2, 0x7f02018b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->FILTERBY_ICON_MAP:Ljava/util/LinkedHashMap;

    const-string v1, "Scenery"

    const v2, 0x7f02018e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->FILTERBY_ICON_MAP:Ljava/util/LinkedHashMap;

    const-string v1, "Documents"

    const v2, 0x7f020187

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->FILTERBY_ICON_MAP:Ljava/util/LinkedHashMap;

    const-string v1, "Food"

    const v2, 0x7f02018a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->FILTERBY_ICON_MAP:Ljava/util/LinkedHashMap;

    const-string v1, "Pets"

    const v2, 0x7f02018c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sget-object v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->FILTERBY_ICON_MAP:Ljava/util/LinkedHashMap;

    const-string v1, "Vehicles"

    const v2, 0x7f020191

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->FILTERBY_ICON_MAP:Ljava/util/LinkedHashMap;

    const-string v1, "Flower"

    const v2, 0x7f020189

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->sFilterUpdateThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 74
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/drawer/IDrawerCallback;)V
    .locals 3
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "callback"    # Lcom/sec/samsung/gallery/drawer/IDrawerCallback;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 146
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/drawer/AbstractDrawerGroup;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/drawer/IDrawerCallback;)V

    .line 45
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->CATEGORY_VISIBLE_VALUE:I

    .line 51
    iput-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterbyGroup:Lcom/sec/samsung/gallery/drawer/Group;

    .line 55
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mEventAlbumVisible:Z

    .line 56
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mPeopleAlbumVisible:Z

    .line 57
    iput-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mCategoryMap:Ljava/util/HashMap;

    .line 58
    iput-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterbySettingMap:Ljava/util/HashMap;

    .line 76
    new-instance v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$1;-><init>(Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterByHandler:Landroid/os/Handler;

    .line 101
    new-instance v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$2;

    sget-object v1, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->sFilterUpdateThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$2;-><init>(Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterUpdateHandler:Landroid/os/Handler;

    .line 111
    new-instance v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$3;

    const-string v1, "EVENT_DRAWER"

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$3;-><init>(Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mDrawerMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 129
    new-instance v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$4;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$4;-><init>(Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mCategoryObserver:Landroid/database/ContentObserver;

    .line 137
    new-instance v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$5;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$5;-><init>(Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mEventObserver:Landroid/database/ContentObserver;

    .line 147
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mContentResolver:Landroid/content/ContentResolver;

    .line 148
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/EventAlbumManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mEventMgr:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    .line 150
    invoke-static {p1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    .line 152
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mDrawerMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mDrawerMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 155
    :cond_0
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->isContainViewFilterTypeOfLocal()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;)Lcom/sec/samsung/gallery/drawer/Group;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterbyGroup:Lcom/sec/samsung/gallery/drawer/Group;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->isNeedToUpdateFilter()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterByHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private checkCategoryItems()Z
    .locals 15

    .prologue
    const/4 v14, 0x0

    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 271
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 272
    .local v6, "categoryMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v8, 0x0

    .line 274
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->DCM_CATEGORY_TABLE_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "category_name"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "media_count"

    aput-object v4, v2, v3

    const-string v3, "category_type=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v13, "4"

    aput-object v13, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 281
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 283
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 284
    .local v10, "name":Ljava/lang/String;
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 285
    .local v7, "count":I
    sget-object v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkCategoryItems : [DCM query] category = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", count = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v10, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 293
    .end local v7    # "count":I
    .end local v10    # "name":Ljava/lang/String;
    :cond_1
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 296
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mCategoryMap:Ljava/util/HashMap;

    if-nez v0, :cond_2

    .line 297
    iput-object v6, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mCategoryMap:Ljava/util/HashMap;

    move v0, v11

    .line 309
    :goto_1
    return v0

    .line 290
    :catch_0
    move-exception v9

    .line 291
    .local v9, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 293
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    .line 301
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mCategoryMap:Ljava/util/HashMap;

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v12

    .line 302
    goto :goto_1

    .line 304
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mCategoryMap:Ljava/util/HashMap;

    if-eqz v0, :cond_4

    .line 305
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mCategoryMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 306
    iput-object v14, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mCategoryMap:Ljava/util/HashMap;

    .line 308
    :cond_4
    iput-object v6, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mCategoryMap:Ljava/util/HashMap;

    move v0, v11

    .line 309
    goto :goto_1
.end method

.method private isContainViewFilterTypeOfLocal()Z
    .locals 4

    .prologue
    .line 191
    const/4 v0, 0x0

    .line 193
    .local v0, "retValue":Z
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v1

    .line 194
    .local v1, "viewByTypeIndex":I
    sget-object v2, Lcom/sec/samsung/gallery/core/ViewByFilterType;->LOCAL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 196
    :cond_0
    const/4 v0, 0x1

    .line 199
    :cond_1
    if-eqz v0, :cond_2

    .line 200
    sget-object v2, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->TAG:Ljava/lang/String;

    const-string v3, "isContainViewFilterTypeOfLocal() : contains local contents"

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    :cond_2
    return v0
.end method

.method private isNeedToUpdateFilter()Z
    .locals 3

    .prologue
    .line 220
    const/4 v1, 0x0

    .line 221
    .local v1, "retValue":Z
    invoke-direct {p0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->isContainViewFilterTypeOfLocal()Z

    move-result v0

    .line 223
    .local v0, "bIncludeLocalType":Z
    if-eqz v0, :cond_1

    .line 224
    const/4 v1, 0x1

    .line 230
    :cond_0
    :goto_0
    return v1

    .line 226
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterbyGroup:Lcom/sec/samsung/gallery/drawer/Group;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterbyGroup:Lcom/sec/samsung/gallery/drawer/Group;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/drawer/Group;->getChildrenCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 227
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isPickerMode()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 314
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    instance-of v2, v2, Landroid/app/Activity;

    if-eqz v2, :cond_1

    .line 315
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 316
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 317
    const-string v2, "android.intent.action.GET_CONTENT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "android.intent.action.PICK"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "android.intent.action.PERSON_PICK"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "android.intent.action.MULTIPLE_PICK"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 322
    .end local v0    # "action":Ljava/lang/String;
    :cond_1
    return v1
.end method


# virtual methods
.method public addFilterBy()V
    .locals 14

    .prologue
    .line 433
    sget-object v10, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->CATEGORY_STRING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v10}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v2

    .line 435
    .local v2, "categoryStrings":[Ljava/lang/Object;
    iget-object v10, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterbyGroup:Lcom/sec/samsung/gallery/drawer/Group;

    invoke-virtual {v10}, Lcom/sec/samsung/gallery/drawer/Group;->removeChildren()V

    .line 436
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->updateEventItem()V

    .line 437
    iget-object v10, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mCategoryMap:Ljava/util/HashMap;

    if-eqz v10, :cond_1

    .line 438
    move-object v1, v2

    .local v1, "arr$":[Ljava/lang/Object;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_1

    aget-object v7, v1, v5

    .local v7, "object":Ljava/lang/Object;
    move-object v3, v7

    .line 439
    check-cast v3, Ljava/lang/String;

    .line 440
    .local v3, "categoryType":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mCategoryMap:Ljava/util/HashMap;

    invoke-virtual {v10, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 441
    .local v0, "ItemCount":Ljava/lang/Integer;
    sget-object v10, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "addFilterBy : categoryType = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", itemCount = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v10

    const/4 v11, 0x1

    if-lt v10, v11, :cond_0

    iget-object v10, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterbySettingMap:Ljava/util/HashMap;

    invoke-virtual {v10, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 443
    sget-object v10, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->CATEGORY_STRING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v10, v3}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 444
    .local v8, "stringId":I
    sget-object v10, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->CATEGORY_TTS_STRING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v10, v3}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 446
    .local v9, "ttsId":I
    :try_start_0
    iget-object v11, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterbyGroup:Lcom/sec/samsung/gallery/drawer/Group;

    const v12, 0x7f0e04cb

    sget-object v10, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->FILTERBY_ICON_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v10, v3}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v13

    sget-object v10, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->CATEGORY_TYPE_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v10, v3}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-static {v12, v8, v9, v13, v10}, Lcom/sec/samsung/gallery/drawer/DrawerItemFilterBy;->createFilterbyItems(IIIILcom/sec/samsung/gallery/core/TabTagType;)Lcom/sec/samsung/gallery/drawer/DrawerItem;

    move-result-object v10

    invoke-virtual {v11, v10}, Lcom/sec/samsung/gallery/drawer/Group;->addChild(Lcom/sec/samsung/gallery/drawer/DrawerItem;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 438
    .end local v8    # "stringId":I
    .end local v9    # "ttsId":I
    :cond_0
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 448
    .restart local v8    # "stringId":I
    .restart local v9    # "ttsId":I
    :catch_0
    move-exception v4

    .line 449
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 454
    .end local v0    # "ItemCount":Ljava/lang/Integer;
    .end local v1    # "arr$":[Ljava/lang/Object;
    .end local v3    # "categoryType":Ljava/lang/String;
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "object":Ljava/lang/Object;
    .end local v8    # "stringId":I
    .end local v9    # "ttsId":I
    :cond_1
    sget-boolean v10, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-nez v10, :cond_2

    .line 455
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->updatePeopleItem()V

    .line 458
    :cond_2
    return-void
.end method

.method public checkEvent()Z
    .locals 4

    .prologue
    .line 380
    const/4 v0, 0x0

    .line 381
    .local v0, "changed":Z
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mEventMgr:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->hasEventItem(Landroid/content/Context;)Z

    move-result v1

    .line 382
    .local v1, "eventAlbumVisible":Z
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mEventAlbumVisible:Z

    if-eq v1, v2, :cond_0

    .line 383
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mEventAlbumVisible:Z

    .line 384
    const/4 v0, 0x1

    .line 386
    :cond_0
    return v0
.end method

.method public checkFilterbySettingData()Ljava/lang/Boolean;
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 235
    const/4 v3, 0x0

    .line 236
    .local v3, "changedSetting":Z
    sget-object v9, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->CATEGORY_STRING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v9}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v2

    .line 237
    .local v2, "categoryStrings":[Ljava/lang/Object;
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 239
    .local v4, "filterbySettingMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    iget-object v9, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v9}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 241
    .local v7, "prefs":Landroid/content/SharedPreferences;
    const-string v9, "filterby_Event"

    invoke-interface {v7, v9, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 242
    const-string v9, "event"

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    :goto_0
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/Object;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_1
    if-ge v5, v6, :cond_2

    aget-object v1, v0, v5

    .local v1, "categoryString":Ljava/lang/Object;
    move-object v8, v1

    .line 248
    check-cast v8, Ljava/lang/String;

    .line 249
    .local v8, "type":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "filterby_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v9, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 250
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 244
    .end local v0    # "arr$":[Ljava/lang/Object;
    .end local v1    # "categoryString":Ljava/lang/Object;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v8    # "type":Ljava/lang/String;
    :cond_0
    const-string v9, "event"

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 252
    .restart local v0    # "arr$":[Ljava/lang/Object;
    .restart local v1    # "categoryString":Ljava/lang/Object;
    .restart local v5    # "i$":I
    .restart local v6    # "len$":I
    .restart local v8    # "type":Ljava/lang/String;
    :cond_1
    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 255
    .end local v1    # "categoryString":Ljava/lang/Object;
    .end local v8    # "type":Ljava/lang/String;
    :cond_2
    iget-object v9, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterbySettingMap:Ljava/util/HashMap;

    if-eqz v9, :cond_4

    iget-object v9, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterbySettingMap:Ljava/util/HashMap;

    invoke-virtual {v9, v4}, Ljava/util/HashMap;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 256
    iget-object v9, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterbySettingMap:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->clear()V

    .line 257
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterbySettingMap:Ljava/util/HashMap;

    .line 258
    const/4 v3, 0x1

    .line 262
    :goto_3
    iget-object v9, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterbySettingMap:Ljava/util/HashMap;

    if-nez v9, :cond_3

    .line 263
    iput-object v4, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterbySettingMap:Ljava/util/HashMap;

    .line 264
    const/4 v3, 0x1

    .line 267
    :cond_3
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    return-object v9

    .line 260
    :cond_4
    const/4 v3, 0x0

    goto :goto_3
.end method

.method public checkPeopleAndEvent()Z
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 341
    const/4 v7, 0x0

    .line 342
    .local v7, "changedPeople":Z
    const/4 v6, 0x0

    .line 343
    .local v6, "changedEvent":Z
    const/4 v10, 0x0

    .line 345
    .local v10, "peopleAlbumVisible":Z
    const/4 v9, 0x0

    .line 346
    .local v9, "faceCursor":Landroid/database/Cursor;
    const/4 v8, -0x1

    .line 349
    .local v8, "faceCount":I
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mContentResolver:Landroid/content/ContentResolver;

    const-string v1, "content://media/external/file"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "face_count"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 353
    if-eqz v9, :cond_1

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 355
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 356
    if-lez v8, :cond_4

    .line 357
    const/4 v10, 0x1

    .line 363
    :cond_1
    :goto_0
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 366
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mPeopleAlbumVisible:Z

    if-eq v0, v10, :cond_2

    .line 367
    iput-boolean v10, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mPeopleAlbumVisible:Z

    .line 368
    const/4 v7, 0x1

    .line 371
    :cond_2
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->checkEvent()Z

    move-result v6

    .line 373
    if-nez v7, :cond_3

    if-eqz v6, :cond_5

    :cond_3
    move v0, v12

    .line 376
    :goto_1
    return v0

    .line 360
    :cond_4
    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 363
    :catchall_0
    move-exception v0

    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    :cond_5
    move v0, v11

    .line 376
    goto :goto_1
.end method

.method public getGroup()Lcom/sec/samsung/gallery/drawer/Group;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterbyGroup:Lcom/sec/samsung/gallery/drawer/Group;

    return-object v0
.end method

.method public initialize()V
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->updateFilterByGroup(Z)V

    .line 160
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mCallback:Lcom/sec/samsung/gallery/drawer/IDrawerCallback;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/drawer/IDrawerCallback;->onUpdate()V

    .line 161
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v1, "EVENT_DRAWER"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 184
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mCategoryObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 178
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mEventObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 179
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 166
    invoke-static {}, Lcom/sec/android/gallery3d/settings/FilterbySettingActivity;->getUpdateState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v1, "UPDATE_CATEGORY"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 168
    invoke-static {}, Lcom/sec/android/gallery3d/settings/FilterbySettingActivity;->resetUpdateState()V

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->DCM_CATEGORY_TABLE_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mCategoryObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 172
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->EVENT_ALBUM_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mEventObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 173
    return-void
.end method

.method public updateEventAndCategoryItems(Z)Z
    .locals 6
    .param p1, "checkAllFilter"    # Z

    .prologue
    const/4 v3, 0x0

    .line 420
    iget-object v4, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mEventMgr:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    iget-object v5, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->hasEventItem(Landroid/content/Context;)Z

    move-result v2

    .line 421
    .local v2, "eventAlbumVisible":Z
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->checkFilterbySettingData()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 422
    .local v1, "changedFilter":Z
    :goto_0
    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->checkCategoryItems()Z

    move-result v0

    .line 424
    .local v0, "changedCategory":Z
    :goto_1
    if-nez v1, :cond_2

    if-nez v0, :cond_2

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mEventAlbumVisible:Z

    if-ne v2, v4, :cond_2

    .line 429
    :goto_2
    return v3

    .end local v0    # "changedCategory":Z
    .end local v1    # "changedFilter":Z
    :cond_0
    move v1, v3

    .line 421
    goto :goto_0

    .restart local v1    # "changedFilter":Z
    :cond_1
    move v0, v3

    .line 422
    goto :goto_1

    .line 428
    .restart local v0    # "changedCategory":Z
    :cond_2
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mEventAlbumVisible:Z

    .line 429
    const/4 v3, 0x1

    goto :goto_2
.end method

.method public updateEventItem()V
    .locals 7

    .prologue
    const v6, 0x7f0e0441

    .line 390
    const/4 v2, 0x1

    .line 391
    .local v2, "setting":Z
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v3, :cond_0

    .line 392
    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 393
    .local v1, "prefs":Landroid/content/SharedPreferences;
    const-string v3, "filterby_Event"

    const/4 v4, 0x1

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 396
    .end local v1    # "prefs":Landroid/content/SharedPreferences;
    :cond_0
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEventView:Z

    if-eqz v3, :cond_1

    if-eqz v2, :cond_1

    .line 397
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mEventAlbumVisible:Z

    if-eqz v3, :cond_1

    .line 398
    const/4 v0, -0x1

    .line 399
    .local v0, "eventRsrc":I
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v3, :cond_2

    .line 400
    const v0, 0x7f020188

    .line 404
    :goto_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterbyGroup:Lcom/sec/samsung/gallery/drawer/Group;

    const v4, 0x7f0e04cb

    sget-object v5, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_EVENT:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-static {v4, v6, v6, v0, v5}, Lcom/sec/samsung/gallery/drawer/DrawerItemFilterBy;->createFilterbyItems(IIIILcom/sec/samsung/gallery/core/TabTagType;)Lcom/sec/samsung/gallery/drawer/DrawerItem;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/drawer/Group;->addChild(Lcom/sec/samsung/gallery/drawer/DrawerItem;)V

    .line 408
    .end local v0    # "eventRsrc":I
    :cond_1
    return-void

    .line 402
    .restart local v0    # "eventRsrc":I
    :cond_2
    const v0, 0x7f020215

    goto :goto_0
.end method

.method public updateFilterByGroup(Z)V
    .locals 5
    .param p1, "checkAllFilter"    # Z

    .prologue
    const/4 v0, 0x1

    .line 207
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEventView:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->isPickerMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 210
    :cond_1
    sget-object v1, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->TAG:Ljava/lang/String;

    const-string v2, "drawer updateFilterByGroup+"

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterbyGroup:Lcom/sec/samsung/gallery/drawer/Group;

    if-nez v1, :cond_2

    .line 212
    new-instance v1, Lcom/sec/samsung/gallery/drawer/Group;

    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0e04cb

    const v4, 0x7f0e02ae

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/sec/samsung/gallery/drawer/Group;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;III)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterbyGroup:Lcom/sec/samsung/gallery/drawer/Group;

    .line 215
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterUpdateHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 216
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterUpdateHandler:Landroid/os/Handler;

    if-eqz p1, :cond_3

    :goto_1
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public updateFilterByItems(Z)Z
    .locals 2
    .param p1, "checkAllFilter"    # Z

    .prologue
    .line 327
    const/4 v0, 0x0

    .line 329
    .local v0, "changed":Z
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v1, :cond_1

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEventView:Z

    if-eqz v1, :cond_1

    .line 330
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->updateEventAndCategoryItems(Z)Z

    move-result v0

    .line 337
    :cond_0
    :goto_0
    return v0

    .line 331
    :cond_1
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-nez v1, :cond_2

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEventView:Z

    if-eqz v1, :cond_2

    .line 332
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->checkPeopleAndEvent()Z

    move-result v0

    goto :goto_0

    .line 333
    :cond_2
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEventView:Z

    if-eqz v1, :cond_0

    .line 334
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->checkEvent()Z

    move-result v0

    goto :goto_0
.end method

.method public updatePeopleItem()V
    .locals 5

    .prologue
    const v4, 0x7f0e00a7

    .line 411
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-nez v0, :cond_0

    .line 412
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mPeopleAlbumVisible:Z

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterbyGroup:Lcom/sec/samsung/gallery/drawer/Group;

    const v1, 0x7f0e04cb

    const v2, 0x7f02018b

    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-static {v1, v4, v4, v2, v3}, Lcom/sec/samsung/gallery/drawer/DrawerItemFilterBy;->createFilterbyItems(IIIILcom/sec/samsung/gallery/core/TabTagType;)Lcom/sec/samsung/gallery/drawer/DrawerItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/Group;->addChild(Lcom/sec/samsung/gallery/drawer/DrawerItem;)V

    .line 417
    :cond_0
    return-void
.end method
