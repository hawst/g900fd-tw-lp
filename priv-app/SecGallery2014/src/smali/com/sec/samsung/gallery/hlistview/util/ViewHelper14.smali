.class public Lcom/sec/samsung/gallery/hlistview/util/ViewHelper14;
.super Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelperDefault;
.source "ViewHelper14.java"


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelperDefault;-><init>(Landroid/view/View;)V

    .line 11
    return-void
.end method


# virtual methods
.method public isHardwareAccelerated()Z
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/util/ViewHelper14;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isHardwareAccelerated()Z

    move-result v0

    return v0
.end method

.method public setScrollX(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/util/ViewHelper14;->view:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setScrollX(I)V

    .line 16
    return-void
.end method
