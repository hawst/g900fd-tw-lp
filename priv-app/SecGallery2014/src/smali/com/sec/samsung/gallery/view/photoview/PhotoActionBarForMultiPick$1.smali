.class Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;
.super Ljava/lang/Object;
.source "PhotoActionBarForMultiPick.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->setTitle(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

.field final synthetic val$numberOfItemsSelected:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;I)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

    iput p2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 64
    const/4 v2, 0x0

    .line 65
    .local v2, "numberOfSelectedItems":Ljava/lang/String;
    const/4 v0, 0x0

    .line 66
    .local v0, "contentDescription":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

    iget v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    iput v8, v7, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mCount:I

    .line 67
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mMenu:Landroid/view/Menu;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->access$000(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;)Landroid/view/Menu;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 68
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->access$100(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->invalidateOptionsMenu()V

    .line 70
    :cond_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->access$200(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 71
    .local v4, "res":Landroid/content/res/Resources;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->getMaxCount()I

    move-result v1

    .line 72
    .local v1, "maxCount":I
    if-lez v1, :cond_4

    .line 73
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->access$300(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isWQHD(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v7

    if-nez v7, :cond_1

    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionOnActionBar:Z

    if-eqz v7, :cond_3

    :cond_1
    const v3, 0x7f0e0051

    .line 75
    .local v3, "numberOfSelectedItemsStringID":I
    :goto_0
    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v10, [Ljava/lang/Object;

    iget v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v5

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 76
    const v7, 0x7f0e0052

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v10, [Ljava/lang/Object;

    iget v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v5

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 78
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionBar:Z

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->access$400(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;)Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    move-result-object v7

    if-eqz v7, :cond_2

    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisplaySelectAllButtonInLimitationPickMode:Z

    if-nez v7, :cond_2

    .line 79
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->access$500(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;)Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    move-result-object v7

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->setVisibility(I)V

    .line 89
    .end local v3    # "numberOfSelectedItemsStringID":I
    :cond_2
    :goto_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

    iget v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    # invokes: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->setSelectAllButtonTitle(ILjava/lang/String;)V
    invoke-static {v7, v8, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;ILjava/lang/String;)V

    .line 90
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

    # invokes: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->setSelectAllButtonTTS(Ljava/lang/String;)V
    invoke-static {v7, v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->access$700(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;Ljava/lang/String;)V

    .line 91
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mMenu:Landroid/view/Menu;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->access$000(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;)Landroid/view/Menu;

    move-result-object v7

    const v8, 0x7f0f0024

    iget v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    if-lez v9, :cond_6

    :goto_2
    invoke-static {v7, v8, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 93
    return-void

    .line 73
    :cond_3
    const v3, 0x7f0e0050

    goto :goto_0

    .line 80
    :cond_4
    iget v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    if-ltz v7, :cond_2

    .line 81
    iget v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    if-ne v7, v5, :cond_5

    .line 82
    const v7, 0x7f0e004f

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v5, [Ljava/lang/Object;

    iget v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 86
    :goto_3
    move-object v0, v2

    goto :goto_1

    .line 84
    :cond_5
    const v7, 0x7f0e004e

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v5, [Ljava/lang/Object;

    iget v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :cond_6
    move v5, v6

    .line 91
    goto :goto_2
.end method
