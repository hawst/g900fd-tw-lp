.class Lcom/sec/samsung/gallery/model/ThumbnailProxy$3;
.super Ljava/lang/Object;
.source "ThumbnailProxy.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem$OnBitmapAvailableListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/model/ThumbnailProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)V
    .locals 0

    .prologue
    .line 667
    iput-object p1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$3;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBitmapAvailable(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    const/4 v3, 0x0

    .line 669
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$3;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;
    invoke-static {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$1300(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    move-result-object v0

    invoke-virtual {v0, p1, v3}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->notifyDataSetChanged(II)V

    .line 670
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$3;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$1700(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$3;->this$0:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    # getter for: Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->access$1700(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 671
    return-void
.end method
