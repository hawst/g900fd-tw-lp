.class public Lcom/sec/samsung/gallery/decoder/LargeImage;
.super Ljava/lang/Object;
.source "LargeImage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/decoder/LargeImage$1;,
        Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;,
        Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileUploader;,
        Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageListener;,
        Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageJob;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "LargeImage"

.field private static final UPLOAD_LIMIT:I = 0x32


# instance fields
.field private FILL_COLOR:I

.field private largeImageTask:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Landroid/util/LongSparseArray",
            "<",
            "Lcom/sec/samsung/gallery/decoder/LargeImageTile;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field public final mDecodeLock:Ljava/lang/Object;

.field public mDecodedTileList:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Lcom/sec/samsung/gallery/decoder/LargeImageTile;",
            ">;"
        }
    .end annotation
.end field

.field private final mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

.field public volatile mDecodingComplete:Z

.field public volatile mDecodingStarted:Z

.field private mFillBackground:Z

.field public final mImageHeight:I

.field public final mImageWidth:I

.field private final mItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mLargeImageTilePool:Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;

.field public mLevel:[I

.field private final mLevelCount:I

.field private volatile mNeedClear:Z

.field private final mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

.field private final mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

.field private final mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

.field public mSize:[I

.field private mTileBorder:I

.field private mTileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/decoder/LargeImageTile;",
            ">;"
        }
    .end annotation
.end field

.field private mTileRange:Landroid/graphics/Rect;

.field public final mTileSize:I

.field private final mTileUploader:Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileUploader;

.field private final mUploadQueue:Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;

.field private mUploadTileCount:I

.field public volatile mUploadingComplete:Z

.field public mUsePool:Z

.field private final mViewHeight:I

.field private final mViewWidth:I


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaItem;ILcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/app/PhotoDataAdapter;Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;)V
    .locals 8
    .param p1, "regionDecoder"    # Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    .param p2, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p3, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p4, "screenNailWidth"    # I
    .param p5, "photoView"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p6, "photodataadpter"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .param p7, "largeImageTilePool"    # Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x2

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput v4, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadTileCount:I

    .line 69
    iput v5, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mTileBorder:I

    .line 77
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mTileRange:Landroid/graphics/Rect;

    .line 83
    new-instance v1, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;-><init>(Lcom/sec/samsung/gallery/decoder/LargeImage$1;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadQueue:Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;

    .line 84
    new-instance v1, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileUploader;

    invoke-direct {v1, p0, v2}, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileUploader;-><init>(Lcom/sec/samsung/gallery/decoder/LargeImage;Lcom/sec/samsung/gallery/decoder/LargeImage$1;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mTileUploader:Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileUploader;

    .line 90
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mFillBackground:Z

    .line 91
    const v1, -0x777778

    iput v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->FILL_COLOR:I

    .line 93
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mNeedClear:Z

    .line 98
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUsePool:Z

    .line 100
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodeLock:Ljava/lang/Object;

    .line 104
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mTileList:Ljava/util/ArrayList;

    .line 105
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodingStarted:Z

    .line 106
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodingComplete:Z

    .line 107
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadingComplete:Z

    .line 108
    iput-object p1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .line 109
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->useLTN()V

    .line 110
    iput-object p2, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 111
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDecoderInterface()Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    .line 112
    iput-object p6, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .line 113
    iput-object p3, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 114
    invoke-direct {p0, p3}, Lcom/sec/samsung/gallery/decoder/LargeImage;->checkFillBackgroundNeeded(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mFillBackground:Z

    .line 116
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageHeight:I

    .line 117
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageWidth:I

    .line 118
    iput-object p5, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    .line 119
    invoke-virtual {p5}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mViewWidth:I

    .line 120
    invoke-virtual {p5}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mViewHeight:I

    .line 121
    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->getTileSize()I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mTileSize:I

    .line 122
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mTileRange:Landroid/graphics/Rect;

    iget v2, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageWidth:I

    iget v3, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageHeight:I

    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 124
    iget v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageWidth:I

    int-to-float v1, v1

    int-to-float v2, p4

    div-float/2addr v1, v2

    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->ceilLog2(F)I

    move-result v1

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevelCount:I

    .line 125
    new-array v1, v7, [I

    iput-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevel:[I

    .line 126
    iget v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mViewWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mViewHeight:I

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageHeight:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 127
    .local v0, "imageScale":F
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevel:[I

    div-float v2, v6, v0

    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->floorLog2(F)I

    move-result v2

    iget v3, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevelCount:I

    invoke-static {v2, v4, v3}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v2

    aput v2, v1, v4

    .line 129
    iget v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mViewWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mViewHeight:I

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageWidth:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 130
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevel:[I

    div-float v2, v6, v0

    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->floorLog2(F)I

    move-result v2

    iget v3, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevelCount:I

    invoke-static {v2, v4, v3}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v2

    aput v2, v1, v5

    .line 131
    new-array v1, v7, [I

    iput-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mSize:[I

    .line 132
    iput-object p7, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLargeImageTilePool:Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;

    .line 133
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUsePool:Z

    if-nez v1, :cond_0

    .line 134
    invoke-direct {p0}, Lcom/sec/samsung/gallery/decoder/LargeImage;->fillTileList()V

    .line 135
    :cond_0
    return-void
.end method

.method static synthetic access$1000(III)J
    .locals 2
    .param p0, "x0"    # I
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 51
    invoke-static {p0, p1, p2}, Lcom/sec/samsung/gallery/decoder/LargeImage;->makeKey(III)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/decoder/LargeImage;Lcom/sec/android/gallery3d/util/Future;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/decoder/LargeImage;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/util/Future;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/decoder/LargeImage;->updateLargeImage(Lcom/sec/android/gallery3d/util/Future;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/decoder/LargeImage;)Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/decoder/LargeImage;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadQueue:Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/decoder/LargeImage;)Lcom/sec/android/gallery3d/ui/PhotoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/decoder/LargeImage;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/decoder/LargeImage;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/decoder/LargeImage;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mTileRange:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/decoder/LargeImage;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/decoder/LargeImage;

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadTileCount:I

    return v0
.end method

.method static synthetic access$502(Lcom/sec/samsung/gallery/decoder/LargeImage;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/decoder/LargeImage;
    .param p1, "x1"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadTileCount:I

    return p1
.end method

.method static synthetic access$510(Lcom/sec/samsung/gallery/decoder/LargeImage;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/samsung/gallery/decoder/LargeImage;

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadTileCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadTileCount:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/decoder/LargeImage;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/decoder/LargeImage;

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mTileBorder:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/decoder/LargeImage;)Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/decoder/LargeImage;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLargeImageTilePool:Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/decoder/LargeImage;Landroid/util/LongSparseArray;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/decoder/LargeImage;
    .param p1, "x1"    # Landroid/util/LongSparseArray;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/decoder/LargeImage;->clearDecodedTiles(Landroid/util/LongSparseArray;)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/decoder/LargeImage;)Lcom/sec/samsung/gallery/decoder/DecoderInterface;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/decoder/LargeImage;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    return-object v0
.end method

.method private checkFillBackgroundNeeded(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 3
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v1, 0x0

    .line 523
    if-nez p1, :cond_1

    .line 536
    :cond_0
    :goto_0
    return v1

    .line 526
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v0

    .line 527
    .local v0, "mimeType":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 530
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 531
    const-string v2, "image/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "image/png"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "gif"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 532
    invoke-static {p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSignatureFile(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 533
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private cleanUp()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 558
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mNeedClear:Z

    if-eqz v0, :cond_1

    .line 559
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodingStarted:Z

    .line 560
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodingComplete:Z

    .line 561
    invoke-direct {p0}, Lcom/sec/samsung/gallery/decoder/LargeImage;->freeTiles()V

    .line 562
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadingComplete:Z

    .line 569
    :cond_0
    :goto_0
    return-void

    .line 564
    :cond_1
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodingStarted:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodingComplete:Z

    if-nez v0, :cond_0

    .line 565
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodingStarted:Z

    .line 566
    invoke-direct {p0}, Lcom/sec/samsung/gallery/decoder/LargeImage;->freeTiles()V

    goto :goto_0
.end method

.method private clearDecodedTiles(Landroid/util/LongSparseArray;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Lcom/sec/samsung/gallery/decoder/LargeImageTile;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 572
    .local p1, "tileList":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/sec/samsung/gallery/decoder/LargeImageTile;>;"
    if-nez p1, :cond_0

    .line 593
    :goto_0
    return-void

    .line 575
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/decoder/LargeImage;->cleanUp()V

    .line 576
    invoke-virtual {p1}, Landroid/util/LongSparseArray;->size()I

    move-result v3

    .line 577
    .local v3, "n":I
    const/4 v0, 0x0

    .line 578
    .local v0, "count":I
    const/4 v4, 0x0

    .line 579
    .local v4, "tile":Lcom/sec/samsung/gallery/decoder/LargeImageTile;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_3

    .line 580
    invoke-virtual {p1, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "tile":Lcom/sec/samsung/gallery/decoder/LargeImageTile;
    check-cast v4, Lcom/sec/samsung/gallery/decoder/LargeImageTile;

    .line 581
    .restart local v4    # "tile":Lcom/sec/samsung/gallery/decoder/LargeImageTile;
    if-eqz v4, :cond_2

    .line 582
    iget-object v5, v4, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mDecodedTile:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_1

    .line 583
    iget-object v5, v4, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mDecodedTile:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->onFreeBitmap(Landroid/graphics/Bitmap;)V

    .line 584
    const/4 v5, 0x0

    iput-object v5, v4, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mDecodedTile:Landroid/graphics/Bitmap;

    .line 586
    :cond_1
    const/4 v5, 0x4

    iput v5, v4, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mTileState:I

    .line 588
    iget-object v5, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLargeImageTilePool:Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;

    invoke-virtual {v5, v4}, Lcom/sec/samsung/gallery/decoder/LargeImageTilePool;->add(Lcom/sec/samsung/gallery/decoder/LargeImageTile;)Z

    move-result v2

    .line 589
    .local v2, "isAdded":Z
    add-int/lit8 v0, v0, 0x1

    .line 579
    .end local v2    # "isAdded":Z
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 592
    :cond_3
    invoke-virtual {p1}, Landroid/util/LongSparseArray;->clear()V

    goto :goto_0
.end method

.method private fillTileList()V
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 222
    iput v3, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadTileCount:I

    .line 223
    iget-object v8, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mTileRange:Landroid/graphics/Rect;

    .line 224
    .local v8, "r":Landroid/graphics/Rect;
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevel:[I

    aget v0, v0, v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevel:[I

    const/4 v5, 0x1

    aget v4, v4, v5

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 225
    .local v7, "level":I
    iget v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mTileSize:I

    shl-int v9, v0, v7

    .line 227
    .local v9, "size":I
    iget v2, v8, Landroid/graphics/Rect;->top:I

    .local v2, "ty":I
    :goto_0
    iget v0, v8, Landroid/graphics/Rect;->bottom:I

    if-ge v2, v0, :cond_1

    .line 228
    iget v1, v8, Landroid/graphics/Rect;->left:I

    .local v1, "tx":I
    :goto_1
    iget v0, v8, Landroid/graphics/Rect;->right:I

    if-ge v1, v0, :cond_0

    .line 229
    iget-object v10, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mTileList:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;

    move v4, v3

    move v5, v3

    move v6, v3

    invoke-direct/range {v0 .. v7}, Lcom/sec/samsung/gallery/decoder/LargeImageTile;-><init>(IIIIIII)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 230
    iget v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadTileCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadTileCount:I

    .line 228
    add-int/2addr v1, v9

    goto :goto_1

    .line 227
    :cond_0
    add-int/2addr v2, v9

    goto :goto_0

    .line 233
    .end local v1    # "tx":I
    :cond_1
    return-void
.end method

.method private freeTiles()V
    .locals 2

    .prologue
    .line 544
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadQueue:Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;

    monitor-enter v1

    .line 545
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadQueue:Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;->clean()V

    .line 554
    monitor-exit v1

    .line 555
    return-void

    .line 554
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getTileWithoutReusingBitmap(IIII)Landroid/graphics/Bitmap;
    .locals 14
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "level"    # I
    .param p4, "tileSize"    # I

    .prologue
    .line 175
    shl-int v8, p4, p3

    .line 176
    .local v8, "t":I
    new-instance v9, Landroid/graphics/Rect;

    add-int v10, p1, v8

    add-int v11, p2, v8

    move/from16 v0, p2

    invoke-direct {v9, p1, v0, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 181
    .local v9, "wantRegion":Landroid/graphics/Rect;
    monitor-enter p0

    .line 182
    :try_start_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .line 183
    .local v6, "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    if-nez v6, :cond_1

    .line 184
    const/4 v2, 0x0

    monitor-exit p0

    .line 211
    :cond_0
    :goto_0
    return-object v2

    .line 185
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 186
    new-instance v5, Landroid/graphics/Rect;

    const/4 v10, 0x0

    const/4 v11, 0x0

    iget v12, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageWidth:I

    iget v13, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageHeight:I

    invoke-direct {v5, v10, v11, v12, v13}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 187
    .local v5, "overlapRegion":Landroid/graphics/Rect;
    invoke-virtual {v5, v9}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v10

    invoke-static {v10}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 189
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 190
    .local v4, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v10, v4, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 191
    const/4 v10, 0x1

    iput-boolean v10, v4, Landroid/graphics/BitmapFactory$Options;->inPreferQualityOverSpeed:Z

    .line 192
    const/4 v10, 0x1

    shl-int v10, v10, p3

    iput v10, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 193
    const/4 v2, 0x0

    .line 196
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    monitor-enter v6

    .line 197
    :try_start_1
    invoke-virtual {v6, v5, v4}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 198
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 200
    if-nez v2, :cond_2

    .line 201
    const-string v10, "LargeImage"

    const-string v11, "fail in decoding region"

    invoke-static {v10, v11}, Lcom/sec/android/gallery3d/ui/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    const/4 v2, 0x0

    goto :goto_0

    .line 185
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    .end local v4    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v5    # "overlapRegion":Landroid/graphics/Rect;
    .end local v6    # "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    :catchall_0
    move-exception v10

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v10

    .line 198
    .restart local v2    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v4    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v5    # "overlapRegion":Landroid/graphics/Rect;
    .restart local v6    # "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    :catchall_1
    move-exception v10

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v10

    .line 205
    :cond_2
    invoke-virtual {v9, v5}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 208
    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, p4

    move/from16 v1, p4

    invoke-static {v0, v1, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 209
    .local v7, "result":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 210
    .local v3, "canvas":Landroid/graphics/Canvas;
    iget v10, v5, Landroid/graphics/Rect;->left:I

    iget v11, v9, Landroid/graphics/Rect;->left:I

    sub-int/2addr v10, v11

    shr-int v10, v10, p3

    int-to-float v10, v10

    iget v11, v5, Landroid/graphics/Rect;->top:I

    iget v12, v9, Landroid/graphics/Rect;->top:I

    sub-int/2addr v11, v12

    shr-int v11, v11, p3

    int-to-float v11, v11

    const/4 v12, 0x0

    invoke-virtual {v3, v2, v10, v11, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    move-object v2, v7

    .line 211
    goto :goto_0
.end method

.method private static makeKey(III)J
    .locals 7
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "level"    # I

    .prologue
    const/16 v6, 0x10

    .line 252
    int-to-long v0, p0

    .line 253
    .local v0, "result":J
    shl-long v2, v0, v6

    int-to-long v4, p1

    or-long v0, v2, v4

    .line 254
    shl-long v2, v0, v6

    int-to-long v4, p2

    or-long v0, v2, v4

    .line 255
    return-wide v0
.end method

.method private updateLargeImage(Lcom/sec/android/gallery3d/util/Future;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Landroid/util/LongSparseArray",
            "<",
            "Lcom/sec/samsung/gallery/decoder/LargeImageTile;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .local p1, "future":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<Landroid/util/LongSparseArray<Lcom/sec/samsung/gallery/decoder/LargeImageTile;>;>;"
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 392
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mNeedClear:Z

    if-eqz v0, :cond_1

    .line 393
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/LongSparseArray;

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/decoder/LargeImage;->clearDecodedTiles(Landroid/util/LongSparseArray;)V

    .line 406
    :cond_0
    :goto_0
    return-void

    .line 395
    :cond_1
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/LongSparseArray;

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodedTileList:Landroid/util/LongSparseArray;

    .line 396
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodedTileList:Landroid/util/LongSparseArray;

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mSize:[I

    iget v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageHeight:I

    iget v2, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageWidth:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevel:[I

    aget v2, v2, v5

    iget-object v3, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevel:[I

    aget v3, v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    shl-int v2, v4, v2

    div-int/2addr v1, v2

    aput v1, v0, v5

    .line 398
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mSize:[I

    iget v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageHeight:I

    iget v2, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageWidth:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevel:[I

    aget v2, v2, v5

    iget-object v3, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevel:[I

    aget v3, v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    shl-int v2, v4, v2

    div-int/2addr v1, v2

    aput v1, v0, v4

    .line 400
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodingComplete:Z

    .line 401
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-ne v0, v1, :cond_0

    .line 402
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->prepareAlphablendingAnimation()V

    goto :goto_0
.end method


# virtual methods
.method public calculateLTNLevel(IIIII)I
    .locals 4
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "imageW"    # I
    .param p4, "imageH"    # I
    .param p5, "levelCount"    # I

    .prologue
    .line 409
    int-to-float v1, p1

    int-to-float v2, p3

    div-float/2addr v1, v2

    int-to-float v2, p2

    int-to-float v3, p4

    div-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 410
    .local v0, "scale":F
    const/high16 v1, 0x3f800000    # 1.0f

    div-float/2addr v1, v0

    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->floorLog2(F)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v1, v2, p5}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v1

    return v1
.end method

.method public decode()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 215
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodedTileList:Landroid/util/LongSparseArray;

    if-nez v0, :cond_0

    .line 216
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodingStarted:Z

    .line 217
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    new-instance v1, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageJob;

    invoke-direct {v1, p0, v3}, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageJob;-><init>(Lcom/sec/samsung/gallery/decoder/LargeImage;Lcom/sec/samsung/gallery/decoder/LargeImage$1;)V

    new-instance v2, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageListener;

    invoke-direct {v2, p0, v3}, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageListener;-><init>(Lcom/sec/samsung/gallery/decoder/LargeImage;Lcom/sec/samsung/gallery/decoder/LargeImage$1;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->largeImageTask:Lcom/sec/android/gallery3d/util/Future;

    .line 219
    :cond_0
    return-void
.end method

.method public getTile(IIII)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "level"    # I
    .param p4, "tileSize"    # I

    .prologue
    const/4 v8, 0x0

    .line 138
    sget-boolean v2, Lcom/sec/android/gallery3d/common/ApiHelper;->HAS_REUSING_BITMAP_IN_BITMAP_REGION_DECODER:Z

    if-nez v2, :cond_1

    .line 139
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/decoder/LargeImage;->getTileWithoutReusingBitmap(IIII)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 171
    :cond_0
    :goto_0
    return-object v8

    .line 142
    :cond_1
    shl-int v9, p4, p3

    .line 143
    .local v9, "t":I
    new-instance v1, Landroid/graphics/Rect;

    add-int v2, p1, v9

    add-int v3, p2, v9

    invoke-direct {v1, p1, p2, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 145
    .local v1, "wantRegion":Landroid/graphics/Rect;
    const/4 v0, 0x0

    .line 146
    .local v0, "regionDecoder":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    monitor-enter p0

    .line 147
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .line 148
    if-nez v0, :cond_2

    .line 149
    monitor-exit p0

    goto :goto_0

    .line 150
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_2
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154
    const/4 v8, 0x0

    .line 155
    .local v8, "bitmap":Landroid/graphics/Bitmap;
    monitor-enter v0

    .line 156
    const/4 v2, 0x0

    :try_start_2
    iget v3, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageWidth:I

    iget v4, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageHeight:I

    move v5, p4

    move v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->decodeRegionEx(Landroid/graphics/Rect;Landroid/graphics/Rect;IIII)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 157
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 159
    if-nez v8, :cond_3

    .line 160
    const-string v2, "LargeImage"

    const-string v3, "fail in decoding region"

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/ui/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 157
    :catchall_1
    move-exception v2

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2

    .line 164
    :cond_3
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mFillBackground:Z

    if-eqz v2, :cond_0

    .line 165
    invoke-static {}, Lcom/sec/android/photos/data/GalleryBitmapPool;->getInstance()Lcom/sec/android/photos/data/GalleryBitmapPool;

    move-result-object v2

    iget v3, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mTileSize:I

    iget v4, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mTileSize:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/photos/data/GalleryBitmapPool;->get(II)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 166
    .local v7, "bgBitmap":Landroid/graphics/Bitmap;
    iget v2, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->FILL_COLOR:I

    invoke-static {v8, v7, v2}, Lcom/sec/android/gallery3d/common/BitmapUtils;->setBGColor(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;I)V

    .line 167
    iget-object v2, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    invoke-virtual {v2, v8}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->BitmapReuseFreeBitmap(Landroid/graphics/Bitmap;)V

    .line 168
    move-object v8, v7

    goto :goto_0
.end method

.method public isLocalImage()Z
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v0, :cond_0

    .line 517
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/LocalImage;

    .line 519
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUploadCompleted()Z
    .locals 1

    .prologue
    .line 540
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodingComplete:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadingComplete:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isZoomState()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 504
    const/4 v1, 0x0

    .line 505
    .local v1, "result":Z
    iget-object v2, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v2, :cond_1

    .line 507
    iget-object v3, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    monitor-enter v3

    .line 508
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->getCurrentScale()F

    move-result v2

    iget-object v4, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/PhotoView;->getScaleMin()F

    move-result v4

    sub-float v0, v2, v4

    .line 509
    .local v0, "diff":F
    monitor-exit v3

    .line 510
    cmpg-float v2, v0, v5

    if-gez v2, :cond_0

    neg-float v0, v0

    .end local v0    # "diff":F
    :cond_0
    cmpl-float v2, v0, v5

    if-lez v2, :cond_2

    const/4 v1, 0x1

    .line 512
    :cond_1
    :goto_0
    return v1

    .line 509
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 510
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method queueForUpload(Lcom/sec/samsung/gallery/decoder/LargeImageTile;)V
    .locals 3
    .param p1, "tile"    # Lcom/sec/samsung/gallery/decoder/LargeImageTile;

    .prologue
    .line 494
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadQueue:Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;

    monitor-enter v1

    .line 495
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadQueue:Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;->push(Lcom/sec/samsung/gallery/decoder/LargeImageTile;)Z

    .line 496
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 498
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mTileUploader:Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileUploader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileUploader;->mActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mTileUploader:Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileUploader;

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/GLRoot;->addOnGLIdleListener(Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;)V

    .line 501
    :cond_0
    return-void

    .line 496
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public stopDecodeThread(Z)V
    .locals 2
    .param p1, "clear"    # Z

    .prologue
    .line 236
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mNeedClear:Z

    .line 237
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->largeImageTask:Lcom/sec/android/gallery3d/util/Future;

    if-eqz v0, :cond_1

    .line 238
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mNeedClear:Z

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mRegionDecoder:Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->requestCanceDecode()V

    .line 240
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->largeImageTask:Lcom/sec/android/gallery3d/util/Future;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 243
    :cond_1
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mNeedClear:Z

    if-eqz v0, :cond_2

    .line 244
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 245
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodedTileList:Landroid/util/LongSparseArray;

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/decoder/LargeImage;->clearDecodedTiles(Landroid/util/LongSparseArray;)V

    .line 246
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodedTileList:Landroid/util/LongSparseArray;

    .line 247
    monitor-exit v1

    .line 249
    :cond_2
    return-void

    .line 247
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
