.class public Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GlFadeOutAnim"
.end annotation


# static fields
.field public static final ANIM_TYPE_1:I = 0x1

.field public static final ANIM_TYPE_2:I = 0x2

.field private static final Z_MOVE:F = 200.0f


# instance fields
.field private mAnimType:I

.field private mCount:I

.field private mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

.field private mPosZ:[F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGridView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V
    .locals 0

    .prologue
    .line 1403
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 5
    .param p1, "ratio"    # F

    .prologue
    const/4 v4, 0x0

    .line 1431
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    if-nez v1, :cond_1

    .line 1443
    :cond_0
    return-void

    .line 1434
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mCount:I

    if-ge v0, v1, :cond_0

    .line 1435
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v1, v0

    if-nez v1, :cond_3

    .line 1434
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1438
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mPosZ:[F

    aget v2, v2, v0

    const/high16 v3, 0x43480000    # 200.0f

    mul-float/2addr v3, p1

    sub-float/2addr v2, v3

    const/4 v3, 0x4

    invoke-virtual {v1, v4, v4, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPos(FFFI)V

    .line 1439
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v1, v0

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v2, p1

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setAlpha(F)V

    .line 1440
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mAnimType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v1, v0

    if-eqz v1, :cond_2

    .line 1441
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v1, v0

    const/high16 v2, 0x42b40000    # 90.0f

    mul-float/2addr v2, p1

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setYaw(F)V

    goto :goto_1
.end method

.method protected onCancel()V
    .locals 0

    .prologue
    .line 1446
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->onStop()V

    .line 1447
    return-void
.end method

.method protected onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1450
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    if-nez v1, :cond_0

    .line 1463
    :goto_0
    return-void

    .line 1453
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mCount:I

    if-ge v0, v1, :cond_2

    .line 1454
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    .line 1455
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->remove()V

    .line 1456
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aput-object v3, v1, v0

    .line 1453
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1460
    :cond_2
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 1461
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mFadeOutAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1600(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 1462
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mFadeOutAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;
    invoke-static {v1, v3}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1602(Lcom/sec/samsung/gallery/glview/GlGridView;Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;)Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;

    goto :goto_0
.end method

.method public startAnimation([Lcom/sec/android/gallery3d/glcore/GlObject;I)V
    .locals 4
    .param p1, "objects"    # [Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "animType"    # I

    .prologue
    .line 1414
    if-nez p1, :cond_0

    .line 1428
    :goto_0
    return-void

    .line 1417
    :cond_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 1418
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mAnimType:I

    .line 1419
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    array-length v1, v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mCount:I

    .line 1420
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mCount:I

    new-array v1, v1, [F

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mPosZ:[F

    .line 1422
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mCount:I

    if-ge v0, v1, :cond_2

    .line 1423
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    .line 1424
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mPosZ:[F

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->getZ()F

    move-result v2

    aput v2, v1, v0

    .line 1422
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1426
    :cond_2
    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v2, v3}, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->setDuration(J)V

    .line 1427
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView$GlFadeOutAnim;->start()V

    goto :goto_0
.end method
