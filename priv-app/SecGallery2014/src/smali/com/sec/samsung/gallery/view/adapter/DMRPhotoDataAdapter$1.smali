.class Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$1;
.super Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
.source "DMRPhotoDataAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/data/MediaItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;Lcom/sec/android/gallery3d/ui/GLRoot;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/gallery3d/ui/GLRoot;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;

    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;-><init>(Lcom/sec/android/gallery3d/ui/GLRoot;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 81
    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v2, v0, :cond_1

    :goto_0
    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 82
    # getter for: Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v2, "DMRPhotoDataAdapter MSG_UPDATE_IMAGE !!!"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mIsCanceled:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->access$100(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 84
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;

    # setter for: Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mIsCanceled:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->access$102(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;Z)Z

    .line 85
    # getter for: Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v2, "cancel handled !!!"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    :goto_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->access$500(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;)Lcom/sec/android/gallery3d/app/LoadingListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->access$500(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;)Lcom/sec/android/gallery3d/app/LoadingListener;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/app/LoadingListener;->onLoadingFinished(Z)V

    .line 101
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 81
    goto :goto_0

    .line 87
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->mHasFullImage:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->access$200(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 88
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$ImageBundle;

    # invokes: Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->onDecodeLargeComplete(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$ImageBundle;)V
    invoke-static {v2, v0}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->access$300(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$ImageBundle;)V

    goto :goto_1

    .line 90
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/gallery3d/util/Future;

    # invokes: Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->onDecodeThumbComplete(Lcom/sec/android/gallery3d/util/Future;)V
    invoke-static {v2, v0}, Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;->access$400(Lcom/sec/samsung/gallery/view/adapter/DMRPhotoDataAdapter;Lcom/sec/android/gallery3d/util/Future;)V

    goto :goto_1
.end method
