.class Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$2;
.super Ljava/lang/Object;
.source "SASlideShowService.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)V
    .locals 0

    .prologue
    .line 326
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 329
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onDismiss"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    # invokes: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->clearSlideshowMode(Landroid/content/Context;)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$600(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;Landroid/content/Context;)V

    .line 335
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->clearKeepScreenOnFlag(Landroid/view/Window;)V

    .line 337
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->stopSelf()V

    .line 338
    return-void
.end method
