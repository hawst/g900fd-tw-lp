.class Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;
.super Ljava/lang/Object;
.source "TimeViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/timeview/TimeViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V
    .locals 0

    .prologue
    .line 405
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private loadAllContentsAfterFakeLoading()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 451
    sget-boolean v0, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->sbNeedFullLoading:Z

    if-eqz v0, :cond_0

    .line 452
    sput-boolean v2, Lcom/sec/android/gallery3d/data/LocalAlbumSet;->sbNeedFullLoading:Z

    .line 453
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$3300(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://album_order"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 455
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloud:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->sbNeedFullLoading:Z

    if-eqz v0, :cond_1

    .line 456
    sput-boolean v2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeSet;->sbNeedFullLoading:Z

    .line 457
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$3400(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://talbum_full_load"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 459
    :cond_1
    return-void
.end method


# virtual methods
.method public onAllContentReady()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 438
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mNeedIdleProcess:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$3000(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 448
    :goto_0
    return-void

    .line 441
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # setter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mNeedIdleProcess:Z
    invoke-static {v0, v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$3002(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Z)Z

    .line 442
    new-instance v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$3100(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam;->setBeamListener()V

    .line 443
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;->loadAllContentsAfterFakeLoading()V

    .line 444
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$1900(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "START_IDLE_PROCESS"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$3200(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onContentChanged(II)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 434
    return-void
.end method

.method public onSizeChanged(I)V
    .locals 7
    .param p1, "size"    # I

    .prologue
    const/4 v6, 0x0

    .line 408
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # invokes: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->checkMediaAvailability()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$2600(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    .line 410
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$1100(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 411
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$1100(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    if-lez v2, :cond_1

    .line 412
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$2700(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 413
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$2700(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->cancel(Z)Z

    .line 415
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    new-instance v3, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Lcom/sec/samsung/gallery/view/timeview/TimeViewState$1;)V

    # setter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$2702(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;)Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;

    .line 416
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$2700(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 417
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # invokes: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->postUpdateCountOnActionBar()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$2900(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    .line 430
    :cond_1
    :goto_0
    return-void

    .line 420
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$1100(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$1100(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 421
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$1100(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 422
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_1

    .line 423
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v1

    .line 424
    .local v1, "path":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 425
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$2400(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->dismissDialogs()V

    goto :goto_0
.end method
