.class Lcom/sec/samsung/gallery/view/common/SelectionModeBar$2;
.super Ljava/lang/Object;
.source "SelectionModeBar.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/common/SelectionModeBar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/view/ViewObservable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/common/SelectionModeBar;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar$2;->this$0:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 85
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    const/16 v2, 0x14

    if-ne p2, v2, :cond_1

    .line 86
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar$2;->this$0:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    # getter for: Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->access$100(Lcom/sec/samsung/gallery/view/common/SelectionModeBar;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 87
    .local v0, "root":Lcom/sec/android/gallery3d/glcore/GlRootView;
    if-eqz v0, :cond_0

    .line 88
    iput-boolean v1, v0, Lcom/sec/android/gallery3d/glcore/GlRootView;->mDragDownFromSelectAll:Z

    .line 89
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setFocusable(Z)V

    .line 90
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setFocusableInTouchMode(Z)V

    .line 91
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestFocus()Z

    .line 92
    invoke-virtual {v0, p2, p3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 96
    .end local v0    # "root":Lcom/sec/android/gallery3d/glcore/GlRootView;
    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
