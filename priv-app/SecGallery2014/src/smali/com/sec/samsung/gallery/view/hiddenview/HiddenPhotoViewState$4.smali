.class Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$4;
.super Landroid/content/BroadcastReceiver;
.source "HiddenPhotoViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$4;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 158
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$4;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$800(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$4;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$600(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->hasWindowFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 159
    const-string v1, "isEnable"

    const/4 v2, 0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 160
    .local v0, "isEnable":Z
    if-eqz v0, :cond_1

    .line 161
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$4;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$800(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$4;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$900(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    .line 165
    .end local v0    # "isEnable":Z
    :cond_0
    :goto_0
    return-void

    .line 163
    .restart local v0    # "isEnable":Z
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$4;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->access$800(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    goto :goto_0
.end method
