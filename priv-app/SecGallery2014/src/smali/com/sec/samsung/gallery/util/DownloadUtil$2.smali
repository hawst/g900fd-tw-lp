.class final Lcom/sec/samsung/gallery/util/DownloadUtil$2;
.super Ljava/lang/Object;
.source "DownloadUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/util/DownloadUtil;->registerDownloadReceiver(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/sec/samsung/gallery/util/DownloadUtil$2;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 197
    # getter for: Lcom/sec/samsung/gallery/util/DownloadUtil;->smIsReceiverRegistered:Z
    invoke-static {}, Lcom/sec/samsung/gallery/util/DownloadUtil;->access$100()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 198
    new-instance v1, Ljava/util/ArrayList;

    # getter for: Lcom/sec/samsung/gallery/util/DownloadUtil;->mIDArray:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/samsung/gallery/util/DownloadUtil;->access$000()Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 199
    .local v1, "tempArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 200
    .local v2, "id":J
    iget-object v4, p0, Lcom/sec/samsung/gallery/util/DownloadUtil$2;->val$context:Landroid/content/Context;

    invoke-static {v4, v2, v3}, Lcom/sec/samsung/gallery/util/DownloadUtil;->checkDownloadStatus(Landroid/content/Context;J)V

    goto :goto_0

    .line 203
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "tempArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v2    # "id":J
    :cond_0
    # getter for: Lcom/sec/samsung/gallery/util/DownloadUtil;->smIsReceiverRegistered:Z
    invoke-static {}, Lcom/sec/samsung/gallery/util/DownloadUtil;->access$100()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 204
    # getter for: Lcom/sec/samsung/gallery/util/DownloadUtil;->mHandlerDownloadInfo:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/samsung/gallery/util/DownloadUtil;->access$300()Landroid/os/Handler;

    move-result-object v4

    # getter for: Lcom/sec/samsung/gallery/util/DownloadUtil;->mRunnableDownloadCheck:Ljava/lang/Runnable;
    invoke-static {}, Lcom/sec/samsung/gallery/util/DownloadUtil;->access$200()Ljava/lang/Runnable;

    move-result-object v5

    const-wide/16 v6, 0x1388

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 205
    :cond_1
    return-void
.end method
