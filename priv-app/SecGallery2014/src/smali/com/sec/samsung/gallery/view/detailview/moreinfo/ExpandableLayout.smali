.class public Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;
.super Landroid/view/ViewGroup;
.source "ExpandableLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mLayoutParam:Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;

.field private final mLineHeights:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->mLineHeights:Ljava/util/List;

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 48
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->mLineHeights:Ljava/util/List;

    .line 49
    sget-object v1, Lcom/sec/android/gallery3d/R$styleable;->expandable:[I

    invoke-virtual {p1, p2, v1, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 50
    .local v0, "t":Landroid/content/res/TypedArray;
    new-instance v1, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    invoke-virtual {v0, v5, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    const/4 v4, 0x2

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;-><init>(III)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->mLayoutParam:Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;

    .line 53
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 54
    return-void
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 126
    instance-of v0, p1, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;

    if-eqz v0, :cond_0

    .line 127
    const/4 v0, 0x1

    .line 129
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->mLayoutParam:Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 13
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->getChildCount()I

    move-result v3

    .line 135
    .local v3, "count":I
    sub-int v8, p4, p2

    .line 136
    .local v8, "width":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->getPaddingLeft()I

    move-result v9

    .line 137
    .local v9, "xpos":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->getPaddingTop()I

    move-result v10

    .line 138
    .local v10, "ypos":I
    const/4 v4, 0x0

    .line 140
    .local v4, "currentLine":I
    const/4 v6, 0x0

    .local v6, "i":I
    move v5, v4

    .end local v4    # "currentLine":I
    .local v5, "currentLine":I
    :goto_0
    if-ge v6, v3, :cond_0

    .line 141
    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 142
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v11

    const/16 v12, 0x8

    if-eq v11, v12, :cond_2

    .line 143
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;

    .line 144
    .local v7, "lp":Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 145
    .local v2, "childw":I
    iget v1, v7, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;->itemHeight:I

    .line 146
    .local v1, "childh":I
    add-int v11, v9, v2

    if-le v11, v8, :cond_1

    .line 147
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->getPaddingLeft()I

    move-result v9

    .line 148
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->mLineHeights:Ljava/util/List;

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "currentLine":I
    .restart local v4    # "currentLine":I
    invoke-interface {v11, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    add-int/2addr v10, v11

    .line 150
    :goto_1
    add-int v11, v9, v2

    add-int v12, v10, v1

    invoke-virtual {v0, v9, v10, v11, v12}, Landroid/view/View;->layout(IIII)V

    .line 151
    iget v11, v7, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;->horizontalSpacing:I

    add-int/2addr v11, v2

    add-int/2addr v9, v11

    .line 140
    .end local v1    # "childh":I
    .end local v2    # "childw":I
    .end local v7    # "lp":Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;
    :goto_2
    add-int/lit8 v6, v6, 0x1

    move v5, v4

    .end local v4    # "currentLine":I
    .restart local v5    # "currentLine":I
    goto :goto_0

    .line 154
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    return-void

    .restart local v0    # "child":Landroid/view/View;
    .restart local v1    # "childh":I
    .restart local v2    # "childw":I
    .restart local v7    # "lp":Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;
    :cond_1
    move v4, v5

    .end local v5    # "currentLine":I
    .restart local v4    # "currentLine":I
    goto :goto_1

    .end local v1    # "childh":I
    .end local v2    # "childw":I
    .end local v4    # "currentLine":I
    .end local v7    # "lp":Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;
    .restart local v5    # "currentLine":I
    :cond_2
    move v4, v5

    .end local v5    # "currentLine":I
    .restart local v4    # "currentLine":I
    goto :goto_2
.end method

.method protected onMeasure(II)V
    .locals 15
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 58
    sget-boolean v13, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->$assertionsDisabled:Z

    if-nez v13, :cond_0

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v13

    if-nez v13, :cond_0

    new-instance v13, Ljava/lang/AssertionError;

    invoke-direct {v13}, Ljava/lang/AssertionError;-><init>()V

    throw v13

    .line 60
    :cond_0
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v13

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->getPaddingLeft()I

    move-result v14

    sub-int/2addr v13, v14

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->getPaddingRight()I

    move-result v14

    sub-int v9, v13, v14

    .line 62
    .local v9, "maxWidth":I
    const/4 v10, 0x0

    .line 63
    .local v10, "width":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v13

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->getPaddingTop()I

    move-result v14

    sub-int/2addr v13, v14

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->getPaddingBottom()I

    move-result v14

    sub-int v6, v13, v14

    .line 64
    .local v6, "height":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->getChildCount()I

    move-result v3

    .line 65
    .local v3, "count":I
    const/4 v5, 0x0

    .line 66
    .local v5, "currentLineHeight":I
    const/4 v4, 0x0

    .line 67
    .local v4, "currentHeight":I
    iget-object v13, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->mLineHeights:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->clear()V

    .line 69
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->getPaddingLeft()I

    move-result v11

    .line 70
    .local v11, "xpos":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->getPaddingTop()I

    move-result v12

    .line 73
    .local v12, "ypos":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v13

    const/high16 v14, -0x80000000

    if-ne v13, v14, :cond_2

    .line 74
    const/high16 v13, -0x80000000

    invoke-static {v6, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 80
    .local v1, "childHeightMeasureSpec":I
    :goto_0
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    if-ge v7, v3, :cond_4

    .line 81
    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 82
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v13

    const/16 v14, 0x8

    if-eq v13, v14, :cond_1

    .line 83
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;

    .line 84
    .local v8, "lp":Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;
    const/high16 v13, -0x80000000

    invoke-static {v9, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    invoke-virtual {v0, v13, v1}, Landroid/view/View;->measure(II)V

    .line 86
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 87
    .local v2, "childw":I
    iget v13, v8, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;->itemHeight:I

    iget v14, v8, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;->verticalSpacing:I

    add-int/2addr v13, v14

    invoke-static {v5, v13}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 89
    add-int v13, v11, v2

    if-le v13, v9, :cond_3

    .line 90
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->getPaddingLeft()I

    move-result v11

    .line 91
    add-int/2addr v12, v5

    .line 92
    iget-object v13, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->mLineHeights:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-interface {v13, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    move v5, v4

    .line 99
    :goto_2
    iget v13, v8, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;->horizontalSpacing:I

    add-int/2addr v13, v2

    add-int/2addr v11, v13

    .line 80
    .end local v2    # "childw":I
    .end local v8    # "lp":Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 76
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "childHeightMeasureSpec":I
    .end local v7    # "i":I
    :cond_2
    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v13, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .restart local v1    # "childHeightMeasureSpec":I
    goto :goto_0

    .line 95
    .restart local v0    # "child":Landroid/view/View;
    .restart local v2    # "childw":I
    .restart local v7    # "i":I
    .restart local v8    # "lp":Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;
    :cond_3
    add-int v13, v11, v2

    invoke-static {v13, v10}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 96
    move v5, v4

    goto :goto_2

    .line 102
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "childw":I
    .end local v8    # "lp":Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;
    :cond_4
    iget-object v13, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->mLineHeights:Ljava/util/List;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-interface {v13, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v13

    if-nez v13, :cond_7

    .line 105
    add-int v6, v12, v5

    .line 112
    :cond_5
    :goto_3
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v13

    const/high16 v14, 0x40000000    # 2.0f

    if-ne v13, v14, :cond_6

    .line 113
    move v10, v9

    .line 116
    :cond_6
    invoke-virtual {p0, v10, v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->setMeasuredDimension(II)V

    .line 117
    return-void

    .line 106
    :cond_7
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v13

    const/high16 v14, -0x80000000

    if-ne v13, v14, :cond_5

    .line 107
    add-int v13, v12, v5

    if-ge v13, v6, :cond_5

    .line 108
    add-int v6, v12, v5

    goto :goto_3
.end method
