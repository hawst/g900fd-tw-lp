.class public Lcom/sec/samsung/gallery/decoder/CacheInterface;
.super Ljava/lang/Object;
.source "CacheInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;
    }
.end annotation


# instance fields
.field public mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v0, :cond_0

    .line 40
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getXIVInterface()Lcom/sec/samsung/gallery/decoder/XIVInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/CacheInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    .line 43
    :cond_0
    return-void
.end method

.method public static getCacheBitmap(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/MediaItem;I)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "type"    # I

    .prologue
    .line 79
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v0, :cond_0

    .line 80
    invoke-static {p0, p1, p2}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->getCacheBitmap(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/MediaItem;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 82
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getMTNCachingSize()I
    .locals 1

    .prologue
    .line 112
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v0, :cond_0

    .line 113
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->getMTNCachingSize()I

    move-result v0

    .line 115
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isUseBufferReuseTexture()Z
    .locals 1

    .prologue
    .line 54
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v0, :cond_0

    .line 55
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->isUseBufferReuseTexture()Z

    move-result v0

    .line 57
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isUseBufferThumbnailMode()Z
    .locals 1

    .prologue
    .line 63
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v0, :cond_0

    .line 64
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->isUseBufferThumbnailMode()Z

    move-result v0

    .line 66
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isUseLargeThumbnail()Z
    .locals 1

    .prologue
    .line 46
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v0, :cond_0

    .line 47
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->isUseLargeThumbnail()Z

    move-result v0

    .line 49
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isUseWSTN()Z
    .locals 1

    .prologue
    .line 71
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v0, :cond_0

    .line 72
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->isUseWSTN()Z

    move-result v0

    .line 74
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public createAllOnCurrent()V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/CacheInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/CacheInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->createAllOnCurrent()V

    .line 143
    :cond_0
    return-void
.end method

.method public createBufferReusableTexture(Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;IZ)Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;
    .locals 1
    .param p1, "data"    # Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;
    .param p2, "type"    # I
    .param p3, "fastReuse"    # Z

    .prologue
    .line 146
    new-instance v0, Lcom/sec/samsung/gallery/decoder/BufferReusableTexture;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/sec/samsung/gallery/decoder/BufferReusableTexture;-><init>(Lcom/sec/samsung/gallery/decoder/CacheInterface;Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;IZ)V

    return-object v0
.end method

.method public drawOnScreenNail(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;ZLcom/sec/android/gallery3d/ui/ScreenNail;IIFFF)V
    .locals 9
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "isNext"    # Z
    .param p3, "backUp"    # Lcom/sec/android/gallery3d/ui/ScreenNail;
    .param p4, "x"    # I
    .param p5, "y"    # I
    .param p6, "drawWidth"    # F
    .param p7, "drawHeight"    # F
    .param p8, "rotation"    # F

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/CacheInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/CacheInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->drawOnScreenNail(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;ZLcom/sec/android/gallery3d/ui/ScreenNail;IIFFF)V

    .line 97
    :cond_0
    return-void
.end method

.method public freeBuffer(ILjava/nio/ByteBuffer;)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "buffer"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/CacheInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/CacheInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->freeBuffer(ILjava/nio/ByteBuffer;)V

    .line 109
    :cond_0
    return-void
.end method

.method public getBuffer(II)Ljava/nio/ByteBuffer;
    .locals 1
    .param p1, "size"    # I
    .param p2, "type"    # I

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/CacheInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/CacheInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->getBuffer(II)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 153
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCacheBuffer(Lcom/sec/android/gallery3d/data/MediaItem;I)Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;
    .locals 2
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "type"    # I

    .prologue
    .line 100
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/CacheInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-static {v1, p1, p2}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->getCacheBuffer(Lcom/sec/samsung/gallery/decoder/XIVInterface;Lcom/sec/android/gallery3d/data/MediaItem;I)Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    move-result-object v0

    .line 101
    .local v0, "data":Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;

    invoke-direct {v1, v0}, Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;-><init>(Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;)V

    goto :goto_0
.end method

.method public getImageBufferDataFromBitmap(Landroid/graphics/Bitmap;Ljava/nio/ByteBuffer;)Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "buffer"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/CacheInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/CacheInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->getImageBufferDataFromBitmap(Landroid/graphics/Bitmap;Ljava/nio/ByteBuffer;)Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;

    move-result-object v0

    .line 160
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setViewSize(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/CacheInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/CacheInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->setViewSize(II)V

    .line 90
    :cond_0
    return-void
.end method
