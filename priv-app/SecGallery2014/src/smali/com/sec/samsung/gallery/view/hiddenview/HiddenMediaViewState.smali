.class public Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;
.super Lcom/sec/android/gallery3d/app/ActivityState;
.source "HiddenMediaViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;


# static fields
.field private static final MSG_EXIT_SELECTION_MODE_AND_RESET:I = 0x1

.field private static final TAG:Ljava/lang/String; = "HiddenAlbumViewState"

.field private static final THUMB_CACHE_SIZE:I = 0x20


# instance fields
.field private mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

.field private mAirMotionSettingsReceiver:Landroid/content/BroadcastReceiver;

.field private mBgImageIndex:I

.field private mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

.field private mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

.field private mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

.field private mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

.field private mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

.field private mHiddenAlbumViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mIsDeleteMode:Z

.field private mLaunchMode:Lcom/sec/samsung/gallery/core/LaunchModeType;

.field private mMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

.field private mMediaSelectionCallback:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;

.field private mMediaSetPath:Ljava/lang/String;

.field private mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mScrolledPosition:F

.field private mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field private mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 71
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ActivityState;-><init>()V

    .line 78
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    .line 86
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    .line 94
    iput v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mBgImageIndex:I

    .line 96
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mMediaSetPath:Ljava/lang/String;

    .line 98
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mIsDeleteMode:Z

    .line 102
    new-instance v0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$1;

    const-string v1, "HIDDEN_ALBUM_VIEW"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$1;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 127
    new-instance v0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mMediaSelectionCallback:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;

    .line 151
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    .line 152
    new-instance v0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$3;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    .line 168
    new-instance v0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$4;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mAirMotionSettingsReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->exitSelectionMode()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    iget v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mBgImageIndex:I

    return v0
.end method

.method static synthetic access$1202(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;
    .param p1, "x1"    # I

    .prologue
    .line 71
    iput p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mBgImageIndex:I

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;
    .param p1, "x1"    # I

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->selectMedia(I)V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;
    .param p1, "x1"    # I

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->handleHiddenItemsLaunch(I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->enterSelectionMode(Z)V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;
    .param p1, "x1"    # I

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->genericMotionEnter(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->genericMotionCancel()V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->showDeleteDialog()V

    return-void
.end method

.method static synthetic access$2200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/core/LaunchModeType;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mLaunchMode:Lcom/sec/samsung/gallery/core/LaunchModeType;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->checkMediaAvailability()V

    return-void
.end method

.method static synthetic access$2600(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->updateSelectAllActionBar()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/view/utils/EditModeHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/android/gallery3d/app/StateManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->exitViewState()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/glview/GlGridView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    return-object v0
.end method

.method private checkMediaAvailability()V
    .locals 1

    .prologue
    .line 701
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 702
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->exitViewState()V

    .line 706
    :goto_0
    return-void

    .line 704
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->removeNoItemLayout(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    goto :goto_0
.end method

.method private enterSelectionMode(Z)V
    .locals 6
    .param p1, "useVibrator"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 583
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595
    :goto_0
    return-void

    .line 585
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 586
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->enterSelectionMode(Z)V

    .line 587
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/LaunchModeType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 588
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 589
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-direct {v1, v2, v3}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/view/utils/EditModeHelper;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 591
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, v5}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(Ljava/lang/String;)V

    .line 592
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    sget v1, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_SELECT:I

    invoke-virtual {v0, v1, v4, v5}, Lcom/sec/samsung/gallery/glview/GlGridView;->setMode(IILjava/lang/Object;)V

    .line 593
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/glview/GlGridView;->refreshView(Z)V

    .line 594
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mMediaSelectionCallback:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->setMediaSelectionCallback(Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;)V

    goto :goto_0
.end method

.method private exitSelectionMode()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 598
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 599
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v0, v5}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->setMediaSelectionCallback(Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;)V

    .line 600
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->leaveSelectionMode()V

    .line 601
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/LaunchModeType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 602
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 603
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Z)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 605
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0e01d9

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(Ljava/lang/String;)V

    .line 606
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    if-eqz v0, :cond_1

    .line 607
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    sget v1, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_NORMAL:I

    invoke-virtual {v0, v1, v4, v5}, Lcom/sec/samsung/gallery/glview/GlGridView;->setMode(IILjava/lang/Object;)V

    .line 608
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/glview/GlGridView;->refreshView(Z)V

    .line 610
    :cond_1
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mIsDeleteMode:Z

    .line 611
    return-void
.end method

.method private exitViewState()V
    .locals 1

    .prologue
    .line 694
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->leaveSelectionMode()V

    .line 697
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 698
    return-void
.end method

.method private genericMotionCancel()V
    .locals 3

    .prologue
    .line 359
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getGenericMotionFocus()I

    move-result v0

    .line 360
    .local v0, "genericMotionFocus":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->setGenericMotionFocus(I)V

    .line 361
    if-ltz v0, :cond_0

    .line 362
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->notifyDataSetChanged(II)V

    .line 364
    :cond_0
    return-void
.end method

.method private genericMotionEnter(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x0

    .line 350
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getGenericMotionFocus()I

    move-result v0

    .line 351
    .local v0, "genericMotionFocus":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->setGenericMotionFocus(I)V

    .line 352
    if-ltz v0, :cond_0

    .line 353
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v1, v0, v2}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->notifyDataSetChanged(II)V

    .line 355
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v1, p1, v2}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->notifyDataSetChanged(II)V

    .line 356
    return-void
.end method

.method private handleHiddenItemsLaunch(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    const/4 v5, 0x1

    .line 709
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 710
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v3, p1}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getMedia(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    .line 711
    .local v1, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v1, :cond_1

    .line 730
    :cond_0
    :goto_0
    return-void

    .line 713
    :cond_1
    instance-of v3, v1, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v3, :cond_2

    .line 714
    const-string v3, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 715
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    const-class v4, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0

    .line 716
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v3

    sget-object v4, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/core/LaunchModeType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v3

    sget-object v4, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/core/LaunchModeType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 718
    const-string v3, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    const-string v3, "KEY_MEDIA_SET_PATH"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mMediaSetPath:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 720
    const-string v3, "KEY_ITEM_POSITION"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 721
    const-string v3, "KEY_ITEM_IS_HIDDEN"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 722
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    const-class v4, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0

    .line 723
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mLaunchMode:Lcom/sec/samsung/gallery/core/LaunchModeType;

    sget-object v4, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v3, v4, :cond_4

    .line 724
    const/4 v3, 0x2

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v4, v2, v3

    aput-object v1, v2, v5

    .line 725
    .local v2, "params":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v3

    const-string v4, "PICKER_ITEM_SELECTED"

    invoke-virtual {v3, v4, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 727
    .end local v2    # "params":[Ljava/lang/Object;
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v3

    sget-object v4, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/core/LaunchModeType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 728
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->selectMedia(I)V

    goto/16 :goto_0
.end method

.method private handleResultWallpaper(Landroid/content/Intent;I)V
    .locals 7
    .param p1, "data"    # Landroid/content/Intent;
    .param p2, "result"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, -0x1

    const/4 v3, 0x0

    .line 761
    if-eq p2, v6, :cond_0

    if-nez p2, :cond_1

    if-eqz p1, :cond_1

    const-string v5, "is_pressed_cancel"

    invoke-virtual {p1, v5, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    move v0, v4

    .line 763
    .local v0, "finishActivity":Z
    :goto_0
    if-eqz v0, :cond_3

    .line 764
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-ne p2, v6, :cond_2

    .end local p1    # "data":Landroid/content/Intent;
    :goto_1
    invoke-virtual {v3, p2, p1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 765
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 770
    :goto_2
    return-void

    .end local v0    # "finishActivity":Z
    .restart local p1    # "data":Landroid/content/Intent;
    :cond_1
    move v0, v3

    .line 761
    goto :goto_0

    .restart local v0    # "finishActivity":Z
    :cond_2
    move-object p1, v2

    .line 764
    goto :goto_1

    .line 767
    :cond_3
    const/4 v5, 0x2

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v5, v1, v3

    aput-object v2, v1, v4

    .line 768
    .local v1, "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "PICKER_ITEM_SELECTED"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2
.end method

.method private isAvailableCount(IZ)Z
    .locals 9
    .param p1, "selected"    # I
    .param p2, "showToast"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 642
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v6, :cond_1

    .line 643
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    .line 644
    .local v0, "currentMode":Lcom/sec/samsung/gallery/core/LaunchModeType;
    sget-object v6, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v6, :cond_1

    sget-object v6, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v6, :cond_1

    .line 661
    .end local v0    # "currentMode":Lcom/sec/samsung/gallery/core/LaunchModeType;
    :cond_0
    :goto_0
    return v4

    .line 650
    :cond_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 651
    .local v1, "intent":Landroid/content/Intent;
    const-string v6, "pick-max-item"

    sget v7, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 653
    .local v2, "maxPickCount":I
    if-le p1, v2, :cond_0

    .line 654
    if-eqz p2, :cond_2

    .line 655
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e0117

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v4, v5

    invoke-virtual {v6, v7, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 657
    .local v3, "text":Ljava/lang/CharSequence;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .end local v3    # "text":Ljava/lang/CharSequence;
    :cond_2
    move v4, v5

    .line 659
    goto :goto_0
.end method

.method private selectAll()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 665
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 666
    .local v0, "intent":Landroid/content/Intent;
    const-string v6, "pick-max-item"

    sget v7, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 668
    .local v1, "maxPickCount":I
    const/4 v3, 0x0

    .local v3, "position":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getCount()I

    move-result v4

    .local v4, "size":I
    :goto_0
    if-ge v3, v4, :cond_1

    .line 669
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v6, v3}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getMedia(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v2

    .line 670
    .local v2, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mLaunchMode:Lcom/sec/samsung/gallery/core/LaunchModeType;

    sget-object v7, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v6, v7, :cond_0

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mLaunchMode:Lcom/sec/samsung/gallery/core/LaunchModeType;

    sget-object v7, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v6, v7, :cond_2

    :cond_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v6

    if-ne v6, v1, :cond_2

    .line 672
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e0117

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 673
    .local v5, "text":Ljava/lang/CharSequence;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 680
    .end local v2    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v5    # "text":Ljava/lang/CharSequence;
    :cond_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setCurrentMediaSetSelectedCount(I)V

    .line 681
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->updateSelectAllActionBar()V

    .line 682
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    .line 683
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-virtual {v6, v10}, Lcom/sec/samsung/gallery/glview/GlGridView;->refreshView(Z)V

    .line 684
    return-void

    .line 676
    .restart local v2    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 668
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private selectMedia(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 614
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getMedia(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 616
    .local v0, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v0, :cond_0

    .line 617
    const-string v2, "HiddenAlbumViewState"

    const-string v3, "media is null at selectMedia"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 639
    :goto_0
    return-void

    .line 621
    :cond_0
    const-string v1, ""

    .line 623
    .local v1, "name":Ljava/lang/String;
    instance-of v2, v0, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v2, :cond_2

    move-object v2, v0

    .line 624
    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v1

    .line 629
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 630
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v2

    const v3, 0x7f0e029e

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 634
    :goto_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->toggle(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 635
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->updateSelectAllActionBar()V

    .line 636
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setCurrentMediaSetSelectedCount(I)V

    .line 637
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    goto :goto_0

    .line 625
    :cond_2
    instance-of v2, v0, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v2, :cond_1

    move-object v2, v0

    .line 626
    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 632
    :cond_3
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v2

    const v3, 0x7f0e029d

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method private setHiddenAlbumAdapter()V
    .locals 6

    .prologue
    .line 573
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    if-nez v2, :cond_0

    .line 574
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    .line 575
    .local v0, "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mMediaSetPath:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 576
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    new-instance v2, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v4

    sget-object v5, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/core/LaunchModeType;->equals(Ljava/lang/Object;)Z

    move-result v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    invoke-direct {v2, v3, v1, v4, v5}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;ZLcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    .line 580
    .end local v0    # "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    return-void
.end method

.method private showDeleteDialog()V
    .locals 3

    .prologue
    .line 367
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showDeleteDialog(ZZ)V

    .line 368
    return-void
.end method

.method private unselectAll()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 687
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 688
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 689
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    .line 690
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->refreshView(Z)V

    .line 691
    return-void
.end method

.method private updateSelectAllActionBar()V
    .locals 4

    .prologue
    .line 446
    const/4 v0, 0x0

    .line 447
    .local v0, "selected":Z
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getCount()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 448
    const/4 v0, 0x1

    .line 450
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v2, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 451
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->isAvailableCount(IZ)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateDoneButton(Z)V

    .line 452
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 453
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalSelectedItems()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    .line 454
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    .prologue
    .line 523
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/LaunchModeType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 524
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->exitSelectionMode()V

    .line 531
    :goto_0
    return-void

    .line 526
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 527
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->exitSelectionMode()V

    .line 529
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 567
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 568
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onConfigChanged(Landroid/content/res/Configuration;)V

    .line 569
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 570
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 183
    if-eqz p1, :cond_0

    .line 184
    const-string v2, "KEY_MEDIA_SET_PATH"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mMediaSetPath:Ljava/lang/String;

    .line 186
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mMediaSetPath:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 187
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    .line 188
    .local v0, "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    const/16 v2, 0xe

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 189
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v1, :cond_1

    .line 190
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mMediaSetPath:Ljava/lang/String;

    .line 194
    .end local v0    # "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    new-instance v2, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 196
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0f00bf

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 197
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAlbumThumbnailProxy()Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    .line 198
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setCacheSize(I)V

    .line 199
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 200
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 201
    new-instance v2, Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v2, v3, p0}, Lcom/sec/samsung/gallery/view/ActionBarManager;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/app/ActivityState;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 202
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mLaunchMode:Lcom/sec/samsung/gallery/core/LaunchModeType;

    .line 203
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .line 204
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v2, v5}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setPostInflateStatus(Z)V

    .line 205
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAirJump:Z

    if-eqz v2, :cond_2

    .line 206
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mAirMotionSettingsReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;-><init>(Landroid/content/Context;Landroid/content/BroadcastReceiver;I)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    .line 210
    :cond_2
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 488
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 489
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->onStop()V

    .line 481
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v0, :cond_1

    .line 482
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->destoryAirMotionDetector()V

    .line 484
    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 514
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 515
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->exitSelectionMode()V

    .line 519
    :goto_0
    return-void

    .line 517
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onOptionsItemSelected(Landroid/view/MenuItem;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 458
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    if-eqz v0, :cond_0

    .line 459
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->onPause()V

    .line 462
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->pause(Z)V

    .line 463
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 464
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    if-eqz v0, :cond_1

    .line 465
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->getScroll()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mScrolledPosition:F

    .line 466
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->pause()V

    .line 468
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "HIDDEN_ALBUM_VIEW"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 470
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v0, :cond_2

    .line 471
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    .line 473
    :cond_2
    return-void
.end method

.method public onPrepareOptionMenu(Landroid/view/Menu;)V
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 493
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 494
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mIsDeleteMode:Z

    if-eqz v4, :cond_2

    .line 495
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    .line 496
    .local v1, "isSelected":Z
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v3

    .local v3, "n":I
    :goto_0
    if-ge v0, v3, :cond_2

    .line 497
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 498
    .local v2, "item":Landroid/view/MenuItem;
    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0f027b

    if-ne v4, v5, :cond_1

    .line 499
    invoke-interface {v2, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 500
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 501
    if-eqz v1, :cond_0

    .line 502
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 496
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 504
    :cond_0
    invoke-interface {v2, v7}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 506
    :cond_1
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 510
    .end local v0    # "i":I
    .end local v1    # "isSelected":Z
    .end local v2    # "item":Landroid/view/MenuItem;
    .end local v3    # "n":I
    :cond_2
    return-void
.end method

.method public onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 372
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 374
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 375
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->exitSelectionMode()V

    .line 377
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    if-eqz v2, :cond_1

    .line 378
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->dismissDialogs()V

    .line 380
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 443
    :cond_2
    :goto_0
    return-void

    .line 383
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    .line 384
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->setHiddenAlbumAdapter()V

    .line 385
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    new-instance v3, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$13;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$13;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setWindowContentChangedListener(Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;)V

    .line 399
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V

    .line 400
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resetContentWindow()V

    .line 401
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resume()V

    .line 402
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->onResume()V

    .line 403
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onResume()V

    .line 404
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v2, v5}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 406
    const/4 v0, 0x0

    .line 407
    .local v0, "firstVisible":I
    const/4 v1, 0x0

    .line 408
    .local v1, "lastVisible":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    if-eqz v2, :cond_4

    .line 409
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlGridView;->getFirstVisiblePosition()I

    move-result v0

    .line 410
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlGridView;->getLastVisiblePosition()I

    move-result v1

    .line 412
    :cond_4
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mScrolledPosition:F

    invoke-direct {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlGridView;-><init>(Landroid/content/Context;F)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    .line 413
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    iput v0, v2, Lcom/sec/samsung/gallery/glview/GlGridView;->mLastVisibleFirst:I

    .line 414
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    iput v1, v2, Lcom/sec/samsung/gallery/glview/GlGridView;->mLastVisibleLast:I

    .line 415
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-virtual {v2, v3, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->attachLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;)V

    .line 417
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v2

    sget-object v3, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/LaunchModeType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 418
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 419
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v3, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v3, v4, v6}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Z)V

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 420
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->updateSelectAllActionBar()V

    .line 421
    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->enterSelectionMode(Z)V

    .line 424
    :cond_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v2

    if-nez v2, :cond_6

    .line 425
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 426
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v3, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v3, v4, v6}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Z)V

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 427
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v4, 0x7f0e01d9

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(Ljava/lang/String;)V

    .line 438
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 440
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v2, :cond_2

    .line 441
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    goto/16 :goto_0

    .line 429
    :cond_6
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    sget v3, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_SELECT:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v5, v4}, Lcom/sec/samsung/gallery/glview/GlGridView;->setMode(IILjava/lang/Object;)V

    .line 430
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v2

    sget-object v3, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/LaunchModeType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 431
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 432
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v3, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-direct {v3, v4, v5}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/view/utils/EditModeHelper;)V

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 434
    :cond_7
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->updateSelectAllActionBar()V

    .line 435
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    goto :goto_1
.end method

.method public onStateResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "request"    # I
    .param p2, "result"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 752
    packed-switch p1, :pswitch_data_0

    .line 758
    :goto_0
    return-void

    .line 755
    :pswitch_0
    invoke-direct {p0, p3, p2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->handleResultWallpaper(Landroid/content/Intent;I)V

    goto :goto_0

    .line 752
    :pswitch_data_0
    .packed-switch 0x403
        :pswitch_0
    .end packed-switch
.end method

.method public onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 3
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    .line 214
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V

    .line 215
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    new-instance v1, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$5;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$5;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->setOnScrollListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;)V

    .line 237
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    new-instance v1, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$6;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$6;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->setOnItemClickListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;)V

    .line 249
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mLaunchMode:Lcom/sec/samsung/gallery/core/LaunchModeType;

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_0

    .line 250
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    new-instance v1, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$7;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$7;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->setOnItemLongClickListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemLongClickListener;)V

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    new-instance v1, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$8;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$8;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->setOnFocusChangedListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;)V

    .line 283
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    new-instance v1, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$9;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$9;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->setOnStatusChangedListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;)V

    .line 295
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    const/4 v1, 0x0

    new-instance v2, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$10;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$10;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlGridView;->setOnGenericMotionListener(ILcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;)V

    .line 312
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    const/4 v1, 0x2

    new-instance v2, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$11;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$11;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlGridView;->setOnGenericMotionListener(ILcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;)V

    .line 326
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;

    new-instance v1, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$12;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$12;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->setOnKeyListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;)V

    .line 347
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3
    .param p1, "hasFocus"    # Z

    .prologue
    .line 734
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 736
    .local v0, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v1, v0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    if-eqz v1, :cond_0

    .line 738
    if-eqz p1, :cond_1

    .line 739
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v1, :cond_0

    .line 740
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    .line 748
    :cond_0
    :goto_0
    return-void

    .line 743
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v1, :cond_0

    .line 744
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    goto :goto_0
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 8
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 535
    move-object v0, p2

    check-cast v0, Lcom/sec/samsung/gallery/core/Event;

    .line 536
    .local v0, "ev":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v1

    .line 538
    .local v1, "eventType":I
    const-string v3, "HiddenAlbumViewState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Event update ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_SELECT_ALL:I

    if-ne v1, v3, :cond_1

    .line 541
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->selectAll()V

    .line 563
    :cond_0
    :goto_0
    return-void

    .line 542
    :cond_1
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_UNSELECT_ALL:I

    if-ne v1, v3, :cond_2

    .line 543
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->unselectAll()V

    goto :goto_0

    .line 544
    :cond_2
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SELECTION_MODE:I

    if-ne v1, v3, :cond_3

    .line 545
    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->enterSelectionMode(Z)V

    .line 546
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 548
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalSelectedItems()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    goto :goto_0

    .line 549
    :cond_3
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_DELETE_SELECTION_MODE:I

    if-ne v1, v3, :cond_4

    .line 550
    iput-boolean v7, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mIsDeleteMode:Z

    .line 551
    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->enterSelectionMode(Z)V

    .line 552
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 553
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalSelectedItems()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    goto :goto_0

    .line 554
    :cond_4
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_LAST_SHARE_APP:I

    if-ne v1, v3, :cond_5

    .line 555
    new-array v2, v7, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v3, v2, v6

    .line 558
    .local v2, "params":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v3

    const-string v4, "LAST_SHARE_APP"

    invoke-virtual {v3, v4, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 560
    .end local v2    # "params":[Ljava/lang/Object;
    :cond_5
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_HOME_ICON:I

    if-ne v1, v3, :cond_0

    .line 561
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->onBackPressed()V

    goto :goto_0
.end method
