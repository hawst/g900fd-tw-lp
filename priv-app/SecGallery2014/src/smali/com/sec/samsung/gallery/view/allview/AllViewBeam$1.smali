.class Lcom/sec/samsung/gallery/view/allview/AllViewBeam$1;
.super Ljava/lang/Object;
.source "AllViewBeam.java"

# interfaces
.implements Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/allview/AllViewBeam;->setBeamListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/allview/AllViewBeam;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/allview/AllViewBeam;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewBeam$1;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewBeam;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetFilePath()[Landroid/net/Uri;
    .locals 6

    .prologue
    .line 33
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewBeam$1;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewBeam;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/allview/AllViewBeam;->access$000(Lcom/sec/samsung/gallery/view/allview/AllViewBeam;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    .line 34
    .local v1, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v0

    .line 35
    .local v0, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewBeam;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/allview/AllViewBeam;->access$100()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onGetFilePath(). Selected item count= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/ui/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewBeam$1;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewBeam;

    # invokes: Lcom/sec/samsung/gallery/view/allview/AllViewBeam;->toUriPathArray(Ljava/util/List;)[Landroid/net/Uri;
    invoke-static {v3, v0}, Lcom/sec/samsung/gallery/view/allview/AllViewBeam;->access$200(Lcom/sec/samsung/gallery/view/allview/AllViewBeam;Ljava/util/List;)[Landroid/net/Uri;

    move-result-object v2

    .line 38
    .local v2, "uriPathArray":[Landroid/net/Uri;
    return-object v2
.end method
