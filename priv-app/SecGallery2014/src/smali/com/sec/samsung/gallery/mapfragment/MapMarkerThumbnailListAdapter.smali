.class public Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;
.super Landroid/widget/BaseAdapter;
.source "MapMarkerThumbnailListAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private DEFAUlT_THUMB_SIZE:I

.field private mContext:Landroid/content/Context;

.field private mImageOnGenericModionListener:Landroid/view/View$OnGenericMotionListener;

.field private mImageView:[Landroid/widget/ImageView;

.field mMapViewLayout:Landroid/view/View;

.field private mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mOnGenericModionListener:Landroid/view/View$OnGenericMotionListener;

.field private mReverseGeocoder:Lcom/sec/android/gallery3d/util/ReverseGeocoder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;Landroid/view/View;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "clusterPoint"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    .param p3, "mapViewLayout"    # Landroid/view/View;

    .prologue
    const/4 v9, 0x0

    .line 54
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 280
    new-instance v7, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter$2;

    invoke-direct {v7, p0}, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter$2;-><init>(Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;)V

    iput-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mOnGenericModionListener:Landroid/view/View$OnGenericMotionListener;

    .line 288
    new-instance v7, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter$3;

    invoke-direct {v7, p0}, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter$3;-><init>(Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;)V

    iput-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mImageOnGenericModionListener:Landroid/view/View$OnGenericMotionListener;

    .line 56
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mContext:Landroid/content/Context;

    .line 57
    iput-object p3, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mMapViewLayout:Landroid/view/View;

    .line 58
    const/16 v7, 0x50

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v7

    iput v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->DEFAUlT_THUMB_SIZE:I

    .line 59
    new-instance v7, Lcom/sec/android/gallery3d/util/ReverseGeocoder;

    iget-object v8, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mReverseGeocoder:Lcom/sec/android/gallery3d/util/ReverseGeocoder;

    .line 62
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 63
    .local v4, "mediaItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {p2}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->getPointsInCluster()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;

    .line 64
    .local v3, "inputPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;->getInfo()Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;->getMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 67
    .end local v3    # "inputPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
    :cond_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    .line 68
    .local v0, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    const-string v7, "MapView"

    invoke-virtual {v0, v7, v4}, Lcom/sec/android/gallery3d/data/DataManager;->setLocalItemList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 69
    const-string v7, "/local/ListAlbum/MapView"

    invoke-static {v7}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    .line 70
    .local v5, "path":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 71
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-nez v7, :cond_1

    .line 72
    sget-object v7, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->TAG:Ljava/lang/String;

    const-string v8, "mediaSet is null"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    :goto_1
    return-void

    .line 76
    :cond_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    .line 77
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v7, v7, Lcom/sec/android/gallery3d/data/LocalListAlbum;

    if-eqz v7, :cond_2

    .line 78
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v7, Lcom/sec/android/gallery3d/data/LocalListAlbum;

    iget-object v8, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mReverseGeocoder:Lcom/sec/android/gallery3d/util/ReverseGeocoder;

    invoke-direct {p0, v4, v8}, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->generateName(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/util/ReverseGeocoder;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/data/LocalListAlbum;->setName(Ljava/lang/String;)V

    .line 82
    :cond_2
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v6

    .line 86
    .local v6, "size":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mImageView:[Landroid/widget/ImageView;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mImageView:[Landroid/widget/ImageView;

    array-length v7, v7

    if-eqz v7, :cond_5

    .line 87
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mImageView:[Landroid/widget/ImageView;

    array-length v7, v7

    if-ge v1, v7, :cond_4

    .line 88
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mImageView:[Landroid/widget/ImageView;

    aget-object v7, v7, v1

    if-eqz v7, :cond_3

    .line 89
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mImageView:[Landroid/widget/ImageView;

    aput-object v9, v7, v1

    .line 87
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 91
    :cond_4
    iput-object v9, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mImageView:[Landroid/widget/ImageView;

    .line 92
    new-array v7, v6, [Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mImageView:[Landroid/widget/ImageView;

    goto :goto_1

    .line 94
    .end local v1    # "i":I
    :cond_5
    new-array v7, v6, [Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mImageView:[Landroid/widget/ImageView;

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private generateName(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/util/ReverseGeocoder;)Ljava/lang/String;
    .locals 10
    .param p2, "geocoder"    # Lcom/sec/android/gallery3d/util/ReverseGeocoder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;",
            "Lcom/sec/android/gallery3d/util/ReverseGeocoder;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 253
    .local p1, "mediaItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;

    invoke-direct {v6}, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;-><init>()V

    .line 255
    .local v6, "set":Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 256
    .local v1, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatitude()D

    move-result-wide v2

    .line 257
    .local v2, "itemLatitude":D
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getLongitude()D

    move-result-wide v4

    .line 259
    .local v4, "itemLongitude":D
    iget-wide v8, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLatitude:D

    cmpl-double v7, v8, v2

    if-lez v7, :cond_1

    .line 260
    iput-wide v2, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLatitude:D

    .line 261
    iput-wide v4, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLatLongitude:D

    .line 263
    :cond_1
    iget-wide v8, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLatitude:D

    cmpg-double v7, v8, v2

    if-gez v7, :cond_2

    .line 264
    iput-wide v2, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLatitude:D

    .line 265
    iput-wide v4, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLatLongitude:D

    .line 267
    :cond_2
    iget-wide v8, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLongitude:D

    cmpl-double v7, v8, v4

    if-lez v7, :cond_3

    .line 268
    iput-wide v2, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLatitude:D

    .line 269
    iput-wide v4, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMinLonLongitude:D

    .line 271
    :cond_3
    iget-wide v8, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLongitude:D

    cmpg-double v7, v8, v4

    if-gez v7, :cond_0

    .line 272
    iput-wide v2, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLatitude:D

    .line 273
    iput-wide v4, v6, Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;->mMaxLonLongitude:D

    goto :goto_0

    .line 277
    .end local v1    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v2    # "itemLatitude":D
    .end local v4    # "itemLongitude":D
    :cond_4
    invoke-virtual {p2, v6}, Lcom/sec/android/gallery3d/util/ReverseGeocoder;->computeAddress(Lcom/sec/android/gallery3d/util/ReverseGeocoder$SetLatLong;)Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method

.method private getExifOrientation(Ljava/lang/String;)I
    .locals 8
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v7, -0x1

    .line 196
    if-nez p1, :cond_1

    .line 197
    sget-object v5, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->TAG:Ljava/lang/String;

    const-string v6, "file path for exif is null"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    const/4 v0, 0x0

    .line 230
    :cond_0
    :goto_0
    return v0

    .line 201
    :cond_1
    const/4 v0, 0x0

    .line 202
    .local v0, "degree":I
    const/4 v2, 0x0

    .line 205
    .local v2, "exif":Landroid/media/ExifInterface;
    :try_start_0
    new-instance v3, Landroid/media/ExifInterface;

    invoke-direct {v3, p1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "exif":Landroid/media/ExifInterface;
    .local v3, "exif":Landroid/media/ExifInterface;
    move-object v2, v3

    .line 210
    .end local v3    # "exif":Landroid/media/ExifInterface;
    .restart local v2    # "exif":Landroid/media/ExifInterface;
    :goto_1
    if-eqz v2, :cond_0

    .line 211
    const-string v5, "Orientation"

    invoke-virtual {v2, v5, v7}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v4

    .line 212
    .local v4, "orientation":I
    if-eq v4, v7, :cond_0

    .line 214
    packed-switch v4, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 220
    :pswitch_1
    const/16 v0, 0xb4

    .line 221
    goto :goto_0

    .line 206
    .end local v4    # "orientation":I
    :catch_0
    move-exception v1

    .line 207
    .local v1, "ex":Ljava/io/IOException;
    sget-object v5, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->TAG:Ljava/lang/String;

    const-string v6, "cannot read exif"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 217
    .end local v1    # "ex":Ljava/io/IOException;
    .restart local v4    # "orientation":I
    :pswitch_2
    const/16 v0, 0x5a

    .line 218
    goto :goto_0

    .line 223
    :pswitch_3
    const/16 v0, 0x10e

    .line 224
    goto :goto_0

    .line 214
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "b"    # Landroid/graphics/Bitmap;
    .param p2, "degrees"    # I

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 235
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 236
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 237
    .local v5, "m":Landroid/graphics/Matrix;
    int-to-float v0, p2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-virtual {v5, v0, v1, v2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 239
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 240
    .local v7, "b2":Landroid/graphics/Bitmap;
    invoke-virtual {p1, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 241
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    move-object p1, v7

    .line 249
    .end local v5    # "m":Landroid/graphics/Matrix;
    .end local v7    # "b2":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-object p1

    .line 244
    .restart local v5    # "m":Landroid/graphics/Matrix;
    :catch_0
    move-exception v8

    .line 246
    .local v8, "ex":Ljava/lang/OutOfMemoryError;
    sget-object v0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->TAG:Ljava/lang/String;

    const-string v1, "Out of memory"

    invoke-static {v0, v1, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "arg2"    # Landroid/view/ViewGroup;

    .prologue
    .line 116
    sget-object v7, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->TAG:Ljava/lang/String;

    const-string v8, "getview"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v8, 0x1

    invoke-virtual {v7, p1, v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 122
    .local v2, "currentItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v2, :cond_0

    .line 123
    const/4 v3, 0x0

    .line 191
    :goto_0
    return-object v3

    .line 126
    :cond_0
    if-nez p2, :cond_3

    .line 127
    new-instance v3, Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v3, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 128
    .local v3, "imageView":Landroid/widget/ImageView;
    new-instance v7, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    iget v8, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->DEFAUlT_THUMB_SIZE:I

    iget v9, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->DEFAUlT_THUMB_SIZE:I

    invoke-direct {v7, v8, v9}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 131
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mMapViewLayout:Landroid/view/View;

    const v8, 0x7f0f0183

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 134
    .local v5, "mThumbnailAddress_txt":Landroid/widget/TextView;
    if-eqz v5, :cond_1

    .line 135
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v4

    .line 136
    .local v4, "locationName":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mOnGenericModionListener:Landroid/view/View$OnGenericMotionListener;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setOnGenericMotionListener(Landroid/view/View$OnGenericMotionListener;)V

    .line 141
    .end local v4    # "locationName":Ljava/lang/String;
    :cond_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mImageView:[Landroid/widget/ImageView;

    aput-object v3, v7, p1

    .line 142
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mImageView:[Landroid/widget/ImageView;

    aget-object v7, v7, p1

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 143
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mImageView:[Landroid/widget/ImageView;

    aget-object v7, v7, p1

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 144
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mImageView:[Landroid/widget/ImageView;

    aget-object v7, v7, p1

    iget-object v8, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mImageOnGenericModionListener:Landroid/view/View$OnGenericMotionListener;

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setOnGenericMotionListener(Landroid/view/View$OnGenericMotionListener;)V

    .line 146
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v6

    .line 147
    .local v6, "name":Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 148
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mImageView:[Landroid/widget/ImageView;

    aget-object v7, v7, p1

    invoke-virtual {v7, v6}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 154
    .end local v5    # "mThumbnailAddress_txt":Landroid/widget/TextView;
    .end local v6    # "name":Ljava/lang/String;
    :cond_2
    :goto_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->mImageView:[Landroid/widget/ImageView;

    if-nez v7, :cond_4

    .line 155
    const/4 v3, 0x0

    goto :goto_0

    .end local v3    # "imageView":Landroid/widget/ImageView;
    :cond_3
    move-object v3, p2

    .line 151
    check-cast v3, Landroid/widget/ImageView;

    .restart local v3    # "imageView":Landroid/widget/ImageView;
    goto :goto_1

    .line 158
    :cond_4
    const/4 v7, 0x3

    invoke-virtual {v2, v7}, Lcom/sec/android/gallery3d/data/MediaItem;->requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v7

    sget-object v8, Lcom/sec/android/gallery3d/util/ThreadPool;->JOB_CONTEXT_STUB:Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    invoke-interface {v7, v8}, Lcom/sec/android/gallery3d/util/ThreadPool$Job;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 161
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_6

    .line 162
    const/4 v7, 0x0

    invoke-static {v0, v7}, Lcom/sec/android/gallery3d/common/BitmapUtils;->cropCenter(Landroid/graphics/Bitmap;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 163
    .local v1, "cropped_bitmap":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_5

    .line 164
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->getExifOrientation(Ljava/lang/String;)I

    move-result v7

    invoke-direct {p0, v1, v7}, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 166
    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 172
    .end local v1    # "cropped_bitmap":Landroid/graphics/Bitmap;
    :cond_5
    :goto_2
    new-instance v7, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter$1;

    invoke-direct {v7, p0, p1}, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter$1;-><init>(Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;I)V

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 169
    :cond_6
    const/4 v7, 0x0

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_2
.end method
