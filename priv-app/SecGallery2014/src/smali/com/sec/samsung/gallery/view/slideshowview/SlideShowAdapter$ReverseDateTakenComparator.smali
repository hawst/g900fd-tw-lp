.class Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$ReverseDateTakenComparator;
.super Ljava/lang/Object;
.source "SlideShowAdapter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ReverseDateTakenComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/gallery3d/data/MediaObject;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$1;

    .prologue
    .line 241
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$ReverseDateTakenComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/gallery3d/data/MediaObject;Lcom/sec/android/gallery3d/data/MediaObject;)I
    .locals 6
    .param p1, "obj1"    # Lcom/sec/android/gallery3d/data/MediaObject;
    .param p2, "obj2"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 244
    instance-of v2, p1, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v2, :cond_0

    instance-of v2, p2, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 245
    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    .local v0, "item1":Lcom/sec/android/gallery3d/data/MediaItem;
    move-object v1, p2

    .line 246
    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 247
    .local v1, "item2":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v2

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/gallery3d/common/Utils;->compare(JJ)I

    move-result v2

    .line 249
    .end local v0    # "item1":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v1    # "item2":Lcom/sec/android/gallery3d/data/MediaItem;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 241
    check-cast p1, Lcom/sec/android/gallery3d/data/MediaObject;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/gallery3d/data/MediaObject;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter$ReverseDateTakenComparator;->compare(Lcom/sec/android/gallery3d/data/MediaObject;Lcom/sec/android/gallery3d/data/MediaObject;)I

    move-result v0

    return v0
.end method
