.class public Lcom/sec/samsung/gallery/controller/SStudioCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "SStudioCmd.java"

# interfaces
.implements Ljava/util/Observer;
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field private static final PACKAGE_NAME_VIDEO_CLIP:Ljava/lang/String; = "com.sec.android.app.storycam"

.field private static final PREFIX_HAS_SSTUDIO:Ljava/lang/String; = "sstudio-has-"

.field private static final PREFIX_UPDATE_SSTUDIO:Ljava/lang/String; = "sstudio-update-"

.field private static final SELECTION_ITEM_MAX_LIMIT_SSTUDIO_PHONE:I = 0xf

.field private static final SELECTION_ITEM_MAX_LIMIT_SSTUDIO_PHONE_WITHOUT_VC:I = 0x6

.field private static final SELECTION_ITEM_MAX_LIMIT_SSTUDIO_TABLET:I = 0x20

.field private static final S_STUDIO_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.mimage.sstudio"

.field private static final TAG:Ljava/lang/String;

.field private static selectionItemMaxLimit:I


# instance fields
.field private mContext:Landroid/content/Context;

.field protected mCount:I

.field protected mSelectedItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/sec/samsung/gallery/controller/SStudioCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/SStudioCmd;->TAG:Ljava/lang/String;

    .line 51
    const/4 v0, -0x1

    sput v0, Lcom/sec/samsung/gallery/controller/SStudioCmd;->selectionItemMaxLimit:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method public static getSelectionItemMaxLimit(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 149
    sget v1, Lcom/sec/samsung/gallery/controller/SStudioCmd;->selectionItemMaxLimit:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 150
    sget v1, Lcom/sec/samsung/gallery/controller/SStudioCmd;->selectionItemMaxLimit:I

    .line 166
    :goto_0
    return v1

    .line 152
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 153
    const/16 v1, 0x20

    sput v1, Lcom/sec/samsung/gallery/controller/SStudioCmd;->selectionItemMaxLimit:I

    .line 166
    :goto_1
    sget v1, Lcom/sec/samsung/gallery/controller/SStudioCmd;->selectionItemMaxLimit:I

    goto :goto_0

    .line 156
    :cond_1
    if-eqz p0, :cond_2

    .line 157
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.android.app.storycam"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 159
    :cond_2
    const/16 v1, 0xf

    sput v1, Lcom/sec/samsung/gallery/controller/SStudioCmd;->selectionItemMaxLimit:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 161
    :catch_0
    move-exception v0

    .line 162
    .local v0, "nnfe":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x6

    sput v1, Lcom/sec/samsung/gallery/controller/SStudioCmd;->selectionItemMaxLimit:I

    goto :goto_1
.end method

.method private getUriList(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    .local p1, "selectedItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 108
    .local v6, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 109
    .local v3, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v3, :cond_1

    .line 110
    sget-object v7, Lcom/sec/samsung/gallery/controller/SStudioCmd;->TAG:Ljava/lang/String;

    const-string v8, "selected item\'s uri is null, item is ignored"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 111
    :cond_1
    instance-of v7, v3, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v7, :cond_2

    move-object v7, v3

    .line 112
    check-cast v7, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    .line 113
    .local v0, "count":I
    check-cast v3, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local v3    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    const/4 v7, 0x0

    invoke-virtual {v3, v7, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v4

    .line 114
    .local v4, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 115
    .local v5, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 118
    .end local v0    # "count":I
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v5    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v3    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 121
    .end local v3    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_3
    return-object v6
.end method

.method public static isSStudioAvailable(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 83
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 103
    :goto_0
    return v4

    .line 87
    :cond_0
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/PackagesMonitor;->getPackagesVersion(Landroid/content/Context;)I

    move-result v3

    .line 88
    .local v3, "version":I
    const-string/jumbo v6, "sstudio-update-"

    invoke-static {p0, v6, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v2

    .line 90
    .local v2, "updateKey":I
    if-eq v2, v3, :cond_4

    .line 91
    const/4 v1, 0x0

    .line 93
    .local v1, "info":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v7, "com.sec.android.mimage.sstudio"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 94
    if-eqz v1, :cond_2

    .line 99
    const-string/jumbo v6, "sstudio-update-"

    invoke-static {p0, v6, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 100
    const-string/jumbo v6, "sstudio-has-"

    if-eqz v1, :cond_1

    move v4, v5

    :cond_1
    invoke-static {p0, v6, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    move v4, v5

    goto :goto_0

    .line 99
    :cond_2
    const-string/jumbo v6, "sstudio-update-"

    invoke-static {p0, v6, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 100
    const-string/jumbo v6, "sstudio-has-"

    if-eqz v1, :cond_3

    move v4, v5

    :cond_3
    invoke-static {p0, v6, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 103
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    :cond_4
    :goto_1
    const-string/jumbo v4, "sstudio-has-"

    invoke-static {p0, v4, v5}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v4

    goto :goto_0

    .line 96
    .restart local v1    # "info":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v0

    .line 97
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    const-string v6, "Get text"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "S-Studio is not exist. "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 99
    const-string/jumbo v6, "sstudio-update-"

    invoke-static {p0, v6, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 100
    const-string/jumbo v6, "sstudio-has-"

    if-eqz v1, :cond_5

    move v4, v5

    :cond_5
    invoke-static {p0, v6, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_1

    .line 99
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_0
    move-exception v6

    const-string/jumbo v7, "sstudio-update-"

    invoke-static {p0, v7, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 100
    const-string/jumbo v7, "sstudio-has-"

    if-eqz v1, :cond_6

    :goto_2
    invoke-static {p0, v7, v5}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    throw v6

    :cond_6
    move v5, v4

    goto :goto_2
.end method

.method private startSStudio()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v5, -0x1

    .line 126
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/SStudioCmd;->mSelectedItemList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    move v4, v5

    .line 127
    .local v4, "size":I
    :goto_0
    new-instance v2, Landroid/content/Intent;

    const-string v6, "com.sec.android.mimage.sstudio"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 129
    .local v2, "intent":Landroid/content/Intent;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 130
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v6, "selectedItems"

    if-ne v4, v5, :cond_1

    const/4 v5, 0x0

    :goto_1
    invoke-virtual {v0, v6, v5}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 131
    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 132
    const-string v5, "selectedCount"

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 133
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 134
    const/high16 v5, 0x10000

    invoke-virtual {v2, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 135
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/SStudioCmd;->mContext:Landroid/content/Context;

    check-cast v5, Landroid/app/Activity;

    invoke-virtual {v5, v7, v7}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 137
    const/16 v3, 0x10

    .line 139
    .local v3, "result":I
    :try_start_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/SStudioCmd;->mContext:Landroid/content/Context;

    check-cast v5, Landroid/app/Activity;

    invoke-virtual {v5, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    :goto_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/SStudioCmd;->mContext:Landroid/content/Context;

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v5

    const-string v6, "EXIT_SELECTION_MODE"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 146
    return-void

    .line 126
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "result":I
    .end local v4    # "size":I
    :cond_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/SStudioCmd;->mSelectedItemList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v4

    goto :goto_0

    .line 130
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .restart local v2    # "intent":Landroid/content/Intent;
    .restart local v4    # "size":I
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/SStudioCmd;->mSelectedItemList:Ljava/util/ArrayList;

    goto :goto_1

    .line 140
    .restart local v3    # "result":I
    :catch_0
    move-exception v1

    .line 141
    .local v1, "excp":Landroid/content/ActivityNotFoundException;
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/SStudioCmd;->mContext:Landroid/content/Context;

    const v6, 0x7f0e02bc

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 143
    const/4 v3, 0x0

    goto :goto_2
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 3
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 60
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Object;

    move-object v0, v2

    check-cast v0, [Ljava/lang/Object;

    .line 61
    .local v0, "params":[Ljava/lang/Object;
    const/4 v2, 0x0

    aget-object v2, v0, v2

    check-cast v2, Landroid/content/Context;

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/SStudioCmd;->mContext:Landroid/content/Context;

    .line 62
    const/4 v2, 0x1

    aget-object v2, v0, v2

    check-cast v2, Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/SStudioCmd;->mSelectedItemList:Ljava/util/ArrayList;

    .line 63
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/SStudioCmd;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/SStudioCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 64
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/SStudioCmd;->mSelectedItemList:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    .line 65
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/SStudioCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCloneMediaList()Ljava/util/LinkedList;

    move-result-object v1

    .line 66
    .local v1, "selectedItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/SStudioCmd;->getUriList(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/SStudioCmd;->mSelectedItemList:Ljava/util/ArrayList;

    .line 68
    .end local v1    # "selectedItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/SStudioCmd;->startSStudio()V

    .line 69
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 4
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 73
    move-object v0, p2

    check-cast v0, Lcom/sec/samsung/gallery/core/Event;

    .line 75
    .local v0, "ev":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_START_SELECTED_APP:I

    if-ne v2, v3, :cond_0

    .line 76
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/SStudioCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCloneMediaList()Ljava/util/LinkedList;

    move-result-object v1

    .line 77
    .local v1, "selectedItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/SStudioCmd;->getUriList(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/SStudioCmd;->mSelectedItemList:Ljava/util/ArrayList;

    .line 78
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/SStudioCmd;->startSStudio()V

    .line 80
    .end local v1    # "selectedItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :cond_0
    return-void
.end method
