.class Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;
.super Ljava/lang/Object;
.source "MoveToKNOXCmd.java"

# interfaces
.implements Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->handleOperation(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private requestFilePath:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

.field final synthetic val$count:I

.field final synthetic val$operationId:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;II)V
    .locals 1

    .prologue
    .line 233
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    iput p2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->val$operationId:I

    iput p3, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->val$count:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 234
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->requestFilePath:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 4

    .prologue
    .line 313
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->requestFilePath:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 330
    :cond_0
    :goto_0
    return-void

    .line 314
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->requestFilePath:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 315
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$100(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX2(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 316
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$100(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 317
    .local v1, "sndPackageName":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$900(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)Lcom/sec/knox/containeragent/ContainerInstallerManager;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 318
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$900(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)Lcom/sec/knox/containeragent/ContainerInstallerManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->requestFilePath:Ljava/util/ArrayList;

    invoke-virtual {v2, v3, v1}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->KNOXFileRelayCancel(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0

    .line 320
    :cond_2
    const-string v2, "MoveToKNOXCmd"

    const-string v3, " mKNOXProgressReceiver mContainerInstallerManager is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 323
    .end local v1    # "sndPackageName":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$100(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "rcp"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/RCPManager;

    .line 325
    .local v0, "mRcpm":Landroid/os/RCPManager;
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mThreadId:J
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$1400(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/os/RCPManager;->cancelCopyChunks(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 326
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public failed(I)V
    .locals 5
    .param p1, "errCode"    # I

    .prologue
    const/4 v4, 0x0

    .line 334
    const-string v0, "MoveToKNOXCmd"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mOnKNOXListener errCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$600(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 337
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    iget v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->val$operationId:I

    # invokes: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->showToast(IIII)V
    invoke-static {v0, v1, v4, v4, p1}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$700(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;IIII)V

    .line 338
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->requestFilePath:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->requestFilePath:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 340
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->requestFilePath:Ljava/util/ArrayList;

    .line 342
    :cond_0
    return-void
.end method

.method public handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 7
    .param p1, "mediaObject"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 250
    const/4 v4, 0x0

    .line 251
    .local v4, "result":Z
    iget v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->val$operationId:I

    if-eqz v5, :cond_0

    iget v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->val$operationId:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    .line 253
    :cond_0
    instance-of v5, p1, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v5, :cond_4

    move-object v3, p1

    .line 254
    check-cast v3, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 255
    .local v3, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    instance-of v5, v3, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v5, :cond_3

    .line 256
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    .line 257
    .local v0, "count":I
    const/4 v5, 0x0

    invoke-virtual {v3, v5, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v2

    .line 258
    .local v2, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 259
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->requestFilePath:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 258
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 261
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    # operator++ for: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mSelectedAlbumCount:I
    invoke-static {v5}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$808(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)I

    .line 262
    const/4 v4, 0x1

    .line 271
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v3    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local p1    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    :goto_1
    return v4

    .line 264
    .restart local v3    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .restart local p1    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_3
    const/4 v4, 0x0

    goto :goto_1

    .line 266
    .end local v3    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_4
    instance-of v5, p1, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v5, :cond_2

    .line 267
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->requestFilePath:Ljava/util/ArrayList;

    check-cast p1, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .end local p1    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 268
    const/4 v4, 0x1

    goto :goto_1
.end method

.method public onCompleted(I)V
    .locals 6
    .param p1, "successCnt"    # I

    .prologue
    .line 239
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$600(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    const-wide/16 v4, 0x64

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 240
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->requestFilePath:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 241
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->requestFilePath:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 242
    .local v0, "requestCount":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    iget v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->val$operationId:I

    sub-int v3, v0, p1

    const/4 v4, 0x0

    # invokes: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->showToast(IIII)V
    invoke-static {v1, v2, v3, v0, v4}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$700(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;IIII)V

    .line 243
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->requestFilePath:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 244
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->requestFilePath:Ljava/util/ArrayList;

    .line 246
    .end local v0    # "requestCount":I
    :cond_0
    return-void
.end method

.method public onRequestKNOXOperation()V
    .locals 5

    .prologue
    .line 276
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->requestFilePath:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 277
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$100(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX2(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 278
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$100(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 279
    .local v0, "sndPackageName":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$900(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)Lcom/sec/knox/containeragent/ContainerInstallerManager;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 280
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$900(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)Lcom/sec/knox/containeragent/ContainerInstallerManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->requestFilePath:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v0}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->KNOXFileRelayRequest(Ljava/util/List;Ljava/lang/String;)V

    .line 281
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    iget v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->val$operationId:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->requestFilePath:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/16 v4, 0x64

    # invokes: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->showProgressivePopup(III)V
    invoke-static {v1, v2, v3, v4}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$1000(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;III)V

    .line 289
    .end local v0    # "sndPackageName":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$100(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "EXIT_SELECTION_MODE"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 290
    return-void

    .line 283
    .restart local v0    # "sndPackageName":Ljava/lang/String;
    :cond_1
    const-string v1, "MoveToKNOXCmd"

    const-string v2, " mKNOXProgressReceiver mContainerInstallerManager is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 286
    .end local v0    # "sndPackageName":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->requestFilePath:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->val$operationId:I

    # invokes: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->moveToKnoxAPI2(Ljava/util/ArrayList;I)V
    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$1100(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;Ljava/util/ArrayList;I)V

    goto :goto_0
.end method

.method public startProgress()V
    .locals 4

    .prologue
    .line 345
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    iget v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->val$operationId:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->requestFilePath:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    iget v3, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->val$count:I

    # invokes: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->showProgressivePopup(III)V
    invoke-static {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$1000(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;III)V

    .line 346
    return-void
.end method

.method public updateProgress()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x1

    .line 305
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->requestFilePath:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$1200(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$1200(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->increaseProgress(JZ)V

    .line 307
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$1200(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->increaseProgress(JZ)V

    .line 309
    :cond_0
    return-void
.end method

.method public updateProgress(I)V
    .locals 5
    .param p1, "percentage"    # I

    .prologue
    .line 294
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$1200(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 295
    const-string v1, "MoveToKNOXCmd"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mKNOXProgressReceiver: PROGRESS percent :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " % , previousPercentage : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->previousPercentage:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$1300(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " %"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    move v0, p1

    .line 297
    .local v0, "currentPercentage":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->previousPercentage:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$1300(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)I

    move-result v1

    sub-int/2addr p1, v1

    .line 298
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$1200(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v1

    int-to-long v2, p1

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->increaseProgress(JZ)V

    .line 299
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    # setter for: Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->previousPercentage:I
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->access$1302(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;I)I

    .line 301
    .end local v0    # "currentPercentage":I
    :cond_0
    return-void
.end method
