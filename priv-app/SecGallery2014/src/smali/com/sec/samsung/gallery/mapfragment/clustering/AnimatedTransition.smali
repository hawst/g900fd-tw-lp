.class Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;
.super Ljava/lang/Object;
.source "AnimatedTransition.java"


# instance fields
.field private final destinationClusterPoint:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

.field private final originClusterPoint:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

.field private final originClusterRelevantInputPoints:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

.field private final spans180Meridian:Z


# direct methods
.method constructor <init>(Lcom/google/android/gms/maps/Projection;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)V
    .locals 6
    .param p1, "projection"    # Lcom/google/android/gms/maps/Projection;
    .param p2, "originClusterPoint"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    .param p3, "firstRelevantInputPointFromOriginClusterPoint"    # Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
    .param p4, "destinationClusterPoint"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    .prologue
    const/4 v0, 0x1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    invoke-virtual {p2}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->clearScreenPosition()V

    .line 18
    invoke-virtual {p2, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->buildScreenPosition(Lcom/google/android/gms/maps/Projection;)V

    .line 19
    iput-object p2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;->originClusterPoint:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    .line 21
    new-instance v1, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    invoke-virtual {p2}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->getMapPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    invoke-direct {v1, p3, p1, v0, v2}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;-><init>(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;Lcom/google/android/gms/maps/Projection;ZLcom/google/android/gms/maps/model/LatLng;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;->originClusterRelevantInputPoints:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    .line 23
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;->originClusterRelevantInputPoints:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    invoke-virtual {p2}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->getScreenPosition()Landroid/graphics/Point;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->setScreenPosition(Landroid/graphics/Point;)V

    .line 25
    iput-object p4, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;->destinationClusterPoint:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    .line 27
    invoke-virtual {p2}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->getMapPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    iget-wide v2, v1, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-virtual {p4}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->getMapPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    iget-wide v4, v1, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide v4, 0x4066800000000000L    # 180.0

    cmpl-double v1, v2, v4

    if-lez v1, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;->spans180Meridian:Z

    .line 28
    return-void

    .line 27
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method addOriginClusterRelevantInputPoint(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;)V
    .locals 1
    .param p1, "previousInputPoint"    # Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;->originClusterRelevantInputPoints:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->add(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;)V

    .line 48
    return-void
.end method

.method destinationContains(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;)Z
    .locals 1
    .param p1, "point"    # Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;->destinationClusterPoint:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;->destinationClusterPoint:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->containsInputPoint(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getDestinationClusterPoint()Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;->destinationClusterPoint:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    return-object v0
.end method

.method getOriginClusterRelevantInputPoints()Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;->originClusterRelevantInputPoints:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    return-object v0
.end method

.method originContains(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;)Z
    .locals 1
    .param p1, "point"    # Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;->originClusterPoint:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;->originClusterPoint:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->containsInputPoint(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method spans180Meridian()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;->spans180Meridian:Z

    return v0
.end method
