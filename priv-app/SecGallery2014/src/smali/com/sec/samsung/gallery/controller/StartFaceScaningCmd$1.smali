.class Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd$1;
.super Ljava/lang/Thread;
.source "StartFaceScaningCmd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->startScaning()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;

.field final synthetic val$albumThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

.field final synthetic val$currentViewState:Lcom/sec/android/gallery3d/app/ActivityState;

.field final synthetic val$photoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;Ljava/lang/String;Lcom/sec/samsung/gallery/model/ThumbnailProxy;Lcom/sec/samsung/gallery/model/ThumbnailProxy;Lcom/sec/android/gallery3d/app/ActivityState;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;

    iput-object p3, p0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd$1;->val$albumThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    iput-object p4, p0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd$1;->val$photoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    iput-object p5, p0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd$1;->val$currentViewState:Lcom/sec/android/gallery3d/app/ActivityState;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private reloadRectOfAllFaces()V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd$1;->val$albumThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd$1;->val$albumThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->reloadRectOfAllFaces()V

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd$1;->val$photoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    if-eqz v0, :cond_1

    .line 146
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd$1;->val$photoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->reloadRectOfAllFaces()V

    .line 148
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd$1;->val$currentViewState:Lcom/sec/android/gallery3d/app/ActivityState;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/ActivityState;->onRefresh()V

    .line 149
    return-void
.end method


# virtual methods
.method public run()V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd$1;->reloadRectOfAllFaces()V

    .line 139
    return-void
.end method
