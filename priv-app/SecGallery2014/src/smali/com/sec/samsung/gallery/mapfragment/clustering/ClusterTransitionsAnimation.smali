.class Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;
.super Ljava/lang/Object;
.source "ClusterTransitionsAnimation.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$1;,
        Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$Host;,
        Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;
    }
.end annotation


# instance fields
.field private animatedMarkers:[Lcom/google/android/gms/maps/model/Marker;

.field private final animatedTransitionsByMarker:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/gms/maps/model/Marker;",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;",
            ">;"
        }
    .end annotation
.end field

.field private animator:Landroid/animation/ObjectAnimator;

.field private final hostRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$Host;",
            ">;"
        }
    .end annotation
.end field

.field private final mapRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/gms/maps/GoogleMap;",
            ">;"
        }
    .end annotation
.end field

.field private final optionsRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/Options;",
            ">;"
        }
    .end annotation
.end field

.field private state:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;

.field private stationaryMarkers:[Lcom/google/android/gms/maps/model/Marker;

.field private final stationaryTransitionsByMarker:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/gms/maps/model/Marker;",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;",
            ">;"
        }
    .end annotation
.end field

.field private transitions:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions;


# direct methods
.method constructor <init>(Lcom/google/android/gms/maps/GoogleMap;Lcom/sec/samsung/gallery/mapfragment/clustering/Options;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$Host;)V
    .locals 1
    .param p1, "map"    # Lcom/google/android/gms/maps/GoogleMap;
    .param p2, "options"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    .param p3, "host"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$Host;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animatedTransitionsByMarker:Ljava/util/HashMap;

    .line 33
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->stationaryTransitionsByMarker:Ljava/util/HashMap;

    .line 36
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->mapRef:Ljava/lang/ref/WeakReference;

    .line 37
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->optionsRef:Ljava/lang/ref/WeakReference;

    .line 38
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->hostRef:Ljava/lang/ref/WeakReference;

    .line 39
    return-void
.end method

.method private addMarker(Lcom/google/android/gms/maps/GoogleMap;Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)Lcom/google/android/gms/maps/model/Marker;
    .locals 2
    .param p1, "map"    # Lcom/google/android/gms/maps/GoogleMap;
    .param p2, "moc"    # Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;
    .param p3, "clusterPoint"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    .prologue
    .line 242
    new-instance v0, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    .line 243
    .local v0, "mo":Lcom/google/android/gms/maps/model/MarkerOptions;
    invoke-virtual {p3}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->getMapPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->position(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 244
    if-eqz p2, :cond_0

    .line 245
    invoke-virtual {p2, v0, p3}, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->choose(Lcom/google/android/gms/maps/model/MarkerOptions;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)V

    .line 247
    :cond_0
    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/GoogleMap;->addMarker(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/Marker;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method animate(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions;)V
    .locals 6
    .param p1, "transitions"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions;

    .prologue
    .line 42
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->state:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;

    if-nez v2, :cond_0

    .line 43
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->optionsRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    .line 44
    .local v1, "options":Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->hostRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$Host;

    .line 45
    .local v0, "host":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$Host;
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 46
    new-instance v2, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;

    iget-object v3, p1, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions;->animated:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;-><init>(Ljava/util/ArrayList;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$1;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->state:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;

    .line 47
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->transitions:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions;

    .line 48
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->state:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;

    const-string/jumbo v3, "value"

    const/4 v4, 0x2

    new-array v4, v4, [F

    fill-array-data v4, :array_0

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animator:Landroid/animation/ObjectAnimator;

    .line 49
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2, p0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 50
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2, p0}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 51
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->getTransitionDuration()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 52
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->getTransitionInterpolator()Landroid/view/animation/Interpolator;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 53
    invoke-interface {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$Host;->onClusterTransitionStarting()V

    .line 54
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    .line 57
    .end local v0    # "host":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$Host;
    .end local v1    # "options":Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    :cond_0
    return-void

    .line 48
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method cancel()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 75
    :cond_0
    return-void
.end method

.method getAnimatedDestinationClusterPoint(Lcom/google/android/gms/maps/model/Marker;)Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    .locals 2
    .param p1, "marker"    # Lcom/google/android/gms/maps/model/Marker;

    .prologue
    .line 60
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animatedTransitionsByMarker:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;

    .line 61
    .local v0, "animatedTransition":Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;
    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;->getDestinationClusterPoint()Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    move-result-object v1

    .line 64
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method getStationaryClusterPoint(Lcom/google/android/gms/maps/model/Marker;)Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    .locals 1
    .param p1, "marker"    # Lcom/google/android/gms/maps/model/Marker;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->stationaryTransitionsByMarker:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    return-object v0
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 146
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 154
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->hostRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$Host;

    .line 155
    .local v0, "host":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$Host;
    if-eqz v0, :cond_0

    .line 156
    invoke-interface {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$Host;->onClusterTransitionFinished()V

    .line 158
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 167
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 14
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 178
    iget-object v13, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->mapRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/maps/GoogleMap;

    .line 179
    .local v5, "map":Lcom/google/android/gms/maps/GoogleMap;
    iget-object v13, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->optionsRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    .line 180
    .local v8, "options":Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    if-eqz v5, :cond_1

    if-eqz v8, :cond_1

    .line 181
    invoke-virtual {v8}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->getMarkerOptionsChooser()Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser;

    move-result-object v7

    check-cast v7, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;

    .line 184
    .local v7, "moc":Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;
    iget-object v13, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->state:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;->getTransitions()Ljava/util/ArrayList;

    move-result-object v2

    .line 185
    .local v2, "animatedTransitions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 186
    .local v1, "animatedTransitionCount":I
    new-array v13, v1, [Lcom/google/android/gms/maps/model/Marker;

    iput-object v13, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animatedMarkers:[Lcom/google/android/gms/maps/model/Marker;

    .line 187
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v1, :cond_0

    .line 188
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;

    .line 189
    .local v0, "animatedTransition":Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;->getOriginClusterRelevantInputPoints()Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    move-result-object v9

    .line 190
    .local v9, "origin":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    invoke-direct {p0, v5, v7, v9}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->addMarker(Lcom/google/android/gms/maps/GoogleMap;Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)Lcom/google/android/gms/maps/model/Marker;

    move-result-object v6

    .line 192
    .local v6, "marker":Lcom/google/android/gms/maps/model/Marker;
    iget-object v13, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animatedMarkers:[Lcom/google/android/gms/maps/model/Marker;

    aput-object v6, v13, v4

    .line 193
    iget-object v13, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animatedTransitionsByMarker:Ljava/util/HashMap;

    invoke-virtual {v13, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 197
    .end local v0    # "animatedTransition":Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;
    .end local v6    # "marker":Lcom/google/android/gms/maps/model/Marker;
    .end local v9    # "origin":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    :cond_0
    iget-object v13, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->transitions:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions;

    iget-object v12, v13, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions;->stationary:Ljava/util/ArrayList;

    .line 198
    .local v12, "stationaryClusters":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;>;"
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 199
    .local v11, "stationaryClusterCount":I
    if-lez v11, :cond_1

    .line 200
    new-array v13, v11, [Lcom/google/android/gms/maps/model/Marker;

    iput-object v13, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->stationaryMarkers:[Lcom/google/android/gms/maps/model/Marker;

    .line 201
    const/4 v4, 0x0

    :goto_1
    if-ge v4, v11, :cond_1

    .line 202
    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    .line 203
    .local v10, "stationaryCluster":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    invoke-direct {p0, v5, v7, v10}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->addMarker(Lcom/google/android/gms/maps/GoogleMap;Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)Lcom/google/android/gms/maps/model/Marker;

    move-result-object v6

    .line 205
    .restart local v6    # "marker":Lcom/google/android/gms/maps/model/Marker;
    iget-object v13, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->stationaryMarkers:[Lcom/google/android/gms/maps/model/Marker;

    aput-object v6, v13, v4

    .line 206
    iget-object v13, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->stationaryTransitionsByMarker:Ljava/util/HashMap;

    invoke-virtual {v13, v6, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 211
    .end local v1    # "animatedTransitionCount":I
    .end local v2    # "animatedTransitions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;>;"
    .end local v4    # "i":I
    .end local v6    # "marker":Lcom/google/android/gms/maps/model/Marker;
    .end local v7    # "moc":Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;
    .end local v10    # "stationaryCluster":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    .end local v11    # "stationaryClusterCount":I
    .end local v12    # "stationaryClusters":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;>;"
    :cond_1
    iget-object v13, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->hostRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$Host;

    .line 212
    .local v3, "host":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$Host;
    invoke-interface {v3}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$Host;->onClusterTransitionStarted()V

    .line 213
    return-void
.end method

.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4
    .param p1, "animator"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 131
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->state:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animatedMarkers:[Lcom/google/android/gms/maps/model/Marker;

    if-eqz v2, :cond_0

    .line 132
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->state:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;

    # invokes: Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;->getPositions()[Lcom/google/android/gms/maps/model/LatLng;
    invoke-static {v2}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;->access$100(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;)[Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    .line 133
    .local v1, "positions":[Lcom/google/android/gms/maps/model/LatLng;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animatedMarkers:[Lcom/google/android/gms/maps/model/Marker;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 134
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animatedMarkers:[Lcom/google/android/gms/maps/model/Marker;

    aget-object v2, v2, v0

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/model/Marker;->setPosition(Lcom/google/android/gms/maps/model/LatLng;)V

    .line 133
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 137
    .end local v0    # "i":I
    .end local v1    # "positions":[Lcom/google/android/gms/maps/model/LatLng;
    :cond_0
    return-void
.end method

.method onHostPlottedDestinationClusterPoints()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 220
    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animatedMarkers:[Lcom/google/android/gms/maps/model/Marker;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animatedMarkers:[Lcom/google/android/gms/maps/model/Marker;

    array-length v4, v4

    if-lez v4, :cond_1

    .line 221
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animatedMarkers:[Lcom/google/android/gms/maps/model/Marker;

    .local v0, "arr$":[Lcom/google/android/gms/maps/model/Marker;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 222
    .local v3, "marker":Lcom/google/android/gms/maps/model/Marker;
    invoke-virtual {v3}, Lcom/google/android/gms/maps/model/Marker;->remove()V

    .line 221
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 224
    .end local v3    # "marker":Lcom/google/android/gms/maps/model/Marker;
    :cond_0
    iput-object v5, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animatedMarkers:[Lcom/google/android/gms/maps/model/Marker;

    .line 227
    .end local v0    # "arr$":[Lcom/google/android/gms/maps/model/Marker;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->stationaryMarkers:[Lcom/google/android/gms/maps/model/Marker;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->stationaryMarkers:[Lcom/google/android/gms/maps/model/Marker;

    array-length v4, v4

    if-lez v4, :cond_3

    .line 228
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->stationaryMarkers:[Lcom/google/android/gms/maps/model/Marker;

    .restart local v0    # "arr$":[Lcom/google/android/gms/maps/model/Marker;
    array-length v2, v0

    .restart local v2    # "len$":I
    const/4 v1, 0x0

    .restart local v1    # "i$":I
    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 229
    .restart local v3    # "marker":Lcom/google/android/gms/maps/model/Marker;
    invoke-virtual {v3}, Lcom/google/android/gms/maps/model/Marker;->remove()V

    .line 228
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 231
    .end local v3    # "marker":Lcom/google/android/gms/maps/model/Marker;
    :cond_2
    iput-object v5, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->stationaryMarkers:[Lcom/google/android/gms/maps/model/Marker;

    .line 234
    .end local v0    # "arr$":[Lcom/google/android/gms/maps/model/Marker;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    :cond_3
    iput-object v5, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->state:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;

    .line 235
    iput-object v5, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->transitions:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions;

    .line 236
    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animatedTransitionsByMarker:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 237
    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->stationaryTransitionsByMarker:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 238
    iput-object v5, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animator:Landroid/animation/ObjectAnimator;

    .line 239
    return-void
.end method
