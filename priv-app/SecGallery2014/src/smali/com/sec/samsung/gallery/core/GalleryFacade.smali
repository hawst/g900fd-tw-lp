.class public Lcom/sec/samsung/gallery/core/GalleryFacade;
.super Lorg/puremvc/java/patterns/facade/Facade;
.source "GalleryFacade.java"


# static fields
.field private static mGalleryFacadeInstances:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/samsung/gallery/core/GalleryFacade;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCommandMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Lorg/puremvc/java/interfaces/ICommand;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 103
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mGalleryFacadeInstances:Landroid/util/SparseArray;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0}, Lorg/puremvc/java/patterns/facade/Facade;-><init>()V

    .line 104
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    .line 111
    new-instance v0, Lcom/sec/samsung/gallery/core/GalleryFacade$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/core/GalleryFacade$1;-><init>(Lcom/sec/samsung/gallery/core/GalleryFacade;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->view:Lorg/puremvc/java/core/View;

    .line 112
    new-instance v0, Lcom/sec/samsung/gallery/core/GalleryFacade$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/core/GalleryFacade$2;-><init>(Lcom/sec/samsung/gallery/core/GalleryFacade;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->model:Lorg/puremvc/java/core/Model;

    .line 113
    new-instance v0, Lcom/sec/samsung/gallery/core/GalleryFacade$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/core/GalleryFacade$3;-><init>(Lcom/sec/samsung/gallery/core/GalleryFacade;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->controller:Lorg/puremvc/java/core/Controller;

    .line 119
    invoke-direct {p0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->initCommands()V

    .line 120
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/core/GalleryFacade;)Lorg/puremvc/java/core/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/core/GalleryFacade;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->view:Lorg/puremvc/java/core/View;

    return-object v0
.end method

.method public static getInstance(I)Lcom/sec/samsung/gallery/core/GalleryFacade;
    .locals 2
    .param p0, "galleryId"    # I

    .prologue
    .line 123
    sget-object v1, Lcom/sec/samsung/gallery/core/GalleryFacade;->mGalleryFacadeInstances:Landroid/util/SparseArray;

    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/core/GalleryFacade;

    .line 124
    .local v0, "instance":Lcom/sec/samsung/gallery/core/GalleryFacade;
    if-nez v0, :cond_0

    .line 125
    new-instance v0, Lcom/sec/samsung/gallery/core/GalleryFacade;

    .end local v0    # "instance":Lcom/sec/samsung/gallery/core/GalleryFacade;
    invoke-direct {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;-><init>()V

    .line 126
    .restart local v0    # "instance":Lcom/sec/samsung/gallery/core/GalleryFacade;
    sget-object v1, Lcom/sec/samsung/gallery/core/GalleryFacade;->mGalleryFacadeInstances:Landroid/util/SparseArray;

    invoke-virtual {v1, p0, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 129
    :cond_0
    return-object v0
.end method

.method public static getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;
    .locals 1
    .param p0, "gallery"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryId()I

    move-result v0

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(I)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    return-object v0
.end method

.method private initCommands()V
    .locals 3

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "DESTROY"

    const-class v2, Lcom/sec/samsung/gallery/controller/DestroyCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "START_GALLERY_VIEW"

    const-class v2, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "POST_GALLERY"

    const-class v2, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "START_CAMERA"

    const-class v2, Lcom/sec/samsung/gallery/controller/StartCameraCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "COPY_TO_CLIPBOARD"

    const-class v2, Lcom/sec/samsung/gallery/controller/CopyToClipBoardCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "PRINT_IMAGE"

    const-class v2, Lcom/sec/samsung/gallery/controller/PrintImageCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "CROP_IMAGE"

    const-class v2, Lcom/sec/samsung/gallery/controller/CropImageCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "VIDEO_PLAY"

    const-class v2, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "IMAGE_EDIT"

    const-class v2, Lcom/sec/samsung/gallery/controller/ImageEditCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "FLASH_ANNOTATE"

    const-class v2, Lcom/sec/samsung/gallery/controller/FlashAnnotateCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "S_STUDIO"

    const-class v2, Lcom/sec/samsung/gallery/controller/SStudioCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "PHOTO_SIGNATURE"

    const-class v2, Lcom/sec/samsung/gallery/controller/PhotoSignatureCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "ROTATE_FILES"

    const-class v2, Lcom/sec/samsung/gallery/controller/RotateFilesCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "RENAME_ALBUM_OR_FILE"

    const-class v2, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "VIDEO_TRIM"

    const-class v2, Lcom/sec/samsung/gallery/controller/VideoTrimCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SET_DNIE_MODE"

    const-class v2, Lcom/sec/samsung/gallery/controller/SetDnieModeCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "ANDROID_BEAM"

    const-class v2, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "START_IMAGE_NOTE"

    const-class v2, Lcom/sec/samsung/gallery/controller/StartImageNoteCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "START_GOOGLE_MAPS"

    const-class v2, Lcom/sec/samsung/gallery/controller/StartGoogleMapsCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SHOW_ON_MAP"

    const-class v2, Lcom/sec/samsung/gallery/controller/ShowOnMapCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "START_NAVIGATION"

    const-class v2, Lcom/sec/samsung/gallery/controller/StartNavigation;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "START_SLIDESHOW"

    const-class v2, Lcom/sec/samsung/gallery/controller/StartSlideShowCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "START_CONTACT_PICK"

    const-class v2, Lcom/sec/samsung/gallery/controller/StartContactPickCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "START_STORY_ALBUM_APP"

    const-class v2, Lcom/sec/samsung/gallery/controller/StartStoryAlbumAppCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SOUND_SCENE"

    const-class v2, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "CPU_BOOST"

    const-class v2, Lcom/sec/samsung/gallery/controller/CpuBoostCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "USE_MULTI_CORE_CPU"

    const-class v2, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "START_COLLAGE_APP"

    const-class v2, Lcom/sec/samsung/gallery/controller/StartCollageCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "MAKE_MOTION_PICTURE"

    const-class v2, Lcom/sec/samsung/gallery/controller/MakeMotionPictureCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "START_FACE_SCANING"

    const-class v2, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SHOW_DELETE_DIALOG"

    const-class v2, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    iget-object v1, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v2, "SHOW_SHARE_DIALOG"

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseChooserActivity:Z

    if-eqz v0, :cond_0

    const-class v0, Lcom/sec/samsung/gallery/controller/ShareViaCmd;

    :goto_0
    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SHOW_USAGE_ALERT_DIALOG"

    const-class v2, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "LAST_SHARE_APP"

    const-class v2, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SHOW_SET_AS_DIALOG"

    const-class v2, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SHOW_NEW_ALBUM_DIALOG"

    const-class v2, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SHOW_RENAME_DIALOG"

    const-class v2, Lcom/sec/samsung/gallery/controller/ShowRenameDialogCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SHOW_DETAILS_DIALOG"

    const-class v2, Lcom/sec/samsung/gallery/controller/ShowDetailsDialogCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SHOW_COPY_MOVE_ALBUM_LIST_DIALOG"

    const-class v2, Lcom/sec/samsung/gallery/controller/ShowCopyMoveDialogCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SHOW_NEW_ALBUM_COPY_MOVE_DIALOG"

    const-class v2, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SHOW_NEW_ALBUM_CANCEL_DIALOG"

    const-class v2, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCancelDialogCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SHOW_FACES_SCAN_DIALOG"

    const-class v2, Lcom/sec/samsung/gallery/controller/ShowFacesScanDialogCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SHOW_IMPORT_DIALOG"

    const-class v2, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SHOW_SLIDESHOW_MUSIC_LIST"

    const-class v2, Lcom/sec/samsung/gallery/controller/ShowSlideshowMusicListCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SHOW_BARGEIN_NOTIFICATION"

    const-class v2, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SHOW_CONTENT_TO_DISPLAY_DIALOG"

    const-class v2, Lcom/sec/samsung/gallery/controller/ShowContentToDisplayDialogCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SHOW_EDIT_BUSRT_SHOT_DIALOG"

    const-class v2, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SHOW_BURST_SHOT_PLAY_SETTING_DIALOG"

    const-class v2, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SHOW_IMAGE_VIDEO_CONVERSION_DIALOG"

    const-class v2, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SHOW_IMAGE_VIDEO_SHARE_DIALOG"

    const-class v2, Lcom/sec/samsung/gallery/controller/ShowImageVideoShareDialogCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "MAP_VIEW_START"

    const-class v2, Lcom/sec/samsung/gallery/controller/MapViewStartCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "HIDE_ALBUMS"

    const-class v2, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "START_FACE_TAG"

    const-class v2, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "HIDE_ROOT_VIEW"

    const-class v2, Lcom/sec/samsung/gallery/controller/HideRootViewCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "DOWNLOAD_CLOUD"

    const-class v2, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "START_MMS_SAVE"

    const-class v2, Lcom/sec/samsung/gallery/controller/StartMmsSaveCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "DMR_VIEW_CONTROLLER"

    const-class v2, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "CONVERT_MULTI_FORMAT_TO_MP4"

    const-class v2, Lcom/sec/samsung/gallery/controller/ConvertMultiFormatToMP4Cmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "MOVE_TO_SECRETBOX"

    const-class v2, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SECRET_MODE_RECEIVER"

    const-class v2, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "CREATE_VIDEO_ALBUM"

    const-class v2, Lcom/sec/samsung/gallery/controller/StartCreateVideoAlbumCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "MOVE_TO_KNOX"

    const-class v2, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "KNOX_MODE_RECEIVER"

    const-class v2, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "COPY_TO_EVENT"

    const-class v2, Lcom/sec/samsung/gallery/controller/CopyToEventCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "START_CATEGORY_TAG"

    const-class v2, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "PLAY_3DTOUR"

    const-class v2, Lcom/sec/samsung/gallery/controller/Play3DTourCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "SOUNDSCENE_EDITOR"

    const-class v2, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "MAGIC_SHOT_STUDIO"

    const-class v2, Lcom/sec/samsung/gallery/controller/MagicShotStudioCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "ADD_SLIDESHOW_MUSIC"

    const-class v2, Lcom/sec/samsung/gallery/controller/AddSlideshowMusicCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "MEDIASCANNER_RECEIVER"

    const-class v2, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "PLAY_OUTOFFOCUS_IMAGE"

    const-class v2, Lcom/sec/samsung/gallery/controller/PlayOutOfFocusCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "PLAY_SEQUENCE_IMAGE"

    const-class v2, Lcom/sec/samsung/gallery/controller/PlaySequenceCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "ADD_USER_TAG"

    const-class v2, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "START_IDLE_PROCESS"

    const-class v2, Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "RECOMMEND_DROPBOX"

    const-class v2, Lcom/sec/samsung/gallery/controller/StartRecommendDropboxCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "IMAGE_VIEWER_START"

    const-class v2, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "PICKER_START"

    const-class v2, Lcom/sec/samsung/gallery/controller/PickerStartCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "PICKER_ITEM_SELECTED"

    const-class v2, Lcom/sec/samsung/gallery/controller/PickerItemSelected;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "MULTIPLE_PICKER_SELECTION_COMPLETED"

    const-class v2, Lcom/sec/samsung/gallery/controller/MultiplePickerSelectionCompleted;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "TILT_TUTORIAL_START"

    const-class v2, Lcom/sec/samsung/gallery/controller/TiltTutorialStartCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "START_HELP"

    const-class v2, Lcom/sec/samsung/gallery/controller/StartHelpCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    const-string v1, "START_GIF_MAKER"

    const-class v2, Lcom/sec/samsung/gallery/controller/StartGifEditCmd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    return-void

    .line 214
    :cond_0
    const-class v0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;

    goto/16 :goto_0
.end method

.method private needCommandRegistration(Ljava/lang/String;)Z
    .locals 1
    .param p1, "notificationName"    # Ljava/lang/String;

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static removeInstance(I)V
    .locals 1
    .param p0, "galleryId"    # I

    .prologue
    .line 137
    sget-object v0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mGalleryFacadeInstances:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->remove(I)V

    .line 138
    return-void
.end method

.method public static removeInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 1
    .param p0, "gallery"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryId()I

    move-result v0

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeInstance(I)V

    .line 142
    return-void
.end method


# virtual methods
.method public sendNotification(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4
    .param p1, "notificationName"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/Object;

    .prologue
    .line 146
    const/4 v2, 0x0

    .line 148
    .local v2, "isCommandExist":Z
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->hasCommand(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 152
    :goto_0
    if-nez v2, :cond_0

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->needCommandRegistration(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 153
    iget-object v3, p0, Lcom/sec/samsung/gallery/core/GalleryFacade;->mCommandMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 156
    .local v0, "commandClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/puremvc/java/interfaces/ICommand;>;"
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/puremvc/java/interfaces/ICommand;

    invoke-virtual {p0, p1, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerCommand(Ljava/lang/String;Lorg/puremvc/java/interfaces/ICommand;)V
    :try_end_1
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_4

    .line 172
    .end local v0    # "commandClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/puremvc/java/interfaces/ICommand;>;"
    :cond_0
    invoke-super {p0, p1, p2}, Lorg/puremvc/java/patterns/facade/Facade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 173
    :goto_1
    return-void

    .line 149
    :catch_0
    move-exception v1

    .line 150
    .local v1, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    invoke-virtual {v1}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0

    .line 157
    .end local v1    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    .restart local v0    # "commandClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/puremvc/java/interfaces/ICommand;>;"
    :catch_1
    move-exception v1

    .line 158
    .local v1, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v1}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_1

    .line 160
    .end local v1    # "e":Ljava/lang/InstantiationException;
    :catch_2
    move-exception v1

    .line 161
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 163
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v1

    .line 164
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 166
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catch_4
    move-exception v1

    .line 167
    .local v1, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    invoke-virtual {v1}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1
.end method

.method public startup(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 281
    return-void
.end method
