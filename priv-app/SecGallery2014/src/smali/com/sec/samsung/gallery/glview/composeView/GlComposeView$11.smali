.class Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$11;
.super Ljava/lang/Object;
.source "GlComposeView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 0

    .prologue
    .line 1252
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$11;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 7
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/4 v6, 0x1

    .line 1254
    move-object v0, p1

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 1255
    .local v0, "comObject":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    iget v1, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 1257
    .local v1, "index":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$11;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$700(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setExpansionMode(Z)V

    .line 1258
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$11;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-boolean v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mClickable:Z

    if-nez v4, :cond_1

    .line 1265
    :cond_0
    :goto_0
    return v6

    .line 1259
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$11;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$11;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->isRunning()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1260
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$11;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # invokes: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->scaleAnimationForSelectionMode(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)Z
    invoke-static {v4, v0, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$1200(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1262
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$11;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # invokes: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->convertToViewPointX(Lcom/sec/android/gallery3d/glcore/GlObject;I)I
    invoke-static {v4, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$1300(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;Lcom/sec/android/gallery3d/glcore/GlObject;I)I

    move-result v2

    .line 1263
    .local v2, "pressedX":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$11;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # invokes: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->convertToViewPointY(Lcom/sec/android/gallery3d/glcore/GlObject;I)I
    invoke-static {v4, p1, p3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$1400(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;Lcom/sec/android/gallery3d/glcore/GlObject;I)I

    move-result v3

    .line 1264
    .local v3, "pressedY":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$11;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v4, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->handleLongClick(III)Z

    goto :goto_0
.end method
