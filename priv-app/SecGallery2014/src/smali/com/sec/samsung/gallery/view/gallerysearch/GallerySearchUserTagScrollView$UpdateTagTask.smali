.class public Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;
.super Ljava/lang/Thread;
.source "GallerySearchUserTagScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "UpdateTagTask"
.end annotation


# instance fields
.field private volatile mDirty:Z

.field private volatile mIsActive:Z

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;Ljava/lang/String;)V
    .locals 1
    .param p2, "threadName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 1038
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    .line 1039
    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 1036
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;->mIsActive:Z

    .line 1037
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;->mDirty:Z

    .line 1040
    return-void
.end method


# virtual methods
.method public declared-synchronized notifyDirty()V
    .locals 1

    .prologue
    .line 1057
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;->mDirty:Z

    .line 1058
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1059
    monitor-exit p0

    return-void

    .line 1057
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1043
    :goto_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;->mIsActive:Z

    if-eqz v0, :cond_1

    .line 1044
    monitor-enter p0

    .line 1045
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;->mDirty:Z

    if-nez v0, :cond_0

    .line 1046
    invoke-static {p0}, Lcom/sec/android/gallery3d/common/Utils;->waitWithoutInterrupt(Ljava/lang/Object;)V

    .line 1047
    monitor-exit p0

    goto :goto_0

    .line 1049
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1050
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;->mDirty:Z

    .line 1051
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->updateTagList()V

    .line 1052
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1054
    :cond_1
    return-void
.end method

.method public declared-synchronized terminate()V
    .locals 1

    .prologue
    .line 1062
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$UpdateTagTask;->mIsActive:Z

    .line 1063
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1064
    monitor-exit p0

    return-void

    .line 1062
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
