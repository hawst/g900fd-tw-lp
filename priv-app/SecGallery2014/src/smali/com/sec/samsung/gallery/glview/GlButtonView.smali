.class public Lcom/sec/samsung/gallery/glview/GlButtonView;
.super Lcom/sec/android/gallery3d/glcore/GlImageView;
.source "GlButtonView.java"


# static fields
.field private static final CHANGE_BUTTON_IMAGE_TO_NORMAL:I = 0x64

.field private static final CHANGE_BUTTON_IMAGE_TO_PRESSED:I = 0x65

.field private static final CHANGE_HOVER_POINTE:I = 0x1


# instance fields
.field private final BUTTON_GAP:I

.field private isPressed:Z

.field private mClickListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

.field private mContext:Landroid/content/Context;

.field protected mDetectorListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

.field private mFocusedDrawableId:I

.field private mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

.field private mHoverLabelListener:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$GlHoverLabelListener;

.field mIsPreviousState:Z

.field private mMargine:I

.field protected mMoveDetect:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

.field private mNormalDrawableId:I

.field private mPressedDrawableId:I

.field protected mResMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

.field private mVibrator:Landroid/os/Vibrator;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlRootView;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "glRoot"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    const/4 v1, 0x0

    .line 46
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mMoveDetect:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    .line 27
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mFocusedDrawableId:I

    .line 28
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mPressedDrawableId:I

    .line 29
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mNormalDrawableId:I

    .line 30
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->isPressed:Z

    .line 39
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->BUTTON_GAP:I

    .line 172
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mIsPreviousState:Z

    .line 47
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mContext:Landroid/content/Context;

    .line 48
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mResMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    .line 49
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlButtonView;->initMoveDetectorListener()V

    .line 50
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mVibrator:Landroid/os/Vibrator;

    .line 53
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlButtonView$1;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/glview/GlButtonView$1;-><init>(Lcom/sec/samsung/gallery/glview/GlButtonView;Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/glview/GlButtonView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlButtonView;

    .prologue
    .line 22
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mNormalDrawableId:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/glview/GlButtonView;)Landroid/content/res/Resources;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlButtonView;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/glview/GlButtonView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlButtonView;

    .prologue
    .line 22
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mPressedDrawableId:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/glview/GlButtonView;)Landroid/content/res/Resources;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlButtonView;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/glview/GlButtonView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlButtonView;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlButtonView;->playSoundAndHapticFeedback()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/glview/GlButtonView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlButtonView;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->isPressed:Z

    return v0
.end method

.method static synthetic access$502(Lcom/sec/samsung/gallery/glview/GlButtonView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlButtonView;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->isPressed:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/glview/GlButtonView;)Landroid/content/res/Resources;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlButtonView;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/glview/GlButtonView;)Landroid/content/res/Resources;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlButtonView;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/glview/GlButtonView;)Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlButtonView;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mClickListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    return-object v0
.end method

.method private btnRect()Landroid/graphics/Rect;
    .locals 6

    .prologue
    .line 232
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mRect:Lcom/sec/android/gallery3d/glcore/GlRect;

    iget v1, v1, Lcom/sec/android/gallery3d/glcore/GlRect;->mLeft:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mRect:Lcom/sec/android/gallery3d/glcore/GlRect;

    iget v2, v2, Lcom/sec/android/gallery3d/glcore/GlRect;->mTop:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mRect:Lcom/sec/android/gallery3d/glcore/GlRect;

    iget v3, v3, Lcom/sec/android/gallery3d/glcore/GlRect;->mLeft:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mRect:Lcom/sec/android/gallery3d/glcore/GlRect;

    iget v4, v4, Lcom/sec/android/gallery3d/glcore/GlRect;->mWidth:I

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x1

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mRect:Lcom/sec/android/gallery3d/glcore/GlRect;

    iget v4, v4, Lcom/sec/android/gallery3d/glcore/GlRect;->mTop:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mRect:Lcom/sec/android/gallery3d/glcore/GlRect;

    iget v5, v5, Lcom/sec/android/gallery3d/glcore/GlRect;->mHeight:I

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x1

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method private checkPosIn(IILandroid/graphics/Rect;)Z
    .locals 2
    .param p1, "eventX"    # I
    .param p2, "eventY"    # I
    .param p3, "parentRect"    # Landroid/graphics/Rect;

    .prologue
    .line 223
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mRect:Lcom/sec/android/gallery3d/glcore/GlRect;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRect;->mLeft:I

    add-int/lit8 v0, v0, 0xa

    if-gt v0, p1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mRect:Lcom/sec/android/gallery3d/glcore/GlRect;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlRect;->mLeft:I

    add-int/lit8 v0, v0, 0xa

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mRect:Lcom/sec/android/gallery3d/glcore/GlRect;

    iget v1, v1, Lcom/sec/android/gallery3d/glcore/GlRect;->mWidth:I

    add-int/lit8 v1, v1, -0x14

    add-int/2addr v0, v1

    if-gt p1, v0, :cond_0

    iget v0, p3, Landroid/graphics/Rect;->top:I

    if-gt v0, p2, :cond_0

    iget v0, p3, Landroid/graphics/Rect;->bottom:I

    if-gt p2, v0, :cond_0

    .line 225
    const/4 v0, 0x1

    .line 227
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initMoveDetectorListener()V
    .locals 1

    .prologue
    .line 75
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlButtonView$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlButtonView$2;-><init>(Lcom/sec/samsung/gallery/glview/GlButtonView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mDetectorListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    .line 127
    return-void
.end method

.method private playSoundAndHapticFeedback()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 253
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getModeAirViewSoundAndHaptic(Landroid/content/Context;)I

    move-result v0

    .line 254
    .local v0, "mode":I
    if-eq v0, v3, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 255
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    .line 257
    :cond_1
    if-eq v0, v3, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 258
    :cond_2
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseVibetones:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-eqz v1, :cond_4

    .line 259
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    .line 267
    :cond_3
    :goto_0
    return-void

    .line 261
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mVibrator:Landroid/os/Vibrator;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v1}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 262
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v1}, Landroid/os/Vibrator;->cancel()V

    .line 263
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mVibrator:Landroid/os/Vibrator;

    const-wide/16 v2, 0xc

    invoke-virtual {v1, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    goto :goto_0
.end method


# virtual methods
.method public dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 175
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 176
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    float-to-int v4, v8

    .line 177
    .local v4, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    float-to-int v5, v8

    .line 178
    .local v5, "y":I
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlButtonView;->btnRect()Landroid/graphics/Rect;

    move-result-object v3

    .line 180
    .local v3, "parentRect":Landroid/graphics/Rect;
    const/4 v1, 0x0

    .line 181
    .local v1, "isInnerArea":Z
    const/4 v2, 0x0

    .line 182
    .local v2, "isSuccess":Z
    invoke-direct {p0, v4, v5, v3}, Lcom/sec/samsung/gallery/glview/GlButtonView;->checkPosIn(IILandroid/graphics/Rect;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 183
    const/4 v1, 0x1

    .line 186
    :cond_0
    const/16 v8, 0x9

    if-eq v0, v8, :cond_1

    const/4 v8, 0x7

    if-ne v0, v8, :cond_5

    .line 187
    :cond_1
    if-eqz v1, :cond_4

    .line 188
    iget-boolean v8, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mIsPreviousState:Z

    if-nez v8, :cond_2

    .line 189
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mResMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mResources:Landroid/content/res/Resources;

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mFocusedDrawableId:I

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/sec/samsung/gallery/glview/GlButtonView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 190
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mHoverLabelListener:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$GlHoverLabelListener;

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mMargine:I

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlButtonView;->getId()I

    move-result v10

    invoke-interface {v8, v9, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$GlHoverLabelListener;->onEnter(II)Z

    .line 191
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlButtonView;->playSoundAndHapticFeedback()V

    .line 192
    const/4 v2, 0x1

    .line 200
    :cond_2
    :goto_0
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mIsPreviousState:Z

    .line 207
    :cond_3
    :goto_1
    if-eqz v2, :cond_6

    .line 209
    :goto_2
    return v6

    .line 195
    :cond_4
    iget-boolean v8, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mIsPreviousState:Z

    if-ne v8, v6, :cond_2

    .line 196
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mResMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mResources:Landroid/content/res/Resources;

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mNormalDrawableId:I

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/sec/samsung/gallery/glview/GlButtonView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 197
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mHoverLabelListener:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$GlHoverLabelListener;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlButtonView;->getId()I

    move-result v9

    invoke-interface {v8, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$GlHoverLabelListener;->onExit(I)Z

    goto :goto_0

    .line 201
    :cond_5
    const/16 v8, 0xa

    if-ne v0, v8, :cond_3

    .line 202
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mResMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mResources:Landroid/content/res/Resources;

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mNormalDrawableId:I

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/sec/samsung/gallery/glview/GlButtonView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 203
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mHoverLabelListener:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$GlHoverLabelListener;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlButtonView;->getId()I

    move-result v9

    invoke-interface {v8, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$GlHoverLabelListener;->onExit(I)Z

    .line 204
    iput-boolean v7, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mIsPreviousState:Z

    goto :goto_1

    :cond_6
    move v6, v7

    .line 209
    goto :goto_2
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 159
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mMoveDetect:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    if-nez v2, :cond_0

    .line 160
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->getInstance(Lcom/sec/android/gallery3d/glcore/GlRootView;)Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mMoveDetect:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    .line 162
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlButtonView;->getChildCount()I

    move-result v0

    .line 163
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 164
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlButtonView;->getChild(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/glcore/GlView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 163
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 166
    :cond_1
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->isPressed:Z

    if-nez v2, :cond_2

    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlButtonView;->btnRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 167
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mMoveDetect:Lcom/sec/android/gallery3d/glcore/GlMovementDetector;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mDetectorListener:Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;

    invoke-virtual {v2, p1, v3}, Lcom/sec/android/gallery3d/glcore/GlMovementDetector;->onTouch(Landroid/view/MotionEvent;Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;)Z

    move-result v2

    .line 169
    :goto_1
    return v2

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getButtonRightMargine()I
    .locals 1

    .prologue
    .line 238
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mMargine:I

    return v0
.end method

.method public getPressedD()I
    .locals 1

    .prologue
    .line 150
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mPressedDrawableId:I

    return v0
.end method

.method public getRect()Lcom/sec/android/gallery3d/glcore/GlRect;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mRect:Lcom/sec/android/gallery3d/glcore/GlRect;

    return-object v0
.end method

.method public nonPressedD()I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mNormalDrawableId:I

    return v0
.end method

.method protected playSoundOnClickThumb()V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 131
    return-void
.end method

.method public resetButton()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 246
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/16 v1, 0x64

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move v3, v2

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIIILjava/lang/Object;)V

    .line 249
    :cond_0
    return-void
.end method

.method public setButtonRightMargine(I)V
    .locals 0
    .param p1, "margine"    # I

    .prologue
    .line 242
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mMargine:I

    .line 243
    return-void
.end method

.method public setClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mClickListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .line 143
    return-void
.end method

.method public setFocusedDrawableId(I)V
    .locals 0
    .param p1, "drawableId"    # I

    .prologue
    .line 134
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mFocusedDrawableId:I

    .line 135
    return-void
.end method

.method public setHoverLabelListener(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$GlHoverLabelListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$GlHoverLabelListener;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mHoverLabelListener:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$GlHoverLabelListener;

    .line 147
    return-void
.end method

.method public setImageResource(I)V
    .locals 0
    .param p1, "resId"    # I

    .prologue
    .line 214
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 215
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mNormalDrawableId:I

    .line 216
    return-void
.end method

.method public setPressedDrawableId(I)V
    .locals 0
    .param p1, "drawableId"    # I

    .prologue
    .line 138
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView;->mPressedDrawableId:I

    .line 139
    return-void
.end method
