.class Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$1;
.super Landroid/telephony/PhoneStateListener;
.source "SlideShowViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 2
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    .line 210
    packed-switch p1, :pswitch_data_0

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 212
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowPauseView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$000(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mSlideShowPauseView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$000(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setIndicatorVisibility(Z)V

    .line 214
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    const v1, 0x7f0e0066

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 215
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->finishCurrentViewState()V

    goto :goto_0

    .line 210
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
