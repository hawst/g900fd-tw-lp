.class public Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "SecretModeReceiverCmd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd$CmdType;
    }
.end annotation


# static fields
.field private static final PERSONAL_PAGE_OFF:Ljava/lang/String; = "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

.field private static final PERSONAL_PAGE_ON:Ljava/lang/String; = "com.samsung.android.intent.action.PRIVATE_MODE_ON"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mSecretModeReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 31
    new-instance v0, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private registerReceiver(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 77
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 78
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.android.intent.action.PRIVATE_MODE_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 79
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 81
    new-instance v0, Landroid/content/IntentFilter;

    .end local v0    # "filter":Landroid/content/IntentFilter;
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 82
    .restart local v0    # "filter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 83
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 84
    return-void
.end method

.method private unregisterReceiver(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 87
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 89
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    iput-object v3, p0, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    sget-object v1, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;->TAG:Ljava/lang/String;

    const-string v2, "catch IllegalArgumentException and ignore it"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 94
    iput-object v3, p0, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    goto :goto_0

    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v1

    iput-object v3, p0, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;->mSecretModeReceiver:Landroid/content/BroadcastReceiver;

    throw v1
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 4
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 66
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Object;

    move-object v2, v3

    check-cast v2, [Ljava/lang/Object;

    .line 67
    .local v2, "params":[Ljava/lang/Object;
    const/4 v3, 0x0

    aget-object v1, v2, v3

    check-cast v1, Landroid/content/Context;

    .line 68
    .local v1, "context":Landroid/content/Context;
    const/4 v3, 0x1

    aget-object v0, v2, v3

    check-cast v0, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd$CmdType;

    .line 70
    .local v0, "cmdType":Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd$CmdType;
    sget-object v3, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd$CmdType;->REGISTER_RECEIVER:Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd$CmdType;

    if-ne v0, v3, :cond_1

    .line 71
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;->registerReceiver(Landroid/content/Context;)V

    .line 74
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    sget-object v3, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd$CmdType;->UNREGISTER_RECEIVER:Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd$CmdType;

    if-ne v0, v3, :cond_0

    .line 73
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/SecretModeReceiverCmd;->unregisterReceiver(Landroid/content/Context;)V

    goto :goto_0
.end method
