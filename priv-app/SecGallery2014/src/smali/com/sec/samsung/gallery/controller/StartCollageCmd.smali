.class public Lcom/sec/samsung/gallery/controller/StartCollageCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "StartCollageCmd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;
    }
.end annotation


# static fields
.field public static final COLLAGE_ALBUM_PATH:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    const-class v0, Lcom/sec/samsung/gallery/controller/StartCollageCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/StartCollageCmd;->TAG:Ljava/lang/String;

    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Collage"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/StartCollageCmd;->COLLAGE_ALBUM_PATH:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 32
    return-void
.end method

.method public static isCollageAlbumFile(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 4
    .param p0, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v2, 0x0

    .line 54
    if-nez p0, :cond_1

    .line 66
    :cond_0
    :goto_0
    return v2

    .line 58
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v1

    .line 59
    .local v1, "path":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 62
    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 63
    .local v0, "directoryPath":Ljava/lang/String;
    sget-object v3, Lcom/sec/samsung/gallery/controller/StartCollageCmd;->COLLAGE_ALBUM_PATH:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCollage:Z

    if-eqz v3, :cond_0

    .line 64
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private startCollageMaker(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    move-object v9, p1

    check-cast v9, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v7

    .line 89
    .local v7, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    new-instance v1, Ljava/util/LinkedList;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v9

    invoke-direct {v1, v9}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 90
    .local v1, "copiedList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 91
    .local v6, "selectedItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v4

    .local v4, "n":I
    :goto_0
    if-ge v2, v4, :cond_1

    .line 92
    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 93
    .local v5, "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v5, :cond_0

    .line 94
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v8

    .line 95
    .local v8, "uri":Landroid/net/Uri;
    if-eqz v8, :cond_0

    .line 96
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    .end local v8    # "uri":Landroid/net/Uri;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 100
    .end local v5    # "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    new-instance v3, Landroid/content/Intent;

    const-string v9, "com.sec.android.mimage.photoretouching.multigrid"

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 101
    .local v3, "makeCollageIntent":Landroid/content/Intent;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 102
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v9, "selectedItems"

    invoke-virtual {v0, v9, v6}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 103
    invoke-virtual {v3, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 104
    const-string v9, "selectedCount"

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 107
    :try_start_0
    invoke-virtual {p1, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    :goto_1
    return-void

    .line 108
    :catch_0
    move-exception v9

    goto :goto_1
.end method

.method private startCollageViewer(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 71
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 72
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    new-instance v0, Landroid/content/ComponentName;

    const-string v3, "com.sec.android.app.collage"

    const-string v4, "com.sec.android.app.collage.CollageView"

    invoke-direct {v0, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    .local v0, "cn":Landroid/content/ComponentName;
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 76
    invoke-virtual {v2, p2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 78
    :try_start_0
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    const/16 v3, 0x203

    invoke-virtual {p1, v2, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    return-void

    .line 79
    :catch_0
    move-exception v1

    .line 80
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    sget-object v3, Lcom/sec/samsung/gallery/controller/StartCollageCmd;->TAG:Ljava/lang/String;

    const-string v4, "Activity Not Found"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 5
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 40
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    move-object v2, v4

    check-cast v2, [Ljava/lang/Object;

    .line 41
    .local v2, "params":[Ljava/lang/Object;
    const/4 v4, 0x0

    aget-object v1, v2, v4

    check-cast v1, Landroid/content/Context;

    .line 42
    .local v1, "context":Landroid/content/Context;
    const/4 v4, 0x1

    aget-object v0, v2, v4

    check-cast v0, Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;

    .line 44
    .local v0, "cmdType":Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;
    sget-object v4, Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;->START_COLLAGE_MAKER:Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;

    if-ne v0, v4, :cond_1

    .line 45
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/StartCollageCmd;->startCollageMaker(Landroid/content/Context;)V

    .line 51
    :cond_0
    :goto_0
    return-void

    .line 46
    :cond_1
    sget-object v4, Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;->START_COLLAGE_VIEWER:Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;

    if-ne v0, v4, :cond_0

    .line 47
    const/4 v4, 0x2

    aget-object v3, v2, v4

    check-cast v3, Landroid/net/Uri;

    .line 49
    .local v3, "uri":Landroid/net/Uri;
    invoke-direct {p0, v1, v3}, Lcom/sec/samsung/gallery/controller/StartCollageCmd;->startCollageViewer(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0
.end method
