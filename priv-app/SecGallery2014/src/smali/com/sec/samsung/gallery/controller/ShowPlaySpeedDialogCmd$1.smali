.class Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$1;
.super Ljava/lang/Object;
.source "ShowPlaySpeedDialogCmd.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setPlaySpeed(Landroid/content/Context;I)V

    .line 62
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->mAdapter:Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->access$100(Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;)Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$SpeedItemAdapter;->notifyDataSetInvalidated()V

    .line 63
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->mPlaySpeedDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->access$200(Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 64
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;

    const/4 v1, 0x0

    # setter for: Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->mPlaySpeedDialog:Landroid/app/AlertDialog;
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->access$202(Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 65
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ShowPlaySpeedDialogCmd;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "BUSRT_SHOT_SETTING_CHANGED"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 67
    return-void
.end method
