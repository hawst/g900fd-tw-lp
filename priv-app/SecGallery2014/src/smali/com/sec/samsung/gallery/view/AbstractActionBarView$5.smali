.class Lcom/sec/samsung/gallery/view/AbstractActionBarView$5;
.super Ljava/lang/Object;
.source "AbstractActionBarView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/AbstractActionBarView;->showUpButtonOnTopLeft()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView$5;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 188
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView$5;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->isUpButtonVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 189
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView$5;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0f024b

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 190
    .local v0, "upButton":Landroid/widget/ImageButton;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 191
    new-instance v1, Lcom/sec/samsung/gallery/view/AbstractActionBarView$5$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView$5$1;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarView$5;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    new-instance v1, Lcom/sec/samsung/gallery/view/AbstractActionBarView$5$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView$5$2;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarView$5;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 204
    .end local v0    # "upButton":Landroid/widget/ImageButton;
    :cond_0
    return-void
.end method
