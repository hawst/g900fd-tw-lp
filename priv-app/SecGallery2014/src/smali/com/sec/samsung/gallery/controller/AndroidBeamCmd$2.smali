.class Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$2;
.super Ljava/lang/Object;
.source "AndroidBeamCmd.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->initBeamHelper()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createBeamUris(Landroid/nfc/NfcEvent;)[Landroid/net/Uri;
    .locals 3
    .param p1, "event"    # Landroid/nfc/NfcEvent;

    .prologue
    const/4 v0, 0x0

    .line 123
    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string v2, "create beam uris"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mListener:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$400(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;

    move-result-object v1

    if-nez v1, :cond_1

    .line 132
    :cond_0
    :goto_0
    return-object v0

    .line 127
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    # invokes: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->isSbeamOn()Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$500(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 128
    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string v2, "sbeam uri create"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 131
    :cond_2
    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string v2, "abeam uri create"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNdefStatus:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$600(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mListener:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$400(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;->onGetFilePath()[Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method
