.class Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionTask;
.super Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;
.source "AlbumViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SelectionTask"
.end annotation


# instance fields
.field mSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mType:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionType;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionType;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onProgressListener"    # Lcom/sec/android/gallery3d/data/OnProgressListener;
    .param p3, "type"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionType;
    .param p4, "maxCount"    # I

    .prologue
    .line 2084
    invoke-direct {p0, p1, p2, p4}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;I)V

    .line 2085
    iput-object p3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionTask;->mType:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionType;

    .line 2086
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionTask;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionTask;->mSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 2087
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 2080
    check-cast p1, [Lcom/sec/android/gallery3d/data/MediaSet;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionTask;->doInBackground([Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/Void;
    .locals 10
    .param p1, "arg0"    # [Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    const-wide/16 v8, 0x1

    const/4 v6, 0x0

    .line 2091
    aget-object v1, p1, v6

    .line 2092
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-object v4, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$19;->$SwitchMap$com$sec$samsung$gallery$view$albumview$AlbumViewState$SelectionType:[I

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionTask;->mType:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionType;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2116
    const-string v4, "AlbumViewState"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknow type : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionTask;->mType:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionType;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2119
    :cond_0
    :goto_0
    const/4 v4, 0x0

    return-object v4

    .line 2094
    :pswitch_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionTask;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionTask;->mSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->toggleSelectProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;)V
    invoke-static {v4, v5, v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$6100(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_0

    .line 2097
    :pswitch_1
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v2

    .line 2098
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "ix":I
    :goto_1
    if-ge v0, v2, :cond_0

    .line 2099
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionTask;->isCancelled()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2102
    invoke-virtual {p0, v8, v9, v6}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionTask;->increaseProgress(JZ)V

    .line 2103
    const/4 v4, 0x1

    invoke-virtual {p0, v8, v9, v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionTask;->increaseProgress(JZ)V

    .line 2105
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    .line 2106
    .local v3, "subMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionTask;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionTask;->mSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->addSelectProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;)V
    invoke-static {v4, v5, v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$6200(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 2098
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2110
    .end local v0    # "ix":I
    .end local v2    # "size":I
    .end local v3    # "subMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :pswitch_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionTask;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionTask;->mSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->addSelectProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;)V
    invoke-static {v4, v5, v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$6200(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_0

    .line 2113
    :pswitch_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionTask;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionTask;->mSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->removeSelectProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;)V
    invoke-static {v4, v5, v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$6300(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_0

    .line 2092
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
