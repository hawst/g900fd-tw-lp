.class public Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GlSpreadAnim"
.end annotation


# instance fields
.field private mDoComplete:Z

.field private mDoneShow:Z

.field private mLastAnimStep:I

.field private mShowStep:I

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGridView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V
    .locals 0

    .prologue
    .line 1466
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    return-void
.end method

.method private procShow(Z)V
    .locals 8
    .param p1, "doAnim"    # Z

    .prologue
    const/4 v7, 0x1

    .line 1493
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->mShowStep:I

    .line 1494
    .local v1, "curStep":I
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->mShowStep:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->mShowStep:I

    .line 1495
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->mLastAnimStep:I

    if-ne v1, v5, :cond_1

    .line 1511
    :cond_0
    :goto_0
    return-void

    .line 1496
    :cond_1
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->mLastAnimStep:I

    .line 1497
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->mLastAnimStep:I

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemVVisibleCnt:I

    div-int v0, v5, v6

    .line 1498
    .local v0, "col":I
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->mLastAnimStep:I

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemVVisibleCnt:I

    rem-int v3, v5, v6

    .line 1499
    .local v3, "row":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemHVisibleCnt:I

    mul-int/2addr v5, v3

    add-int v2, v5, v0

    .line 1500
    .local v2, "index":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mVisibleFirst:I

    add-int/2addr v6, v2

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->getThumbByIndex(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 1501
    .local v4, "thumb":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    if-eqz v4, :cond_2

    .line 1502
    sget v5, Lcom/sec/samsung/gallery/glview/GlScroller;->VISIBLE_BY_INIT:I

    invoke-virtual {v4, v7, v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setVisibility(ZI)V

    .line 1503
    if-eqz p1, :cond_2

    .line 1504
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->startShowUpAnim(I)V

    .line 1507
    :cond_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemVisibleCnt:I

    add-int/lit8 v5, v5, -0x1

    if-lt v2, v5, :cond_0

    .line 1508
    iput-boolean v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->mDoneShow:Z

    .line 1509
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->stop()V

    goto :goto_0
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 1
    .param p1, "ratio"    # F

    .prologue
    .line 1514
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->procShow(Z)V

    .line 1515
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1518
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->mDoComplete:Z

    if-eqz v0, :cond_1

    .line 1519
    :goto_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->mDoneShow:Z

    if-nez v0, :cond_0

    .line 1520
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->procShow(Z)V

    goto :goto_0

    .line 1522
    :cond_0
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->mDoComplete:Z

    .line 1524
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mSpreadAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1800(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1525
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mSpreadAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1800(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 1526
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mSpreadAnim:Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1802(Lcom/sec/samsung/gallery/glview/GlGridView;Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;)Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;

    .line 1528
    :cond_2
    return-void
.end method

.method public startChange()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1473
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->mDoneShow:Z

    .line 1474
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->mDoComplete:Z

    .line 1475
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->mLastAnimStep:I

    .line 1476
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->mShowStep:I

    .line 1477
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mConfig:Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1700(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;

    move-result-object v0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;->glspread_anim_time:I

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->setDuration(J)V

    .line 1478
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->start()V

    .line 1479
    return-void
.end method

.method public stop(Z)V
    .locals 0
    .param p1, "complete"    # Z

    .prologue
    .line 1482
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->mDoComplete:Z

    .line 1483
    invoke-super {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->stop()V

    .line 1484
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView$GlSpreadAnim;->onStop()V

    .line 1485
    return-void
.end method
