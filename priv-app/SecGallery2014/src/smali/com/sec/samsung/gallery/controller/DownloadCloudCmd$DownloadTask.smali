.class Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;
.super Landroid/os/AsyncTask;
.source "DownloadCloudCmd.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DownloadTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;",
        "Landroid/content/DialogInterface$OnCancelListener;"
    }
.end annotation


# instance fields
.field private final mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;)V
    .locals 3

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;->this$0:Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 59
    new-instance v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;-><init>(II)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$1;

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;-><init>(Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 57
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 11
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    const/4 v10, 0x0

    .line 71
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;->this$0:Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;->access$100(Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;)Landroid/content/Context;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v6

    .line 72
    .local v6, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v5

    .line 73
    .local v5, "medias":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 91
    :cond_0
    return-object v10

    .line 75
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 76
    .local v3, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/remote/cloud/CloudImage;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 77
    .local v4, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;->isCancelled()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 85
    .end local v4    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    .line 86
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;->isCancelled()Z

    move-result v7

    if-nez v7, :cond_0

    .line 87
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;->this$0:Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;->access$100(Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;->download(Landroid/content/Context;)V

    .line 88
    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Integer;

    const/4 v8, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;->publishProgress([Ljava/lang/Object;)V

    .line 86
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 80
    .end local v0    # "count":I
    .end local v1    # "i":I
    .restart local v4    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_4
    instance-of v7, v4, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;

    if-nez v7, :cond_5

    instance-of v7, v4, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;

    if-eqz v7, :cond_2

    .line 81
    :cond_5
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;->this$0:Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local v4    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    # invokes: Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;->getCloudItems(Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/util/List;
    invoke-static {v7, v4}, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;->access$200(Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 110
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;->cancel(Z)Z

    .line 111
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 57
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 102
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 103
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;->this$0:Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;->access$100(Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "EXIT_SELECTION_MODE"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 105
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->closeProgressDialog()V

    .line 106
    return-void
.end method

.method protected onPreExecute()V
    .locals 6

    .prologue
    .line 64
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 65
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;->this$0:Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;->access$100(Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;->this$0:Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;->access$100(Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e010b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->showProgressDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 67
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 3
    .param p1, "values"    # [Ljava/lang/Integer;

    .prologue
    .line 96
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 97
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    aget-object v2, p1, v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->updateProgress(II)V

    .line 98
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 57
    check-cast p1, [Ljava/lang/Integer;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
