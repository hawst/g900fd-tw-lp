.class public Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;
.super Lcom/sec/samsung/gallery/glview/GlScroller;
.source "GlGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ItemSetScroller"
.end annotation


# instance fields
.field private mAlbumPitchRate:F

.field private mFullVisibleCnt:I

.field public mScrollHEnd:F

.field public mScrollHStart:F

.field public mScrollVisibleW:F

.field public mSelectionModePadding:F

.field private mSkipObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

.field private mThumbnailHGap:F

.field private paddingLeft:F

.field private paddingTop:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGridView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1531
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlScroller;-><init>()V

    .line 1533
    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mAlbumPitchRate:F

    .line 1535
    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScrollHStart:F

    .line 1536
    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScrollHEnd:F

    .line 1540
    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mSelectionModePadding:F

    .line 1542
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mSkipObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    return-void
.end method

.method private adjustTextObjects(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V
    .locals 10
    .param p1, "thmObj"    # Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    .line 1606
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v7, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 1607
    .local v0, "dimensionUtil":Lcom/sec/samsung/gallery/util/DimensionUtil;
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I
    invoke-static {v7}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1900(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v4

    .line 1613
    .local v4, "viewMode":I
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v3

    .line 1614
    .local v3, "txtObj":Lcom/sec/samsung/gallery/glview/GlTextObject;
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemW:F

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNameTextboxXOffset(I)F

    move-result v8

    mul-float/2addr v8, v9

    sub-float v2, v7, v8

    .line 1615
    .local v2, "textBoxWidth":F
    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNameTextboxHeight(I)F

    move-result v1

    .line 1616
    .local v1, "textBoxHeight":F
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemW:F

    neg-float v7, v7

    div-float v5, v7, v9

    .line 1617
    .local v5, "x":F
    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNameTextboxXOffset(I)F

    move-result v7

    div-float v8, v2, v9

    add-float/2addr v7, v8

    add-float/2addr v5, v7

    .line 1618
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemH:F

    neg-float v7, v7

    div-float v6, v7, v9

    .line 1619
    .local v6, "y":F
    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNameTextboxYOffset(I)F

    move-result v7

    div-float v8, v1, v9

    add-float/2addr v7, v8

    sub-float/2addr v6, v7

    .line 1620
    if-eqz v3, :cond_0

    .line 1621
    const/4 v7, 0x0

    invoke-virtual {v3, v5, v6, v7}, Lcom/sec/samsung/gallery/glview/GlTextObject;->setPos(FFF)V

    .line 1622
    invoke-virtual {v3, v2, v1}, Lcom/sec/samsung/gallery/glview/GlTextObject;->setSize(FF)V

    .line 1624
    :cond_0
    return-void
.end method


# virtual methods
.method public getAlbumPitchRate()F
    .locals 1

    .prologue
    .line 1757
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mAlbumPitchRate:F

    return v0
.end method

.method protected getItemRowCount()I
    .locals 3

    .prologue
    .line 1828
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemCount:I

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->widthMultiple:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$2200(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->heightMultiple:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$2100(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v2

    mul-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemHVisibleCnt:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemHVisibleCnt:I

    div-int/2addr v0, v1

    return v0
.end method

.method public getMoveOrder(III)I
    .locals 3
    .param p1, "nRowCount"    # I
    .param p2, "nPressedRow"    # I
    .param p3, "nCurrRow"    # I

    .prologue
    .line 1810
    int-to-float v1, p1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-gt p2, v1, :cond_1

    .line 1811
    if-gt p3, p2, :cond_0

    .line 1812
    sub-int v0, p2, p3

    .line 1823
    .local v0, "order":I
    :goto_0
    return v0

    .line 1814
    .end local v0    # "order":I
    :cond_0
    move v0, p3

    .restart local v0    # "order":I
    goto :goto_0

    .line 1817
    .end local v0    # "order":I
    :cond_1
    add-int/lit8 v1, p1, -0x1

    sub-int/2addr v1, p2

    if-gt p3, v1, :cond_2

    .line 1818
    add-int v0, p2, p3

    .restart local v0    # "order":I
    goto :goto_0

    .line 1820
    .end local v0    # "order":I
    :cond_2
    add-int/lit8 v1, p1, -0x1

    sub-int v0, v1, p3

    .restart local v0    # "order":I
    goto :goto_0
.end method

.method public getScrollLength()F
    .locals 3

    .prologue
    .line 1680
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1900(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewTopPadding(I)F

    move-result v0

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemGapH:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemRowCount:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public getThumbPosition(ILcom/sec/android/gallery3d/glcore/GlPos;)V
    .locals 7
    .param p1, "index"    # I
    .param p2, "retPos"    # Lcom/sec/android/gallery3d/glcore/GlPos;

    .prologue
    .line 1764
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlGridView;->mColCnt:I

    rem-int v0, p1, v4

    .line 1765
    .local v0, "col":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlGridView;->mColCnt:I

    div-int v1, p1, v4

    .line 1766
    .local v1, "row":I
    int-to-float v4, v0

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemGapW:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemStartX:F

    add-float v2, v4, v5

    .line 1767
    .local v2, "x":F
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemStartY:F

    int-to-float v5, v1

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemGapH:F

    mul-float/2addr v5, v6

    sub-float v3, v4, v5

    .line 1769
    .local v3, "y":F
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScroll:F

    add-float/2addr v4, v3

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemStartZ:F

    invoke-virtual {p2, v2, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlPos;->set(FFF)V

    .line 1770
    return-void
.end method

.method public makeVisible(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 9
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    const/4 v8, 0x1

    const/high16 v6, 0x40000000    # 2.0f

    const/4 v7, 0x0

    .line 1833
    if-nez p1, :cond_0

    .line 1857
    :goto_0
    return-void

    .line 1835
    :cond_0
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getY()F

    move-result v3

    .line 1836
    .local v3, "y":F
    invoke-virtual {p1, v8}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getHeight(Z)F

    move-result v1

    .line 1837
    .local v1, "h":F
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScroll:F

    .line 1838
    .local v2, "newScroll":F
    const/4 v0, 0x0

    .line 1840
    .local v0, "deltaY":F
    cmpl-float v4, v3, v7

    if-lez v4, :cond_3

    .line 1841
    div-float v4, v1, v6

    add-float/2addr v4, v3

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlGridView;->mHeightSpace:F

    div-float/2addr v5, v6

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v6

    sub-float/2addr v5, v6

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->paddingTop:F

    sub-float/2addr v5, v6

    sub-float v0, v4, v5

    .line 1842
    cmpl-float v4, v0, v7

    if-lez v4, :cond_2

    .line 1843
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScroll:F

    sub-float v2, v4, v0

    .line 1855
    :cond_1
    :goto_1
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->onMove(F)V

    .line 1856
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->setScroll(F)V

    goto :goto_0

    .line 1844
    :cond_2
    invoke-static {v0, v7}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->isAlmostEquals(FF)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1845
    const/4 v2, 0x0

    goto :goto_1

    .line 1848
    :cond_3
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 1849
    div-float v4, v1, v6

    sub-float v4, v3, v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlGridView;->mHeightSpace:F

    div-float/2addr v5, v6

    add-float/2addr v4, v5

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v5

    invoke-virtual {v5, v8}, Lcom/sec/samsung/gallery/glview/GlTextObject;->getHeight(Z)F

    move-result v5

    sub-float v0, v4, v5

    .line 1850
    :cond_4
    cmpg-float v4, v0, v7

    if-gez v4, :cond_1

    .line 1851
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScroll:F

    sub-float v2, v4, v0

    goto :goto_1
.end method

.method public onFocusChange(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 1718
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    if-eqz v0, :cond_0

    .line 1719
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;->onFocusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V

    .line 1721
    :cond_0
    return-void
.end method

.method public onMove(F)V
    .locals 1
    .param p1, "delta"    # F

    .prologue
    .line 1714
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mSx:F
    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$2302(Lcom/sec/samsung/gallery/glview/GlGridView;F)F

    .line 1715
    return-void
.end method

.method public onScrollStep(I)V
    .locals 3
    .param p1, "step"    # I

    .prologue
    .line 1724
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnScrollListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    sget v2, Lcom/sec/samsung/gallery/glview/GlAbsListView;->SCROLL_STATE_FLING:I

    invoke-interface {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;->onScrollStateChanged(Lcom/sec/samsung/gallery/glview/GlAbsListView;I)V

    .line 1725
    return-void
.end method

.method public register([Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 4
    .param p1, "objSet"    # [Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    .line 1592
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/GlScroller;->register([Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 1594
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 1595
    .local v0, "glThumbSet":[Lcom/sec/samsung/gallery/glview/GlBaseObject;
    const/4 v1, 0x0

    .local v1, "i":I
    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->ALBUM_ITEM_MAX:I
    invoke-static {}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1100()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 1596
    aget-object v3, v0, v1

    check-cast v3, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-direct {p0, v3}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->adjustTextObjects(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    .line 1595
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1598
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$600(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->getDragObject()Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->adjustTextObjects(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V

    .line 1600
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$600(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1601
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$600(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->registerObject([Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 1603
    :cond_1
    return-void
.end method

.method public reset()V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 1545
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 1547
    .local v0, "dimensionUtil":Lcom/sec/samsung/gallery/util/DimensionUtil;
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1900(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewLeftPadding(I)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->paddingLeft:F

    .line 1549
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mMode:I

    sget v2, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_NORMAL:I

    if-eq v1, v2, :cond_0

    .line 1550
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1900(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewTopPadding(I)F

    move-result v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getSelectionModeBarHeight()F

    move-result v2

    add-float/2addr v1, v2

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->paddingTop:F

    .line 1555
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryActivity;

    # invokes: Lcom/sec/samsung/gallery/glview/GlGridView;->isTabletPickerMode(Lcom/sec/android/gallery3d/app/GalleryActivity;)Z
    invoke-static {v2, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$2000(Lcom/sec/samsung/gallery/glview/GlGridView;Lcom/sec/android/gallery3d/app/GalleryActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1556
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getScreenWidth()F

    move-result v1

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemW:F

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlGridView;->mColCnt:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlGridView;->mColCnt:I

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mThumbnailHGap:F

    .line 1563
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1900(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewThumbnailWidth(I)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemW:F

    .line 1564
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1900(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewThumbnailHeight(I)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemH:F

    .line 1566
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemW:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mThumbnailHGap:F

    add-float/2addr v1, v2

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemGapW:F

    .line 1567
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemH:F

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1900(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewVerticalGap(I)F

    move-result v2

    add-float/2addr v1, v2

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemGapH:F

    .line 1569
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mColCnt:I

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemHVisibleCnt:I

    .line 1570
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mRowCnt:I

    add-int/lit8 v1, v1, 0x3

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemVVisibleCnt:I

    .line 1571
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemHVisibleCnt:I

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemVVisibleCnt:I

    mul-int/2addr v1, v2

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemVisibleCnt:I

    .line 1572
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mWidthSpace:F

    div-float/2addr v1, v4

    neg-float v1, v1

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemW:F

    div-float/2addr v2, v4

    add-float/2addr v1, v2

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->paddingLeft:F

    add-float/2addr v1, v2

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemStartX:F

    .line 1573
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mHeightSpace:F

    div-float/2addr v1, v4

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemH:F

    div-float/2addr v2, v4

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v2

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->paddingTop:F

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemStartY:F

    .line 1574
    sget v1, Lcom/sec/samsung/gallery/glview/GlAbsListView;->DEF_DISTANCE:F

    neg-float v1, v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemStartZ:F

    .line 1575
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mWidthSpace:F

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScrollVisibleW:F

    .line 1576
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mHeightSpace:F

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v2

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->paddingTop:F

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScrollVisibleH:F

    .line 1577
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mHeightSpace:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemGapH:F

    div-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mFullVisibleCnt:I

    .line 1578
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemGapH:F

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScrollHStart:F

    .line 1579
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScrollVisibleH:F

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScrollHEnd:F

    .line 1580
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemGapH:F

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScrollVStart:F

    .line 1581
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScrollVisibleH:F

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScrollVEnd:F

    .line 1585
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->heightMultiple:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$2100(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScrollOffset:I

    .line 1587
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v2

    invoke-virtual {v1, p0, v2}, Lcom/sec/samsung/gallery/glview/GlGridView;->resetScrollBar(Lcom/sec/samsung/gallery/glview/GlScroller;F)V

    .line 1588
    return-void

    .line 1553
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1900(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewTopPadding(I)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->paddingTop:F

    goto/16 :goto_0

    .line 1558
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1900(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewHorizontalGap(I)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mThumbnailHGap:F

    goto/16 :goto_1
.end method

.method public resetSet(II)V
    .locals 8
    .param p1, "startIndex"    # I
    .param p2, "totalCount"    # I

    .prologue
    .line 1684
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemHVisibleCnt:I

    add-int/2addr v5, p2

    add-int/lit8 v5, v5, -0x1

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemHVisibleCnt:I

    div-int/2addr v5, v6

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemRowCount:I

    .line 1685
    const/4 v0, 0x0

    .line 1686
    .local v0, "maxRange":F
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScrollVisibleH:F

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->getScrollLength()F

    move-result v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_0

    .line 1687
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->getScrollLength()F

    move-result v5

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScrollVisibleH:F

    sub-float v0, v5, v6

    .line 1692
    :cond_0
    const/4 v3, 0x0

    .line 1694
    .local v3, "setScroll":Z
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemHVisibleCnt:I

    div-int v2, p1, v5

    .line 1695
    .local v2, "rowIndex":I
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemHVisibleCnt:I

    add-int/2addr v5, p2

    add-int/lit8 v5, v5, -0x1

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemHVisibleCnt:I

    div-int v1, v5, v6

    .line 1696
    .local v1, "rowCount":I
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mFullVisibleCnt:I

    if-gt v1, v5, :cond_3

    .line 1697
    const/4 v2, 0x0

    .line 1703
    :cond_1
    :goto_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1500(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    move-result-object v5

    int-to-float v6, v2

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemGapH:F

    mul-float/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 1704
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemHVisibleCnt:I

    mul-int v4, v2, v5

    .line 1706
    .local v4, "startRowIndex":I
    const/4 v5, 0x1

    invoke-super {p0, v4, p2, v5}, Lcom/sec/samsung/gallery/glview/GlScroller;->resetSet(IIZ)V

    .line 1707
    if-eqz v3, :cond_2

    .line 1708
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->setScroll(F)V

    .line 1709
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1500(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 1711
    :cond_2
    return-void

    .line 1698
    .end local v4    # "startRowIndex":I
    :cond_3
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mFullVisibleCnt:I

    add-int/2addr v5, v2

    if-lt v5, v1, :cond_1

    .line 1700
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemGapH:F

    div-float v5, v0, v5

    float-to-int v2, v5

    .line 1701
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public setAlbumPitchRate(F)V
    .locals 6
    .param p1, "rate"    # F

    .prologue
    .line 1732
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->getAlbumPitchRate()F

    move-result v4

    cmpl-float v4, p1, v4

    if-nez v4, :cond_1

    .line 1754
    :cond_0
    return-void

    .line 1735
    :cond_1
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mAlbumPitchRate:F

    .line 1736
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v4, :cond_0

    .line 1740
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemVisibleCnt:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v5, v5

    if-le v4, v5, :cond_2

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v1, v4

    .line 1741
    .local v1, "count":I
    :goto_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v1, :cond_0

    .line 1742
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v3, v4, v2

    .line 1743
    .local v3, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-nez v3, :cond_3

    .line 1741
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1740
    .end local v1    # "count":I
    .end local v2    # "i":I
    .end local v3    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_2
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemVisibleCnt:I

    goto :goto_0

    .line 1746
    .restart local v1    # "count":I
    .restart local v2    # "i":I
    .restart local v3    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_3
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlGridView;->mRowCnt:I

    div-int v0, v4, v5

    .line 1748
    .local v0, "col":I
    if-eqz v0, :cond_4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemCount:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlGridView;->mRowCnt:I

    div-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x1

    sub-int/2addr v4, v0

    if-nez v4, :cond_5

    .line 1749
    :cond_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewEndAffordanceRowDegree(I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, p1

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPitch(F)V

    goto :goto_2

    .line 1751
    :cond_5
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewEndAffordanceRowDegree(I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, p1

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPitch(F)V

    goto :goto_2
.end method

.method public setPositionSkipObject(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 0
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    .line 1728
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mSkipObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 1729
    return-void
.end method

.method public setThumbPosition(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 9
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    .line 1779
    if-nez p1, :cond_1

    .line 1780
    const-string v6, "GlGridView"

    const-string v7, "setThumbPosition: The obj is null."

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1806
    :cond_0
    :goto_0
    return-void

    .line 1783
    :cond_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mSkipObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eq p1, v6, :cond_0

    .line 1787
    iget v0, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 1788
    .local v0, "backup":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mIsDragEnabled:Z
    invoke-static {v6}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$900(Lcom/sec/samsung/gallery/glview/GlGridView;)Z

    move-result v6

    if-eqz v6, :cond_2

    move-object v6, p1

    .line 1789
    check-cast v6, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mDisplayIndex:I

    iput v6, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 1792
    :cond_2
    iget v6, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlGridView;->mColCnt:I

    rem-int v1, v6, v7

    .line 1793
    .local v1, "col":I
    iget v6, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlGridView;->mColCnt:I

    div-int v2, v6, v7

    .line 1794
    .local v2, "row":I
    int-to-float v6, v1

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemGapW:F

    mul-float/2addr v6, v7

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemStartX:F

    add-float v4, v6, v7

    .line 1795
    .local v4, "x":F
    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemStartY:F

    int-to-float v7, v2

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemGapH:F

    mul-float/2addr v7, v8

    sub-float v5, v6, v7

    .line 1797
    .local v5, "y":F
    const/4 v3, 0x0

    .line 1798
    .local v3, "startPosition":F
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    check-cast v6, Lcom/sec/android/gallery3d/app/GalleryActivity;

    # invokes: Lcom/sec/samsung/gallery/glview/GlGridView;->isTabletPickerMode(Lcom/sec/android/gallery3d/app/GalleryActivity;)Z
    invoke-static {v7, v6}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$2000(Lcom/sec/samsung/gallery/glview/GlGridView;Lcom/sec/android/gallery3d/app/GalleryActivity;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1799
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mThumbnailHGap:F

    .line 1801
    :cond_3
    add-float v6, v4, v3

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScroll:F

    add-float/2addr v7, v5

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemStartZ:F

    invoke-virtual {p1, v6, v7, v8}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 1802
    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemW:F

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemH:F

    invoke-virtual {p1, v6, v7}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setSize(FF)V

    .line 1805
    iput v0, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    goto :goto_0
.end method

.method public update(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 14
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    .line 1627
    if-nez p1, :cond_1

    .line 1676
    :cond_0
    :goto_0
    return-void

    .line 1630
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mIsDragEnabled:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$900(Lcom/sec/samsung/gallery/glview/GlGridView;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$600(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1631
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$600(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->updateObject(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 1634
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-ltz v0, :cond_0

    iget v0, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemCount:I

    if-ge v0, v1, :cond_0

    iget v0, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1637
    iget v0, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-nez v0, :cond_3

    .line 1638
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1900(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewThumbnailWidthPixel(I)I

    move-result v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1900(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewHorizontalGapPixel(I)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->widthMultiple:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$2200(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v1

    mul-int v7, v0, v1

    .line 1639
    .local v7, "canvasWidth":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1900(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewHorizontalGapPixel(I)I

    move-result v0

    sub-int/2addr v7, v0

    .line 1640
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1900(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewThumbnailHeightPixel(I)I

    move-result v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1900(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewVerticalGapPixel(I)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->heightMultiple:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$2100(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v1

    mul-int v6, v0, v1

    .line 1641
    .local v6, "canvasHeight":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1900(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewVerticalGapPixel(I)I

    move-result v0

    sub-int/2addr v6, v0

    .line 1647
    :goto_1
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v13

    .line 1648
    .local v13, "textObj":Lcom/sec/samsung/gallery/glview/GlTextObject;
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v8

    .line 1649
    .local v8, "glCanvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    if-nez v8, :cond_4

    .line 1650
    const-string v0, "GlGridView"

    const-string v1, "glCanvas is null. ignore update"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1643
    .end local v6    # "canvasHeight":I
    .end local v7    # "canvasWidth":I
    .end local v8    # "glCanvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    .end local v13    # "textObj":Lcom/sec/samsung/gallery/glview/GlTextObject;
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1900(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewThumbnailWidthPixel(I)I

    move-result v7

    .line 1644
    .restart local v7    # "canvasWidth":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1900(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewThumbnailHeightPixel(I)I

    move-result v6

    .restart local v6    # "canvasHeight":I
    goto :goto_1

    .line 1654
    .restart local v8    # "glCanvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    .restart local v13    # "textObj":Lcom/sec/samsung/gallery/glview/GlTextObject;
    :cond_4
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->getWidth()I

    move-result v0

    if-ne v0, v7, :cond_5

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->getHeight()I

    move-result v0

    if-eq v0, v6, :cond_6

    .line 1655
    :cond_5
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v1, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v0, v1, v7, v6}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {p1, v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 1657
    if-eqz v13, :cond_6

    .line 1658
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1900(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNameTextboxXOffsetPixel(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    sub-int v12, v7, v0

    .line 1659
    .local v12, "textCanvasWidth":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1900(Lcom/sec/samsung/gallery/glview/GlGridView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNameTextboxHeightPixel(I)I

    move-result v11

    .line 1660
    .local v11, "textCanvasHeight":I
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v1, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v0, v1, v12, v11}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {v13, v0}, Lcom/sec/samsung/gallery/glview/GlTextObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 1664
    .end local v11    # "textCanvasHeight":I
    .end local v12    # "textCanvasWidth":I
    :cond_6
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->isDragEnabled()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1665
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget v1, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlGridView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v4, -0x1

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v10

    .line 1669
    .local v10, "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    :goto_2
    invoke-virtual {p1, v10}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 1671
    if-eqz v13, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v0, :cond_0

    .line 1672
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget v1, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/glview/GlTextObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlGridView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v4, 0x0

    move-object v5, v13

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v9

    .line 1673
    .local v9, "glTextView":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {v13, v9}, Lcom/sec/samsung/gallery/glview/GlTextObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    goto/16 :goto_0

    .line 1667
    .end local v9    # "glTextView":Lcom/sec/android/gallery3d/glcore/GlView;
    .end local v10    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_7
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget v1, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlGridView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v4, 0x0

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v10

    .restart local v10    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    goto :goto_2
.end method
