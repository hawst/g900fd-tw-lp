.class public Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlMovetoAnimation.java"


# instance fields
.field hnd:I

.field mDstIndex:I

.field mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

.field mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 9
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlPos;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glcore/GlPos;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    .line 11
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->hnd:I

    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 6
    .param p1, "ratio"    # F

    .prologue
    .line 41
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->msx:F

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mtx:F

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->msx:F

    sub-float/2addr v4, v5

    mul-float/2addr v4, p1

    add-float v0, v3, v4

    .line 42
    .local v0, "dx":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->msy:F

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mty:F

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->msy:F

    sub-float/2addr v4, v5

    mul-float/2addr v4, p1

    add-float v1, v3, v4

    .line 43
    .local v1, "dy":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget v2, v3, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->msz:F

    .line 44
    .local v2, "dz":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v3, v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setPos(FFF)V

    .line 45
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mDstIndex:I

    iput v1, v0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mIndex:I

    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 37
    return-void
.end method

.method public startAnimation(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;II)V
    .locals 4
    .param p1, "posCtrl"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
    .param p2, "dragObject"    # Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    .param p3, "focusedObjIndex"    # I
    .param p4, "duration"    # I

    .prologue
    const/4 v2, 0x0

    .line 14
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .line 15
    iput p3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mDstIndex:I

    .line 17
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mHndDispmnt:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setPosIndex()I

    move-result v1

    iput v1, v0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mHndDispmnt:I

    .line 19
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v0, v2, v2, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setSourcePos(FFF)V

    .line 27
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    invoke-virtual {p1, p3, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getGroupPosition(ILcom/sec/android/gallery3d/glcore/GlPos;)V

    .line 28
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    iget v1, v1, Lcom/sec/android/gallery3d/glcore/GlPos;->mX:F

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    iget v2, v2, Lcom/sec/android/gallery3d/glcore/GlPos;->mY:F

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    iget v3, v3, Lcom/sec/android/gallery3d/glcore/GlPos;->mZ:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setTargetPos(FFF)V

    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 30
    int-to-long v0, p4

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->setDuration(J)V

    .line 31
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->start()V

    .line 32
    return-void

    .line 21
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mHndDispmnt:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->hnd:I

    .line 22
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->hnd:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->addPos(II)V

    .line 23
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->hnd:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->releasePosIndex(I)V

    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->getAbsX()F

    move-result v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->getAbsY()F

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->mObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->getAbsZ()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setSourcePos(FFF)V

    goto :goto_0
.end method
