.class public Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;
.super Landroid/widget/BaseAdapter;
.source "GallerySearchHistoryAdaper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;
    }
.end annotation


# instance fields
.field private final HISTORY_LIST_MAX_COUNT:I

.field private final THRESHOLD_HISTORY_DELETE_ALL:I

.field private mContext:Landroid/content/Context;

.field private mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 29
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->mContext:Landroid/content/Context;

    .line 30
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->mItems:Ljava/util/ArrayList;

    .line 31
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->THRESHOLD_HISTORY_DELETE_ALL:I

    .line 32
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->HISTORY_LIST_MAX_COUNT:I

    .line 36
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->mContext:Landroid/content/Context;

    .line 37
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->init()V

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->deleteHistoryItem(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;)V

    return-void
.end method

.method private deleteHistoryItem(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;)V
    .locals 2
    .param p1, "item"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->deleteHistory(Ljava/lang/String;)V

    .line 138
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->getSuggestionNames()V

    .line 139
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->notifyDataSetChanged()V

    .line 140
    return-void
.end method


# virtual methods
.method protected deleteAllItems()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 143
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string v1, "search_history"

    invoke-virtual {v0, v1, v2, v2}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 144
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->getSuggestionNames()V

    .line 145
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->notifyDataSetChanged()V

    .line 146
    return-void
.end method

.method protected deleteOverMaxCountItems()V
    .locals 4

    .prologue
    .line 149
    const/16 v0, 0x1e

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 150
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->getItem(I)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;

    move-result-object v1

    .line 151
    .local v1, "item":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;
    if-eqz v1, :cond_0

    .line 152
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->deleteHistory(Ljava/lang/String;)V

    .line 149
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 155
    .end local v1    # "item":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->getSuggestionNames()V

    .line 156
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->notifyDataSetChanged()V

    .line 157
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->mItems:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHistoryDeleteAllThreshHold()I
    .locals 1

    .prologue
    .line 160
    const/16 v0, 0xa

    return v0
.end method

.method public getItem(I)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->mItems:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 53
    :cond_0
    const/4 v0, 0x0

    .line 54
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->getItem(I)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 60
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getMaxHistoryListCount()I
    .locals 1

    .prologue
    .line 164
    const/16 v0, 0x1e

    return v0
.end method

.method public getSuggestionNames()V
    .locals 9

    .prologue
    .line 97
    const/4 v6, 0x0

    .line 98
    .local v6, "cursor":Landroid/database/Cursor;
    const-string v5, "date_added DESC"

    .line 99
    .local v5, "orderClause":Ljava/lang/String;
    const/4 v8, 0x0

    .line 101
    .local v8, "name":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    const-string v1, "search_history"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "title"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "date_added"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->mItems:Ljava/util/ArrayList;

    .line 104
    if-eqz v6, :cond_2

    .line 105
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 107
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 108
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->mItems:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;

    invoke-direct {v1, v8}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 111
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    :cond_2
    if-eqz v6, :cond_3

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 118
    :cond_3
    :goto_0
    return-void

    .line 113
    :catch_0
    move-exception v7

    .line 114
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 116
    if-eqz v6, :cond_3

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :cond_4
    throw v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "arg2"    # Landroid/view/ViewGroup;

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->getItem(I)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;

    move-result-object v2

    .line 67
    .local v2, "item":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;
    if-nez p2, :cond_0

    .line 68
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030069

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 72
    :cond_0
    const v4, 0x7f0f0121

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 73
    .local v3, "name":Landroid/widget/TextView;
    if-eqz v2, :cond_1

    .line 74
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    :cond_1
    const v4, 0x7f0f0122

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 76
    .local v1, "divider":Landroid/widget/ImageView;
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 77
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->getCount()I

    move-result v4

    const/16 v5, 0xa

    if-ge v4, v5, :cond_2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne p1, v4, :cond_2

    .line 78
    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 81
    :cond_2
    const v4, 0x7f0f0120

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 82
    .local v0, "btnDelete":Landroid/widget/ImageView;
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 83
    new-instance v4, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$1;

    invoke-direct {v4, p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$1;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;I)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    return-object p2
.end method

.method public init()V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->mDBMgr:Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    .line 42
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->getSuggestionNames()V

    .line 43
    return-void
.end method

.method public updateHistoryList()V
    .locals 0

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->getSuggestionNames()V

    .line 169
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->notifyDataSetChanged()V

    .line 170
    return-void
.end method
