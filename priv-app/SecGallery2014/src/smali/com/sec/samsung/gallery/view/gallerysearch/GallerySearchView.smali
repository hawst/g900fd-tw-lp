.class public Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;
.super Landroid/widget/LinearLayout;
.source "GallerySearchView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchImageSpan;,
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;,
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnUpdateTagListListener;,
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnVoiceRecognitionListener;,
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnSuggestionListener;,
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnQueryTextListener;
    }
.end annotation


# static fields
.field public static final VOICE_RECOGNITION_REQUEST:I = 0x22b


# instance fields
.field private final MAX_INPUT_LENGTH:I

.field private final MSG_NEED_SHOW_CURSOR:I

.field private mCloseButton:Landroid/widget/ImageView;

.field private mCloseTagListButton:Landroid/widget/ImageView;

.field private mContext:Landroid/content/Context;

.field private mEditGroup:Landroid/view/ViewGroup;

.field private mHandler:Landroid/os/Handler;

.field private mHintTextView:Landroid/widget/TextView;

.field private mLengthFilter:Landroid/text/InputFilter$LengthFilter;

.field private mOldQueryText:Ljava/lang/CharSequence;

.field private final mOnClickListener:Landroid/view/View$OnClickListener;

.field private final mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

.field private final mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field private final mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mOnQueryChangeListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnQueryTextListener;

.field private mOnSuggestionListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnSuggestionListener;

.field private mOnUpdateHistoryListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$OnUpdateHistoryListener;

.field private mOnUpdateTagListListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnUpdateTagListListener;

.field private mOnVoiceRecognitionListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnVoiceRecognitionListener;

.field private mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

.field private mSearchIconView:Landroid/widget/ImageView;

.field private mSearchProgress:Landroid/widget/ProgressBar;

.field private mShowImeRunnable:Ljava/lang/Runnable;

.field private mShowTagList:Z

.field private mSuggestionsAdapter:Landroid/widget/CursorAdapter;

.field private mTextWatcher:Landroid/text/TextWatcher;

.field private mVoiceButton:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 130
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 131
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v2, 0x64

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 134
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 82
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mSearchProgress:Landroid/widget/ProgressBar;

    .line 85
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mHintTextView:Landroid/widget/TextView;

    .line 86
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mSearchIconView:Landroid/widget/ImageView;

    .line 91
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mShowTagList:Z

    .line 94
    iput v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->MSG_NEED_SHOW_CURSOR:I

    .line 96
    iput v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->MAX_INPUT_LENGTH:I

    .line 98
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$1;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mShowImeRunnable:Ljava/lang/Runnable;

    .line 283
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$4;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 315
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$5;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$5;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    .line 388
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$6;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$6;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    .line 442
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$7;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$7;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 450
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$8;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$8;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mTextWatcher:Landroid/text/TextWatcher;

    .line 480
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$9;

    invoke-direct {v0, p0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$9;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;I)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mLengthFilter:Landroid/text/InputFilter$LengthFilter;

    .line 135
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mContext:Landroid/content/Context;

    .line 136
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->initSearchView()V

    .line 137
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mShowImeRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setHintTextColor()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->onSubmitQuery()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnSuggestionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnSuggestionListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnSuggestionListener;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;Ljava/lang/CharSequence;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;
    .param p1, "x1"    # Ljava/lang/CharSequence;

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->onTextChanged(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;
    .param p1, "x1"    # I

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setSearchHintTextAndIcon(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mCloseButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->onCloseClicked()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mVoiceButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->startVoiceRecognitionActivity()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Landroid/widget/CursorAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mCloseTagListButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnUpdateTagListListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnUpdateTagListListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnUpdateTagListListener;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mShowTagList:Z

    return v0
.end method

.method static synthetic access$902(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mShowTagList:Z

    return p1
.end method

.method private getDecoratedHint(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 7
    .param p1, "hintText"    # Ljava/lang/CharSequence;

    .prologue
    .line 365
    new-instance v2, Landroid/text/SpannableString;

    if-eqz p1, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-direct {v2, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 367
    .local v2, "ssb":Landroid/text/SpannableString;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0201de

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 368
    .local v0, "searchIcon":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d026f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v1, v3

    .line 369
    .local v1, "size":I
    new-instance v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchImageSpan;

    invoke-direct {v3, p0, v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchImageSpan;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;Landroid/graphics/drawable/Drawable;I)V

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/16 v6, 0x21

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 370
    return-object v2

    .line 365
    .end local v0    # "searchIcon":Landroid/graphics/drawable/Drawable;
    .end local v1    # "size":I
    .end local v2    # "ssb":Landroid/text/SpannableString;
    :cond_0
    const-string v3, " "

    goto :goto_0
.end method

.method private initSearchView()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 140
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 142
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030066

    invoke-virtual {v0, v1, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 144
    const v1, 0x7f0f0118

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mCloseButton:Landroid/widget/ImageView;

    .line 145
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mCloseButton:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    const v1, 0x7f0f0117

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mVoiceButton:Landroid/view/View;

    .line 147
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mVoiceButton:Landroid/view/View;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setVoiceButtonEnabled(Z)V

    .line 149
    const v1, 0x7f0f0113

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mCloseTagListButton:Landroid/widget/ImageView;

    .line 150
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mCloseTagListButton:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    const v1, 0x7f0f00eb

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    .line 153
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d026e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v5, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setTextSize(IF)V

    .line 154
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v1, :cond_0

    .line 155
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0062

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setTextColor(I)V

    .line 156
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-nez v1, :cond_0

    .line 157
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0061

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setCursorColor(I)V

    .line 160
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 162
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 163
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 164
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 165
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    new-array v2, v4, [Landroid/text/InputFilter;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mLengthFilter:Landroid/text/InputFilter$LengthFilter;

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setFilters([Landroid/text/InputFilter;)V

    .line 167
    const v1, 0x7f0f0114

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mEditGroup:Landroid/view/ViewGroup;

    .line 169
    const v1, 0x7f0f0116

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mHintTextView:Landroid/widget/TextView;

    .line 170
    const v1, 0x7f0f0115

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mSearchIconView:Landroid/widget/ImageView;

    .line 171
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setFocusable(Z)V

    .line 172
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setFocusableInTouchMode(Z)V

    .line 175
    const v1, 0x7f0f0119

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mSearchProgress:Landroid/widget/ProgressBar;

    .line 176
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->updateButtons()V

    .line 178
    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$2;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$2;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mHandler:Landroid/os/Handler;

    .line 194
    return-void
.end method

.method private onCloseClicked()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 417
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setText(Ljava/lang/CharSequence;)V

    .line 418
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->requestFocus()Z

    .line 419
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setHintTextColor()V

    .line 420
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-eqz v0, :cond_0

    .line 421
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->dismissDropDown()V

    .line 422
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/app/StateManager;->setSearchText(Ljava/lang/String;)V

    .line 423
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/app/StateManager;->setLastQueryText(Ljava/lang/String;)V

    .line 425
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setImeVisibility(Z)V

    .line 426
    return-void
.end method

.method private onSubmitQuery()V
    .locals 3

    .prologue
    .line 404
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 405
    .local v0, "query":Ljava/lang/CharSequence;
    if-eqz v0, :cond_0

    .line 406
    invoke-static {v0}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v1

    if-lez v1, :cond_1

    .line 407
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnQueryChangeListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnQueryTextListener;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnQueryTextListener;->onQueryTextSubmit(Ljava/lang/String;)Z

    .line 408
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setImeVisibility(Z)V

    .line 409
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->dismissDropDown()V

    .line 414
    :cond_0
    :goto_0
    return-void

    .line 411
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnQueryChangeListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnQueryTextListener;

    const-string v2, ""

    invoke-interface {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnQueryTextListener;->onQueryTextSubmit(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method private onTextChanged(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "newText"    # Ljava/lang/CharSequence;

    .prologue
    .line 396
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->updateButtons()V

    .line 397
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnQueryChangeListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnQueryTextListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOldQueryText:Ljava/lang/CharSequence;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 398
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnQueryChangeListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnQueryTextListener;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnQueryTextListener;->onQueryTextChange(Ljava/lang/String;)Z

    .line 400
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOldQueryText:Ljava/lang/CharSequence;

    .line 401
    return-void
.end method

.method private setHintTextColor()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 608
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-eqz v0, :cond_0

    .line 609
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    const v1, -0x717172

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setHintTextColor(I)V

    .line 616
    :goto_0
    return-void

    .line 610
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_1

    .line 612
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    const/16 v1, 0x59

    invoke-static {v1, v2, v2, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setHintTextColor(I)V

    goto :goto_0

    .line 614
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    const v1, -0x7f0a0a0b

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setHintTextColor(I)V

    goto :goto_0
.end method

.method private setSearchHintTextAndIcon(I)V
    .locals 1
    .param p1, "visible"    # I

    .prologue
    .line 560
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mSearchIconView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 561
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mSearchIconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 564
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mHintTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 565
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mHintTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 567
    :cond_1
    return-void
.end method

.method private startVoiceRecognitionActivity()V
    .locals 5

    .prologue
    .line 429
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnVoiceRecognitionListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnVoiceRecognitionListener;

    if-eqz v2, :cond_0

    .line 430
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnVoiceRecognitionListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnVoiceRecognitionListener;

    invoke-interface {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnVoiceRecognitionListener;->onVoiceRecognitionStarted()V

    .line 432
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 433
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "android.speech.extra.LANGUAGE_MODEL"

    const-string v3, "free_form"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 434
    const-string v2, "android.speech.extra.PROMPT"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0257

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 436
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    const/16 v3, 0x22b

    invoke-virtual {v2, v1, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 440
    :goto_0
    return-void

    .line 437
    :catch_0
    move-exception v0

    .line 438
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private updateButtons()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/16 v3, 0x8

    .line 374
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 375
    .local v0, "empty":Z
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mCloseButton:Landroid/widget/ImageView;

    if-nez v0, :cond_3

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 376
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mVoiceButton:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_4

    :goto_1
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 377
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mCloseButton:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->isShown()Z

    move-result v1

    if-nez v1, :cond_1

    .line 378
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnUpdateHistoryListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$OnUpdateHistoryListener;

    if-eqz v1, :cond_0

    .line 379
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnUpdateHistoryListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$OnUpdateHistoryListener;

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$OnUpdateHistoryListener;->updateHistoryList()V

    .line 380
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->dismissDropDown()V

    .line 382
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setCursorVisible(Z)V

    .line 383
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v1, :cond_2

    .line 384
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mVoiceButton:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 386
    :cond_2
    return-void

    :cond_3
    move v1, v3

    .line 375
    goto :goto_0

    :cond_4
    move v2, v3

    .line 376
    goto :goto_1
.end method


# virtual methods
.method getQueryText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hideProgressBar()V
    .locals 4

    .prologue
    .line 474
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mSearchProgress:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 475
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mSearchProgress:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 476
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 478
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 247
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 248
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 249
    .local v2, "width":I
    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-super {p0, v3, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 251
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 252
    .local v1, "qeryTextViewLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0281

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 253
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    invoke-virtual {v3, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 254
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v3, :cond_0

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-eqz v3, :cond_1

    .line 255
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mCloseTagListButton:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 256
    .local v0, "closeTagListButtonLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0282

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 257
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mCloseTagListButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 259
    .end local v0    # "closeTagListButtonLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 0
    .param p1, "visibility"    # I

    .prologue
    .line 243
    return-void
.end method

.method public setDropDownIcon(Z)V
    .locals 3
    .param p1, "isShow"    # Z

    .prologue
    .line 339
    if-eqz p1, :cond_0

    .line 340
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mCloseTagListButton:Landroid/widget/ImageView;

    const v1, 0x7f0200f1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 341
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mCloseTagListButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e017f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 342
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mShowTagList:Z

    .line 348
    :goto_0
    return-void

    .line 344
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mCloseTagListButton:Landroid/widget/ImageView;

    const v1, 0x7f0200f2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 345
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mCloseTagListButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e017e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 346
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mShowTagList:Z

    goto :goto_0
.end method

.method public setImeVisibility(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 264
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$3;

    invoke-direct {v2, p0, p1, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$3;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;ZLandroid/view/inputmethod/InputMethodManager;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 281
    return-void
.end method

.method public setOnQueryTextListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnQueryTextListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnQueryTextListener;

    .prologue
    .line 200
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnQueryChangeListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnQueryTextListener;

    .line 201
    return-void
.end method

.method public setOnSuggestionListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnSuggestionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnSuggestionListener;

    .prologue
    .line 204
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnSuggestionListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnSuggestionListener;

    .line 205
    return-void
.end method

.method public setOnUpdateTagListListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnUpdateTagListListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnUpdateTagListListener;

    .prologue
    .line 212
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnUpdateTagListListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnUpdateTagListListener;

    .line 213
    return-void
.end method

.method public setOnVoiceRecognitionListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnVoiceRecognitionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnVoiceRecognitionListener;

    .prologue
    .line 208
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnVoiceRecognitionListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnVoiceRecognitionListener;

    .line 209
    return-void
.end method

.method public setQuery(Ljava/lang/CharSequence;Z)V
    .locals 3
    .param p1, "query"    # Ljava/lang/CharSequence;
    .param p2, "submit"    # Z

    .prologue
    .line 217
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setText(Ljava/lang/CharSequence;)V

    .line 218
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->dismissDropDown()V

    .line 220
    if-eqz p1, :cond_0

    .line 221
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setSelection(I)V

    .line 224
    :cond_0
    if-eqz p2, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 225
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->onSubmitQuery()V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    :cond_1
    :goto_0
    return-void

    .line 227
    :catch_0
    move-exception v0

    .line 228
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method

.method public setQueryHint(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "hint"    # Ljava/lang/CharSequence;

    .prologue
    .line 359
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setHint(Ljava/lang/CharSequence;)V

    .line 360
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    invoke-static {}, Lcom/sec/samsung/gallery/util/FontUtil;->getRobotoLightFont()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setTypeface(Landroid/graphics/Typeface;)V

    .line 361
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setHintTextColor()V

    .line 362
    return-void
.end method

.method public setSuggestionsAdapter(Landroid/widget/CursorAdapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/widget/CursorAdapter;

    .prologue
    .line 237
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;

    .line 238
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 239
    return-void
.end method

.method public setUpdateHistoryListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$OnUpdateHistoryListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$OnUpdateHistoryListener;

    .prologue
    .line 604
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mOnUpdateHistoryListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$OnUpdateHistoryListener;

    .line 605
    return-void
.end method

.method public setVoiceButtonEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 197
    return-void
.end method

.method public showDropDownIcon(Z)V
    .locals 4
    .param p1, "isShow"    # Z

    .prologue
    const/4 v1, 0x0

    .line 351
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mCloseTagListButton:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 352
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mShowTagList:Z

    .line 353
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mEditGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz p1, :cond_1

    move v2, v1

    :goto_1
    invoke-virtual {v0, v2, v1, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 355
    return-void

    .line 351
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 353
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d027b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_1
.end method

.method public showProgressBar()V
    .locals 2

    .prologue
    .line 468
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mSearchProgress:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mSearchProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 471
    :cond_0
    return-void
.end method
