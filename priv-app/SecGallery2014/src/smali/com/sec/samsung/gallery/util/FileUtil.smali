.class public Lcom/sec/samsung/gallery/util/FileUtil;
.super Ljava/lang/Object;
.source "FileUtil.java"


# static fields
.field private static final FILE_COPY_CHUNK_SIZE:I = 0x2000

.field private static final TAG:Ljava/lang/String;

.field public static final TOTAL_FILE_SIZE_KEY:Ljava/lang/Integer;

.field public static final USE_FILE_SIZE:I = -0x1

.field private static mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

.field private static mTasks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/sec/android/gallery3d/data/OnProgressListener;",
            "Lcom/sec/samsung/gallery/util/MediaOperations;",
            ">;"
        }
    .end annotation
.end field

.field private static mTasksForMultiple:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/sec/android/gallery3d/data/OnProgressListener;",
            "Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/util/FileUtil;->TOTAL_FILE_SIZE_KEY:Ljava/lang/Integer;

    .line 38
    const-class v0, Lcom/sec/samsung/gallery/util/FileUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/util/FileUtil;->TAG:Ljava/lang/String;

    .line 42
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/samsung/gallery/util/FileUtil;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 44
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/util/FileUtil;->mTasks:Ljava/util/HashMap;

    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/util/FileUtil;->mTasksForMultiple:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/sec/samsung/gallery/util/FileUtil;->mContext:Landroid/content/Context;

    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/FileUtil;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/FileUtil;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 50
    return-void
.end method

.method static synthetic access$000()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/samsung/gallery/util/FileUtil;->mTasks:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/samsung/gallery/util/FileUtil;->mTasksForMultiple:Ljava/util/HashMap;

    return-object v0
.end method

.method private static addPostfix(Ljava/io/File;I)Ljava/lang/String;
    .locals 8
    .param p0, "f"    # Ljava/io/File;
    .param p1, "postfixNum"    # I

    .prologue
    const/16 v7, 0x29

    .line 219
    const/4 v4, 0x0

    .line 221
    .local v4, "resultStr":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 222
    .local v1, "fullName":Ljava/lang/String;
    const/16 v6, 0x2e

    invoke-virtual {v1, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 224
    .local v2, "lastDot":I
    const/4 v6, -0x1

    if-eq v2, v6, :cond_0

    .line 225
    const/4 v6, 0x0

    invoke-virtual {v1, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 226
    .local v3, "name":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v1, v2, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 228
    .local v0, "ext":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 229
    .local v5, "sb":Ljava/lang/StringBuffer;
    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 230
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    .line 236
    .end local v0    # "ext":Ljava/lang/String;
    .end local v3    # "name":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 232
    .end local v5    # "sb":Ljava/lang/StringBuffer;
    :cond_0
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 233
    .restart local v5    # "sb":Ljava/lang/StringBuffer;
    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 234
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public static copyFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 16
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "srcFilePath"    # Ljava/lang/String;
    .param p2, "dstFilePath"    # Ljava/lang/String;
    .param p3, "newFileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    const/4 v11, 0x0

    .line 72
    .local v11, "srcFile":Ljava/io/FileInputStream;
    const/4 v3, 0x0

    .line 73
    .local v3, "dstFile":Ljava/io/FileOutputStream;
    const/4 v13, 0x0

    .line 74
    .local v13, "srcFileChannel":Ljava/nio/channels/FileChannel;
    const/4 v5, 0x0

    .line 75
    .local v5, "dstFileChannel":Ljava/nio/channels/FileChannel;
    if-eqz p3, :cond_1

    .line 76
    new-instance v14, Ljava/io/File;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-direct {v14, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p2

    .line 81
    :goto_0
    :try_start_0
    invoke-static/range {p0 .. p0}, Lcom/sec/samsung/gallery/util/FileUtil;->setPowerWakeLock(Landroid/content/Context;)V

    .line 82
    new-instance v12, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v12, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    .end local v11    # "srcFile":Ljava/io/FileInputStream;
    .local v12, "srcFile":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v4, Ljava/io/FileOutputStream;

    move-object/from16 v0, p2

    invoke-direct {v4, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 85
    .end local v3    # "dstFile":Ljava/io/FileOutputStream;
    .local v4, "dstFile":Ljava/io/FileOutputStream;
    :try_start_2
    invoke-virtual {v12}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v13

    .line 86
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v5

    .line 88
    sget-object v14, Lcom/sec/samsung/gallery/util/FileUtil;->mTasks:Ljava/util/HashMap;

    invoke-virtual {v14}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v8

    .line 90
    .local v8, "keys":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/gallery3d/data/OnProgressListener;>;"
    const/16 v14, 0x2000

    invoke-static {v14}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 91
    .local v2, "buffer":Ljava/nio/ByteBuffer;
    const/4 v9, -0x1

    .line 92
    .local v9, "len":I
    :cond_0
    invoke-virtual {v13, v2}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v9

    const/4 v14, -0x1

    if-eq v9, v14, :cond_5

    .line 93
    const/16 v14, 0x2000

    if-ne v9, v14, :cond_2

    .line 94
    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 98
    :goto_1
    invoke-virtual {v5, v2}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 99
    const/16 v14, 0x2000

    if-ne v9, v14, :cond_3

    .line 100
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 104
    :goto_2
    invoke-static {}, Lcom/sec/samsung/gallery/util/FileUtil;->isTaskCancelled()Z

    move-result v14

    if-eqz v14, :cond_4

    .line 105
    new-instance v14, Ljava/io/IOException;

    const-string v15, "Task is canceled"

    invoke-direct {v14, v15}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v14
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 112
    .end local v2    # "buffer":Ljava/nio/ByteBuffer;
    .end local v8    # "keys":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/gallery3d/data/OnProgressListener;>;"
    .end local v9    # "len":I
    :catch_0
    move-exception v6

    move-object v3, v4

    .end local v4    # "dstFile":Ljava/io/FileOutputStream;
    .restart local v3    # "dstFile":Ljava/io/FileOutputStream;
    move-object v11, v12

    .line 113
    .end local v12    # "srcFile":Ljava/io/FileInputStream;
    .local v6, "e":Ljava/io/IOException;
    .restart local v11    # "srcFile":Ljava/io/FileInputStream;
    :goto_3
    :try_start_3
    invoke-static/range {p2 .. p2}, Lcom/sec/samsung/gallery/util/FileUtil;->deleteFile(Ljava/lang/String;)V

    .line 114
    throw v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 116
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v14

    :goto_4
    invoke-static {}, Lcom/sec/samsung/gallery/util/FileUtil;->releasePowerWakeLock()V

    .line 117
    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 118
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 119
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 120
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v14

    .line 78
    :cond_1
    invoke-static/range {p1 .. p2}, Lcom/sec/samsung/gallery/util/FileUtil;->getDstFilePath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 96
    .end local v3    # "dstFile":Ljava/io/FileOutputStream;
    .end local v11    # "srcFile":Ljava/io/FileInputStream;
    .restart local v2    # "buffer":Ljava/nio/ByteBuffer;
    .restart local v4    # "dstFile":Ljava/io/FileOutputStream;
    .restart local v8    # "keys":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/gallery3d/data/OnProgressListener;>;"
    .restart local v9    # "len":I
    .restart local v12    # "srcFile":Ljava/io/FileInputStream;
    :cond_2
    :try_start_4
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    goto :goto_1

    .line 116
    .end local v2    # "buffer":Ljava/nio/ByteBuffer;
    .end local v8    # "keys":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/gallery3d/data/OnProgressListener;>;"
    .end local v9    # "len":I
    :catchall_1
    move-exception v14

    move-object v3, v4

    .end local v4    # "dstFile":Ljava/io/FileOutputStream;
    .restart local v3    # "dstFile":Ljava/io/FileOutputStream;
    move-object v11, v12

    .end local v12    # "srcFile":Ljava/io/FileInputStream;
    .restart local v11    # "srcFile":Ljava/io/FileInputStream;
    goto :goto_4

    .line 102
    .end local v3    # "dstFile":Ljava/io/FileOutputStream;
    .end local v11    # "srcFile":Ljava/io/FileInputStream;
    .restart local v2    # "buffer":Ljava/nio/ByteBuffer;
    .restart local v4    # "dstFile":Ljava/io/FileOutputStream;
    .restart local v8    # "keys":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/gallery3d/data/OnProgressListener;>;"
    .restart local v9    # "len":I
    .restart local v12    # "srcFile":Ljava/io/FileInputStream;
    :cond_3
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_2

    .line 107
    :cond_4
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/gallery3d/data/OnProgressListener;

    .line 108
    .local v10, "listener":Lcom/sec/android/gallery3d/data/OnProgressListener;
    const/4 v14, -0x1

    invoke-interface {v10, v9, v14}, Lcom/sec/android/gallery3d/data/OnProgressListener;->onProgress(II)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_5

    .line 116
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v10    # "listener":Lcom/sec/android/gallery3d/data/OnProgressListener;
    :cond_5
    invoke-static {}, Lcom/sec/samsung/gallery/util/FileUtil;->releasePowerWakeLock()V

    .line 117
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 118
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 119
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 120
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 123
    return-object p2

    .line 116
    .end local v2    # "buffer":Ljava/nio/ByteBuffer;
    .end local v4    # "dstFile":Ljava/io/FileOutputStream;
    .end local v8    # "keys":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/gallery3d/data/OnProgressListener;>;"
    .end local v9    # "len":I
    .restart local v3    # "dstFile":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v14

    move-object v11, v12

    .end local v12    # "srcFile":Ljava/io/FileInputStream;
    .restart local v11    # "srcFile":Ljava/io/FileInputStream;
    goto :goto_4

    .line 112
    :catch_1
    move-exception v6

    goto :goto_3

    .end local v11    # "srcFile":Ljava/io/FileInputStream;
    .restart local v12    # "srcFile":Ljava/io/FileInputStream;
    :catch_2
    move-exception v6

    move-object v11, v12

    .end local v12    # "srcFile":Ljava/io/FileInputStream;
    .restart local v11    # "srcFile":Ljava/io/FileInputStream;
    goto :goto_3
.end method

.method public static deleteFile(Ljava/lang/String;)V
    .locals 2
    .param p0, "dstFilePath"    # Ljava/lang/String;

    .prologue
    .line 200
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 201
    .local v0, "file":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 202
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 204
    :cond_0
    return-void
.end method

.method public static getDstFilePath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "srcFilePath"    # Ljava/lang/String;
    .param p1, "dstAlbumPath"    # Ljava/lang/String;

    .prologue
    .line 207
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 208
    .local v0, "dstFolder":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 209
    .local v3, "srcFile":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 210
    .local v2, "newFile":Ljava/io/File;
    const/4 v1, 0x1

    .line 211
    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 212
    new-instance v2, Ljava/io/File;

    .end local v2    # "newFile":Ljava/io/File;
    invoke-static {v3, v1}, Lcom/sec/samsung/gallery/util/FileUtil;->addPostfix(Ljava/io/File;I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 213
    .restart local v2    # "newFile":Ljava/io/File;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 215
    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private static isPrivateTaskCancelled()Z
    .locals 3

    .prologue
    .line 338
    sget-object v2, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateTasks:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/util/SecretboxOperations;

    .line 339
    .local v1, "task":Lcom/sec/samsung/gallery/util/SecretboxOperations;
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/SecretboxOperations;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    .line 341
    .end local v1    # "task":Lcom/sec/samsung/gallery/util/SecretboxOperations;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static isTaskCancelled()Z
    .locals 3

    .prologue
    .line 331
    sget-object v2, Lcom/sec/samsung/gallery/util/FileUtil;->mTasks:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/util/MediaOperations;

    .line 332
    .local v1, "task":Lcom/sec/samsung/gallery/util/MediaOperations;
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/MediaOperations;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    .line 334
    .end local v1    # "task":Lcom/sec/samsung/gallery/util/MediaOperations;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static moveFileForPrivate(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 16
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "srcFilePath"    # Ljava/lang/String;
    .param p2, "dstFilePath"    # Ljava/lang/String;
    .param p3, "newFileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    const/4 v11, 0x0

    .line 129
    .local v11, "srcFile":Ljava/io/FileInputStream;
    const/4 v3, 0x0

    .line 130
    .local v3, "dstFile":Ljava/io/FileOutputStream;
    const/4 v13, 0x0

    .line 131
    .local v13, "srcFileChannel":Ljava/nio/channels/FileChannel;
    const/4 v5, 0x0

    .line 132
    .local v5, "dstFileChannel":Ljava/nio/channels/FileChannel;
    if-eqz p3, :cond_1

    .line 133
    new-instance v14, Ljava/io/File;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-direct {v14, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p2

    .line 138
    :goto_0
    :try_start_0
    invoke-static/range {p0 .. p0}, Lcom/sec/samsung/gallery/util/FileUtil;->setPowerWakeLock(Landroid/content/Context;)V

    .line 139
    new-instance v12, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v12, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    .end local v11    # "srcFile":Ljava/io/FileInputStream;
    .local v12, "srcFile":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v4, Ljava/io/FileOutputStream;

    move-object/from16 v0, p2

    invoke-direct {v4, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 142
    .end local v3    # "dstFile":Ljava/io/FileOutputStream;
    .local v4, "dstFile":Ljava/io/FileOutputStream;
    :try_start_2
    invoke-virtual {v12}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v13

    .line 143
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v5

    .line 145
    sget-object v14, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateTasks:Ljava/util/HashMap;

    invoke-virtual {v14}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v8

    .line 147
    .local v8, "keys":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;>;"
    const/16 v14, 0x2000

    invoke-static {v14}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 148
    .local v2, "buffer":Ljava/nio/ByteBuffer;
    const/4 v9, -0x1

    .line 149
    .local v9, "len":I
    :cond_0
    invoke-virtual {v13, v2}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v9

    const/4 v14, -0x1

    if-eq v9, v14, :cond_5

    .line 150
    const/16 v14, 0x2000

    if-ne v9, v14, :cond_2

    .line 151
    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 155
    :goto_1
    invoke-virtual {v5, v2}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 156
    const/16 v14, 0x2000

    if-ne v9, v14, :cond_3

    .line 157
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 161
    :goto_2
    invoke-static {}, Lcom/sec/samsung/gallery/util/FileUtil;->isPrivateTaskCancelled()Z

    move-result v14

    if-eqz v14, :cond_4

    .line 162
    new-instance v14, Ljava/io/IOException;

    const-string v15, "Task is canceled"

    invoke-direct {v14, v15}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v14
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 169
    .end local v2    # "buffer":Ljava/nio/ByteBuffer;
    .end local v8    # "keys":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;>;"
    .end local v9    # "len":I
    :catch_0
    move-exception v6

    move-object v3, v4

    .end local v4    # "dstFile":Ljava/io/FileOutputStream;
    .restart local v3    # "dstFile":Ljava/io/FileOutputStream;
    move-object v11, v12

    .line 170
    .end local v12    # "srcFile":Ljava/io/FileInputStream;
    .local v6, "e":Ljava/io/IOException;
    .restart local v11    # "srcFile":Ljava/io/FileInputStream;
    :goto_3
    :try_start_3
    invoke-static/range {p2 .. p2}, Lcom/sec/samsung/gallery/util/FileUtil;->deleteFile(Ljava/lang/String;)V

    .line 171
    throw v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 173
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v14

    :goto_4
    invoke-static {}, Lcom/sec/samsung/gallery/util/FileUtil;->releasePowerWakeLock()V

    .line 174
    invoke-static {v11}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 175
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 176
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 177
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v14

    .line 135
    :cond_1
    invoke-static/range {p1 .. p2}, Lcom/sec/samsung/gallery/util/FileUtil;->getDstFilePath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 153
    .end local v3    # "dstFile":Ljava/io/FileOutputStream;
    .end local v11    # "srcFile":Ljava/io/FileInputStream;
    .restart local v2    # "buffer":Ljava/nio/ByteBuffer;
    .restart local v4    # "dstFile":Ljava/io/FileOutputStream;
    .restart local v8    # "keys":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;>;"
    .restart local v9    # "len":I
    .restart local v12    # "srcFile":Ljava/io/FileInputStream;
    :cond_2
    :try_start_4
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    goto :goto_1

    .line 173
    .end local v2    # "buffer":Ljava/nio/ByteBuffer;
    .end local v8    # "keys":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;>;"
    .end local v9    # "len":I
    :catchall_1
    move-exception v14

    move-object v3, v4

    .end local v4    # "dstFile":Ljava/io/FileOutputStream;
    .restart local v3    # "dstFile":Ljava/io/FileOutputStream;
    move-object v11, v12

    .end local v12    # "srcFile":Ljava/io/FileInputStream;
    .restart local v11    # "srcFile":Ljava/io/FileInputStream;
    goto :goto_4

    .line 159
    .end local v3    # "dstFile":Ljava/io/FileOutputStream;
    .end local v11    # "srcFile":Ljava/io/FileInputStream;
    .restart local v2    # "buffer":Ljava/nio/ByteBuffer;
    .restart local v4    # "dstFile":Ljava/io/FileOutputStream;
    .restart local v8    # "keys":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;>;"
    .restart local v9    # "len":I
    .restart local v12    # "srcFile":Ljava/io/FileInputStream;
    :cond_3
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_2

    .line 164
    :cond_4
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;

    .line 165
    .local v10, "listener":Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;
    const/4 v14, -0x1

    invoke-interface {v10, v9, v14}, Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;->onProgress(II)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_5

    .line 173
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v10    # "listener":Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;
    :cond_5
    invoke-static {}, Lcom/sec/samsung/gallery/util/FileUtil;->releasePowerWakeLock()V

    .line 174
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 175
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 176
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 177
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 180
    return-object p2

    .line 173
    .end local v2    # "buffer":Ljava/nio/ByteBuffer;
    .end local v4    # "dstFile":Ljava/io/FileOutputStream;
    .end local v8    # "keys":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;>;"
    .end local v9    # "len":I
    .restart local v3    # "dstFile":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v14

    move-object v11, v12

    .end local v12    # "srcFile":Ljava/io/FileInputStream;
    .restart local v11    # "srcFile":Ljava/io/FileInputStream;
    goto :goto_4

    .line 169
    :catch_1
    move-exception v6

    goto :goto_3

    .end local v11    # "srcFile":Ljava/io/FileInputStream;
    .restart local v12    # "srcFile":Ljava/io/FileInputStream;
    :catch_2
    move-exception v6

    move-object v11, v12

    .end local v12    # "srcFile":Ljava/io/FileInputStream;
    .restart local v11    # "srcFile":Ljava/io/FileInputStream;
    goto :goto_3
.end method

.method private static releasePowerWakeLock()V
    .locals 1

    .prologue
    .line 193
    sget-object v0, Lcom/sec/samsung/gallery/util/FileUtil;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/samsung/gallery/util/FileUtil;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    sget-object v0, Lcom/sec/samsung/gallery/util/FileUtil;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 195
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/samsung/gallery/util/FileUtil;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 197
    :cond_0
    return-void
.end method

.method private static setPowerWakeLock(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 184
    const-string v1, "power"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 186
    .local v0, "pm":Landroid/os/PowerManager;
    const/4 v1, 0x1

    const-string v2, "Gallery-Copy"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    sput-object v1, Lcom/sec/samsung/gallery/util/FileUtil;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 188
    sget-object v1, Lcom/sec/samsung/gallery/util/FileUtil;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_0

    .line 189
    sget-object v1, Lcom/sec/samsung/gallery/util/FileUtil;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 190
    :cond_0
    return-void
.end method


# virtual methods
.method public addNewFileToDB(Ljava/lang/String;)V
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 240
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/FileUtil;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 242
    return-void
.end method

.method public cancelOperation()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 315
    sget-object v2, Lcom/sec/samsung/gallery/util/FileUtil;->mTasks:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/util/MediaOperations;

    .line 316
    .local v1, "task":Lcom/sec/samsung/gallery/util/MediaOperations;
    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/util/MediaOperations;->cancel(Z)Z

    goto :goto_0

    .line 319
    .end local v1    # "task":Lcom/sec/samsung/gallery/util/MediaOperations;
    :cond_0
    sget-object v2, Lcom/sec/samsung/gallery/util/FileUtil;->mTasksForMultiple:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;

    .line 320
    .local v1, "task":Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;
    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->cancel(Z)Z

    goto :goto_1

    .line 322
    .end local v1    # "task":Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;
    :cond_1
    return-void
.end method

.method public cancelOperationForPrivate()V
    .locals 3

    .prologue
    .line 325
    sget-object v2, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateTasks:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/util/SecretboxOperations;

    .line 326
    .local v1, "task":Lcom/sec/samsung/gallery/util/SecretboxOperations;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/util/SecretboxOperations;->cancel(Z)Z

    goto :goto_0

    .line 328
    .end local v1    # "task":Lcom/sec/samsung/gallery/util/SecretboxOperations;
    :cond_0
    return-void
.end method

.method public getSelectedMediaSize()Ljava/util/HashMap;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 348
    iget-object v13, p0, Lcom/sec/samsung/gallery/util/FileUtil;->mContext:Landroid/content/Context;

    check-cast v13, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v8

    .line 349
    .local v8, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalItemList()Ljava/util/LinkedList;

    move-result-object v13

    invoke-direct {v5, v13}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 350
    .local v5, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    .line 351
    .local v0, "count":I
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 354
    .local v2, "fileSize":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Long;>;"
    const/4 v9, 0x0

    .line 355
    .local v9, "sizeObj":Ljava/lang/Object;
    const-wide/16 v10, 0x0

    .line 356
    .local v10, "totalSize":J
    const-wide/16 v6, 0x0

    .line 357
    .local v6, "itemSize":J
    const/4 v3, 0x0

    .end local v9    # "sizeObj":Ljava/lang/Object;
    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_5

    .line 358
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 359
    .local v4, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v4, :cond_0

    .line 357
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 360
    :cond_0
    const-wide/16 v6, 0x0

    .line 361
    instance-of v13, v4, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v13, :cond_2

    move-object v13, v4

    .line 362
    check-cast v13, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getSize()J

    move-result-wide v6

    .line 370
    :cond_1
    :goto_2
    add-long/2addr v10, v6

    .line 372
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v12

    .line 373
    .local v12, "uri":Landroid/net/Uri;
    if-eqz v12, :cond_4

    .line 374
    invoke-virtual {v12}, Landroid/net/Uri;->hashCode()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 364
    .end local v12    # "uri":Landroid/net/Uri;
    :cond_2
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaObject;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v1

    .line 365
    .local v1, "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    if-eqz v1, :cond_3

    const/16 v13, 0xe

    invoke-virtual {v1, v13}, Lcom/sec/android/gallery3d/data/MediaDetails;->getDetail(I)Ljava/lang/Object;

    move-result-object v9

    .line 366
    .restart local v9    # "sizeObj":Ljava/lang/Object;
    :goto_3
    if-eqz v9, :cond_1

    .line 367
    check-cast v9, Ljava/lang/Long;

    .end local v9    # "sizeObj":Ljava/lang/Object;
    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    goto :goto_2

    .line 365
    :cond_3
    const/4 v9, 0x0

    goto :goto_3

    .line 376
    .end local v1    # "details":Lcom/sec/android/gallery3d/data/MediaDetails;
    .restart local v12    # "uri":Landroid/net/Uri;
    :cond_4
    sget-object v13, Lcom/sec/samsung/gallery/util/FileUtil;->TAG:Ljava/lang/String;

    const-string v14, "getSelectedMediaSize : uri is null"

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 379
    .end local v4    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v12    # "uri":Landroid/net/Uri;
    :cond_5
    sget-object v13, Lcom/sec/samsung/gallery/util/FileUtil;->TOTAL_FILE_SIZE_KEY:Ljava/lang/Integer;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 380
    return-object v2
.end method

.method public operateMediaList(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/OnProgressListener;)Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;
    .locals 4
    .param p1, "parentMediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "onProgressListener"    # Lcom/sec/android/gallery3d/data/OnProgressListener;

    .prologue
    .line 280
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/FileUtil;->cancelOperation()V

    .line 281
    new-instance v1, Lcom/sec/samsung/gallery/util/FileUtil$2;

    invoke-direct {v1, p0, p2}, Lcom/sec/samsung/gallery/util/FileUtil$2;-><init>(Lcom/sec/samsung/gallery/util/FileUtil;Lcom/sec/android/gallery3d/data/OnProgressListener;)V

    .line 307
    .local v1, "progressListener":Lcom/sec/android/gallery3d/data/OnProgressListener;
    iget-object v2, p0, Lcom/sec/samsung/gallery/util/FileUtil;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/StateManager;->setReloadRequiredOnResume(Z)V

    .line 308
    new-instance v0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;

    iget-object v2, p0, Lcom/sec/samsung/gallery/util/FileUtil;->mContext:Landroid/content/Context;

    invoke-direct {v0, p1, v1, v2}, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/OnProgressListener;Landroid/content/Context;)V

    .line 309
    .local v0, "mOperateMediaAsyncTask":Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;
    sget-object v2, Lcom/sec/samsung/gallery/util/FileUtil;->mTasksForMultiple:Ljava/util/HashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 311
    return-object v0
.end method

.method public operateMedias(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/OnProgressListener;)Lcom/sec/samsung/gallery/util/MediaOperations;
    .locals 4
    .param p1, "parentMediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "onProgressListener"    # Lcom/sec/android/gallery3d/data/OnProgressListener;

    .prologue
    .line 245
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/FileUtil;->cancelOperation()V

    .line 246
    new-instance v1, Lcom/sec/samsung/gallery/util/FileUtil$1;

    invoke-direct {v1, p0, p2}, Lcom/sec/samsung/gallery/util/FileUtil$1;-><init>(Lcom/sec/samsung/gallery/util/FileUtil;Lcom/sec/android/gallery3d/data/OnProgressListener;)V

    .line 272
    .local v1, "progressListener":Lcom/sec/android/gallery3d/data/OnProgressListener;
    iget-object v2, p0, Lcom/sec/samsung/gallery/util/FileUtil;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/StateManager;->setReloadRequiredOnResume(Z)V

    .line 273
    new-instance v0, Lcom/sec/samsung/gallery/util/MediaOperations;

    iget-object v2, p0, Lcom/sec/samsung/gallery/util/FileUtil;->mContext:Landroid/content/Context;

    invoke-direct {v0, p1, v1, v2}, Lcom/sec/samsung/gallery/util/MediaOperations;-><init>(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/OnProgressListener;Landroid/content/Context;)V

    .line 274
    .local v0, "mOperateMediaAsyncTask":Lcom/sec/samsung/gallery/util/MediaOperations;
    sget-object v2, Lcom/sec/samsung/gallery/util/FileUtil;->mTasks:Ljava/util/HashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/util/MediaOperations;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 276
    return-object v0
.end method

.method public rename(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "srcPath"    # Ljava/lang/String;
    .param p2, "dstPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 56
    new-instance v2, Lcom/sec/samsung/gallery/util/DestAlreadyExistException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "File name is same : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/util/DestAlreadyExistException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 59
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 60
    .local v1, "srcFile":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 61
    .local v0, "dstFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 62
    new-instance v2, Lcom/sec/samsung/gallery/util/DestAlreadyExistException;

    const-string v3, "A target already exist : "

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/util/DestAlreadyExistException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 64
    :cond_1
    invoke-virtual {v1, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 65
    new-instance v2, Ljava/io/IOException;

    const-string v3, "File.renameTo() returns false"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 67
    :cond_2
    return-void
.end method
