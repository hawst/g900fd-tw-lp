.class public Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder;
.super Ljava/lang/Object;
.source "MusicItemBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;
    }
.end annotation


# static fields
.field private static final DEFAULT_MUSIC_RESOURCE_IDS:[I

.field private static final PREFIX_RESOURCE_URI:Ljava/lang/String; = "android.resource://com.sec.android.gallery3d/"

.field private static final TYPE_ADD_MUSIC:I = 0x2

.field private static final TYPE_MUSIC:I = 0x1

.field private static final TYPE_NONE:I

.field private static final mComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;",
            ">;"
        }
    .end annotation
.end field

.field private static sMusicTitleDbIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder;->DEFAULT_MUSIC_RESOURCE_IDS:[I

    .line 40
    const/4 v0, -0x1

    sput v0, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder;->sMusicTitleDbIndex:I

    .line 107
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$1;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$1;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder;->mComparator:Ljava/util/Comparator;

    return-void

    .line 30
    nop

    :array_0
    .array-data 4
        0x7f080003
        0x7f080004
        0x7f080005
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    return-void
.end method

.method public static createAddSlideshowMusicItem(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 118
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;

    const/4 v1, 0x2

    const v2, 0x7f0e0175

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;-><init>(ILjava/lang/String;Landroid/net/Uri;Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$1;)V

    return-object v0
.end method

.method public static createAddedMusicItems(Landroid/content/Context;)Ljava/util/Collection;
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 63
    invoke-static {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->load(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v12

    .line 64
    .local v12, "setting":Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    invoke-virtual {v12}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getMusicsAdded()Ljava/util/Set;

    move-result-object v10

    .line 65
    .local v10, "musicsAdded":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 67
    .local v11, "musicsItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 68
    .local v0, "resolver":Landroid/content/ContentResolver;
    const/4 v7, 0x0

    .line 69
    .local v7, "isItemRemoved":Z
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 70
    .local v8, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 71
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 72
    .local v14, "uriString":Ljava/lang/String;
    invoke-static {v14}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 74
    .local v1, "uri":Landroid/net/Uri;
    const/4 v13, 0x0

    .local v13, "title":Ljava/lang/String;
    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 75
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 77
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-nez v3, :cond_1

    .line 79
    :cond_0
    const/4 v7, 0x1

    .line 80
    invoke-interface {v8}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 83
    :cond_1
    :try_start_1
    sget v3, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder;->sMusicTitleDbIndex:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_2

    .line 84
    const-string/jumbo v3, "title"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    sput v3, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder;->sMusicTitleDbIndex:I

    .line 87
    :cond_2
    sget v3, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder;->sMusicTitleDbIndex:I

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v13

    .line 89
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 92
    new-instance v9, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;

    const/4 v3, 0x1

    invoke-direct {v9, v3, v13, v1, v2}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;-><init>(ILjava/lang/String;Landroid/net/Uri;Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$1;)V

    .line 93
    .local v9, "item":Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;
    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 89
    .end local v9    # "item":Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;
    :catchall_0
    move-exception v2

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2

    .line 97
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v13    # "title":Ljava/lang/String;
    .end local v14    # "uriString":Ljava/lang/String;
    :cond_3
    invoke-virtual {v11}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 98
    sget-object v2, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder;->mComparator:Ljava/util/Comparator;

    invoke-static {v11, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 100
    :cond_4
    if-eqz v7, :cond_5

    .line 101
    invoke-static {p0, v12}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->save(Landroid/content/Context;Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;)V

    .line 104
    :cond_5
    return-object v11
.end method

.method public static createDefaultMusicItems(Landroid/content/Context;)Ljava/util/Collection;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 50
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 52
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v4, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder;->DEFAULT_MUSIC_RESOURCE_IDS:[I

    array-length v4, v4

    if-ge v0, v4, :cond_0

    .line 53
    const v4, 0x7f0e0281

    new-array v5, v8, [Ljava/lang/Object;

    const/4 v6, 0x0

    add-int/lit8 v7, v0, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 54
    .local v2, "title":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "android.resource://com.sec.android.gallery3d/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder;->DEFAULT_MUSIC_RESOURCE_IDS:[I

    aget v5, v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 55
    .local v3, "uri":Landroid/net/Uri;
    new-instance v4, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;

    const/4 v5, 0x0

    invoke-direct {v4, v8, v2, v3, v5}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;-><init>(ILjava/lang/String;Landroid/net/Uri;Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$1;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 58
    .end local v2    # "title":Ljava/lang/String;
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_0
    return-object v1
.end method

.method public static createNoneItem(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;

    const/4 v1, 0x0

    const v2, 0x7f0e0174

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;-><init>(ILjava/lang/String;Landroid/net/Uri;Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$1;)V

    return-object v0
.end method
