.class Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3$2;
.super Ljava/lang/Object;
.source "SlideshowVideoView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;->onPrepared(Landroid/media/MediaPlayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3$2;->this$1:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3$2;->this$1:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3$2;->this$1:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3$2;->this$1:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # invokes: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->abandonAudioFocus()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$500(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)V

    .line 186
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3$2;->this$1:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$400(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 187
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3$2;->this$1:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$400(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->startSlideShowPauseActivity()V

    .line 188
    :cond_1
    const/4 v0, 0x0

    return v0
.end method
