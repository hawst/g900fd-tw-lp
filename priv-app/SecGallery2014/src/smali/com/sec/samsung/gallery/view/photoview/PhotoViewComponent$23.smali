.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$23;
.super Ljava/lang/Object;
.source "PhotoViewComponent.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V
    .locals 0

    .prologue
    .line 2869
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$23;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGenericMotionCancel(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 3
    .param p1, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    const/4 v2, -0x1

    .line 2890
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$23;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getGenericMotionFocus()I

    move-result v0

    .line 2891
    .local v0, "oldFocusedIndex":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$23;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->updateBorder(II)V

    .line 2892
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$23;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setGenericMotionFocus(I)V

    .line 2893
    return-void
.end method

.method public onGenericMotionEnter(Lcom/sec/android/gallery3d/glcore/GlLayer;I)V
    .locals 5
    .param p1, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "position"    # I

    .prologue
    const/4 v3, -0x1

    .line 2872
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$23;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getGenericMotionFocus()I

    move-result v0

    .line 2874
    .local v0, "oldFocusedIndex":I
    if-ne v0, p2, :cond_1

    .line 2886
    :cond_0
    :goto_0
    return-void

    .line 2878
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$23;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    move-result-object v1

    invoke-virtual {v1, v0, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->updateBorder(II)V

    .line 2880
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$23;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2881
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$23;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$23;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$100(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getGenericMotionFocus()I

    move-result v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->updateBorder(II)V

    .line 2882
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$23;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$100(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->setGenericMotionFocus(I)V

    .line 2883
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$23;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setGenericMotionFocus(I)V

    .line 2884
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$23;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    move-result-object v2

    const v3, 0xffff

    and-int/2addr v3, p2

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$23;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(Ljava/lang/Object;ILcom/sec/android/gallery3d/ui/SelectionManager;)V

    goto :goto_0
.end method
