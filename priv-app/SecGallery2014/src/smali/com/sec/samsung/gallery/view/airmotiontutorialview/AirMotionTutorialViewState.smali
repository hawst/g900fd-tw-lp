.class public Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;
.super Lcom/sec/android/gallery3d/app/ActivityState;
.source "AirMotionTutorialViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;


# instance fields
.field private mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

.field private mAirMotionSettingsReceiver:Landroid/content/BroadcastReceiver;

.field private mBubbleAnimation:Landroid/view/animation/Animation;

.field private mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mParentView:Landroid/widget/RelativeLayout;

.field private mPopupView:Landroid/view/View;

.field protected mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mView:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ActivityState;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    .line 44
    new-instance v0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState$1;-><init>(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    .line 64
    new-instance v0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState$2;-><init>(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mAirMotionSettingsReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mPopupView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mView:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mBubbleAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;Landroid/view/animation/Animation;)Landroid/view/animation/Animation;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;
    .param p1, "x1"    # Landroid/view/animation/Animation;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mBubbleAnimation:Landroid/view/animation/Animation;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 170
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 182
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 183
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mView:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->onConfigChanged(Landroid/content/res/Configuration;)V

    .line 184
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->invalidate()V

    .line 185
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 9
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    const-wide/16 v2, 0x12c

    const/4 v4, -0x1

    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v7

    .line 80
    .local v7, "win":Landroid/view/Window;
    invoke-virtual {v7}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v8

    .line 81
    .local v8, "winParams":Landroid/view/WindowManager$LayoutParams;
    iget v0, v8, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v8, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    .line 82
    invoke-virtual {v7, v8}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 84
    new-instance v0, Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1, p0}, Lcom/sec/samsung/gallery/view/ActionBarManager;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/app/ActivityState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 85
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mView:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;

    .line 86
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0f00bf

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 87
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mView:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->attachLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;)V

    .line 88
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v6, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 89
    .local v6, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v0, 0xd

    invoke-virtual {v6, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 91
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0f0175

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mParentView:Landroid/widget/RelativeLayout;

    .line 92
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mInflater:Landroid/view/LayoutInflater;

    .line 93
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f03001c

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mPopupView:Landroid/view/View;

    .line 95
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mPopupView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mPopupView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 97
    new-instance v0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState$3;

    move-object v1, p0

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState$3;-><init>(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;JJ)V

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState$3;->start()Landroid/os/CountDownTimer;

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mParentView:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mPopupView:Landroid/view/View;

    invoke-virtual {v0, v1, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 111
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setVisibility(I)V

    .line 113
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAirBrowse:Z

    if-eqz v0, :cond_1

    .line 114
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mAirMotionSettingsReceiver:Landroid/content/BroadcastReceiver;

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;-><init>(Landroid/content/Context;Landroid/content/BroadcastReceiver;I)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    .line 119
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 120
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->hide()V

    .line 121
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 150
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->destoryAirMotionDetector()V

    .line 146
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 158
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_1

    .line 159
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 160
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 161
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 162
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0f0284

    if-ne v0, v1, :cond_0

    .line 163
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->goToHome(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mView:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->pause()V

    .line 139
    return-void
.end method

.method public onPrepareOptionMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 154
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mView:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->resume()V

    .line 130
    return-void
.end method

.method public onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 0
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    .line 178
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 0
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 174
    return-void
.end method
