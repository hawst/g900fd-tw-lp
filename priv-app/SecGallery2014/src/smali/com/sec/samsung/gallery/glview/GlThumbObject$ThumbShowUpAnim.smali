.class public Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlThumbObject.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlThumbObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ThumbShowUpAnim"
.end annotation


# instance fields
.field private mFromX:F

.field private mFromZ:F

.field private mPosIndex:I

.field private mRad:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V
    .locals 1

    .prologue
    .line 166
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 171
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->mPosIndex:I

    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 10
    .param p1, "ratio"    # F

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 192
    sub-float v0, v3, p1

    neg-float v0, v0

    const/high16 v1, 0x42b40000    # 90.0f

    mul-float v6, v0, v1

    .line 193
    .local v6, "degree":F
    sub-float v0, v3, p1

    const v1, 0x40490fd8

    mul-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float v7, v0, v1

    .line 194
    .local v7, "theta":F
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->mFromX:F

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    double-to-float v1, v8

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->mRad:F

    mul-float/2addr v1, v3

    add-float v2, v0, v1

    .line 195
    .local v2, "dx":F
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->mFromZ:F

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    double-to-float v1, v8

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->mRad:F

    mul-float/2addr v1, v3

    add-float v4, v0, v1

    .line 197
    .local v4, "dz":F
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->mPosIndex:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPosIndex()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->mPosIndex:I

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->mPosIndex:I

    const/4 v3, 0x0

    const/4 v5, 0x5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setPos(IFFFI)V

    .line 199
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v0, v6}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setYaw(F)V

    .line 200
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setAlpha(F)V

    .line 201
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->mPosIndex:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->releasePosIndex(I)V

    .line 205
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->mPosIndex:I

    .line 206
    return-void
.end method

.method public setTransition()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 179
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mWidth:F

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v1, v1, Lcom/sec/android/gallery3d/glcore/GlObject;->mScaleX:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->mRad:F

    .line 180
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->mRad:F

    neg-float v0, v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->mFromX:F

    .line 181
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->mFromZ:F

    .line 182
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setVisibility(Z)V

    .line 183
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setAlpha(F)V

    .line 184
    return-void
.end method

.method public start()V
    .locals 0

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlThumbObject$ThumbShowUpAnim;->setTransition()V

    .line 175
    invoke-super {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->start()V

    .line 176
    return-void
.end method
