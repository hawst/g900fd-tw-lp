.class Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick$3;
.super Ljava/lang/Object;
.source "PhotoActionBarForPick.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->setTitle(Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;

.field final synthetic val$albumName:Ljava/lang/String;

.field final synthetic val$numberOfItemsInAlbum:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick$3;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick$3;->val$albumName:Ljava/lang/String;

    iput p3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick$3;->val$numberOfItemsInAlbum:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 116
    :try_start_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick$3;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->access$700(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 117
    .local v2, "res":Landroid/content/res/Resources;
    const v3, 0x7f0e01bf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick$3;->val$albumName:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick$3;->val$numberOfItemsInAlbum:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 118
    .local v0, "albumLabelCountText":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick$3;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;

    invoke-virtual {v3, v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->setTitle(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    .end local v0    # "albumLabelCountText":Ljava/lang/String;
    .end local v2    # "res":Landroid/content/res/Resources;
    :goto_0
    return-void

    .line 120
    :catch_0
    move-exception v1

    .line 121
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
