.class Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;
.super Ljava/lang/Object;
.source "AbstractActionBarViewForSelection.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

.field final synthetic val$button:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

.field final synthetic val$toggleImage:Z

.field final synthetic val$visible:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;ILcom/sec/samsung/gallery/util/Consts$ButtonType;Z)V
    .locals 0

    .prologue
    .line 541
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iput p2, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->val$visible:I

    iput-object p3, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->val$button:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    iput-boolean p4, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->val$toggleImage:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 544
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mUsingSelectionModeBar:Z

    if-eqz v0, :cond_3

    .line 545
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    if-nez v0, :cond_1

    .line 546
    # getter for: Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->access$600()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mSelectionModeBar is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 584
    :cond_0
    :goto_0
    return-void

    .line 550
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->val$visible:I

    if-ne v0, v5, :cond_2

    .line 551
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->setChecked(Z)V

    .line 552
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->setEnabled(Z)V

    goto :goto_0

    .line 554
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->setEnabled(Z)V

    .line 555
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->val$button:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    sget-object v1, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    if-ne v0, v1, :cond_0

    .line 556
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->val$toggleImage:Z

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->setChecked(Z)V

    goto :goto_0

    .line 559
    :cond_3
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionOnActionBar:Z

    if-eqz v0, :cond_6

    .line 560
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    # getter for: Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeOnActionBarLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->access$700(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-nez v0, :cond_4

    .line 561
    # getter for: Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->access$600()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mSelectionModeOnActionBarLayout is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 564
    :cond_4
    iget v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->val$visible:I

    if-ne v0, v5, :cond_5

    .line 565
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->setCheckboxState(Z)V

    .line 566
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->setEnableSelectionOnActionBar(Z)V

    goto :goto_0

    .line 568
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->setEnableSelectionOnActionBar(Z)V

    .line 569
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->val$button:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    sget-object v1, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    if-ne v0, v1, :cond_0

    .line 570
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->val$toggleImage:Z

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->setCheckboxState(Z)V

    goto :goto_0

    .line 574
    :cond_6
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->val$toggleImage:Z

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    # getter for: Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mNumberOfItemsSelected:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->access$500(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->updatePopupMenuItemsVisibility(ZI)V

    .line 575
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0f0018

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    # setter for: Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->access$202(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 576
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    # getter for: Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->access$200(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 577
    iget v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->val$visible:I

    if-ne v0, v5, :cond_7

    .line 578
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    # getter for: Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->access$200(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto/16 :goto_0

    .line 580
    :cond_7
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    # getter for: Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->access$200(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    goto/16 :goto_0
.end method
