.class Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1$1;
.super Ljava/lang/Object;
.source "SlideshowPresentationVideoView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;->onPrepared(Landroid/media/MediaPlayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1$1;->this$1:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 97
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1$1;->this$1:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$600(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1$1;->this$1:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$600(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getInstance()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->getGlSlideShowView()Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 99
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1$1;->this$1:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$600(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1$1;->this$1:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$600(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mCurrentIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mImageIndex:I

    .line 100
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1$1;->this$1:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$600(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    if-eqz v1, :cond_1

    .line 101
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1$1;->this$1:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$600(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mEffect:Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->isBitmapdecoded()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 108
    :cond_1
    :goto_0
    return-void

    .line 105
    :catch_0
    move-exception v0

    .line 106
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
