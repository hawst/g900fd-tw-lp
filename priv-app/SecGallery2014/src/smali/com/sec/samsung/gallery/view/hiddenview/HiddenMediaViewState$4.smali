.class Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$4;
.super Landroid/content/BroadcastReceiver;
.source "HiddenMediaViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$4;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 171
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$4;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$1000(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$4;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$800(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->hasWindowFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    const-string v1, "isEnable"

    const/4 v2, 0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 173
    .local v0, "isEnable":Z
    if-eqz v0, :cond_1

    .line 174
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$4;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$1000(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$4;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$1100(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    .line 178
    .end local v0    # "isEnable":Z
    :cond_0
    :goto_0
    return-void

    .line 176
    .restart local v0    # "isEnable":Z
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$4;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$1000(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    goto :goto_0
.end method
