.class public Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "SoundShotEditorCmd.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$1;,
        Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SoundShotEditorCmd"


# instance fields
.field private eventType:I

.field private mContext:Landroid/content/Context;

.field private mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

.field private mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mRemoveDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 55
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;)Lcom/sec/samsung/gallery/core/GalleryFacade;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    return-object v0
.end method

.method private dismissdialog()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->mRemoveDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->mRemoveDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->dismissDialog()V

    .line 134
    :cond_0
    return-void
.end method

.method private showDialog(Landroid/content/Context;ILjava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "title"    # I
    .param p3, "popupText"    # Ljava/lang/String;

    .prologue
    .line 124
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->dismissdialog()V

    .line 125
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    iget v2, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->eventType:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    .line 126
    .local v0, "event":Lcom/sec/samsung/gallery/core/Event;
    new-instance v1, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    invoke-direct {v1, p1, p2, p3, v0}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;-><init>(Landroid/content/Context;ILjava/lang/String;Lcom/sec/samsung/gallery/core/Event;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->mRemoveDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    .line 127
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->mRemoveDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    invoke-virtual {v1, p0}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->addObserver(Ljava/util/Observer;)V

    .line 128
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->mRemoveDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->showDialog()V

    .line 129
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 5
    .param p1, "notification"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 40
    const-string v1, "SoundShotEditorCmd"

    const-string v2, "Start SoundShot Editor"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    .line 43
    .local v0, "params":[Ljava/lang/Object;
    const/4 v1, 0x0

    aget-object v1, v0, v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->mContext:Landroid/content/Context;

    .line 44
    const/4 v1, 0x1

    aget-object v1, v0, v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 45
    const/4 v1, 0x2

    aget-object v1, v0, v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->eventType:I

    .line 46
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    .line 48
    iget v1, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->eventType:I

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELETE_SOUND_IN_PICTURE:I

    if-ne v1, v2, :cond_0

    .line 49
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->mContext:Landroid/content/Context;

    const v2, 0x7f0e04a3

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->mContext:Landroid/content/Context;

    const v4, 0x7f0e04a4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->showDialog(Landroid/content/Context;ILjava/lang/String;)V

    .line 52
    :cond_0
    invoke-super {p0, p1}, Lorg/puremvc/java/patterns/command/SimpleCommand;->execute(Lorg/puremvc/java/interfaces/INotification;)V

    .line 53
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 5
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 138
    move-object v0, p2

    check-cast v0, Lcom/sec/samsung/gallery/core/Event;

    .line 139
    .local v0, "ev":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v1

    iget v2, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->eventType:I

    if-ne v1, v2, :cond_0

    .line 140
    new-instance v1, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;-><init>(Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$1;)V

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/android/gallery3d/data/MediaItem;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 142
    :cond_0
    return-void
.end method
