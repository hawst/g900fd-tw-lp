.class public Lcom/sec/samsung/gallery/view/common/DragAndDrop;
.super Ljava/lang/Object;
.source "DragAndDrop.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/common/DragAndDrop$LazyLoadedBitmap;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "DragAndDrop"


# instance fields
.field private bitmap:Landroid/graphics/Bitmap;

.field private mContext:Landroid/content/Context;

.field private mIcon:Lcom/sec/samsung/gallery/view/common/DragAndDrop$LazyLoadedBitmap;

.field private rotationDegree:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->bitmap:Landroid/graphics/Bitmap;

    .line 54
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->rotationDegree:I

    .line 58
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    .line 59
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/common/DragAndDrop;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/DragAndDrop;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private getBitmap(Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/ui/PhotoView$Model;Lcom/sec/android/gallery3d/ui/PhotoView;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "model"    # Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    .param p3, "layer"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 432
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getFilePathFrom(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 433
    .local v1, "mFilePath":Ljava/lang/String;
    invoke-virtual {p3}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v2

    invoke-virtual {p3}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v3

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;->getSafeResizingBitmap(Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 434
    .local v0, "bmp":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    invoke-interface {p2}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getScreenNail()Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    if-eqz v2, :cond_0

    .line 435
    invoke-interface {p2}, Lcom/sec/android/gallery3d/ui/PhotoView$Model;->getScreenNail()Lcom/sec/android/gallery3d/ui/ScreenNail;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/BitmapScreenNail;->getTexture()Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glrenderer/BitmapTexture;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 437
    :cond_0
    return-object v0
.end method

.method private getData(IILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;Lcom/sec/android/gallery3d/ui/SelectionManager;)Landroid/content/ClipData;
    .locals 8
    .param p1, "albumPosition"    # I
    .param p2, "itemPosition"    # I
    .param p3, "adapter"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;
    .param p4, "selectionProxy"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    const/4 v0, 0x0

    .line 408
    if-nez p3, :cond_1

    .line 428
    :cond_0
    return-object v0

    .line 411
    :cond_1
    invoke-virtual {p3, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .line 412
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v2, :cond_0

    .line 415
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    .line 416
    .local v5, "selItemUri":Landroid/net/Uri;
    const/4 v0, 0x0

    .line 417
    .local v0, "dragData":Landroid/content/ClipData;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isConnetedSideSync(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 418
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "selectedUri"

    invoke-static {v6, v7, v5}, Landroid/content/ClipData;->newUri(Landroid/content/ContentResolver;Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    move-result-object v0

    .line 421
    :goto_0
    const/4 v3, 0x0

    .line 422
    .local v3, "mItemUri":Landroid/net/Uri;
    invoke-virtual {p4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 423
    .local v4, "mObject":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    .line 424
    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v3, v5}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 425
    new-instance v6, Landroid/content/ClipData$Item;

    invoke-direct {v6, v3}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v0, v6}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V

    goto :goto_1

    .line 420
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "mItemUri":Landroid/net/Uri;
    .end local v4    # "mObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_3
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "galleryUri"

    invoke-static {v6, v7, v5}, Landroid/content/ClipData;->newUri(Landroid/content/ContentResolver;Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    move-result-object v0

    goto :goto_0
.end method

.method private getData(ILcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/ui/SelectionManager;)Landroid/content/ClipData;
    .locals 10
    .param p1, "position"    # I
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p3, "selectionProxy"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    .line 369
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v7

    .line 370
    .local v7, "selItemUri":Landroid/net/Uri;
    const/4 v0, 0x0

    .line 371
    .local v0, "dragData":Landroid/content/ClipData;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isConnetedSideSync(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 372
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "selectedUri"

    invoke-static {v8, v9, v7}, Landroid/content/ClipData;->newUri(Landroid/content/ContentResolver;Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    move-result-object v0

    .line 375
    :goto_0
    const/4 v4, 0x0

    .line 376
    .local v4, "mItemUri":Landroid/net/Uri;
    invoke-virtual {p3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaObject;

    .local v5, "mObject":Lcom/sec/android/gallery3d/data/MediaObject;
    move-object v6, v5

    .line 377
    check-cast v6, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 378
    .local v6, "mSet":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v8, 0x0

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v9

    invoke-virtual {v6, v8, v9}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 379
    .local v3, "mItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    .line 380
    if-eqz v4, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v4, v7}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 381
    new-instance v8, Landroid/content/ClipData$Item;

    invoke-direct {v8, v4}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v0, v8}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V

    goto :goto_1

    .line 374
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "mItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v4    # "mItemUri":Landroid/net/Uri;
    .end local v5    # "mObject":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v6    # "mSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_2
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "galleryUri"

    invoke-static {v8, v9, v7}, Landroid/content/ClipData;->newUri(Landroid/content/ContentResolver;Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    move-result-object v0

    goto :goto_0

    .line 385
    .restart local v4    # "mItemUri":Landroid/net/Uri;
    :cond_3
    return-object v0
.end method

.method private getData(ILcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;Lcom/sec/android/gallery3d/ui/SelectionManager;)Landroid/content/ClipData;
    .locals 7
    .param p1, "position"    # I
    .param p2, "adapter"    # Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;
    .param p3, "selectionProxy"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    .line 389
    if-eqz p2, :cond_0

    invoke-virtual {p2, p1}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    if-nez v5, :cond_2

    .line 390
    :cond_0
    const/4 v0, 0x0

    .line 404
    :cond_1
    return-object v0

    .line 391
    :cond_2
    invoke-virtual {p2, p1}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    .line 392
    .local v4, "selItemUri":Landroid/net/Uri;
    const/4 v0, 0x0

    .line 393
    .local v0, "dragData":Landroid/content/ClipData;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isConnetedSideSync(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 394
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "selectedUri"

    invoke-static {v5, v6, v4}, Landroid/content/ClipData;->newUri(Landroid/content/ContentResolver;Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    move-result-object v0

    .line 397
    :goto_0
    const/4 v2, 0x0

    .line 398
    .local v2, "mItemUri":Landroid/net/Uri;
    invoke-virtual {p3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 399
    .local v3, "mObject":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    .line 400
    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    invoke-virtual {v2, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 401
    new-instance v5, Landroid/content/ClipData$Item;

    invoke-direct {v5, v2}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v0, v5}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V

    goto :goto_1

    .line 396
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "mItemUri":Landroid/net/Uri;
    .end local v3    # "mObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_4
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "galleryUri"

    invoke-static {v5, v6, v4}, Landroid/content/ClipData;->newUri(Landroid/content/ContentResolver;Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    move-result-object v0

    goto :goto_0
.end method

.method private getData(ILcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;Lcom/sec/android/gallery3d/ui/SelectionManager;)Landroid/content/ClipData;
    .locals 7
    .param p1, "position"    # I
    .param p2, "adapter"    # Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;
    .param p3, "selectionProxy"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    .line 350
    if-eqz p2, :cond_0

    invoke-virtual {p2, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    if-nez v5, :cond_2

    .line 351
    :cond_0
    const/4 v0, 0x0

    .line 365
    :cond_1
    return-object v0

    .line 352
    :cond_2
    invoke-virtual {p2, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    .line 353
    .local v4, "selItemUri":Landroid/net/Uri;
    const/4 v0, 0x0

    .line 354
    .local v0, "dragData":Landroid/content/ClipData;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isConnetedSideSync(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 355
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "selectedUri"

    invoke-static {v5, v6, v4}, Landroid/content/ClipData;->newUri(Landroid/content/ContentResolver;Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    move-result-object v0

    .line 358
    :goto_0
    const/4 v2, 0x0

    .line 359
    .local v2, "mItemUri":Landroid/net/Uri;
    invoke-virtual {p3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 360
    .local v3, "mObject":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    .line 361
    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    invoke-virtual {v2, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 362
    new-instance v5, Landroid/content/ClipData$Item;

    invoke-direct {v5, v2}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v0, v5}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V

    goto :goto_1

    .line 357
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "mItemUri":Landroid/net/Uri;
    .end local v3    # "mObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_4
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "galleryUri"

    invoke-static {v5, v6, v4}, Landroid/content/ClipData;->newUri(Landroid/content/ContentResolver;Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    move-result-object v0

    goto :goto_0
.end method

.method private getImageView()Landroid/widget/ImageView;
    .locals 10

    .prologue
    const v9, 0x7f0f00df

    .line 312
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 313
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {v0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 315
    .local v4, "imageView":Landroid/widget/ImageView;
    if-nez v4, :cond_0

    .line 316
    const v6, 0x7f0f0175

    invoke-virtual {v0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    .line 317
    .local v5, "mainView":Landroid/view/ViewGroup;
    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f030052

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 319
    .local v2, "dragView":Landroid/view/View;
    new-instance v1, Lcom/sec/samsung/gallery/view/common/DragAndDrop$1;

    invoke-direct {v1, p0, v5, v2}, Lcom/sec/samsung/gallery/view/common/DragAndDrop$1;-><init>(Lcom/sec/samsung/gallery/view/common/DragAndDrop;Landroid/view/ViewGroup;Landroid/view/View;)V

    .line 329
    .local v1, "addViewOnUiThread":Ljava/lang/Runnable;
    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 331
    invoke-virtual {v0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .end local v4    # "imageView":Landroid/widget/ImageView;
    check-cast v4, Landroid/widget/ImageView;

    .line 332
    .restart local v4    # "imageView":Landroid/widget/ImageView;
    if-nez v4, :cond_0

    .line 334
    :try_start_0
    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 335
    :try_start_1
    const-string v6, "DragAndDrop"

    const-string v7, "addViewOnUiThread wait"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    invoke-virtual {v1}, Ljava/lang/Object;->wait()V

    .line 337
    const-string v6, "DragAndDrop"

    const-string v7, "addViewOnUiThread resume"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 343
    :goto_0
    invoke-virtual {v0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .end local v4    # "imageView":Landroid/widget/ImageView;
    check-cast v4, Landroid/widget/ImageView;

    .line 346
    .end local v1    # "addViewOnUiThread":Ljava/lang/Runnable;
    .end local v2    # "dragView":Landroid/view/View;
    .end local v5    # "mainView":Landroid/view/ViewGroup;
    .restart local v4    # "imageView":Landroid/widget/ImageView;
    :cond_0
    return-object v4

    .line 338
    .restart local v1    # "addViewOnUiThread":Ljava/lang/Runnable;
    .restart local v2    # "dragView":Landroid/view/View;
    .restart local v5    # "mainView":Landroid/view/ViewGroup;
    :catchall_0
    move-exception v6

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v6
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    .line 339
    :catch_0
    move-exception v3

    .line 340
    .local v3, "e":Ljava/lang/InterruptedException;
    const-string v6, "DragAndDrop"

    const-string v7, "ImageView was not added to main layout"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private sendToSideSync(Landroid/content/ClipData;Lcom/sec/android/gallery3d/ui/SelectionManager;)V
    .locals 11
    .param p1, "dragData"    # Landroid/content/ClipData;
    .param p2, "selectionProxy"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    .line 273
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 275
    .local v0, "i":Landroid/content/Intent;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 276
    .local v5, "mShareList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v2, 0x0

    .line 278
    .local v2, "isAlbumsDrag":Z
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPSSRunning(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 279
    const-string v9, "com.sec.android.sidesync.source.START_DRAG"

    invoke-virtual {v0, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 280
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setClipData(Landroid/content/ClipData;)V

    .line 281
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 282
    const-string v9, "DragAndDrop"

    const-string/jumbo v10, "sideSync send intent : START_DRAG"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKMSRunning(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 284
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v7

    .line 285
    .local v7, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 287
    .local v6, "mediaItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 288
    .local v4, "mObject":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v9, v4, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v9, :cond_3

    .line 289
    const/4 v2, 0x1

    move-object v8, v4

    .line 290
    check-cast v8, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 291
    .local v8, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v9, 0x0

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v10

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v9

    invoke-interface {v6, v9}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 293
    .end local v8    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_3
    instance-of v9, v4, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v9, :cond_2

    .line 294
    const/4 v2, 0x0

    move-object v3, v4

    .line 295
    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 296
    .local v3, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 299
    .end local v3    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v4    # "mObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_4
    if-eqz v2, :cond_5

    .line 300
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 301
    .restart local v3    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 304
    .end local v3    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_5
    const-string/jumbo v9, "sidesync.app.action.KMS_FILETRANSFER_DRAG_FILEINFO"

    invoke-virtual {v0, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 305
    const-string v9, "android.intent.extra.STREAM"

    invoke-virtual {v0, v9, v5}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 306
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    const-string/jumbo v10, "sidesync.app.action.permission.KMS_FILETRANSFER_DRAG_FILEINFO"

    invoke-virtual {v9, v0, v10}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 307
    const-string v9, "DragAndDrop"

    const-string/jumbo v10, "sideSync send intent : KMS_FILETRANSFER_DRAG_FILEINFO"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public setRotatedBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 20
    .param p1, "bm"    # Landroid/graphics/Bitmap;
    .param p2, "rotationDegree"    # I
    .param p3, "count"    # I

    .prologue
    .line 65
    const/4 v9, 0x0

    .line 66
    .local v9, "bitmap":Landroid/graphics/Bitmap;
    const/4 v10, 0x0

    .line 67
    .local v10, "bm2":Landroid/graphics/Bitmap;
    const-string v18, ""

    .line 68
    .local v18, "text":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v16

    .line 69
    .local v16, "numberFormat":Ljava/text/NumberFormat;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v15

    .line 70
    .local v15, "locale":Ljava/lang/String;
    const-string v2, "ar"

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "fa"

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "ur"

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 71
    :cond_0
    move/from16 v0, p3

    int-to-long v2, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v18

    .line 75
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d022a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    .line 76
    .local v14, "fontSize":F
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 77
    new-instance v2, Lcom/sec/samsung/gallery/view/common/DragAndDrop$LazyLoadedBitmap;

    const v3, 0x7f020209

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/sec/samsung/gallery/view/common/DragAndDrop$LazyLoadedBitmap;-><init>(Lcom/sec/samsung/gallery/view/common/DragAndDrop;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mIcon:Lcom/sec/samsung/gallery/view/common/DragAndDrop$LazyLoadedBitmap;

    .line 81
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 82
    .local v5, "sw":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 83
    .local v6, "sh":I
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 84
    .local v7, "mtx":Landroid/graphics/Matrix;
    new-instance v11, Landroid/graphics/Canvas;

    invoke-direct {v11}, Landroid/graphics/Canvas;-><init>()V

    .line 85
    .local v11, "canvas":Landroid/graphics/Canvas;
    new-instance v17, Landroid/graphics/Paint;

    const/4 v2, 0x2

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    .line 86
    .local v17, "paint":Landroid/graphics/Paint;
    move/from16 v0, p2

    int-to-float v2, v0

    invoke-virtual {v7, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 88
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x0

    move-object/from16 v2, p1

    :try_start_0
    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 89
    const/4 v2, 0x2

    invoke-static {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v2

    const/4 v3, 0x1

    invoke-static {v10, v2, v3}, Lcom/sec/android/gallery3d/common/BitmapUtils;->resizeAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 90
    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v19

    .line 91
    .local v19, "w":I
    invoke-virtual {v11, v9}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 92
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v2, :cond_3

    .line 93
    const/high16 v2, -0x1000000

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 97
    :goto_2
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 98
    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 99
    const/4 v2, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 100
    const v2, 0x3dcccccd    # 0.1f

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v8, -0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3, v4, v8}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 101
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mIcon:Lcom/sec/samsung/gallery/view/common/DragAndDrop$LazyLoadedBitmap;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/common/DragAndDrop$LazyLoadedBitmap;->get()Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mIcon:Lcom/sec/samsung/gallery/view/common/DragAndDrop$LazyLoadedBitmap;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/common/DragAndDrop$LazyLoadedBitmap;->get()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sub-int v3, v19, v3

    int-to-float v3, v3

    const/4 v4, 0x0

    const/4 v8, 0x0

    invoke-virtual {v11, v2, v3, v4, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 102
    move/from16 v0, v19

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mIcon:Lcom/sec/samsung/gallery/view/common/DragAndDrop$LazyLoadedBitmap;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/common/DragAndDrop$LazyLoadedBitmap;->get()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mIcon:Lcom/sec/samsung/gallery/view/common/DragAndDrop$LazyLoadedBitmap;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/common/DragAndDrop$LazyLoadedBitmap;->get()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    const/high16 v4, 0x40400000    # 3.0f

    div-float/2addr v3, v4

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v11, v0, v2, v3, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v2, v9

    .line 111
    .end local v19    # "w":I
    :goto_3
    return-object v2

    .line 73
    .end local v5    # "sw":I
    .end local v6    # "sh":I
    .end local v7    # "mtx":Landroid/graphics/Matrix;
    .end local v11    # "canvas":Landroid/graphics/Canvas;
    .end local v14    # "fontSize":F
    .end local v17    # "paint":Landroid/graphics/Paint;
    :cond_1
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v18

    goto/16 :goto_0

    .line 79
    .restart local v14    # "fontSize":F
    :cond_2
    new-instance v2, Lcom/sec/samsung/gallery/view/common/DragAndDrop$LazyLoadedBitmap;

    const v3, 0x7f020208

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/sec/samsung/gallery/view/common/DragAndDrop$LazyLoadedBitmap;-><init>(Lcom/sec/samsung/gallery/view/common/DragAndDrop;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mIcon:Lcom/sec/samsung/gallery/view/common/DragAndDrop$LazyLoadedBitmap;

    goto/16 :goto_1

    .line 95
    .restart local v5    # "sw":I
    .restart local v6    # "sh":I
    .restart local v7    # "mtx":Landroid/graphics/Matrix;
    .restart local v11    # "canvas":Landroid/graphics/Canvas;
    .restart local v17    # "paint":Landroid/graphics/Paint;
    .restart local v19    # "w":I
    :cond_3
    const/4 v2, -0x1

    :try_start_1
    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_2

    .line 103
    .end local v19    # "w":I
    :catch_0
    move-exception v12

    .line 104
    .local v12, "e":Ljava/lang/OutOfMemoryError;
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 105
    const/4 v2, 0x0

    goto :goto_3

    .line 106
    .end local v12    # "e":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v13

    .line 107
    .local v13, "ex":Ljava/lang/RuntimeException;
    invoke-virtual {v13}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 108
    const/4 v2, 0x0

    goto :goto_3
.end method

.method public startAlbumDrag(ILandroid/view/View;Landroid/widget/BaseAdapter;Lcom/sec/android/gallery3d/ui/SelectionManager;)V
    .locals 5
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "adapter"    # Landroid/widget/BaseAdapter;
    .param p4, "selectionProxy"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    .line 166
    invoke-virtual {p3, p1}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 167
    .local v2, "items":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getCoverMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    .line 168
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_0

    invoke-direct {p0, p1, v1, p4}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->getData(ILcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/ui/SelectionManager;)Landroid/content/ClipData;

    move-result-object v0

    .line 170
    .local v0, "dragData":Landroid/content/ClipData;
    :goto_0
    new-instance v3, Landroid/view/View$DragShadowBuilder;

    invoke-direct {v3, p2}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    .line 171
    .local v3, "shadowBuilder":Landroid/view/View$DragShadowBuilder;
    const/4 v4, 0x0

    invoke-virtual {p2, v0, v3, p2, v4}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 172
    return-void

    .line 168
    .end local v0    # "dragData":Landroid/content/ClipData;
    .end local v3    # "shadowBuilder":Landroid/view/View$DragShadowBuilder;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startAlbumDrag(ILcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;Lcom/sec/android/gallery3d/ui/SelectionManager;)V
    .locals 10
    .param p1, "position"    # I
    .param p2, "gridView"    # Lcom/sec/samsung/gallery/glview/GlAbsListView;
    .param p3, "adapter"    # Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;
    .param p4, "selectionProxy"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 116
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->getImageView()Landroid/widget/ImageView;

    move-result-object v3

    .line 118
    .local v3, "imageView":Landroid/widget/ImageView;
    if-nez p3, :cond_0

    .line 119
    const-string v5, "DragAndDrop"

    const-string/jumbo v6, "startAlbumDrag() adapter is null"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    .end local p2    # "gridView":Lcom/sec/samsung/gallery/glview/GlAbsListView;
    :goto_0
    return-void

    .line 122
    .restart local p2    # "gridView":Lcom/sec/samsung/gallery/glview/GlAbsListView;
    :cond_0
    const/4 v1, 0x0

    .line 123
    .local v1, "dragData":Landroid/content/ClipData;
    invoke-virtual {p3, p1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 124
    .local v4, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v4, :cond_2

    invoke-direct {p0, p1, v4, p4}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->getData(ILcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/ui/SelectionManager;)Landroid/content/ClipData;

    move-result-object v1

    .line 125
    :goto_1
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v5

    :goto_2
    iput v5, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->rotationDegree:I

    .line 126
    invoke-direct {p0, v1, p4}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->sendToSideSync(Landroid/content/ClipData;Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    .line 127
    const/4 v2, 0x0

    .line 128
    .local v2, "dragShadow":Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;
    invoke-virtual {p3, p1, v6}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->getItemImage(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 129
    .local v0, "bm":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    .line 130
    iget v5, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->rotationDegree:I

    invoke-virtual {p4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberItemOfMarkedAsSelected()I

    move-result v8

    invoke-virtual {p0, v0, v5, v8}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->setRotatedBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->bitmap:Landroid/graphics/Bitmap;

    .line 132
    :cond_1
    new-instance v2, Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;

    .end local v2    # "dragShadow":Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->bitmap:Landroid/graphics/Bitmap;

    check-cast p2, Lcom/sec/samsung/gallery/glview/GlGridView;

    .end local p2    # "gridView":Lcom/sec/samsung/gallery/glview/GlAbsListView;
    invoke-virtual {p2, p1}, Lcom/sec/samsung/gallery/glview/GlGridView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v9

    invoke-direct {v2, v3, v5, v8, v9}, Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;-><init>(Landroid/view/View;Landroid/content/Context;Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 133
    .restart local v2    # "dragShadow":Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;
    invoke-virtual {v3, v1, v2, v7, v6}, Landroid/widget/ImageView;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    goto :goto_0

    .end local v0    # "bm":Landroid/graphics/Bitmap;
    .end local v2    # "dragShadow":Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;
    .restart local p2    # "gridView":Lcom/sec/samsung/gallery/glview/GlAbsListView;
    :cond_2
    move-object v1, v7

    .line 124
    goto :goto_1

    :cond_3
    move v5, v6

    .line 125
    goto :goto_2
.end method

.method public startAlbumDrag(ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;Lcom/sec/android/gallery3d/ui/SelectionManager;)V
    .locals 11
    .param p1, "position"    # I
    .param p2, "composeView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;
    .param p3, "adapter"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;
    .param p4, "selectionProxy"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    const/4 v6, 0x0

    const/4 v10, 0x0

    .line 138
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->getImageView()Landroid/widget/ImageView;

    move-result-object v3

    .line 140
    .local v3, "imageView":Landroid/widget/ImageView;
    if-nez p3, :cond_0

    .line 141
    const-string v6, "DragAndDrop"

    const-string/jumbo v7, "startAlbumDrag() adapter is null"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    :goto_0
    return-void

    .line 144
    :cond_0
    const/4 v1, 0x0

    .line 145
    .local v1, "dragData":Landroid/content/ClipData;
    shl-int/lit8 v5, p1, 0x10

    .line 146
    .local v5, "objectIndex":I
    invoke-virtual {p3, p1, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    .line 147
    .local v4, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v4, :cond_2

    invoke-direct {p0, p1, v4, p4}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->getData(ILcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/ui/SelectionManager;)Landroid/content/ClipData;

    move-result-object v1

    .line 148
    :goto_1
    if-eqz v4, :cond_3

    .line 149
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v7

    iput v7, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->rotationDegree:I

    .line 155
    invoke-direct {p0, v1, p4}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->sendToSideSync(Landroid/content/ClipData;Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    .line 156
    const/4 v2, 0x0

    .line 157
    .local v2, "dragShadow":Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;
    invoke-virtual {p3, p1, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getItemImage(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 158
    .local v0, "bm":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    .line 159
    iget v7, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->rotationDegree:I

    invoke-virtual {p4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v8

    invoke-virtual {p0, v0, v7, v8}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->setRotatedBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->bitmap:Landroid/graphics/Bitmap;

    .line 161
    :cond_1
    new-instance v2, Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;

    .end local v2    # "dragShadow":Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p2, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v9

    invoke-direct {v2, v3, v7, v8, v9}, Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;-><init>(Landroid/view/View;Landroid/content/Context;Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 162
    .restart local v2    # "dragShadow":Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;
    invoke-virtual {v3, v1, v2, v6, v10}, Landroid/widget/ImageView;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    goto :goto_0

    .end local v0    # "bm":Landroid/graphics/Bitmap;
    .end local v2    # "dragShadow":Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;
    :cond_2
    move-object v1, v6

    .line 147
    goto :goto_1

    .line 151
    :cond_3
    const-string v6, "DragAndDrop"

    const-string v7, "drag item is null"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startPhotosDrag(IILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;Lcom/sec/android/gallery3d/ui/SelectionManager;)V
    .locals 7
    .param p1, "albumPostion"    # I
    .param p2, "itemPosition"    # I
    .param p3, "composeView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;
    .param p4, "adapter"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;
    .param p5, "selectionProxy"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    .line 226
    if-nez p4, :cond_1

    .line 227
    const-string v4, "DragAndDrop"

    const-string/jumbo v5, "startPhotosDrag() adapter is null "

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    :cond_0
    :goto_0
    return-void

    .line 230
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->getImageView()Landroid/widget/ImageView;

    move-result-object v2

    .line 231
    .local v2, "imageView":Landroid/widget/ImageView;
    invoke-direct {p0, p1, p2, p4, p5}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->getData(IILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;Lcom/sec/android/gallery3d/ui/SelectionManager;)Landroid/content/ClipData;

    move-result-object v0

    .line 232
    .local v0, "dragData":Landroid/content/ClipData;
    invoke-virtual {p4, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    .line 233
    .local v3, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v3, :cond_0

    .line 235
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->rotationDegree:I

    .line 237
    invoke-virtual {p4, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getItemImage(II)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->bitmap:Landroid/graphics/Bitmap;

    .line 238
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_2

    .line 239
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->bitmap:Landroid/graphics/Bitmap;

    iget v5, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->rotationDegree:I

    invoke-virtual {p5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v6

    invoke-virtual {p0, v4, v5, v6}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->setRotatedBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->bitmap:Landroid/graphics/Bitmap;

    .line 241
    :cond_2
    invoke-direct {p0, v0, p5}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->sendToSideSync(Landroid/content/ClipData;Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    .line 243
    new-instance v1, Lcom/sec/samsung/gallery/view/common/DragShadow;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->bitmap:Landroid/graphics/Bitmap;

    shl-int/lit8 v6, p1, 0x10

    or-int/2addr v6, p2

    invoke-virtual {p3, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v6

    invoke-direct {v1, v2, v4, v5, v6}, Lcom/sec/samsung/gallery/view/common/DragShadow;-><init>(Landroid/view/View;Landroid/content/Context;Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 245
    .local v1, "dragShadow":Lcom/sec/samsung/gallery/view/common/DragShadow;
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v0, v1, v4, v5}, Landroid/widget/ImageView;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    goto :goto_0
.end method

.method public startPhotosDrag(ILcom/sec/android/gallery3d/glcore/GlObject;Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/ui/SelectionManager;)V
    .locals 5
    .param p1, "position"    # I
    .param p2, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p3, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p4, "bitmap"    # Landroid/graphics/Bitmap;
    .param p5, "selectionProxy"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    .line 189
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->getImageView()Landroid/widget/ImageView;

    move-result-object v2

    .line 190
    .local v2, "imageView":Landroid/widget/ImageView;
    invoke-direct {p0, p1, p3, p5}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->getData(ILcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/ui/SelectionManager;)Landroid/content/ClipData;

    move-result-object v0

    .line 191
    .local v0, "dragData":Landroid/content/ClipData;
    if-eqz p4, :cond_0

    .line 192
    iget v3, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->rotationDegree:I

    invoke-virtual {p5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v4

    invoke-virtual {p0, p4, v3, v4}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->setRotatedBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object p4

    .line 194
    :cond_0
    invoke-direct {p0, v0, p5}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->sendToSideSync(Landroid/content/ClipData;Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    .line 196
    new-instance v1, Lcom/sec/samsung/gallery/view/common/DragShadow;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, v3, p4, p2}, Lcom/sec/samsung/gallery/view/common/DragShadow;-><init>(Landroid/view/View;Landroid/content/Context;Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 197
    .local v1, "dragShadow":Lcom/sec/samsung/gallery/view/common/DragShadow;
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/widget/ImageView;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 198
    return-void
.end method

.method public startPhotosDrag(ILcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;Lcom/sec/android/gallery3d/ui/SelectionManager;)V
    .locals 8
    .param p1, "position"    # I
    .param p2, "gridView"    # Lcom/sec/samsung/gallery/glview/GlAbsListView;
    .param p3, "adapter"    # Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;
    .param p4, "selectionProxy"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    const/4 v7, 0x0

    .line 250
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->getImageView()Landroid/widget/ImageView;

    move-result-object v2

    .line 252
    .local v2, "imageView":Landroid/widget/ImageView;
    if-nez p3, :cond_1

    .line 253
    const-string v4, "DragAndDrop"

    const-string/jumbo v5, "startPhotosDrag() adapter is null "

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    .end local p2    # "gridView":Lcom/sec/samsung/gallery/glview/GlAbsListView;
    :cond_0
    :goto_0
    return-void

    .line 256
    .restart local p2    # "gridView":Lcom/sec/samsung/gallery/glview/GlAbsListView;
    :cond_1
    invoke-direct {p0, p1, p3, p4}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->getData(ILcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;Lcom/sec/android/gallery3d/ui/SelectionManager;)Landroid/content/ClipData;

    move-result-object v0

    .line 257
    .local v0, "dragData":Landroid/content/ClipData;
    invoke-virtual {p3, p1}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    .line 258
    .local v3, "items":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v3, :cond_0

    .line 260
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->rotationDegree:I

    .line 261
    invoke-virtual {p3, p1, v7}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getItemImage(II)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->bitmap:Landroid/graphics/Bitmap;

    .line 262
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_2

    .line 263
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->bitmap:Landroid/graphics/Bitmap;

    iget v5, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->rotationDegree:I

    invoke-virtual {p4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberItemOfMarkedAsSelected()I

    move-result v6

    invoke-virtual {p0, v4, v5, v6}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->setRotatedBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->bitmap:Landroid/graphics/Bitmap;

    .line 265
    :cond_2
    invoke-direct {p0, v0, p4}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->sendToSideSync(Landroid/content/ClipData;Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    .line 267
    const/4 v1, 0x0

    .line 268
    .local v1, "dragShadow":Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;
    new-instance v1, Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;

    .end local v1    # "dragShadow":Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->bitmap:Landroid/graphics/Bitmap;

    check-cast p2, Lcom/sec/samsung/gallery/glview/GlGridView;

    .end local p2    # "gridView":Lcom/sec/samsung/gallery/glview/GlAbsListView;
    invoke-virtual {p2, p1}, Lcom/sec/samsung/gallery/glview/GlGridView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v6

    invoke-direct {v1, v2, v4, v5, v6}, Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;-><init>(Landroid/view/View;Landroid/content/Context;Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 269
    .restart local v1    # "dragShadow":Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;
    const/4 v4, 0x0

    invoke-virtual {v2, v0, v1, v4, v7}, Landroid/widget/ImageView;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    goto :goto_0
.end method

.method public startPhotosDrag(ILcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;Lcom/sec/android/gallery3d/ui/SelectionManager;)V
    .locals 7
    .param p1, "position"    # I
    .param p2, "mTimelineView"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;
    .param p3, "adapter"    # Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;
    .param p4, "selectionProxy"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    .line 203
    if-nez p3, :cond_1

    .line 204
    const-string v4, "DragAndDrop"

    const-string/jumbo v5, "startPhotosDrag() adapter is null "

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 207
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->getImageView()Landroid/widget/ImageView;

    move-result-object v2

    .line 208
    .local v2, "imageView":Landroid/widget/ImageView;
    invoke-direct {p0, p1, p3, p4}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->getData(ILcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;Lcom/sec/android/gallery3d/ui/SelectionManager;)Landroid/content/ClipData;

    move-result-object v0

    .line 209
    .local v0, "dragData":Landroid/content/ClipData;
    invoke-virtual {p3, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    .line 210
    .local v3, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v3, :cond_0

    .line 212
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->rotationDegree:I

    .line 213
    invoke-virtual {p3, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getThumbnailBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->bitmap:Landroid/graphics/Bitmap;

    .line 214
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_2

    .line 215
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->bitmap:Landroid/graphics/Bitmap;

    iget v5, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->rotationDegree:I

    invoke-virtual {p4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v6

    invoke-virtual {p0, v4, v5, v6}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->setRotatedBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->bitmap:Landroid/graphics/Bitmap;

    .line 217
    :cond_2
    invoke-direct {p0, v0, p4}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->sendToSideSync(Landroid/content/ClipData;Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    .line 219
    new-instance v1, Lcom/sec/samsung/gallery/view/common/DragShadow;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p2, p1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getObjectIndex(I)Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v6

    invoke-direct {v1, v2, v4, v5, v6}, Lcom/sec/samsung/gallery/view/common/DragShadow;-><init>(Landroid/view/View;Landroid/content/Context;Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 220
    .local v1, "dragShadow":Lcom/sec/samsung/gallery/view/common/DragShadow;
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v0, v1, v4, v5}, Landroid/widget/ImageView;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    goto :goto_0
.end method

.method public startSinglePhotoDrag(Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/ui/PhotoView$Model;Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 7
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "model"    # Lcom/sec/android/gallery3d/ui/PhotoView$Model;
    .param p3, "layer"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 176
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->getImageView()Landroid/widget/ImageView;

    move-result-object v1

    .line 177
    .local v1, "imageView":Landroid/widget/ImageView;
    const/4 v6, 0x0

    .line 178
    .local v6, "dragData":Landroid/content/ClipData;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isConnetedSideSync(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 179
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v4, "selectedUri"

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    invoke-static {v2, v4, v5}, Landroid/content/ClipData;->newUri(Landroid/content/ContentResolver;Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    move-result-object v6

    .line 182
    :goto_0
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->getBitmap(Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/ui/PhotoView$Model;Lcom/sec/android/gallery3d/ui/PhotoView;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 184
    .local v3, "bmp":Landroid/graphics/Bitmap;
    new-instance v0, Lcom/sec/samsung/gallery/view/common/SinglePhotoDragShadow;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    invoke-virtual {p3}, Lcom/sec/android/gallery3d/ui/PhotoView;->getWidth()I

    move-result v4

    invoke-virtual {p3}, Lcom/sec/android/gallery3d/ui/PhotoView;->getHeight()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/common/SinglePhotoDragShadow;-><init>(Landroid/view/View;Landroid/content/Context;Landroid/graphics/Bitmap;II)V

    .line 185
    .local v0, "mDragShadow":Lcom/sec/samsung/gallery/view/common/SinglePhotoDragShadow;
    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v6, v0, v2, v4}, Landroid/widget/ImageView;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 186
    return-void

    .line 181
    .end local v0    # "mDragShadow":Lcom/sec/samsung/gallery/view/common/SinglePhotoDragShadow;
    .end local v3    # "bmp":Landroid/graphics/Bitmap;
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v4, "galleryUri"

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    invoke-static {v2, v4, v5}, Landroid/content/ClipData;->newUri(Landroid/content/ContentResolver;Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    move-result-object v6

    goto :goto_0
.end method
