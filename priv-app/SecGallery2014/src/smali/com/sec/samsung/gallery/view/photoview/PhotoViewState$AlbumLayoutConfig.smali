.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;
.super Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;
.source "PhotoViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AlbumLayoutConfig"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;Landroid/content/res/Resources;)V
    .locals 6
    .param p2, "res"    # Landroid/content/res/Resources;

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 749
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;-><init>()V

    .line 750
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;->mUsePenSelectInPickMode:Z

    .line 751
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;->mUseEnlargeAnim:Z

    .line 752
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;->mUseLayoutChange:Z

    .line 753
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;->mUseGroupSelect:Z

    .line 754
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;->mUseGroupTitle:Z

    .line 755
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;->mUseItemSelect:Z

    .line 757
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;->mAlbumList:Z

    .line 758
    sget v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->SPLIT_RATIO_LAND:F

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;->mLandRatio:F

    .line 759
    sget v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->SPLIT_RATIO_PORT:F

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;->mPortRatio:F

    .line 760
    const/16 v1, 0xa

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;->mMaxObject:I

    .line 763
    iput v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;->mInitialLevel:I

    .line 764
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;->mTopGroupTitle:Z

    .line 765
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;->mUseQuickScroll:Z

    .line 766
    iget-object v1, p1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;->mViewTabType:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 767
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v2

    aput-object v4, v1, v3

    const/4 v2, 0x2

    aput-object v4, v1, v2

    const/4 v2, 0x3

    aput-object v4, v1, v2

    const-class v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    aput-object v2, v1, v5

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;->mPosCtrl:[Ljava/lang/Object;

    .line 769
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 770
    .local v0, "outValue":Landroid/util/TypedValue;
    const v1, 0x7f0d00f0

    invoke-virtual {p2, v1, v0, v3}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 771
    invoke-virtual {v0}, Landroid/util/TypedValue;->getFloat()F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;->mItemSizeScale:F

    .line 772
    return-void
.end method
