.class Lcom/sec/samsung/gallery/glview/GlGridView$6;
.super Ljava/lang/Object;
.source "GlGridView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGridView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V
    .locals 0

    .prologue
    .line 837
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 3
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 843
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    if-eqz v0, :cond_0

    .line 844
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iput-object v2, v0, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 845
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iput-object v2, v0, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mIndexs:[I

    .line 846
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iput-object v2, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    .line 848
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mOnConvAnim:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$102(Lcom/sec/samsung/gallery/glview/GlGridView;Z)Z

    .line 849
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iput v1, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAnimType:I

    .line 850
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iput-boolean v1, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mLockPos:Z

    .line 851
    return-void
.end method

.method public onAnimationRepeat(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 0
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 854
    return-void
.end method

.method public onAnimationStart(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 0
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 840
    return-void
.end method
