.class Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$6;
.super Ljava/lang/Object;
.source "MoreInfoItemUserTag.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->resetEditMode()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)V
    .locals 0

    .prologue
    .line 368
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$6;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 372
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$6;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->clearFocus()V

    .line 373
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$6;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->setCursorVisible(Z)V

    .line 374
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$6;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->setText(Ljava/lang/CharSequence;)V

    .line 375
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$6;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$6;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    # invokes: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->hideSoftInput(Landroid/os/IBinder;)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$800(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;Landroid/os/IBinder;)V

    .line 376
    return-void
.end method
