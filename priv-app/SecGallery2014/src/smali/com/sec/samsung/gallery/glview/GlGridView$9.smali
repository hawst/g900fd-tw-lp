.class Lcom/sec/samsung/gallery/glview/GlGridView$9;
.super Ljava/lang/Object;
.source "GlGridView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGridView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V
    .locals 0

    .prologue
    .line 1031
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Lcom/sec/android/gallery3d/glcore/GlObject;)Z
    .locals 6
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1033
    move-object v1, p1

    check-cast v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget v0, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 1035
    .local v0, "index":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mClickable:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$800(Lcom/sec/samsung/gallery/glview/GlGridView;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1050
    :cond_0
    :goto_0
    return v5

    .line 1037
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mMode:I

    sget v2, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_NORMAL:I

    if-eq v1, v2, :cond_3

    .line 1038
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    .line 1043
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCaneExtraHapticEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1044
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/16 v2, 0x5320

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    .line 1045
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;

    if-eqz v1, :cond_0

    if-ltz v0, :cond_0

    .line 1046
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mFocused:I
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$502(Lcom/sec/samsung/gallery/glview/GlGridView;I)I

    .line 1047
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlGridView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;->onItemClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 1048
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v0, v4, v4}, Lcom/sec/samsung/gallery/glview/GlGridView;->setCommand(IIII)V

    goto :goto_0

    .line 1040
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    goto :goto_1
.end method
