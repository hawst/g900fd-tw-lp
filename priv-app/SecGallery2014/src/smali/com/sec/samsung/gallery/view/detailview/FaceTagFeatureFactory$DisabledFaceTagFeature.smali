.class Lcom/sec/samsung/gallery/view/detailview/FaceTagFeatureFactory$DisabledFaceTagFeature;
.super Ljava/lang/Object;
.source "FaceTagFeatureFactory.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/detailview/IFaceTagFeature;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/FaceTagFeatureFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DisabledFaceTagFeature"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/view/detailview/FaceTagFeatureFactory$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/view/detailview/FaceTagFeatureFactory$1;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/FaceTagFeatureFactory$DisabledFaceTagFeature;-><init>()V

    return-void
.end method


# virtual methods
.method public assignName(Landroid/net/Uri;Z)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "checkUserId"    # Z

    .prologue
    .line 51
    return-void
.end method

.method public cancelFaceScan()V
    .locals 0

    .prologue
    .line 36
    return-void
.end method

.method public cropManualFD(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V
    .locals 0
    .param p1, "bmp"    # Landroid/graphics/Bitmap;
    .param p2, "croppedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 46
    return-void
.end method

.method public disableBuddyShare()V
    .locals 0

    .prologue
    .line 31
    return-void
.end method

.method public dismissAlertDialog()V
    .locals 0

    .prologue
    .line 56
    return-void
.end method

.method public enableBuddyShare()V
    .locals 0

    .prologue
    .line 26
    return-void
.end method

.method public getManualFaceTagUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return-object v0
.end method

.method public setAsCallerId(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 41
    return-void
.end method

.method public setManualFaceTagUri(Landroid/net/Uri;)V
    .locals 0
    .param p1, "manualFaceTagUri"    # Landroid/net/Uri;

    .prologue
    .line 76
    return-void
.end method

.method public setScanPriority(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 0
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 71
    return-void
.end method

.method public showAlertDialog(Landroid/content/Context;ILjava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "titleId"    # I
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 61
    return-void
.end method

.method public showAlertDialog(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "titleId"    # I
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "lookupkey"    # Ljava/lang/String;

    .prologue
    .line 66
    return-void
.end method
