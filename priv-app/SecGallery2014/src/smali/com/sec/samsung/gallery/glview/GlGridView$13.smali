.class Lcom/sec/samsung/gallery/glview/GlGridView$13;
.super Ljava/lang/Object;
.source "GlGridView.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGridView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V
    .locals 0

    .prologue
    .line 1179
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$13;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public hoverClick(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;I)Z
    .locals 4
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "index"    # I

    .prologue
    .line 1182
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$13;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnHoverListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnHoverListenerEx;

    if-nez v2, :cond_0

    .line 1183
    const/4 v2, 0x0

    .line 1187
    :goto_0
    return v2

    .line 1185
    :cond_0
    shr-int/lit8 v0, p3, 0x10

    .line 1186
    .local v0, "albumIndex":I
    const v2, 0xffff

    and-int v1, p3, v2

    .line 1187
    .local v1, "photoIndex":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$13;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlGridView;->mOnHoverListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnHoverListenerEx;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView$13;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlGridView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-interface {v2, v3, v0, v1, p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnHoverListenerEx;->onHoverClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;IILjava/lang/Object;)Z

    move-result v2

    goto :goto_0
.end method
