.class public Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;
.super Lcom/sec/android/gallery3d/glcore/GlObject;
.source "GlComposeScrollBar.java"


# static fields
.field private static final FADE_OUT_DELAY:I = 0x1f4

.field public static FADE_OUT_DURATION:I

.field public static THRESHOLD:F


# instance fields
.field protected mBottom:F

.field private mCanvasColor:I

.field protected mDefZ:F

.field protected mEnabledQuickScroll:Z

.field private mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

.field protected mFadeOutDelay:I

.field protected mFocusBorderVisible:Z

.field protected mHeightSpace:F

.field protected mHeightViewRatio:F

.field private mMaxHeight:F

.field private mMinHeight:F

.field private mOffsetX:F

.field private mOffsetY:F

.field private mOffsetZ:F

.field protected mPaddingRight:F

.field private mQuickScrollRatio:F

.field protected mThinkness:F

.field protected mTop:F

.field protected mWidthSpace:F

.field protected mWidthViewRatio:F

.field private mXRatio:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const/16 v0, 0x1f4

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->FADE_OUT_DURATION:I

    .line 11
    const/high16 v0, 0x40000000    # 2.0f

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->THRESHOLD:F

    return-void
.end method

.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 3
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 37
    invoke-direct {p0, p1, v0, v0}, Lcom/sec/android/gallery3d/glcore/GlObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 20
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mXRatio:F

    .line 21
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mDefZ:F

    .line 23
    const/high16 v0, 0x43480000    # 200.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mMaxHeight:F

    .line 24
    const/high16 v0, 0x42200000    # 40.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mMinHeight:F

    .line 25
    const v0, -0xca9889

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mCanvasColor:I

    .line 31
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mEnabledQuickScroll:Z

    .line 32
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mFocusBorderVisible:Z

    .line 34
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mQuickScrollRatio:F

    .line 38
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_0

    .line 39
    const v0, -0x5e5c5b

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mCanvasColor:I

    .line 41
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->initScrollBar()V

    .line 42
    return-void
.end method

.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V
    .locals 3
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "width"    # F
    .param p3, "height"    # F

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 45
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/gallery3d/glcore/GlObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 20
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mXRatio:F

    .line 21
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mDefZ:F

    .line 23
    const/high16 v0, 0x43480000    # 200.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mMaxHeight:F

    .line 24
    const/high16 v0, 0x42200000    # 40.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mMinHeight:F

    .line 25
    const v0, -0xca9889

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mCanvasColor:I

    .line 31
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mEnabledQuickScroll:Z

    .line 32
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mFocusBorderVisible:Z

    .line 34
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mQuickScrollRatio:F

    .line 46
    return-void
.end method


# virtual methods
.method public calcuScrollRatio(FF)V
    .locals 2
    .param p1, "currentY"    # F
    .param p2, "scrollArea"    # F

    .prologue
    .line 190
    const/4 v1, 0x0

    cmpg-float v1, p1, v1

    if-gez v1, :cond_0

    .line 191
    const/4 v0, 0x0

    .line 198
    .local v0, "ratio":F
    :goto_0
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mQuickScrollRatio:F

    .line 199
    return-void

    .line 192
    .end local v0    # "ratio":F
    :cond_0
    cmpl-float v1, p1, p2

    if-lez v1, :cond_1

    .line 193
    const/high16 v0, 0x3f800000    # 1.0f

    .restart local v0    # "ratio":F
    goto :goto_0

    .line 195
    .end local v0    # "ratio":F
    :cond_1
    div-float v0, p1, p2

    .restart local v0    # "ratio":F
    goto :goto_0
.end method

.method public disableQuickScroll()V
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mEnabledQuickScroll:Z

    .line 214
    return-void
.end method

.method public enableQuickScroll()V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->stop()V

    .line 209
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mEnabledQuickScroll:Z

    .line 210
    return-void
.end method

.method public fadeOut()V
    .locals 4

    .prologue
    .line 162
    const-wide/16 v0, 0x0

    sget v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->FADE_OUT_DURATION:I

    int-to-long v2, v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->fadeOut(JJ)V

    .line 163
    return-void
.end method

.method public fadeOut(J)V
    .locals 3
    .param p1, "delay"    # J

    .prologue
    .line 166
    sget v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->FADE_OUT_DURATION:I

    int-to-long v0, v0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->fadeOut(JJ)V

    .line 167
    return-void
.end method

.method public fadeOut(JJ)V
    .locals 3
    .param p1, "delay"    # J
    .param p3, "duration"    # J

    .prologue
    const/4 v2, 0x0

    .line 170
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    if-nez v0, :cond_0

    .line 171
    new-instance v0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->getAlpha()F

    move-result v1

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;-><init>(Lcom/sec/android/gallery3d/glcore/GlObject;FF)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    .line 172
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->getAlpha()F

    move-result v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->setParam(FF)V

    .line 175
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0, p3, p4}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->setDuration(J)V

    .line 176
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->startAfter(J)V

    .line 177
    return-void
.end method

.method public getFocusBorderVisible()Z
    .locals 1

    .prologue
    .line 225
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mFocusBorderVisible:Z

    return v0
.end method

.method protected getHeight(FF)F
    .locals 7
    .param p1, "min"    # F
    .param p2, "max"    # F

    .prologue
    const/4 v4, 0x0

    .line 134
    sub-float v3, p2, p1

    .line 136
    .local v3, "range":F
    sget v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->THRESHOLD:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mHeightSpace:F

    mul-float v0, v5, v6

    .line 137
    .local v0, "T":F
    cmpl-float v5, v3, v0

    if-lez v5, :cond_1

    .line 138
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mMinHeight:F

    .line 143
    :cond_0
    :goto_0
    return v4

    .line 141
    :cond_1
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mMinHeight:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mMaxHeight:F

    sub-float/2addr v5, v6

    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mTop:F

    add-float/2addr v5, v6

    div-float v1, v5, v0

    .line 142
    .local v1, "a":F
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mMaxHeight:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mTop:F

    sub-float v2, v5, v6

    .line 143
    .local v2, "b":F
    cmpl-float v5, v3, v4

    if-lez v5, :cond_0

    mul-float v4, v1, v3

    add-float/2addr v4, v2

    goto :goto_0
.end method

.method public getQuickScrollRatio()F
    .locals 1

    .prologue
    .line 202
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mQuickScrollRatio:F

    return v0
.end method

.method public getScrollRatio()F
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/high16 v5, 0x40000000    # 2.0f

    .line 180
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mHeightSpace:F

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->getHeight(Z)F

    move-result v4

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mTop:F

    sub-float v2, v3, v4

    .line 181
    .local v2, "scrollArea":F
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mHeightSpace:F

    div-float/2addr v3, v5

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->getY()F

    move-result v4

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mTop:F

    sub-float/2addr v3, v4

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->getHeight(Z)F

    move-result v4

    div-float/2addr v4, v5

    sub-float v0, v3, v4

    .line 182
    .local v0, "curScrollY":F
    div-float v1, v0, v2

    .line 184
    .local v1, "ratio":F
    return v1
.end method

.method public hide()V
    .locals 2

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mFadeOutDelay:I

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->fadeOut(J)V

    .line 155
    return-void
.end method

.method public hideNow()V
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->setAlpha(F)V

    .line 159
    return-void
.end method

.method protected initScrollBar()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 49
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->setEmptyFill(Z)V

    .line 50
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mCanvasColor:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->setEmptyFillColor(I)V

    .line 52
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_0

    .line 53
    const v0, -0x706d6a

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->setBorderColor(I)V

    .line 57
    :goto_0
    const/high16 v0, 0x40000000    # 2.0f

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->setBorderWidth(F)V

    .line 58
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->setBorderVisible(Z)V

    .line 59
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mFadeOutDelay:I

    .line 60
    return-void

    .line 55
    :cond_0
    const v0, -0xbb8372

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->setBorderColor(I)V

    goto :goto_0
.end method

.method public isEnabledQuickScroll()Z
    .locals 1

    .prologue
    .line 217
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mEnabledQuickScroll:Z

    return v0
.end method

.method public reset(FFFFFF)V
    .locals 2
    .param p1, "widthViewRatio"    # F
    .param p2, "heightViewRatio"    # F
    .param p3, "widthSpace"    # F
    .param p4, "heightSpace"    # F
    .param p5, "top"    # F
    .param p6, "bottom"    # F

    .prologue
    .line 74
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mWidthViewRatio:F

    .line 75
    iput p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mHeightViewRatio:F

    .line 76
    iput p5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mTop:F

    .line 77
    iput p6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mBottom:F

    .line 78
    iput p3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mWidthSpace:F

    .line 79
    iput p4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mHeightSpace:F

    .line 80
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mHeightSpace:F

    const/high16 v1, 0x41a00000    # 20.0f

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mMaxHeight:F

    .line 81
    return-void
.end method

.method public setCanvasColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mCanvasColor:I

    if-eq v0, p1, :cond_0

    .line 64
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mCanvasColor:I

    .line 65
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->invalidate()V

    .line 67
    :cond_0
    return-void
.end method

.method public setDefZ(F)V
    .locals 0
    .param p1, "z"    # F

    .prologue
    .line 88
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mDefZ:F

    .line 89
    return-void
.end method

.method public setFocusBorderVisible(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 221
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mFocusBorderVisible:Z

    .line 222
    return-void
.end method

.method public setGenericMotionListener()V
    .locals 0

    .prologue
    .line 234
    return-void
.end method

.method public setMaxHeight(F)V
    .locals 0
    .param p1, "maxHeight"    # F

    .prologue
    .line 120
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mMaxHeight:F

    .line 121
    return-void
.end method

.method public setMinHeight(F)V
    .locals 0
    .param p1, "minHeight"    # F

    .prologue
    .line 124
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mMinHeight:F

    .line 125
    return-void
.end method

.method public setOffset(FFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 128
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mOffsetX:F

    .line 129
    iput p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mOffsetY:F

    .line 130
    iput p3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mOffsetZ:F

    .line 131
    return-void
.end method

.method public setPosXRatio(F)V
    .locals 0
    .param p1, "r"    # F

    .prologue
    .line 84
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mXRatio:F

    .line 85
    return-void
.end method

.method public setRightPadding(F)V
    .locals 0
    .param p1, "padding"    # F

    .prologue
    .line 92
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mPaddingRight:F

    .line 93
    return-void
.end method

.method public setThinkness(F)V
    .locals 0
    .param p1, "thinkness"    # F

    .prologue
    .line 70
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mThinkness:F

    .line 71
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->stop()V

    .line 150
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->setAlpha(F)V

    .line 151
    return-void
.end method

.method public update(FFF)V
    .locals 10
    .param p1, "scroll"    # F
    .param p2, "min"    # F
    .param p3, "max"    # F

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    .line 96
    sub-float v4, p3, p2

    .line 97
    .local v4, "range":F
    invoke-virtual {p0, p2, p3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->getHeight(FF)F

    move-result v3

    .line 99
    .local v3, "height":F
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mThinkness:F

    invoke-virtual {p0, v6, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->setSize(FF)V

    .line 100
    const/4 v6, 0x0

    cmpg-float v6, v4, v6

    if-gtz v6, :cond_0

    .line 117
    :goto_0
    return-void

    .line 111
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mHeightSpace:F

    .line 112
    .local v0, "H":F
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mTop:F

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mBottom:F

    add-float/2addr v6, v7

    add-float/2addr v6, v3

    sub-float/2addr v6, v0

    div-float v1, v6, v4

    .line 113
    .local v1, "a":F
    neg-float v6, v0

    div-float/2addr v6, v8

    div-float v7, v3, v8

    add-float/2addr v6, v7

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mBottom:F

    add-float/2addr v6, v7

    mul-float v7, p3, v1

    sub-float v2, v6, v7

    .line 114
    .local v2, "b":F
    mul-float v6, v1, p1

    add-float v5, v6, v2

    .line 115
    .local v5, "y":F
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mWidthSpace:F

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mXRatio:F

    mul-float/2addr v6, v7

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mWidthSpace:F

    div-float/2addr v7, v8

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mWidth:F

    div-float/2addr v7, v8

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mPaddingRight:F

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mOffsetX:F

    add-float/2addr v6, v7

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mOffsetY:F

    add-float/2addr v7, v5

    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mDefZ:F

    const/high16 v9, 0x3f800000    # 1.0f

    sub-float/2addr v8, v9

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->mOffsetZ:F

    add-float/2addr v8, v9

    invoke-virtual {p0, v6, v7, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->setPos(FFF)V

    goto :goto_0
.end method

.method public updateScrollTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 230
    return-void
.end method
