.class public Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
.super Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;
.source "DisplayMediaItem.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/FutureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem$OnBitmapAvailableListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;",
        "Lcom/sec/android/gallery3d/util/FutureListener",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# static fields
.field public static DEFAULT_VERSION:J = 0x0L

.field private static final TAG:Ljava/lang/String; = "DisplayMediaItem"


# instance fields
.field private mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mFuture:Lcom/sec/android/gallery3d/util/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public mIndex:I

.field private mOnBitmapAvailableListener:Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem$OnBitmapAvailableListener;

.field private mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

.field private mType:I

.field public mVersion:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->DEFAULT_VERSION:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/samsung/gallery/glview/GlBaseAdapter;Lcom/sec/android/gallery3d/data/MediaItem;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "adapter"    # Lcom/sec/samsung/gallery/glview/GlBaseAdapter;
    .param p3, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p4, "index"    # I

    .prologue
    .line 33
    const/4 v5, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;-><init>(Landroid/content/Context;Lcom/sec/samsung/gallery/glview/GlBaseAdapter;Lcom/sec/android/gallery3d/data/MediaItem;II)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/samsung/gallery/glview/GlBaseAdapter;Lcom/sec/android/gallery3d/data/MediaItem;II)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "adapter"    # Lcom/sec/samsung/gallery/glview/GlBaseAdapter;
    .param p3, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p4, "index"    # I
    .param p5, "type"    # I

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0, p3}, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;-><init>(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 22
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mOnBitmapAvailableListener:Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem$OnBitmapAvailableListener;

    .line 23
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .line 26
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 29
    sget-wide v0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->DEFAULT_VERSION:J

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mVersion:J

    .line 38
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .line 39
    iput p4, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mIndex:I

    .line 40
    check-cast p1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    .line 41
    iput p5, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mType:I

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem$OnBitmapAvailableListener;Lcom/sec/android/gallery3d/data/MediaItem;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem$OnBitmapAvailableListener;
    .param p3, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p4, "index"    # I

    .prologue
    .line 45
    const/4 v5, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;-><init>(Landroid/content/Context;Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem$OnBitmapAvailableListener;Lcom/sec/android/gallery3d/data/MediaItem;II)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem$OnBitmapAvailableListener;Lcom/sec/android/gallery3d/data/MediaItem;II)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem$OnBitmapAvailableListener;
    .param p3, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p4, "index"    # I
    .param p5, "type"    # I

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0, p3}, Lcom/sec/android/gallery3d/ui/AbstractDisplayItem;-><init>(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 22
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mOnBitmapAvailableListener:Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem$OnBitmapAvailableListener;

    .line 23
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .line 26
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 29
    sget-wide v0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->DEFAULT_VERSION:J

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mVersion:J

    .line 50
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mOnBitmapAvailableListener:Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem$OnBitmapAvailableListener;

    .line 51
    iput p4, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mIndex:I

    .line 52
    check-cast p1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    .line 53
    iput p5, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mType:I

    .line 54
    return-void
.end method


# virtual methods
.method protected cancelLoadBitmap()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mFuture:Lcom/sec/android/gallery3d/util/Future;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mFuture:Lcom/sec/android/gallery3d/util/Future;

    invoke-interface {v0}, Lcom/sec/android/gallery3d/util/Future;->cancel()V

    .line 71
    :cond_0
    return-void
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getItem()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method public getThumbType()I
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mType:I

    return v0
.end method

.method public getVersion()J
    .locals 2

    .prologue
    .line 135
    iget-wide v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mVersion:J

    return-wide v0
.end method

.method public isBitmapReady()Z
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onBitmapAvailable(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget v1, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mIndex:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->notifyDataSetChanged(II)V

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mOnBitmapAvailableListener:Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem$OnBitmapAvailableListener;

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mOnBitmapAvailableListener:Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem$OnBitmapAvailableListener;

    iget v1, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mIndex:I

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem$OnBitmapAvailableListener;->onBitmapAvailable(I)V

    .line 80
    :cond_1
    return-void
.end method

.method public onFutureDone(Lcom/sec/android/gallery3d/util/Future;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/util/Future",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 89
    .local p1, "future":Lcom/sec/android/gallery3d/util/Future;, "Lcom/sec/android/gallery3d/util/Future<Landroid/graphics/Bitmap;>;"
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/Future;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 90
    invoke-interface {p1}, Lcom/sec/android/gallery3d/util/Future;->isCancelled()Z

    move-result v0

    .line 95
    .local v0, "isCancelled":Z
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v1, v0}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->updateImage(Landroid/graphics/Bitmap;Z)V

    .line 96
    return-void
.end method

.method protected recycleBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 100
    invoke-static {}, Lcom/sec/android/photos/data/GalleryBitmapPool;->getInstance()Lcom/sec/android/photos/data/GalleryBitmapPool;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/photos/data/GalleryBitmapPool;->put(Landroid/graphics/Bitmap;)Z

    .line 101
    return-void
.end method

.method public refreshAvailable()Z
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-nez v0, :cond_0

    .line 140
    const/4 v0, 0x0

    .line 141
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->refreshAvailable()Z

    move-result v0

    goto :goto_0
.end method

.method public render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;I)I
    .locals 1
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "pass"    # I

    .prologue
    .line 84
    const/4 v0, 0x0

    return v0
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 112
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mBitmap:Landroid/graphics/Bitmap;

    if-ne p1, v0, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 116
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 119
    :cond_2
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public setThumbType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 149
    iput p1, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mType:I

    .line 150
    return-void
.end method

.method public setVersion(J)V
    .locals 1
    .param p1, "version"    # J

    .prologue
    .line 131
    iput-wide p1, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mVersion:J

    .line 132
    return-void
.end method

.method protected startLoadBitmap()V
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    iget v2, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mType:I

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mFuture:Lcom/sec/android/gallery3d/util/Future;

    .line 59
    return-void
.end method

.method public startRefreshBitmap()V
    .locals 3

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->requestRefresh(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/gallery3d/util/ThreadPool;->submit(Lcom/sec/android/gallery3d/util/ThreadPool$Job;Lcom/sec/android/gallery3d/util/FutureListener;)Lcom/sec/android/gallery3d/util/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mFuture:Lcom/sec/android/gallery3d/util/Future;

    .line 64
    return-void
.end method
