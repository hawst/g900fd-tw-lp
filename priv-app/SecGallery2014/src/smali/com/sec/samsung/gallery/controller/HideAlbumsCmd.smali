.class public Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "HideAlbumsCmd.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Ljava/util/Observer;
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field private static final MSG_EXIT_SELECTION_MODE:I = 0x0

.field private static final MSG_EXIT_SELECTION_MODE_AND_RESET:I = 0x1


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDeleteDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

.field private mHandler:Landroid/os/Handler;

.field private mOnHiddenListener:Lcom/sec/samsung/gallery/util/HiddenOperations$OnHiddenListener;

.field private mOperateHideAsyncTask:Lcom/sec/samsung/gallery/util/HiddenOperations;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 51
    new-instance v0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->dismissDialog()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->showToast(II)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;)Lcom/sec/samsung/gallery/util/HiddenOperations;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mOperateHideAsyncTask:Lcom/sec/samsung/gallery/util/HiddenOperations;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method private dismissDeleteDialog()V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mDeleteDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mDeleteDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->dismissDialog()V

    .line 208
    :cond_0
    return-void
.end method

.method private dismissDialog()V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 191
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 194
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 196
    :cond_1
    return-void
.end method

.method private getNumberOfItemsToDelete()I
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    return v0
.end method

.method private getPopupText(Landroid/content/Context;)Ljava/lang/String;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 141
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v2

    .line 142
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->getNumberOfItemsToDelete()I

    move-result v4

    if-ne v4, v5, :cond_1

    .line 143
    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v4, :cond_0

    .line 144
    const v4, 0x7f0e01c3

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 160
    :goto_0
    return-object v4

    .line 146
    :cond_0
    const v4, 0x7f0e0042

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 149
    :cond_1
    const/4 v1, 0x1

    .line 150
    .local v1, "isAlbumOnly":Z
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 151
    .local v3, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v4, v3, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v4, :cond_2

    .line 152
    const/4 v1, 0x0

    .line 156
    .end local v3    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_3
    if-eqz v1, :cond_4

    .line 157
    const v4, 0x7f0e01c4

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v5, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->getNumberOfItemsToDelete()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 160
    :cond_4
    const v4, 0x7f0e0043

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v5, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->getNumberOfItemsToDelete()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private handleOperation(I)V
    .locals 3
    .param p1, "operationId"    # I

    .prologue
    const/4 v1, 0x1

    .line 91
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mProgressDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 92
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->showProgressDialog()Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 93
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd$2;

    invoke-direct {v0, p0, p1}, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd$2;-><init>(Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;I)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mOnHiddenListener:Lcom/sec/samsung/gallery/util/HiddenOperations$OnHiddenListener;

    .line 123
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mOperateHideAsyncTask:Lcom/sec/samsung/gallery/util/HiddenOperations;

    if-eqz v0, :cond_1

    .line 124
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mOperateHideAsyncTask:Lcom/sec/samsung/gallery/util/HiddenOperations;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/HiddenOperations;->cancel(Z)Z

    .line 127
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setReloadRequiredOnResume(Z)V

    .line 128
    new-instance v0, Lcom/sec/samsung/gallery/util/HiddenOperations;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mOnHiddenListener:Lcom/sec/samsung/gallery/util/HiddenOperations$OnHiddenListener;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, p1, v1, v2}, Lcom/sec/samsung/gallery/util/HiddenOperations;-><init>(ILcom/sec/samsung/gallery/util/HiddenOperations$OnHiddenListener;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mOperateHideAsyncTask:Lcom/sec/samsung/gallery/util/HiddenOperations;

    .line 129
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mOperateHideAsyncTask:Lcom/sec/samsung/gallery/util/HiddenOperations;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/HiddenOperations;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 130
    return-void
.end method

.method private showDeleteDialog(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "popupText"    # Ljava/lang/String;

    .prologue
    .line 199
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELETE_MEDIA:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    .line 200
    .local v0, "event":Lcom/sec/samsung/gallery/core/Event;
    new-instance v1, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    const v2, 0x7f0e0041

    invoke-direct {v1, p1, v2, p2, v0}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;-><init>(Landroid/content/Context;ILjava/lang/String;Lcom/sec/samsung/gallery/core/Event;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mDeleteDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    .line 201
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mDeleteDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    invoke-virtual {v1, p0}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->addObserver(Ljava/util/Observer;)V

    .line 202
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mDeleteDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->showDialog()V

    .line 203
    return-void
.end method

.method private showProgressDialog()Landroid/app/ProgressDialog;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 171
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 172
    .local v0, "dialog":Landroid/app/ProgressDialog;
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 173
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 174
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mContext:Landroid/content/Context;

    const v2, 0x7f0e00ca

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 175
    new-instance v1, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd$3;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd$3;-><init>(Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 184
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->hideStatusIcon(Ljava/lang/Object;)V

    .line 185
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 186
    return-object v0
.end method

.method private showToast(II)V
    .locals 5
    .param p1, "operationId"    # I
    .param p2, "failedCount"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 211
    const/4 v0, 0x0

    .line 212
    .local v0, "msg":Ljava/lang/String;
    if-nez p1, :cond_3

    .line 213
    if-ne p2, v3, :cond_1

    .line 214
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e01dc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 235
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 236
    return-void

    .line 215
    :cond_1
    if-le p2, v3, :cond_2

    .line 216
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e01dd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 218
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e01db

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 220
    :cond_3
    if-ne p1, v3, :cond_6

    .line 221
    if-ne p2, v3, :cond_4

    .line 222
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e01df

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 223
    :cond_4
    if-le p2, v3, :cond_5

    .line 224
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e01e0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 226
    :cond_5
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e01de

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 228
    :cond_6
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 229
    if-lez p2, :cond_7

    .line 230
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e01f7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 232
    :cond_7
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mContext:Landroid/content/Context;

    const v2, 0x7f0e01a1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 9
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 67
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/Object;

    move-object v2, v5

    check-cast v2, [Ljava/lang/Object;

    .line 68
    .local v2, "params":[Ljava/lang/Object;
    aget-object v5, v2, v8

    check-cast v5, Landroid/content/Context;

    iput-object v5, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mContext:Landroid/content/Context;

    .line 69
    aget-object v5, v2, v6

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 70
    .local v1, "hide":Z
    const/4 v5, 0x2

    aget-object v5, v2, v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 71
    .local v4, "show":Z
    const/4 v5, 0x3

    aget-object v5, v2, v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 72
    .local v0, "delete":Z
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mContext:Landroid/content/Context;

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 73
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mContext:Landroid/content/Context;

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 74
    if-eqz v1, :cond_1

    .line 75
    invoke-direct {p0, v8}, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->handleOperation(I)V

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    if-eqz v4, :cond_2

    .line 77
    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->handleOperation(I)V

    goto :goto_0

    .line 78
    :cond_2
    if-eqz v0, :cond_0

    .line 79
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->dismissDeleteDialog()V

    .line 80
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->getNumberOfItemsToDelete()I

    move-result v5

    if-nez v5, :cond_3

    .line 81
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mHandler:Landroid/os/Handler;

    const-wide/16 v6, 0x64

    invoke-virtual {v5, v8, v6, v7}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 82
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mContext:Landroid/content/Context;

    const v6, 0x7f0e0113

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 84
    :cond_3
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mContext:Landroid/content/Context;

    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->getPopupText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 85
    .local v3, "popupText":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mContext:Landroid/content/Context;

    invoke-direct {p0, v5, v3}, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->showDeleteDialog(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    .line 240
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mOperateHideAsyncTask:Lcom/sec/samsung/gallery/util/HiddenOperations;

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mOperateHideAsyncTask:Lcom/sec/samsung/gallery/util/HiddenOperations;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/HiddenOperations;->cancel(Z)Z

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 244
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 3
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 134
    move-object v0, p2

    check-cast v0, Lcom/sec/samsung/gallery/core/Event;

    .line 135
    .local v0, "ev":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELETE_MEDIA:I

    if-ne v1, v2, :cond_0

    .line 136
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->handleOperation(I)V

    .line 138
    :cond_0
    return-void
.end method
