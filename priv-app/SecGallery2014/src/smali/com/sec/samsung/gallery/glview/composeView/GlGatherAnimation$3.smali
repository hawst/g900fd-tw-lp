.class Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$3;
.super Ljava/lang/Object;
.source "GlGatherAnimation.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;)V
    .locals 0

    .prologue
    .line 373
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$3;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 3
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    const/4 v1, 0x0

    .line 381
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$3;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->access$500(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;)Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$3;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->access$500(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;)Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    move-result-object v2

    iget v0, v2, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mIndex:I

    .line 382
    .local v0, "headIndex":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$3;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mOnDragListener:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$OnDragAnimListener;
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->access$600(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;)Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$OnDragAnimListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 383
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$3;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mOnDragListener:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$OnDragAnimListener;
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->access$600(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;)Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$OnDragAnimListener;

    move-result-object v2

    invoke-interface {v2, p1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$OnDragAnimListener;->onDragAnimDone(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;I)V

    .line 385
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$3;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->releaseDrag()V

    .line 386
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$3;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    # setter for: Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mIsMoveToAnimationRunning:Z
    invoke-static {v2, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->access$402(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;Z)Z

    .line 387
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$3;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    sget v2, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->DRAG_ANIM_NONE:I

    # setter for: Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCurrentAnim:I
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->access$002(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;I)I

    .line 388
    return-void

    .end local v0    # "headIndex":I
    :cond_1
    move v0, v1

    .line 381
    goto :goto_0
.end method

.method public onAnimationRepeat(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 0
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 390
    return-void
.end method

.method public onAnimationStart(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 2
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 376
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$3;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    const/4 v1, 0x1

    # setter for: Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mIsMoveToAnimationRunning:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->access$402(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;Z)Z

    .line 377
    return-void
.end method
