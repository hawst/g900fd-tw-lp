.class Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick$1;
.super Ljava/lang/Object;
.source "AlbumActionBarForPick.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 46
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->access$000(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const v7, 0x7f030006

    invoke-virtual {v4, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 48
    .local v0, "customView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 49
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->access$100(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 51
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->access$200(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    const v7, 0x7f0f0018

    invoke-virtual {v4, v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 52
    .local v3, "mSelectionButton":Landroid/widget/Button;
    if-eqz v3, :cond_1

    .line 53
    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 55
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->access$300(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ActionBar;->removeAllTabs()V

    .line 56
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->access$400(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 57
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->access$500(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/app/ActionBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 59
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->isDrawableHideMode()Z

    move-result v2

    .line 60
    .local v2, "isDrawableHide":Z
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->access$600(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v7

    if-nez v2, :cond_2

    move v4, v5

    :goto_0
    invoke-virtual {v7, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 61
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->access$700(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v7

    if-nez v2, :cond_3

    move v4, v5

    :goto_1
    invoke-virtual {v7, v4}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 63
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->access$800(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 64
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->access$900(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 66
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->access$1000(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 67
    .local v1, "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    const-string v5, "help_mode"

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    # setter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->mIsHelpMode:Z
    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;->access$1102(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;Z)Z

    .line 68
    return-void

    .end local v1    # "intent":Landroid/content/Intent;
    :cond_2
    move v4, v6

    .line 60
    goto :goto_0

    :cond_3
    move v4, v6

    .line 61
    goto :goto_1
.end method
