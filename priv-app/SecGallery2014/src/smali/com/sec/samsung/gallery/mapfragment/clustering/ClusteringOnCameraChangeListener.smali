.class Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;
.super Ljava/lang/Object;
.source "ClusteringOnCameraChangeListener.java"

# interfaces
.implements Lcom/google/android/gms/maps/GoogleMap$OnCameraChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener$Host;
    }
.end annotation


# instance fields
.field private dirty:J

.field private final hostRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener$Host;",
            ">;"
        }
    .end annotation
.end field

.field private final options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener$Host;Lcom/sec/samsung/gallery/mapfragment/clustering/Options;)V
    .locals 2
    .param p1, "host"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener$Host;
    .param p2, "options"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;->dirty:J

    .line 19
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;->hostRef:Ljava/lang/ref/WeakReference;

    .line 20
    iput-object p2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    .line 21
    return-void
.end method


# virtual methods
.method public onCameraChange(Lcom/google/android/gms/maps/model/CameraPosition;)V
    .locals 8
    .param p1, "newPosition"    # Lcom/google/android/gms/maps/model/CameraPosition;

    .prologue
    .line 25
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 26
    .local v4, "now":J
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->getClusteringOnCameraChangeListenerDirtyLifetimeMillis()J

    move-result-wide v6

    sub-long v2, v4, v6

    .line 27
    .local v2, "notDirtyAfter":J
    iget-wide v6, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;->dirty:J

    cmp-long v1, v6, v2

    if-gez v1, :cond_0

    .line 28
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;->hostRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener$Host;

    .line 29
    .local v0, "host":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener$Host;
    if-eqz v0, :cond_0

    .line 30
    iput-wide v4, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;->dirty:J

    .line 31
    invoke-interface {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener$Host;->onClusteringCameraChange()V

    .line 34
    .end local v0    # "host":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener$Host;
    :cond_0
    return-void
.end method

.method public setDirty(J)V
    .locals 1
    .param p1, "when"    # J

    .prologue
    .line 37
    iput-wide p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;->dirty:J

    .line 38
    return-void
.end method
