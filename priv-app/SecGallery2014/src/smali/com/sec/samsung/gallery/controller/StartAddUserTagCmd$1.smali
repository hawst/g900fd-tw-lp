.class Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$1;
.super Ljava/lang/Object;
.source "StartAddUserTagCmd.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/OnProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->showDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mUriList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;

.field final synthetic val$isCancelled:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final synthetic val$progressDialog:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

.field final synthetic val$requestCode:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;I)V
    .locals 1

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;

    iput-object p2, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$1;->val$isCancelled:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p3, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$1;->val$progressDialog:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    iput p4, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$1;->val$requestCode:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$1;->mUriList:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 8
    .param p1, "object"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    const/4 v6, 0x0

    .line 67
    if-nez p1, :cond_1

    .line 68
    # getter for: Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->access$000()Ljava/lang/String;

    move-result-object v5

    const-string v6, "selected item\'s uri is null, item is ignored"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    .end local p1    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_0
    :goto_0
    const/4 v5, 0x1

    :goto_1
    return v5

    .line 69
    .restart local p1    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    instance-of v5, p1, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v5, :cond_3

    move-object v5, p1

    .line 70
    check-cast v5, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    .line 71
    .local v0, "count":I
    check-cast p1, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local p1    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p1, v6, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v2

    .line 72
    .local v2, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 73
    .local v3, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$1;->val$isCancelled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v5

    if-eqz v5, :cond_2

    move v5, v6

    .line 74
    goto :goto_1

    .line 76
    :cond_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v7

    invoke-virtual {v5, v7}, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->getBaseUri(I)Ljava/lang/String;

    move-result-object v4

    .line 77
    .local v4, "uri":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$1;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 79
    .end local v0    # "count":I
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v4    # "uri":Ljava/lang/String;
    .restart local p1    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_3
    instance-of v5, p1, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v5, :cond_0

    move-object v3, p1

    .line 80
    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 81
    .restart local v3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->getBaseUri(I)Ljava/lang/String;

    move-result-object v4

    .line 82
    .restart local v4    # "uri":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$1;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public handleOperation(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 89
    .local p1, "objectList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public onCompleted(Z)V
    .locals 3
    .param p1, "result"    # Z

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$1;->val$progressDialog:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->closeProgressDialog()V

    .line 95
    if-eqz p1, :cond_0

    .line 96
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;

    new-instance v1, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->access$200(Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->mAddTagDialog:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->access$102(Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;)Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    .line 97
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->mAddTagDialog:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->access$100(Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;)Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$1;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->setSelectedItemUriList(Ljava/util/ArrayList;)V

    .line 98
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->mAddTagDialog:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->access$100(Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;)Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    move-result-object v0

    iget v1, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$1;->val$requestCode:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->setRequestCode(I)V

    .line 99
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->mAddTagDialog:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;->access$100(Lcom/sec/samsung/gallery/controller/StartAddUserTagCmd;)Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->showAddTagDialog()V

    .line 101
    :cond_0
    return-void
.end method

.method public onProgress(II)V
    .locals 0
    .param p1, "progress"    # I
    .param p2, "total"    # I

    .prologue
    .line 63
    return-void
.end method
