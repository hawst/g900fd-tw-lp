.class public Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;
.super Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
.source "PhotoNoteView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView$SpenNoteDocLoadTask;
    }
.end annotation


# static fields
.field private static final MESSAGE_SPENNOTEDOC_LOAD_FINISHED:I = 0x0

.field public static final TAG:Ljava/lang/String; = "PhotoNote"


# instance fields
.field private imageRect:Landroid/graphics/Rect;

.field private mMainHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "listener"    # Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;-><init>(Landroid/content/Context;Landroid/content/Intent;Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;)V

    .line 59
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView$1;-><init>(Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mMainHandler:Landroid/os/Handler;

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getBackgroundImage()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 234
    sget-object v1, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->BGIMAGE_PATH:Ljava/lang/String;

    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 235
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 236
    const-string v1, "PhotoNote"

    const-string v2, "PhotoNote : photo note bg was not exist, create again"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mFilePath:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->imageRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->createPhotoNoteBG(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Rect;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 239
    :cond_0
    return-object v0
.end method

.method private getMediaItemFromIntentData()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 285
    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 286
    .local v0, "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    .line 287
    .local v1, "manager":Lcom/sec/android/gallery3d/data/DataManager;
    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mUri:Landroid/net/Uri;

    if-nez v4, :cond_0

    .line 288
    const-string v4, "PhotoNote"

    const-string v5, "no data given"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    :goto_0
    return-object v3

    .line 291
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v4, v3}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    .line 292
    .local v2, "path":Lcom/sec/android/gallery3d/data/Path;
    if-nez v2, :cond_1

    .line 293
    const-string v4, "PhotoNote"

    const-string v5, "cannot get path for: "

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 296
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;Z)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    goto :goto_0
.end method

.method private getSaveFilePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "srcFilePath"    # Ljava/lang/String;

    .prologue
    .line 300
    new-instance v2, Ljava/io/File;

    sget-object v4, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->PHOTO_NOTE_FOLDER:Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 301
    .local v2, "folder":Ljava/io/File;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 302
    .local v1, "fileName":Ljava/lang/String;
    const/4 v4, 0x0

    const-string v5, "."

    invoke-virtual {v1, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 303
    .local v3, "newFileName":Ljava/lang/String;
    const-string v4, "."

    invoke-virtual {v1, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 304
    .local v0, "ext":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x2f

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2, v3, v0}, Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;->getUniqueFilename(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private setCanvasZoomToFit()V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x0

    .line 243
    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    if-nez v3, :cond_0

    .line 252
    :goto_0
    return-void

    .line 245
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mScreenSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float v2, v3, v4

    .line 246
    .local v2, "ratioW":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mScreenSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float v1, v3, v4

    .line 247
    .local v1, "ratioH":F
    cmpg-float v3, v2, v1

    if-gez v3, :cond_1

    move v0, v2

    .line 248
    .local v0, "ratio":F
    :goto_1
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->setMinZoomRatio(F)Z

    .line 249
    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v6

    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v6

    invoke-virtual {p0, v3, v4, v0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->setZoom(FFF)V

    .line 250
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3, v5, v5}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->setPan(Landroid/graphics/PointF;)V

    .line 251
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->updateScreenFrameBuffer()V

    goto :goto_0

    .end local v0    # "ratio":F
    :cond_1
    move v0, v1

    .line 247
    goto :goto_1
.end method

.method private update(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 12
    .param p1, "srcPath"    # Ljava/lang/String;
    .param p2, "destPath"    # Ljava/lang/String;
    .param p3, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 255
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->getMediaItemFromIntentData()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    .line 256
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v1, :cond_0

    .line 257
    const/4 v8, 0x0

    .line 281
    :goto_0
    return-object v8

    :cond_0
    move-object v4, v1

    .line 259
    check-cast v4, Lcom/sec/android/gallery3d/data/LocalImage;

    .line 260
    .local v4, "localImage":Lcom/sec/android/gallery3d/data/LocalImage;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long v6, v8, v10

    .line 261
    .local v6, "now":J
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 262
    .local v2, "fileSize":J
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 263
    .local v0, "bucketName":Ljava/lang/String;
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 264
    .local v5, "values":Landroid/content/ContentValues;
    const-string v8, "_id"

    iget v9, v4, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 265
    const-string/jumbo v8, "title"

    iget-object v9, v4, Lcom/sec/android/gallery3d/data/LocalImage;->caption:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    const-string v8, "mime_type"

    iget-object v9, v4, Lcom/sec/android/gallery3d/data/LocalImage;->mimeType:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    const-string v8, "latitude"

    iget-wide v10, v4, Lcom/sec/android/gallery3d/data/LocalImage;->latitude:D

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 268
    const-string v8, "longitude"

    iget-wide v10, v4, Lcom/sec/android/gallery3d/data/LocalImage;->longitude:D

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 269
    const-string v8, "datetaken"

    iget-wide v10, v4, Lcom/sec/android/gallery3d/data/LocalImage;->dateTakenInMs:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 270
    const-string v8, "date_added"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 271
    const-string v8, "date_modified"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 272
    const-string v8, "_data"

    invoke-virtual {v5, v8, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    const-string v8, "orientation"

    iget v9, v4, Lcom/sec/android/gallery3d/data/LocalImage;->rotation:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 274
    const-string v8, "_size"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 275
    const-string/jumbo v8, "width"

    iget v9, v4, Lcom/sec/android/gallery3d/data/LocalImage;->width:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 276
    const-string v8, "height"

    iget v9, v4, Lcom/sec/android/gallery3d/data/LocalImage;->height:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 278
    const-string v8, "_id"

    invoke-virtual {v5, v8}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 279
    const-string v8, "bucket_id"

    invoke-static {p3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getBucketId(Ljava/lang/String;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 280
    const-string v8, "bucket_display_name"

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    iget-object v8, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8, v9, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v8

    goto/16 :goto_0
.end method

.method private writeExifInfo(Ljava/lang/String;)V
    .locals 8
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 310
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->getMediaItemFromIntentData()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    .line 311
    .local v4, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isJpeg(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 312
    new-instance v3, Lcom/sec/android/gallery3d/exif/ExifInterface;

    invoke-direct {v3}, Lcom/sec/android/gallery3d/exif/ExifInterface;-><init>()V

    .line 313
    .local v3, "exifInterface":Lcom/sec/android/gallery3d/exif/ExifInterface;
    sget v6, Lcom/sec/android/gallery3d/exif/ExifInterface;->TAG_ORIENTATION:I

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/exif/ExifInterface;->getOrientationValueForRotation(I)S

    move-result v7

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/gallery3d/exif/ExifInterface;->buildTag(ILjava/lang/Object;)Lcom/sec/android/gallery3d/exif/ExifTag;

    move-result-object v5

    .line 316
    .local v5, "tag":Lcom/sec/android/gallery3d/exif/ExifTag;
    :try_start_0
    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/exif/ExifInterface;->setTag(Lcom/sec/android/gallery3d/exif/ExifTag;)Lcom/sec/android/gallery3d/exif/ExifTag;

    .line 317
    invoke-virtual {v3, p1}, Lcom/sec/android/gallery3d/exif/ExifInterface;->forceRewriteExif(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 335
    .end local v3    # "exifInterface":Lcom/sec/android/gallery3d/exif/ExifInterface;
    .end local v5    # "tag":Lcom/sec/android/gallery3d/exif/ExifTag;
    :cond_0
    :goto_0
    return-void

    .line 318
    .restart local v3    # "exifInterface":Lcom/sec/android/gallery3d/exif/ExifInterface;
    .restart local v5    # "tag":Lcom/sec/android/gallery3d/exif/ExifTag;
    :catch_0
    move-exception v0

    .line 319
    .local v0, "e":Ljava/io/FileNotFoundException;
    const-string v6, "PhotoNote"

    const-string v7, "cannot find file to set exif: "

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 321
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 322
    .local v0, "e":Ljava/io/IOException;
    const-string v6, "PhotoNote"

    const-string v7, "cannot set exif data - write orientation"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    :try_start_1
    new-instance v2, Landroid/media/ExifInterface;

    invoke-direct {v2, p1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 327
    .local v2, "exif":Landroid/media/ExifInterface;
    const-string v6, "Orientation"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    invoke-virtual {v2}, Landroid/media/ExifInterface;->saveAttributes()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 330
    .end local v2    # "exif":Landroid/media/ExifInterface;
    :catch_2
    move-exception v1

    .line 331
    .local v1, "e1":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method protected createPhotoNoteBG(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Rect;)Landroid/graphics/Bitmap;
    .locals 18
    .param p1, "photoFile"    # Ljava/lang/String;
    .param p2, "waterMarkerFile"    # Ljava/lang/String;
    .param p3, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 338
    const/16 v8, 0xc8

    .line 339
    .local v8, "DIMMING_ALPHA":I
    move-object/from16 v0, p3

    iget v2, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p3

    iget v3, v0, Landroid/graphics/Rect;->left:I

    sub-int v16, v2, v3

    .line 340
    .local v16, "nCurWidth":I
    move-object/from16 v0, p3

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p3

    iget v3, v0, Landroid/graphics/Rect;->top:I

    sub-int v15, v2, v3

    .line 341
    .local v15, "nCurHeight":I
    const/4 v12, 0x0

    .line 343
    .local v12, "bmpResult":Landroid/graphics/Bitmap;
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;->decodeImageFile(Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 344
    .local v11, "bmpPhoto":Landroid/graphics/Bitmap;
    if-nez v11, :cond_1

    .line 345
    const/4 v1, 0x0

    .line 393
    :cond_0
    :goto_0
    return-object v1

    .line 346
    :cond_1
    const/4 v2, 0x0

    move/from16 v0, v16

    invoke-static {v11, v0, v15, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 347
    .local v1, "bmpScaledPhoto":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_2

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 348
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->recycle()V

    .line 350
    :cond_2
    if-nez v1, :cond_0

    .line 355
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 356
    .local v6, "matrix":Landroid/graphics/Matrix;
    const/high16 v2, -0x40800000    # -1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v6, v2, v3}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 358
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 359
    const/4 v1, 0x0

    goto :goto_0

    .line 360
    :cond_3
    const/4 v10, 0x0

    .line 361
    .local v10, "bmpInversedPhoto":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_4

    .line 362
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 366
    :cond_4
    if-eqz v10, :cond_5

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_5

    .line 367
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 370
    :cond_5
    new-instance v14, Landroid/graphics/Canvas;

    invoke-direct {v14, v10}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 372
    .local v14, "canvas":Landroid/graphics/Canvas;
    new-instance v17, Landroid/graphics/Paint;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Paint;-><init>()V

    .line 373
    .local v17, "textPaint":Landroid/graphics/Paint;
    const/16 v2, 0xc8

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 375
    const/4 v13, 0x0

    .line 377
    .local v13, "bmpWaterMarker":Landroid/graphics/Bitmap;
    move/from16 v0, v16

    if-lt v0, v15, :cond_8

    .line 378
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0203fc

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 383
    :goto_1
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    move/from16 v0, v16

    if-le v0, v2, :cond_6

    .line 384
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v16

    .line 385
    :cond_6
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-le v15, v2, :cond_7

    .line 386
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v15

    .line 388
    :cond_7
    const/4 v2, 0x0

    const/4 v3, 0x0

    move/from16 v0, v16

    invoke-static {v13, v2, v3, v0, v15}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 389
    .local v9, "bmpFixedWaterMarker":Landroid/graphics/Bitmap;
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v14, v9, v2, v3, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 390
    move-object v12, v10

    .line 391
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V

    move-object v1, v12

    .line 393
    goto/16 :goto_0

    .line 380
    .end local v9    # "bmpFixedWaterMarker":Landroid/graphics/Bitmap;
    :cond_8
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0203fd

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v13

    goto :goto_1
.end method

.method public getNoteType()I
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x1

    return v0
.end method

.method protected initializeSPenModel()V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mCanvasContainer:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 110
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView$SpenNoteDocLoadTask;

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView$SpenNoteDocLoadTask;-><init>(Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;Landroid/content/Context;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView$SpenNoteDocLoadTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 111
    return-void
.end method

.method protected setBackgroundToNoteDoc()V
    .locals 5

    .prologue
    .line 221
    :try_start_0
    sget-object v2, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->BGIMAGE_PATH:Ljava/lang/String;

    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 222
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 223
    const-string v2, "PhotoNote"

    const-string v3, "PhotoNote : photo note bg was not exist, create again"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mFilePath:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->imageRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v2, v3, v4}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->createPhotoNoteBG(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Rect;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 226
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->setVolatileBackgroundImage(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 231
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-void

    .line 227
    :catch_0
    move-exception v1

    .line 228
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mContext:Landroid/content/Context;

    const v3, 0x7f0e0035

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 229
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSCanvasViewLayout()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 73
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mSourceRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v2, v3, v3, v3}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->getMaximumCanvasRect(Landroid/graphics/Rect;III)Landroid/graphics/Rect;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->imageRect:Landroid/graphics/Rect;

    .line 74
    new-instance v0, Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->imageRect:Landroid/graphics/Rect;

    invoke-direct {v0, v2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 75
    .local v0, "canvasRect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mImageRect:Landroid/graphics/Point;

    iget v3, v0, Landroid/graphics/Rect;->right:I

    iget v4, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Point;->x:I

    .line 76
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mImageRect:Landroid/graphics/Point;

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    iget v4, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Point;->y:I

    .line 79
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mScreenSize:Landroid/graphics/Point;

    .line 80
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mCanvasContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 82
    .local v1, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mScreenSize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 83
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mScreenSize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 84
    const/16 v2, 0x11

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 85
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mCanvasContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 87
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mCanvasContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 88
    return-void
.end method

.method public updateAnimation()V
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mCanvasContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 175
    return-void
.end method

.method protected updateNoteDoc()V
    .locals 6

    .prologue
    .line 116
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    if-eqz v1, :cond_0

    .line 117
    invoke-super {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->updateNoteDoc()V

    .line 137
    :goto_0
    return-void

    .line 122
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mImageRect:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mImageRect:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    if-gt v1, v2, :cond_1

    .line 123
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mImageRect:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mImageRect:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;-><init>(Landroid/content/Context;III)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 135
    :goto_1
    invoke-super {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->updateNoteDoc()V

    .line 136
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->setCanvasZoomToFit()V

    goto :goto_0

    .line 126
    :cond_1
    :try_start_1
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mImageRect:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mImageRect:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;-><init>(Landroid/content/Context;III)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 129
    :catch_0
    move-exception v0

    .line 130
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 131
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 132
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateSCanvasViewLayout()V
    .locals 2

    .prologue
    .line 92
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mScreenSize:Landroid/graphics/Point;

    .line 94
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mCanvasContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 96
    .local v0, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mScreenSize:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 97
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mScreenSize:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 98
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 99
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mCanvasContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 101
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mSpenDocInitialized:Z

    if-eqz v1, :cond_0

    .line 102
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->setCanvasZoomToFit()V

    .line 104
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mCanvasContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 105
    return-void
.end method

.method public writeData(Ljava/lang/String;Z)Z
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "clearNoteDocData"    # Z

    .prologue
    const/4 v4, 0x0

    .line 184
    if-eqz p2, :cond_0

    .line 186
    :try_start_0
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteFile;->removeNote(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 215
    :goto_0
    const/4 v4, 0x1

    :goto_1
    return v4

    .line 187
    :catch_0
    move-exception v0

    .line 188
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 190
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 191
    .local v0, "e":Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;->printStackTrace()V

    goto :goto_1

    .line 195
    .end local v0    # "e":Lcom/samsung/android/sdk/pen/document/SpenUnsupportedTypeException;
    :cond_0
    invoke-static {p1}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isSoundScene(Ljava/lang/String;)Z

    move-result v1

    .line 196
    .local v1, "isSoundScene":Z
    if-eqz v1, :cond_1

    .line 197
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->getSaveFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 198
    .local v3, "savePath":Ljava/lang/String;
    invoke-static {p1, v3}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteUtils;->copyFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 199
    sget-object v5, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->PHOTO_NOTE_FOLDER:Ljava/lang/String;

    invoke-direct {p0, p1, v3, v5}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->update(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    .line 200
    move-object p1, v3

    .line 205
    .end local v3    # "savePath":Ljava/lang/String;
    :cond_1
    :try_start_1
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->writeExifInfo(Ljava/lang/String;)V

    .line 206
    iget-object v5, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteView;->mNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    invoke-virtual {v5, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->attachToFile(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_0

    .line 207
    :catch_2
    move-exception v0

    .line 208
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 210
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 211
    .local v2, "re":Ljava/lang/RuntimeException;
    invoke-virtual {v2}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_1
.end method
