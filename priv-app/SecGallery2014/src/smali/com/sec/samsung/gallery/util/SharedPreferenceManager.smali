.class public Lcom/sec/samsung/gallery/util/SharedPreferenceManager;
.super Ljava/lang/Object;
.source "SharedPreferenceManager.java"


# static fields
.field public static final ADAPT_DISPLAY_DIALOG_OFF_PERF:Ljava/lang/String; = "AdaptDisplayDialogOff"

.field public static final ALBUM_REORDER_MODE_ORDER_BACKUP:Ljava/lang/String; = "album_order_backup"

.field public static final ALBUM_VIEW_MODE:Ljava/lang/String; = "albumViewMode"

.field public static final ALLOW_MOBILE_NETWORK:Ljava/lang/String; = "allow_mobile_network"

.field public static final ALL_VIEW_MODE:Ljava/lang/String; = "allViewMode"

.field private static final APPPREFS:Ljava/lang/String; = "preference_manager"

.field public static final AVERAGE_DELETE_DB_OPERATION_TIME:Ljava/lang/String; = "AverageDeleteDbOperationTime"

.field public static final CATEGORY_VIEW_COLCNT:Ljava/lang/String; = "categoryViewColCnt"

.field public static final CHANGE_PLAYER_WIFI_ALERT_DIALOG_OFF_PERF:Ljava/lang/String; = "ChangePlayerWifiDataAlertDialogOff"

.field public static final CONTEXTUAL_TAG_HELP_OFF:Ljava/lang/String; = "contextualTagHelpOff"

.field public static final DRAG_AND_DROP_DIALOG_OFF_PERF:Ljava/lang/String; = "DragAndDropDialogOff"

.field public static final DROPBOX_PREFERENCE_NAME:Ljava/lang/String; = "DropBoxPref"

.field public static final EVENT_LAST_TIME:Ljava/lang/String; = "EventLastTime"

.field public static final EVENT_PREFERENCE_NAME:Ljava/lang/String; = "EventInfo"

.field public static final EVENT_VIEW_MODE:Ljava/lang/String; = "eventViewMode"

.field public static final FACETAG_PERMISSION_ALERT_DIALOG_OFF_PERF:Ljava/lang/String; = "FaceTagPermissionAlertDialogOff"

.field public static final FACE_TAG_DIALOG_OFF_PERF:Ljava/lang/String; = "FaceTagDialogOff"

.field public static final HIDDEN_PHOTO_MODE:Ljava/lang/String; = "hiddenPhotoMode"

.field public static final IS_SPLIT_VIEW_EXPANDED:Ljava/lang/String; = "is_split_view_expanded"

.field public static final LAST_VIEW_TYPE:Ljava/lang/String; = "lastViewType"

.field public static final LATEST_UPDATE_ALUBM:Ljava/lang/String; = "latest_update_album"

.field public static final LATEST_UPDATE_ITEM:Ljava/lang/String; = "latest_update_item"

.field public static final LOCATION_NETWORK_ALERT_DIALOG_OFF_PERF:Ljava/lang/String; = "LocationNetworkAlertDialogOff"

.field public static final LOCATION_PERMISSION_ALERT_DIALOG_OFF_PERF:Ljava/lang/String; = "LocationPermissionAlertDialogOff"

.field public static final MOTION_CALIBRATION_OFF_PERF:Ljava/lang/String; = "MotionCalibrationOff"

.field public static final MOTION_DIALOG_OFF_PERF:Ljava/lang/String; = "MotionDialogOff"

.field public static final MTP_MODE:Ljava/lang/String; = "mtpMode"

.field public static final PEOPLE_VIEW_MODE:Ljava/lang/String; = "peopleViewMode"

.field public static final PHOTO_VIEW_COLCNT:Ljava/lang/String; = "photoViewColCnt"

.field public static final PRIVATE_MOVE_SET_AS_DO_NOT_SHOW:Ljava/lang/String; = "private_move_do_not_show"

.field public static final RECOMMAND_DROPBOX_SHOW_VALUE:Ljava/lang/String; = "recommand_dropbox_show_value"

.field public static final RECOMMAND_DROPBOX_TIME_VALUE:Ljava/lang/String; = "recommand_dropbox_time_value"

.field public static final SCAN_NEARBY_DEVICE_WIFI_ALERT_DIALOG_OFF_PERF:Ljava/lang/String; = "ScanNearbyDeviceWifiDataAlertDialogOff"

.field public static final SEARCH_COLCNT_PORT:Ljava/lang/String; = "searchColPort"

.field public static final SEARCH_COLCNT_WIDE:Ljava/lang/String; = "searchColWide"

.field public static final SEARCH_VIEW_MODE:Ljava/lang/String; = "searchViewMode"

.field public static final SHARE_ACTION_CHECKL_DIALOG_OFF_PERF:Ljava/lang/String; = "ShareActionCheckDialogOff"

.field public static final SORT_BY_TYPE_ALL:Ljava/lang/String; = "sort_by_type_ALL"

.field public static final SORT_BY_TYPE_TIME:Ljava/lang/String; = "sort_by_type_time"

.field public static final SOUND_SHOT_AUTO_PLAY:Ljava/lang/String; = "soundshotautoplay"

.field public static final SYNC_WIFI_ONLY:Ljava/lang/String; = "setting_wifi_only"

.field private static final TAG:Ljava/lang/String; = "SharedPreferenceManager"

.field public static final TAG_BUDDY_PERMISSION_ALERT_DIALOG_OFF_PERF:Ljava/lang/String; = "TagBuddyPermissionAlertDialogOff"

.field public static final THUMBNAIL_VIEW_MODE:Ljava/lang/String; = "thumbnailViewMode"

.field public static final TIME_VIEW_COLCNT:Ljava/lang/String; = "timeViewColCnt"

.field public static final USER_ONCE_SELECTED_DEVICE:Ljava/lang/String; = "user_once_selected_devices"

.field public static final USER_SELECTED_ALBUM:Ljava/lang/String; = "user_selected_album"

.field public static final VIEWBY_PREFERENCE_NAME:Ljava/lang/String; = "ViewByPref"

.field public static final VIEW_BY_FILTER_TYPES_PREF:Ljava/lang/String; = "viewByFilterTypes"

.field public static final WIFI_DISPLAY_ALERT_FOR_GROUP_PLAY:Ljava/lang/String; = "wifi_display_alert_for_group_play"

.field public static final WIFI_DISPLAY_ALERT_FOR_HOTSPOT_PREF:Ljava/lang/String; = "wifi_display_alert_for_hotspot_is_off"

.field public static final WIFI_DISPLAY_ALERT_FOR_POWER_SAVING_ON:Ljava/lang/String; = "wifi_display_alert_for_power_saving_on"

.field public static final WIFI_DISPLAY_ALERT_FOR_SYDESYNC:Ljava/lang/String; = "wifi_display_alert_for_sydesync"

.field public static final WIFI_DISPLAY_ALERT_FOR_WIFIDIRECT_OFF:Ljava/lang/String; = "wifi_display_alert_for_wifi_direct_is_off"

.field public static final WIFI_SYNC_DIALOG_OFF:Ljava/lang/String; = "WifiSyncDialogOff"

.field private static mPreferenceMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final mode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->mPreferenceMap:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBoolean(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "keyString"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 242
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 243
    .local v0, "pref":Landroid/content/SharedPreferences;
    if-nez v0, :cond_0

    .line 246
    :goto_0
    return v1

    .line 245
    :cond_0
    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 246
    .local v1, "value":Z
    goto :goto_0
.end method

.method public static loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "returnValue"    # Z

    .prologue
    .line 204
    :try_start_0
    sget-object v2, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->mPreferenceMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 205
    sget-object v2, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->mPreferenceMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 213
    :goto_0
    return v2

    .line 207
    :cond_0
    const-string v2, "preference_manager"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 208
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p2

    .line 209
    sget-object v2, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->mPreferenceMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :goto_1
    move v2, p2

    .line 213
    goto :goto_0

    .line 210
    :catch_0
    move-exception v0

    .line 211
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "returnValue"    # I

    .prologue
    .line 171
    const/4 v1, 0x0

    .line 173
    .local v1, "key":I
    :try_start_0
    sget-object v3, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->mPreferenceMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 174
    sget-object v3, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->mPreferenceMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 183
    :goto_0
    return v3

    .line 176
    :cond_0
    const-string v3, "preference_manager"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 177
    .local v2, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2, p1, p2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 178
    sget-object v3, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->mPreferenceMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :goto_1
    move v3, v1

    .line 183
    goto :goto_0

    .line 179
    :catch_0
    move-exception v0

    .line 180
    .local v0, "e":Ljava/lang/Exception;
    move v1, p2

    .line 181
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static loadLongKey(Landroid/content/Context;Ljava/lang/String;J)J
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "returnValue"    # J

    .prologue
    .line 187
    const-wide/16 v2, 0x0

    .line 189
    .local v2, "key":J
    :try_start_0
    sget-object v4, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->mPreferenceMap:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 190
    sget-object v4, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->mPreferenceMap:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 199
    :goto_0
    return-wide v4

    .line 192
    :cond_0
    const-string v4, "preference_manager"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 193
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1, p1, p2, p3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 194
    sget-object v4, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->mPreferenceMap:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, p1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :goto_1
    move-wide v4, v2

    .line 199
    goto :goto_0

    .line 195
    :catch_0
    move-exception v0

    .line 196
    .local v0, "e":Ljava/lang/Exception;
    move-wide v2, p2

    .line 197
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static loadStringKey(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "keyString"    # Ljava/lang/String;

    .prologue
    .line 141
    const-string v1, ""

    .line 143
    .local v1, "key":Ljava/lang/String;
    :try_start_0
    sget-object v3, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->mPreferenceMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 144
    sget-object v3, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->mPreferenceMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 152
    :goto_0
    return-object v3

    .line 146
    :cond_0
    const-string v3, "preference_manager"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 147
    .local v2, "pref":Landroid/content/SharedPreferences;
    const-string v3, ""

    invoke-interface {v2, p1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 148
    sget-object v3, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->mPreferenceMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :goto_1
    move-object v3, v1

    .line 152
    goto :goto_0

    .line 149
    :catch_0
    move-exception v0

    .line 150
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static loadStringSetKey(Landroid/content/Context;Ljava/lang/String;)Ljava/util/Set;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "keyString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156
    const/4 v1, 0x0

    .line 158
    .local v1, "key":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :try_start_0
    sget-object v3, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->mPreferenceMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 159
    sget-object v3, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->mPreferenceMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Set;

    .line 167
    :goto_0
    return-object v3

    .line 161
    :cond_0
    const-string v3, "preference_manager"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 162
    .local v2, "pref":Landroid/content/SharedPreferences;
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v2, p1, v3}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    .line 163
    sget-object v3, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->mPreferenceMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :goto_1
    move-object v3, v1

    .line 167
    goto :goto_0

    .line 164
    :catch_0
    move-exception v0

    .line 165
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static removeKey(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "keyString"    # Ljava/lang/String;

    .prologue
    .line 232
    :try_start_0
    const-string v3, "preference_manager"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 233
    .local v2, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 234
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v1, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 235
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 239
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-void

    .line 236
    :catch_0
    move-exception v0

    .line 237
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static saveBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "key"    # Z

    .prologue
    .line 218
    :try_start_0
    const-string v3, "preference_manager"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 219
    .local v2, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 220
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v1, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 221
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 222
    sget-object v3, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->mPreferenceMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 228
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-void

    .line 225
    :catch_0
    move-exception v0

    .line 226
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static saveState(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "key"    # I

    .prologue
    .line 85
    :try_start_0
    const-string v3, "preference_manager"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 86
    .local v2, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 87
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v1, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 88
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 89
    sget-object v3, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->mPreferenceMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-void

    .line 91
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "SharedPreferenceManager"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static saveState(Landroid/content/Context;Ljava/lang/String;J)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "key"    # J

    .prologue
    .line 98
    :try_start_0
    const-string v3, "preference_manager"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 99
    .local v2, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 100
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v1, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 101
    invoke-interface {v1, p1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 102
    sget-object v3, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->mPreferenceMap:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-void

    .line 104
    :catch_0
    move-exception v0

    .line 105
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static saveState(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 111
    :try_start_0
    const-string v3, "preference_manager"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 112
    .local v2, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 113
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v1, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 114
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 115
    sget-object v3, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->mPreferenceMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-void

    .line 117
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static saveState(Landroid/content/Context;Ljava/lang/String;Ljava/util/Set;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "keyString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 123
    .local p2, "key":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v2, "preference_manager"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 124
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 125
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 126
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 127
    sget-object v2, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->mPreferenceMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 129
    return-void
.end method

.method public static saveState(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "key"    # Z

    .prologue
    .line 132
    const-string v2, "preference_manager"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 133
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 134
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 135
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 136
    sget-object v2, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->mPreferenceMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 138
    return-void
.end method
