.class Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$2;
.super Landroid/os/Handler;
.source "DrawerGroupFilterBy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$2;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 104
    iget v1, p1, Landroid/os/Message;->what:I

    if-nez v1, :cond_2

    const/4 v0, 0x0

    .line 105
    .local v0, "checkAllFilter":Z
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$2;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->updateFilterByItems(Z)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$2;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    # invokes: Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->isNeedToUpdateFilter()Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->access$300(Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 106
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$2;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterByHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->access$400(Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 108
    :cond_1
    return-void

    .line 104
    .end local v0    # "checkAllFilter":Z
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
