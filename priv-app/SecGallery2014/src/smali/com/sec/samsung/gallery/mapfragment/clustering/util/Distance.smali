.class public Lcom/sec/samsung/gallery/mapfragment/clustering/util/Distance;
.super Ljava/lang/Object;
.source "Distance.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static from(DDDD)D
    .locals 8
    .param p0, "x1"    # D
    .param p2, "y1"    # D
    .param p4, "x2"    # D
    .param p6, "y2"    # D

    .prologue
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 19
    sub-double v4, p0, p4

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    .line 20
    .local v0, "a":D
    sub-double v4, p2, p6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    .line 21
    .local v2, "b":D
    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    return-wide v4
.end method

.method public static from(Landroid/graphics/Point;Landroid/graphics/Point;)D
    .locals 8
    .param p0, "a"    # Landroid/graphics/Point;
    .param p1, "b"    # Landroid/graphics/Point;

    .prologue
    .line 33
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 34
    iget v0, p0, Landroid/graphics/Point;->x:I

    int-to-double v0, v0

    iget v2, p0, Landroid/graphics/Point;->y:I

    int-to-double v2, v2

    iget v4, p1, Landroid/graphics/Point;->x:I

    int-to-double v4, v4

    iget v6, p1, Landroid/graphics/Point;->y:I

    int-to-double v6, v6

    invoke-static/range {v0 .. v7}, Lcom/sec/samsung/gallery/mapfragment/clustering/util/Distance;->from(DDDD)D

    move-result-wide v0

    .line 36
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method
