.class public Lcom/sec/samsung/gallery/decoder/XIVInterface;
.super Ljava/lang/Object;
.source "XIVInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/decoder/XIVInterface$BufferReusableTexture;
    }
.end annotation


# static fields
.field public static final BUFFER_TYPE_MTN:I = 0x2

.field public static final LARGETHUMBNAIL_CENTER:I = 0x1

.field public static final LARGETHUMBNAIL_NEXT:I = 0x2

.field public static final LARGETHUMBNAIL_PREV:I


# instance fields
.field private mXiv:Lcom/quramsoft/xiv/XIV;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-string v0, "XIVGallery2014"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    invoke-static {p0}, Lcom/quramsoft/xiv/XIV;->create(Lcom/sec/samsung/gallery/decoder/XIVInterface;)Lcom/quramsoft/xiv/XIV;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    .line 82
    return-void
.end method

.method public static AlphablendingAnimationCreate(FFF)Ljava/lang/Object;
    .locals 1
    .param p0, "start"    # F
    .param p1, "end"    # F
    .param p2, "duration"    # F

    .prologue
    .line 381
    invoke-static {p0, p1, p2}, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->create(FFF)Lcom/quramsoft/xiv/XIVAlphablendingAnimation;

    move-result-object v0

    return-object v0
.end method

.method public static AlphablendingAnimationDrawBlended(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Landroid/graphics/RectF;Landroid/graphics/RectF;F)V
    .locals 0
    .param p0, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p1, "texture"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p2, "source"    # Landroid/graphics/RectF;
    .param p3, "target"    # Landroid/graphics/RectF;
    .param p4, "alpha"    # F

    .prologue
    .line 394
    invoke-static {p0, p1, p2, p3, p4}, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->drawBlended(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Landroid/graphics/RectF;Landroid/graphics/RectF;F)V

    .line 395
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/decoder/XIVInterface;)Lcom/quramsoft/xiv/XIV;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/decoder/XIVInterface;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    return-object v0
.end method

.method public static alphaTransform(Ljavax/microedition/khronos/opengles/GL11;Landroid/graphics/RectF;[F)V
    .locals 0
    .param p0, "gl"    # Ljavax/microedition/khronos/opengles/GL11;
    .param p1, "rect"    # Landroid/graphics/RectF;
    .param p2, "matrix"    # [F

    .prologue
    .line 201
    invoke-static {p0, p1, p2}, Lcom/quramsoft/xiv/XIVUtils;->alphaTransform(Ljavax/microedition/khronos/opengles/GL11;Landroid/graphics/RectF;[F)V

    .line 202
    return-void
.end method

.method public static cancelAllNewInstances()V
    .locals 0

    .prologue
    .line 581
    invoke-static {}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->cancelAllNewInstances()V

    .line 582
    return-void
.end method

.method public static create()Lcom/sec/samsung/gallery/decoder/XIVInterface;
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/XIVInterface;-><init>()V

    return-object v0
.end method

.method public static createDualCacheService(Landroid/content/Context;Ljava/lang/String;III)Lcom/sec/android/gallery3d/common/BlobCache;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cacheName"    # Ljava/lang/String;
    .param p2, "maxEntries"    # I
    .param p3, "maxBytes"    # I
    .param p4, "version"    # I

    .prologue
    .line 472
    invoke-static {p0, p1, p2, p3, p4}, Lcom/quramsoft/xiv/XIVDualCacheService;->create(Landroid/content/Context;Ljava/lang/String;III)Lcom/sec/android/gallery3d/common/BlobCache;

    move-result-object v0

    return-object v0
.end method

.method public static cropThumbnailIfNeeded(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "type"    # I

    .prologue
    .line 151
    invoke-static {p0, p1}, Lcom/quramsoft/xiv/XIVUtils;->cropThumbnailIfNeeded(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "opts"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 627
    invoke-static {p0, p1, p2, p3}, Lcom/quramsoft/xiv/XIVCacheManager;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;IZ)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p2, "targetSize"    # I
    .param p3, "isLarger"    # Z

    .prologue
    .line 147
    invoke-static {p0, p1, p2, p3}, Lcom/quramsoft/xiv/XIVCacheManager;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static getAlphablendingAnimationTimeFromLTNToRD()F
    .locals 1

    .prologue
    .line 407
    invoke-static {}, Lcom/quramsoft/xiv/XIVDefinedValues;->getAlphablendingAnimationTimeFromLTNToRD()F

    move-result v0

    return v0
.end method

.method public static getCacheBitmap(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/MediaItem;I)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "type"    # I

    .prologue
    .line 155
    invoke-static {p0, p1, p2}, Lcom/quramsoft/xiv/XIVCacheManager;->getCacheBitmap(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/android/gallery3d/data/MediaItem;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static getCacheBuffer(Lcom/sec/samsung/gallery/decoder/XIVInterface;Lcom/sec/android/gallery3d/data/MediaItem;I)Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;
    .locals 2
    .param p0, "xivInterface"    # Lcom/sec/samsung/gallery/decoder/XIVInterface;
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "type"    # I

    .prologue
    .line 505
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-static {v1, p1, p2}, Lcom/quramsoft/xiv/XIVCacheManager;->getCacheBuffer(Lcom/quramsoft/xiv/XIV;Lcom/sec/android/gallery3d/data/MediaItem;I)Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    move-result-object v0

    .line 507
    .local v0, "output":Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;
    return-object v0
.end method

.method public static getCropBitmap(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;Lcom/sec/android/gallery3d/data/MediaItem;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p1, "buffer"    # Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 513
    invoke-static {p0, p1, p2}, Lcom/quramsoft/xiv/XIVCacheManager;->getCropBitmap(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Lcom/sec/android/gallery3d/data/BytesBufferPool$BytesBuffer;Lcom/sec/android/gallery3d/data/MediaItem;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 515
    .local v0, "output":Landroid/graphics/Bitmap;
    return-object v0
.end method

.method public static getDelayTimeForRD()I
    .locals 1

    .prologue
    .line 257
    invoke-static {}, Lcom/quramsoft/xiv/XIVDefinedValues;->getDelayTimeForRD()I

    move-result v0

    return v0
.end method

.method public static getMTNCachingSize()I
    .locals 1

    .prologue
    .line 524
    invoke-static {}, Lcom/quramsoft/xiv/XIVDefinedValues;->getMTNCachingSize()I

    move-result v0

    return v0
.end method

.method public static getMicroCacheFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 464
    invoke-static {}, Lcom/quramsoft/xiv/XIVDefinedValues;->getMicroCacheFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getMiniCacheFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 468
    invoke-static {}, Lcom/quramsoft/xiv/XIVDefinedValues;->getMiniCacheFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSleepTimeUnitForRD()I
    .locals 1

    .prologue
    .line 261
    invoke-static {}, Lcom/quramsoft/xiv/XIVDefinedValues;->getSleepTimeUnitForRD()I

    move-result v0

    return v0
.end method

.method public static getTextureSizeFromImageSize(I)I
    .locals 1
    .param p0, "size"    # I

    .prologue
    .line 197
    invoke-static {p0}, Lcom/quramsoft/xiv/XIVUtils;->getTextureSizeFromImageSize(I)I

    move-result v0

    return v0
.end method

.method public static isLocalImage(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 1
    .param p0, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 496
    invoke-static {p0}, Lcom/quramsoft/xiv/XIVUtils;->isLocalImage(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v0

    return v0
.end method

.method public static isUseAlphablendingAnimationFromLTNToRD()Z
    .locals 1

    .prologue
    .line 403
    invoke-static {}, Lcom/quramsoft/xiv/XIVConfig;->isUseAlphablendingAnimationFromLTNToRD()Z

    move-result v0

    return v0
.end method

.method public static isUseBufferReuseTexture()Z
    .locals 1

    .prologue
    .line 482
    invoke-static {}, Lcom/quramsoft/xiv/XIVConfig;->isUseBufferReuseTexture()Z

    move-result v0

    return v0
.end method

.method public static isUseBufferThumbnailMode()Z
    .locals 1

    .prologue
    .line 487
    invoke-static {}, Lcom/quramsoft/xiv/XIVConfig;->isUseBufferThumbnailMode()Z

    move-result v0

    return v0
.end method

.method public static isUseCenterFirstRD()Z
    .locals 1

    .prologue
    .line 205
    invoke-static {}, Lcom/quramsoft/xiv/XIVConfig;->isUseCenterFirstRD()Z

    move-result v0

    return v0
.end method

.method public static isUseDelayDecoding()Z
    .locals 1

    .prologue
    .line 249
    invoke-static {}, Lcom/quramsoft/xiv/XIVConfig;->isUseDelayDecoding()Z

    move-result v0

    return v0
.end method

.method public static isUseFastLargeThumbnail()Z
    .locals 1

    .prologue
    .line 164
    invoke-static {}, Lcom/quramsoft/xiv/XIVConfig;->isUseFastLargeThumbnail()Z

    move-result v0

    return v0
.end method

.method public static isUseImageFitTexture()Z
    .locals 1

    .prologue
    .line 193
    invoke-static {}, Lcom/quramsoft/xiv/XIVConfig;->isUseImageFitTexture()Z

    move-result v0

    return v0
.end method

.method public static isUseLargeThumbnail()Z
    .locals 1

    .prologue
    .line 478
    invoke-static {}, Lcom/quramsoft/xiv/XIVConfig;->isUseLargeThumbnail()Z

    move-result v0

    return v0
.end method

.method public static final isUseRDEx()Z
    .locals 1

    .prologue
    .line 555
    invoke-static {}, Lcom/quramsoft/xiv/XIVConfig;->isUseRDEX()Z

    move-result v0

    return v0
.end method

.method public static isUseTileReuseMode()Z
    .locals 1

    .prologue
    .line 238
    invoke-static {}, Lcom/quramsoft/xiv/XIVConfig;->isUseTileReuseMode()Z

    move-result v0

    return v0
.end method

.method public static isUseWSTN()Z
    .locals 1

    .prologue
    .line 492
    invoke-static {}, Lcom/quramsoft/xiv/XIVConfig;->isUseWSTN()Z

    move-result v0

    return v0
.end method

.method public static loadLibrary()V
    .locals 1

    .prologue
    .line 90
    const-string v0, "XIVGallery2014"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 91
    return-void
.end method

.method public static newInstance(Ljava/io/FileDescriptor;Z)Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    .locals 1
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .param p1, "isShareable"    # Z

    .prologue
    .line 537
    invoke-static {p0, p1}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->newInstance(Ljava/io/FileDescriptor;Z)Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance(Ljava/io/InputStream;Z)Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    .locals 1
    .param p0, "is"    # Ljava/io/InputStream;
    .param p1, "isShareable"    # Z

    .prologue
    .line 533
    invoke-static {p0, p1}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->newInstance(Ljava/io/InputStream;Z)Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;Z)Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    .locals 1
    .param p0, "pathName"    # Ljava/lang/String;
    .param p1, "isShareable"    # Z

    .prologue
    .line 529
    invoke-static {p0, p1}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->newInstance(Ljava/lang/String;Z)Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance([BIIZ)Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    .locals 1
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "isShareable"    # Z

    .prologue
    .line 541
    invoke-static {p0, p1, p2, p3}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->newInstance([BIIZ)Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    move-result-object v0

    return-object v0
.end method

.method public static requestFullImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;Lcom/sec/android/gallery3d/app/PhotoDataAdapter;ILcom/quramsoft/xiv/XIVBitmapRegionDecoder;)V
    .locals 2
    .param p0, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p1, "pda"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    .param p2, "index"    # I
    .param p3, "qrd"    # Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    .prologue
    .line 615
    if-nez p1, :cond_0

    .line 624
    :goto_0
    return-void

    .line 618
    :cond_0
    if-eqz p3, :cond_1

    .line 619
    iget-object v1, p1, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->mDecoderInterface:Lcom/sec/samsung/gallery/decoder/DecoderInterface;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->getXIVInterface()Lcom/sec/samsung/gallery/decoder/XIVInterface;

    move-result-object v1

    invoke-static {p3, v1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->newInstance(Ljava/lang/Object;Lcom/sec/samsung/gallery/decoder/XIVInterface;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    move-result-object v0

    .line 620
    .local v0, "rd":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    invoke-virtual {p1, p0, p2, v0}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->requestFullImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;ILcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    goto :goto_0

    .line 622
    .end local v0    # "rd":Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p1, p0, p2, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->requestFullImage(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;ILcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    goto :goto_0
.end method

.method public static final setTileDecSize(I)V
    .locals 0
    .param p0, "size"    # I

    .prologue
    .line 655
    invoke-static {p0}, Lcom/quramsoft/xiv/XIVDefinedValues;->setTileDecSize(I)V

    .line 656
    return-void
.end method

.method private setXivFeature()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 112
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableUseXivLargeThumbnail:Z

    if-eqz v0, :cond_0

    .line 113
    invoke-static {v1}, Lcom/quramsoft/xiv/XIVConfig;->setUseLargeThumbnail(Z)V

    .line 116
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableUseResizeLargeThumbnail:Z

    if-eqz v0, :cond_1

    .line 117
    invoke-static {v1}, Lcom/quramsoft/xiv/XIVConfig;->setUseResizeLargeThumbnail(Z)V

    .line 120
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getPhotoDataAdapter()Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 121
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getPhotoDataAdapter()Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-result-object v0

    invoke-static {}, Lcom/quramsoft/xiv/XIVConfig;->isUseLargeThumbnail()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->setImageFetchSeq(Z)V

    .line 123
    :cond_2
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXivStopRDWhileZoomin:Z

    invoke-static {v0}, Lcom/quramsoft/xiv/XIVConfig;->setUseStopRDWhileZooming(Z)V

    .line 125
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXivWSTN:Z

    invoke-static {v0}, Lcom/quramsoft/xiv/XIVConfig;->setUseWSTN(Z)V

    .line 126
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXivTileReuse:Z

    invoke-static {v0}, Lcom/quramsoft/xiv/XIVConfig;->setUseTileReuseMode(Z)V

    .line 127
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXivFilledReusePoolMode:Z

    invoke-static {v0}, Lcom/quramsoft/xiv/XIVConfig;->setUseFilledReusePoolMode(Z)V

    .line 128
    return-void
.end method

.method public static waitForAnimationOnPhotoView(Lcom/sec/samsung/gallery/decoder/XIVInterface;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;IJ)Z
    .locals 1
    .param p0, "xivInterface"    # Lcom/sec/samsung/gallery/decoder/XIVInterface;
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;
    .param p2, "delayTime"    # I
    .param p3, "sleepTime"    # J

    .prologue
    .line 253
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-static {v0, p1, p2, p3, p4}, Lcom/quramsoft/xiv/XIVUtils;->waitForAnimationOnPhotoView(Lcom/quramsoft/xiv/XIV;Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;IJ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public AlphablendingAnimationGetProgress(Ljava/lang/Object;)F
    .locals 1
    .param p1, "instance"    # Ljava/lang/Object;

    .prologue
    .line 399
    check-cast p1, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;

    .end local p1    # "instance":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->getProgress()F

    move-result v0

    return v0
.end method

.method public AlphablendingAnimationNeedToAnimate(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "instance"    # Ljava/lang/Object;

    .prologue
    .line 389
    check-cast p1, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;

    .end local p1    # "instance":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->needToAnimate()Z

    move-result v0

    return v0
.end method

.method public AlphablendingAnimationReset(Ljava/lang/Object;)V
    .locals 0
    .param p1, "instance"    # Ljava/lang/Object;

    .prologue
    .line 385
    check-cast p1, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;

    .end local p1    # "instance":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/quramsoft/xiv/XIVAlphablendingAnimation;->reset()V

    .line 386
    return-void
.end method

.method public BitmapReuseFreeBitmap(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 242
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v1}, Lcom/quramsoft/xiv/XIV;->getBitmapReuseManager()Lcom/quramsoft/xiv/XIVBitmapReuseManager;

    move-result-object v0

    .line 243
    .local v0, "brm":Lcom/quramsoft/xiv/XIVBitmapReuseManager;
    if-eqz v0, :cond_0

    .line 244
    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1}, Lcom/quramsoft/xiv/XIVBitmapReuseManager;->freeBitmap(ILandroid/graphics/Bitmap;)V

    .line 246
    :cond_0
    return-void
.end method

.method public checkAnimation(Z)V
    .locals 1
    .param p1, "isMoreAnim"    # Z

    .prologue
    .line 353
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0, p1}, Lcom/quramsoft/xiv/XIV;->checkAnimation(Z)V

    .line 354
    return-void
.end method

.method public checkDecodingNoComplete()V
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getTileManager()Lcom/quramsoft/xiv/XIVTileManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVTileManager;->checkDecodingNoComplete()V

    .line 439
    return-void
.end method

.method public checkDecodingTime(J)V
    .locals 1
    .param p1, "startTime"    # J

    .prologue
    .line 442
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getTileManager()Lcom/quramsoft/xiv/XIVTileManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/quramsoft/xiv/XIVTileManager;->checkDecodingTime(J)V

    .line 443
    return-void
.end method

.method public checkFlingVelocity(FF)V
    .locals 1
    .param p1, "vX"    # F
    .param p2, "vY"    # F

    .prologue
    .line 376
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getAnimationManager()Lcom/quramsoft/xiv/XIVAnimationManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/quramsoft/xiv/XIVAnimationManager;->checkFlingVelocity(FF)V

    .line 377
    return-void
.end method

.method public checkSliding()V
    .locals 1

    .prologue
    .line 500
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getSlidingSpeedManager()Lcom/quramsoft/xiv/XIVSlidingSpeedManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->checkSliding()V

    .line 501
    return-void
.end method

.method public createAllOnCurrent()V
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getLargeThumbnailManager()Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->createAllOnCurrent()V

    .line 183
    :cond_0
    return-void
.end method

.method public createCenterLargeThumbnail(Lcom/sec/android/gallery3d/data/MediaItem;I)V
    .locals 1
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "index"    # I

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getLargeThumbnailManager()Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->createCenterLargeThumbnail(Lcom/sec/android/gallery3d/data/MediaItem;I)V

    .line 171
    :cond_0
    return-void
.end method

.method public decodeRegion(Ljava/lang/Object;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "regionDecoder"    # Ljava/lang/Object;
    .param p2, "rect"    # Landroid/graphics/Rect;
    .param p3, "options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 546
    const/4 v0, 0x0

    .line 547
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/quramsoft/xiv/XIVConfig;->isUseTileReuseMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 548
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v1}, Lcom/quramsoft/xiv/XIV;->getBitmapReuseManager()Lcom/quramsoft/xiv/XIVBitmapReuseManager;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/quramsoft/xiv/XIVBitmapReuseManager;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 551
    :cond_0
    check-cast p1, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    .end local p1    # "regionDecoder":Ljava/lang/Object;
    invoke-virtual {p1, v0, p2, p3}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->decodeRegion(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method public decodeRegionEx(Ljava/lang/Object;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "regionDecoder"    # Ljava/lang/Object;
    .param p2, "region"    # Landroid/graphics/Rect;
    .param p3, "intersectRect"    # Landroid/graphics/Rect;
    .param p4, "options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    const/16 v3, 0x200

    .line 559
    const/4 v0, 0x0

    .line 560
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/quramsoft/xiv/XIVConfig;->isUseTileReuseMode()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 561
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v1}, Lcom/quramsoft/xiv/XIV;->getBitmapReuseManager()Lcom/quramsoft/xiv/XIVBitmapReuseManager;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/quramsoft/xiv/XIVBitmapReuseManager;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 566
    :goto_0
    invoke-static {}, Lcom/quramsoft/xiv/XIVConfig;->isUseRDEX()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 567
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v1

    if-nez v1, :cond_3

    .line 568
    :cond_0
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v3, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 572
    :cond_1
    :goto_1
    check-cast p1, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    .end local p1    # "regionDecoder":Ljava/lang/Object;
    invoke-virtual {p1, v0, p2, p4}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->decodeRegionEx(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 574
    :goto_2
    return-object v1

    .line 563
    .restart local p1    # "regionDecoder":Ljava/lang/Object;
    :cond_2
    iget-object v0, p4, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 569
    :cond_3
    if-eq p3, p2, :cond_1

    .line 570
    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    goto :goto_1

    .line 574
    :cond_4
    check-cast p1, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    .end local p1    # "regionDecoder":Ljava/lang/Object;
    invoke-virtual {p1, v0, p2, p4}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->decodeRegion(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_2
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/quramsoft/xiv/XIV;->processTouchEvent(I)V

    .line 132
    return-void
.end method

.method public drawOnCenter(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIIIFFLcom/sec/android/gallery3d/ui/ScreenNail;)Z
    .locals 9
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "offsetX"    # I
    .param p3, "offsetY"    # I
    .param p4, "imageWidth"    # I
    .param p5, "imageHeight"    # I
    .param p6, "rotation"    # F
    .param p7, "scale"    # F
    .param p8, "backUp"    # Lcom/sec/android/gallery3d/ui/ScreenNail;

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getLargeThumbnailManager()Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    move-result-object v0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->drawOnCenter(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIIIFFLcom/sec/android/gallery3d/ui/ScreenNail;)Z

    move-result v0

    return v0
.end method

.method public drawOnScreenNail(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;ZLcom/sec/android/gallery3d/ui/ScreenNail;IIFFF)V
    .locals 9
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "isNext"    # Z
    .param p3, "backUp"    # Lcom/sec/android/gallery3d/ui/ScreenNail;
    .param p4, "x"    # I
    .param p5, "y"    # I
    .param p6, "drawWidth"    # F
    .param p7, "drawHeight"    # F
    .param p8, "rotation"    # F

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getLargeThumbnailManager()Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    move-result-object v0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->drawOnScreenNail(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;ZLcom/sec/android/gallery3d/ui/ScreenNail;IIFFF)V

    .line 189
    return-void
.end method

.method public drawTiles(Lcom/sec/android/gallery3d/ui/TileImageView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;IIFII)V
    .locals 9
    .param p1, "tiv"    # Lcom/sec/android/gallery3d/ui/TileImageView;
    .param p2, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p3, "r"    # Landroid/graphics/Rect;
    .param p4, "level"    # I
    .param p5, "size"    # I
    .param p6, "length"    # F
    .param p7, "offsetX"    # I
    .param p8, "offsetY"    # I

    .prologue
    .line 429
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getTileManager()Lcom/quramsoft/xiv/XIVTileManager;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcom/quramsoft/xiv/XIVTileManager;->drawTiles(Lcom/sec/android/gallery3d/ui/TileImageView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;IIFII)V

    .line 430
    return-void
.end method

.method public freeBuffer(ILjava/nio/ByteBuffer;)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "buffer"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 520
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getBufferReuseManager()Lcom/quramsoft/xiv/XIVBufferReuseManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/quramsoft/xiv/XIVBufferReuseManager;->freeBuffer(ILjava/nio/ByteBuffer;)V

    .line 521
    return-void
.end method

.method public getAnimationDuration(II)I
    .locals 3
    .param p1, "kind"    # I
    .param p2, "originalTime"    # I

    .prologue
    .line 357
    packed-switch p1, :pswitch_data_0

    .line 363
    .end local p2    # "originalTime":I
    :goto_0
    :pswitch_0
    return p2

    .line 359
    .restart local p2    # "originalTime":I
    :pswitch_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getAnimationManager()Lcom/quramsoft/xiv/XIVAnimationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    int-to-float v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/quramsoft/xiv/XIVAnimationManager;->getScrollAnimationTime(Lcom/quramsoft/xiv/XIV;F)F

    move-result v0

    float-to-int p2, v0

    goto :goto_0

    .line 361
    :pswitch_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getAnimationManager()Lcom/quramsoft/xiv/XIVAnimationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    int-to-float v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/quramsoft/xiv/XIVAnimationManager;->getSlideAnimationTime(Lcom/quramsoft/xiv/XIV;F)F

    move-result v0

    float-to-int p2, v0

    goto :goto_0

    .line 357
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public getBuffer(II)Ljava/nio/ByteBuffer;
    .locals 1
    .param p1, "size"    # I
    .param p2, "type"    # I

    .prologue
    .line 639
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getBufferReuseManager()Lcom/quramsoft/xiv/XIVBufferReuseManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/quramsoft/xiv/XIVBufferReuseManager;->getBuffer(II)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentPhotoIndex()I
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getCurrentPhotoIndex()I

    move-result v0

    return v0
.end method

.method public getHeight(Ljava/lang/Object;)I
    .locals 1
    .param p1, "instance"    # Ljava/lang/Object;

    .prologue
    .line 585
    check-cast p1, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    .end local p1    # "instance":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->getHeight()I

    move-result v0

    return v0
.end method

.method public getImageBufferDataFromBitmap(Landroid/graphics/Bitmap;Ljava/nio/ByteBuffer;)Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "buffer"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 643
    invoke-static {p1, p2}, Lcom/quramsoft/xiv/XIVCacheManager;->getImageBufferDataFromBitmap(Landroid/graphics/Bitmap;Ljava/nio/ByteBuffer;)Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    move-result-object v0

    .line 645
    .local v0, "data":Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;

    invoke-direct {v1, v0}, Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;-><init>(Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;)V

    goto :goto_0
.end method

.method public getProgress(FFFI)F
    .locals 1
    .param p1, "fromX"    # F
    .param p2, "toX"    # F
    .param p3, "progress"    # F
    .param p4, "animKind"    # I

    .prologue
    .line 368
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getAnimationManager()Lcom/quramsoft/xiv/XIVAnimationManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/quramsoft/xiv/XIVAnimationManager;->getProgress(FFFI)F

    move-result v0

    return v0
.end method

.method public getWidth(Ljava/lang/Object;)I
    .locals 1
    .param p1, "instance"    # Ljava/lang/Object;

    .prologue
    .line 589
    check-cast p1, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    .end local p1    # "instance":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->getWidth()I

    move-result v0

    return v0
.end method

.method public hasCache(Lcom/sec/android/gallery3d/data/MediaItem;I)Z
    .locals 1
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "type"    # I

    .prologue
    .line 635
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-static {v0, p1, p2}, Lcom/quramsoft/xiv/XIVCacheManager;->hasCache(Lcom/quramsoft/xiv/XIV;Lcom/sec/android/gallery3d/data/MediaItem;I)Z

    move-result v0

    return v0
.end method

.method public initXIV(Landroid/view/WindowManager;Lcom/sec/android/gallery3d/app/GalleryApp;)V
    .locals 3
    .param p1, "windowManager"    # Landroid/view/WindowManager;
    .param p2, "galleryApp"    # Lcom/sec/android/gallery3d/app/GalleryApp;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    if-eqz v0, :cond_0

    .line 99
    invoke-direct {p0}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->setXivFeature()V

    .line 100
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0, p1}, Lcom/quramsoft/xiv/XIV;->setWidthOfView(Landroid/view/WindowManager;)V

    .line 101
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0, p2}, Lcom/quramsoft/xiv/XIV;->setGalleryApp(Lcom/sec/android/gallery3d/app/GalleryApp;)V

    .line 103
    const/16 v0, 0x1c2

    const/16 v1, 0x96

    const v2, 0x1e8480

    invoke-static {v0, v1, v2}, Lcom/quramsoft/xiv/XIVDefinedValues;->setSlidingAnimationFactors(III)V

    .line 108
    :goto_0
    return-void

    .line 106
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    goto :goto_0
.end method

.method public isAllowedUpdateContent()Z
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getTileManager()Lcom/quramsoft/xiv/XIVTileManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVTileManager;->isAllowedUpdateContent()Z

    move-result v0

    return v0
.end method

.method public isAnimating()Z
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getImageRotationAnimationManager()Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->isAnimating()Z

    move-result v0

    return v0
.end method

.method public isBeingSliding()Z
    .locals 4

    .prologue
    .line 631
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getSlidingSpeedManager()Lcom/quramsoft/xiv/XIVSlidingSpeedManager;

    move-result-object v0

    invoke-static {}, Lcom/quramsoft/xiv/XIVDefinedValues;->getSlidingDurationLimit()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/quramsoft/xiv/XIVSlidingSpeedManager;->isDurationUnderLimit(J)Z

    move-result v0

    return v0
.end method

.method public isDoingDecoding()Z
    .locals 2

    .prologue
    .line 419
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v1}, Lcom/quramsoft/xiv/XIV;->getTileManager()Lcom/quramsoft/xiv/XIVTileManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/quramsoft/xiv/XIVTileManager;->getCurrentRegionDecoder()Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    move-result-object v0

    .line 420
    .local v0, "decoder":Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;
    if-eqz v0, :cond_0

    .line 421
    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->isDoingDecoding()Z

    move-result v1

    .line 423
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isFilmMode()Z
    .locals 1

    .prologue
    .line 649
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->isFilmMode()Z

    move-result v0

    return v0
.end method

.method public isMinLevel(FII)Z
    .locals 1
    .param p1, "currentScale"    # F
    .param p2, "imageWidth"    # I
    .param p3, "backUpWidth"    # I

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getLargeThumbnailManager()Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->isMinLevel(FII)Z

    move-result v0

    return v0
.end method

.method public isMultiViewState()Z
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->isMultiViewState()Z

    move-result v0

    return v0
.end method

.method public final isRecycled(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "instance"    # Ljava/lang/Object;

    .prologue
    .line 593
    check-cast p1, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    .end local p1    # "instance":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->isRecycled()Z

    move-result v0

    return v0
.end method

.method public isSupportedRegionDecoding(Ljava/lang/String;)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 143
    invoke-static {p1}, Lcom/quramsoft/xiv/XIVUtils;->isSupportedRegionDecoding(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isUseXIVAnimation(I)Z
    .locals 1
    .param p1, "animKind"    # I

    .prologue
    .line 372
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getAnimationManager()Lcom/quramsoft/xiv/XIVAnimationManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/quramsoft/xiv/XIVAnimationManager;->isUseXIVAnimation(I)Z

    move-result v0

    return v0
.end method

.method public isZoomState()Z
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->isZoomState()Z

    move-result v0

    return v0
.end method

.method public moveTo(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 312
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0, p1}, Lcom/quramsoft/xiv/XIV;->moveTo(I)V

    .line 313
    return-void
.end method

.method public onDoubleTapZoomBegin()V
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->onDoubleTapZoomBegin()V

    .line 289
    return-void
.end method

.method public onDoubleTapZoomEnd()V
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->onDoubleTapZoomEnd()V

    .line 293
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 136
    return-void
.end method

.method public onPhotoView()Z
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->onPhotoView()Z

    move-result v0

    return v0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 140
    return-void
.end method

.method public onScale(F)V
    .locals 1
    .param p1, "scale"    # F

    .prologue
    .line 296
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0, p1}, Lcom/quramsoft/xiv/XIV;->onScale(F)V

    .line 297
    return-void
.end method

.method public onScaleBegin(F)V
    .locals 1
    .param p1, "scale"    # F

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0, p1}, Lcom/quramsoft/xiv/XIV;->onScaleBegin(F)V

    .line 301
    return-void
.end method

.method public onScaleEnd()V
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->onScaleEnd()V

    .line 305
    return-void
.end method

.method public postRender(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Z)V
    .locals 1
    .param p1, "photoView"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p2, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p3, "isActive"    # Z

    .prologue
    .line 283
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getImageRotationAnimationManager()Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->postRender(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Z)V

    .line 284
    return-void
.end method

.method public preRender(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Z
    .locals 1
    .param p1, "photoView"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p2, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getImageRotationAnimationManager()Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->preRender(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Z

    move-result v0

    return v0
.end method

.method public prepareAnimation(Lcom/sec/android/gallery3d/ui/PhotoView;ZLjava/lang/Object;)V
    .locals 1
    .param p1, "view"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p2, "isCW"    # Z
    .param p3, "cb"    # Ljava/lang/Object;

    .prologue
    .line 266
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getImageRotationAnimationManager()Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;

    move-result-object v0

    check-cast p3, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationCallback;

    .end local p3    # "cb":Ljava/lang/Object;
    invoke-virtual {v0, p1, p2, p3}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->prepareAnimation(Lcom/sec/android/gallery3d/ui/PhotoView;ZLcom/quramsoft/xiv/XIVImageRotationAnimationManager$RotationCallback;)V

    .line 267
    return-void
.end method

.method public readyToDraw(I)Z
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getLargeThumbnailManager()Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->readyToDraw(I)Z

    move-result v0

    return v0
.end method

.method public recycle(Ljava/lang/Object;)V
    .locals 0
    .param p1, "instance"    # Ljava/lang/Object;

    .prologue
    .line 598
    check-cast p1, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    .end local p1    # "instance":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;->recycle()V

    .line 599
    return-void
.end method

.method public setDelayTime()V
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getTileManager()Lcom/quramsoft/xiv/XIVTileManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVTileManager;->setDelayTime()V

    .line 447
    return-void
.end method

.method public setMultiViewState(Z)V
    .locals 1
    .param p1, "state"    # Z

    .prologue
    .line 349
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0, p1}, Lcom/quramsoft/xiv/XIV;->setMultiViewState(Z)V

    .line 350
    return-void
.end method

.method public setPhotoDataAdapter(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 337
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0, p1}, Lcom/quramsoft/xiv/XIV;->setPhotoDataAdapter(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)V

    .line 338
    return-void
.end method

.method public setPhotoView(Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 5
    .param p1, "view"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    const/4 v4, 0x3

    .line 324
    iget-object v2, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v2, p1}, Lcom/quramsoft/xiv/XIV;->setPhotoView(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    .line 326
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXivTileReuse:Z

    if-eqz v2, :cond_1

    .line 327
    const/16 v2, 0x18

    new-array v0, v2, [Landroid/graphics/Bitmap;

    .line 328
    .local v0, "b":[Landroid/graphics/Bitmap;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 329
    iget-object v2, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v2}, Lcom/quramsoft/xiv/XIV;->getBitmapReuseManager()Lcom/quramsoft/xiv/XIVBitmapReuseManager;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/quramsoft/xiv/XIVBitmapReuseManager;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v0, v1

    .line 328
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 330
    :cond_0
    const/4 v1, 0x0

    :goto_1
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 331
    iget-object v2, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v2}, Lcom/quramsoft/xiv/XIV;->getBitmapReuseManager()Lcom/quramsoft/xiv/XIVBitmapReuseManager;

    move-result-object v2

    aget-object v3, v0, v1

    invoke-virtual {v2, v4, v3}, Lcom/quramsoft/xiv/XIVBitmapReuseManager;->freeBitmap(ILandroid/graphics/Bitmap;)V

    .line 330
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 334
    .end local v0    # "b":[Landroid/graphics/Bitmap;
    .end local v1    # "i":I
    :cond_1
    return-void
.end method

.method public setQrd(Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)V
    .locals 2
    .param p1, "regionDecoder"    # Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;

    .prologue
    .line 450
    invoke-static {p1}, Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;->getNativeInstance(Lcom/sec/samsung/gallery/decoder/regiondecoder/RegionDecoder;)Ljava/lang/Object;

    move-result-object v0

    .line 451
    .local v0, "nativeInstance":Ljava/lang/Object;
    instance-of v1, v0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    if-eqz v1, :cond_0

    .line 452
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v1}, Lcom/quramsoft/xiv/XIV;->getTileManager()Lcom/quramsoft/xiv/XIVTileManager;

    move-result-object v1

    check-cast v0, Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;

    .end local v0    # "nativeInstance":Ljava/lang/Object;
    invoke-virtual {v1, v0}, Lcom/quramsoft/xiv/XIVTileManager;->setQrd(Lcom/quramsoft/xiv/XIVBitmapRegionDecoder;)V

    .line 454
    :cond_0
    return-void
.end method

.method public setViewSize(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getLargeThumbnailManager()Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->setViewSize(II)V

    .line 160
    return-void
.end method

.method public startAnimation()V
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getImageRotationAnimationManager()Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVImageRotationAnimationManager;->startAnimation()V

    .line 275
    return-void
.end method

.method public switchToNext()V
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->switchToNext()V

    .line 317
    return-void
.end method

.method public switchToPrev()V
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->switchToPrev()V

    .line 321
    return-void
.end method

.method public unsetPhotoDataAdapter()V
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->unsetPhotoDataAdapter()V

    .line 346
    return-void
.end method

.method public unsetPhotoView()V
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->unsetPhotoView()V

    .line 342
    return-void
.end method

.method public uploadCenterLargeThumbnail(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Z
    .locals 1
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getLargeThumbnailManager()Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->uploadCenterLargeThumbnail(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Z

    move-result v0

    .line 177
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public useCenterLargeThumbnail()Z
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getLargeThumbnailManager()Lcom/quramsoft/xiv/XIVLargeThumbnailManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIVLargeThumbnailManager;->useCenterLargeThumbnail()Z

    move-result v0

    return v0
.end method
