.class public Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;
.super Lcom/sec/samsung/gallery/view/help/HelpViewState;
.source "HelpEditPictureState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;


# static fields
.field protected static final TAG:Ljava/lang/String; = "HelpEditPictureState"


# instance fields
.field private mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->onClickViewingPictures()V

    return-void
.end method

.method private onClickEditingPictures()V
    .locals 4

    .prologue
    const-wide/16 v2, 0xbb8

    const/4 v1, 0x4

    .line 194
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mRelativeLayoutForCustomTap:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 195
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 196
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->showCompletedPopup()V

    .line 197
    invoke-virtual {p0, v2, v3, v2, v3}, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->startFinishActivity(JJ)V

    .line 198
    return-void
.end method

.method private onClickViewingPictures()V
    .locals 3

    .prologue
    .line 201
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mViewMode:I

    .line 202
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 203
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setTitle(Ljava/lang/String;)V

    .line 204
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->setHelpView()V

    .line 205
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->startTapAnimation(I)V

    .line 206
    return-void
.end method

.method private setViewEditPictures()V
    .locals 13

    .prologue
    .line 63
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v9}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 64
    .local v7, "res":Landroid/content/res/Resources;
    const v9, 0x7f0d015d

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 65
    .local v5, "popup_x":I
    const v9, 0x7f0d0167

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 66
    .local v2, "margin":I
    const v9, 0x7f0d0168

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 67
    .local v3, "marginBottom":I
    const v9, 0x7f0d0169

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 69
    .local v4, "marginLeft":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mItem:Landroid/widget/ImageView;

    if-nez v9, :cond_0

    .line 70
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v10, 0x7f0f0133

    invoke-virtual {v9, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    check-cast v9, Landroid/widget/ImageView;

    iput-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mItem:Landroid/widget/ImageView;

    .line 72
    :cond_0
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getActionBarSize(Landroid/content/Context;)I

    move-result v0

    .line 73
    .local v0, "actionBarHeight":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->orientation:I

    const/4 v10, 0x2

    if-ne v9, v10, :cond_8

    .line 75
    iget v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mViewMode:I

    if-nez v9, :cond_5

    .line 76
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mText:Landroid/widget/TextView;

    const v10, 0x7f0e02c2

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    .line 77
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mText:Landroid/widget/TextView;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/widget/TextView;->measure(II)V

    .line 78
    const v9, 0x7f0d01ad

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    if-nez v9, :cond_2

    .line 79
    iget v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mWidth:I

    div-int/lit8 v9, v9, 0x2

    iget v10, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mWidth:I

    div-int/lit8 v10, v10, 0x4

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    sub-int/2addr v10, v11

    sub-int/2addr v9, v10

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v10}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    sub-int v8, v9, v10

    .line 81
    .local v8, "x":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    neg-int v10, v8

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 82
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    iget v10, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHeight:I

    int-to-float v10, v10

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 83
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    iget v10, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHeight:I

    int-to-float v10, v10

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v11

    int-to-float v11, v11

    sub-float/2addr v10, v11

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setY(F)V

    .line 84
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    iget v10, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mWidth:I

    int-to-float v10, v10

    const/high16 v11, 0x40800000    # 4.0f

    div-float/2addr v10, v11

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v11

    int-to-float v11, v11

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    sub-float/2addr v10, v11

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setX(F)V

    .line 85
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHead:Landroid/widget/ImageView;

    iget v10, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mWidth:I

    int-to-float v10, v10

    const/high16 v11, 0x40800000    # 4.0f

    div-float/2addr v10, v11

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHead:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v11

    int-to-float v11, v11

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    sub-float/2addr v10, v11

    int-to-float v11, v8

    add-float/2addr v10, v11

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setX(F)V

    .line 93
    .end local v8    # "x":I
    :goto_0
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v9, :cond_3

    .line 94
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mBG:Landroid/widget/ImageView;

    const-string v10, "gallery_help_album_land"

    invoke-super {p0, v10}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 101
    :goto_1
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mItem:Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 102
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mItem:Landroid/widget/ImageView;

    add-int v10, v0, v2

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setY(F)V

    .line 103
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mItem:Landroid/widget/ImageView;

    mul-int/lit8 v10, v2, 0x2

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setX(F)V

    .line 104
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mItem:Landroid/widget/ImageView;

    iget v10, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mWidth:I

    div-int/lit8 v10, v10, 0x5

    mul-int/lit8 v10, v10, 0x2

    sub-int/2addr v10, v2

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 105
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mItem:Landroid/widget/ImageView;

    iget v10, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHeight:I

    div-int/lit8 v10, v10, 0xa

    mul-int/lit8 v10, v10, 0x6

    sub-int/2addr v10, v3

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 107
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 108
    .local v6, "r":Landroid/content/res/Resources;
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mText:Landroid/widget/TextView;

    const v10, 0x7f0d0199

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 181
    .end local v6    # "r":Landroid/content/res/Resources;
    :cond_1
    :goto_2
    const-string v1, ""

    .line 182
    .local v1, "description":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mBG:Landroid/widget/ImageView;

    invoke-virtual {v9, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 183
    iget v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mViewMode:I

    if-nez v9, :cond_d

    .line 184
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0e02c2

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 188
    :goto_3
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v9, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 189
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mItem:Landroid/widget/ImageView;

    invoke-virtual {v9, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 190
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->startBubbleAnimation()V

    .line 191
    return-void

    .line 87
    .end local v1    # "description":Ljava/lang/String;
    :cond_2
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    const v10, 0x7f0d01af

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    neg-int v10, v10

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 88
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    const v10, 0x7f0d01b0

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 89
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    const v10, 0x7f0d01ae

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setY(F)V

    .line 90
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    const v10, 0x7f0d01ad

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setX(F)V

    .line 91
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHead:Landroid/widget/ImageView;

    const v10, 0x7f0d01b1

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setX(F)V

    goto/16 :goto_0

    .line 96
    :cond_3
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v9}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 97
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mBG:Landroid/widget/ImageView;

    const v10, 0x7f0202ce

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 99
    :cond_4
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mBG:Landroid/widget/ImageView;

    const v10, 0x7f0202cd

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 109
    :cond_5
    iget v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mViewMode:I

    const/4 v10, 0x2

    if-ne v9, v10, :cond_1

    .line 110
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mText:Landroid/widget/TextView;

    const v10, 0x7f0e02c8

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    .line 111
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mText:Landroid/widget/TextView;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/widget/TextView;->measure(II)V

    .line 112
    const v9, 0x7f0d01b3

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    if-nez v9, :cond_6

    .line 113
    iget v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mWidth:I

    div-int/lit8 v9, v9, 0x2

    iget v10, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mWidth:I

    div-int/lit8 v10, v10, 0x3

    sub-int/2addr v9, v10

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v10}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    sub-int v8, v9, v10

    .line 114
    .restart local v8    # "x":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    neg-int v10, v8

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 115
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    iget v10, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHeight:I

    int-to-float v10, v10

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 116
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    iget v10, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHeight:I

    div-int/lit8 v10, v10, 0x2

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setY(F)V

    .line 117
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    iget v10, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mWidth:I

    int-to-float v10, v10

    const/high16 v11, 0x40400000    # 3.0f

    div-float/2addr v10, v11

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setX(F)V

    .line 118
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHead:Landroid/widget/ImageView;

    iget v10, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mWidth:I

    div-int/lit8 v10, v10, 0x3

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v11

    div-int/lit8 v11, v11, 0x3

    add-int/2addr v10, v11

    add-int/2addr v10, v8

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setX(F)V

    .line 126
    .end local v8    # "x":I
    :goto_4
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v9, :cond_7

    .line 127
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mBG:Landroid/widget/ImageView;

    const-string v10, "help_2"

    invoke-super {p0, v10}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 132
    :goto_5
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mItem:Landroid/widget/ImageView;

    add-int v10, v0, v2

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setY(F)V

    .line 133
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mItem:Landroid/widget/ImageView;

    iget v10, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mWidth:I

    div-int/lit8 v10, v10, 0x5

    sub-int/2addr v10, v4

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setX(F)V

    .line 134
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mItem:Landroid/widget/ImageView;

    iget v10, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mWidth:I

    div-int/lit8 v10, v10, 0x5

    mul-int/lit8 v10, v10, 0x2

    div-int/lit8 v11, v4, 0x2

    add-int/2addr v10, v11

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 135
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mItem:Landroid/widget/ImageView;

    iget v10, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHeight:I

    div-int/lit8 v10, v10, 0x2

    mul-int/lit8 v11, v2, 0x14

    sub-int/2addr v10, v11

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    goto/16 :goto_2

    .line 120
    :cond_6
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    const v10, 0x7f0d01b5

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    neg-int v10, v10

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 121
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    const v10, 0x7f0d01b6

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 122
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    const v10, 0x7f0d01b4

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setY(F)V

    .line 123
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    const v10, 0x7f0d01b3

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setX(F)V

    .line 124
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHead:Landroid/widget/ImageView;

    const v10, 0x7f0d01b8

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setX(F)V

    goto :goto_4

    .line 130
    :cond_7
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mBG:Landroid/widget/ImageView;

    const v10, 0x7f0202c8

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_5

    .line 138
    :cond_8
    iget v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mViewMode:I

    if-nez v9, :cond_b

    .line 139
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mText:Landroid/widget/TextView;

    const v10, 0x7f0e02c2

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    .line 140
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mText:Landroid/widget/TextView;

    const v10, 0x7f0d0199

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 141
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    neg-int v10, v5

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 142
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    const v10, 0x7f0d015e

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    add-int/2addr v10, v11

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 143
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    iget v10, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mWidth:I

    div-int/lit8 v10, v10, 0x8

    mul-int/lit8 v10, v10, 0x3

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    sub-int/2addr v10, v11

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setX(F)V

    .line 144
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    const v10, 0x7f0d015e

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    sub-int/2addr v10, v11

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setY(F)V

    .line 145
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHead:Landroid/widget/ImageView;

    iget v10, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mWidth:I

    div-int/lit8 v10, v10, 0x8

    mul-int/lit8 v10, v10, 0x3

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHead:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    sub-int/2addr v10, v11

    add-int/2addr v10, v5

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setX(F)V

    .line 147
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v9, :cond_9

    .line 148
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mBG:Landroid/widget/ImageView;

    const-string v10, "gallery_help_album"

    invoke-super {p0, v10}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 155
    :goto_6
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mItem:Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 156
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mItem:Landroid/widget/ImageView;

    const v10, 0x7f0d015f

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setX(F)V

    .line 157
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mItem:Landroid/widget/ImageView;

    const v10, 0x7f0d0160

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setY(F)V

    .line 158
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mItem:Landroid/widget/ImageView;

    const v10, 0x7f0d0161

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 159
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mItem:Landroid/widget/ImageView;

    const v10, 0x7f0d0161

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    goto/16 :goto_2

    .line 150
    :cond_9
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v9}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v9

    if-nez v9, :cond_a

    .line 151
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mBG:Landroid/widget/ImageView;

    const v10, 0x7f0202cb

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_6

    .line 153
    :cond_a
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mBG:Landroid/widget/ImageView;

    const v10, 0x7f0202cc

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_6

    .line 160
    :cond_b
    iget v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mViewMode:I

    const/4 v10, 0x2

    if-ne v9, v10, :cond_1

    .line 161
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 162
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mItem:Landroid/widget/ImageView;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 163
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->clearAnimation()V

    .line 164
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mText:Landroid/widget/TextView;

    const v10, 0x7f0e02c8

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    .line 165
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    int-to-float v10, v5

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 166
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    int-to-float v10, v0

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 167
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mRelativeLayoutForCustomTap:Landroid/widget/RelativeLayout;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 168
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mRelativeLayoutForCustomTap:Landroid/widget/RelativeLayout;

    const v10, 0x7f0f012a

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    iput-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    .line 169
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    const v10, 0x7f0d0162

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    sub-int/2addr v10, v11

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setX(F)V

    .line 170
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    div-int/lit8 v10, v0, 0x2

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    sub-int/2addr v10, v11

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setY(F)V

    .line 171
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 172
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHead:Landroid/widget/ImageView;

    const v10, 0x7f0d0162

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHead:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    sub-int/2addr v10, v11

    sub-int/2addr v10, v5

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setX(F)V

    .line 174
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v9, :cond_c

    .line 175
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mBG:Landroid/widget/ImageView;

    const-string v10, "help_2"

    invoke-super {p0, v10}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 177
    :cond_c
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHead:Landroid/widget/ImageView;

    iget v10, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mWidth:I

    div-int/lit8 v10, v10, 0x2

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v11

    add-int/2addr v10, v11

    sub-int/2addr v10, v5

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setX(F)V

    goto/16 :goto_2

    .line 186
    .restart local v1    # "description":Ljava/lang/String;
    :cond_d
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0e02c8

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_3
.end method


# virtual methods
.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 210
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 211
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 213
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 28
    const-string v0, "Help"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mHelpMode:I

    .line 29
    iput v1, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mViewMode:I

    .line 30
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .line 31
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->setHelpView()V

    .line 32
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-super {p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->animationInit(Landroid/content/Context;)V

    .line 33
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->setOnClickListener()V

    .line 34
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 217
    iget v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mViewMode:I

    if-nez v0, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 218
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0e02da

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 224
    :goto_0
    return-void

    .line 219
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0f029c

    if-ne v0, v1, :cond_1

    .line 220
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->onClickEditingPictures()V

    goto :goto_0

    .line 222
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->onOptionsItemSelected(Landroid/view/MenuItem;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 51
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->onPause()V

    .line 52
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->pause()V

    .line 54
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const v1, 0x7f0e0073

    .line 38
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->onResume()V

    .line 39
    iget v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mViewMode:I

    if-nez v0, :cond_1

    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->resume(I)V

    .line 42
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setTitle(I)V

    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setDrawerEnable(Z)V

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 47
    :cond_1
    return-void
.end method

.method protected setHelpView()V
    .locals 0

    .prologue
    .line 58
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->setHelpView()V

    .line 59
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->setViewEditPictures()V

    .line 60
    return-void
.end method

.method protected setOnClickListener()V
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mBG:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mBG:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSoundEffectsEnabled(Z)V

    .line 229
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mBG:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState$1;-><init>(Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mText:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 238
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mText:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setImportantForAccessibility(I)V

    .line 241
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mItem:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 242
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mItem:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState$2;-><init>(Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 251
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;->mTap:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState$3;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/help/HelpEditPictureState$3;-><init>(Lcom/sec/samsung/gallery/view/help/HelpEditPictureState;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 258
    :cond_3
    return-void
.end method
