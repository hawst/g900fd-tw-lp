.class Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;
.super Landroid/os/AsyncTask;
.source "MapViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/mapfragment/MapViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MarkerDrawTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field mInputPointList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)V
    .locals 0

    .prologue
    .line 431
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/mapfragment/MapViewState;Lcom/sec/samsung/gallery/mapfragment/MapViewState$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState$1;

    .prologue
    .line 431
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;-><init>(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 20
    .param p1, "arg"    # [Ljava/lang/Void;

    .prologue
    .line 464
    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$500()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MarkerDrawTask doInBackground"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 467
    .local v16, "overlayInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mCurrentSetPath:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1100(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 468
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapViewDataAdapter:Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1200(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->getCount()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mAlbumPosition:I
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1300(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)I

    move-result v4

    if-gt v3, v4, :cond_0

    .line 469
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapViewDataAdapter:Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1200(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->getCount()I

    move-result v3

    if-eqz v3, :cond_0

    .line 470
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapViewDataAdapter:Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1200(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    # setter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mAlbumPosition:I
    invoke-static {v3, v4}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1302(Lcom/sec/samsung/gallery/mapfragment/MapViewState;I)I

    .line 474
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mAlbumPosition:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1300(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)I

    move-result v3

    if-ltz v3, :cond_7

    .line 475
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapViewDataAdapter:Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1200(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mAlbumPosition:I
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1300(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 477
    .local v2, "album":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_1

    .line 478
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mAlbumName:Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1402(Lcom/sec/samsung/gallery/mapfragment/MapViewState;Ljava/lang/String;)Ljava/lang/String;

    .line 480
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapViewDataAdapter:Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1200(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mAlbumPosition:I
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1300(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->getMediaItemList(I)Ljava/util/ArrayList;

    move-result-object v14

    .line 483
    .local v14, "mediaItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 484
    .local v9, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getLongitude()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 486
    new-instance v3, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getLongitude()D

    move-result-wide v6

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v8

    invoke-direct/range {v3 .. v9}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;-><init>(DDLcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/MediaItem;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 520
    .end local v2    # "album":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v9    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v14    # "mediaItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :catch_0
    move-exception v11

    .line 521
    .local v11, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 525
    .end local v11    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_1
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    return-object v3

    .line 493
    :cond_4
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapViewDataAdapter:Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1200(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->getCount()I

    move-result v10

    .line 494
    .local v10, "count":I
    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$500()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mMapViewDataAdapter Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    const-string v3, "MapViewState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mMapViewDataAdapter Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    const/4 v13, 0x0

    .local v13, "ix":I
    :goto_2
    if-ge v13, v10, :cond_7

    .line 498
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapViewDataAdapter:Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1200(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;

    move-result-object v3

    invoke-virtual {v3, v13}, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->getMediaItemList(I)Ljava/util/ArrayList;

    move-result-object v14

    .line 500
    .restart local v14    # "mediaItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .restart local v12    # "i$":Ljava/util/Iterator;
    :cond_5
    :goto_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 501
    .restart local v9    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getLongitude()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 503
    new-instance v3, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getLongitude()D

    move-result-wide v6

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v8

    invoke-direct/range {v3 .. v9}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;-><init>(DDLcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/MediaItem;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 522
    .end local v9    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v10    # "count":I
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "ix":I
    .end local v14    # "mediaItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :catchall_0
    move-exception v3

    throw v3

    .line 497
    .restart local v10    # "count":I
    .restart local v12    # "i$":Ljava/util/Iterator;
    .restart local v13    # "ix":I
    .restart local v14    # "mediaItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_6
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 510
    .end local v10    # "count":I
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "ix":I
    .end local v14    # "mediaItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_7
    :try_start_3
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->mInputPointList:Ljava/util/ArrayList;

    .line 511
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .restart local v12    # "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;

    .line 512
    .local v15, "overlayInfo":Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->mInputPointList:Ljava/util/ArrayList;

    new-instance v4, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;

    new-instance v5, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v15}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v15}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;->getLongitude()D

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-direct {v5, v6, v7, v0, v1}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-direct {v4, v5, v15}, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;-><init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 515
    .end local v15    # "overlayInfo":Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->mInputPointList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_3

    .line 517
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1500(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Landroid/os/Handler;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1500(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Landroid/os/Handler;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 431
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 10
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 530
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->mInputPointList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 531
    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$500()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MarkerDrawTask onPostExecute. mInputPointList count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->mInputPointList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    :cond_0
    :try_start_0
    new-instance v2, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    invoke-direct {v2}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;-><init>()V

    .line 535
    .local v2, "options":Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    new-instance v3, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;

    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1600(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;-><init>(Landroid/content/Context;Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;)V

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->setMarkerOptionsChooser(Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser;)V

    .line 537
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    iget-object v3, v3, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-nez v3, :cond_3

    .line 538
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # invokes: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->setUpMapIfNeeded()V
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$100(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)V

    .line 542
    :goto_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    new-instance v4, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    iget-object v5, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    iget-object v5, v5, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    iget-object v6, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->mInputPointList:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v7}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1700(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v7

    invoke-direct {v4, v5, v2, v6, v7}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;-><init>(Lcom/google/android/gms/maps/GoogleMap;Lcom/sec/samsung/gallery/mapfragment/clustering/Options;Ljava/util/ArrayList;Landroid/content/Context;)V

    # setter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->clusterkraf:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;
    invoke-static {v3, v4}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1002(Lcom/sec/samsung/gallery/mapfragment/MapViewState;Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 547
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1800(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->isDestroyed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 548
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mLoadingDialog:Landroid/app/ProgressDialog;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$600(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Landroid/app/ProgressDialog;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 549
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mLoadingDialog:Landroid/app/ProgressDialog;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$600(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Landroid/app/ProgressDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->dismiss()V

    .line 553
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1900(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEventViewMode(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 554
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->mInputPointList:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->mInputPointList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_2

    .line 555
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->mInputPointList:Ljava/util/ArrayList;

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;->getMapPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    .line 556
    .local v1, "itemLatLng":Lcom/google/android/gms/maps/model/LatLng;
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mItemLatLng:[D
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$2000(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)[D

    move-result-object v3

    iget-wide v4, v1, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    aput-wide v4, v3, v8

    .line 557
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mItemLatLng:[D
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$2000(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)[D

    move-result-object v3

    iget-wide v4, v1, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    aput-wide v4, v3, v9

    .line 558
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$2100(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mAlbumName:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1400(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(Ljava/lang/String;)V

    .line 559
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    iget-object v3, v3, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v3, :cond_2

    .line 560
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # invokes: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->setCameraPositon()V
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$2200(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)V

    .line 565
    .end local v1    # "itemLatLng":Lcom/google/android/gms/maps/model/LatLng;
    .end local v2    # "options":Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    :cond_2
    :goto_1
    return-void

    .line 540
    .restart local v2    # "options":Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    :cond_3
    :try_start_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    iget-object v3, v3, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/GoogleMap;->clear()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 544
    .end local v2    # "options":Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    :catch_0
    move-exception v0

    .line 545
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 547
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1800(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->isDestroyed()Z

    move-result v3

    if-nez v3, :cond_4

    .line 548
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mLoadingDialog:Landroid/app/ProgressDialog;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$600(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Landroid/app/ProgressDialog;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 549
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mLoadingDialog:Landroid/app/ProgressDialog;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$600(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Landroid/app/ProgressDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->dismiss()V

    .line 553
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1900(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEventViewMode(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 554
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->mInputPointList:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->mInputPointList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_2

    .line 555
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->mInputPointList:Ljava/util/ArrayList;

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;->getMapPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    .line 556
    .restart local v1    # "itemLatLng":Lcom/google/android/gms/maps/model/LatLng;
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mItemLatLng:[D
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$2000(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)[D

    move-result-object v3

    iget-wide v4, v1, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    aput-wide v4, v3, v8

    .line 557
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mItemLatLng:[D
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$2000(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)[D

    move-result-object v3

    iget-wide v4, v1, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    aput-wide v4, v3, v9

    .line 558
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$2100(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mAlbumName:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1400(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(Ljava/lang/String;)V

    .line 559
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    iget-object v3, v3, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v3, :cond_2

    .line 560
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # invokes: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->setCameraPositon()V
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$2200(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)V

    goto :goto_1

    .line 547
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "itemLatLng":Lcom/google/android/gms/maps/model/LatLng;
    :catchall_0
    move-exception v3

    move-object v4, v3

    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1800(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->isDestroyed()Z

    move-result v3

    if-nez v3, :cond_5

    .line 548
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mLoadingDialog:Landroid/app/ProgressDialog;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$600(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Landroid/app/ProgressDialog;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 549
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mLoadingDialog:Landroid/app/ProgressDialog;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$600(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Landroid/app/ProgressDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->dismiss()V

    .line 553
    :cond_5
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1900(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEventViewMode(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 554
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->mInputPointList:Ljava/util/ArrayList;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->mInputPointList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_6

    .line 555
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->mInputPointList:Ljava/util/ArrayList;

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;->getMapPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    .line 556
    .restart local v1    # "itemLatLng":Lcom/google/android/gms/maps/model/LatLng;
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mItemLatLng:[D
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$2000(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)[D

    move-result-object v3

    iget-wide v6, v1, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    aput-wide v6, v3, v8

    .line 557
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mItemLatLng:[D
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$2000(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)[D

    move-result-object v3

    iget-wide v6, v1, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    aput-wide v6, v3, v9

    .line 558
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$2100(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v3

    iget-object v5, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mAlbumName:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1400(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(Ljava/lang/String;)V

    .line 559
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    iget-object v3, v3, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v3, :cond_6

    .line 560
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # invokes: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->setCameraPositon()V
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$2200(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)V

    .line 562
    .end local v1    # "itemLatLng":Lcom/google/android/gms/maps/model/LatLng;
    :cond_6
    throw v4
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 431
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 437
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 438
    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MarkerDrawTask onPreExecute"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$700(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mLoadingDialog:Landroid/app/ProgressDialog;
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$602(Lcom/sec/samsung/gallery/mapfragment/MapViewState;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 441
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mLoadingDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$600(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$800(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0032

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 442
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mLoadingDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$600(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 443
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mLoadingDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$600(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 444
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mLoadingDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$600(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Landroid/app/ProgressDialog;

    move-result-object v0

    new-instance v1, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask$1;-><init>(Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 452
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->clusterkraf:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1000(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 453
    invoke-static {v3}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->hideLocationThumbnail(I)V

    .line 456
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mCurrentSetPath:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1100(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 457
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapViewDataAdapter:Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$1200(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->reloadData()V

    .line 459
    :cond_1
    return-void
.end method
