.class Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$5;
.super Ljava/lang/Object;
.source "GallerySearchFilterGroupViewPager.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnRefreshTagListListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;)V
    .locals 0

    .prologue
    .line 680
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public refreshTagList()V
    .locals 6

    .prologue
    .line 684
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCMRelatedCategory:Z

    if-eqz v1, :cond_1

    .line 685
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;

    if-nez v1, :cond_0

    .line 686
    const-class v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 687
    .local v0, "threadName":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    invoke-direct {v2, v3, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;Ljava/lang/String;)V

    iput-object v2, v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;

    .line 688
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;->start()V

    .line 690
    .end local v0    # "threadName":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUpdateTagTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;->notifyDirty()V

    .line 692
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x65

    const-wide/16 v4, 0x190

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 693
    return-void
.end method
