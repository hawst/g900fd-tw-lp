.class public abstract Lcom/sec/samsung/gallery/dialog/GalleryDialog;
.super Landroid/app/DialogFragment;
.source "GalleryDialog.java"


# static fields
.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mTag:Ljava/lang/String;

.field private mViewObservable:Lcom/sec/samsung/gallery/view/ViewObservable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/sec/samsung/gallery/dialog/GalleryDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/dialog/GalleryDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/sec/samsung/gallery/dialog/GalleryDialog;->mContext:Landroid/content/Context;

    .line 25
    new-instance v0, Lcom/sec/samsung/gallery/view/ViewObservable;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/ViewObservable;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/dialog/GalleryDialog;->mViewObservable:Lcom/sec/samsung/gallery/view/ViewObservable;

    .line 26
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/dialog/GalleryDialog;->mTag:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public addObserver(Ljava/util/Observer;)V
    .locals 1
    .param p1, "obs"    # Ljava/util/Observer;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/GalleryDialog;->mViewObservable:Lcom/sec/samsung/gallery/view/ViewObservable;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ViewObservable;->addObserver(Ljava/util/Observer;)V

    .line 49
    return-void
.end method

.method public dismissDialog()V
    .locals 0

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/dialog/GalleryDialog;->dismiss()V

    .line 39
    return-void
.end method

.method public notifyObservers(Ljava/lang/Object;)V
    .locals 1
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/GalleryDialog;->mViewObservable:Lcom/sec/samsung/gallery/view/ViewObservable;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ViewObservable;->notifyObservers(Ljava/lang/Object;)V

    .line 53
    return-void
.end method

.method public setEnableButton(IZ)V
    .locals 2
    .param p1, "ButtonType"    # I
    .param p2, "enable"    # Z

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/dialog/GalleryDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 43
    .local v0, "dialog":Landroid/app/Dialog;
    instance-of v1, v0, Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    .line 44
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/dialog/GalleryDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    check-cast v1, Landroid/app/AlertDialog;

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 45
    :cond_0
    return-void
.end method

.method public showDialog()V
    .locals 3

    .prologue
    .line 31
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/GalleryDialog;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/GalleryDialog;->mTag:Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, Lcom/sec/samsung/gallery/dialog/GalleryDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 35
    :goto_0
    return-void

    .line 32
    :catch_0
    move-exception v0

    .line 33
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method
