.class public Lcom/sec/samsung/gallery/controller/StartContactPickCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "StartContactPickCmd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 23
    return-void
.end method

.method private closeCameraLens(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    invoke-static {p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->closeCameraLens(Landroid/content/Context;)V

    .line 62
    return-void
.end method

.method private startContactPickActivity(Landroid/content/Context;Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cmdType"    # Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    .prologue
    .line 38
    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.GET_CONTENT"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 39
    .local v2, "intent":Landroid/content/Intent;
    const-string/jumbo v4, "vnd.android.cursor.item/contact"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v4

    const-string v5, "CscFeature_Contact_ReplacePackageAs"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 43
    .local v3, "strContactPackageName":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 44
    :cond_0
    const-string v3, "com.android.contacts"

    .line 47
    :cond_1
    invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 49
    sget-object v4, Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;->INCLUDE_PROFILE:Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    if-ne p2, v4, :cond_2

    .line 50
    const-string v4, "additional"

    const-string v5, "include-profile"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    :cond_2
    :try_start_0
    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    move-object v4, v0

    const/16 v5, 0x304

    invoke-virtual {v4, v2, v5}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    :goto_0
    return-void

    .line 54
    :catch_0
    move-exception v1

    .line 55
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    const v4, 0x7f0e0062

    invoke-static {p1, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 56
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 4
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 29
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Object;

    move-object v2, v3

    check-cast v2, [Ljava/lang/Object;

    .line 30
    .local v2, "params":[Ljava/lang/Object;
    const/4 v3, 0x0

    aget-object v1, v2, v3

    check-cast v1, Landroid/content/Context;

    .line 31
    .local v1, "context":Landroid/content/Context;
    const/4 v3, 0x1

    aget-object v0, v2, v3

    check-cast v0, Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;

    .line 33
    .local v0, "cmdType":Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;
    invoke-direct {p0, v1, v0}, Lcom/sec/samsung/gallery/controller/StartContactPickCmd;->startContactPickActivity(Landroid/content/Context;Lcom/sec/samsung/gallery/controller/StartContactPickCmd$ContactPickCmdType;)V

    .line 34
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/StartContactPickCmd;->closeCameraLens(Landroid/content/Context;)V

    .line 35
    return-void
.end method
