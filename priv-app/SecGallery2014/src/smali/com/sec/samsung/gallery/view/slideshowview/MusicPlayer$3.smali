.class Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$3;
.super Ljava/lang/Object;
.source "MusicPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    .line 108
    const-string v0, "MusicPlayer"

    const-string v1, "Error occurred while playing audio."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->stop()V

    .line 110
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->release()V

    .line 111
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 112
    const/4 v0, 0x1

    return v0
.end method
