.class public Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;
.super Lcom/sec/android/gallery3d/app/ActivityState;
.source "GallerySearchViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$31;,
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$OnShowHistoryListListener;,
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$OnSelectedItemsListener;,
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;,
        Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;
    }
.end annotation


# static fields
.field private static final DCM_NOTY_BUFFER_SIZE:I = 0xa

.field private static final DCM_NOTY_DELAY:I = 0xbb8

.field private static final HISTORY_LIST_OPEN_DELAY:F = 0.07f

.field private static final TAG:Ljava/lang/String;

.field private static final THUMB_PHOTO_CACHE_SIZE:I = 0x40


# instance fields
.field private final MSG_UPDATE_CATEGORY:I

.field private final PROGRESS_HELPER_ON:Z

.field private final SUPPORT_REALTIME_CATEGORY_UPDATE:Z

.field private isAppStart:Z

.field private isfromEditmode:Z

.field private mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

.field private mBgImageIndex:I

.field private mContainer:Landroid/view/ViewGroup;

.field private mCurrentTopSetPath:Ljava/lang/String;

.field private mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

.field private mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

.field private mFilterDetailView:Landroid/view/ViewGroup;

.field private mFilterGroupView:Landroid/view/ViewGroup;

.field private mFilterHorizontalView:Landroid/view/ViewGroup;

.field private mFilterPagerView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

.field private mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

.field private mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

.field private mGroupClickListener:Landroid/view/View$OnClickListener;

.field private mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

.field private mHistoryAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;

.field private mHistoryListView:Landroid/widget/ListView;

.field mIsAlreadyDisplayNoResultPopup:Z

.field private mIsFilterShow:Z

.field private mIsPickMode:Z

.field private mIsShowHistoryList:Z

.field private mListStyleTagCloudView:Landroid/widget/ExpandableListView;

.field private mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

.field private mNewSearchTagFilterView:Landroid/view/View;

.field private mNoItemView:Landroid/view/View;

.field protected mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

.field private mOnGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

.field private mPreviousMultiWindowState:Z

.field private mPreviousTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

.field private mPreviousViewMode:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;"
        }
    .end annotation
.end field

.field private mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

.field private mRemoveCountUpdateMessage:I

.field private mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mScrolledPosition:I

.field private mSearchTagFilterView:Landroid/view/View;

.field private mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

.field private mSearchViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mSelectedItemView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

.field mSelectedItemsListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$OnSelectedItemsListener;

.field private mSelectionFilterShow:Z

.field protected mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mShowProgressDialog:Z

.field private mShowSlideshowsettingsMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

.field private mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field private mTagCloudListAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;

.field private mTagCloudViewMode:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

.field private mTempKeyword:Ljava/lang/String;

.field private mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;

.field private mVoiceRecog:Z

.field private mWindowContentChangedListener:Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;

.field private needRedrawForSelect:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 121
    const-class v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 119
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ActivityState;-><init>()V

    .line 127
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->SUPPORT_REALTIME_CATEGORY_UPDATE:Z

    .line 129
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    .line 130
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    .line 131
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mCurrentTopSetPath:Ljava/lang/String;

    .line 134
    iput v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mBgImageIndex:I

    .line 135
    iput v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mScrolledPosition:I

    .line 137
    new-instance v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    .line 138
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mShowProgressDialog:Z

    .line 139
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    .line 142
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mContainer:Landroid/view/ViewGroup;

    .line 143
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mVoiceRecog:Z

    .line 149
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionFilterShow:Z

    .line 150
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->isfromEditmode:Z

    .line 151
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->PROGRESS_HELPER_ON:Z

    .line 153
    iput v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->MSG_UPDATE_CATEGORY:I

    .line 154
    iput v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mRemoveCountUpdateMessage:I

    .line 156
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    .line 160
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mFilterHorizontalView:Landroid/view/ViewGroup;

    .line 162
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mFilterGroupView:Landroid/view/ViewGroup;

    .line 164
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mFilterDetailView:Landroid/view/ViewGroup;

    .line 165
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectedItemView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    .line 166
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mFilterPagerView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    .line 167
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTempKeyword:Ljava/lang/String;

    .line 170
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    .line 171
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNoItemView:Landroid/view/View;

    .line 172
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHistoryListView:Landroid/widget/ListView;

    .line 174
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHistoryAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;

    .line 175
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsShowHistoryList:Z

    .line 176
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->isAppStart:Z

    .line 177
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->needRedrawForSelect:Z

    .line 178
    const-class v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mPreviousViewMode:Ljava/lang/Class;

    .line 183
    sget-object v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->REGULAR:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudViewMode:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    .line 184
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;

    .line 185
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudListAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;

    .line 205
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;

    const-string v1, "GALLERY_SEARCH_VIEW"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 262
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$3;

    const-string v1, "SLIDESHOW_SETTINGS"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$3;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mShowSlideshowsettingsMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 285
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$4;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

    .line 541
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$15;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$15;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    .line 554
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$16;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$16;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mWindowContentChangedListener:Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;

    .line 1579
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsAlreadyDisplayNoResultPopup:Z

    .line 1893
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$29;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$29;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGroupClickListener:Landroid/view/View$OnClickListener;

    .line 1934
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$30;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$30;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectedItemsListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$OnSelectedItemsListener;

    .line 1945
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mRemoveCountUpdateMessage:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;
    .param p1, "x1"    # I

    .prologue
    .line 119
    iput p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mRemoveCountUpdateMessage:I

    return p1
.end method

.method static synthetic access$108(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mRemoveCountUpdateMessage:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mRemoveCountUpdateMessage:I

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/core/GalleryFacade;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/glview/GlGallerySearchView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHistoryAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->addToSildeProxy()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mBgImageIndex:I

    return v0
.end method

.method static synthetic access$2002(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;
    .param p1, "x1"    # I

    .prologue
    .line 119
    iput p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mBgImageIndex:I

    return p1
.end method

.method static synthetic access$2100(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;
    .param p1, "x1"    # I

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->startDetailViewState(I)V

    return-void
.end method

.method static synthetic access$2300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/android/gallery3d/app/StateManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->enterSelectionMode(Z)V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->updateCountOnActionBar()V

    return-void
.end method

.method static synthetic access$2600(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsFilterShow:Z

    return v0
.end method

.method static synthetic access$2800(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNoItemView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHistoryListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->showDeleteAlertDialog()V

    return-void
.end method

.method static synthetic access$3400(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mFilterGroupView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$3502(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 119
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsShowHistoryList:Z

    return p1
.end method

.method static synthetic access$3600(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->showNoItemView(Z)V

    return-void
.end method

.method static synthetic access$3700(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;
    .param p1, "x1"    # I

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->startFilterGroupAnimation(I)V

    return-void
.end method

.method static synthetic access$3800(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mPreviousMultiWindowState:Z

    return v0
.end method

.method static synthetic access$3902(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 119
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mPreviousMultiWindowState:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->initHandler()V

    return-void
.end method

.method static synthetic access$4000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudViewMode:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionFilterShow:Z

    return v0
.end method

.method static synthetic access$4102(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 119
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionFilterShow:Z

    return p1
.end method

.method static synthetic access$4200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->startSlideShow()V

    return-void
.end method

.method static synthetic access$4600()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    sget-object v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4700(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$4800(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mFilterPagerView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->updateSelectedItems()V

    return-void
.end method

.method static synthetic access$502(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudListAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Landroid/widget/ExpandableListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->updateNoItemViewTopMargin()V

    return-void
.end method

.method private addToSildeProxy()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1670
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSlideShowSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v3

    .line 1674
    .local v3, "slideShowProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1675
    iput-boolean v8, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->isfromEditmode:Z

    .line 1676
    new-instance v4, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v6, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$28;

    invoke-direct {v6, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$28;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->getSubMediaSetCount()I

    move-result v7

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;I)V

    new-array v5, v9, [Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1716
    :goto_0
    return-void

    .line 1697
    :cond_0
    iput-boolean v9, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->isfromEditmode:Z

    .line 1698
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 1699
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "position":I
    :goto_1
    if-ge v2, v0, :cond_4

    .line 1701
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1702
    .local v1, "media":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v4, v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    if-nez v4, :cond_1

    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloud:Z

    if-eqz v4, :cond_2

    instance-of v4, v1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    if-nez v4, :cond_1

    instance-of v4, v1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    if-eqz v4, :cond_2

    .line 1699
    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1706
    :cond_2
    instance-of v4, v1, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v4, :cond_3

    .line 1707
    check-cast v1, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    .end local v1    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;

    move-result-object v1

    .line 1709
    .restart local v1    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_3
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1712
    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_2

    .line 1714
    .end local v1    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_4
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->startSlideShow()V

    goto :goto_0
.end method

.method private checkTagCloudViewMode()Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;
    .locals 4

    .prologue
    .line 1009
    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->REGULAR:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    .line 1010
    .local v1, "mode":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGallerySearchUISeperatedMode:Z

    if-eqz v2, :cond_0

    .line 1011
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 1012
    .local v0, "conf":Landroid/content/res/Configuration;
    iget v2, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 1013
    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->LIST:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    .line 1014
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v2, :cond_0

    .line 1015
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->LIST:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    .line 1021
    .end local v0    # "conf":Landroid/content/res/Configuration;
    :cond_0
    :goto_0
    return-object v1

    .line 1015
    .restart local v0    # "conf":Landroid/content/res/Configuration;
    :cond_1
    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->MULTILINE:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    goto :goto_0

    .line 1019
    :cond_2
    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->MULTILINE:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    goto :goto_0
.end method

.method private enterSelectionMode(Z)V
    .locals 4
    .param p1, "useVibrator"    # Z

    .prologue
    const/4 v3, 0x0

    .line 1164
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1197
    :cond_0
    :goto_0
    return-void

    .line 1167
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    instance-of v0, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    if-eqz v0, :cond_2

    .line 1168
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->setImeVisibility(Z)V

    .line 1171
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->enterSelectionMode(Z)V

    .line 1172
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    .line 1173
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1174
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(Ljava/lang/String;)V

    .line 1175
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->needRedrawForSelect:Z

    .line 1177
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    if-eqz v0, :cond_0

    .line 1178
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$25;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$25;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1194
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    sget v1, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_SELECT:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setMode(IILjava/lang/Object;)V

    .line 1195
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->refreshView(Z)V

    goto :goto_0
.end method

.method private initHandler()V
    .locals 3

    .prologue
    .line 189
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$1;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$1;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;Lcom/sec/android/gallery3d/ui/GLRoot;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    .line 204
    return-void
.end method

.method private initMultiWindowLayoutListener()V
    .locals 1

    .prologue
    .line 975
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$24;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$24;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mOnGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 989
    return-void
.end method

.method private initNewFilterSearchView()V
    .locals 9

    .prologue
    const v8, 0x7f0f0105

    const v7, 0x7f0f0104

    const v6, 0x7f0f0103

    const v5, 0x7f0f0102

    const/4 v4, 0x1

    .line 615
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    if-nez v1, :cond_0

    .line 616
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030060

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    .line 618
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGroupClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 619
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGroupClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 620
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGroupClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 621
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGroupClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 622
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    const v2, 0x7f0f0106

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGroupClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 624
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setHoverPopupType(I)V

    .line 625
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setHoverPopupType(I)V

    .line 626
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setHoverPopupType(I)V

    .line 627
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setHoverPopupType(I)V

    .line 628
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    const v2, 0x7f0f0106

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setHoverPopupType(I)V

    .line 630
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    const v2, 0x7f0f00ff

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mFilterHorizontalView:Landroid/view/ViewGroup;

    .line 631
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    const v2, 0x7f0f0101

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mFilterGroupView:Landroid/view/ViewGroup;

    .line 632
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    const v2, 0x7f0f0107

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mFilterDetailView:Landroid/view/ViewGroup;

    .line 633
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    const v2, 0x7f0f010b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHistoryListView:Landroid/widget/ListView;

    .line 634
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHistoryListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHistoryAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 635
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHistoryListView:Landroid/widget/ListView;

    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$17;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$17;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 656
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030068

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 658
    .local v0, "footerView":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHistoryAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;

    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$18;

    invoke-direct {v2, p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$18;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 685
    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$19;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$19;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 693
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->initNoItemView()V

    .line 695
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    const v2, 0x7f0f0100

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectedItemView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    .line 697
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectedItemView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->setData(Ljava/util/ArrayList;)V

    .line 699
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    const v2, 0x7f0f0109

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mFilterPagerView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    .line 700
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mFilterPagerView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->setOffscreenPageLimit(I)V

    .line 701
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mFilterPagerView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->setAnchorView(Landroid/view/View;)V

    .line 702
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mFilterPagerView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->init()V

    .line 703
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mFilterPagerView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$20;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$20;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 726
    .end local v0    # "footerView":Landroid/view/View;
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mContainer:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 727
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->startFilterGroupAnimation(I)V

    .line 728
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    const/4 v2, -0x1

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 730
    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->showNoItemView(Z)V

    .line 732
    return-void
.end method

.method private initNoItemView()V
    .locals 3

    .prologue
    .line 764
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNoItemView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 765
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300c8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNoItemView:Landroid/view/View;

    .line 767
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNoItemView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 768
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNoItemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 770
    :cond_0
    return-void
.end method

.method private initSearchView()V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 881
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudViewMode:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    sget-object v8, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->LIST:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    if-eq v3, v8, :cond_1

    move v3, v4

    :goto_0
    invoke-virtual {v7, v3}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->showDropDownIcon(Z)V

    .line 882
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mContainer:Landroid/view/ViewGroup;

    if-nez v3, :cond_c

    .line 883
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v7, 0x7f0f0175

    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mContainer:Landroid/view/ViewGroup;

    .line 884
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-eqz v3, :cond_2

    .line 885
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->initNewFilterSearchView()V

    .line 973
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v3, v5

    .line 881
    goto :goto_0

    .line 888
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    if-nez v3, :cond_3

    .line 889
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudViewMode:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    sget-object v8, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->REGULAR:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    if-ne v3, v8, :cond_5

    const v3, 0x7f0300e5

    :goto_2
    const/4 v8, 0x0

    invoke-virtual {v7, v3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    .line 893
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-nez v3, :cond_3

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v3, :cond_3

    .line 894
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->initNoItemView()V

    .line 895
    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->showNoItemView(Z)V

    .line 898
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    if-eqz v3, :cond_4

    .line 899
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mContainer:Landroid/view/ViewGroup;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 900
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mOnGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v3, v7}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 901
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudViewMode:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    sget-object v7, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->REGULAR:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    if-eq v3, v7, :cond_4

    .line 903
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    const/4 v7, -0x1

    iput v7, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 907
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudViewMode:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    sget-object v7, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->REGULAR:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    if-eq v3, v7, :cond_7

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;

    if-nez v3, :cond_7

    .line 908
    new-instance v3, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v3, v7}, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudListAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;

    .line 909
    new-instance v3, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableList;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v3, v7}, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableList;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;

    .line 910
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudListAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;

    invoke-virtual {v3, v7}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 912
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudListAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->getGroupCount()I

    move-result v3

    if-ge v1, v3, :cond_6

    .line 913
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;

    invoke-virtual {v3, v1}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 912
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 889
    .end local v1    # "i":I
    :cond_5
    const v3, 0x7f0300e4

    goto :goto_2

    .line 915
    .restart local v1    # "i":I
    :cond_6
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;

    const/4 v7, 0x2

    invoke-virtual {v3, v7}, Landroid/widget/ExpandableListView;->setChoiceMode(I)V

    .line 917
    .end local v1    # "i":I
    :cond_7
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;

    if-eqz v3, :cond_8

    .line 918
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mContainer:Landroid/view/ViewGroup;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;

    invoke-virtual {v3, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 919
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudViewMode:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    sget-object v8, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->LIST:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    if-ne v3, v8, :cond_a

    move v3, v5

    :goto_4
    invoke-virtual {v7, v3}, Landroid/widget/ExpandableListView;->setVisibility(I)V

    .line 921
    :cond_8
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    if-eqz v3, :cond_9

    .line 922
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    const v7, 0x7f0f024a

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 923
    .local v0, "filterTagView":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 924
    .local v2, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getActionBarSize(Landroid/content/Context;)I

    move-result v3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 925
    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 927
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudViewMode:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    sget-object v8, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->LIST:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    if-ne v7, v8, :cond_b

    :goto_5
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 929
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->updateNoItemViewTopMargin()V

    .line 930
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-nez v3, :cond_9

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v3, :cond_9

    .line 931
    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->showNoItemView(Z)V

    .line 971
    .end local v0    # "filterTagView":Landroid/view/View;
    .end local v2    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_9
    :goto_6
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-nez v3, :cond_0

    .line 972
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mContainer:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/high16 v5, 0x10a0000

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_1

    :cond_a
    move v3, v6

    .line 919
    goto :goto_4

    .restart local v0    # "filterTagView":Landroid/view/View;
    .restart local v2    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_b
    move v6, v5

    .line 927
    goto :goto_5

    .line 935
    .end local v0    # "filterTagView":Landroid/view/View;
    .end local v2    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_c
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudViewMode:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    sget-object v7, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->LIST:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    if-ne v3, v7, :cond_e

    .line 936
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    if-eqz v3, :cond_d

    .line 937
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 938
    :cond_d
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;

    if-eqz v3, :cond_9

    .line 939
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;

    invoke-virtual {v3, v5}, Landroid/widget/ExpandableListView;->setVisibility(I)V

    goto :goto_6

    .line 941
    :cond_e
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsFilterShow:Z

    if-eqz v3, :cond_13

    .line 942
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v3

    if-nez v3, :cond_f

    .line 943
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->setDropDownIcon(Z)V

    .line 944
    :cond_f
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;

    if-eqz v3, :cond_10

    .line 945
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;

    invoke-virtual {v3, v6}, Landroid/widget/ExpandableListView;->setVisibility(I)V

    .line 946
    :cond_10
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    if-eqz v3, :cond_11

    .line 947
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 956
    :cond_11
    :goto_7
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-eqz v3, :cond_9

    .line 957
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsFilterShow:Z

    if-eqz v3, :cond_16

    .line 958
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v3

    if-nez v3, :cond_12

    .line 959
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->setDropDownIcon(Z)V

    .line 960
    :cond_12
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    if-eqz v3, :cond_9

    .line 961
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_6

    .line 949
    :cond_13
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v3

    if-nez v3, :cond_14

    .line 950
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->setDropDownIcon(Z)V

    .line 951
    :cond_14
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;

    if-eqz v3, :cond_15

    .line 952
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;

    invoke-virtual {v3, v6}, Landroid/widget/ExpandableListView;->setVisibility(I)V

    .line 953
    :cond_15
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    if-eqz v3, :cond_11

    .line 954
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_7

    .line 963
    :cond_16
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v3

    if-nez v3, :cond_17

    .line 964
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->setDropDownIcon(Z)V

    .line 965
    :cond_17
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    if-eqz v3, :cond_9

    .line 966
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_6
.end method

.method private isPickMode()Z
    .locals 2

    .prologue
    .line 1266
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v1, :cond_1

    .line 1267
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    .line 1268
    .local v0, "currentMode":Lcom/sec/samsung/gallery/core/LaunchModeType;
    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v1, :cond_1

    .line 1270
    :cond_0
    const/4 v1, 0x1

    .line 1273
    .end local v0    # "currentMode":Lcom/sec/samsung/gallery/core/LaunchModeType;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private selectAll()V
    .locals 6

    .prologue
    .line 1634
    const/4 v3, 0x1

    .line 1636
    .local v3, "refresh":Z
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getCount()I

    move-result v0

    .line 1637
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "position":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 1638
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v4, v2}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    .line 1639
    .local v1, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1637
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1642
    .end local v1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_0
    if-eqz v3, :cond_1

    .line 1643
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->updateCountOnActionBar()V

    .line 1644
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->updateItems()V

    .line 1645
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->refreshView(Z)V

    .line 1647
    :cond_1
    return-void
.end method

.method private setTagCloudViewMode(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;)V
    .locals 2
    .param p1, "mode"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    .prologue
    .line 1001
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudViewMode:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1006
    :goto_0
    return-void

    .line 1003
    :cond_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudViewMode:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    .line 1004
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    sget-object v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->LIST:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->setTagCloudViewListMode(Z)V

    .line 1005
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->initSearchView()V

    goto :goto_0

    .line 1004
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private showDeleteAlertDialog()V
    .locals 7

    .prologue
    const v6, 0x7f0e0041

    .line 748
    const/4 v0, 0x0

    .line 749
    .local v0, "message":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0043

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHistoryAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 751
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$22;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$22;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    invoke-virtual {v1, v6, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e0046

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 761
    return-void
.end method

.method private showNoItemView(Z)V
    .locals 6
    .param p1, "isShow"    # Z

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 773
    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 774
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v2, v4}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->setSlideshowSettingEnable(Z)V

    .line 776
    :cond_0
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-eqz v2, :cond_6

    .line 777
    if-eqz p1, :cond_5

    .line 778
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsShowHistoryList:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHistoryAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;->getCount()I

    move-result v2

    if-lez v2, :cond_4

    .line 779
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNoItemView:Landroid/view/View;

    const v3, 0x7f0f0207

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 802
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHistoryListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getVisibility()I

    move-result v2

    if-ne v2, v5, :cond_2

    .line 803
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f04000b

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 804
    .local v0, "ani":Landroid/view/animation/Animation;
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->isAppStart:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 805
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 807
    :cond_1
    new-instance v1, Landroid/view/animation/LayoutAnimationController;

    const v2, 0x3d8f5c29    # 0.07f

    invoke-direct {v1, v0, v2}, Landroid/view/animation/LayoutAnimationController;-><init>(Landroid/view/animation/Animation;F)V

    .line 809
    .local v1, "controller":Landroid/view/animation/LayoutAnimationController;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHistoryListView:Landroid/widget/ListView;

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V

    .line 823
    .end local v0    # "ani":Landroid/view/animation/Animation;
    .end local v1    # "controller":Landroid/view/animation/LayoutAnimationController;
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHistoryListView:Landroid/widget/ListView;

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setVisibility(I)V

    .line 824
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNoItemView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 825
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->isAppStart:Z

    .line 841
    :cond_3
    :goto_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->updateNoItemViewTopMargin()V

    .line 842
    return-void

    .line 827
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHistoryListView:Landroid/widget/ListView;

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setVisibility(I)V

    .line 828
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNoItemView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 831
    :cond_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHistoryListView:Landroid/widget/ListView;

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setVisibility(I)V

    .line 832
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNoItemView:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 834
    :cond_6
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-nez v2, :cond_3

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v2, :cond_3

    .line 835
    if-eqz p1, :cond_7

    .line 836
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNoItemView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 838
    :cond_7
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNoItemView:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private startDetailViewState(I)V
    .locals 5
    .param p1, "position"    # I

    .prologue
    const/16 v4, 0x8

    .line 1555
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1556
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    .line 1558
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v1, :cond_0

    .line 1577
    :goto_0
    return-void

    .line 1561
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTempKeyword:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTempKeyword:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1562
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTempKeyword:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->addKeyword(Ljava/lang/String;)V

    .line 1564
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    if-eqz v2, :cond_2

    .line 1565
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1566
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;

    if-eqz v2, :cond_3

    .line 1567
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;

    invoke-virtual {v2, v4}, Landroid/widget/ExpandableListView;->setVisibility(I)V

    .line 1568
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    if-eqz v2, :cond_4

    .line 1569
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1571
    :cond_4
    const-string v2, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1574
    const-string v2, "KEY_MEDIA_SET_PATH"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getAlbumPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1575
    const-string v2, "KEY_ITEM_POSITION"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1576
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    const-class v3, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private startFilterGroupAnimation(I)V
    .locals 7
    .param p1, "i"    # I

    .prologue
    const/4 v6, 0x0

    .line 845
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-lt p1, v4, :cond_0

    .line 877
    :goto_0
    return-void

    .line 848
    :cond_0
    new-instance v1, Landroid/view/animation/AnimationSet;

    const/4 v4, 0x1

    invoke-direct {v1, v4}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 850
    .local v1, "rootSet":Landroid/view/animation/AnimationSet;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {v4, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 851
    .local v3, "view":Landroid/view/View;
    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v1, v4}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 853
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    const/high16 v4, -0x3f000000    # -8.0f

    invoke-direct {v2, v4, v6, v6, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 854
    .local v2, "trans":Landroid/view/animation/TranslateAnimation;
    const-wide/16 v4, 0x82

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 856
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v0, v6, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 857
    .local v0, "alpha":Landroid/view/animation/AlphaAnimation;
    const-wide/16 v4, 0x5a

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 858
    invoke-virtual {v1, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 859
    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 860
    new-instance v4, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$23;

    invoke-direct {v4, p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$23;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;I)V

    invoke-virtual {v1, v4}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 874
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 875
    invoke-virtual {v3, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private startSlideShow()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1719
    const/4 v1, 0x6

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v1, v0, v2

    const/4 v1, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object v3, v0, v1

    const/4 v1, 0x3

    aput-object v3, v0, v1

    const/4 v1, 0x4

    aput-object v3, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->isfromEditmode:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1722
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "START_SLIDESHOW"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1723
    return-void
.end method

.method private unselectAll()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1661
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 1662
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 1663
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    .line 1664
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v1, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v0, v1, v2, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 1665
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->updateItems()V

    .line 1666
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->refreshView(Z)V

    .line 1667
    return-void
.end method

.method private updateCountOnActionBar()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1650
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 1652
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    .line 1653
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    .line 1654
    .local v1, "selectedCount":I
    if-lez v1, :cond_0

    .line 1655
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getCount()I

    move-result v3

    if-ne v1, v3, :cond_1

    const/4 v0, 0x1

    .line 1656
    .local v0, "isSelectAll":Z
    :goto_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v4, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v3, v4, v2, v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 1658
    .end local v0    # "isSelectAll":Z
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 1655
    goto :goto_0
.end method

.method private updateNoItemViewTopMargin()V
    .locals 4

    .prologue
    .line 1956
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-nez v2, :cond_1

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v2, :cond_1

    .line 1957
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNoItemView:Landroid/view/View;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNoItemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 1958
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNoItemView:Landroid/view/View;

    const v3, 0x7f0f0207

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1959
    .local v0, "emptyView":Landroid/widget/TextView;
    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1960
    .local v1, "rlp":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v2, 0x0

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1961
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsFilterShow:Z

    if-eqz v2, :cond_0

    .line 1962
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->getTagFilterHeight()I

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getActionBarSize(Landroid/content/Context;)I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1964
    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1967
    .end local v0    # "emptyView":Landroid/widget/TextView;
    .end local v1    # "rlp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    return-void
.end method

.method private updateSelectedItems()V
    .locals 2

    .prologue
    .line 1927
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectedItemView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->setData(Ljava/util/ArrayList;)V

    .line 1928
    return-void
.end method


# virtual methods
.method checkSearchResult()V
    .locals 6

    .prologue
    const v5, 0x7f0e04b6

    const v4, 0x7f0e024c

    const v3, 0x7f0f0207

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1582
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    if-nez v0, :cond_3

    .line 1584
    :cond_0
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsAlreadyDisplayNoResultPopup:Z

    .line 1585
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-eqz v0, :cond_2

    .line 1586
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsShowHistoryList:Z

    .line 1587
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->showNoItemView(Z)V

    .line 1622
    :cond_1
    :goto_0
    return-void

    .line 1588
    :cond_2
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_1

    .line 1589
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->showNoItemView(Z)V

    goto :goto_0

    .line 1594
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->checkSearchConditionChange()Z

    move-result v0

    if-ne v0, v1, :cond_8

    .line 1595
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsAlreadyDisplayNoResultPopup:Z

    .line 1596
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-eqz v0, :cond_7

    .line 1597
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNoItemView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1598
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsShowHistoryList:Z

    .line 1599
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->showNoItemView(Z)V

    .line 1605
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->resetSearchConditionChange()V

    .line 1612
    :cond_5
    :goto_2
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsAlreadyDisplayNoResultPopup:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->isSearchKeyEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1613
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-nez v0, :cond_6

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_9

    .line 1614
    :cond_6
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNoItemView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 1615
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsShowHistoryList:Z

    .line 1616
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->showNoItemView(Z)V

    .line 1620
    :goto_3
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsAlreadyDisplayNoResultPopup:Z

    goto :goto_0

    .line 1601
    :cond_7
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-nez v0, :cond_4

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_4

    .line 1602
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNoItemView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1603
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->showNoItemView(Z)V

    goto :goto_1

    .line 1607
    :cond_8
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->isSearchKeyEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1608
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsAlreadyDisplayNoResultPopup:Z

    goto :goto_2

    .line 1618
    :cond_9
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_3
.end method

.method public exitSelectionMode()V
    .locals 4

    .prologue
    .line 1201
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->leaveSelectionMode()V

    .line 1202
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->needRedrawForSelect:Z

    .line 1203
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    if-eqz v0, :cond_0

    .line 1204
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    sget v1, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_NORMAL:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setMode(IILjava/lang/Object;)V

    .line 1206
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$26;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$26;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1224
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 1226
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    if-eqz v0, :cond_1

    .line 1227
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->updateItems()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1230
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    .line 1232
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1233
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    .line 1234
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1235
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->clearSelectedCount()V

    .line 1236
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsFilterShow:Z

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->setDropDownIcon(Z)V

    .line 1237
    return-void

    .line 1230
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    throw v0
.end method

.method public getAdapter()Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;
    .locals 1

    .prologue
    .line 1625
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    return-object v0
.end method

.method public getContentsForDetailsDialog()Ljava/util/ArrayList;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1727
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1729
    .local v2, "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v10

    .line 1730
    .local v10, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    if-nez v10, :cond_0

    .line 1731
    sget-object v11, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->TAG:Ljava/lang/String;

    const-string v16, "mediaList is null"

    move-object/from16 v0, v16

    invoke-static {v11, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 1770
    .end local v2    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local v3, "contentList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    return-object v3

    .line 1735
    .end local v3    # "contentList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v2    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v9

    .line 1738
    .local v9, "mediaCount":I
    const/4 v11, 0x1

    if-ne v9, v11, :cond_2

    .line 1739
    const/4 v11, 0x0

    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 1740
    .local v6, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v6, :cond_1

    .line 1741
    new-instance v11, Lcom/sec/samsung/gallery/controller/DetailModel;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaObject;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v11, v0, v1}, Lcom/sec/samsung/gallery/controller/DetailModel;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaDetails;)V

    invoke-virtual {v11}, Lcom/sec/samsung/gallery/controller/DetailModel;->toStringList()Ljava/util/ArrayList;

    move-result-object v2

    .end local v6    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    :goto_1
    move-object v3, v2

    .line 1770
    .restart local v3    # "contentList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    goto :goto_0

    .line 1744
    .end local v3    # "contentList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    const-string v7, ""

    .line 1745
    .local v7, "line1":Ljava/lang/String;
    const-string v8, ""

    .line 1746
    .local v8, "line2":Ljava/lang/String;
    const-wide/16 v14, 0x0

    .line 1747
    .local v14, "sizeSummary":J
    const-wide/16 v12, 0x0

    .line 1750
    .local v12, "size":D
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    if-ge v5, v9, :cond_4

    .line 1751
    :try_start_0
    invoke-interface {v10, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 1752
    .restart local v6    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaObject;->getSize()J

    move-result-wide v16

    const-wide/16 v18, 0x0

    cmp-long v11, v16, v18

    if-lez v11, :cond_3

    .line 1753
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaObject;->getSize()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v16

    add-long v14, v14, v16

    .line 1750
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1756
    .end local v6    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :catch_0
    move-exception v4

    .line 1757
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 1760
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_4
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Long;->doubleValue()D

    move-result-wide v12

    .line 1761
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v16, 0x7f0e01d0

    move/from16 v0, v16

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    move-object/from16 v0, v16

    invoke-static {v11, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 1763
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0e01d1

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v16, ": "

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v0, v12, v13}, Lcom/sec/android/gallery3d/util/GalleryUtils;->formatItemsSelectedSize(Landroid/content/Context;D)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1766
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1767
    const-wide/16 v16, 0x0

    cmpl-double v11, v12, v16

    if-lez v11, :cond_1

    .line 1768
    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method

.method public getDetailPanelHeight()I
    .locals 2

    .prologue
    .line 1038
    const/4 v0, 0x0

    .line 1039
    .local v0, "height":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    add-int/2addr v0, v1

    .line 1040
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->isExistingOpenedPanel()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1041
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mFilterDetailView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    add-int/2addr v0, v1

    .line 1043
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->isExistingSelectedFilter()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1044
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mFilterHorizontalView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    add-int/2addr v0, v1

    .line 1046
    :cond_1
    return v0
.end method

.method public getMediaForDetails()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 3

    .prologue
    .line 1775
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1776
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 1777
    .local v0, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v1, v0, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v1, :cond_0

    .line 1778
    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1782
    .end local v0    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTagFilterHeight()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1025
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-eqz v2, :cond_1

    .line 1026
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->getDetailPanelHeight()I

    move-result v1

    .line 1033
    :cond_0
    :goto_0
    return v1

    .line 1028
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 1030
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    const v3, 0x7f0f024a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1031
    .local v0, "filterTagView":Landroid/view/View;
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/view/View;->measure(II)V

    .line 1033
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    goto :goto_0
.end method

.method protected isAvailableCount(IZ)Z
    .locals 6
    .param p1, "selected"    # I
    .param p2, "showToast"    # Z

    .prologue
    const/16 v5, 0x3e8

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1277
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsPickMode:Z

    if-nez v3, :cond_1

    .line 1290
    :cond_0
    :goto_0
    return v1

    .line 1281
    :cond_1
    if-le p1, v5, :cond_0

    .line 1282
    if-eqz p2, :cond_2

    .line 1283
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0117

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-virtual {v3, v4, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1286
    .local v0, "text":Ljava/lang/CharSequence;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .end local v0    # "text":Ljava/lang/CharSequence;
    :cond_2
    move v1, v2

    .line 1288
    goto :goto_0
.end method

.method public isExistingOpenedPanel()Z
    .locals 4

    .prologue
    .line 1054
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 1056
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 1057
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mFilterGroupView:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1059
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1060
    const/4 v3, 0x1

    .line 1063
    .end local v0    # "child":Landroid/view/View;
    :goto_1
    return v3

    .line 1056
    .restart local v0    # "child":Landroid/view/View;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1063
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public isExistingSelectedFilter()Z
    .locals 1

    .prologue
    .line 1050
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFilterShow()Z
    .locals 1

    .prologue
    .line 612
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsFilterShow:Z

    return v0
.end method

.method public isLastRowKeyboardfocus()Z
    .locals 3

    .prologue
    .line 603
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 604
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    const v2, 0x7f0f024a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;

    .line 605
    .local v0, "view":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;
    if-eqz v0, :cond_0

    .line 606
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->isLastRowFocus()Z

    move-result v1

    .line 609
    .end local v0    # "view":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 1380
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1381
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->setSlideshowSettingEnable(Z)V

    .line 1382
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setSlideShowMode(Z)V

    .line 1383
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1384
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1385
    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    .line 1386
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1387
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->updateCountOnActionBar()V

    .line 1388
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    .line 1413
    :cond_2
    :goto_0
    return-void

    .line 1389
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1390
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->exitSelectionMode()V

    .line 1391
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->redrawLayout(Z)V

    goto :goto_0

    .line 1392
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getCount()I

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->isSearchKeyEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 1393
    :cond_5
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->resetFilterList()V

    .line 1394
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    check-cast v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->removeKeyword()V

    .line 1396
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-eqz v1, :cond_6

    .line 1397
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->resetNewSearchData()V

    .line 1400
    :cond_6
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 1401
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    .line 1402
    .local v0, "view":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;
    if-eqz v0, :cond_2

    .line 1403
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->resetTagList()V

    goto :goto_0

    .line 1407
    .end local v0    # "view":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;
    :cond_7
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    if-eqz v1, :cond_8

    .line 1408
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-virtual {v1, v2, v2, v2, v2}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setGlBackgroundColor(FFFF)V

    .line 1409
    :cond_8
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 1410
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 1411
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 1787
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1788
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGallerySearchUISeperatedMode:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v2

    if-ne v2, p0, :cond_1

    .line 1789
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->prepareActionBar()V

    .line 1790
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->checkTagCloudViewMode()Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->setTagCloudViewMode(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;)V

    .line 1805
    :cond_0
    :goto_0
    return-void

    .line 1791
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    if-eqz v2, :cond_0

    .line 1792
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onConfigChanged(Landroid/content/res/Configuration;)V

    .line 1794
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    if-eqz v2, :cond_2

    .line 1795
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    const v3, 0x7f0f024a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1796
    .local v0, "filterTagView":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 1797
    .local v1, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getActionBarSize(Landroid/content/Context;)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 1798
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1800
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->updateNoItemViewTopMargin()V

    goto :goto_0

    .line 1801
    .end local v0    # "filterTagView":Landroid/view/View;
    .end local v1    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 1802
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->redrawLayout(Z)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    const/high16 v5, 0x80000

    const/16 v3, 0x40

    const/4 v4, 0x1

    .line 311
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsFilterShow:Z

    .line 312
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->isPickMode()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsPickMode:Z

    .line 313
    new-instance v1, Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2, p0}, Lcom/sec/samsung/gallery/view/ActionBarManager;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/app/ActivityState;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 314
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    .line 315
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 317
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eq v1, v2, :cond_0

    .line 318
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .line 319
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setStatusProxy(Lcom/sec/android/gallery3d/app/StateManager;)V

    .line 320
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setTitle(Ljava/lang/String;)V

    .line 323
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getPhotoThumbnailProxy()Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    .line 324
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->getCacheSize()I

    move-result v1

    if-eq v1, v3, :cond_1

    .line 325
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setCacheSize(I)V

    .line 326
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resetContentWindow()V

    .line 327
    new-instance v1, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 328
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0f00bf

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 329
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v0

    .line 330
    .local v0, "basePath":Ljava/lang/String;
    invoke-static {v0, v5}, Lcom/sec/android/gallery3d/app/FilterUtils;->newClusterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mCurrentTopSetPath:Ljava/lang/String;

    .line 331
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-virtual {v1, v5}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->setCurrentClusterType(I)V

    .line 332
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 333
    new-instance v1, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mCurrentTopSetPath:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    .line 334
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewMode()Ljava/lang/Class;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mPreviousViewMode:Ljava/lang/Class;

    .line 335
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const-class v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentViewMode(Ljava/lang/Class;)V

    .line 336
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mPreviousTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 337
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SEARCH:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 338
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mShowProgressDialog:Z

    .line 340
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    .line 346
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->setLoadingListener(Lcom/sec/android/gallery3d/app/LoadingListener;)V

    .line 349
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mPreviousMultiWindowState:Z

    .line 350
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectedItemsListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$OnSelectedItemsListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->setSelectedItemListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$OnSelectedItemsListener;)V

    .line 351
    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHistoryAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;

    .line 353
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->initMultiWindowLayoutListener()V

    .line 354
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1354
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 1355
    return-void
.end method

.method public onDestroy()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1294
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v4, 0x10a0001

    invoke-static {v1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1295
    .local v0, "animation":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mContainer:Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1296
    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$27;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$27;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1331
    invoke-virtual {v0}, Landroid/view/animation/Animation;->getDuration()J

    move-result-wide v4

    const-wide/16 v6, 0x2

    div-long v2, v4, v6

    .line 1332
    .local v2, "duration":J
    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1333
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1335
    .end local v2    # "duration":J
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 1336
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mOnGlobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, v4}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1338
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mPreviousViewMode:Ljava/lang/Class;

    if-eqz v1, :cond_2

    .line 1339
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mPreviousViewMode:Ljava/lang/Class;

    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentViewMode(Ljava/lang/Class;)V

    .line 1341
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1, v8}, Lcom/sec/android/gallery3d/app/StateManager;->setSearchText(Ljava/lang/String;)V

    .line 1342
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1, v8}, Lcom/sec/android/gallery3d/app/StateManager;->setLastQueryText(Ljava/lang/String;)V

    .line 1343
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->clearFilterList()V

    .line 1344
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->onDestroy()V

    .line 1345
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 1364
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_1

    .line 1365
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1376
    :cond_0
    :goto_0
    return-void

    .line 1371
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0f0284

    if-ne v0, v1, :cond_2

    .line 1372
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->goToHome(Landroid/content/Context;)V

    goto :goto_0

    .line 1374
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onOptionsItemSelected(Landroid/view/MenuItem;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 1104
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-nez v1, :cond_0

    .line 1105
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    .line 1106
    .local v0, "view":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;
    if-eqz v0, :cond_0

    .line 1107
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->cleanUp()V

    .line 1110
    .end local v0    # "view":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->pause()V

    .line 1111
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->onPause()V

    .line 1112
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->getScrollPosition()I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mScrolledPosition:I

    .line 1113
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->pause()V

    .line 1114
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1115
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "GALLERY_SEARCH_VIEW"

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 1116
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mPreviousTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 1117
    return-void
.end method

.method public onPrepareOptionMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1359
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 1360
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1068
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 1069
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V

    .line 1070
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mWindowContentChangedListener:Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setWindowContentChangedListener(Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;)V

    .line 1071
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resume()V

    .line 1072
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setVisibility(I)V

    .line 1074
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 1075
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    .line 1076
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mScrolledPosition:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setScrollPosition(I)V

    .line 1077
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1078
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    sget v1, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_SELECT:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setMode(IILjava/lang/Object;)V

    .line 1081
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->attachLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;)V

    .line 1083
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->prepareActionBar()V

    .line 1084
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->registerHistoryListener()V

    .line 1086
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v1, "SLIDESHOW_SETTINGS"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 1087
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mShowSlideshowsettingsMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 1089
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1090
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setSlideShowMode(Z)V

    .line 1091
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->checkTagCloudViewMode()Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->setTagCloudViewMode(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;)V

    .line 1093
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsFilterShow:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudViewMode:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->LIST:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    if-ne v0, v1, :cond_3

    .line 1094
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudViewMode:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->LIST:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 1095
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1099
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->onResume()V

    .line 1100
    return-void

    .line 1096
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudViewMode:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->LIST:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;

    if-eqz v0, :cond_3

    .line 1097
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, v3}, Landroid/widget/ExpandableListView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 1349
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mPreviousTabTagType:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 1350
    return-void
.end method

.method public onStateResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1, "request"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1532
    const/4 v2, 0x0

    .line 1533
    .local v2, "result":Ljava/lang/String;
    const/16 v3, 0x22b

    if-ne p1, v3, :cond_2

    const/4 v3, -0x1

    if-ne p2, v3, :cond_2

    .line 1534
    const-string v3, "android.speech.extra.RESULTS"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1535
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v0, :cond_1

    .line 1552
    .end local v0    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    return-void

    .line 1537
    .restart local v0    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "result":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 1538
    .restart local v2    # "result":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1539
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mVoiceRecog:Z

    .line 1540
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    check-cast v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    invoke-virtual {v3, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->onVoiceResult(Ljava/lang/String;)V

    .line 1541
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    if-eqz v3, :cond_0

    .line 1542
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v3, v2}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->addKeyword(Ljava/lang/String;)V

    goto :goto_0

    .line 1545
    .end local v0    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    const/16 v3, 0x9

    if-ne p1, v3, :cond_0

    .line 1546
    const/4 v3, 0x3

    new-array v1, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v3, v1, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v5

    const/4 v3, 0x2

    aput-object p3, v1, v3

    .line 1549
    .local v1, "params":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v4, "ADD_SLIDESHOW_MUSIC"

    invoke-virtual {v3, v4, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 6
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 358
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V

    .line 359
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->setImeVisibility(Z)V

    .line 360
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v2, "searchColWide"

    const/4 v3, 0x4

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v4, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setAttrs(II)V

    .line 362
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v2, "searchColPort"

    const/4 v3, 0x3

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v5, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setAttrs(II)V

    .line 364
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setOnFocusChangedListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;)V

    .line 365
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$5;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$5;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setOnScrollListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;)V

    .line 388
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$6;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$6;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setOnItemClickListenerEx(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListenerEx;)V

    .line 406
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$7;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$7;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setOnItemClickListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;)V

    .line 417
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$8;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$8;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setOnKeyListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;)V

    .line 432
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$9;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$9;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setOnItemLongClickListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemLongClickListener;)V

    .line 451
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    const/4 v1, 0x0

    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$10;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$10;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setOnGenericMotionListener(ILcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;)V

    .line 474
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$11;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$11;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    invoke-virtual {v0, v5, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setOnGenericMotionListener(ILcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;)V

    .line 497
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$12;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$12;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setOnStatusChangedListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;)V

    .line 513
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$13;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$13;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setOnPenSelectionListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnPenSelectionListener;)V

    .line 528
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$14;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$14;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setOnHoverListenerEx(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnHoverListenerEx;)V

    .line 539
    return-void
.end method

.method protected prepareActionBar()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1808
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-nez v5, :cond_2

    move v0, v3

    .line 1809
    .local v0, "firstLaunch":Z
    :goto_0
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mVoiceRecog:Z

    if-nez v5, :cond_4

    .line 1810
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1811
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1812
    new-instance v5, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v5, v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    .line 1815
    :goto_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1819
    :goto_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v5, :cond_0

    .line 1820
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->resetLayout()V

    .line 1822
    :cond_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    if-eqz v5, :cond_1

    .line 1823
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onResume()V

    .line 1824
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1825
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1826
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1827
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v4, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v4, v5}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1842
    :cond_1
    :goto_3
    return-void

    .end local v0    # "firstLaunch":Z
    :cond_2
    move v0, v4

    .line 1808
    goto :goto_0

    .line 1814
    .restart local v0    # "firstLaunch":Z
    :cond_3
    new-instance v5, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v5, v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    goto :goto_1

    .line 1817
    :cond_4
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mVoiceRecog:Z

    goto :goto_2

    .line 1829
    :cond_5
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    .line 1830
    .local v2, "selectedCount":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getCount()I

    move-result v5

    if-ne v2, v5, :cond_6

    move v1, v3

    .line 1831
    .local v1, "isSelectAll":Z
    :goto_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v5, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v3, v5, v4, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 1832
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    goto :goto_3

    .end local v1    # "isSelectAll":Z
    :cond_6
    move v1, v4

    .line 1830
    goto :goto_4

    .line 1835
    .end local v2    # "selectedCount":I
    :cond_7
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-eqz v4, :cond_1

    .line 1837
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsFilterShow:Z

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->setDropDownIcon(Z)V

    .line 1838
    if-eqz v0, :cond_1

    .line 1839
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->setImeVisibility(Z)V

    goto :goto_3
.end method

.method public registerHistoryListener()V
    .locals 2

    .prologue
    .line 735
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    instance-of v0, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    if-eqz v0, :cond_0

    .line 736
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    new-instance v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$21;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$21;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->setShowHistoryListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$OnShowHistoryListListener;)V

    .line 745
    :cond_0
    return-void
.end method

.method public resetNewSearchData()V
    .locals 1

    .prologue
    .line 1950
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectedItemView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListView;->resetData()V

    .line 1951
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->resetSelectedItem()Ljava/util/ArrayList;

    .line 1952
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->refreshTagList()V

    .line 1953
    return-void
.end method

.method public selectMedia(I)V
    .locals 8
    .param p1, "position"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1241
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    if-nez v2, :cond_1

    .line 1263
    :cond_0
    :goto_0
    return-void

    .line 1245
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 1246
    .local v0, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v0, :cond_0

    .line 1250
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1251
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v5

    const v6, 0x7f0e029e

    new-array v7, v3, [Ljava/lang/Object;

    move-object v2, v0

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v7, v4

    invoke-virtual {v5, v6, v7}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 1256
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->toggle(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1257
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getCount()I

    move-result v5

    if-ne v2, v5, :cond_3

    move v1, v3

    .line 1258
    .local v1, "selected":Z
    :goto_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v5

    invoke-virtual {p0, v5, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->isAvailableCount(IZ)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateDoneButton(Z)V

    .line 1259
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    .line 1260
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 1261
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v3, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v2, v3, v4, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 1262
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    goto :goto_0

    .line 1253
    .end local v1    # "selected":Z
    :cond_2
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v5

    const v6, 0x7f0e029d

    new-array v7, v3, [Ljava/lang/Object;

    move-object v2, v0

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v7, v4

    invoke-virtual {v5, v6, v7}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    move v1, v4

    .line 1257
    goto :goto_2
.end method

.method public setImeVisibility(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 1524
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    check-cast v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->setImeVisibility(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1528
    :goto_0
    return-void

    .line 1525
    :catch_0
    move-exception v0

    .line 1526
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setItemFocus(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x0

    .line 581
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getGenericMotionFocus()I

    move-result v0

    .line 582
    .local v0, "genericMotionFocus":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->setGenericMotionFocus(I)V

    .line 583
    if-ltz v0, :cond_0

    .line 584
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v1, v0, v2}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->notifyDataSetChanged(II)V

    .line 586
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v1, p1, v2}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->notifyDataSetChanged(II)V

    .line 587
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->setGenericMotionKeywordFocus(I)V

    .line 588
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->notifyDataSetChanged(II)V

    .line 589
    return-void
.end method

.method public setKeywordFocus(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x1

    .line 592
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getGenericMotionKeywordFocus()I

    move-result v0

    .line 593
    .local v0, "genericMotionFocus":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->setGenericMotionKeywordFocus(I)V

    .line 594
    if-ltz v0, :cond_0

    .line 595
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v1, v0, v2}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->notifyDataSetChanged(II)V

    .line 597
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v1, p1, v2}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->notifyDataSetChanged(II)V

    .line 598
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->setGenericMotionFocus(I)V

    .line 599
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->notifyDataSetChanged(II)V

    .line 600
    return-void
.end method

.method public setupTagViewMode()V
    .locals 3

    .prologue
    .line 992
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->checkTagCloudViewMode()Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    move-result-object v0

    .line 993
    .local v0, "mode":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudViewMode:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    if-eq v0, v1, :cond_0

    .line 994
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudViewMode:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    .line 995
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->initSearchView()V

    .line 997
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->LIST:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    if-ne v0, v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->setTagCloudViewListMode(Z)V

    .line 998
    return-void

    .line 997
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method showDeleteDialog()V
    .locals 3

    .prologue
    .line 1630
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showDeleteDialog(ZZ)V

    .line 1631
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 17
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1444
    :try_start_0
    move-object/from16 v0, p2

    check-cast v0, Lcom/sec/samsung/gallery/core/Event;

    move-object v4, v0

    .line 1445
    .local v4, "ev":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v4}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v5

    .line 1446
    .local v5, "eventType":I
    sget v13, Lcom/sec/samsung/gallery/core/Event;->EVENT_PRESS_SEARCH:I

    if-ne v5, v13, :cond_2

    .line 1447
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mShowProgressDialog:Z

    .line 1448
    invoke-virtual {v4}, Lcom/sec/samsung/gallery/core/Event;->getData()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 1452
    .local v10, "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v13, v10}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->addKeyword(Ljava/lang/String;)V

    .line 1453
    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->setImeVisibility(Z)V

    .line 1459
    .end local v10    # "key":Ljava/lang/String;
    :cond_0
    :goto_0
    sget v13, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_DELETE_DIALOG:I

    if-ne v5, v13, :cond_4

    .line 1460
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->showDeleteDialog()V

    .line 1520
    .end local v4    # "ev":Lcom/sec/samsung/gallery/core/Event;
    .end local v5    # "eventType":I
    :cond_1
    :goto_1
    return-void

    .line 1454
    .restart local v4    # "ev":Lcom/sec/samsung/gallery/core/Event;
    .restart local v5    # "eventType":I
    :cond_2
    sget v13, Lcom/sec/samsung/gallery/core/Event;->EVENT_TAG_LIST_UPDATE_SEARCH:I

    if-ne v5, v13, :cond_3

    .line 1455
    invoke-virtual {v4}, Lcom/sec/samsung/gallery/core/Event;->getData()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Boolean;

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    .line 1456
    .local v9, "isShow":Z
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->updateTagListView(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1517
    .end local v4    # "ev":Lcom/sec/samsung/gallery/core/Event;
    .end local v5    # "eventType":I
    .end local v9    # "isShow":Z
    :catch_0
    move-exception v3

    .line 1518
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 1457
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v4    # "ev":Lcom/sec/samsung/gallery/core/Event;
    .restart local v5    # "eventType":I
    :cond_3
    :try_start_1
    sget v13, Lcom/sec/samsung/gallery/core/Event;->EVENT_EXIT_SELECTION_MODE:I

    if-ne v5, v13, :cond_0

    .line 1458
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->exitSelectionMode()V

    goto :goto_0

    .line 1461
    :cond_4
    sget v13, Lcom/sec/samsung/gallery/core/Event;->EVENT_SELECT_ALL:I

    if-ne v5, v13, :cond_5

    .line 1462
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->loadContentData(Z)V

    .line 1463
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->selectAll()V

    goto :goto_1

    .line 1464
    :cond_5
    sget v13, Lcom/sec/samsung/gallery/core/Event;->EVENT_UNSELECT_ALL:I

    if-ne v5, v13, :cond_6

    .line 1465
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->unselectAll()V

    goto :goto_1

    .line 1466
    :cond_6
    sget v13, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_SLIDESHOW_SETTING:I

    if-ne v5, v13, :cond_a

    .line 1467
    sget-boolean v13, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideSettingPopup:Z

    if-nez v13, :cond_7

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v13}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isExternalDisplayAvailable(Landroid/content/Context;)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 1468
    :cond_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1469
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v14, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v14, v15}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v13, v14}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1470
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v14, "SLIDE_SHOW_SETTING_VIEW"

    invoke-virtual {v13, v14}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 1471
    new-instance v13, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    const-string v14, "SLIDE_SHOW_SETTING_VIEW"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v13, v14, v15}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;-><init>(Ljava/lang/String;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    .line 1472
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->setSlideshowSettingEnable(Z)V

    .line 1477
    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    if-eqz v13, :cond_8

    .line 1478
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setSlideShowMode(Z)V

    .line 1479
    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    if-eqz v13, :cond_1

    .line 1480
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/GallerySearchAlbum;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/app/StateManager;->getSearchText()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/android/gallery3d/data/GallerySearchAlbum;->addKeyword(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1474
    :cond_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v14, "PREPARE_SLIDE_SHOW_DATA"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v13, v14, v15}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2

    .line 1482
    :cond_a
    sget v13, Lcom/sec/samsung/gallery/core/Event;->EVENT_ADD_SUGGESTION:I

    if-ne v5, v13, :cond_d

    .line 1483
    invoke-virtual {v4}, Lcom/sec/samsung/gallery/core/Event;->getData()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Ljava/lang/Object;

    move-object v0, v13

    check-cast v0, [Ljava/lang/Object;

    move-object v12, v0

    .line 1484
    .local v12, "params":[Ljava/lang/Object;
    const/4 v13, 0x0

    aget-object v1, v12, v13

    check-cast v1, Landroid/database/MatrixCursor;

    .line 1485
    .local v1, "cursor":Landroid/database/MatrixCursor;
    const/4 v13, 0x1

    aget-object v6, v12, v13

    check-cast v6, Ljava/lang/String;

    .line 1486
    .local v6, "filterText":Ljava/lang/String;
    const/4 v13, 0x2

    aget-object v8, v12, v13

    check-cast v8, Ljava/lang/Integer;

    .line 1487
    .local v8, "isHistoryOnly":Ljava/lang/Integer;
    const/4 v7, 0x0

    .line 1488
    .local v7, "historyOnly":Z
    if-eqz v8, :cond_b

    .line 1489
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v13

    if-nez v13, :cond_c

    .line 1490
    const/4 v7, 0x0

    .line 1495
    :cond_b
    :goto_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v6, v7}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->updateSuggestionList(Landroid/database/MatrixCursor;Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 1492
    :cond_c
    const/4 v7, 0x1

    goto :goto_3

    .line 1496
    .end local v1    # "cursor":Landroid/database/MatrixCursor;
    .end local v6    # "filterText":Ljava/lang/String;
    .end local v7    # "historyOnly":Z
    .end local v8    # "isHistoryOnly":Ljava/lang/Integer;
    .end local v12    # "params":[Ljava/lang/Object;
    :cond_d
    sget v13, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTERKEYWORD_SEARCH:I

    if-ne v5, v13, :cond_f

    .line 1497
    invoke-virtual {v4}, Lcom/sec/samsung/gallery/core/Event;->getData()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 1498
    .restart local v10    # "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTempKeyword:Ljava/lang/String;

    .line 1499
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    if-eqz v13, :cond_1

    .line 1500
    sget-boolean v13, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-eqz v13, :cond_e

    .line 1501
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->getDetailPanelHeight()I

    move-result v14

    invoke-virtual {v13, v14}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->setFilterTagViewHeight(I)V

    .line 1502
    :cond_e
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v13, v10}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->changeKeyword(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1504
    .end local v10    # "key":Ljava/lang/String;
    :cond_f
    sget v13, Lcom/sec/samsung/gallery/core/Event;->EVENT_PRESS_VOICE_SEARCH:I

    if-ne v5, v13, :cond_10

    .line 1505
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    if-eqz v13, :cond_1

    .line 1506
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBar:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    check-cast v13, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->setVoiceSearchState(Z)V

    goto/16 :goto_1

    .line 1508
    :cond_10
    sget v13, Lcom/sec/samsung/gallery/core/Event;->EVENT_COPY_FILES:I

    if-ne v5, v13, :cond_1

    .line 1509
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v2

    .line 1510
    .local v2, "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    if-eqz v2, :cond_1

    .line 1511
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v13

    const/4 v14, 0x7

    invoke-virtual {v13, v14}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v11

    .line 1513
    .local v11, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/4 v14, 0x0

    const/4 v15, 0x1

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v13, v11, v14, v15, v0}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAlbumListDialogToCopyMove(Lcom/sec/android/gallery3d/data/MediaSet;IZI)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method public updateSuggestionList(Landroid/database/MatrixCursor;Ljava/lang/String;Z)V
    .locals 10
    .param p1, "cursor"    # Landroid/database/MatrixCursor;
    .param p2, "filterText"    # Ljava/lang/String;
    .param p3, "isOnlyHistory"    # Z

    .prologue
    .line 1416
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v5, p3}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getSuggestionNames(Z)Ljava/util/List;

    move-result-object v3

    .line 1417
    .local v3, "suggestionNameList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-ge v0, v5, :cond_7

    .line 1418
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;

    iget-object v2, v5, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;->mKeyword:Ljava/lang/String;

    .line 1419
    .local v2, "suggestionName":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;

    iget-wide v8, v5, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;->mTime:J

    invoke-virtual {v6, v8, v9}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getSuggestionHistoryTime(J)Ljava/lang/String;

    move-result-object v4

    .line 1420
    .local v4, "time":Ljava/lang/String;
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-eqz v5, :cond_1

    .line 1421
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1422
    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const/4 v7, 0x0

    aput-object v7, v5, v6

    const/4 v6, 0x2

    aput-object v2, v5, v6

    const/4 v6, 0x3

    aput-object v4, v5, v6

    const/4 v6, 0x4

    aput-object p2, v5, v6

    invoke-virtual {p1, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 1417
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1423
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1424
    :cond_2
    const/4 v1, 0x0

    .line 1425
    .local v1, "resourceId":Ljava/lang/String;
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGallerySearchUISeperatedMode:Z

    if-eqz v5, :cond_3

    .line 1426
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0455

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1427
    const v5, 0x7f020436

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 1436
    :cond_3
    :goto_2
    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v1, v5, v6

    const/4 v6, 0x2

    aput-object v2, v5, v6

    const/4 v6, 0x3

    aput-object v4, v5, v6

    const/4 v6, 0x4

    aput-object p2, v5, v6

    invoke-virtual {p1, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_1

    .line 1428
    :cond_4
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0453

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1429
    const v5, 0x7f020437

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 1430
    :cond_5
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0454

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1431
    const v5, 0x7f020438

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 1433
    :cond_6
    const v5, 0x7f020439

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 1439
    .end local v1    # "resourceId":Ljava/lang/String;
    .end local v2    # "suggestionName":Ljava/lang/String;
    .end local v4    # "time":Ljava/lang/String;
    :cond_7
    return-void
.end method

.method public updateTagListView(Z)V
    .locals 5
    .param p1, "isShow"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1125
    const/4 v0, 0x0

    .line 1126
    .local v0, "tagListView":Landroid/view/View;
    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$31;->$SwitchMap$com$sec$samsung$gallery$view$gallerysearch$GallerySearchViewState$TagCloudViewMode:[I

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudViewMode:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1141
    :goto_0
    if-eqz p1, :cond_2

    .line 1142
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f04001b

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1144
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1145
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsFilterShow:Z

    .line 1152
    :goto_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->updateNoItemViewTopMargin()V

    .line 1154
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    if-eqz v1, :cond_0

    .line 1155
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->needRedrawForSelect:Z

    if-nez v1, :cond_3

    .line 1156
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->redrawLayout(Z)V

    .line 1160
    :cond_0
    :goto_2
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->needRedrawForSelect:Z

    .line 1161
    return-void

    .line 1128
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;

    .line 1129
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->setFilterVisiblility(Z)V

    goto :goto_0

    .line 1133
    :pswitch_1
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-eqz v1, :cond_1

    .line 1134
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mNewSearchTagFilterView:Landroid/view/View;

    .line 1137
    :goto_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->setFilterVisiblility(Z)V

    goto :goto_0

    .line 1136
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchTagFilterView:Landroid/view/View;

    goto :goto_3

    .line 1147
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f04001c

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1149
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1150
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mIsFilterShow:Z

    goto :goto_1

    .line 1158
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->redrawLayout(Z)V

    goto :goto_2

    .line 1126
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
