.class public Lcom/sec/samsung/gallery/view/common/DetailsDialog;
.super Lcom/sec/samsung/gallery/view/ViewObservable;
.source "DetailsDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;,
        Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "DetailsDialog"


# instance fields
.field private mContentsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected final mCtx:Landroid/content/Context;

.field private mDetailItemLayout:I

.field private mDetailsListAdapter:Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;

.field private mDialogFragment:Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment;

.field private mSDCardRemovedReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 41
    .local p2, "contents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/ViewObservable;-><init>()V

    .line 64
    new-instance v0, Lcom/sec/samsung/gallery/view/common/DetailsDialog$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/common/DetailsDialog$1;-><init>(Lcom/sec/samsung/gallery/view/common/DetailsDialog;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mSDCardRemovedReceiver:Landroid/content/BroadcastReceiver;

    .line 42
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mCtx:Landroid/content/Context;

    .line 43
    new-instance v0, Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mCtx:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment;-><init>(Lcom/sec/samsung/gallery/view/common/DetailsDialog;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment;

    .line 44
    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->setContensList(Ljava/util/ArrayList;)V

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/common/DetailsDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->unregisterSdCardReceiver()V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/view/common/DetailsDialog;Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;)Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/DetailsDialog;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mDetailsListAdapter:Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/common/DetailsDialog;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mContentsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/common/DetailsDialog;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mSDCardRemovedReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/common/DetailsDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    .prologue
    .line 32
    iget v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mDetailItemLayout:I

    return v0
.end method

.method private unregisterSdCardReceiver()V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mCtx:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mSDCardRemovedReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mCtx:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mSDCardRemovedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mSDCardRemovedReceiver:Landroid/content/BroadcastReceiver;

    .line 78
    :cond_0
    return-void
.end method


# virtual methods
.method public dismissDialog()V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment;->dismiss()V

    .line 178
    return-void
.end method

.method protected initDetailList(Landroid/widget/ListView;I)V
    .locals 1
    .param p1, "detailsList"    # Landroid/widget/ListView;
    .param p2, "layout"    # I

    .prologue
    .line 106
    iput p2, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mDetailItemLayout:I

    .line 107
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mDetailsListAdapter:Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 108
    return-void
.end method

.method public isShowing()Z
    .locals 2

    .prologue
    .line 170
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 171
    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    const/4 v1, 0x1

    .line 173
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onCreateDialog()Landroid/app/Dialog;
    .locals 8

    .prologue
    const/16 v7, 0x400

    .line 81
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mCtx:Landroid/content/Context;

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 83
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030050

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    .line 84
    .local v2, "detailsList":Landroid/widget/ListView;
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 85
    const v4, 0x7f03004e

    invoke-virtual {p0, v2, v4}, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->initDetailList(Landroid/widget/ListView;I)V

    .line 86
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mCtx:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 87
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0e004b

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0e004c

    new-instance v6, Lcom/sec/samsung/gallery/view/common/DetailsDialog$2;

    invoke-direct {v6, p0}, Lcom/sec/samsung/gallery/view/common/DetailsDialog$2;-><init>(Lcom/sec/samsung/gallery/view/common/DetailsDialog;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 94
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 95
    .local v1, "detailDialog":Landroid/app/Dialog;
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    const v5, 0x10100

    invoke-virtual {v4, v5}, Landroid/view/Window;->addFlags(I)V

    .line 98
    invoke-virtual {v1}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFullScreen(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 99
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4, v7, v7}, Landroid/view/Window;->setFlags(II)V

    .line 102
    :cond_0
    return-object v1
.end method

.method public onDialogDestroy()V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method protected onDialogDismiss()V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->unregisterSdCardReceiver()V

    .line 61
    const-string v0, "DetailsDialog"

    const-string v1, "dialog dismissed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    return-void
.end method

.method protected setContensList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p1, "contents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mContentsList:Ljava/util/ArrayList;

    .line 49
    return-void
.end method

.method public showDialog()V
    .locals 4

    .prologue
    .line 163
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mCtx:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v3, "dialog"

    invoke-virtual {v2, v1, v3}, Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    :goto_0
    return-void

    .line 164
    :catch_0
    move-exception v0

    .line 165
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "DetailsDialog"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can not display details dialog. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateDetailDialogList()V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mDetailsListAdapter:Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mDetailsListAdapter:Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;->notifyDataSetChanged()V

    .line 54
    :cond_0
    return-void
.end method
