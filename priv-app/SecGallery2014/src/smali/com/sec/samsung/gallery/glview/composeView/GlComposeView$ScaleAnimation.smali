.class public Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlComposeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ScaleAnimation"
.end annotation


# instance fields
.field private mClickedIndex:I

.field private mEnd:F

.field private mStart:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 0

    .prologue
    .line 1813
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;

    .prologue
    .line 1813
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->mClickedIndex:I

    return v0
.end method

.method static synthetic access$1602(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;
    .param p1, "x1"    # I

    .prologue
    .line 1813
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->mClickedIndex:I

    return p1
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 5
    .param p1, "ratio"    # F

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1824
    sub-float v2, v4, p1

    sub-float v3, v4, p1

    mul-float/2addr v2, v3

    sub-float v0, v4, v2

    .line 1827
    .local v0, "proRatio":F
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->mStart:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->mEnd:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->mStart:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v0

    add-float v1, v2, v3

    .line 1828
    .local v1, "scale":F
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

    invoke-virtual {v2, v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->doScale(FF)Z

    .line 1829
    return-void
.end method

.method protected onCancel()V
    .locals 2

    .prologue
    .line 1843
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 1844
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->stopScaleAnimation()V

    .line 1845
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnScaling:Z

    .line 1846
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1832
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->getLastRatio()F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->isCreated()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1833
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->applyTransform(F)V

    .line 1836
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 1837
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScaleCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->stopScaleAnimation()V

    .line 1838
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnScaling:Z

    .line 1839
    return-void
.end method

.method public startScale(FF)V
    .locals 0
    .param p1, "start"    # F
    .param p2, "end"    # F

    .prologue
    .line 1818
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->mStart:F

    .line 1819
    iput p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->mEnd:F

    .line 1820
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleAnimation;->start()V

    .line 1821
    return-void
.end method
