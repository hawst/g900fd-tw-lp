.class Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote$1;
.super Landroid/print/PrintDocumentAdapter;
.source "PrintImageForPhotoNote.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;->doPrint()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mAttributes:Landroid/print/PrintAttributes;

.field final synthetic this$0:Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;

    invoke-direct {p0}, Landroid/print/PrintDocumentAdapter;-><init>()V

    return-void
.end method

.method private getMatrix(IILandroid/graphics/RectF;I)Landroid/graphics/Matrix;
    .locals 7
    .param p1, "imageWidth"    # I
    .param p2, "imageHeight"    # I
    .param p3, "content"    # Landroid/graphics/RectF;
    .param p4, "fittingMode"    # I

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 182
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 185
    .local v0, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v4

    int-to-float v5, p1

    div-float v1, v4, v5

    .line 186
    .local v1, "scale":F
    const/4 v4, 0x2

    if-ne p4, v4, :cond_0

    .line 187
    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v4

    int-to-float v5, p2

    div-float/2addr v4, v5

    invoke-static {v1, v4}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 191
    :goto_0
    invoke-virtual {v0, v1, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 194
    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v4

    int-to-float v5, p1

    mul-float/2addr v5, v1

    sub-float/2addr v4, v5

    div-float v2, v4, v6

    .line 195
    .local v2, "translateX":F
    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v4

    int-to-float v5, p2

    mul-float/2addr v5, v1

    sub-float/2addr v4, v5

    div-float v3, v4, v6

    .line 196
    .local v3, "translateY":F
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 197
    return-object v0

    .line 189
    .end local v2    # "translateX":F
    .end local v3    # "translateY":F
    :cond_0
    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v4

    int-to-float v5, p2

    div-float/2addr v4, v5

    invoke-static {v1, v4}, Ljava/lang/Math;->min(FF)F

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method public onLayout(Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$LayoutResultCallback;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "oldAttributes"    # Landroid/print/PrintAttributes;
    .param p2, "newAttributes"    # Landroid/print/PrintAttributes;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .param p4, "callback"    # Landroid/print/PrintDocumentAdapter$LayoutResultCallback;
    .param p5, "metadata"    # Landroid/os/Bundle;

    .prologue
    const/4 v0, 0x1

    .line 127
    iput-object p2, p0, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote$1;->mAttributes:Landroid/print/PrintAttributes;

    .line 128
    invoke-virtual {p3}, Landroid/os/CancellationSignal;->isCanceled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 129
    invoke-virtual {p4}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutCancelled()V

    .line 140
    :goto_0
    return-void

    .line 132
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;

    iget-object v2, v2, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;->mUri:Landroid/net/Uri;

    if-eqz v2, :cond_2

    .line 133
    new-instance v2, Landroid/print/PrintDocumentInfo$Builder;

    const-string v3, "gallery"

    invoke-direct {v2, v3}, Landroid/print/PrintDocumentInfo$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Landroid/print/PrintDocumentInfo$Builder;->setContentType(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/print/PrintDocumentInfo$Builder;->setPageCount(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/print/PrintDocumentInfo$Builder;->build()Landroid/print/PrintDocumentInfo;

    move-result-object v1

    .line 135
    .local v1, "info":Landroid/print/PrintDocumentInfo;
    invoke-virtual {p2, p1}, Landroid/print/PrintAttributes;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 136
    .local v0, "changed":Z
    :goto_1
    invoke-virtual {p4, v1, v0}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFinished(Landroid/print/PrintDocumentInfo;Z)V

    goto :goto_0

    .line 135
    .end local v0    # "changed":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 138
    .end local v1    # "info":Landroid/print/PrintDocumentInfo;
    :cond_2
    const-string v2, "Page count calculation failed."

    invoke-virtual {p4, v2}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFailed(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onWrite([Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V
    .locals 9
    .param p1, "pageRanges"    # [Landroid/print/PageRange;
    .param p2, "destination"    # Landroid/os/ParcelFileDescriptor;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .param p4, "callback"    # Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    .prologue
    .line 145
    new-instance v3, Landroid/print/pdf/PrintedPdfDocument;

    iget-object v6, p0, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;

    iget-object v6, v6, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;->mContext:Landroid/content/Context;

    check-cast v6, Landroid/app/Activity;

    iget-object v7, p0, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote$1;->mAttributes:Landroid/print/PrintAttributes;

    invoke-direct {v3, v6, v7}, Landroid/print/pdf/PrintedPdfDocument;-><init>(Landroid/content/Context;Landroid/print/PrintAttributes;)V

    .line 146
    .local v3, "mPdfDocument":Landroid/print/pdf/PrintedPdfDocument;
    if-eqz p1, :cond_2

    .line 147
    const/4 v0, 0x0

    .line 148
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v6, p0, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;

    iget-object v6, v6, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;->mUri:Landroid/net/Uri;

    if-eqz v6, :cond_0

    .line 149
    iget-object v6, p0, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;

    iget-object v6, v6, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote;->mUri:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 151
    :cond_0
    invoke-virtual {p3}, Landroid/os/CancellationSignal;->isCanceled()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 152
    invoke-virtual {p4}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteCancelled()V

    .line 153
    invoke-virtual {v3}, Landroid/print/pdf/PrintedPdfDocument;->close()V

    .line 154
    const/4 v3, 0x0

    .line 179
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-void

    .line 157
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Landroid/print/pdf/PrintedPdfDocument;->startPage(I)Landroid/graphics/pdf/PdfDocument$Page;

    move-result-object v5

    .line 158
    .local v5, "page":Landroid/graphics/pdf/PdfDocument$Page;
    new-instance v1, Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/pdf/PdfDocument$Page;->getInfo()Landroid/graphics/pdf/PdfDocument$PageInfo;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/pdf/PdfDocument$PageInfo;->getContentRect()Landroid/graphics/Rect;

    move-result-object v6

    invoke-direct {v1, v6}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 159
    .local v1, "content":Landroid/graphics/RectF;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    const/4 v8, 0x1

    invoke-direct {p0, v6, v7, v1, v8}, Lcom/sec/samsung/gallery/app/photonote/PrintImageForPhotoNote$1;->getMatrix(IILandroid/graphics/RectF;I)Landroid/graphics/Matrix;

    move-result-object v4

    .line 160
    .local v4, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v5}, Landroid/graphics/pdf/PdfDocument$Page;->getCanvas()Landroid/graphics/Canvas;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v0, v4, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 161
    invoke-virtual {v3, v5}, Landroid/print/pdf/PrintedPdfDocument;->finishPage(Landroid/graphics/pdf/PdfDocument$Page;)V

    .line 162
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-nez v6, :cond_2

    .line 163
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 169
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "content":Landroid/graphics/RectF;
    .end local v4    # "matrix":Landroid/graphics/Matrix;
    .end local v5    # "page":Landroid/graphics/pdf/PdfDocument$Page;
    :cond_2
    :try_start_0
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    invoke-virtual {v3, v6}, Landroid/print/pdf/PrintedPdfDocument;->writeTo(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 174
    invoke-virtual {v3}, Landroid/print/pdf/PrintedPdfDocument;->close()V

    .line 175
    const/4 v3, 0x0

    .line 177
    invoke-virtual {p4, p1}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteFinished([Landroid/print/PageRange;)V

    goto :goto_0

    .line 170
    :catch_0
    move-exception v2

    .line 171
    .local v2, "e":Ljava/io/IOException;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p4, v6}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteFailed(Ljava/lang/CharSequence;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 174
    invoke-virtual {v3}, Landroid/print/pdf/PrintedPdfDocument;->close()V

    .line 175
    const/4 v3, 0x0

    goto :goto_0

    .line 174
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    invoke-virtual {v3}, Landroid/print/pdf/PrintedPdfDocument;->close()V

    .line 175
    const/4 v3, 0x0

    throw v6
.end method
