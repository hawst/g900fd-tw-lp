.class Lcom/sec/samsung/gallery/view/AbstractActionBarView$4$1;
.super Ljava/lang/Object;
.source "AbstractActionBarView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/AbstractActionBarView$4;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/samsung/gallery/view/AbstractActionBarView$4;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/AbstractActionBarView$4;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView$4$1;->this$1:Lcom/sec/samsung/gallery/view/AbstractActionBarView$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 166
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView$4$1;->this$1:Lcom/sec/samsung/gallery/view/AbstractActionBarView$4;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/AbstractActionBarView$4;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->isLaunchFromSetupWidzard()Z

    move-result v0

    .line 167
    .local v0, "fromSetupWidzard":Z
    if-eqz v0, :cond_0

    .line 168
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView$4$1;->this$1:Lcom/sec/samsung/gallery/view/AbstractActionBarView$4;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/AbstractActionBarView$4;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->goToSetupWidzard(Landroid/content/Context;)V

    .line 171
    :goto_0
    return-void

    .line 170
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarView$4$1;->this$1:Lcom/sec/samsung/gallery/view/AbstractActionBarView$4;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/AbstractActionBarView$4;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->goToHome(Landroid/content/Context;)V

    goto :goto_0
.end method
