.class Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;
.super Ljava/lang/Object;
.source "GlBlurObject.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlBlurObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "presetInfo"
.end annotation


# instance fields
.field mAlign:I

.field mIHeight:F

.field mIWidth:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlBlurObject;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/GlBlurObject;III)V
    .locals 1
    .param p2, "align"    # I
    .param p3, "iWidth"    # I
    .param p4, "iHeight"    # I

    .prologue
    .line 241
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 242
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;->mAlign:I

    .line 243
    int-to-float v0, p3

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;->mIWidth:F

    .line 244
    int-to-float v0, p4

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;->mIHeight:F

    .line 245
    return-void
.end method


# virtual methods
.method public set(III)V
    .locals 1
    .param p1, "align"    # I
    .param p2, "iWidth"    # I
    .param p3, "iHeight"    # I

    .prologue
    .line 248
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;->mAlign:I

    .line 249
    int-to-float v0, p2

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;->mIWidth:F

    .line 250
    int-to-float v0, p3

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBlurObject$presetInfo;->mIHeight:F

    .line 251
    return-void
.end method
