.class public Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
.super Landroid/widget/FrameLayout;
.source "GallerySearchFilterGroupDetailView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private bDisplaying:Z

.field protected mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;

.field private mDataSetObserver:Landroid/database/DataSetObserver;

.field protected mFilterClickListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;

.field private mHorizontalSpacing:I

.field protected mPageIndex:I

.field private mVerticalSpacing:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "index"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 60
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 23
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mPageIndex:I

    .line 25
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;

    .line 27
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mFilterClickListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;

    .line 29
    iput v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mHorizontalSpacing:I

    .line 31
    iput v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mVerticalSpacing:I

    .line 33
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->bDisplaying:Z

    .line 35
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView$1;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mDataSetObserver:Landroid/database/DataSetObserver;

    .line 61
    iput p2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mPageIndex:I

    .line 63
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->init()V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 67
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mPageIndex:I

    .line 25
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;

    .line 27
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mFilterClickListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;

    .line 29
    iput v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mHorizontalSpacing:I

    .line 31
    iput v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mVerticalSpacing:I

    .line 33
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->bDisplaying:Z

    .line 35
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView$1;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mDataSetObserver:Landroid/database/DataSetObserver;

    .line 69
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->init()V

    .line 70
    return-void
.end method

.method private init()V
    .locals 7

    .prologue
    .line 73
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-direct {v4, v5, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 75
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0350

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 76
    .local v1, "l":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0351

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    .line 77
    .local v3, "t":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0352

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 78
    .local v2, "r":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0353

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 80
    .local v0, "b":I
    invoke-virtual {p0, v1, v3, v2, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setPadding(IIII)V

    .line 82
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0354

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mHorizontalSpacing:I

    .line 84
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0355

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mVerticalSpacing:I

    .line 87
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0356

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setMinimumHeight(I)V

    .line 89
    return-void
.end method

.method private setChildFrame(Landroid/view/View;IIII)V
    .locals 2
    .param p1, "child"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    .line 262
    add-int v0, p2, p4

    add-int v1, p3, p5

    invoke-virtual {p1, p2, p3, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 263
    return-void
.end method


# virtual methods
.method public findViewByTagData(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)Landroid/view/View;
    .locals 6
    .param p1, "dstData"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getChildCount()I

    move-result v1

    .line 102
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 103
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 104
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 106
    .local v3, "srcData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    if-eqz v3, :cond_0

    .line 107
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 113
    .end local v0    # "child":Landroid/view/View;
    .end local v3    # "srcData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    :goto_1
    return-object v0

    .line 102
    .restart local v0    # "child":Landroid/view/View;
    .restart local v3    # "srcData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 113
    .end local v0    # "child":Landroid/view/View;
    .end local v3    # "srcData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public hasSelectedItems()Z
    .locals 4

    .prologue
    .line 293
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getChildCount()I

    move-result v1

    .line 295
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 296
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 298
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 299
    const/4 v3, 0x1

    .line 303
    .end local v0    # "child":Landroid/view/View;
    :goto_1
    return v3

    .line 295
    .restart local v0    # "child":Landroid/view/View;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 303
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public isDisplaying()Z
    .locals 1

    .prologue
    .line 310
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->bDisplaying:Z

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 289
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 267
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setSelected(Z)V

    .line 268
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mFilterClickListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    invoke-interface {v1, p1, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;->setFilterClick(Landroid/view/View;Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)V

    .line 269
    return-void

    .line 267
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0356

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setMinimumHeight(I)V

    .line 96
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 97
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 13
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getChildCount()I

    move-result v6

    .line 119
    .local v6, "count":I
    sub-int v0, p4, p2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getPaddingLeft()I

    move-result v11

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getPaddingRight()I

    move-result v12

    add-int/2addr v11, v12

    sub-int v10, v0, v11

    .line 120
    .local v10, "parentWidth":I
    sub-int v0, p5, p3

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getPaddingTop()I

    move-result v11

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getPaddingBottom()I

    move-result v12

    add-int/2addr v11, v12

    sub-int v9, v0, v11

    .line 121
    .local v9, "parentHeight":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getPaddingLeft()I

    move-result v2

    .local v2, "dstLeft":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getPaddingTop()I

    move-result v3

    .line 123
    .local v3, "dstTop":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v6, :cond_3

    .line 124
    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 125
    .local v1, "child":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/widget/FrameLayout$LayoutParams;

    .line 127
    .local v8, "lp":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v11, 0x8

    if-ne v0, v11, :cond_0

    .line 123
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 131
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 132
    .local v4, "childWidth":I
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 134
    .local v5, "childHeight":I
    const/4 v0, 0x1

    if-ne v6, v0, :cond_2

    iget v0, v8, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    and-int/lit8 v0, v0, 0x70

    const/16 v11, 0x10

    if-ne v0, v11, :cond_2

    iget v0, v8, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    and-int/lit8 v0, v0, 0x7

    const/4 v11, 0x1

    if-ne v0, v11, :cond_2

    .line 137
    div-int/lit8 v0, v10, 0x2

    div-int/lit8 v11, v4, 0x2

    sub-int/2addr v0, v11

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getPaddingLeft()I

    move-result v11

    add-int v2, v0, v11

    .line 138
    div-int/lit8 v0, v9, 0x2

    div-int/lit8 v11, v5, 0x2

    sub-int/2addr v0, v11

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getPaddingTop()I

    move-result v11

    add-int v3, v0, v11

    :cond_1
    :goto_2
    move-object v0, p0

    .line 147
    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setChildFrame(Landroid/view/View;IIII)V

    .line 149
    iget v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mHorizontalSpacing:I

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    goto :goto_1

    .line 140
    :cond_2
    add-int v0, v2, v4

    if-le v0, v10, :cond_1

    .line 142
    iget v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mVerticalSpacing:I

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 143
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getPaddingLeft()I

    move-result v2

    goto :goto_2

    .line 151
    .end local v1    # "child":Landroid/view/View;
    .end local v4    # "childWidth":I
    .end local v5    # "childHeight":I
    .end local v8    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    :cond_3
    return-void
.end method

.method protected onMeasure(II)V
    .locals 17
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 155
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v12

    .line 156
    .local v12, "width":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    .line 157
    .local v8, "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getPaddingLeft()I

    move-result v14

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getPaddingRight()I

    move-result v15

    add-int/2addr v14, v15

    sub-int v3, v12, v14

    .line 158
    .local v3, "availableWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getPaddingTop()I

    move-result v14

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getPaddingBottom()I

    move-result v15

    add-int/2addr v14, v15

    sub-int v2, v8, v14

    .line 159
    .local v2, "availableHeight":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getChildCount()I

    move-result v7

    .line 160
    .local v7, "count":I
    const/4 v13, 0x0

    .local v13, "widthSum":I
    const/4 v9, 0x0

    .line 162
    .local v9, "heightSum":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    if-ge v10, v7, :cond_4

    .line 163
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 164
    .local v4, "child":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/widget/FrameLayout$LayoutParams;

    .line 166
    .local v11, "lp":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v14

    const/16 v15, 0x8

    if-ne v14, v15, :cond_0

    .line 162
    :goto_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 170
    :cond_0
    iget v14, v11, Landroid/widget/FrameLayout$LayoutParams;->width:I

    const/4 v15, -0x1

    if-ne v14, v15, :cond_1

    iget v14, v11, Landroid/widget/FrameLayout$LayoutParams;->height:I

    const/4 v15, -0x1

    if-ne v14, v15, :cond_1

    .line 171
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getMinimumHeight()I

    move-result v14

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getPaddingTop()I

    move-result v15

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getPaddingBottom()I

    move-result v16

    add-int v15, v15, v16

    sub-int v2, v14, v15

    .line 173
    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v3, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v14

    const/high16 v15, 0x40000000    # 2.0f

    invoke-static {v2, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    invoke-virtual {v4, v14, v15}, Landroid/view/View;->measure(II)V

    .line 180
    :goto_2
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    .line 181
    .local v6, "childWidth":I
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 183
    .local v5, "childHeight":I
    add-int v14, v13, v6

    if-le v14, v3, :cond_2

    .line 184
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mVerticalSpacing:I

    add-int/2addr v14, v5

    add-int/2addr v9, v14

    .line 185
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mHorizontalSpacing:I

    add-int v13, v6, v14

    goto :goto_1

    .line 176
    .end local v5    # "childHeight":I
    .end local v6    # "childWidth":I
    :cond_1
    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-static {v14, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v14

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v15 .. v16}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    invoke-virtual {v4, v14, v15}, Landroid/view/View;->measure(II)V

    goto :goto_2

    .line 187
    .restart local v5    # "childHeight":I
    .restart local v6    # "childWidth":I
    :cond_2
    if-nez v9, :cond_3

    .line 188
    add-int/2addr v9, v5

    .line 191
    :cond_3
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mHorizontalSpacing:I

    add-int/2addr v14, v6

    add-int/2addr v13, v14

    goto :goto_1

    .line 195
    .end local v4    # "child":Landroid/view/View;
    .end local v5    # "childHeight":I
    .end local v6    # "childWidth":I
    .end local v11    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getPaddingTop()I

    move-result v14

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getPaddingBottom()I

    move-result v15

    add-int/2addr v14, v15

    add-int/2addr v9, v14

    .line 197
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getMinimumHeight()I

    move-result v14

    if-ge v9, v14, :cond_5

    .line 198
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getMinimumHeight()I

    move-result v9

    .line 201
    :cond_5
    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v9, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v14

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v14}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setMeasuredDimension(II)V

    .line 203
    return-void
.end method

.method public setData(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 210
    .local p1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;

    if-nez v0, :cond_0

    .line 211
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;

    .line 212
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mDataSetObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->setData(Ljava/util/ArrayList;)V

    .line 216
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->notifyDataSetChanged()V

    .line 217
    return-void
.end method

.method public setDisplay(Z)V
    .locals 0
    .param p1, "bShow"    # Z

    .prologue
    .line 307
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->bDisplaying:Z

    .line 308
    return-void
.end method

.method public setFilterClickListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mFilterClickListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;

    .line 207
    return-void
.end method

.method public setItemState(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    .line 232
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getChildCount()I

    move-result v1

    .line 234
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 235
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 237
    .local v0, "child":Landroid/view/View;
    if-eqz p1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 238
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 234
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 240
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_1

    .line 243
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    return-void
.end method

.method public setItemStateUnrelatedTimeFilter(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    .line 246
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getChildCount()I

    move-result v1

    .line 248
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_2

    .line 249
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 251
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->supportTimeTag()Z

    move-result v3

    if-nez v3, :cond_0

    .line 252
    if-eqz p1, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 253
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 248
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 255
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_1

    .line 259
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    return-void
.end method

.method public setUnselectedState()V
    .locals 4

    .prologue
    .line 220
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getChildCount()I

    move-result v1

    .line 222
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 223
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 225
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 226
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/View;->setSelected(Z)V

    .line 222
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 229
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    return-void
.end method
