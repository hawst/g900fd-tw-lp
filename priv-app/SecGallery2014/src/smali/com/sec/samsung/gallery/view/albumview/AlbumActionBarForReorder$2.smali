.class Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder$2;
.super Ljava/lang/Object;
.source "AlbumActionBarForReorder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->updateActionbarButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder$2;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder$2;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->isOrderChanged:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->access$100(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder$2;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mDoneActionView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->access$200(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 84
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder$2;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mDoneActionView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->access$200(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;)Landroid/view/View;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 89
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder$2;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mDoneActionView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->access$200(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 87
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder$2;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->mDoneActionView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->access$200(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;)Landroid/view/View;

    move-result-object v0

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method
