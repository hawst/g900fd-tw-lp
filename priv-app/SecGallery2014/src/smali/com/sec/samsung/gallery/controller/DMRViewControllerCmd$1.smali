.class Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$1;
.super Ljava/lang/Object;
.source "DMRViewControllerCmd.java"

# interfaces
.implements Lcom/sec/android/gallery3d/homesync/IDMRControllerActionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->startViewController()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setViewAngle(I)V
    .locals 0
    .param p1, "angle"    # I

    .prologue
    .line 65
    return-void
.end method

.method public setZoomPort(I)I
    .locals 4
    .param p1, "port"    # I

    .prologue
    .line 69
    # getter for: Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->access$100()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PhotoView setZoomPort() = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 71
    .local v0, "zoomportIntent":Landroid/content/Intent;
    const-string v1, "com.android.image.playstatechanged"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    const-string v1, "ZoomPort"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 73
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->access$200(Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/ContextWrapper;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 74
    const/4 v1, 0x0

    return v1
.end method

.method public zoom(FFF)V
    .locals 1
    .param p1, "posX"    # F
    .param p2, "posY"    # F
    .param p3, "zoomRatio"    # F

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->mDMRZoomListener:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRZoomListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->access$000(Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;)Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRZoomListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->mDMRZoomListener:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRZoomListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;->access$000(Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;)Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRZoomListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRZoomListener;->zoom(FFF)V

    .line 60
    :cond_0
    return-void
.end method
