.class public Lcom/sec/samsung/gallery/decoder/XIVInterface$BufferReusableTexture;
.super Lcom/quramsoft/xiv/XIVBufferReusableTexture;
.source "XIVInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/decoder/XIVInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "BufferReusableTexture"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/decoder/XIVInterface;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/decoder/XIVInterface;Lcom/quramsoft/xiv/XIVBufferReuseManager;Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;IZ)V
    .locals 4
    .param p2, "manager"    # Lcom/quramsoft/xiv/XIVBufferReuseManager;
    .param p3, "info"    # Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;
    .param p4, "type"    # I
    .param p5, "fastReuse"    # Z

    .prologue
    .line 609
    iput-object p1, p0, Lcom/sec/samsung/gallery/decoder/XIVInterface$BufferReusableTexture;->this$0:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    .line 610
    if-nez p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, p2, v0, p4, p5}, Lcom/quramsoft/xiv/XIVBufferReusableTexture;-><init>(Lcom/quramsoft/xiv/XIVBufferReuseManager;Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;IZ)V

    .line 611
    return-void

    .line 610
    :cond_0
    new-instance v0, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;

    iget v1, p3, Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;->width:I

    iget v2, p3, Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;->height:I

    iget-object v3, p3, Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;->buffer:Ljava/nio/ByteBuffer;

    invoke-direct {v0, v1, v2, v3}, Lcom/quramsoft/qrb/QuramBitmapFactory$ImageBufferData;-><init>(IILjava/nio/ByteBuffer;)V

    goto :goto_0
.end method

.method public constructor <init>(Lcom/sec/samsung/gallery/decoder/XIVInterface;Lcom/sec/samsung/gallery/decoder/XIVInterface;Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;IZ)V
    .locals 6
    .param p2, "xivInterface"    # Lcom/sec/samsung/gallery/decoder/XIVInterface;
    .param p3, "info"    # Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;
    .param p4, "type"    # I
    .param p5, "fastReuse"    # Z

    .prologue
    .line 606
    # getter for: Lcom/sec/samsung/gallery/decoder/XIVInterface;->mXiv:Lcom/quramsoft/xiv/XIV;
    invoke-static {p2}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->access$000(Lcom/sec/samsung/gallery/decoder/XIVInterface;)Lcom/quramsoft/xiv/XIV;

    move-result-object v0

    invoke-virtual {v0}, Lcom/quramsoft/xiv/XIV;->getBufferReuseManager()Lcom/quramsoft/xiv/XIVBufferReuseManager;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/decoder/XIVInterface$BufferReusableTexture;-><init>(Lcom/sec/samsung/gallery/decoder/XIVInterface;Lcom/quramsoft/xiv/XIVBufferReuseManager;Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;IZ)V

    .line 607
    return-void
.end method
