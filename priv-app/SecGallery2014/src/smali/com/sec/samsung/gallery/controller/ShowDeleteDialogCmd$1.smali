.class Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;
.super Ljava/lang/Object;
.source "ShowDeleteDialogCmd.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/OnProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->handleDeleteMedias(Lcom/sec/android/gallery3d/data/MediaSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

.field final synthetic val$dataProxy:Lcom/sec/android/gallery3d/data/DataManager;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;Lcom/sec/android/gallery3d/data/DataManager;)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    iput-object p2, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;->val$dataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 4
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    const/4 v3, 0x1

    .line 301
    instance-of v1, p1, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v1, :cond_1

    move-object v1, p1

    .line 302
    check-cast v1, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v0

    .line 303
    .local v0, "filePath":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardPath(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 304
    # getter for: Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->access$200()Ljava/lang/String;

    move-result-object v1

    const-string v2, "isSDCardPath file is not exist when deleting"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    .end local v0    # "filePath":Ljava/lang/String;
    :cond_0
    :goto_0
    return v3

    .line 309
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;->val$dataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/data/DataManager;->deleteItem(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 311
    invoke-static {}, Landroid/util/GateConfig;->isGateEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 313
    const-string v1, "GATE"

    const-string v2, "<GATE-M>PICTURE_DELETED: filePath is removed due to security </GATE-M>"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public handleOperation(Ljava/util/List;)Z
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 322
    .local p1, "objectList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v11, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$UpdateProgressTask;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v11, v0, v1}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$UpdateProgressTask;-><init>(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;)V

    .line 323
    .local v11, "progressTask":Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$UpdateProgressTask;
    sget-object v17, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Integer;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v11, v0, v1}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$UpdateProgressTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 325
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 327
    .local v12, "startTime":J
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 328
    .local v8, "imageIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 329
    .local v14, "videoIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 330
    .local v10, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v0, v10, Lcom/sec/android/gallery3d/data/LocalImage;

    move/from16 v17, v0

    if-eqz v17, :cond_1

    .line 331
    check-cast v10, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v10    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    iget v0, v10, Lcom/sec/android/gallery3d/data/LocalImage;->id:I

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 332
    .restart local v10    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    instance-of v0, v10, Lcom/sec/android/gallery3d/data/LocalVideo;

    move/from16 v17, v0

    if-eqz v17, :cond_0

    .line 333
    check-cast v10, Lcom/sec/android/gallery3d/data/LocalVideo;

    .end local v10    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    iget v0, v10, Lcom/sec/android/gallery3d/data/LocalVideo;->id:I

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 337
    :cond_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 338
    .local v9, "imgSize":I
    if-lez v9, :cond_3

    .line 339
    sget-object v4, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 340
    .local v4, "baseUri":Landroid/net/Uri;
    const-string v17, ", "

    move-object/from16 v0, v17

    invoke-static {v0, v8}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v7

    .line 341
    .local v7, "ids":Ljava/lang/String;
    const-string v17, "_id  IN (%s)"

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v7, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .line 343
    .local v16, "where":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v17

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    move-object/from16 v2, v18

    invoke-static {v0, v4, v1, v2}, Lcom/sec/android/gallery3d/data/ContentResolverDelegate;->delete(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 351
    .end local v4    # "baseUri":Landroid/net/Uri;
    .end local v7    # "ids":Ljava/lang/String;
    .end local v16    # "where":Ljava/lang/String;
    :cond_3
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v15

    .line 352
    .local v15, "videoSize":I
    if-lez v15, :cond_4

    .line 353
    sget-object v4, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 354
    .restart local v4    # "baseUri":Landroid/net/Uri;
    const-string v17, ", "

    move-object/from16 v0, v17

    invoke-static {v0, v14}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v7

    .line 356
    .restart local v7    # "ids":Ljava/lang/String;
    const-string v17, "_id  IN (%s)"

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v7, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .line 358
    .restart local v16    # "where":Ljava/lang/String;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v17

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    move-object/from16 v2, v18

    invoke-virtual {v0, v4, v1, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 365
    .end local v4    # "baseUri":Landroid/net/Uri;
    .end local v7    # "ids":Ljava/lang/String;
    .end local v16    # "where":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    move-object/from16 v17, v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sub-long v18, v18, v12

    add-int v20, v9, v15

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    div-long v18, v18, v20

    # setter for: Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mElapsedTime:J
    invoke-static/range {v17 .. v19}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->access$602(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;J)J

    .line 366
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const-string v18, "AverageDeleteDbOperationTime"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mElapsedTime:J
    invoke-static/range {v19 .. v19}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;)J

    move-result-wide v20

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-wide/from16 v2, v20

    invoke-static {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;J)V

    .line 367
    const/16 v17, 0x1

    .end local v15    # "videoSize":I
    :goto_1
    return v17

    .line 345
    .restart local v4    # "baseUri":Landroid/net/Uri;
    .restart local v7    # "ids":Ljava/lang/String;
    .restart local v16    # "where":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 346
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    .line 347
    const/16 v17, 0x0

    goto :goto_1

    .line 359
    .end local v5    # "e":Ljava/lang/Exception;
    .restart local v15    # "videoSize":I
    :catch_1
    move-exception v5

    .line 360
    .restart local v5    # "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    .line 361
    const/16 v17, 0x0

    goto :goto_1
.end method

.method public onCompleted(Z)V
    .locals 6
    .param p1, "result"    # Z

    .prologue
    .line 269
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 270
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->closeCustomProgressDialog()V

    .line 272
    :cond_0
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecretModeOn()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 273
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    iget-object v1, v1, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MEDIA_SCAN"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "file://"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->EXTERNAL_SD_DIRECTORY:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->access$100()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 275
    :cond_1
    if-nez p1, :cond_2

    .line 276
    # getter for: Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->access$200()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Delete Albums failed!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    const/4 v2, 0x0

    # setter for: Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->access$302(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;Lcom/sec/samsung/gallery/util/FileUtil;)Lcom/sec/samsung/gallery/util/FileUtil;

    .line 280
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    iget-object v1, v1, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 281
    .local v0, "currentState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v1, v0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    if-nez v1, :cond_3

    instance-of v1, v0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    if-eqz v1, :cond_4

    .line 282
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    iget-object v1, v1, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "EXIT_SELECTION_MODE"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 288
    :goto_0
    return-void

    .line 285
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    iget-object v1, v1, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "EXIT_SELECTION_MODE"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onProgress(II)V
    .locals 4
    .param p1, "progress"    # I
    .param p2, "total"    # I

    .prologue
    .line 292
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mFakeProgress:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->access$400(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v0

    int-to-long v2, p1

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->increaseProgress(JZ)V

    .line 294
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v0

    int-to-long v2, p1

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v3, v1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->increaseProgress(JZ)V

    .line 296
    :cond_0
    return-void
.end method
