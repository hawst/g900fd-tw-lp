.class Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam$1;
.super Ljava/lang/Object;
.source "AlbumViewBeam.java"

# interfaces
.implements Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;->setBeamListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetFilePath()[Landroid/net/Uri;
    .locals 7

    .prologue
    .line 35
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;->access$000(Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    .line 36
    .local v2, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v1

    .line 37
    .local v1, "mediaSetList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;->access$100()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onGetFilePath(). Selected item count= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/ui/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;->toItemList(Ljava/util/List;)Ljava/util/List;
    invoke-static {v4, v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;->access$200(Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 40
    .local v0, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam$1;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;->toUriPathArray(Ljava/util/List;)[Landroid/net/Uri;
    invoke-static {v4, v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;->access$300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewBeam;Ljava/util/List;)[Landroid/net/Uri;

    move-result-object v3

    .line 42
    .local v3, "uriPaths":[Landroid/net/Uri;
    return-object v3
.end method
