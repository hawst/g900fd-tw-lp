.class Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial$1;
.super Ljava/lang/Object;
.source "DetailActionBarForMotionTutorial.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->setTitle()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 57
    const-string v3, ""

    .line 58
    .local v3, "title":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryMotion()Lcom/sec/android/gallery3d/app/GalleryMotion;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v2

    .line 59
    .local v2, "motionType":Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    sget-object v4, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v2, v4, :cond_0

    .line 60
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->access$100(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e00df

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 66
    :goto_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;)Landroid/app/ActionBar;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 67
    const/16 v1, 0x10

    .line 68
    .local v1, "flags":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->access$500(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ActionBar;->getDisplayOptions()I

    move-result v4

    and-int/lit8 v0, v4, -0x11

    .line 69
    .local v0, "change":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->access$600(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 70
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->access$700(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;)Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 77
    return-void

    .line 61
    .end local v0    # "change":I
    .end local v1    # "flags":I
    :cond_0
    sget-object v4, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v2, v4, :cond_1

    .line 62
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->access$200(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e00de

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 64
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;->access$300(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForMotionTutorial;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e00e0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method
