.class Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;
.super Ljava/lang/Object;
.source "ClustersBuilder.java"


# instance fields
.field private final options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

.field private final projectionRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/gms/maps/Projection;",
            ">;"
        }
    .end annotation
.end field

.field private final relevantInputPointsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;",
            ">;"
        }
    .end annotation
.end field

.field private final releventInputPointsSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;",
            ">;"
        }
    .end annotation
.end field

.field private final visibleRegionRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/gms/maps/model/VisibleRegion;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/gms/maps/Projection;Lcom/sec/samsung/gallery/mapfragment/clustering/Options;Ljava/util/ArrayList;)V
    .locals 2
    .param p1, "projection"    # Lcom/google/android/gms/maps/Projection;
    .param p2, "options"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/maps/Projection;",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/Options;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p3, "initialClusteredPoints":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;->relevantInputPointsList:Ljava/util/ArrayList;

    .line 17
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;->releventInputPointsSet:Ljava/util/HashSet;

    .line 23
    iput-object p2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    .line 25
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;->projectionRef:Ljava/lang/ref/WeakReference;

    .line 26
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Lcom/google/android/gms/maps/Projection;->getVisibleRegion()Lcom/google/android/gms/maps/model/VisibleRegion;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;->visibleRegionRef:Ljava/lang/ref/WeakReference;

    .line 28
    if-eqz p3, :cond_0

    .line 29
    invoke-direct {p0, p3}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;->addRelevantInitialInputPoints(Ljava/util/ArrayList;)V

    .line 31
    :cond_0
    return-void
.end method

.method private addIfNecessary(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;Lcom/google/android/gms/maps/Projection;Lcom/google/android/gms/maps/model/LatLngBounds;Z)V
    .locals 1
    .param p1, "point"    # Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
    .param p2, "projection"    # Lcom/google/android/gms/maps/Projection;
    .param p3, "bounds"    # Lcom/google/android/gms/maps/model/LatLngBounds;
    .param p4, "skipCheck"    # Z

    .prologue
    .line 87
    if-eqz p3, :cond_0

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;->getMapPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/google/android/gms/maps/model/LatLngBounds;->contains(Lcom/google/android/gms/maps/model/LatLng;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    if-eqz p4, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;->relevantInputPointsList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 88
    invoke-virtual {p1, p2}, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;->buildScreenPosition(Lcom/google/android/gms/maps/Projection;)V

    .line 89
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;->relevantInputPointsList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;->releventInputPointsSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 92
    :cond_2
    return-void
.end method

.method private addRelevantInitialInputPoints(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p1, "initialClusteredPoints":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    .line 35
    .local v0, "clusterPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->clearScreenPosition()V

    .line 36
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->getPointsInCluster()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;->addAll(Ljava/util/ArrayList;Z)V

    goto :goto_0

    .line 38
    .end local v0    # "clusterPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    :cond_0
    return-void
.end method

.method private getExpandedBounds(Lcom/google/android/gms/maps/model/LatLngBounds;)Lcom/google/android/gms/maps/model/LatLngBounds;
    .locals 24
    .param p1, "bounds"    # Lcom/google/android/gms/maps/model/LatLngBounds;

    .prologue
    .line 62
    if-eqz p1, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    move-object/from16 v17, v0

    if-eqz v17, :cond_2

    .line 63
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->getExpandBoundsFactor()D

    move-result-wide v8

    .line 65
    .local v8, "expandBoundsFactor":D
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/maps/model/LatLngBounds;->northeast:Lcom/google/android/gms/maps/model/LatLng;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/maps/model/LatLngBounds;->southwest:Lcom/google/android/gms/maps/model/LatLng;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    move-wide/from16 v20, v0

    cmpg-double v17, v18, v20

    if-gez v17, :cond_0

    const/16 v16, 0x1

    .line 67
    .local v16, "spans180Meridian":Z
    :goto_0
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/maps/model/LatLngBounds;->northeast:Lcom/google/android/gms/maps/model/LatLng;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/maps/model/LatLngBounds;->southwest:Lcom/google/android/gms/maps/model/LatLng;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    move-wide/from16 v20, v0

    sub-double v6, v18, v20

    .line 69
    .local v6, "distanceFromNorthToSouth":D
    if-nez v16, :cond_1

    .line 70
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/maps/model/LatLngBounds;->northeast:Lcom/google/android/gms/maps/model/LatLng;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/maps/model/LatLngBounds;->southwest:Lcom/google/android/gms/maps/model/LatLng;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    move-wide/from16 v20, v0

    sub-double v4, v18, v20

    .line 75
    .local v4, "distanceFromEastToWest":D
    :goto_1
    mul-double v10, v6, v8

    .line 76
    .local v10, "expandLatitude":D
    mul-double v12, v4, v8

    .line 78
    .local v12, "expandLongitude":D
    new-instance v14, Lcom/google/android/gms/maps/model/LatLng;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/maps/model/LatLngBounds;->northeast:Lcom/google/android/gms/maps/model/LatLng;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    move-wide/from16 v18, v0

    add-double v18, v18, v10

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/maps/model/LatLngBounds;->northeast:Lcom/google/android/gms/maps/model/LatLng;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    move-wide/from16 v20, v0

    add-double v20, v20, v12

    move-wide/from16 v0, v18

    move-wide/from16 v2, v20

    invoke-direct {v14, v0, v1, v2, v3}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 79
    .local v14, "newNortheast":Lcom/google/android/gms/maps/model/LatLng;
    new-instance v15, Lcom/google/android/gms/maps/model/LatLng;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/maps/model/LatLngBounds;->southwest:Lcom/google/android/gms/maps/model/LatLng;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    move-wide/from16 v18, v0

    sub-double v18, v18, v10

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/maps/model/LatLngBounds;->southwest:Lcom/google/android/gms/maps/model/LatLng;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    move-wide/from16 v20, v0

    sub-double v20, v20, v12

    move-wide/from16 v0, v18

    move-wide/from16 v2, v20

    invoke-direct {v15, v0, v1, v2, v3}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 81
    .local v15, "newSouthwest":Lcom/google/android/gms/maps/model/LatLng;
    new-instance v17, Lcom/google/android/gms/maps/model/LatLngBounds;

    move-object/from16 v0, v17

    invoke-direct {v0, v15, v14}, Lcom/google/android/gms/maps/model/LatLngBounds;-><init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;)V

    .line 83
    .end local v4    # "distanceFromEastToWest":D
    .end local v6    # "distanceFromNorthToSouth":D
    .end local v8    # "expandBoundsFactor":D
    .end local v10    # "expandLatitude":D
    .end local v12    # "expandLongitude":D
    .end local v14    # "newNortheast":Lcom/google/android/gms/maps/model/LatLng;
    .end local v15    # "newSouthwest":Lcom/google/android/gms/maps/model/LatLng;
    .end local v16    # "spans180Meridian":Z
    :goto_2
    return-object v17

    .line 65
    .restart local v8    # "expandBoundsFactor":D
    :cond_0
    const/16 v16, 0x0

    goto/16 :goto_0

    .line 72
    .restart local v6    # "distanceFromNorthToSouth":D
    .restart local v16    # "spans180Meridian":Z
    :cond_1
    const-wide v18, 0x4066800000000000L    # 180.0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/maps/model/LatLngBounds;->northeast:Lcom/google/android/gms/maps/model/LatLng;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    move-wide/from16 v20, v0

    add-double v18, v18, v20

    const-wide v20, 0x4066800000000000L    # 180.0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/maps/model/LatLngBounds;->southwest:Lcom/google/android/gms/maps/model/LatLng;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    move-wide/from16 v22, v0

    sub-double v20, v20, v22

    add-double v4, v18, v20

    .restart local v4    # "distanceFromEastToWest":D
    goto/16 :goto_1

    .line 83
    .end local v4    # "distanceFromEastToWest":D
    .end local v6    # "distanceFromNorthToSouth":D
    .end local v8    # "expandBoundsFactor":D
    .end local v16    # "spans180Meridian":Z
    :cond_2
    const/16 v17, 0x0

    goto :goto_2
.end method

.method private getProjection()Lcom/google/android/gms/maps/Projection;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;->projectionRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/Projection;

    return-object v0
.end method

.method private getVisibleRegion()Lcom/google/android/gms/maps/model/VisibleRegion;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;->visibleRegionRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/VisibleRegion;

    return-object v0
.end method


# virtual methods
.method addAll(Ljava/util/ArrayList;Z)V
    .locals 6
    .param p2, "skipCheck"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p1, "points":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;>;"
    if-eqz p1, :cond_0

    .line 50
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;->getProjection()Lcom/google/android/gms/maps/Projection;

    move-result-object v3

    .line 51
    .local v3, "projection":Lcom/google/android/gms/maps/Projection;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;->getVisibleRegion()Lcom/google/android/gms/maps/model/VisibleRegion;

    move-result-object v4

    .line 52
    .local v4, "visibleRegion":Lcom/google/android/gms/maps/model/VisibleRegion;
    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    .line 53
    iget-object v5, v4, Lcom/google/android/gms/maps/model/VisibleRegion;->latLngBounds:Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;->getExpandedBounds(Lcom/google/android/gms/maps/model/LatLngBounds;)Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    .line 54
    .local v0, "bounds":Lcom/google/android/gms/maps/model/LatLngBounds;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;

    .line 55
    .local v2, "point":Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
    invoke-direct {p0, v2, v3, v0, p2}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;->addIfNecessary(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;Lcom/google/android/gms/maps/Projection;Lcom/google/android/gms/maps/model/LatLngBounds;Z)V

    goto :goto_0

    .line 59
    .end local v0    # "bounds":Lcom/google/android/gms/maps/model/LatLngBounds;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "point":Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
    .end local v3    # "projection":Lcom/google/android/gms/maps/Projection;
    .end local v4    # "visibleRegion":Lcom/google/android/gms/maps/model/VisibleRegion;
    :cond_0
    return-void
.end method

.method build()Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;->getProjection()Lcom/google/android/gms/maps/Projection;

    move-result-object v6

    .line 96
    .local v6, "projection":Lcom/google/android/gms/maps/Projection;
    const/4 v2, 0x0

    .line 97
    .local v2, "clusteredPoints":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;>;"
    if-eqz v6, :cond_3

    .line 98
    new-instance v2, Ljava/util/ArrayList;

    .end local v2    # "clusteredPoints":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;>;"
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;->relevantInputPointsList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-direct {v2, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 99
    .restart local v2    # "clusteredPoints":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;>;"
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;->relevantInputPointsList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;

    .line 100
    .local v5, "point":Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
    const/4 v0, 0x0

    .line 101
    .local v0, "addedToExistingCluster":Z
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    .line 102
    .local v1, "clusterPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    invoke-virtual {v1, v5}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->getPixelDistanceFrom(Lcom/sec/samsung/gallery/mapfragment/clustering/BasePoint;)D

    move-result-wide v8

    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClustersBuilder;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->getPixelDistanceToJoinCluster()I

    move-result v7

    int-to-double v10, v7

    cmpg-double v7, v8, v10

    if-gtz v7, :cond_1

    .line 103
    invoke-virtual {v1, v5}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->add(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;)V

    .line 104
    const/4 v0, 0x1

    .line 108
    .end local v1    # "clusterPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    :cond_2
    if-nez v0, :cond_0

    .line 109
    new-instance v7, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    const/4 v8, 0x0

    invoke-direct {v7, v5, v6, v8}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;-><init>(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;Lcom/google/android/gms/maps/Projection;Z)V

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 113
    .end local v0    # "addedToExistingCluster":Z
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "point":Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
    :cond_3
    return-object v2
.end method
