.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;
.super Ljava/lang/Object;
.source "PhotoViewBeam.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 24
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public setBeamListener()V
    .locals 4

    .prologue
    .line 27
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam$1;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;)V

    .line 40
    .local v0, "onGetFilePathListener":Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v0, v1, v2

    .line 44
    .local v1, "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "ANDROID_BEAM"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 46
    return-void
.end method
