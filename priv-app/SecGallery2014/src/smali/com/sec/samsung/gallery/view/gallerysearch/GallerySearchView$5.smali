.class Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$5;
.super Ljava/lang/Object;
.source "GallerySearchView.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    const/4 v2, 0x0

    .line 318
    if-eqz p2, :cond_0

    .line 319
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # invokes: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setHintTextColor()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$1100(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)V

    .line 330
    :goto_0
    return-void

    .line 321
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-eqz v0, :cond_1

    .line 322
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    move-result-object v0

    const v1, -0x717172

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setHintTextColor(I)V

    goto :goto_0

    .line 323
    :cond_1
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_2

    .line 325
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    move-result-object v0

    const/16 v1, 0x59

    invoke-static {v1, v2, v2, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setHintTextColor(I)V

    goto :goto_0

    .line 327
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    move-result-object v0

    const v1, -0x4c0a0a0b

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setHintTextColor(I)V

    goto :goto_0
.end method
