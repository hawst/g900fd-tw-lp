.class Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$6;
.super Ljava/lang/Object;
.source "GlHoverController.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem$OnBitmapAvailableListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V
    .locals 0

    .prologue
    .line 1069
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$6;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized onBitmapAvailable(I)V
    .locals 10
    .param p1, "index"    # I

    .prologue
    const-wide/16 v4, 0x190

    .line 1071
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$6;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mStartTime:J
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1300(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)J

    move-result-wide v2

    sub-long v8, v0, v2

    .line 1073
    .local v8, "timeTerm":J
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$6;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # operator++ for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLoadedCount:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1408(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)I

    .line 1074
    const-string v0, "GlHoverController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mListenerBitmap = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$6;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLoadedCount:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1400(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", active = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$6;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActiveCount:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1500(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", term = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1075
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$6;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mLoadedCount:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1400(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$6;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mActiveCount:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1500(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$6;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1600(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/android/gallery3d/glcore/GlHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1076
    cmp-long v0, v8, v4

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$6;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mNeedDelay:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1700(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1077
    sub-long v6, v4, v8

    .line 1078
    .local v6, "delay":J
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$6;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1600(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/android/gallery3d/glcore/GlHandler;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessageDelayed(IIIIJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1084
    .end local v6    # "delay":J
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1080
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$6;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1600(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/android/gallery3d/glcore/GlHandler;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 1081
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$6;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    const/4 v1, 0x1

    # setter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mNeedDelay:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1702(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1071
    .end local v8    # "timeTerm":J
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
