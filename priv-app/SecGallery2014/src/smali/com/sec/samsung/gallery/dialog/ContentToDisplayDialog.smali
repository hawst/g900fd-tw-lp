.class public Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;
.super Lcom/sec/samsung/gallery/dialog/GalleryDialog;
.source "ContentToDisplayDialog.java"


# instance fields
.field adapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurrentViewType:I

.field private mIsDropBoxAccountActive:Z

.field private mIsPicasaAccountActive:Z

.field private mIsSNSAccountActive:Z

.field private mIsTCloudAccountActive:Z

.field private mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field private mValidOptions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/core/ViewByFilterType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/dialog/GalleryDialog;-><init>(Landroid/content/Context;)V

    .line 41
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mIsDropBoxAccountActive:Z

    .line 43
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mIsSNSAccountActive:Z

    .line 45
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mIsPicasaAccountActive:Z

    .line 47
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mIsTCloudAccountActive:Z

    .line 49
    iput v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I

    .line 61
    iput-object p1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mContext:Landroid/content/Context;

    .line 62
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 63
    invoke-direct {p0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->UpdateDisplayTypes()V

    .line 64
    return-void
.end method

.method private UpdateDisplayTypes()V
    .locals 2

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->updateActivedViewByTypes()V

    .line 105
    invoke-static {}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getValidOptions()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mValidOptions:Ljava/util/ArrayList;

    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->isCloudSupported(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mValidOptions:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->CLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 109
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSettings:Z

    if-eqz v0, :cond_1

    .line 110
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mValidOptions:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->FACEBOOK:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 111
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mValidOptions:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->PICASA:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 113
    :cond_1
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloudMandatory:Z

    if-nez v0, :cond_2

    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mValidOptions:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->TCLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 116
    :cond_2
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_3

    .line 117
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mValidOptions:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->FACEBOOK:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 119
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I

    .line 120
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    .prologue
    .line 40
    iget v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I

    return v0
.end method

.method static synthetic access$072(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;
    .param p1, "x1"    # I

    .prologue
    .line 40
    iget v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I

    and-int/2addr v0, p1

    iput v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I

    return v0
.end method

.method static synthetic access$076(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;
    .param p1, "x1"    # I

    .prologue
    .line 40
    iget v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)Lcom/sec/android/gallery3d/app/StateManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mValidOptions:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->isViewByOptionEnabled(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->updateSaveButton()V

    return-void
.end method

.method private createViewtypeDialog()V
    .locals 6

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mValidOptions:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getViewByDisplayOptions(Landroid/content/Context;Ljava/util/ArrayList;)[Ljava/lang/String;

    move-result-object v5

    .line 153
    .local v5, "options":[Ljava/lang/String;
    new-instance v0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;

    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mContext:Landroid/content/Context;

    const v3, 0x7f0300ca

    const v4, 0x1020014

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$2;-><init>(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;Landroid/content/Context;II[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->adapter:Landroid/widget/ArrayAdapter;

    .line 267
    return-void
.end method

.method private isViewByOptionEnabled(I)Z
    .locals 2
    .param p1, "viewType"    # I

    .prologue
    const/4 v0, 0x1

    .line 88
    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->LOCAL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 100
    :cond_0
    :goto_0
    return v0

    .line 91
    :cond_1
    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->CLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 92
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mIsDropBoxAccountActive:Z

    goto :goto_0

    .line 93
    :cond_2
    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->FACEBOOK:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 94
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mIsSNSAccountActive:Z

    goto :goto_0

    .line 95
    :cond_3
    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->PICASA:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 96
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mIsPicasaAccountActive:Z

    goto :goto_0

    .line 97
    :cond_4
    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->TCLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mIsTCloudAccountActive:Z

    goto :goto_0
.end method

.method private setupListView(Landroid/widget/ListView;)V
    .locals 1
    .param p1, "listView"    # Landroid/widget/ListView;

    .prologue
    .line 270
    if-nez p1, :cond_0

    .line 345
    :goto_0
    return-void

    .line 273
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;

    invoke-direct {v0, p0, p1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;-><init>(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;Landroid/widget/ListView;)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto :goto_0
.end method

.method private updateActivedViewByTypes()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 71
    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->isCloudSupported(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->isCloudContentSyncOn(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v1

    sget-object v4, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-eq v1, v4, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mIsDropBoxAccountActive:Z

    .line 74
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->isSNSAccountsActive(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->isSNSDataExist(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v1

    sget-object v4, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-eq v1, v4, :cond_1

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mIsSNSAccountActive:Z

    .line 77
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v1, v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->isPicasaDataExist(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    :goto_2
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mIsPicasaAccountActive:Z

    .line 80
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;-><init>()V

    .line 81
    .local v0, "refer":Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloud:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->isTCloudAccoutActivated(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v1

    sget-object v4, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-eq v1, v4, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v1

    sget-object v4, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_NORMAL:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v1, v4, :cond_3

    :goto_3
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mIsTCloudAccountActive:Z

    .line 85
    return-void

    .end local v0    # "refer":Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    :cond_0
    move v1, v3

    .line 71
    goto :goto_0

    :cond_1
    move v1, v3

    .line 74
    goto :goto_1

    :cond_2
    move v1, v3

    .line 77
    goto :goto_2

    .restart local v0    # "refer":Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    :cond_3
    move v2, v3

    .line 81
    goto :goto_3
.end method

.method private updateSaveButton()V
    .locals 2

    .prologue
    .line 67
    const/4 v1, -0x1

    iget v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->setEnableButton(IZ)V

    .line 68
    return-void

    .line 67
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 124
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/dialog/GalleryDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    .line 125
    invoke-direct {p0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->createViewtypeDialog()V

    .line 126
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->adapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2, v4}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e014f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e00db

    new-instance v3, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$1;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$1;-><init>(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e0046

    invoke-virtual {v1, v2, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 144
    .local v0, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->setupListView(Landroid/widget/ListView;)V

    .line 146
    return-object v0
.end method
