.class public Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;
.super Ljava/lang/Thread;
.source "GallerySearchFilterGroupViewPager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "UpdateTagTask"
.end annotation


# instance fields
.field private volatile mDirty:Z

.field private volatile mIsActive:Z

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;Ljava/lang/String;)V
    .locals 1
    .param p2, "threadName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 630
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    .line 631
    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 628
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;->mIsActive:Z

    .line 629
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;->mDirty:Z

    .line 632
    return-void
.end method


# virtual methods
.method public declared-synchronized notifyDirty()V
    .locals 1

    .prologue
    .line 649
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;->mDirty:Z

    .line 650
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 651
    monitor-exit p0

    return-void

    .line 649
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 635
    :goto_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;->mIsActive:Z

    if-eqz v0, :cond_1

    .line 636
    monitor-enter p0

    .line 637
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;->mDirty:Z

    if-nez v0, :cond_0

    .line 638
    invoke-static {p0}, Lcom/sec/android/gallery3d/common/Utils;->waitWithoutInterrupt(Ljava/lang/Object;)V

    .line 639
    monitor-exit p0

    goto :goto_0

    .line 641
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 642
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;->mDirty:Z

    .line 643
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUserTag:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->access$400(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->updateTagList()V

    .line 644
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 646
    :cond_1
    return-void
.end method

.method public declared-synchronized terminate()V
    .locals 1

    .prologue
    .line 654
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$UpdateTagTask;->mIsActive:Z

    .line 655
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 656
    monitor-exit p0

    return-void

    .line 654
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
