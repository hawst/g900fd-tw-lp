.class public abstract Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;
.super Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;
.source "AbsHListView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;,
        Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;,
        Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;,
        Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;,
        Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;,
        Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;,
        Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;,
        Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;,
        Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;,
        Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;,
        Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$WindowRunnnable;,
        Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$ListItemAccessibilityDelegate;,
        Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;,
        Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SelectionBoundsAdjuster;,
        Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$OnScrollListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView",
        "<",
        "Landroid/widget/ListAdapter;",
        ">;",
        "Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;",
        "Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;"
    }
.end annotation


# static fields
.field private static final CHECK_POSITION_SEARCH_DISTANCE:I = 0x14

.field private static final INVALID_POINTER:I = -0x1

.field public static final LAYOUT_FORCE_LEFT:I = 0x1

.field public static final LAYOUT_FORCE_RIGHT:I = 0x3

.field public static final LAYOUT_MOVE_SELECTION:I = 0x6

.field public static final LAYOUT_NORMAL:I = 0x0

.field public static final LAYOUT_SET_SELECTION:I = 0x2

.field public static final LAYOUT_SPECIFIC:I = 0x4

.field public static final LAYOUT_SYNC:I = 0x5

.field protected static final OVERSCROLL_LIMIT_DIVISOR:I = 0x3

.field public static final STATESET_NOTHING:[I

.field private static final TAG:Ljava/lang/String; = "AbsListView"

.field public static final TOUCH_MODE_DONE_WAITING:I = 0x2

.field public static final TOUCH_MODE_DOWN:I = 0x0

.field public static final TOUCH_MODE_FLING:I = 0x4

.field private static final TOUCH_MODE_OFF:I = 0x1

.field private static final TOUCH_MODE_ON:I = 0x0

.field public static final TOUCH_MODE_OVERFLING:I = 0x6

.field public static final TOUCH_MODE_OVERSCROLL:I = 0x5

.field public static final TOUCH_MODE_REST:I = -0x1

.field public static final TOUCH_MODE_SCROLL:I = 0x3

.field public static final TOUCH_MODE_TAP:I = 0x1

.field private static final TOUCH_MODE_UNKNOWN:I = -0x1

.field public static final TRANSCRIPT_MODE_ALWAYS_SCROLL:I = 0x2

.field public static final TRANSCRIPT_MODE_DISABLED:I = 0x0

.field public static final TRANSCRIPT_MODE_NORMAL:I = 0x1

.field static final sLinearInterpolator:Landroid/view/animation/Interpolator;


# instance fields
.field private mAccessibilityDelegate:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$ListItemAccessibilityDelegate;

.field private mActivePointerId:I

.field protected mAdapter:Landroid/widget/ListAdapter;

.field mAdapterHasStableIds:Z

.field private mCacheColorHint:I

.field protected mCachingActive:Z

.field protected mCachingStarted:Z

.field protected mCheckStates:Landroid/util/SparseBooleanArray;

.field mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mCheckedItemCount:I

.field public mChoiceActionMode:Ljava/lang/Object;

.field protected mChoiceMode:I

.field private mClearScrollingCache:Ljava/lang/Runnable;

.field private mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

.field protected mDataSetObserver:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;

.field private mDirection:I

.field mDrawSelectorOnTop:Z

.field private mEdgeGlowBottom:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

.field private mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

.field mFastScrollEnabled:Z

.field private mFirstPositionDistanceGuess:I

.field private mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

.field private mForceTranscriptScroll:Z

.field private mGlowPaddingBottom:I

.field private mGlowPaddingTop:I

.field protected mHeightMeasureSpec:I

.field private mHorizontalScrollFactor:F

.field protected mIsAttached:Z

.field private mIsChildViewEnabled:Z

.field protected final mIsScrap:[Z

.field private mLastAccessibilityScrollEventFromIndex:I

.field private mLastAccessibilityScrollEventToIndex:I

.field private mLastHandledItemCount:I

.field private mLastPositionDistanceGuess:I

.field private mLastScrollState:I

.field private mLastTouchMode:I

.field mLastX:I

.field protected mLayoutMode:I

.field protected mListPadding:Landroid/graphics/Rect;

.field private mMaximumVelocity:I

.field private mMinimumVelocity:I

.field mMotionCorrection:I

.field protected mMotionPosition:I

.field mMotionViewNewLeft:I

.field mMotionViewOriginalLeft:I

.field mMotionX:I

.field mMotionY:I

.field mMultiChoiceModeCallback:Ljava/lang/Object;

.field private mOnScrollListener:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$OnScrollListener;

.field mOverflingDistance:I

.field mOverscrollDistance:I

.field protected mOverscrollMax:I

.field private mPendingCheckForKeyLongPress:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;

.field private mPendingCheckForLongPress:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;

.field private mPendingCheckForTap:Ljava/lang/Runnable;

.field private mPendingSync:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;

.field private mPerformClick:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;

.field protected mPositionScrollAfterLayout:Ljava/lang/Runnable;

.field protected mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

.field protected final mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

.field protected mResurrectToPosition:I

.field mScrollLeft:Landroid/view/View;

.field mScrollRight:Landroid/view/View;

.field mScrollingCacheEnabled:Z

.field protected mSelectedLeft:I

.field mSelectionBottomPadding:I

.field mSelectionLeftPadding:I

.field mSelectionRightPadding:I

.field mSelectionTopPadding:I

.field mSelector:Landroid/graphics/drawable/Drawable;

.field mSelectorPosition:I

.field protected mSelectorRect:Landroid/graphics/Rect;

.field private mSmoothScrollbarEnabled:Z

.field protected mStackFromRight:Z

.field private mTouchFrame:Landroid/graphics/Rect;

.field protected mTouchMode:I

.field private mTouchModeReset:Ljava/lang/Runnable;

.field private mTouchSlop:I

.field private mTranscriptMode:I

.field private mVelocityScale:F

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field mViewHelper:Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 561
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->sLinearInterpolator:Landroid/view/animation/Interpolator;

    .line 2187
    const/4 v0, 0x1

    new-array v0, v0, [I

    aput v1, v0, v1

    sput-object v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->STATESET_NOTHING:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 637
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;-><init>(Landroid/content/Context;)V

    .line 182
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    .line 216
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLayoutMode:I

    .line 236
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDrawSelectorOnTop:Z

    .line 246
    iput v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectorPosition:I

    .line 251
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectorRect:Landroid/graphics/Rect;

    .line 256
    new-instance v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    .line 261
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectionLeftPadding:I

    .line 266
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectionTopPadding:I

    .line 271
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectionRightPadding:I

    .line 276
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectionBottomPadding:I

    .line 281
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    .line 286
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mHeightMeasureSpec:I

    .line 332
    iput v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 363
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedLeft:I

    .line 388
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSmoothScrollbarEnabled:Z

    .line 398
    iput v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mResurrectToPosition:I

    .line 400
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 425
    iput v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastTouchMode:I

    .line 470
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastScrollState:I

    .line 478
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mVelocityScale:F

    .line 480
    new-array v0, v3, [Z

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mIsScrap:[Z

    .line 485
    iput v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mActivePointerId:I

    .line 530
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDirection:I

    .line 638
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->initAbsListView()V

    .line 639
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 642
    const v0, 0x7f010011

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 643
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v10, 0x1

    const/4 v9, -0x1

    const/4 v8, 0x0

    .line 646
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 182
    iput v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    .line 216
    iput v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLayoutMode:I

    .line 236
    iput-boolean v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDrawSelectorOnTop:Z

    .line 246
    iput v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectorPosition:I

    .line 251
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    iput-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectorRect:Landroid/graphics/Rect;

    .line 256
    new-instance v7, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    invoke-direct {v7, p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V

    iput-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    .line 261
    iput v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectionLeftPadding:I

    .line 266
    iput v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectionTopPadding:I

    .line 271
    iput v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectionRightPadding:I

    .line 276
    iput v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectionBottomPadding:I

    .line 281
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    iput-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    .line 286
    iput v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mHeightMeasureSpec:I

    .line 332
    iput v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 363
    iput v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedLeft:I

    .line 388
    iput-boolean v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSmoothScrollbarEnabled:Z

    .line 398
    iput v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mResurrectToPosition:I

    .line 400
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 425
    iput v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastTouchMode:I

    .line 470
    iput v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastScrollState:I

    .line 478
    const/high16 v7, 0x3f800000    # 1.0f

    iput v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mVelocityScale:F

    .line 480
    new-array v7, v10, [Z

    iput-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mIsScrap:[Z

    .line 485
    iput v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mActivePointerId:I

    .line 530
    iput v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDirection:I

    .line 647
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->initAbsListView()V

    .line 649
    sget-object v7, Lcom/sec/android/gallery3d/R$styleable;->AbsHListView:[I

    invoke-virtual {p1, p2, v7, p3, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 651
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v8}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 652
    .local v2, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_0

    .line 653
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 656
    :cond_0
    invoke-virtual {v0, v10, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v7

    iput-boolean v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDrawSelectorOnTop:Z

    .line 658
    const/4 v7, 0x6

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v5

    .line 659
    .local v5, "stackFromRight":Z
    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setStackFromRight(Z)V

    .line 661
    const/4 v7, 0x2

    invoke-virtual {v0, v7, v10}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    .line 662
    .local v3, "scrollingCacheEnabled":Z
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setScrollingCacheEnabled(Z)V

    .line 664
    const/4 v7, 0x7

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v6

    .line 665
    .local v6, "transcriptMode":I
    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setTranscriptMode(I)V

    .line 667
    const/4 v7, 0x3

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 668
    .local v1, "color":I
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setCacheColorHint(I)V

    .line 670
    const/4 v7, 0x5

    invoke-virtual {v0, v7, v10}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    .line 671
    .local v4, "smoothScrollbar":Z
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setSmoothScrollbarEnabled(Z)V

    .line 673
    const/4 v7, 0x4

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setChoiceMode(I)V

    .line 675
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 676
    return-void
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMaximumVelocity:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMinimumVelocity:I

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->contentFits()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowBottom:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->clearScrollingCache()V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;IIIIIIIIZ)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # I
    .param p5, "x5"    # I
    .param p6, "x6"    # I
    .param p7, "x7"    # I
    .param p8, "x8"    # I
    .param p9, "x9"    # Z

    .prologue
    .line 76
    invoke-virtual/range {p0 .. p9}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->overScrollBy(IIIIIIIIZ)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1800(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;IIIIIIIIZ)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # I
    .param p5, "x5"    # I
    .param p6, "x6"    # I
    .param p7, "x7"    # I
    .param p8, "x8"    # I
    .param p9, "x9"    # Z

    .prologue
    .line 76
    invoke-virtual/range {p0 .. p9}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->overScrollBy(IIIIIIIIZ)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;IIIIIIIIZ)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # I
    .param p5, "x5"    # I
    .param p6, "x6"    # I
    .param p7, "x7"    # I
    .param p8, "x8"    # I
    .param p9, "x9"    # Z

    .prologue
    .line 76
    invoke-virtual/range {p0 .. p9}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->overScrollBy(IIIIIIIIZ)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWindowAttachCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;
    .param p1, "x1"    # Z

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setChildrenDrawnWithCacheEnabled(Z)V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;
    .param p1, "x1"    # Z

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setChildrenDrawingCacheEnabled(Z)V

    return-void
.end method

.method static synthetic access$2300(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Landroid/view/View;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z

    .prologue
    .line 76
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$2400(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Landroid/view/View;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z

    .prologue
    .line 76
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Landroid/view/View;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z

    .prologue
    .line 76
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$2600(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Landroid/view/View;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z

    .prologue
    .line 76
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Landroid/view/View;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z

    .prologue
    .line 76
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWindowAttachCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingCheckForLongPress:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;)Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingCheckForLongPress:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mActivePointerId:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)Landroid/view/VelocityTracker;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    return-object v0
.end method

.method private clearScrollingCache()V
    .locals 1

    .prologue
    .line 4418
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mViewHelper:Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelper;->isHardwareAccelerated()Z

    move-result v0

    if-nez v0, :cond_1

    .line 4419
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mClearScrollingCache:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 4420
    new-instance v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$2;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mClearScrollingCache:Ljava/lang/Runnable;

    .line 4437
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mClearScrollingCache:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->post(Ljava/lang/Runnable;)Z

    .line 4439
    :cond_1
    return-void
.end method

.method private contentFits()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1063
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v0

    .line 1064
    .local v0, "childCount":I
    if-nez v0, :cond_1

    .line 1067
    :cond_0
    :goto_0
    return v1

    .line 1065
    :cond_1
    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    if-eq v0, v3, :cond_2

    move v1, v2

    goto :goto_0

    .line 1067
    :cond_2
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    if-lt v3, v4, :cond_3

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    sub-int/2addr v4, v5

    if-le v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method private createScrollingCache()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 4410
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mScrollingCacheEnabled:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCachingStarted:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mViewHelper:Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelper;->isHardwareAccelerated()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4411
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setChildrenDrawnWithCacheEnabled(Z)V

    .line 4412
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setChildrenDrawingCacheEnabled(Z)V

    .line 4413
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCachingActive:Z

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCachingStarted:Z

    .line 4415
    :cond_0
    return-void
.end method

.method private drawSelector(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 2085
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2086
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelector:Landroid/graphics/drawable/Drawable;

    .line 2087
    .local v0, "selector":Landroid/graphics/drawable/Drawable;
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 2088
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2090
    .end local v0    # "selector":Landroid/graphics/drawable/Drawable;
    :cond_0
    return-void
.end method

.method private finishGlows()V
    .locals 1

    .prologue
    .line 5225
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    if-eqz v0, :cond_0

    .line 5226
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->finish()V

    .line 5227
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowBottom:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->finish()V

    .line 5229
    :cond_0
    return-void
.end method

.method public static getDistance(Landroid/graphics/Rect;Landroid/graphics/Rect;I)I
    .locals 8
    .param p0, "source"    # Landroid/graphics/Rect;
    .param p1, "dest"    # Landroid/graphics/Rect;
    .param p2, "direction"    # I

    .prologue
    .line 5051
    sparse-switch p2, :sswitch_data_0

    .line 5084
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT, FOCUS_FORWARD, FOCUS_BACKWARD}."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 5053
    :sswitch_0
    iget v4, p0, Landroid/graphics/Rect;->right:I

    .line 5054
    .local v4, "sX":I
    iget v6, p0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v5, v6, v7

    .line 5055
    .local v5, "sY":I
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 5056
    .local v0, "dX":I
    iget v6, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v1, v6, v7

    .line 5088
    .local v1, "dY":I
    :goto_0
    sub-int v2, v0, v4

    .line 5089
    .local v2, "deltaX":I
    sub-int v3, v1, v5

    .line 5090
    .local v3, "deltaY":I
    mul-int v6, v3, v3

    mul-int v7, v2, v2

    add-int/2addr v6, v7

    return v6

    .line 5059
    .end local v0    # "dX":I
    .end local v1    # "dY":I
    .end local v2    # "deltaX":I
    .end local v3    # "deltaY":I
    .end local v4    # "sX":I
    .end local v5    # "sY":I
    :sswitch_1
    iget v6, p0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v4, v6, v7

    .line 5060
    .restart local v4    # "sX":I
    iget v5, p0, Landroid/graphics/Rect;->bottom:I

    .line 5061
    .restart local v5    # "sY":I
    iget v6, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v0, v6, v7

    .line 5062
    .restart local v0    # "dX":I
    iget v1, p1, Landroid/graphics/Rect;->top:I

    .line 5063
    .restart local v1    # "dY":I
    goto :goto_0

    .line 5065
    .end local v0    # "dX":I
    .end local v1    # "dY":I
    .end local v4    # "sX":I
    .end local v5    # "sY":I
    :sswitch_2
    iget v4, p0, Landroid/graphics/Rect;->left:I

    .line 5066
    .restart local v4    # "sX":I
    iget v6, p0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v5, v6, v7

    .line 5067
    .restart local v5    # "sY":I
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 5068
    .restart local v0    # "dX":I
    iget v6, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v1, v6, v7

    .line 5069
    .restart local v1    # "dY":I
    goto :goto_0

    .line 5071
    .end local v0    # "dX":I
    .end local v1    # "dY":I
    .end local v4    # "sX":I
    .end local v5    # "sY":I
    :sswitch_3
    iget v6, p0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v4, v6, v7

    .line 5072
    .restart local v4    # "sX":I
    iget v5, p0, Landroid/graphics/Rect;->top:I

    .line 5073
    .restart local v5    # "sY":I
    iget v6, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v0, v6, v7

    .line 5074
    .restart local v0    # "dX":I
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 5075
    .restart local v1    # "dY":I
    goto :goto_0

    .line 5078
    .end local v0    # "dX":I
    .end local v1    # "dY":I
    .end local v4    # "sX":I
    .end local v5    # "sY":I
    :sswitch_4
    iget v6, p0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v4, v6, v7

    .line 5079
    .restart local v4    # "sX":I
    iget v6, p0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v5, v6, v7

    .line 5080
    .restart local v5    # "sY":I
    iget v6, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v0, v6, v7

    .line 5081
    .restart local v0    # "dX":I
    iget v6, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v1, v6, v7

    .line 5082
    .restart local v1    # "dY":I
    goto :goto_0

    .line 5051
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_4
        0x2 -> :sswitch_4
        0x11 -> :sswitch_2
        0x21 -> :sswitch_3
        0x42 -> :sswitch_0
        0x82 -> :sswitch_1
    .end sparse-switch
.end method

.method private initAbsListView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 679
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setClickable(Z)V

    .line 680
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setFocusableInTouchMode(Z)V

    .line 681
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setWillNotDraw(Z)V

    .line 682
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setAlwaysDrawnWithCacheEnabled(Z)V

    .line 683
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setScrollingCacheEnabled(Z)V

    .line 685
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 686
    .local v0, "configuration":Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchSlop:I

    .line 687
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMinimumVelocity:I

    .line 688
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMaximumVelocity:I

    .line 689
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledOverscrollDistance()I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mOverscrollDistance:I

    .line 690
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledOverflingDistance()I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mOverflingDistance:I

    .line 691
    invoke-static {p0}, Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory;->create(Landroid/view/View;)Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mViewHelper:Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelper;

    .line 692
    return-void
.end method

.method private initOrResetVelocityTracker()V
    .locals 1

    .prologue
    .line 3394
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 3395
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 3399
    :goto_0
    return-void

    .line 3397
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_0
.end method

.method private initVelocityTrackerIfNotExists()V
    .locals 1

    .prologue
    .line 3402
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 3403
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 3405
    :cond_0
    return-void
.end method

.method private onSecondaryPointerUp(Landroid/view/MotionEvent;)V
    .locals 6
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    .line 3510
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    const v5, 0xff00

    and-int/2addr v4, v5

    shr-int/lit8 v2, v4, 0x8

    .line 3512
    .local v2, "pointerIndex":I
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    .line 3513
    .local v1, "pointerId":I
    iget v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mActivePointerId:I

    if-ne v1, v4, :cond_0

    .line 3517
    if-nez v2, :cond_1

    const/4 v0, 0x1

    .line 3518
    .local v0, "newPointerIndex":I
    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionX:I

    .line 3519
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionY:I

    .line 3520
    iput v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionCorrection:I

    .line 3521
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mActivePointerId:I

    .line 3523
    .end local v0    # "newPointerIndex":I
    :cond_0
    return-void

    :cond_1
    move v0, v3

    .line 3517
    goto :goto_0
.end method

.method private positionSelector(IIII)V
    .locals 5
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "r"    # I
    .param p4, "b"    # I

    .prologue
    .line 2006
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectorRect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectionLeftPadding:I

    sub-int v1, p1, v1

    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectionTopPadding:I

    sub-int v2, p2, v2

    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectionRightPadding:I

    add-int/2addr v3, p3

    iget v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectionBottomPadding:I

    add-int/2addr v4, p4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 2008
    return-void
.end method

.method private recycleVelocityTracker()V
    .locals 1

    .prologue
    .line 3408
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 3409
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 3410
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 3412
    :cond_0
    return-void
.end method

.method static retrieveFromScrap(Ljava/util/ArrayList;I)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;I)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 5731
    .local p0, "scrapViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 5732
    .local v1, "size":I
    if-lez v1, :cond_2

    .line 5734
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 5735
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 5736
    .local v2, "view":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    iget v3, v3, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->scrappedFromPosition:I

    if-ne v3, p1, :cond_0

    .line 5737
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 5743
    .end local v0    # "i":I
    .end local v2    # "view":Landroid/view/View;
    :goto_1
    return-object v2

    .line 5734
    .restart local v0    # "i":I
    .restart local v2    # "view":Landroid/view/View;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5741
    .end local v2    # "view":Landroid/view/View;
    :cond_1
    add-int/lit8 v3, v1, -0x1

    invoke-virtual {p0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    move-object v2, v3

    goto :goto_1

    .line 5743
    .end local v0    # "i":I
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private scrollIfNeeded(I)V
    .locals 29
    .param p1, "x"    # I

    .prologue
    .line 2711
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionX:I

    sub-int v28, p1, v3

    .line 2712
    .local v28, "rawDeltaX":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionCorrection:I

    sub-int v16, v28, v3

    .line 2713
    .local v16, "deltaX":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastX:I

    const/high16 v5, -0x80000000

    if-eq v3, v5, :cond_9

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastX:I

    sub-int v17, p1, v3

    .line 2715
    .local v17, "incrementalDeltaX":I
    :goto_0
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    const/4 v5, 0x3

    if-ne v3, v5, :cond_d

    .line 2717
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastX:I

    move/from16 v0, p1

    if-eq v0, v3, :cond_8

    .line 2721
    invoke-static/range {v28 .. v28}, Ljava/lang/Math;->abs(I)I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchSlop:I

    if-le v3, v5, :cond_0

    .line 2722
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getParent()Landroid/view/ViewParent;

    move-result-object v27

    .line 2723
    .local v27, "parent":Landroid/view/ViewParent;
    if-eqz v27, :cond_0

    .line 2724
    const/4 v3, 0x1

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 2729
    .end local v27    # "parent":Landroid/view/ViewParent;
    :cond_0
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionPosition:I

    if-ltz v3, :cond_a

    .line 2730
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionPosition:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    sub-int v18, v3, v5

    .line 2737
    .local v18, "motionIndex":I
    :goto_1
    const/16 v21, 0x0

    .line 2738
    .local v21, "motionViewPrevLeft":I
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v20

    .line 2739
    .local v20, "motionView":Landroid/view/View;
    if-eqz v20, :cond_1

    .line 2740
    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getLeft()I

    move-result v21

    .line 2744
    :cond_1
    const/4 v15, 0x0

    .line 2745
    .local v15, "atEdge":Z
    if-eqz v17, :cond_2

    .line 2746
    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->trackMotionScroll(II)Z

    move-result v15

    .line 2750
    :cond_2
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v20

    .line 2751
    if-eqz v20, :cond_7

    .line 2754
    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getLeft()I

    move-result v22

    .line 2755
    .local v22, "motionViewRealLeft":I
    if-eqz v15, :cond_6

    .line 2758
    move/from16 v0, v17

    neg-int v3, v0

    sub-int v5, v22, v21

    sub-int v4, v3, v5

    .line 2759
    .local v4, "overscroll":I
    const/4 v5, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getScrollX()I

    move-result v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mOverscrollDistance:I

    const/4 v11, 0x0

    const/4 v12, 0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v12}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->overScrollBy(IIIIIIIIZ)Z

    .line 2760
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mOverscrollDistance:I

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getScrollX()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-ne v3, v5, :cond_3

    .line 2762
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v3, :cond_3

    .line 2763
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->clear()V

    .line 2767
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getOverScrollMode()I

    move-result v26

    .line 2769
    .local v26, "overscrollMode":I
    if-eqz v26, :cond_4

    const/4 v3, 0x1

    move/from16 v0, v26

    if-ne v0, v3, :cond_6

    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->contentFits()Z

    move-result v3

    if-nez v3, :cond_6

    .line 2771
    :cond_4
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDirection:I

    .line 2772
    const/4 v3, 0x5

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 2773
    if-lez v28, :cond_b

    .line 2774
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    int-to-float v5, v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v5, v7

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->onPull(F)V

    .line 2775
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowBottom:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->isFinished()Z

    move-result v3

    if-nez v3, :cond_5

    .line 2776
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowBottom:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->onRelease()V

    .line 2778
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->getBounds(Z)Landroid/graphics/Rect;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->invalidate(Landroid/graphics/Rect;)V

    .line 2788
    .end local v4    # "overscroll":I
    .end local v26    # "overscrollMode":I
    :cond_6
    :goto_2
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionX:I

    .line 2790
    .end local v22    # "motionViewRealLeft":I
    :cond_7
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastX:I

    .line 2855
    .end local v15    # "atEdge":Z
    .end local v18    # "motionIndex":I
    .end local v20    # "motionView":Landroid/view/View;
    .end local v21    # "motionViewPrevLeft":I
    :cond_8
    :goto_3
    return-void

    .end local v17    # "incrementalDeltaX":I
    :cond_9
    move/from16 v17, v16

    .line 2713
    goto/16 :goto_0

    .line 2734
    .restart local v17    # "incrementalDeltaX":I
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v3

    div-int/lit8 v18, v3, 0x2

    .restart local v18    # "motionIndex":I
    goto/16 :goto_1

    .line 2779
    .restart local v4    # "overscroll":I
    .restart local v15    # "atEdge":Z
    .restart local v20    # "motionView":Landroid/view/View;
    .restart local v21    # "motionViewPrevLeft":I
    .restart local v22    # "motionViewRealLeft":I
    .restart local v26    # "overscrollMode":I
    :cond_b
    if-gez v28, :cond_6

    .line 2780
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowBottom:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    int-to-float v5, v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v5, v7

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->onPull(F)V

    .line 2781
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->isFinished()Z

    move-result v3

    if-nez v3, :cond_c

    .line 2782
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->onRelease()V

    .line 2784
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowBottom:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->getBounds(Z)Landroid/graphics/Rect;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->invalidate(Landroid/graphics/Rect;)V

    goto :goto_2

    .line 2792
    .end local v4    # "overscroll":I
    .end local v15    # "atEdge":Z
    .end local v18    # "motionIndex":I
    .end local v20    # "motionView":Landroid/view/View;
    .end local v21    # "motionViewPrevLeft":I
    .end local v22    # "motionViewRealLeft":I
    .end local v26    # "overscrollMode":I
    :cond_d
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    const/4 v5, 0x5

    if-ne v3, v5, :cond_8

    .line 2793
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastX:I

    move/from16 v0, p1

    if-eq v0, v3, :cond_8

    .line 2794
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getScrollX()I

    move-result v25

    .line 2795
    .local v25, "oldScroll":I
    sub-int v24, v25, v17

    .line 2796
    .local v24, "newScroll":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastX:I

    move/from16 v0, p1

    if-le v0, v3, :cond_16

    const/16 v23, 0x1

    .line 2798
    .local v23, "newDirection":I
    :goto_4
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDirection:I

    if-nez v3, :cond_e

    .line 2799
    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDirection:I

    .line 2802
    :cond_e
    move/from16 v0, v17

    neg-int v6, v0

    .line 2803
    .local v6, "overScrollDistance":I
    if-gez v24, :cond_f

    if-gez v25, :cond_10

    :cond_f
    if-lez v24, :cond_17

    if-gtz v25, :cond_17

    .line 2804
    :cond_10
    move/from16 v0, v25

    neg-int v6, v0

    .line 2805
    add-int v17, v17, v6

    .line 2810
    :goto_5
    if-eqz v6, :cond_13

    .line 2811
    const/4 v7, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getScrollX()I

    move-result v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mOverscrollDistance:I

    const/4 v13, 0x0

    const/4 v14, 0x1

    move-object/from16 v5, p0

    invoke-virtual/range {v5 .. v14}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->overScrollBy(IIIIIIIIZ)Z

    .line 2812
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getOverScrollMode()I

    move-result v26

    .line 2813
    .restart local v26    # "overscrollMode":I
    if-eqz v26, :cond_11

    const/4 v3, 0x1

    move/from16 v0, v26

    if-ne v0, v3, :cond_13

    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->contentFits()Z

    move-result v3

    if-nez v3, :cond_13

    .line 2814
    :cond_11
    if-lez v28, :cond_18

    .line 2815
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    int-to-float v5, v6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v5, v7

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->onPull(F)V

    .line 2816
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowBottom:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->isFinished()Z

    move-result v3

    if-nez v3, :cond_12

    .line 2817
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowBottom:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->onRelease()V

    .line 2819
    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->getBounds(Z)Landroid/graphics/Rect;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->invalidate(Landroid/graphics/Rect;)V

    .line 2830
    .end local v26    # "overscrollMode":I
    :cond_13
    :goto_6
    if-eqz v17, :cond_15

    .line 2832
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getScrollX()I

    move-result v3

    if-eqz v3, :cond_14

    .line 2833
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mViewHelper:Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelper;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelper;->setScrollX(I)V

    .line 2834
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->invalidateParentIfNeeded()V

    .line 2837
    :cond_14
    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->trackMotionScroll(II)Z

    .line 2839
    const/4 v3, 0x3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 2843
    invoke-virtual/range {p0 .. p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->findClosestMotionCol(I)I

    move-result v19

    .line 2845
    .local v19, "motionPosition":I
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionCorrection:I

    .line 2846
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    sub-int v3, v19, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v20

    .line 2847
    .restart local v20    # "motionView":Landroid/view/View;
    if-eqz v20, :cond_1a

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getLeft()I

    move-result v3

    :goto_7
    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionViewOriginalLeft:I

    .line 2848
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionX:I

    .line 2849
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionPosition:I

    .line 2851
    .end local v19    # "motionPosition":I
    .end local v20    # "motionView":Landroid/view/View;
    :cond_15
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastX:I

    .line 2852
    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDirection:I

    goto/16 :goto_3

    .line 2796
    .end local v6    # "overScrollDistance":I
    .end local v23    # "newDirection":I
    :cond_16
    const/16 v23, -0x1

    goto/16 :goto_4

    .line 2807
    .restart local v6    # "overScrollDistance":I
    .restart local v23    # "newDirection":I
    :cond_17
    const/16 v17, 0x0

    goto/16 :goto_5

    .line 2820
    .restart local v26    # "overscrollMode":I
    :cond_18
    if-gez v28, :cond_13

    .line 2821
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowBottom:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    int-to-float v5, v6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v5, v7

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->onPull(F)V

    .line 2822
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->isFinished()Z

    move-result v3

    if-nez v3, :cond_19

    .line 2823
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->onRelease()V

    .line 2825
    :cond_19
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowBottom:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->getBounds(Z)Landroid/graphics/Rect;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->invalidate(Landroid/graphics/Rect;)V

    goto/16 :goto_6

    .line 2847
    .end local v26    # "overscrollMode":I
    .restart local v19    # "motionPosition":I
    .restart local v20    # "motionView":Landroid/view/View;
    :cond_1a
    const/4 v3, 0x0

    goto :goto_7
.end method

.method private startScrollIfNeeded(I)Z
    .locals 10
    .param p1, "x"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2672
    iget v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionX:I

    sub-int v0, p1, v6

    .line 2673
    .local v0, "deltaX":I
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 2674
    .local v1, "distance":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getScrollX()I

    move-result v6

    if-eqz v6, :cond_4

    move v4, v8

    .line 2675
    .local v4, "overscroll":Z
    :goto_0
    if-nez v4, :cond_0

    iget v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchSlop:I

    if-le v1, v6, :cond_7

    .line 2676
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->createScrollingCache()V

    .line 2677
    if-eqz v4, :cond_5

    .line 2678
    const/4 v6, 0x5

    iput v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 2679
    iput v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionCorrection:I

    .line 2684
    :goto_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getHandler()Landroid/os/Handler;

    move-result-object v2

    .line 2688
    .local v2, "handler":Landroid/os/Handler;
    if-eqz v2, :cond_1

    .line 2689
    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingCheckForLongPress:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2691
    :cond_1
    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setPressed(Z)V

    .line 2692
    iget v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionPosition:I

    iget v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    sub-int/2addr v6, v9

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 2693
    .local v3, "motionView":Landroid/view/View;
    if-eqz v3, :cond_2

    .line 2694
    invoke-virtual {v3, v7}, Landroid/view/View;->setPressed(Z)V

    .line 2696
    :cond_2
    invoke-virtual {p0, v8}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->reportScrollStateChange(I)V

    .line 2699
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    .line 2700
    .local v5, "parent":Landroid/view/ViewParent;
    if-eqz v5, :cond_3

    .line 2701
    invoke-interface {v5, v8}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 2703
    :cond_3
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->scrollIfNeeded(I)V

    move v6, v8

    .line 2707
    .end local v2    # "handler":Landroid/os/Handler;
    .end local v3    # "motionView":Landroid/view/View;
    .end local v5    # "parent":Landroid/view/ViewParent;
    :goto_2
    return v6

    .end local v4    # "overscroll":Z
    :cond_4
    move v4, v7

    .line 2674
    goto :goto_0

    .line 2681
    .restart local v4    # "overscroll":Z
    :cond_5
    const/4 v6, 0x3

    iput v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 2682
    if-lez v0, :cond_6

    iget v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchSlop:I

    :goto_3
    iput v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionCorrection:I

    goto :goto_1

    :cond_6
    iget v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchSlop:I

    neg-int v6, v6

    goto :goto_3

    :cond_7
    move v6, v7

    .line 2707
    goto :goto_2
.end method

.method private updateOnScreenCheckedViews()V
    .locals 8

    .prologue
    .line 975
    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    .line 976
    .local v2, "firstPos":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v1

    .line 977
    .local v1, "count":I
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0xb

    if-lt v6, v7, :cond_1

    const/4 v5, 0x1

    .line 978
    .local v5, "useActivated":Z
    :goto_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v1, :cond_3

    .line 979
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 980
    .local v0, "child":Landroid/view/View;
    add-int v4, v2, v3

    .line 982
    .local v4, "position":I
    instance-of v6, v0, Landroid/widget/Checkable;

    if-eqz v6, :cond_2

    .line 983
    check-cast v0, Landroid/widget/Checkable;

    .end local v0    # "child":Landroid/view/View;
    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v6, v4}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v6

    invoke-interface {v0, v6}, Landroid/widget/Checkable;->setChecked(Z)V

    .line 978
    :cond_0
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 977
    .end local v3    # "i":I
    .end local v4    # "position":I
    .end local v5    # "useActivated":Z
    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    .line 984
    .restart local v0    # "child":Landroid/view/View;
    .restart local v3    # "i":I
    .restart local v4    # "position":I
    .restart local v5    # "useActivated":Z
    :cond_2
    if-eqz v5, :cond_0

    .line 985
    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v6, v4}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v6

    invoke-virtual {v0, v6}, Landroid/view/View;->setActivated(Z)V

    goto :goto_2

    .line 988
    .end local v0    # "child":Landroid/view/View;
    .end local v4    # "position":I
    :cond_3
    return-void
.end method

.method private useDefaultSelector()V
    .locals 2

    .prologue
    .line 1246
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1080062

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 1247
    return-void
.end method


# virtual methods
.method public addTouchables(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3530
    .local p1, "views":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v2

    .line 3531
    .local v2, "count":I
    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    .line 3532
    .local v3, "firstPosition":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    .line 3534
    .local v0, "adapter":Landroid/widget/ListAdapter;
    if-nez v0, :cond_1

    .line 3545
    :cond_0
    return-void

    .line 3538
    :cond_1
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v2, :cond_0

    .line 3539
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 3540
    .local v1, "child":Landroid/view/View;
    add-int v5, v3, v4

    invoke-interface {v0, v5}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 3541
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3543
    :cond_2
    invoke-virtual {v1, p1}, Landroid/view/View;->addTouchables(Ljava/util/ArrayList;)V

    .line 3538
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public checkInputConnectionProxy(Landroid/view/View;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 5106
    const/4 v0, 0x0

    return v0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 5127
    instance-of v0, p1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    return v0
.end method

.method public clearChoices()V
    .locals 1

    .prologue
    .line 821
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_0

    .line 822
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    .line 824
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v0, :cond_1

    .line 825
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/support/v4/util/LongSparseArray;->clear()V

    .line 827
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedItemCount:I

    .line 828
    return-void
.end method

.method protected computeHorizontalScrollExtent()I
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 1549
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v0

    .line 1550
    .local v0, "count":I
    if-lez v0, :cond_3

    .line 1551
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSmoothScrollbarEnabled:Z

    if-eqz v7, :cond_2

    .line 1552
    mul-int/lit8 v1, v0, 0x64

    .line 1554
    .local v1, "extent":I
    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1555
    .local v4, "view":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 1556
    .local v2, "left":I
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v5

    .line 1557
    .local v5, "width":I
    if-lez v5, :cond_0

    .line 1558
    mul-int/lit8 v6, v2, 0x64

    div-int/2addr v6, v5

    add-int/2addr v1, v6

    .line 1561
    :cond_0
    add-int/lit8 v6, v0, -0x1

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1562
    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v3

    .line 1563
    .local v3, "right":I
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v5

    .line 1564
    if-lez v5, :cond_1

    .line 1565
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v6

    sub-int v6, v3, v6

    mul-int/lit8 v6, v6, 0x64

    div-int/2addr v6, v5

    sub-int/2addr v1, v6

    .line 1573
    .end local v1    # "extent":I
    .end local v2    # "left":I
    .end local v3    # "right":I
    .end local v4    # "view":Landroid/view/View;
    .end local v5    # "width":I
    :cond_1
    :goto_0
    return v1

    .line 1570
    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    move v1, v6

    .line 1573
    goto :goto_0
.end method

.method protected computeHorizontalScrollOffset()I
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 1578
    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    .line 1579
    .local v2, "firstPosition":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v0

    .line 1580
    .local v0, "childCount":I
    if-ltz v2, :cond_0

    if-lez v0, :cond_0

    .line 1581
    iget-boolean v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSmoothScrollbarEnabled:Z

    if-eqz v8, :cond_1

    .line 1582
    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1583
    .local v5, "view":Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v4

    .line 1584
    .local v4, "left":I
    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v6

    .line 1585
    .local v6, "width":I
    if-lez v6, :cond_0

    .line 1586
    mul-int/lit8 v8, v2, 0x64

    mul-int/lit8 v9, v4, 0x64

    div-int/2addr v9, v6

    sub-int/2addr v8, v9

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getScrollX()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    iget v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    int-to-float v10, v10

    mul-float/2addr v9, v10

    const/high16 v10, 0x42c80000    # 100.0f

    mul-float/2addr v9, v10

    float-to-int v9, v9

    add-int/2addr v8, v9

    invoke-static {v8, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 1602
    .end local v4    # "left":I
    .end local v5    # "view":Landroid/view/View;
    .end local v6    # "width":I
    :cond_0
    :goto_0
    return v7

    .line 1591
    :cond_1
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    .line 1592
    .local v1, "count":I
    if-nez v2, :cond_2

    .line 1593
    const/4 v3, 0x0

    .line 1599
    .local v3, "index":I
    :goto_1
    int-to-float v7, v2

    int-to-float v8, v0

    int-to-float v9, v3

    int-to-float v10, v1

    div-float/2addr v9, v10

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    float-to-int v7, v7

    goto :goto_0

    .line 1594
    .end local v3    # "index":I
    :cond_2
    add-int v7, v2, v0

    if-ne v7, v1, :cond_3

    .line 1595
    move v3, v1

    .restart local v3    # "index":I
    goto :goto_1

    .line 1597
    .end local v3    # "index":I
    :cond_3
    div-int/lit8 v7, v0, 0x2

    add-int v3, v2, v7

    .restart local v3    # "index":I
    goto :goto_1
.end method

.method protected computeHorizontalScrollRange()I
    .locals 3

    .prologue
    .line 1608
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSmoothScrollbarEnabled:Z

    if-eqz v1, :cond_1

    .line 1609
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    mul-int/lit8 v1, v1, 0x64

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1610
    .local v0, "result":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getScrollX()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1612
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getScrollX()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 1617
    :cond_0
    :goto_0
    return v0

    .line 1615
    .end local v0    # "result":I
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    .restart local v0    # "result":I
    goto :goto_0
.end method

.method confirmCheckedPositionsById()V
    .locals 18
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 4845
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3}, Landroid/util/SparseBooleanArray;->clear()V

    .line 4847
    const/4 v2, 0x0

    .line 4848
    .local v2, "checkedCountChanged":Z
    const/4 v9, 0x0

    .local v9, "checkedIndex":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v3}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v3

    if-ge v9, v3, :cond_4

    .line 4849
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v3, v9}, Landroid/support/v4/util/LongSparseArray;->keyAt(I)J

    move-result-wide v6

    .line 4850
    .local v6, "id":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v3, v9}, Landroid/support/v4/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 4852
    .local v5, "lastPos":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3, v5}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v12

    .line 4853
    .local v12, "lastPosId":J
    cmp-long v3, v6, v12

    if-eqz v3, :cond_3

    .line 4855
    const/4 v3, 0x0

    add-int/lit8 v4, v5, -0x14

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v17

    .line 4856
    .local v17, "start":I
    add-int/lit8 v3, v5, 0x14

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 4857
    .local v10, "end":I
    const/4 v11, 0x0

    .line 4858
    .local v11, "found":Z
    move/from16 v16, v17

    .local v16, "searchPos":I
    :goto_1
    move/from16 v0, v16

    if-ge v0, v10, :cond_0

    .line 4859
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    move/from16 v0, v16

    invoke-interface {v3, v0}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v14

    .line 4860
    .local v14, "searchId":J
    cmp-long v3, v6, v14

    if-nez v3, :cond_2

    .line 4861
    const/4 v11, 0x1

    .line 4862
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    const/4 v4, 0x1

    move/from16 v0, v16

    invoke-virtual {v3, v0, v4}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 4863
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v9, v4}, Landroid/support/v4/util/LongSparseArray;->setValueAt(ILjava/lang/Object;)V

    .line 4868
    .end local v14    # "searchId":J
    :cond_0
    if-nez v11, :cond_1

    .line 4869
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v3, v6, v7}, Landroid/support/v4/util/LongSparseArray;->delete(J)V

    .line 4870
    add-int/lit8 v9, v9, -0x1

    .line 4871
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedItemCount:I

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedItemCount:I

    .line 4872
    const/4 v2, 0x1

    .line 4874
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-le v3, v4, :cond_1

    .line 4875
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceActionMode:Ljava/lang/Object;

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMultiChoiceModeCallback:Ljava/lang/Object;

    if-eqz v3, :cond_1

    .line 4876
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMultiChoiceModeCallback:Ljava/lang/Object;

    check-cast v3, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceActionMode:Ljava/lang/Object;

    check-cast v4, Landroid/view/ActionMode;

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;->onItemCheckedStateChanged(Landroid/view/ActionMode;IJZ)V

    .line 4848
    .end local v10    # "end":I
    .end local v11    # "found":Z
    .end local v16    # "searchPos":I
    .end local v17    # "start":I
    :cond_1
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 4858
    .restart local v10    # "end":I
    .restart local v11    # "found":Z
    .restart local v14    # "searchId":J
    .restart local v16    # "searchPos":I
    .restart local v17    # "start":I
    :cond_2
    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    .line 4881
    .end local v10    # "end":I
    .end local v11    # "found":Z
    .end local v14    # "searchId":J
    .end local v16    # "searchPos":I
    .end local v17    # "start":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    const/4 v4, 0x1

    invoke-virtual {v3, v5, v4}, Landroid/util/SparseBooleanArray;->put(IZ)V

    goto :goto_2

    .line 4885
    .end local v5    # "lastPos":I
    .end local v6    # "id":J
    .end local v12    # "lastPosId":J
    :cond_4
    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceActionMode:Ljava/lang/Object;

    if-eqz v3, :cond_5

    .line 4886
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceActionMode:Ljava/lang/Object;

    check-cast v3, Landroid/view/ActionMode;

    invoke-virtual {v3}, Landroid/view/ActionMode;->invalidate()V

    .line 4888
    :cond_5
    return-void
.end method

.method createContextMenuInfo(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "id"    # J

    .prologue
    .line 2371
    new-instance v0, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterContextMenuInfo;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterContextMenuInfo;-><init>(Landroid/view/View;IJ)V

    return-object v0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 2019
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDrawSelectorOnTop:Z

    .line 2020
    .local v0, "drawSelectorOnTop":Z
    if-nez v0, :cond_0

    .line 2021
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->drawSelector(Landroid/graphics/Canvas;)V

    .line 2024
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2026
    if-eqz v0, :cond_1

    .line 2027
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->drawSelector(Landroid/graphics/Canvas;)V

    .line 2033
    :cond_1
    return-void
.end method

.method protected dispatchSetPressed(Z)V
    .locals 0
    .param p1, "pressed"    # Z

    .prologue
    .line 2571
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 3339
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->draw(Landroid/graphics/Canvas;)V

    .line 3340
    iget-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    if-eqz v7, :cond_3

    .line 3341
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getScrollX()I

    move-result v4

    .line 3342
    .local v4, "scrollX":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->isFinished()Z

    move-result v7

    if-nez v7, :cond_1

    .line 3343
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    .line 3344
    .local v3, "restoreCount":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    iget v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mGlowPaddingTop:I

    add-int v5, v7, v8

    .line 3345
    .local v5, "topPadding":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    iget v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mGlowPaddingBottom:I

    add-int v0, v7, v8

    .line 3346
    .local v0, "bottomPadding":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getHeight()I

    move-result v7

    sub-int/2addr v7, v5

    sub-int v2, v7, v0

    .line 3347
    .local v2, "height":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v6

    .line 3349
    .local v6, "width":I
    const/4 v7, 0x0

    iget v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPositionDistanceGuess:I

    add-int/2addr v8, v4

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 3353
    .local v1, "edgeX":I
    const/high16 v7, -0x3d4c0000    # -90.0f

    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->rotate(F)V

    .line 3354
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getHeight()I

    move-result v7

    neg-int v7, v7

    add-int/2addr v7, v5

    int-to-float v7, v7

    int-to-float v8, v1

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3355
    iget-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    invoke-virtual {v7, v2, v2}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->setSize(II)V

    .line 3357
    iget-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    invoke-virtual {v7, p1}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 3358
    iget-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    invoke-virtual {v7, v1, v5}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->setPosition(II)V

    .line 3360
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->invalidate()V

    .line 3362
    :cond_0
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 3364
    .end local v0    # "bottomPadding":I
    .end local v1    # "edgeX":I
    .end local v2    # "height":I
    .end local v3    # "restoreCount":I
    .end local v5    # "topPadding":I
    .end local v6    # "width":I
    :cond_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowBottom:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->isFinished()Z

    move-result v7

    if-nez v7, :cond_3

    .line 3365
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    .line 3366
    .restart local v3    # "restoreCount":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    iget v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mGlowPaddingTop:I

    add-int v5, v7, v8

    .line 3367
    .restart local v5    # "topPadding":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    iget v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mGlowPaddingBottom:I

    add-int v0, v7, v8

    .line 3368
    .restart local v0    # "bottomPadding":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getHeight()I

    move-result v7

    sub-int/2addr v7, v5

    sub-int v2, v7, v0

    .line 3369
    .restart local v2    # "height":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v6

    .line 3371
    .restart local v6    # "width":I
    iget v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastPositionDistanceGuess:I

    add-int/2addr v7, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 3372
    .restart local v1    # "edgeX":I
    const/high16 v7, 0x42b40000    # 90.0f

    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->rotate(F)V

    .line 3373
    neg-int v7, v5

    int-to-float v7, v7

    neg-int v8, v1

    int-to-float v8, v8

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3375
    iget-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowBottom:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    invoke-virtual {v7, v2, v2}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->setSize(II)V

    .line 3377
    iget-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowBottom:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    invoke-virtual {v7, p1}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 3381
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->invalidate()V

    .line 3383
    :cond_2
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 3386
    .end local v0    # "bottomPadding":I
    .end local v1    # "edgeX":I
    .end local v2    # "height":I
    .end local v3    # "restoreCount":I
    .end local v4    # "scrollX":I
    .end local v5    # "topPadding":I
    .end local v6    # "width":I
    :cond_3
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 2201
    invoke-super {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->drawableStateChanged()V

    .line 2202
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->updateSelectorState()V

    .line 2203
    return-void
.end method

.method protected abstract fillGap(Z)V
.end method

.method protected findClosestMotionCol(I)I
    .locals 3
    .param p1, "x"    # I

    .prologue
    const/4 v2, -0x1

    .line 4698
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v0

    .line 4699
    .local v0, "childCount":I
    if-nez v0, :cond_1

    move v1, v2

    .line 4704
    :cond_0
    :goto_0
    return v1

    .line 4703
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->findMotionCol(I)I

    move-result v1

    .line 4704
    .local v1, "motionCol":I
    if-ne v1, v2, :cond_0

    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    add-int/2addr v2, v0

    add-int/lit8 v1, v2, -0x1

    goto :goto_0
.end method

.method protected abstract findMotionCol(I)I
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 4

    .prologue
    .line 5111
    new-instance v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;-><init>(III)V

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "x0"    # Landroid/util/AttributeSet;

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 5117
    new-instance v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    invoke-direct {v0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 5122
    new-instance v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public getCacheColorHint()I
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    .prologue
    .line 5187
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCacheColorHint:I

    return v0
.end method

.method public getCheckedItemCount()I
    .locals 1

    .prologue
    .line 745
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedItemCount:I

    return v0
.end method

.method public getCheckedItemIds()[J
    .locals 6

    .prologue
    .line 802
    iget v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v4, :cond_2

    .line 803
    :cond_0
    const/4 v4, 0x0

    new-array v3, v4, [J

    .line 814
    :cond_1
    return-object v3

    .line 806
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    .line 807
    .local v2, "idStates":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/lang/Integer;>;"
    invoke-virtual {v2}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v0

    .line 808
    .local v0, "count":I
    new-array v3, v0, [J

    .line 810
    .local v3, "ids":[J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 811
    invoke-virtual {v2, v1}, Landroid/support/v4/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    aput-wide v4, v3, v1

    .line 810
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getCheckedItemPosition()I
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 774
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 775
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v0

    .line 778
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getCheckedItemPositions()Landroid/util/SparseBooleanArray;
    .locals 1

    .prologue
    .line 789
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    if-eqz v0, :cond_0

    .line 790
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    .line 792
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getChoiceMode()I
    .locals 1

    .prologue
    .line 996
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    return v0
.end method

.method protected getContextMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    .prologue
    .line 2498
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    return-object v0
.end method

.method public getFocusedRect(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "r"    # Landroid/graphics/Rect;

    .prologue
    .line 1233
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getSelectedView()Landroid/view/View;

    move-result-object v0

    .line 1234
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-ne v1, p0, :cond_0

    .line 1237
    invoke-virtual {v0, p1}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    .line 1238
    invoke-virtual {p0, v0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1243
    :goto_0
    return-void

    .line 1241
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getFocusedRect(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method protected getFooterViewsCount()I
    .locals 1

    .prologue
    .line 4641
    const/4 v0, 0x0

    return v0
.end method

.method protected getHeaderViewsCount()I
    .locals 1

    .prologue
    .line 4631
    const/4 v0, 0x0

    return v0
.end method

.method protected getHorizontalScrollFactor()F
    .locals 4

    .prologue
    .line 3323
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mHorizontalScrollFactor:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_1

    .line 3324
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 3325
    .local v0, "outValue":Landroid/util/TypedValue;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f010012

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3326
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Expected theme to define listPreferredItemHeight."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3329
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mHorizontalScrollFactor:F

    .line 3331
    .end local v0    # "outValue":Landroid/util/TypedValue;
    :cond_1
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mHorizontalScrollFactor:F

    return v1
.end method

.method protected getHorizontalScrollbarHeight()I
    .locals 1

    .prologue
    .line 1073
    invoke-super {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getHorizontalScrollbarHeight()I

    move-result v0

    return v0
.end method

.method protected getLeftFadingEdgeStrength()F
    .locals 5

    .prologue
    .line 1622
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v0

    .line 1623
    .local v0, "count":I
    invoke-super {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getLeftFadingEdgeStrength()F

    move-result v1

    .line 1624
    .local v1, "fadeEdge":F
    if-nez v0, :cond_1

    .line 1633
    .end local v1    # "fadeEdge":F
    :cond_0
    :goto_0
    return v1

    .line 1627
    .restart local v1    # "fadeEdge":F
    :cond_1
    iget v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    if-lez v4, :cond_2

    .line 1628
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_0

    .line 1631
    :cond_2
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 1632
    .local v3, "left":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getHorizontalFadingEdgeLength()I

    move-result v4

    int-to-float v2, v4

    .line 1633
    .local v2, "fadeLength":F
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getPaddingLeft()I

    move-result v4

    if-ge v3, v4, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getPaddingLeft()I

    move-result v4

    sub-int v4, v3, v4

    neg-int v4, v4

    int-to-float v4, v4

    div-float v1, v4, v2

    goto :goto_0
.end method

.method public getListPaddingBottom()I
    .locals 1

    .prologue
    .line 1779
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    return v0
.end method

.method public getListPaddingLeft()I
    .locals 1

    .prologue
    .line 1791
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    return v0
.end method

.method public getListPaddingRight()I
    .locals 1

    .prologue
    .line 1803
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    return v0
.end method

.method public getListPaddingTop()I
    .locals 1

    .prologue
    .line 1767
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    return v0
.end method

.method protected getRightFadingEdgeStrength()F
    .locals 7

    .prologue
    .line 1639
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v0

    .line 1640
    .local v0, "count":I
    invoke-super {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->getRightFadingEdgeStrength()F

    move-result v1

    .line 1641
    .local v1, "fadeEdge":F
    if-nez v0, :cond_1

    .line 1651
    .end local v1    # "fadeEdge":F
    :cond_0
    :goto_0
    return v1

    .line 1644
    .restart local v1    # "fadeEdge":F
    :cond_1
    iget v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    add-int/2addr v5, v0

    add-int/lit8 v5, v5, -0x1

    iget v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    add-int/lit8 v6, v6, -0x1

    if-ge v5, v6, :cond_2

    .line 1645
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_0

    .line 1648
    :cond_2
    add-int/lit8 v5, v0, -0x1

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v3

    .line 1649
    .local v3, "right":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v4

    .line 1650
    .local v4, "width":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getHorizontalFadingEdgeLength()I

    move-result v5

    int-to-float v2, v5

    .line 1651
    .local v2, "fadeLength":F
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getPaddingRight()I

    move-result v5

    sub-int v5, v4, v5

    if-le v3, v5, :cond_0

    sub-int v5, v3, v4

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getPaddingRight()I

    move-result v6

    add-int/2addr v5, v6

    int-to-float v5, v5

    div-float v1, v5, v2

    goto :goto_0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    .prologue
    .line 1751
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedPosition:I

    if-ltz v0, :cond_0

    .line 1752
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedPosition:I

    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1754
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSelector()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 2138
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelector:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getSolidColor()I
    .locals 1

    .prologue
    .line 5155
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCacheColorHint:I

    return v0
.end method

.method public getTranscriptMode()I
    .locals 1

    .prologue
    .line 5150
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTranscriptMode:I

    return v0
.end method

.method protected handleDataChanged()V
    .locals 15

    .prologue
    const/4 v14, 0x5

    const/4 v13, -0x1

    const/4 v8, 0x3

    const/4 v9, 0x1

    const/4 v12, 0x0

    .line 4892
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    .line 4893
    .local v1, "count":I
    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastHandledItemCount:I

    .line 4894
    .local v3, "lastHandledItemCount":I
    iget v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    iput v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastHandledItemCount:I

    .line 4896
    sget v10, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v11, 0x10

    if-lt v10, v11, :cond_0

    iget v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v10}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 4898
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->confirmCheckedPositionsById()V

    .line 4902
    :cond_0
    iget-object v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    invoke-virtual {v10}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->clearTransientStateViews()V

    .line 4904
    if-lez v1, :cond_e

    .line 4909
    iget-boolean v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mNeedSync:Z

    if-eqz v10, :cond_7

    .line 4911
    iput-boolean v12, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mNeedSync:Z

    .line 4912
    const/4 v10, 0x0

    iput-object v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingSync:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;

    .line 4914
    iget v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTranscriptMode:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_2

    .line 4915
    iput v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLayoutMode:I

    .line 5031
    :cond_1
    :goto_0
    return-void

    .line 4917
    :cond_2
    iget v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTranscriptMode:I

    if-ne v10, v9, :cond_6

    .line 4918
    iget-boolean v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mForceTranscriptScroll:Z

    if-eqz v10, :cond_3

    .line 4919
    iput-boolean v12, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mForceTranscriptScroll:Z

    .line 4920
    iput v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLayoutMode:I

    goto :goto_0

    .line 4923
    :cond_3
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v0

    .line 4924
    .local v0, "childCount":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v10

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getPaddingRight()I

    move-result v11

    sub-int v5, v10, v11

    .line 4925
    .local v5, "listRight":I
    add-int/lit8 v10, v0, -0x1

    invoke-virtual {p0, v10}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 4926
    .local v2, "lastChild":Landroid/view/View;
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v4

    .line 4927
    .local v4, "lastRight":I
    :goto_1
    iget v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    add-int/2addr v10, v0

    if-lt v10, v3, :cond_5

    if-gt v4, v5, :cond_5

    .line 4929
    iput v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLayoutMode:I

    goto :goto_0

    .end local v4    # "lastRight":I
    :cond_4
    move v4, v5

    .line 4926
    goto :goto_1

    .line 4934
    .restart local v4    # "lastRight":I
    :cond_5
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->awakenScrollBars()Z

    .line 4937
    .end local v0    # "childCount":I
    .end local v2    # "lastChild":Landroid/view/View;
    .end local v4    # "lastRight":I
    .end local v5    # "listRight":I
    :cond_6
    iget v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSyncMode:I

    packed-switch v10, :pswitch_data_0

    .line 4985
    :cond_7
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isInTouchMode()Z

    move-result v10

    if-nez v10, :cond_d

    .line 4987
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getSelectedItemPosition()I

    move-result v6

    .line 4990
    .local v6, "newPos":I
    if-lt v6, v1, :cond_8

    .line 4991
    add-int/lit8 v6, v1, -0x1

    .line 4993
    :cond_8
    if-gez v6, :cond_9

    .line 4994
    const/4 v6, 0x0

    .line 4998
    :cond_9
    invoke-virtual {p0, v6, v9}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->lookForSelectablePosition(IZ)I

    move-result v7

    .line 5000
    .local v7, "selectablePos":I
    if-ltz v7, :cond_c

    .line 5001
    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setNextSelectedPositionInt(I)V

    goto :goto_0

    .line 4939
    .end local v6    # "newPos":I
    .end local v7    # "selectablePos":I
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isInTouchMode()Z

    move-result v10

    if-eqz v10, :cond_a

    .line 4944
    iput v14, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLayoutMode:I

    .line 4945
    iget v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSyncPosition:I

    invoke-static {v12, v8}, Ljava/lang/Math;->max(II)I

    move-result v8

    add-int/lit8 v9, v1, -0x1

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    iput v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSyncPosition:I

    goto :goto_0

    .line 4951
    :cond_a
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->findSyncPosition()I

    move-result v6

    .line 4952
    .restart local v6    # "newPos":I
    if-ltz v6, :cond_7

    .line 4954
    invoke-virtual {p0, v6, v9}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->lookForSelectablePosition(IZ)I

    move-result v7

    .line 4955
    .restart local v7    # "selectablePos":I
    if-ne v7, v6, :cond_7

    .line 4957
    iput v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSyncPosition:I

    .line 4959
    iget-wide v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSyncWidth:J

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v10

    int-to-long v10, v10

    cmp-long v8, v8, v10

    if-nez v8, :cond_b

    .line 4962
    iput v14, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLayoutMode:I

    .line 4970
    :goto_2
    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setNextSelectedPositionInt(I)V

    goto/16 :goto_0

    .line 4966
    :cond_b
    const/4 v8, 0x2

    iput v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLayoutMode:I

    goto :goto_2

    .line 4978
    .end local v6    # "newPos":I
    .end local v7    # "selectablePos":I
    :pswitch_1
    iput v14, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLayoutMode:I

    .line 4979
    iget v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSyncPosition:I

    invoke-static {v12, v8}, Ljava/lang/Math;->max(II)I

    move-result v8

    add-int/lit8 v9, v1, -0x1

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    iput v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSyncPosition:I

    goto/16 :goto_0

    .line 5005
    .restart local v6    # "newPos":I
    .restart local v7    # "selectablePos":I
    :cond_c
    invoke-virtual {p0, v6, v12}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->lookForSelectablePosition(IZ)I

    move-result v7

    .line 5006
    if-ltz v7, :cond_e

    .line 5007
    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setNextSelectedPositionInt(I)V

    goto/16 :goto_0

    .line 5014
    .end local v6    # "newPos":I
    .end local v7    # "selectablePos":I
    :cond_d
    iget v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mResurrectToPosition:I

    if-gez v10, :cond_1

    .line 5022
    :cond_e
    iget-boolean v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mStackFromRight:Z

    if-eqz v10, :cond_f

    :goto_3
    iput v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLayoutMode:I

    .line 5023
    iput v13, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedPosition:I

    .line 5024
    const-wide/high16 v8, -0x8000000000000000L

    iput-wide v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedColId:J

    .line 5025
    iput v13, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mNextSelectedPosition:I

    .line 5026
    const-wide/high16 v8, -0x8000000000000000L

    iput-wide v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mNextSelectedColId:J

    .line 5027
    iput-boolean v12, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mNeedSync:Z

    .line 5028
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingSync:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;

    .line 5029
    iput v13, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectorPosition:I

    .line 5030
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->checkSelectionChanged()V

    goto/16 :goto_0

    :cond_f
    move v8, v9

    .line 5022
    goto :goto_3

    .line 4937
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected hideSelector()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 4654
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedPosition:I

    if-eq v0, v2, :cond_2

    .line 4655
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLayoutMode:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 4656
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedPosition:I

    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mResurrectToPosition:I

    .line 4658
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mNextSelectedPosition:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mNextSelectedPosition:I

    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedPosition:I

    if-eq v0, v1, :cond_1

    .line 4659
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mNextSelectedPosition:I

    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mResurrectToPosition:I

    .line 4661
    :cond_1
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setSelectedPositionInt(I)V

    .line 4662
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setNextSelectedPositionInt(I)V

    .line 4663
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedLeft:I

    .line 4665
    :cond_2
    return-void
.end method

.method protected invalidateParentIfNeeded()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 2859
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mViewHelper:Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelper;->isHardwareAccelerated()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2860
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 2862
    :cond_0
    return-void
.end method

.method public invalidateViews()V
    .locals 1

    .prologue
    .line 4711
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataChanged:Z

    .line 4712
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->rememberSyncState()V

    .line 4713
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->requestLayout()V

    .line 4714
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->invalidate()V

    .line 4715
    return-void
.end method

.method protected invokeOnItemScrollListener()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1123
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mOnScrollListener:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 1124
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mOnScrollListener:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$OnScrollListener;

    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v2

    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    invoke-interface {v0, p0, v1, v2, v3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$OnScrollListener;->onScroll(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;III)V

    .line 1126
    :cond_0
    invoke-virtual {p0, v4, v4, v4, v4}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->onScrollChanged(IIII)V

    .line 1127
    return-void
.end method

.method public isItemChecked(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 759
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_0

    .line 760
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    .line 763
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isScrollingCacheEnabled()Z
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    .prologue
    .line 1208
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mScrollingCacheEnabled:Z

    return v0
.end method

.method public isSmoothScrollbarEnabled()Z
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    .prologue
    .line 1105
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSmoothScrollbarEnabled:Z

    return v0
.end method

.method public isStackFromRight()Z
    .locals 1

    .prologue
    .line 1255
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mStackFromRight:Z

    return v0
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 2247
    invoke-super {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->jumpDrawablesToCurrentState()V

    .line 2248
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 2249
    :cond_0
    return-void
.end method

.method protected keyPressed()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 2145
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isClickable()Z

    move-result v5

    if-nez v5, :cond_1

    .line 2180
    :cond_0
    :goto_0
    return-void

    .line 2149
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelector:Landroid/graphics/drawable/Drawable;

    .line 2150
    .local v2, "selector":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectorRect:Landroid/graphics/Rect;

    .line 2151
    .local v3, "selectorRect":Landroid/graphics/Rect;
    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isFocused()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->touchModeDrawsInPressedState()Z

    move-result v5

    if-eqz v5, :cond_0

    :cond_2
    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2154
    iget v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedPosition:I

    iget v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    sub-int/2addr v5, v6

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 2156
    .local v4, "v":Landroid/view/View;
    if-eqz v4, :cond_3

    .line 2157
    invoke-virtual {v4}, Landroid/view/View;->hasFocusable()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2158
    invoke-virtual {v4, v7}, Landroid/view/View;->setPressed(Z)V

    .line 2160
    :cond_3
    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setPressed(Z)V

    .line 2162
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isLongClickable()Z

    move-result v1

    .line 2163
    .local v1, "longClickable":Z
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2164
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_4

    instance-of v5, v0, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v5, :cond_4

    .line 2165
    if-eqz v1, :cond_6

    .line 2166
    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    .end local v0    # "d":Landroid/graphics/drawable/Drawable;
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 2172
    :cond_4
    :goto_1
    if-eqz v1, :cond_0

    iget-boolean v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataChanged:Z

    if-nez v5, :cond_0

    .line 2173
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingCheckForKeyLongPress:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;

    if-nez v5, :cond_5

    .line 2174
    new-instance v5, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$1;)V

    iput-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingCheckForKeyLongPress:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;

    .line 2176
    :cond_5
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingCheckForKeyLongPress:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;->rememberWindowAttachCount()V

    .line 2177
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingCheckForKeyLongPress:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {p0, v5, v6, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 2169
    .restart local v0    # "d":Landroid/graphics/drawable/Drawable;
    :cond_6
    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    .end local v0    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    goto :goto_1
.end method

.method protected layoutChildren()V
    .locals 0

    .prologue
    .line 1712
    return-void
.end method

.method protected obtainView(I[Z)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "isScrap"    # [Z
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/16 v7, 0x10

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1821
    aput-boolean v5, p2, v5

    .line 1824
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->getTransientStateView(I)Landroid/view/View;

    move-result-object v2

    .line 1825
    .local v2, "scrapView":Landroid/view/View;
    if-eqz v2, :cond_1

    move-object v0, v2

    .line 1889
    :cond_0
    :goto_0
    return-object v0

    .line 1829
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->getScrapView(I)Landroid/view/View;

    move-result-object v2

    .line 1832
    if-eqz v2, :cond_6

    .line 1833
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v4, p1, v2, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1835
    .local v0, "child":Landroid/view/View;
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v4, v7, :cond_2

    .line 1836
    invoke-virtual {v0}, Landroid/view/View;->getImportantForAccessibility()I

    move-result v4

    if-nez v4, :cond_2

    .line 1837
    invoke-virtual {v0, v6}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 1841
    :cond_2
    if-eq v0, v2, :cond_5

    .line 1842
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    invoke-virtual {v4, v2, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 1843
    iget v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCacheColorHint:I

    if-eqz v4, :cond_3

    .line 1844
    iget v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCacheColorHint:I

    invoke-virtual {v0, v4}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    .line 1864
    :cond_3
    :goto_1
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapterHasStableIds:Z

    if-eqz v4, :cond_4

    .line 1865
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 1867
    .local v3, "vlp":Landroid/view/ViewGroup$LayoutParams;
    if-nez v3, :cond_8

    .line 1868
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    .line 1874
    .local v1, "lp":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    :goto_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v4, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    iput-wide v4, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->itemId:J

    .line 1875
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1878
    .end local v1    # "lp":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    .end local v3    # "vlp":Landroid/view/ViewGroup$LayoutParams;
    :cond_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v4}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1879
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAccessibilityDelegate:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$ListItemAccessibilityDelegate;

    if-nez v4, :cond_0

    .line 1880
    new-instance v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$ListItemAccessibilityDelegate;

    invoke-direct {v4, p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$ListItemAccessibilityDelegate;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V

    iput-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAccessibilityDelegate:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$ListItemAccessibilityDelegate;

    goto :goto_0

    .line 1847
    :cond_5
    aput-boolean v6, p2, v5

    .line 1848
    invoke-virtual {v0}, Landroid/view/View;->onFinishTemporaryDetach()V

    goto :goto_1

    .line 1851
    .end local v0    # "child":Landroid/view/View;
    :cond_6
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    const/4 v5, 0x0

    invoke-interface {v4, p1, v5, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1853
    .restart local v0    # "child":Landroid/view/View;
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v4, v7, :cond_7

    .line 1854
    invoke-virtual {v0}, Landroid/view/View;->getImportantForAccessibility()I

    move-result v4

    if-nez v4, :cond_7

    .line 1855
    invoke-virtual {v0, v6}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 1859
    :cond_7
    iget v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCacheColorHint:I

    if-eqz v4, :cond_3

    .line 1860
    iget v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCacheColorHint:I

    invoke-virtual {v0, v4}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    goto :goto_1

    .line 1869
    .restart local v3    # "vlp":Landroid/view/ViewGroup$LayoutParams;
    :cond_8
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 1870
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    .restart local v1    # "lp":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    goto :goto_2

    .end local v1    # "lp":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    :cond_9
    move-object v1, v3

    .line 1872
    check-cast v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    .restart local v1    # "lp":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    goto :goto_2
.end method

.method public offsetChildrenLeftAndRight(I)V
    .locals 3
    .param p1, "offset"    # I

    .prologue
    .line 4616
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v0

    .line 4618
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 4619
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 4620
    .local v2, "v":Landroid/view/View;
    invoke-virtual {v2, p1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 4618
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4622
    .end local v2    # "v":Landroid/view/View;
    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2253
    invoke-super {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->onAttachedToWindow()V

    .line 2255
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 2256
    .local v0, "treeObserver":Landroid/view/ViewTreeObserver;
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 2258
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataSetObserver:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;

    if-nez v1, :cond_0

    .line 2259
    new-instance v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataSetObserver:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;

    .line 2260
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataSetObserver:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2263
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataChanged:Z

    .line 2264
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mOldItemCount:I

    .line 2265
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    .line 2267
    :cond_0
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mIsAttached:Z

    .line 2268
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 6
    .param p1, "extraSpace"    # I

    .prologue
    .line 2208
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mIsChildViewEnabled:Z

    if-eqz v4, :cond_1

    .line 2210
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->onCreateDrawableState(I)[I

    move-result-object v3

    .line 2236
    :cond_0
    :goto_0
    return-object v3

    .line 2216
    :cond_1
    sget-object v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->ENABLED_STATE_SET:[I

    const/4 v5, 0x0

    aget v1, v4, v5

    .line 2221
    .local v1, "enabledState":I
    add-int/lit8 v4, p1, 0x1

    invoke-super {p0, v4}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->onCreateDrawableState(I)[I

    move-result-object v3

    .line 2222
    .local v3, "state":[I
    const/4 v0, -0x1

    .line 2223
    .local v0, "enabledPos":I
    array-length v4, v3

    add-int/lit8 v2, v4, -0x1

    .local v2, "i":I
    :goto_1
    if-ltz v2, :cond_2

    .line 2224
    aget v4, v3, v2

    if-ne v4, v1, :cond_3

    .line 2225
    move v0, v2

    .line 2231
    :cond_2
    if-ltz v0, :cond_0

    .line 2232
    add-int/lit8 v4, v0, 0x1

    array-length v5, v3

    sub-int/2addr v5, v0

    add-int/lit8 v5, v5, -0x1

    invoke-static {v3, v4, v3, v0, v5}, Ljava/lang/System;->arraycopy([II[III)V

    goto :goto_0

    .line 2223
    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_1
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 1
    .param p1, "outAttrs"    # Landroid/view/inputmethod/EditorInfo;

    .prologue
    .line 5098
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2272
    invoke-super {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->onDetachedFromWindow()V

    .line 2277
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->clear()V

    .line 2279
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 2280
    .local v0, "treeObserver":Landroid/view/ViewTreeObserver;
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 2282
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataSetObserver:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;

    if-eqz v1, :cond_0

    .line 2283
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataSetObserver:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2284
    iput-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataSetObserver:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;

    .line 2287
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    if-eqz v1, :cond_1

    .line 2288
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2291
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    if-eqz v1, :cond_2

    .line 2292
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;->stop()V

    .line 2295
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mClearScrollingCache:Ljava/lang/Runnable;

    if-eqz v1, :cond_3

    .line 2296
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mClearScrollingCache:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2299
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPerformClick:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;

    if-eqz v1, :cond_4

    .line 2300
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPerformClick:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2303
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchModeReset:Ljava/lang/Runnable;

    if-eqz v1, :cond_5

    .line 2304
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchModeReset:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2305
    iput-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchModeReset:Ljava/lang/Runnable;

    .line 2307
    :cond_5
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mIsAttached:Z

    .line 2308
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1
    .param p1, "gainFocus"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 1507
    invoke-super {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 1508
    if-eqz p1, :cond_1

    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedPosition:I

    if-gez v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1509
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mIsAttached:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 1512
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataChanged:Z

    .line 1513
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mOldItemCount:I

    .line 1514
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    .line 1516
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->resurrectSelection()Z

    .line 1518
    :cond_1
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xc
    .end annotation

    .prologue
    .line 3296
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v2

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_0

    .line 3297
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 3311
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    :goto_0
    return v2

    .line 3299
    :pswitch_0
    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 3300
    const/16 v2, 0xa

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v1

    .line 3301
    .local v1, "hscroll":F
    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_0

    .line 3302
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getHorizontalScrollFactor()F

    move-result v2

    mul-float/2addr v2, v1

    float-to-int v0, v2

    .line 3303
    .local v0, "delta":I
    invoke-virtual {p0, v0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->trackMotionScroll(II)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3304
    const/4 v2, 0x1

    goto :goto_0

    .line 3297
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 1150
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1151
    const-class v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 1152
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 2
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 1157
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 1159
    const-class v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 1160
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1161
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getFirstVisiblePosition()I

    move-result v0

    if-lez v0, :cond_0

    .line 1162
    const/16 v0, 0x2000

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 1164
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getLastVisiblePosition()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    .line 1165
    const/16 v0, 0x1000

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 1168
    :cond_1
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v11, 0x4

    const/4 v8, 0x1

    const/4 v10, -0x1

    const/4 v7, 0x0

    .line 3424
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 3427
    .local v0, "action":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    if-eqz v9, :cond_0

    .line 3428
    iget-object v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;->stop()V

    .line 3431
    :cond_0
    iget-boolean v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mIsAttached:Z

    if-nez v9, :cond_2

    .line 3506
    :cond_1
    :goto_0
    return v7

    .line 3439
    :cond_2
    and-int/lit16 v9, v0, 0xff

    packed-switch v9, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 3441
    :pswitch_1
    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 3442
    .local v3, "touchMode":I
    const/4 v9, 0x6

    if-eq v3, v9, :cond_3

    const/4 v9, 0x5

    if-ne v3, v9, :cond_4

    .line 3443
    :cond_3
    iput v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionCorrection:I

    move v7, v8

    .line 3444
    goto :goto_0

    .line 3447
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    float-to-int v5, v9

    .line 3448
    .local v5, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    float-to-int v6, v9

    .line 3449
    .local v6, "y":I
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v9

    iput v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mActivePointerId:I

    .line 3451
    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->findMotionCol(I)I

    move-result v1

    .line 3452
    .local v1, "motionPosition":I
    if-eq v3, v11, :cond_5

    if-ltz v1, :cond_5

    .line 3455
    iget v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    sub-int v9, v1, v9

    invoke-virtual {p0, v9}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 3456
    .local v4, "v":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v9

    iput v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionViewOriginalLeft:I

    .line 3457
    iput v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionX:I

    .line 3458
    iput v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionY:I

    .line 3459
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionPosition:I

    .line 3460
    iput v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 3461
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->clearScrollingCache()V

    .line 3463
    .end local v4    # "v":Landroid/view/View;
    :cond_5
    const/high16 v9, -0x80000000

    iput v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastX:I

    .line 3464
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->initOrResetVelocityTracker()V

    .line 3465
    iget-object v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v9, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 3466
    if-ne v3, v11, :cond_1

    move v7, v8

    .line 3467
    goto :goto_0

    .line 3473
    .end local v1    # "motionPosition":I
    .end local v3    # "touchMode":I
    .end local v5    # "x":I
    .end local v6    # "y":I
    :pswitch_2
    iget v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    packed-switch v9, :pswitch_data_1

    goto :goto_0

    .line 3475
    :pswitch_3
    iget v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mActivePointerId:I

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v2

    .line 3476
    .local v2, "pointerIndex":I
    if-ne v2, v10, :cond_6

    .line 3477
    const/4 v2, 0x0

    .line 3478
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v9

    iput v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mActivePointerId:I

    .line 3480
    :cond_6
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v9

    float-to-int v5, v9

    .line 3481
    .restart local v5    # "x":I
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->initVelocityTrackerIfNotExists()V

    .line 3482
    iget-object v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v9, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 3483
    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->startScrollIfNeeded(I)Z

    move-result v9

    if-eqz v9, :cond_1

    move v7, v8

    .line 3484
    goto :goto_0

    .line 3493
    .end local v2    # "pointerIndex":I
    .end local v5    # "x":I
    :pswitch_4
    iput v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 3494
    iput v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mActivePointerId:I

    .line 3495
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->recycleVelocityTracker()V

    .line 3496
    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->reportScrollStateChange(I)V

    goto/16 :goto_0

    .line 3501
    :pswitch_5
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 3439
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch

    .line 3473
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2539
    const/4 v0, 0x0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 2544
    sparse-switch p1, :sswitch_data_0

    .line 2564
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    :cond_1
    :goto_0
    return v1

    .line 2547
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2550
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isClickable()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isPressed()Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedPosition:I

    if-ltz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedPosition:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 2554
    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedPosition:I

    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    sub-int/2addr v2, v3

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2555
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_2

    .line 2556
    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedPosition:I

    iget-wide v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedColId:J

    invoke-virtual {p0, v0, v2, v4, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->performItemClick(Landroid/view/View;IJ)Z

    .line 2557
    invoke-virtual {v0, v6}, Landroid/view/View;->setPressed(Z)V

    .line 2559
    :cond_2
    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setPressed(Z)V

    goto :goto_0

    .line 2544
    nop

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 1682
    invoke-super/range {p0 .. p5}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->onLayout(ZIIII)V

    .line 1683
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mInLayout:Z

    .line 1684
    if-eqz p1, :cond_1

    .line 1685
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v0

    .line 1686
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 1687
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->forceLayout()V

    .line 1686
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1689
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->markChildrenDirty()V

    .line 1692
    .end local v0    # "childCount":I
    .end local v1    # "i":I
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->layoutChildren()V

    .line 1693
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mInLayout:Z

    .line 1695
    sub-int v2, p4, p2

    div-int/lit8 v2, v2, 0x3

    iput v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mOverscrollMax:I

    .line 1696
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v5, 0x1

    .line 1658
    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-nez v6, :cond_0

    .line 1659
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->useDefaultSelector()V

    .line 1661
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    .line 1662
    .local v3, "listPadding":Landroid/graphics/Rect;
    iget v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectionLeftPadding:I

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getPaddingLeft()I

    move-result v7

    add-int/2addr v6, v7

    iput v6, v3, Landroid/graphics/Rect;->left:I

    .line 1663
    iget v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectionTopPadding:I

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getPaddingTop()I

    move-result v7

    add-int/2addr v6, v7

    iput v6, v3, Landroid/graphics/Rect;->top:I

    .line 1664
    iget v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectionRightPadding:I

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getPaddingRight()I

    move-result v7

    add-int/2addr v6, v7

    iput v6, v3, Landroid/graphics/Rect;->right:I

    .line 1665
    iget v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectionBottomPadding:I

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getPaddingBottom()I

    move-result v7

    add-int/2addr v6, v7

    iput v6, v3, Landroid/graphics/Rect;->bottom:I

    .line 1668
    iget v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTranscriptMode:I

    if-ne v6, v5, :cond_1

    .line 1669
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v0

    .line 1670
    .local v0, "childCount":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getPaddingRight()I

    move-result v7

    sub-int v4, v6, v7

    .line 1671
    .local v4, "listRight":I
    add-int/lit8 v6, v0, -0x1

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1672
    .local v1, "lastChild":Landroid/view/View;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v2

    .line 1673
    .local v2, "lastRight":I
    :goto_0
    iget v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    add-int/2addr v6, v0

    iget v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastHandledItemCount:I

    if-lt v6, v7, :cond_3

    if-gt v2, v4, :cond_3

    :goto_1
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mForceTranscriptScroll:Z

    .line 1675
    .end local v0    # "childCount":I
    .end local v1    # "lastChild":Landroid/view/View;
    .end local v2    # "lastRight":I
    .end local v4    # "listRight":I
    :cond_1
    return-void

    .restart local v0    # "childCount":I
    .restart local v1    # "lastChild":Landroid/view/View;
    .restart local v4    # "listRight":I
    :cond_2
    move v2, v4

    .line 1672
    goto :goto_0

    .line 1673
    .restart local v2    # "lastRight":I
    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method

.method protected onOverScrolled(IIZZ)V
    .locals 3
    .param p1, "scrollX"    # I
    .param p2, "scrollY"    # I
    .param p3, "clampedX"    # Z
    .param p4, "clampedY"    # Z

    .prologue
    .line 3284
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getScrollX()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 3285
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getScrollY()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getScrollY()I

    move-result v2

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->onScrollChanged(IIII)V

    .line 3286
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mViewHelper:Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelper;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelper;->setScrollX(I)V

    .line 3287
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->invalidateParentIfNeeded()V

    .line 3289
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->awakenScrollBars()Z

    .line 3291
    :cond_0
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 8
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, -0x1

    const/4 v4, 0x1

    .line 1459
    move-object v0, p1

    check-cast v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;

    .line 1461
    .local v0, "ss":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1462
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataChanged:Z

    .line 1464
    iget v1, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->width:I

    int-to-long v2, v1

    iput-wide v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSyncWidth:J

    .line 1466
    iget-wide v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->selectedId:J

    cmp-long v1, v2, v6

    if-ltz v1, :cond_4

    .line 1467
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mNeedSync:Z

    .line 1468
    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingSync:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;

    .line 1469
    iget-wide v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->selectedId:J

    iput-wide v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSyncColId:J

    .line 1470
    iget v1, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->position:I

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSyncPosition:I

    .line 1471
    iget v1, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->viewLeft:I

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSpecificLeft:I

    .line 1472
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSyncMode:I

    .line 1486
    :cond_0
    :goto_0
    iget-object v1, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->checkState:Landroid/util/SparseBooleanArray;

    if-eqz v1, :cond_1

    .line 1487
    iget-object v1, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->checkState:Landroid/util/SparseBooleanArray;

    iput-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    .line 1490
    :cond_1
    iget-object v1, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->checkIdState:Landroid/support/v4/util/LongSparseArray;

    if-eqz v1, :cond_2

    .line 1491
    iget-object v1, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->checkIdState:Landroid/support/v4/util/LongSparseArray;

    iput-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    .line 1494
    :cond_2
    iget v1, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->checkedItemCount:I

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedItemCount:I

    .line 1496
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_3

    .line 1497
    iget-boolean v1, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->inActionMode:Z

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMultiChoiceModeCallback:Ljava/lang/Object;

    if-eqz v1, :cond_3

    .line 1498
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMultiChoiceModeCallback:Ljava/lang/Object;

    check-cast v1, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceActionMode:Ljava/lang/Object;

    .line 1502
    :cond_3
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->requestLayout()V

    .line 1503
    return-void

    .line 1473
    :cond_4
    iget-wide v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->firstId:J

    cmp-long v1, v2, v6

    if-ltz v1, :cond_0

    .line 1474
    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setSelectedPositionInt(I)V

    .line 1476
    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setNextSelectedPositionInt(I)V

    .line 1477
    iput v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectorPosition:I

    .line 1478
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mNeedSync:Z

    .line 1479
    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingSync:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;

    .line 1480
    iget-wide v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->firstId:J

    iput-wide v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSyncColId:J

    .line 1481
    iget v1, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->position:I

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSyncPosition:I

    .line 1482
    iget v1, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->viewLeft:I

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSpecificLeft:I

    .line 1483
    iput v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSyncMode:I

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 14

    .prologue
    .line 1377
    invoke-super {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v9

    .line 1379
    .local v9, "superState":Landroid/os/Parcelable;
    new-instance v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;

    invoke-direct {v8, v9}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1381
    .local v8, "ss":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;
    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingSync:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;

    if-eqz v11, :cond_0

    .line 1383
    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingSync:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;

    iget-wide v12, v11, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->selectedId:J

    iput-wide v12, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->selectedId:J

    .line 1384
    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingSync:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;

    iget-wide v12, v11, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->firstId:J

    iput-wide v12, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->firstId:J

    .line 1385
    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingSync:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;

    iget v11, v11, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->viewLeft:I

    iput v11, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->viewLeft:I

    .line 1386
    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingSync:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;

    iget v11, v11, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->position:I

    iput v11, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->position:I

    .line 1387
    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingSync:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;

    iget v11, v11, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->width:I

    iput v11, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->width:I

    .line 1388
    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingSync:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;

    iget-object v11, v11, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->filter:Ljava/lang/String;

    iput-object v11, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->filter:Ljava/lang/String;

    .line 1389
    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingSync:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;

    iget-boolean v11, v11, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->inActionMode:Z

    iput-boolean v11, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->inActionMode:Z

    .line 1390
    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingSync:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;

    iget v11, v11, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->checkedItemCount:I

    iput v11, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->checkedItemCount:I

    .line 1391
    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingSync:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;

    iget-object v11, v11, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->checkState:Landroid/util/SparseBooleanArray;

    iput-object v11, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->checkState:Landroid/util/SparseBooleanArray;

    .line 1392
    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingSync:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;

    iget-object v11, v11, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->checkIdState:Landroid/support/v4/util/LongSparseArray;

    iput-object v11, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->checkIdState:Landroid/support/v4/util/LongSparseArray;

    .line 1454
    :goto_0
    return-object v8

    .line 1396
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v11

    if-lez v11, :cond_2

    iget v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    if-lez v11, :cond_2

    const/4 v3, 0x1

    .line 1397
    .local v3, "haveChildren":Z
    :goto_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getSelectedItemId()J

    move-result-wide v6

    .line 1398
    .local v6, "selectedId":J
    iput-wide v6, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->selectedId:J

    .line 1399
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v11

    iput v11, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->width:I

    .line 1401
    const-wide/16 v12, 0x0

    cmp-long v11, v6, v12

    if-ltz v11, :cond_3

    .line 1403
    iget v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedLeft:I

    iput v11, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->viewLeft:I

    .line 1404
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getSelectedItemPosition()I

    move-result v11

    iput v11, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->position:I

    .line 1405
    const-wide/16 v12, -0x1

    iput-wide v12, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->firstId:J

    .line 1432
    :goto_2
    const/4 v11, 0x0

    iput-object v11, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->filter:Ljava/lang/String;

    .line 1433
    sget v11, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v12, 0xb

    if-lt v11, v12, :cond_6

    iget v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    const/4 v12, 0x3

    if-ne v11, v12, :cond_6

    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceActionMode:Ljava/lang/Object;

    if-eqz v11, :cond_6

    const/4 v11, 0x1

    :goto_3
    iput-boolean v11, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->inActionMode:Z

    .line 1436
    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    if-eqz v11, :cond_1

    .line 1438
    :try_start_0
    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v11}, Landroid/util/SparseBooleanArray;->clone()Landroid/util/SparseBooleanArray;

    move-result-object v11

    iput-object v11, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->checkState:Landroid/util/SparseBooleanArray;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1444
    :cond_1
    :goto_4
    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v11, :cond_8

    .line 1445
    new-instance v5, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v5}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    .line 1446
    .local v5, "idState":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/lang/Integer;>;"
    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v11}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v0

    .line 1447
    .local v0, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_5
    if-ge v4, v0, :cond_7

    .line 1448
    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v11, v4}, Landroid/support/v4/util/LongSparseArray;->keyAt(I)J

    move-result-wide v12

    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v11, v4}, Landroid/support/v4/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v5, v12, v13, v11}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 1447
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 1396
    .end local v0    # "count":I
    .end local v3    # "haveChildren":Z
    .end local v4    # "i":I
    .end local v5    # "idState":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/lang/Integer;>;"
    .end local v6    # "selectedId":J
    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    .line 1407
    .restart local v3    # "haveChildren":Z
    .restart local v6    # "selectedId":J
    :cond_3
    if-eqz v3, :cond_5

    iget v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    if-lez v11, :cond_5

    .line 1417
    const/4 v11, 0x0

    invoke-virtual {p0, v11}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 1418
    .local v10, "v":Landroid/view/View;
    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v11

    iput v11, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->viewLeft:I

    .line 1419
    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    .line 1420
    .local v2, "firstPos":I
    iget v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    if-lt v2, v11, :cond_4

    .line 1421
    iget v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    add-int/lit8 v2, v11, -0x1

    .line 1423
    :cond_4
    iput v2, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->position:I

    .line 1424
    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v11, v2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v12

    iput-wide v12, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->firstId:J

    goto :goto_2

    .line 1426
    .end local v2    # "firstPos":I
    .end local v10    # "v":Landroid/view/View;
    :cond_5
    const/4 v11, 0x0

    iput v11, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->viewLeft:I

    .line 1427
    const-wide/16 v12, -0x1

    iput-wide v12, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->firstId:J

    .line 1428
    const/4 v11, 0x0

    iput v11, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->position:I

    goto :goto_2

    .line 1433
    :cond_6
    const/4 v11, 0x0

    goto :goto_3

    .line 1439
    :catch_0
    move-exception v1

    .line 1440
    .local v1, "e":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v1}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    .line 1441
    new-instance v11, Landroid/util/SparseBooleanArray;

    invoke-direct {v11}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v11, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->checkState:Landroid/util/SparseBooleanArray;

    goto :goto_4

    .line 1450
    .end local v1    # "e":Ljava/lang/NoSuchMethodError;
    .restart local v0    # "count":I
    .restart local v4    # "i":I
    .restart local v5    # "idState":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/lang/Integer;>;"
    :cond_7
    iput-object v5, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->checkIdState:Landroid/support/v4/util/LongSparseArray;

    .line 1452
    .end local v0    # "count":I
    .end local v4    # "i":I
    .end local v5    # "idState":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/lang/Integer;>;"
    :cond_8
    iget v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedItemCount:I

    iput v11, v8, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;->checkedItemCount:I

    goto/16 :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 2054
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2055
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataChanged:Z

    .line 2056
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->rememberSyncState()V

    .line 2058
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 28
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 2919
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isEnabled()Z

    move-result v25

    if-nez v25, :cond_2

    .line 2922
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isClickable()Z

    move-result v25

    if-nez v25, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isLongClickable()Z

    move-result v25

    if-eqz v25, :cond_1

    :cond_0
    const/16 v25, 0x1

    .line 3279
    :goto_0
    return v25

    .line 2922
    :cond_1
    const/16 v25, 0x0

    goto :goto_0

    .line 2925
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    move-object/from16 v25, v0

    if-eqz v25, :cond_3

    .line 2926
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;->stop()V

    .line 2929
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mIsAttached:Z

    move/from16 v25, v0

    if-nez v25, :cond_4

    .line 2934
    const/16 v25, 0x0

    goto :goto_0

    .line 2937
    :cond_4
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    .line 2941
    .local v4, "action":I
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->initVelocityTrackerIfNotExists()V

    .line 2942
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 2944
    and-int/lit16 v0, v4, 0xff

    move/from16 v25, v0

    packed-switch v25, :pswitch_data_0

    .line 3279
    :cond_5
    :goto_1
    :pswitch_0
    const/16 v25, 0x1

    goto :goto_0

    .line 2946
    :pswitch_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    move/from16 v25, v0

    packed-switch v25, :pswitch_data_1

    .line 2962
    const/16 v25, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mActivePointerId:I

    .line 2963
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v23, v0

    .line 2964
    .local v23, "x":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v24, v0

    .line 2965
    .local v24, "y":I
    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->pointToPosition(II)I

    move-result v17

    .line 2966
    .local v17, "motionPosition":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataChanged:Z

    move/from16 v25, v0

    if-nez v25, :cond_7

    .line 2967
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    move/from16 v25, v0

    const/16 v26, 0x4

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_a

    if-ltz v17, :cond_a

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v25

    check-cast v25, Landroid/widget/ListAdapter;

    move-object/from16 v0, v25

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v25

    if-eqz v25, :cond_a

    .line 2972
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 2974
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingCheckForTap:Ljava/lang/Runnable;

    move-object/from16 v25, v0

    if-nez v25, :cond_6

    .line 2975
    new-instance v25, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingCheckForTap:Ljava/lang/Runnable;

    .line 2977
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingCheckForTap:Ljava/lang/Runnable;

    move-object/from16 v25, v0

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v26

    move/from16 v0, v26

    int-to-long v0, v0

    move-wide/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move-wide/from16 v2, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2990
    :cond_7
    :goto_2
    if-ltz v17, :cond_8

    .line 2992
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    move/from16 v25, v0

    sub-int v25, v17, v25

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v21

    .line 2993
    .local v21, "v":Landroid/view/View;
    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getLeft()I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionViewOriginalLeft:I

    .line 2995
    .end local v21    # "v":Landroid/view/View;
    :cond_8
    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionX:I

    .line 2996
    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionY:I

    .line 2997
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionPosition:I

    .line 2998
    const/high16 v25, -0x80000000

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastX:I

    .line 3003
    .end local v17    # "motionPosition":I
    .end local v23    # "x":I
    .end local v24    # "y":I
    :goto_3
    invoke-virtual/range {p0 .. p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->performButtonActionOnTouchDown(Landroid/view/MotionEvent;)Z

    move-result v25

    if-eqz v25, :cond_5

    .line 3004
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    move/from16 v25, v0

    if-nez v25, :cond_5

    .line 3005
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingCheckForTap:Ljava/lang/Runnable;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto/16 :goto_1

    .line 2948
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->endFling()V

    .line 2949
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    move-object/from16 v25, v0

    if-eqz v25, :cond_9

    .line 2950
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;->stop()V

    .line 2952
    :cond_9
    const/16 v25, 0x5

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 2953
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionY:I

    .line 2954
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastX:I

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionX:I

    .line 2955
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionCorrection:I

    .line 2956
    const/16 v25, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mActivePointerId:I

    .line 2957
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDirection:I

    goto/16 :goto_3

    .line 2979
    .restart local v17    # "motionPosition":I
    .restart local v23    # "x":I
    .restart local v24    # "y":I
    :cond_a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    move/from16 v25, v0

    const/16 v26, 0x4

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_7

    .line 2981
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->createScrollingCache()V

    .line 2982
    const/16 v25, 0x3

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 2983
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionCorrection:I

    .line 2984
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->findMotionCol(I)I

    move-result v17

    .line 2985
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->flywheelTouch()V

    goto/16 :goto_2

    .line 3012
    .end local v17    # "motionPosition":I
    .end local v23    # "x":I
    .end local v24    # "y":I
    :pswitch_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mActivePointerId:I

    move/from16 v25, v0

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v20

    .line 3013
    .local v20, "pointerIndex":I
    const/16 v25, -0x1

    move/from16 v0, v20

    move/from16 v1, v25

    if-ne v0, v1, :cond_b

    .line 3014
    const/16 v20, 0x0

    .line 3015
    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mActivePointerId:I

    .line 3017
    :cond_b
    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v23, v0

    .line 3019
    .restart local v23    # "x":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataChanged:Z

    move/from16 v25, v0

    if-eqz v25, :cond_c

    .line 3022
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->layoutChildren()V

    .line 3025
    :cond_c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    move/from16 v25, v0

    packed-switch v25, :pswitch_data_2

    :pswitch_4
    goto/16 :goto_1

    .line 3031
    :pswitch_5
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->startScrollIfNeeded(I)Z

    goto/16 :goto_1

    .line 3035
    :pswitch_6
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->scrollIfNeeded(I)V

    goto/16 :goto_1

    .line 3042
    .end local v20    # "pointerIndex":I
    .end local v23    # "x":I
    :pswitch_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    move/from16 v25, v0

    packed-switch v25, :pswitch_data_3

    .line 3183
    :cond_d
    :goto_4
    :pswitch_8
    const/16 v25, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setPressed(Z)V

    .line 3185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    move-object/from16 v25, v0

    if-eqz v25, :cond_e

    .line 3186
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->onRelease()V

    .line 3187
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowBottom:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->onRelease()V

    .line 3191
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->invalidate()V

    .line 3193
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getHandler()Landroid/os/Handler;

    move-result-object v11

    .line 3194
    .local v11, "handler":Landroid/os/Handler;
    if-eqz v11, :cond_f

    .line 3195
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingCheckForLongPress:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3198
    :cond_f
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->recycleVelocityTracker()V

    .line 3200
    const/16 v25, -0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mActivePointerId:I

    goto/16 :goto_1

    .line 3046
    .end local v11    # "handler":Landroid/os/Handler;
    :pswitch_9
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionPosition:I

    move/from16 v17, v0

    .line 3047
    .restart local v17    # "motionPosition":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    move/from16 v25, v0

    sub-int v25, v17, v25

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 3049
    .local v5, "child":Landroid/view/View;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v23

    .line 3050
    .local v23, "x":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    cmpl-float v25, v23, v25

    if-lez v25, :cond_16

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v26, v0

    sub-int v25, v25, v26

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    cmpg-float v25, v23, v25

    if-gez v25, :cond_16

    const/4 v13, 0x1

    .line 3052
    .local v13, "inList":Z
    :goto_5
    if-eqz v5, :cond_1a

    invoke-virtual {v5}, Landroid/view/View;->hasFocusable()Z

    move-result v25

    if-nez v25, :cond_1a

    if-eqz v13, :cond_1a

    .line 3053
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    move/from16 v25, v0

    if-eqz v25, :cond_10

    .line 3054
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Landroid/view/View;->setPressed(Z)V

    .line 3057
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPerformClick:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;

    move-object/from16 v25, v0

    if-nez v25, :cond_11

    .line 3058
    new-instance v25, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$1;)V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPerformClick:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;

    .line 3061
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPerformClick:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;

    move-object/from16 v19, v0

    .line 3062
    .local v19, "performClick":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;
    move/from16 v0, v17

    move-object/from16 v1, v19

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;->mClickMotionPosition:I

    .line 3063
    invoke-virtual/range {v19 .. v19}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;->rememberWindowAttachCount()V

    .line 3065
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mResurrectToPosition:I

    .line 3067
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    move/from16 v25, v0

    if-eqz v25, :cond_12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_19

    .line 3068
    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getHandler()Landroid/os/Handler;

    move-result-object v11

    .line 3069
    .restart local v11    # "handler":Landroid/os/Handler;
    if-eqz v11, :cond_13

    .line 3070
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    move/from16 v25, v0

    if-nez v25, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingCheckForTap:Ljava/lang/Runnable;

    move-object/from16 v25, v0

    :goto_6
    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3073
    :cond_13
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLayoutMode:I

    .line 3075
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataChanged:Z

    move/from16 v25, v0

    if-nez v25, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v25

    if-eqz v25, :cond_18

    .line 3076
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 3077
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionPosition:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setSelectedPositionInt(I)V

    .line 3078
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->layoutChildren()V

    .line 3079
    const/16 v25, 0x1

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Landroid/view/View;->setPressed(Z)V

    .line 3080
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionPosition:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->positionSelector(ILandroid/view/View;)V

    .line 3081
    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setPressed(Z)V

    .line 3082
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelector:Landroid/graphics/drawable/Drawable;

    move-object/from16 v25, v0

    if-eqz v25, :cond_14

    .line 3083
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelector:Landroid/graphics/drawable/Drawable;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v9

    .line 3084
    .local v9, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v9, :cond_14

    instance-of v0, v9, Landroid/graphics/drawable/TransitionDrawable;

    move/from16 v25, v0

    if-eqz v25, :cond_14

    .line 3085
    check-cast v9, Landroid/graphics/drawable/TransitionDrawable;

    .end local v9    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v9}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    .line 3088
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchModeReset:Ljava/lang/Runnable;

    move-object/from16 v25, v0

    if-eqz v25, :cond_15

    .line 3089
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchModeReset:Ljava/lang/Runnable;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 3091
    :cond_15
    new-instance v25, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$1;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$1;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Landroid/view/View;Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;)V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchModeReset:Ljava/lang/Runnable;

    .line 3103
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchModeReset:Ljava/lang/Runnable;

    move-object/from16 v25, v0

    invoke-static {}, Landroid/view/ViewConfiguration;->getPressedStateDuration()I

    move-result v26

    move/from16 v0, v26

    int-to-long v0, v0

    move-wide/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move-wide/from16 v2, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 3109
    :goto_7
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 3050
    .end local v11    # "handler":Landroid/os/Handler;
    .end local v13    # "inList":Z
    .end local v19    # "performClick":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;
    :cond_16
    const/4 v13, 0x0

    goto/16 :goto_5

    .line 3070
    .restart local v11    # "handler":Landroid/os/Handler;
    .restart local v13    # "inList":Z
    .restart local v19    # "performClick":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingCheckForLongPress:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;

    move-object/from16 v25, v0

    goto/16 :goto_6

    .line 3106
    :cond_18
    const/16 v25, -0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 3107
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->updateSelectorState()V

    goto :goto_7

    .line 3110
    .end local v11    # "handler":Landroid/os/Handler;
    :cond_19
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataChanged:Z

    move/from16 v25, v0

    if-nez v25, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v25

    if-eqz v25, :cond_1a

    .line 3111
    invoke-virtual/range {v19 .. v19}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;->run()V

    .line 3114
    .end local v19    # "performClick":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;
    :cond_1a
    const/16 v25, -0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 3115
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->updateSelectorState()V

    goto/16 :goto_4

    .line 3118
    .end local v5    # "child":Landroid/view/View;
    .end local v13    # "inList":Z
    .end local v17    # "motionPosition":I
    .end local v23    # "x":F
    :pswitch_a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v6

    .line 3119
    .local v6, "childCount":I
    if-lez v6, :cond_21

    .line 3120
    const/16 v25, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getLeft()I

    move-result v10

    .line 3121
    .local v10, "firstChildLeft":I
    add-int/lit8 v25, v6, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getRight()I

    move-result v16

    .line 3122
    .local v16, "lastChildRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v7, v0, Landroid/graphics/Rect;->left:I

    .line 3123
    .local v7, "contentLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v26, v0

    sub-int v8, v25, v26

    .line 3124
    .local v8, "contentRight":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    move/from16 v25, v0

    if-nez v25, :cond_1b

    if-lt v10, v7, :cond_1b

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    move/from16 v25, v0

    add-int v25, v25, v6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_1b

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v25

    sub-int v25, v25, v8

    move/from16 v0, v16

    move/from16 v1, v25

    if-gt v0, v1, :cond_1b

    .line 3126
    const/16 v25, -0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 3127
    const/16 v25, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->reportScrollStateChange(I)V

    goto/16 :goto_4

    .line 3129
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v22, v0

    .line 3130
    .local v22, "velocityTracker":Landroid/view/VelocityTracker;
    const/16 v25, 0x3e8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMaximumVelocity:I

    move/from16 v26, v0

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    move-object/from16 v0, v22

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 3132
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mActivePointerId:I

    move/from16 v25, v0

    move-object/from16 v0, v22

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mVelocityScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    move/from16 v0, v25

    float-to-int v15, v0

    .line 3137
    .local v15, "initialVelocity":I
    invoke-static {v15}, Ljava/lang/Math;->abs(I)I

    move-result v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMinimumVelocity:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_1f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    move/from16 v25, v0

    if-nez v25, :cond_1c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mOverscrollDistance:I

    move/from16 v25, v0

    sub-int v25, v7, v25

    move/from16 v0, v25

    if-eq v10, v0, :cond_1f

    :cond_1c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    move/from16 v25, v0

    add-int v25, v25, v6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_1d

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mOverscrollDistance:I

    move/from16 v25, v0

    add-int v25, v25, v8

    move/from16 v0, v16

    move/from16 v1, v25

    if-eq v0, v1, :cond_1f

    .line 3142
    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    move-object/from16 v25, v0

    if-nez v25, :cond_1e

    .line 3143
    new-instance v25, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    .line 3145
    :cond_1e
    const/16 v25, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->reportScrollStateChange(I)V

    .line 3147
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    move-object/from16 v25, v0

    neg-int v0, v15

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->start(I)V

    goto/16 :goto_4

    .line 3149
    :cond_1f
    const/16 v25, -0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 3150
    const/16 v25, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->reportScrollStateChange(I)V

    .line 3151
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    move-object/from16 v25, v0

    if-eqz v25, :cond_20

    .line 3152
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->endFling()V

    .line 3154
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    move-object/from16 v25, v0

    if-eqz v25, :cond_d

    .line 3155
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;->stop()V

    goto/16 :goto_4

    .line 3160
    .end local v7    # "contentLeft":I
    .end local v8    # "contentRight":I
    .end local v10    # "firstChildLeft":I
    .end local v15    # "initialVelocity":I
    .end local v16    # "lastChildRight":I
    .end local v22    # "velocityTracker":Landroid/view/VelocityTracker;
    :cond_21
    const/16 v25, -0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 3161
    const/16 v25, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->reportScrollStateChange(I)V

    goto/16 :goto_4

    .line 3166
    .end local v6    # "childCount":I
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    move-object/from16 v25, v0

    if-nez v25, :cond_22

    .line 3167
    new-instance v25, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    .line 3169
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v22, v0

    .line 3170
    .restart local v22    # "velocityTracker":Landroid/view/VelocityTracker;
    const/16 v25, 0x3e8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMaximumVelocity:I

    move/from16 v26, v0

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    move-object/from16 v0, v22

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 3171
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mActivePointerId:I

    move/from16 v25, v0

    move-object/from16 v0, v22

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v25

    move/from16 v0, v25

    float-to-int v15, v0

    .line 3173
    .restart local v15    # "initialVelocity":I
    const/16 v25, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->reportScrollStateChange(I)V

    .line 3174
    invoke-static {v15}, Ljava/lang/Math;->abs(I)I

    move-result v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMinimumVelocity:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_23

    .line 3175
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    move-object/from16 v25, v0

    neg-int v0, v15

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->startOverfling(I)V

    goto/16 :goto_4

    .line 3177
    :cond_23
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->startSpringback()V

    goto/16 :goto_4

    .line 3205
    .end local v15    # "initialVelocity":I
    .end local v22    # "velocityTracker":Landroid/view/VelocityTracker;
    :pswitch_c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    move/from16 v25, v0

    packed-switch v25, :pswitch_data_4

    .line 3218
    const/16 v25, -0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 3219
    const/16 v25, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setPressed(Z)V

    .line 3220
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionPosition:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    move/from16 v26, v0

    sub-int v25, v25, v26

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v18

    .line 3221
    .local v18, "motionView":Landroid/view/View;
    if-eqz v18, :cond_24

    .line 3222
    const/16 v25, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 3224
    :cond_24
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->clearScrollingCache()V

    .line 3226
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getHandler()Landroid/os/Handler;

    move-result-object v11

    .line 3227
    .restart local v11    # "handler":Landroid/os/Handler;
    if-eqz v11, :cond_25

    .line 3228
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingCheckForLongPress:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3231
    :cond_25
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->recycleVelocityTracker()V

    .line 3234
    .end local v11    # "handler":Landroid/os/Handler;
    .end local v18    # "motionView":Landroid/view/View;
    :goto_8
    :pswitch_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    move-object/from16 v25, v0

    if-eqz v25, :cond_26

    .line 3235
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->onRelease()V

    .line 3236
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowBottom:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;->onRelease()V

    .line 3238
    :cond_26
    const/16 v25, -0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mActivePointerId:I

    goto/16 :goto_1

    .line 3207
    :pswitch_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    move-object/from16 v25, v0

    if-nez v25, :cond_27

    .line 3208
    new-instance v25, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    .line 3210
    :cond_27
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->startSpringback()V

    goto :goto_8

    .line 3243
    :pswitch_f
    invoke-direct/range {p0 .. p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    .line 3244
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionX:I

    move/from16 v23, v0

    .line 3245
    .local v23, "x":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionY:I

    move/from16 v24, v0

    .line 3246
    .restart local v24    # "y":I
    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->pointToPosition(II)I

    move-result v17

    .line 3247
    .restart local v17    # "motionPosition":I
    if-ltz v17, :cond_28

    .line 3249
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    move/from16 v25, v0

    sub-int v25, v17, v25

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v21

    .line 3250
    .restart local v21    # "v":Landroid/view/View;
    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getLeft()I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionViewOriginalLeft:I

    .line 3251
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionPosition:I

    .line 3253
    .end local v21    # "v":Landroid/view/View;
    :cond_28
    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastX:I

    goto/16 :goto_1

    .line 3259
    .end local v17    # "motionPosition":I
    .end local v23    # "x":I
    .end local v24    # "y":I
    :pswitch_10
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v14

    .line 3260
    .local v14, "index":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v12

    .line 3261
    .local v12, "id":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/view/MotionEvent;->getX(I)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v23, v0

    .line 3262
    .restart local v23    # "x":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/view/MotionEvent;->getY(I)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v24, v0

    .line 3263
    .restart local v24    # "y":I
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionCorrection:I

    .line 3264
    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mActivePointerId:I

    .line 3265
    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionX:I

    .line 3266
    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionY:I

    .line 3267
    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->pointToPosition(II)I

    move-result v17

    .line 3268
    .restart local v17    # "motionPosition":I
    if-ltz v17, :cond_29

    .line 3270
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    move/from16 v25, v0

    sub-int v25, v17, v25

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v21

    .line 3271
    .restart local v21    # "v":Landroid/view/View;
    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getLeft()I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionViewOriginalLeft:I

    .line 3272
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionPosition:I

    .line 3274
    .end local v21    # "v":Landroid/view/View;
    :cond_29
    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastX:I

    goto/16 :goto_1

    .line 2944
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_7
        :pswitch_3
        :pswitch_c
        :pswitch_0
        :pswitch_10
        :pswitch_f
    .end packed-switch

    .line 2946
    :pswitch_data_1
    .packed-switch 0x6
        :pswitch_2
    .end packed-switch

    .line 3025
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_6
    .end packed-switch

    .line 3042
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_a
        :pswitch_8
        :pswitch_b
    .end packed-switch

    .line 3205
    :pswitch_data_4
    .packed-switch 0x5
        :pswitch_e
        :pswitch_d
    .end packed-switch
.end method

.method public onTouchModeChanged(Z)V
    .locals 3
    .param p1, "isInTouchMode"    # Z

    .prologue
    .line 2866
    if-eqz p1, :cond_2

    .line 2868
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->hideSelector()V

    .line 2872
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 2875
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->layoutChildren()V

    .line 2877
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->updateSelectorState()V

    .line 2895
    :cond_1
    :goto_0
    return-void

    .line 2879
    :cond_2
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 2880
    .local v0, "touchMode":I
    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 2881
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    if-eqz v1, :cond_4

    .line 2882
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->endFling()V

    .line 2884
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    if-eqz v1, :cond_5

    .line 2885
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;->stop()V

    .line 2888
    :cond_5
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getScrollX()I

    move-result v1

    if-eqz v1, :cond_1

    .line 2889
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mViewHelper:Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelper;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelper;->setScrollX(I)V

    .line 2890
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->finishGlows()V

    .line 2891
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->invalidate()V

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 5
    .param p1, "hasWindowFocus"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2312
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->onWindowFocusChanged(Z)V

    .line 2314
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isInTouchMode()Z

    move-result v3

    if-eqz v3, :cond_3

    move v0, v1

    .line 2316
    .local v0, "touchMode":I
    :goto_0
    if-nez p1, :cond_4

    .line 2317
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setChildrenDrawingCacheEnabled(Z)V

    .line 2318
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    if-eqz v3, :cond_1

    .line 2319
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2322
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->endFling()V

    .line 2323
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    if-eqz v3, :cond_0

    .line 2324
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;->stop()V

    .line 2326
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getScrollX()I

    move-result v3

    if-eqz v3, :cond_1

    .line 2327
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mViewHelper:Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelper;

    invoke-virtual {v3, v1}, Lcom/sec/samsung/gallery/hlistview/util/ViewHelperFactory$ViewHelper;->setScrollX(I)V

    .line 2328
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->finishGlows()V

    .line 2329
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->invalidate()V

    .line 2334
    :cond_1
    if-ne v0, v2, :cond_2

    .line 2336
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedPosition:I

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mResurrectToPosition:I

    .line 2355
    :cond_2
    :goto_1
    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastTouchMode:I

    .line 2356
    return-void

    .end local v0    # "touchMode":I
    :cond_3
    move v0, v2

    .line 2314
    goto :goto_0

    .line 2340
    .restart local v0    # "touchMode":I
    :cond_4
    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastTouchMode:I

    if-eq v0, v3, :cond_2

    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastTouchMode:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    .line 2342
    if-ne v0, v2, :cond_5

    .line 2344
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->resurrectSelection()Z

    goto :goto_1

    .line 2348
    :cond_5
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->hideSelector()V

    .line 2349
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLayoutMode:I

    .line 2350
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->layoutChildren()V

    goto :goto_1
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .locals 6
    .param p1, "action"    # I
    .param p2, "arguments"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/16 v5, 0xc8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1173
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1194
    :goto_0
    return v1

    .line 1176
    :cond_0
    sparse-switch p1, :sswitch_data_0

    move v1, v2

    .line 1194
    goto :goto_0

    .line 1178
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getLastVisiblePosition()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v3, v4, :cond_1

    .line 1179
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int v0, v2, v3

    .line 1180
    .local v0, "viewportWidth":I
    invoke-virtual {p0, v0, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->smoothScrollBy(II)V

    goto :goto_0

    .end local v0    # "viewportWidth":I
    :cond_1
    move v1, v2

    .line 1184
    goto :goto_0

    .line 1186
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    if-lez v3, :cond_2

    .line 1187
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int v0, v2, v3

    .line 1188
    .restart local v0    # "viewportWidth":I
    neg-int v2, v0

    invoke-virtual {p0, v2, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->smoothScrollBy(II)V

    goto :goto_0

    .end local v0    # "viewportWidth":I
    :cond_2
    move v1, v2

    .line 1192
    goto :goto_0

    .line 1176
    nop

    :sswitch_data_0
    .sparse-switch
        0x1000 -> :sswitch_0
        0x2000 -> :sswitch_1
    .end sparse-switch
.end method

.method protected performButtonActionOnTouchDown(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 2907
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 2908
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 2909
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->showContextMenu(FFI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2910
    const/4 v0, 0x1

    .line 2914
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public performItemClick(Landroid/view/View;IJ)Z
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "id"    # J

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 912
    const/4 v8, 0x0

    .line 913
    .local v8, "handled":Z
    const/4 v7, 0x1

    .line 915
    .local v7, "dispatchItemClick":Z
    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    if-eqz v3, :cond_4

    .line 916
    const/4 v8, 0x1

    .line 917
    const/4 v0, 0x0

    .line 919
    .local v0, "checkedStateChanged":Z
    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v3, v4, :cond_9

    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_9

    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceActionMode:Ljava/lang/Object;

    if-eqz v3, :cond_9

    .line 921
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, p2, v2}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v3

    if-nez v3, :cond_6

    move v6, v1

    .line 922
    .local v6, "checked":Z
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p2, v6}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 923
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 924
    if-eqz v6, :cond_7

    .line 925
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2, p2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 930
    :cond_1
    :goto_1
    if-eqz v6, :cond_8

    .line 931
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedItemCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedItemCount:I

    .line 936
    :goto_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceActionMode:Ljava/lang/Object;

    if-eqz v1, :cond_2

    .line 937
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMultiChoiceModeCallback:Ljava/lang/Object;

    check-cast v1, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;

    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceActionMode:Ljava/lang/Object;

    check-cast v2, Landroid/view/ActionMode;

    move v3, p2

    move-wide v4, p3

    invoke-virtual/range {v1 .. v6}, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;->onItemCheckedStateChanged(Landroid/view/ActionMode;IJZ)V

    .line 938
    const/4 v7, 0x0

    .line 941
    :cond_2
    const/4 v0, 0x1

    .line 958
    .end local v6    # "checked":Z
    :cond_3
    :goto_3
    if-eqz v0, :cond_4

    .line 959
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->updateOnScreenCheckedViews()V

    .line 963
    .end local v0    # "checkedStateChanged":Z
    :cond_4
    if-eqz v7, :cond_5

    .line 964
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->performItemClick(Landroid/view/View;IJ)Z

    move-result v1

    or-int/2addr v8, v1

    .line 967
    :cond_5
    return v8

    .restart local v0    # "checkedStateChanged":Z
    :cond_6
    move v6, v2

    .line 921
    goto :goto_0

    .line 927
    .restart local v6    # "checked":Z
    :cond_7
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2, p2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/util/LongSparseArray;->delete(J)V

    goto :goto_1

    .line 933
    :cond_8
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedItemCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedItemCount:I

    goto :goto_2

    .line 942
    .end local v6    # "checked":Z
    :cond_9
    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    if-ne v3, v1, :cond_3

    .line 943
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, p2, v2}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v3

    if-nez v3, :cond_c

    move v6, v1

    .line 944
    .restart local v6    # "checked":Z
    :goto_4
    if-eqz v6, :cond_d

    .line 945
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->clear()V

    .line 946
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, p2, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 947
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 948
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v2}, Landroid/support/v4/util/LongSparseArray;->clear()V

    .line 949
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3, p2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v4, v5, v3}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 951
    :cond_a
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedItemCount:I

    .line 955
    :cond_b
    :goto_5
    const/4 v0, 0x1

    goto :goto_3

    .end local v6    # "checked":Z
    :cond_c
    move v6, v2

    .line 943
    goto :goto_4

    .line 952
    .restart local v6    # "checked":Z
    :cond_d
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->size()I

    move-result v1

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v1

    if-nez v1, :cond_b

    .line 953
    :cond_e
    iput v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedItemCount:I

    goto :goto_5
.end method

.method performLongPress(Landroid/view/View;IJ)Z
    .locals 9
    .param p1, "child"    # Landroid/view/View;
    .param p2, "longPressPosition"    # I
    .param p3, "longPressId"    # J

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2470
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 2471
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 2472
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceActionMode:Ljava/lang/Object;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMultiChoiceModeCallback:Ljava/lang/Object;

    check-cast v0, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceActionMode:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 2474
    invoke-virtual {p0, p2, v6}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setItemChecked(IZ)V

    .line 2475
    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->performHapticFeedback(I)Z

    .line 2493
    :cond_0
    :goto_0
    return v6

    .line 2481
    :cond_1
    const/4 v6, 0x0

    .line 2482
    .local v6, "handled":Z
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mOnItemLongClickListener:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemLongClickListener;

    if-eqz v0, :cond_2

    .line 2483
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mOnItemLongClickListener:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemLongClickListener;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-interface/range {v0 .. v5}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemLongClickListener;->onItemLongClick(Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;Landroid/view/View;IJ)Z

    move-result v6

    .line 2486
    :cond_2
    if-nez v6, :cond_3

    .line 2487
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->createContextMenuInfo(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 2488
    invoke-super {p0, p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->showContextMenuForChild(Landroid/view/View;)Z

    move-result v6

    .line 2490
    :cond_3
    if-eqz v6, :cond_0

    .line 2491
    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->performHapticFeedback(I)Z

    goto :goto_0
.end method

.method public pointToColId(II)J
    .locals 4
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 2614
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->pointToPosition(II)I

    move-result v0

    .line 2615
    .local v0, "position":I
    if-ltz v0, :cond_0

    .line 2616
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    .line 2618
    :goto_0
    return-wide v2

    :cond_0
    const-wide/high16 v2, -0x8000000000000000L

    goto :goto_0
.end method

.method public pointToPosition(II)I
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 2584
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchFrame:Landroid/graphics/Rect;

    .line 2585
    .local v2, "frame":Landroid/graphics/Rect;
    if-nez v2, :cond_0

    .line 2586
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchFrame:Landroid/graphics/Rect;

    .line 2587
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchFrame:Landroid/graphics/Rect;

    .line 2590
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v1

    .line 2591
    .local v1, "count":I
    add-int/lit8 v3, v1, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_2

    .line 2592
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2593
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_1

    .line 2594
    invoke-virtual {v0, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 2595
    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2596
    iget v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    add-int/2addr v4, v3

    .line 2600
    .end local v0    # "child":Landroid/view/View;
    :goto_1
    return v4

    .line 2591
    .restart local v0    # "child":Landroid/view/View;
    :cond_1
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 2600
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    const/4 v4, -0x1

    goto :goto_1
.end method

.method protected positionSelector(ILandroid/view/View;)V
    .locals 7
    .param p1, "position"    # I
    .param p2, "sel"    # Landroid/view/View;

    .prologue
    const/4 v6, -0x1

    .line 1984
    if-eq p1, v6, :cond_0

    .line 1985
    iput p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectorPosition:I

    .line 1988
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectorRect:Landroid/graphics/Rect;

    .line 1989
    .local v1, "selectorRect":Landroid/graphics/Rect;
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 1990
    instance-of v2, p2, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SelectionBoundsAdjuster;

    if-eqz v2, :cond_1

    move-object v2, p2

    .line 1991
    check-cast v2, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SelectionBoundsAdjuster;

    invoke-interface {v2, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SelectionBoundsAdjuster;->adjustListItemSelectionBounds(Landroid/graphics/Rect;)V

    .line 1993
    :cond_1
    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget v3, v1, Landroid/graphics/Rect;->top:I

    iget v4, v1, Landroid/graphics/Rect;->right:I

    iget v5, v1, Landroid/graphics/Rect;->bottom:I

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->positionSelector(IIII)V

    .line 1996
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mIsChildViewEnabled:Z

    .line 1997
    .local v0, "isChildViewEnabled":Z
    invoke-virtual {p2}, Landroid/view/View;->isEnabled()Z

    move-result v2

    if-eq v2, v0, :cond_2

    .line 1998
    if-nez v0, :cond_3

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mIsChildViewEnabled:Z

    .line 1999
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getSelectedItemPosition()I

    move-result v2

    if-eq v2, v6, :cond_2

    .line 2000
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->refreshDrawableState()V

    .line 2003
    :cond_2
    return-void

    .line 1998
    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public reclaimViews(Ljava/util/List;)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 5199
    .local p1, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v1

    .line 5200
    .local v1, "childCount":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    # getter for: Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mRecyclerListener:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;
    invoke-static {v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->access$2200(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;)Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;

    move-result-object v3

    .line 5203
    .local v3, "listener":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_2

    .line 5204
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 5205
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    .line 5207
    .local v4, "lp":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    if-eqz v4, :cond_1

    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    iget v6, v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->viewType:I

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->shouldRecycleViewType(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 5208
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5210
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xe

    if-lt v5, v6, :cond_0

    .line 5211
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 5214
    :cond_0
    if-eqz v3, :cond_1

    .line 5216
    invoke-interface {v3, v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;->onMovedToScrapHeap(Landroid/view/View;)V

    .line 5203
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 5220
    .end local v0    # "child":Landroid/view/View;
    .end local v4    # "lp":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    :cond_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    invoke-virtual {v5, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->reclaimScrapViews(Ljava/util/List;)V

    .line 5221
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->removeAllViewsInLayout()V

    .line 5222
    return-void
.end method

.method protected reconcileSelectedPosition()I
    .locals 2

    .prologue
    .line 4672
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedPosition:I

    .line 4673
    .local v0, "position":I
    if-gez v0, :cond_0

    .line 4674
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mResurrectToPosition:I

    .line 4676
    :cond_0
    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 4677
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 4678
    return v0
.end method

.method reportScrollStateChange(I)V
    .locals 1
    .param p1, "newState"    # I

    .prologue
    .line 3555
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastScrollState:I

    if-eq p1, v0, :cond_0

    .line 3556
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mOnScrollListener:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 3557
    iput p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastScrollState:I

    .line 3558
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mOnScrollListener:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$OnScrollListener;

    invoke-interface {v0, p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$OnScrollListener;->onScrollStateChanged(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;I)V

    .line 3561
    :cond_0
    return-void
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 0
    .param p1, "disallowIntercept"    # Z

    .prologue
    .line 3416
    if-eqz p1, :cond_0

    .line 3417
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->recycleVelocityTracker()V

    .line 3419
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->requestDisallowInterceptTouchEvent(Z)V

    .line 3420
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 1522
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mBlockLayoutRequests:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mInLayout:Z

    if-nez v0, :cond_0

    .line 1523
    invoke-super {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->requestLayout()V

    .line 1525
    :cond_0
    return-void
.end method

.method requestLayoutIfNecessary()V
    .locals 1

    .prologue
    .line 1272
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 1273
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->resetList()V

    .line 1274
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->requestLayout()V

    .line 1275
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->invalidate()V

    .line 1277
    :cond_0
    return-void
.end method

.method protected resetList()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 1531
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->removeAllViewsInLayout()V

    .line 1532
    iput v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    .line 1533
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataChanged:Z

    .line 1534
    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScrollAfterLayout:Ljava/lang/Runnable;

    .line 1535
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mNeedSync:Z

    .line 1536
    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingSync:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$SavedState;

    .line 1537
    iput v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mOldSelectedPosition:I

    .line 1538
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mOldSelectedColId:J

    .line 1539
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setSelectedPositionInt(I)V

    .line 1540
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setNextSelectedPositionInt(I)V

    .line 1541
    iput v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedLeft:I

    .line 1542
    iput v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectorPosition:I

    .line 1543
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 1544
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->invalidate()V

    .line 1545
    return-void
.end method

.method resurrectSelection()Z
    .locals 19

    .prologue
    .line 4742
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v2

    .line 4744
    .local v2, "childCount":I
    if-gtz v2, :cond_0

    .line 4745
    const/16 v17, 0x0

    .line 4839
    :goto_0
    return v17

    .line 4748
    :cond_0
    const/4 v12, 0x0

    .line 4750
    .local v12, "selectedLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 4751
    .local v3, "childrenLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getRight()I

    move-result v17

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getLeft()I

    move-result v18

    sub-int v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v18, v0

    sub-int v4, v17, v18

    .line 4752
    .local v4, "childrenRight":I
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    .line 4753
    .local v6, "firstPosition":I
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mResurrectToPosition:I

    .line 4754
    .local v15, "toPosition":I
    const/4 v5, 0x1

    .line 4756
    .local v5, "down":Z
    if-lt v15, v6, :cond_4

    add-int v17, v6, v2

    move/from16 v0, v17

    if-ge v15, v0, :cond_4

    .line 4757
    move v13, v15

    .line 4759
    .local v13, "selectedPos":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    move/from16 v17, v0

    sub-int v17, v13, v17

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    .line 4760
    .local v11, "selected":Landroid/view/View;
    invoke-virtual {v11}, Landroid/view/View;->getLeft()I

    move-result v12

    .line 4761
    invoke-virtual {v11}, Landroid/view/View;->getRight()I

    move-result v14

    .line 4764
    .local v14, "selectedRight":I
    if-ge v12, v3, :cond_3

    .line 4765
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getHorizontalFadingEdgeLength()I

    move-result v17

    add-int v12, v3, v17

    .line 4820
    .end local v11    # "selected":Landroid/view/View;
    .end local v14    # "selectedRight":I
    :cond_1
    :goto_1
    const/16 v17, -0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mResurrectToPosition:I

    .line 4821
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 4822
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    move-object/from16 v17, v0

    if-eqz v17, :cond_2

    .line 4823
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;->stop()V

    .line 4825
    :cond_2
    const/16 v17, -0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 4826
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->clearScrollingCache()V

    .line 4827
    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSpecificLeft:I

    .line 4828
    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->lookForSelectablePosition(IZ)I

    move-result v13

    .line 4829
    if-lt v13, v6, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getLastVisiblePosition()I

    move-result v17

    move/from16 v0, v17

    if-gt v13, v0, :cond_c

    .line 4830
    const/16 v17, 0x4

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLayoutMode:I

    .line 4831
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->updateSelectorState()V

    .line 4832
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setSelectionInt(I)V

    .line 4833
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->invokeOnItemScrollListener()V

    .line 4837
    :goto_2
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->reportScrollStateChange(I)V

    .line 4839
    if-ltz v13, :cond_d

    const/16 v17, 0x1

    goto/16 :goto_0

    .line 4766
    .restart local v11    # "selected":Landroid/view/View;
    .restart local v14    # "selectedRight":I
    :cond_3
    if-le v14, v4, :cond_1

    .line 4767
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v17

    sub-int v17, v4, v17

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getHorizontalFadingEdgeLength()I

    move-result v18

    sub-int v12, v17, v18

    goto :goto_1

    .line 4770
    .end local v11    # "selected":Landroid/view/View;
    .end local v13    # "selectedPos":I
    .end local v14    # "selectedRight":I
    :cond_4
    if-ge v15, v6, :cond_8

    .line 4772
    move v13, v6

    .line 4773
    .restart local v13    # "selectedPos":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_3
    if-ge v7, v2, :cond_1

    .line 4774
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v16

    .line 4775
    .local v16, "v":Landroid/view/View;
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getLeft()I

    move-result v9

    .line 4777
    .local v9, "left":I
    if-nez v7, :cond_6

    .line 4779
    move v12, v9

    .line 4781
    if-gtz v6, :cond_5

    if-ge v9, v3, :cond_6

    .line 4784
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getHorizontalFadingEdgeLength()I

    move-result v17

    add-int v3, v3, v17

    .line 4787
    :cond_6
    if-lt v9, v3, :cond_7

    .line 4789
    add-int v13, v6, v7

    .line 4790
    move v12, v9

    .line 4791
    goto/16 :goto_1

    .line 4773
    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 4795
    .end local v7    # "i":I
    .end local v9    # "left":I
    .end local v13    # "selectedPos":I
    .end local v16    # "v":Landroid/view/View;
    :cond_8
    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    .line 4796
    .local v8, "itemCount":I
    const/4 v5, 0x0

    .line 4797
    add-int v17, v6, v2

    add-int/lit8 v13, v17, -0x1

    .line 4799
    .restart local v13    # "selectedPos":I
    add-int/lit8 v7, v2, -0x1

    .restart local v7    # "i":I
    :goto_4
    if-ltz v7, :cond_1

    .line 4800
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v16

    .line 4801
    .restart local v16    # "v":Landroid/view/View;
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getLeft()I

    move-result v9

    .line 4802
    .restart local v9    # "left":I
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getRight()I

    move-result v10

    .line 4804
    .local v10, "right":I
    add-int/lit8 v17, v2, -0x1

    move/from16 v0, v17

    if-ne v7, v0, :cond_a

    .line 4805
    move v12, v9

    .line 4806
    add-int v17, v6, v2

    move/from16 v0, v17

    if-lt v0, v8, :cond_9

    if-le v10, v4, :cond_a

    .line 4807
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getHorizontalFadingEdgeLength()I

    move-result v17

    sub-int v4, v4, v17

    .line 4811
    :cond_a
    if-gt v10, v4, :cond_b

    .line 4812
    add-int v13, v6, v7

    .line 4813
    move v12, v9

    .line 4814
    goto/16 :goto_1

    .line 4799
    :cond_b
    add-int/lit8 v7, v7, -0x1

    goto :goto_4

    .line 4835
    .end local v7    # "i":I
    .end local v8    # "itemCount":I
    .end local v9    # "left":I
    .end local v10    # "right":I
    .end local v16    # "v":Landroid/view/View;
    :cond_c
    const/4 v13, -0x1

    goto :goto_2

    .line 4839
    :cond_d
    const/16 v17, 0x0

    goto/16 :goto_0
.end method

.method protected resurrectSelectionIfNeeded()Z
    .locals 1

    .prologue
    .line 4721
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedPosition:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->resurrectSelection()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4722
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->updateSelectorState()V

    .line 4723
    const/4 v0, 0x1

    .line 4725
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public sendAccessibilityEvent(I)V
    .locals 3
    .param p1, "eventType"    # I

    .prologue
    .line 1134
    const/16 v2, 0x1000

    if-ne p1, v2, :cond_1

    .line 1135
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getFirstVisiblePosition()I

    move-result v0

    .line 1136
    .local v0, "firstVisiblePosition":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getLastVisiblePosition()I

    move-result v1

    .line 1137
    .local v1, "lastVisiblePosition":I
    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastAccessibilityScrollEventFromIndex:I

    if-ne v2, v0, :cond_0

    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastAccessibilityScrollEventToIndex:I

    if-ne v2, v1, :cond_0

    .line 1146
    .end local v0    # "firstVisiblePosition":I
    .end local v1    # "lastVisiblePosition":I
    :goto_0
    return-void

    .line 1141
    .restart local v0    # "firstVisiblePosition":I
    .restart local v1    # "lastVisiblePosition":I
    :cond_0
    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastAccessibilityScrollEventFromIndex:I

    .line 1142
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastAccessibilityScrollEventToIndex:I

    .line 1145
    .end local v0    # "firstVisiblePosition":I
    .end local v1    # "lastVisiblePosition":I
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->sendAccessibilityEvent(I)V

    goto :goto_0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .param p1, "x0"    # Landroid/widget/Adapter;

    .prologue
    .line 75
    check-cast p1, Landroid/widget/ListAdapter;

    .end local p1    # "x0":Landroid/widget/Adapter;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 714
    if-eqz p1, :cond_0

    .line 715
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapterHasStableIds:Z

    .line 716
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapterHasStableIds:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-nez v0, :cond_0

    .line 718
    new-instance v0, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v0}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    .line 722
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_1

    .line 723
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    .line 726
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v0, :cond_2

    .line 727
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/support/v4/util/LongSparseArray;->clear()V

    .line 729
    :cond_2
    return-void
.end method

.method public setCacheColorHint(I)V
    .locals 3
    .param p1, "color"    # I

    .prologue
    .line 5169
    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCacheColorHint:I

    if-eq p1, v2, :cond_1

    .line 5170
    iput p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCacheColorHint:I

    .line 5171
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v0

    .line 5172
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 5173
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    .line 5172
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5175
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->setCacheColorHint(I)V

    .line 5177
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_1
    return-void
.end method

.method public setChoiceMode(I)V
    .locals 2
    .param p1, "choiceMode"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/16 v1, 0xb

    .line 1009
    iput p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    .line 1011
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v1, :cond_1

    .line 1012
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceActionMode:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 1014
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v1, :cond_0

    .line 1015
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceActionMode:Ljava/lang/Object;

    check-cast v0, Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 1017
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceActionMode:Ljava/lang/Object;

    .line 1021
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    if-eqz v0, :cond_4

    .line 1022
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    if-nez v0, :cond_2

    .line 1023
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    .line 1025
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1026
    new-instance v0, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v0}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    .line 1029
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v1, :cond_4

    .line 1030
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 1031
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->clearChoices()V

    .line 1032
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setLongClickable(Z)V

    .line 1036
    :cond_4
    return-void
.end method

.method public setDrawSelectorOnTop(Z)V
    .locals 0
    .param p1, "onTop"    # Z

    .prologue
    .line 2101
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDrawSelectorOnTop:Z

    .line 2102
    return-void
.end method

.method public setFriction(F)V
    .locals 1
    .param p1, "friction"    # F

    .prologue
    .line 4253
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    if-nez v0, :cond_0

    .line 4254
    new-instance v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    .line 4256
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    # getter for: Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->mScroller:Lcom/sec/samsung/gallery/hlistview/widget/OverScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->access$1000(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;)Lcom/sec/samsung/gallery/hlistview/widget/OverScroller;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/OverScroller;->setFriction(F)V

    .line 4257
    return-void
.end method

.method public setItemChecked(IZ)V
    .locals 10
    .param p1, "position"    # I
    .param p2, "value"    # Z

    .prologue
    const/16 v9, 0xb

    const/4 v6, 0x3

    const/4 v2, 0x0

    const/4 v8, 0x1

    .line 840
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    if-nez v1, :cond_1

    .line 908
    :cond_0
    :goto_0
    return-void

    .line 845
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v9, :cond_4

    .line 846
    if-eqz p2, :cond_4

    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    if-ne v1, v6, :cond_4

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceActionMode:Ljava/lang/Object;

    if-nez v1, :cond_4

    .line 847
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMultiChoiceModeCallback:Ljava/lang/Object;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMultiChoiceModeCallback:Ljava/lang/Object;

    check-cast v1, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;->hasWrappedCallback()Z

    move-result v1

    if-nez v1, :cond_3

    .line 849
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "AbsListView: attempted to start selection mode for CHOICE_MODE_MULTIPLE_MODAL but no choice mode callback was supplied. Call setMultiChoiceModeListener to set a callback."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 853
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMultiChoiceModeCallback:Ljava/lang/Object;

    check-cast v1, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceActionMode:Ljava/lang/Object;

    .line 857
    :cond_4
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    const/4 v3, 0x2

    if-eq v1, v3, :cond_5

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v9, :cond_b

    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceMode:I

    if-ne v1, v6, :cond_b

    .line 859
    :cond_5
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    .line 860
    .local v0, "oldValue":Z
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p1, p2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 861
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 862
    if-eqz p2, :cond_9

    .line 863
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v2, v3, v6}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 868
    :cond_6
    :goto_1
    if-eq v0, p2, :cond_7

    .line 869
    if-eqz p2, :cond_a

    .line 870
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedItemCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedItemCount:I

    .line 875
    :cond_7
    :goto_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceActionMode:Ljava/lang/Object;

    if-eqz v1, :cond_8

    .line 876
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    .line 877
    .local v4, "id":J
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMultiChoiceModeCallback:Ljava/lang/Object;

    check-cast v1, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;

    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mChoiceActionMode:Ljava/lang/Object;

    check-cast v2, Landroid/view/ActionMode;

    move v3, p1

    move v6, p2

    invoke-virtual/range {v1 .. v6}, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;->onItemCheckedStateChanged(Landroid/view/ActionMode;IJZ)V

    .line 903
    .end local v0    # "oldValue":Z
    .end local v4    # "id":J
    :cond_8
    :goto_3
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mInLayout:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mBlockLayoutRequests:Z

    if-nez v1, :cond_0

    .line 904
    iput-boolean v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataChanged:Z

    .line 905
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->rememberSyncState()V

    .line 906
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->requestLayout()V

    goto/16 :goto_0

    .line 865
    .restart local v0    # "oldValue":Z
    :cond_9
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/util/LongSparseArray;->delete(J)V

    goto :goto_1

    .line 872
    :cond_a
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedItemCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedItemCount:I

    goto :goto_2

    .line 880
    .end local v0    # "oldValue":Z
    :cond_b
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v1, :cond_f

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v1

    if-eqz v1, :cond_f

    move v7, v8

    .line 883
    .local v7, "updateIds":Z
    :goto_4
    if-nez p2, :cond_c

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isItemChecked(I)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 884
    :cond_c
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->clear()V

    .line 885
    if-eqz v7, :cond_d

    .line 886
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v1}, Landroid/support/v4/util/LongSparseArray;->clear()V

    .line 891
    :cond_d
    if-eqz p2, :cond_10

    .line 892
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p1, v8}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 893
    if-eqz v7, :cond_e

    .line 894
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v2, v3, v6}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 896
    :cond_e
    iput v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedItemCount:I

    goto :goto_3

    .end local v7    # "updateIds":Z
    :cond_f
    move v7, v2

    .line 880
    goto :goto_4

    .line 897
    .restart local v7    # "updateIds":Z
    :cond_10
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->size()I

    move-result v1

    if-eqz v1, :cond_11

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v1

    if-nez v1, :cond_8

    .line 898
    :cond_11
    iput v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCheckedItemCount:I

    goto :goto_3
.end method

.method public setMultiChoiceModeListener(Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeListener;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 1049
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 1050
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMultiChoiceModeCallback:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 1051
    new-instance v0, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMultiChoiceModeCallback:Ljava/lang/Object;

    .line 1053
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMultiChoiceModeCallback:Ljava/lang/Object;

    check-cast v0, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeWrapper;->setWrapped(Lcom/sec/samsung/gallery/hlistview/util/MultiChoiceModeListener;)V

    .line 1057
    :goto_0
    return-void

    .line 1055
    :cond_1
    const-string v0, "AbsListView"

    const-string v1, "setMultiChoiceModeListener not supported for this version of Android"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setOnScrollListener(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$OnScrollListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$OnScrollListener;

    .prologue
    .line 1115
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mOnScrollListener:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$OnScrollListener;

    .line 1116
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->invokeOnItemScrollListener()V

    .line 1117
    return-void
.end method

.method public setOverScrollEffectPadding(II)V
    .locals 0
    .param p1, "topPadding"    # I
    .param p2, "bottomPadding"    # I

    .prologue
    .line 3389
    iput p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mGlowPaddingTop:I

    .line 3390
    iput p2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mGlowPaddingBottom:I

    .line 3391
    return-void
.end method

.method public setOverScrollMode(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 696
    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    .line 697
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    if-nez v1, :cond_0

    .line 698
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 699
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    invoke-direct {v1, v0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    .line 700
    new-instance v1, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    invoke-direct {v1, v0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowBottom:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    .line 706
    .end local v0    # "context":Landroid/content/Context;
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->setOverScrollMode(I)V

    .line 707
    return-void

    .line 703
    :cond_1
    iput-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowTop:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    .line 704
    iput-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mEdgeGlowBottom:Lcom/sec/samsung/gallery/hlistview/widget/EdgeEffect;

    goto :goto_0
.end method

.method public setRecyclerListener(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;

    .prologue
    .line 5247
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    # setter for: Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mRecyclerListener:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;
    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->access$2202(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;)Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;

    .line 5248
    return-void
.end method

.method public setScrollIndicators(Landroid/view/View;Landroid/view/View;)V
    .locals 0
    .param p1, "left"    # Landroid/view/View;
    .param p2, "right"    # Landroid/view/View;

    .prologue
    .line 2183
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mScrollLeft:Landroid/view/View;

    .line 2184
    iput-object p2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mScrollRight:Landroid/view/View;

    .line 2185
    return-void
.end method

.method public setScrollingCacheEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 1225
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mScrollingCacheEnabled:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 1226
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->clearScrollingCache()V

    .line 1228
    :cond_0
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mScrollingCacheEnabled:Z

    .line 1229
    return-void
.end method

.method public abstract setSelectionInt(I)V
.end method

.method public setSelector(I)V
    .locals 1
    .param p1, "resID"    # I

    .prologue
    .line 2113
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 2114
    return-void
.end method

.method public setSelector(Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p1, "sel"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 2117
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 2118
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelector:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 2119
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2121
    :cond_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelector:Landroid/graphics/drawable/Drawable;

    .line 2122
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 2123
    .local v0, "padding":Landroid/graphics/Rect;
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 2124
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectionLeftPadding:I

    .line 2125
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectionTopPadding:I

    .line 2126
    iget v1, v0, Landroid/graphics/Rect;->right:I

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectionRightPadding:I

    .line 2127
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectionBottomPadding:I

    .line 2128
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 2129
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->updateSelectorState()V

    .line 2130
    return-void
.end method

.method public setSmoothScrollbarEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 1093
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSmoothScrollbarEnabled:Z

    .line 1094
    return-void
.end method

.method public setStackFromRight(Z)V
    .locals 1
    .param p1, "stackFromRight"    # Z

    .prologue
    .line 1265
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mStackFromRight:Z

    if-eq v0, p1, :cond_0

    .line 1266
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mStackFromRight:Z

    .line 1267
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->requestLayoutIfNecessary()V

    .line 1269
    :cond_0
    return-void
.end method

.method public setTranscriptMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 5141
    iput p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTranscriptMode:I

    .line 5142
    return-void
.end method

.method public setVelocityScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 4266
    iput p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mVelocityScale:F

    .line 4267
    return-void
.end method

.method protected setVisibleRangeHint(II)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 5234
    return-void
.end method

.method protected shouldShowSelector()Z
    .locals 1

    .prologue
    .line 2081
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->touchModeDrawsInPressedState()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showContextMenu(FFI)Z
    .locals 6
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "metaState"    # I

    .prologue
    .line 2502
    float-to-int v4, p1

    float-to-int v5, p2

    invoke-virtual {p0, v4, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->pointToPosition(II)I

    move-result v1

    .line 2503
    .local v1, "position":I
    const/4 v4, -0x1

    if-eq v1, v4, :cond_0

    .line 2504
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v4, v1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    .line 2505
    .local v2, "id":J
    iget v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    sub-int v4, v1, v4

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2506
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 2507
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->createContextMenuInfo(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 2508
    invoke-super {p0, p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->showContextMenuForChild(Landroid/view/View;)Z

    move-result v4

    .line 2511
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "id":J
    :goto_0
    return v4

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->showContextMenu(FFI)Z

    move-result v4

    goto :goto_0
.end method

.method public showContextMenuForChild(Landroid/view/View;)Z
    .locals 7
    .param p1, "originalView"    # Landroid/view/View;

    .prologue
    .line 2516
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getPositionForView(Landroid/view/View;)I

    move-result v3

    .line 2517
    .local v3, "longPressPosition":I
    if-ltz v3, :cond_2

    .line 2518
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    .line 2519
    .local v4, "longPressId":J
    const/4 v6, 0x0

    .line 2521
    .local v6, "handled":Z
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mOnItemLongClickListener:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemLongClickListener;

    if-eqz v0, :cond_0

    .line 2522
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mOnItemLongClickListener:Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemLongClickListener;

    move-object v1, p0

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$OnItemLongClickListener;->onItemLongClick(Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;Landroid/view/View;IJ)Z

    move-result v6

    .line 2525
    :cond_0
    if-nez v6, :cond_1

    .line 2526
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    sub-int v0, v3, v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0, v3, v4, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->createContextMenuInfo(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 2529
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->showContextMenuForChild(Landroid/view/View;)Z

    move-result v6

    .line 2534
    .end local v4    # "longPressId":J
    .end local v6    # "handled":Z
    :cond_1
    :goto_0
    return v6

    :cond_2
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public smoothScrollBy(II)V
    .locals 1
    .param p1, "distance"    # I
    .param p2, "duration"    # I

    .prologue
    .line 4344
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->smoothScrollBy(IIZ)V

    .line 4345
    return-void
.end method

.method public smoothScrollBy(IIZ)V
    .locals 7
    .param p1, "distance"    # I
    .param p2, "duration"    # I
    .param p3, "linear"    # Z

    .prologue
    .line 4348
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    if-nez v5, :cond_0

    .line 4349
    new-instance v5, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    invoke-direct {v5, p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V

    iput-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    .line 4353
    :cond_0
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    .line 4354
    .local v1, "firstPos":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v0

    .line 4355
    .local v0, "childCount":I
    add-int v2, v1, v0

    .line 4356
    .local v2, "lastPos":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getPaddingLeft()I

    move-result v3

    .line 4357
    .local v3, "leftLimit":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getPaddingRight()I

    move-result v6

    sub-int v4, v5, v6

    .line 4359
    .local v4, "rightLimit":I
    if-eqz p1, :cond_2

    iget v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    if-eqz v5, :cond_2

    if-eqz v0, :cond_2

    if-nez v1, :cond_1

    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v5

    if-ne v5, v3, :cond_1

    if-ltz p1, :cond_2

    :cond_1
    iget v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    if-ne v2, v5, :cond_4

    add-int/lit8 v5, v0, -0x1

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v5

    if-ne v5, v4, :cond_4

    if-lez p1, :cond_4

    .line 4363
    :cond_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->endFling()V

    .line 4364
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    if-eqz v5, :cond_3

    .line 4365
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;->stop()V

    .line 4371
    :cond_3
    :goto_0
    return-void

    .line 4368
    :cond_4
    const/4 v5, 0x2

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->reportScrollStateChange(I)V

    .line 4369
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFlingRunnable:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;

    invoke-virtual {v5, p1, p2, p3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$FlingRunnable;->startScroll(IIZ)V

    goto :goto_0
.end method

.method protected smoothScrollByOffset(I)V
    .locals 10
    .param p1, "position"    # I

    .prologue
    const/high16 v9, 0x3f400000    # 0.75f

    .line 4377
    const/4 v2, -0x1

    .line 4378
    .local v2, "index":I
    if-gez p1, :cond_3

    .line 4379
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getFirstVisiblePosition()I

    move-result v2

    .line 4384
    :cond_0
    :goto_0
    const/4 v7, -0x1

    if-le v2, v7, :cond_2

    .line 4385
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getFirstVisiblePosition()I

    move-result v7

    sub-int v7, v2, v7

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 4386
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_2

    .line 4387
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 4388
    .local v4, "visibleRect":Landroid/graphics/Rect;
    invoke-virtual {v0, v4}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 4390
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v7

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v8

    mul-int v1, v7, v8

    .line 4391
    .local v1, "childRectArea":I
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v7

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v8

    mul-int v5, v7, v8

    .line 4392
    .local v5, "visibleRectArea":I
    int-to-float v7, v5

    int-to-float v8, v1

    div-float v3, v7, v8

    .line 4393
    .local v3, "visibleArea":F
    const/high16 v6, 0x3f400000    # 0.75f

    .line 4394
    .local v6, "visibleThreshold":F
    if-gez p1, :cond_4

    cmpg-float v7, v3, v9

    if-gez v7, :cond_4

    .line 4397
    add-int/lit8 v2, v2, 0x1

    .line 4404
    .end local v1    # "childRectArea":I
    .end local v3    # "visibleArea":F
    .end local v5    # "visibleRectArea":I
    .end local v6    # "visibleThreshold":F
    :cond_1
    :goto_1
    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getCount()I

    move-result v8

    add-int v9, v2, p1

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->smoothScrollToPosition(I)V

    .line 4407
    .end local v0    # "child":Landroid/view/View;
    .end local v4    # "visibleRect":Landroid/graphics/Rect;
    :cond_2
    return-void

    .line 4380
    :cond_3
    if-lez p1, :cond_0

    .line 4381
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getLastVisiblePosition()I

    move-result v2

    goto :goto_0

    .line 4398
    .restart local v0    # "child":Landroid/view/View;
    .restart local v1    # "childRectArea":I
    .restart local v3    # "visibleArea":F
    .restart local v4    # "visibleRect":Landroid/graphics/Rect;
    .restart local v5    # "visibleRectArea":I
    .restart local v6    # "visibleThreshold":F
    :cond_4
    if-lez p1, :cond_1

    cmpg-float v7, v3, v9

    if-gez v7, :cond_1

    .line 4401
    add-int/lit8 v2, v2, -0x1

    goto :goto_1
.end method

.method public smoothScrollToPosition(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 4276
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    if-nez v0, :cond_0

    .line 4277
    new-instance v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    .line 4279
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;->start(I)V

    .line 4280
    return-void
.end method

.method public smoothScrollToPosition(II)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "boundPosition"    # I

    .prologue
    .line 4329
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    if-nez v0, :cond_0

    .line 4330
    new-instance v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    .line 4332
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;->start(II)V

    .line 4333
    return-void
.end method

.method public smoothScrollToPositionFromLeft(II)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "offset"    # I

    .prologue
    .line 4313
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    if-nez v0, :cond_0

    .line 4314
    new-instance v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    .line 4316
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;->startWithOffset(II)V

    .line 4317
    return-void
.end method

.method public smoothScrollToPositionFromLeft(III)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "offset"    # I
    .param p3, "duration"    # I

    .prologue
    .line 4296
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    if-nez v0, :cond_0

    .line 4297
    new-instance v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    .line 4299
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;->startWithOffset(III)V

    .line 4300
    return-void
.end method

.method touchModeDrawsInPressedState()Z
    .locals 1

    .prologue
    .line 2065
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    packed-switch v0, :pswitch_data_0

    .line 2070
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2068
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 2065
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method trackMotionScroll(II)Z
    .locals 31
    .param p1, "deltaX"    # I
    .param p2, "incrementalDeltaX"    # I

    .prologue
    .line 4452
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v8

    .line 4453
    .local v8, "childCount":I
    if-nez v8, :cond_0

    .line 4454
    const/16 v29, 0x1

    .line 4609
    :goto_0
    return v29

    .line 4457
    :cond_0
    const/16 v29, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Landroid/view/View;->getLeft()I

    move-result v15

    .line 4458
    .local v15, "firstLeft":I
    add-int/lit8 v29, v8, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Landroid/view/View;->getRight()I

    move-result v21

    .line 4460
    .local v21, "lastRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    .line 4465
    .local v22, "listPadding":Landroid/graphics/Rect;
    const/4 v12, 0x0

    .line 4466
    .local v12, "effectivePaddingLeft":I
    const/4 v13, 0x0

    .line 4474
    .local v13, "effectivePaddingRight":I
    sub-int v25, v12, v15

    .line 4475
    .local v25, "spaceBefore":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v29

    sub-int v14, v29, v13

    .line 4476
    .local v14, "end":I
    sub-int v24, v21, v14

    .line 4478
    .local v24, "spaceAfter":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v29

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getPaddingRight()I

    move-result v30

    sub-int v29, v29, v30

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getPaddingLeft()I

    move-result v30

    sub-int v28, v29, v30

    .line 4479
    .local v28, "width":I
    if-gez p1, :cond_2

    .line 4480
    add-int/lit8 v29, v28, -0x1

    move/from16 v0, v29

    neg-int v0, v0

    move/from16 v29, v0

    move/from16 v0, v29

    move/from16 v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 4485
    :goto_1
    if-gez p2, :cond_3

    .line 4486
    add-int/lit8 v29, v28, -0x1

    move/from16 v0, v29

    neg-int v0, v0

    move/from16 v29, v0

    move/from16 v0, v29

    move/from16 v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result p2

    .line 4491
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    move/from16 v16, v0

    .line 4494
    .local v16, "firstPosition":I
    if-nez v16, :cond_4

    .line 4495
    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v29, v0

    sub-int v29, v15, v29

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPositionDistanceGuess:I

    .line 4499
    :goto_3
    add-int v29, v16, v8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    move/from16 v30, v0

    move/from16 v0, v29

    move/from16 v1, v30

    if-ne v0, v1, :cond_5

    .line 4500
    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v29, v0

    add-int v29, v29, v21

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastPositionDistanceGuess:I

    .line 4505
    :goto_4
    if-nez v16, :cond_6

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v29, v0

    move/from16 v0, v29

    if-lt v15, v0, :cond_6

    if-ltz p2, :cond_6

    const/4 v6, 0x1

    .line 4506
    .local v6, "cannotScrollRight":Z
    :goto_5
    add-int v29, v16, v8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    move/from16 v30, v0

    move/from16 v0, v29

    move/from16 v1, v30

    if-ne v0, v1, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v29

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v30, v0

    sub-int v29, v29, v30

    move/from16 v0, v21

    move/from16 v1, v29

    if-gt v0, v1, :cond_7

    if-gtz p2, :cond_7

    const/4 v5, 0x1

    .line 4508
    .local v5, "cannotScrollLeft":Z
    :goto_6
    if-nez v6, :cond_1

    if-eqz v5, :cond_9

    .line 4509
    :cond_1
    if-eqz p2, :cond_8

    const/16 v29, 0x1

    goto/16 :goto_0

    .line 4482
    .end local v5    # "cannotScrollLeft":Z
    .end local v6    # "cannotScrollRight":Z
    .end local v16    # "firstPosition":I
    :cond_2
    add-int/lit8 v29, v28, -0x1

    move/from16 v0, v29

    move/from16 v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    goto/16 :goto_1

    .line 4488
    :cond_3
    add-int/lit8 v29, v28, -0x1

    move/from16 v0, v29

    move/from16 v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result p2

    goto/16 :goto_2

    .line 4497
    .restart local v16    # "firstPosition":I
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPositionDistanceGuess:I

    move/from16 v29, v0

    add-int v29, v29, p2

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPositionDistanceGuess:I

    goto :goto_3

    .line 4502
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastPositionDistanceGuess:I

    move/from16 v29, v0

    add-int v29, v29, p2

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLastPositionDistanceGuess:I

    goto :goto_4

    .line 4505
    :cond_6
    const/4 v6, 0x0

    goto :goto_5

    .line 4506
    .restart local v6    # "cannotScrollRight":Z
    :cond_7
    const/4 v5, 0x0

    goto :goto_6

    .line 4509
    .restart local v5    # "cannotScrollLeft":Z
    :cond_8
    const/16 v29, 0x0

    goto/16 :goto_0

    .line 4512
    :cond_9
    if-gez p2, :cond_12

    const/4 v11, 0x1

    .line 4514
    .local v11, "down":Z
    :goto_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isInTouchMode()Z

    move-result v20

    .line 4515
    .local v20, "inTouchMode":Z
    if-eqz v20, :cond_a

    .line 4516
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->hideSelector()V

    .line 4519
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getHeaderViewsCount()I

    move-result v18

    .line 4520
    .local v18, "headerViewsCount":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    move/from16 v29, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getFooterViewsCount()I

    move-result v30

    sub-int v17, v29, v30

    .line 4522
    .local v17, "footerViewsStart":I
    const/16 v26, 0x0

    .line 4523
    .local v26, "start":I
    const/4 v10, 0x0

    .line 4525
    .local v10, "count":I
    if-eqz v11, :cond_15

    .line 4526
    move/from16 v0, p2

    neg-int v0, v0

    move/from16 v27, v0

    .line 4532
    .local v27, "top":I
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_8
    move/from16 v0, v19

    if-ge v0, v8, :cond_b

    .line 4533
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 4534
    .local v7, "child":Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getRight()I

    move-result v29

    move/from16 v0, v29

    move/from16 v1, v27

    if-lt v0, v1, :cond_13

    .line 4565
    .end local v7    # "child":Landroid/view/View;
    .end local v27    # "top":I
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionViewOriginalLeft:I

    move/from16 v29, v0

    add-int v29, v29, p1

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionViewNewLeft:I

    .line 4567
    const/16 v29, 0x1

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mBlockLayoutRequests:Z

    .line 4569
    if-lez v10, :cond_c

    .line 4570
    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1, v10}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->detachViewsFromParent(II)V

    .line 4571
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->removeSkippedScrap()V

    .line 4576
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->awakenScrollBars()Z

    move-result v29

    if-nez v29, :cond_d

    .line 4577
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->invalidate()V

    .line 4580
    :cond_d
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->offsetChildrenLeftAndRight(I)V

    .line 4582
    if-eqz v11, :cond_e

    .line 4583
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    move/from16 v29, v0

    add-int v29, v29, v10

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    .line 4586
    :cond_e
    invoke-static/range {p2 .. p2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 4587
    .local v3, "absIncrementalDeltaX":I
    move/from16 v0, v25

    if-lt v0, v3, :cond_f

    move/from16 v0, v24

    if-ge v0, v3, :cond_10

    .line 4588
    :cond_f
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->fillGap(Z)V

    .line 4591
    :cond_10
    if-nez v20, :cond_17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedPosition:I

    move/from16 v29, v0

    const/16 v30, -0x1

    move/from16 v0, v29

    move/from16 v1, v30

    if-eq v0, v1, :cond_17

    .line 4592
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedPosition:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    move/from16 v30, v0

    sub-int v9, v29, v30

    .line 4593
    .local v9, "childIndex":I
    if-ltz v9, :cond_11

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v29

    move/from16 v0, v29

    if-ge v9, v0, :cond_11

    .line 4594
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedPosition:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v29

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->positionSelector(ILandroid/view/View;)V

    .line 4605
    .end local v9    # "childIndex":I
    :cond_11
    :goto_9
    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mBlockLayoutRequests:Z

    .line 4607
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->invokeOnItemScrollListener()V

    .line 4609
    const/16 v29, 0x0

    goto/16 :goto_0

    .line 4512
    .end local v3    # "absIncrementalDeltaX":I
    .end local v10    # "count":I
    .end local v11    # "down":Z
    .end local v17    # "footerViewsStart":I
    .end local v18    # "headerViewsCount":I
    .end local v19    # "i":I
    .end local v20    # "inTouchMode":Z
    .end local v26    # "start":I
    :cond_12
    const/4 v11, 0x0

    goto/16 :goto_7

    .line 4537
    .restart local v7    # "child":Landroid/view/View;
    .restart local v10    # "count":I
    .restart local v11    # "down":Z
    .restart local v17    # "footerViewsStart":I
    .restart local v18    # "headerViewsCount":I
    .restart local v19    # "i":I
    .restart local v20    # "inTouchMode":Z
    .restart local v26    # "start":I
    .restart local v27    # "top":I
    :cond_13
    add-int/lit8 v10, v10, 0x1

    .line 4538
    add-int v23, v16, v19

    .line 4539
    .local v23, "position":I
    move/from16 v0, v23

    move/from16 v1, v18

    if-lt v0, v1, :cond_14

    move/from16 v0, v23

    move/from16 v1, v17

    if-ge v0, v1, :cond_14

    .line 4540
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move/from16 v1, v23

    invoke-virtual {v0, v7, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 4532
    :cond_14
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_8

    .line 4545
    .end local v7    # "child":Landroid/view/View;
    .end local v19    # "i":I
    .end local v23    # "position":I
    .end local v27    # "top":I
    :cond_15
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getWidth()I

    move-result v29

    sub-int v4, v29, p2

    .line 4550
    .local v4, "bottom":I
    add-int/lit8 v19, v8, -0x1

    .restart local v19    # "i":I
    :goto_a
    if-ltz v19, :cond_b

    .line 4551
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 4552
    .restart local v7    # "child":Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v29

    move/from16 v0, v29

    if-le v0, v4, :cond_b

    .line 4555
    move/from16 v26, v19

    .line 4556
    add-int/lit8 v10, v10, 0x1

    .line 4557
    add-int v23, v16, v19

    .line 4558
    .restart local v23    # "position":I
    move/from16 v0, v23

    move/from16 v1, v18

    if-lt v0, v1, :cond_16

    move/from16 v0, v23

    move/from16 v1, v17

    if-ge v0, v1, :cond_16

    .line 4559
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move/from16 v1, v23

    invoke-virtual {v0, v7, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 4550
    :cond_16
    add-int/lit8 v19, v19, -0x1

    goto :goto_a

    .line 4596
    .end local v4    # "bottom":I
    .end local v7    # "child":Landroid/view/View;
    .end local v23    # "position":I
    .restart local v3    # "absIncrementalDeltaX":I
    :cond_17
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectorPosition:I

    move/from16 v29, v0

    const/16 v30, -0x1

    move/from16 v0, v29

    move/from16 v1, v30

    if-eq v0, v1, :cond_18

    .line 4597
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectorPosition:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    move/from16 v30, v0

    sub-int v9, v29, v30

    .line 4598
    .restart local v9    # "childIndex":I
    if-ltz v9, :cond_11

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v29

    move/from16 v0, v29

    if-ge v9, v0, :cond_11

    .line 4599
    const/16 v29, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v29

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->positionSelector(ILandroid/view/View;)V

    goto/16 :goto_9

    .line 4602
    .end local v9    # "childIndex":I
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectorRect:Landroid/graphics/Rect;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Rect;->setEmpty()V

    goto/16 :goto_9
.end method

.method protected updateScrollIndicators()V
    .locals 10

    .prologue
    const/4 v7, 0x4

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1715
    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mScrollLeft:Landroid/view/View;

    if-eqz v6, :cond_1

    .line 1718
    iget v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    if-lez v6, :cond_4

    move v0, v4

    .line 1721
    .local v0, "canScrollLeft":Z
    :goto_0
    if-nez v0, :cond_0

    .line 1722
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v6

    if-lez v6, :cond_0

    .line 1723
    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1724
    .local v2, "child":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v6

    iget-object v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    if-ge v6, v8, :cond_5

    move v0, v4

    .line 1728
    .end local v2    # "child":Landroid/view/View;
    :cond_0
    :goto_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mScrollLeft:Landroid/view/View;

    if-eqz v0, :cond_6

    move v6, v5

    :goto_2
    invoke-virtual {v8, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1731
    .end local v0    # "canScrollLeft":Z
    :cond_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mScrollRight:Landroid/view/View;

    if-eqz v6, :cond_3

    .line 1733
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildCount()I

    move-result v3

    .line 1736
    .local v3, "count":I
    iget v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    add-int/2addr v6, v3

    iget v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    if-ge v6, v8, :cond_7

    move v1, v4

    .line 1739
    .local v1, "canScrollRight":Z
    :goto_3
    if-nez v1, :cond_2

    if-lez v3, :cond_2

    .line 1740
    add-int/lit8 v6, v3, -0x1

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1741
    .restart local v2    # "child":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v6

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getRight()I

    move-result v8

    iget-object v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mListPadding:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    sub-int/2addr v8, v9

    if-le v6, v8, :cond_8

    move v1, v4

    .line 1744
    .end local v2    # "child":Landroid/view/View;
    :cond_2
    :goto_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mScrollRight:Landroid/view/View;

    if-eqz v1, :cond_9

    :goto_5
    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1746
    .end local v1    # "canScrollRight":Z
    .end local v3    # "count":I
    :cond_3
    return-void

    :cond_4
    move v0, v5

    .line 1718
    goto :goto_0

    .restart local v0    # "canScrollLeft":Z
    .restart local v2    # "child":Landroid/view/View;
    :cond_5
    move v0, v5

    .line 1724
    goto :goto_1

    .end local v2    # "child":Landroid/view/View;
    :cond_6
    move v6, v7

    .line 1728
    goto :goto_2

    .end local v0    # "canScrollLeft":Z
    .restart local v3    # "count":I
    :cond_7
    move v1, v5

    .line 1736
    goto :goto_3

    .restart local v1    # "canScrollRight":Z
    .restart local v2    # "child":Landroid/view/View;
    :cond_8
    move v1, v5

    .line 1741
    goto :goto_4

    .end local v2    # "child":Landroid/view/View;
    :cond_9
    move v5, v7

    .line 1744
    goto :goto_5
.end method

.method updateSelectorState()V
    .locals 2

    .prologue
    .line 2190
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 2191
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->shouldShowSelector()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2192
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 2197
    :cond_0
    :goto_0
    return-void

    .line 2194
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelector:Landroid/graphics/drawable/Drawable;

    sget-object v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->STATESET_NOTHING:[I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    goto :goto_0
.end method

.method public verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1, "dr"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 2241
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
