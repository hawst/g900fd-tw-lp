.class Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$1;
.super Ljava/lang/Object;
.source "HelpListScrollingState.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->access$000(Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getPenHovering(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 41
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mHelpPopup:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 43
    :cond_0
    return v2
.end method
