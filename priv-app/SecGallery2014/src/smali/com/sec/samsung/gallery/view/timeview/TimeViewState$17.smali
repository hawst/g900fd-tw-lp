.class Lcom/sec/samsung/gallery/view/timeview/TimeViewState$17;
.super Ljava/lang/Object;
.source "TimeViewState.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->startDetailViewInUIThread(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

.field final synthetic val$albumIndex:I

.field final synthetic val$itemIndex:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;II)V
    .locals 0

    .prologue
    .line 1999
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$17;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    iput p2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$17;->val$albumIndex:I

    iput p3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$17;->val$itemIndex:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 2001
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$17;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$5700(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 2002
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$17;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setClickEnabled(Z)V

    .line 2003
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$17;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    iget v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$17;->val$albumIndex:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$17;->val$itemIndex:I

    # invokes: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->startDetailView(II)V
    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$5800(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;II)V

    .line 2004
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$17;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$5700(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    .line 2005
    return-void
.end method
