.class Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewMultiPick;
.super Ljava/lang/Object;
.source "AlbumViewEventHandle.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$IAlbumViewMode;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AlbumViewMultiPick"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewMultiPick;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$1;

    .prologue
    .line 117
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewMultiPick;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;)V

    return-void
.end method


# virtual methods
.method public disableView()V
    .locals 0

    .prologue
    .line 127
    return-void
.end method

.method public enableView()V
    .locals 0

    .prologue
    .line 147
    return-void
.end method

.method public initializeView()V
    .locals 3

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewMultiPick;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mAlbumViewState:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->access$400(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;)Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->getActionBarManager()Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v0

    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewMultiPick;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->access$300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForPick;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 122
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewMultiPick;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mAlbumViewState:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->access$400(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;)Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 123
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 142
    return-void
.end method

.method public onHoverClick(Lcom/sec/android/gallery3d/data/MediaObject;)V
    .locals 0
    .param p1, "media"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 137
    return-void
.end method

.method public onItemClick(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlView;I)V
    .locals 0
    .param p1, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "view"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "position"    # I

    .prologue
    .line 133
    return-void
.end method
