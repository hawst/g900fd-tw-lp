.class Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;
.super Landroid/app/DialogFragment;
.source "AppChoiceDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AppChoiceDialogFragment"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment$ListClickListener;,
        Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment$AppListAdapter;
    }
.end annotation


# instance fields
.field private final mCtx:Landroid/content/Context;

.field private mGridItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mGridItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private mListClickListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment$ListClickListener;

.field private mTitleId:I

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;Landroid/content/Context;)V
    .locals 3
    .param p2, "ctx"    # Landroid/content/Context;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 108
    new-instance v1, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment$ListClickListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment$ListClickListener;-><init>(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$1;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->mListClickListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment$ListClickListener;

    .line 328
    new-instance v1, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment$2;-><init>(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->mGridItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 346
    new-instance v1, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment$3;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment$3;-><init>(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->mGridItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 114
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->mCtx:Landroid/content/Context;

    .line 115
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 116
    .local v0, "args":Landroid/os/Bundle;
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 117
    return-void
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;
    .param p1, "x1"    # I

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->onChoiceItemClick(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->mCtx:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;
    .param p1, "x1"    # I

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->resizeDialog(I)V

    return-void
.end method

.method private onChoiceItemClick(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 354
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mEvent:Lcom/sec/samsung/gallery/core/Event;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$1400(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mItemInfoHandler:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$ItemInfoAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$1100(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$ItemInfoAdapter;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mItemInfoHandler:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$ItemInfoAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$1100(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$ItemInfoAdapter;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$ItemInfoAdapter;->getItemObject(I)Ljava/lang/Object;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/core/Event;->setData(Ljava/lang/Object;)Lcom/sec/samsung/gallery/core/Event;

    .line 356
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mEvent:Lcom/sec/samsung/gallery/core/Event;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$1400(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->notifyObservers(Ljava/lang/Object;)V

    .line 358
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$1500(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 362
    :goto_1
    return-void

    .line 354
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mAppList:Ljava/util/List;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$400(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 359
    :catch_0
    move-exception v0

    .line 360
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1
.end method

.method private resizeDialog(I)V
    .locals 6
    .param p1, "orientation"    # I

    .prologue
    const/4 v3, -0x2

    .line 226
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mChoiceDialog:Landroid/app/Dialog;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$600(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/app/Dialog;

    move-result-object v4

    if-nez v4, :cond_1

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 229
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mChoiceDialog:Landroid/app/Dialog;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$600(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/app/Dialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 230
    .local v2, "window":Landroid/view/Window;
    if-eqz v2, :cond_0

    .line 233
    const/4 v4, 0x1

    if-ne p1, v4, :cond_4

    .line 234
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v4

    if-eqz v4, :cond_2

    const/16 v1, 0xc

    .line 235
    .local v1, "minItemCount":I
    :goto_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mAppList:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$400(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mAppList:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$400(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v1, :cond_3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->mCtx:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d012b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 238
    .local v0, "hParam":I
    :goto_2
    invoke-virtual {v2, v3, v0}, Landroid/view/Window;->setLayout(II)V

    goto :goto_0

    .line 234
    .end local v0    # "hParam":I
    .end local v1    # "minItemCount":I
    :cond_2
    const/4 v1, 0x6

    goto :goto_1

    .restart local v1    # "minItemCount":I
    :cond_3
    move v0, v3

    .line 235
    goto :goto_2

    .line 240
    .end local v1    # "minItemCount":I
    :cond_4
    invoke-virtual {v2, v3, v3}, Landroid/view/Window;->setLayout(II)V

    goto :goto_0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 121
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 122
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mOnOrientationChangedListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnOrientationChangedListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$100(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnOrientationChangedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mOnOrientationChangedListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnOrientationChangedListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$100(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnOrientationChangedListener;

    move-result-object v0

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnOrientationChangedListener;->onOrientationChanged(I)V

    .line 124
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 125
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->resizeDialog(I)V

    .line 126
    :cond_1
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDisableScrollBar:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mDisplayAsGridView:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$200(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mGridView:Landroid/widget/GridView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$300(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/widget/GridView;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mAppList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$400(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mAppList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$400(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x10

    if-ge v0, v1, :cond_2

    .line 128
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 129
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mGridView:Landroid/widget/GridView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$300(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/widget/GridView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setVerticalScrollBarEnabled(Z)V

    .line 134
    :cond_2
    :goto_0
    return-void

    .line 131
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mGridView:Landroid/widget/GridView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$300(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/widget/GridView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setVerticalScrollBarEnabled(Z)V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 138
    new-instance v1, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment$AppListAdapter;

    const/4 v9, 0x0

    invoke-direct {v1, p0, v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment$AppListAdapter;-><init>(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$1;)V

    .line 140
    .local v1, "detailsListAdapter":Landroid/widget/BaseAdapter;
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePopupMenuDialog:Z

    if-eqz v9, :cond_4

    .line 141
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    new-instance v10, Landroid/app/Dialog;

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->mCtx:Landroid/content/Context;

    invoke-direct {v10, v11}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mChoiceDialog:Landroid/app/Dialog;
    invoke-static {v9, v10}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$602(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;Landroid/app/Dialog;)Landroid/app/Dialog;

    .line 142
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->mCtx:Landroid/content/Context;

    invoke-static {v9}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 143
    .local v2, "factory":Landroid/view/LayoutInflater;
    const v9, 0x7f0300d3

    const/4 v10, 0x0

    invoke-virtual {v2, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 144
    .local v6, "shareViaDialog":Landroid/view/View;
    const v9, 0x7f0f0212

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    .line 145
    .local v5, "shareItemsList":Landroid/widget/ListView;
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->mGridItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v5, v9}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 146
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->mGridItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {v5, v9}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 147
    invoke-virtual {v5, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 150
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mChoiceDialog:Landroid/app/Dialog;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$600(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/app/Dialog;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v8

    .line 151
    .local v8, "wmlp":Landroid/view/WindowManager$LayoutParams;
    const/16 v9, 0x35

    iput v9, v8, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 152
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->mCtx:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 153
    .local v4, "resources":Landroid/content/res/Resources;
    const v9, 0x7f0d02f6

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, v8, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 156
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 157
    const v9, 0x7f0c0054

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 161
    .local v3, "maxDialogItems":I
    :goto_0
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mAppList:Ljava/util/List;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$400(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-lt v9, v3, :cond_0

    .line 162
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 163
    invoke-virtual {v5}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    const v10, 0x7f0d02f8

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    iput v10, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 169
    :cond_0
    :goto_1
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mChoiceDialog:Landroid/app/Dialog;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$600(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/app/Dialog;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v9

    const/4 v10, 0x2

    invoke-virtual {v9, v10}, Landroid/view/Window;->clearFlags(I)V

    .line 170
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mChoiceDialog:Landroid/app/Dialog;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$600(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/app/Dialog;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v9

    new-instance v10, Landroid/graphics/drawable/ColorDrawable;

    const/4 v11, 0x0

    invoke-direct {v10, v11}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v9, v10}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 171
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mChoiceDialog:Landroid/app/Dialog;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$600(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/app/Dialog;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 172
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mChoiceDialog:Landroid/app/Dialog;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$600(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/app/Dialog;

    move-result-object v9

    invoke-virtual {v9, v6}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 216
    .end local v2    # "factory":Landroid/view/LayoutInflater;
    .end local v3    # "maxDialogItems":I
    .end local v4    # "resources":Landroid/content/res/Resources;
    .end local v5    # "shareItemsList":Landroid/widget/ListView;
    .end local v6    # "shareViaDialog":Landroid/view/View;
    .end local v8    # "wmlp":Landroid/view/WindowManager$LayoutParams;
    :cond_1
    :goto_2
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mChoiceDialog:Landroid/app/Dialog;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$600(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/app/Dialog;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v9

    const v10, 0x10100

    invoke-virtual {v9, v10}, Landroid/view/Window;->addFlags(I)V

    .line 220
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mChoiceDialog:Landroid/app/Dialog;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$600(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/app/Dialog;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 221
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mChoiceDialog:Landroid/app/Dialog;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$600(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/app/Dialog;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->hideStatusIcon(Ljava/lang/Object;)V

    .line 222
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mChoiceDialog:Landroid/app/Dialog;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$600(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/app/Dialog;

    move-result-object v9

    return-object v9

    .line 159
    .restart local v2    # "factory":Landroid/view/LayoutInflater;
    .restart local v4    # "resources":Landroid/content/res/Resources;
    .restart local v5    # "shareItemsList":Landroid/widget/ListView;
    .restart local v6    # "shareViaDialog":Landroid/view/View;
    .restart local v8    # "wmlp":Landroid/view/WindowManager$LayoutParams;
    :cond_2
    const v9, 0x7f0c0053

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .restart local v3    # "maxDialogItems":I
    goto/16 :goto_0

    .line 165
    :cond_3
    invoke-virtual {v5}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    const v10, 0x7f0d02f7

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    iput v10, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_1

    .line 174
    .end local v2    # "factory":Landroid/view/LayoutInflater;
    .end local v3    # "maxDialogItems":I
    .end local v4    # "resources":Landroid/content/res/Resources;
    .end local v5    # "shareItemsList":Landroid/widget/ListView;
    .end local v6    # "shareViaDialog":Landroid/view/View;
    .end local v8    # "wmlp":Landroid/view/WindowManager$LayoutParams;
    :cond_4
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->mCtx:Landroid/content/Context;

    invoke-direct {v0, v9}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 175
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mDisplayAsGridView:Z
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$200(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 176
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->mCtx:Landroid/content/Context;

    invoke-static {v9}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 177
    .restart local v2    # "factory":Landroid/view/LayoutInflater;
    const v9, 0x7f030023

    const/4 v10, 0x0

    invoke-virtual {v2, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 178
    .local v7, "view":Landroid/view/View;
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    const v9, 0x7f0f0064

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/GridView;

    # setter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mGridView:Landroid/widget/GridView;
    invoke-static {v10, v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$302(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;Landroid/widget/GridView;)Landroid/widget/GridView;

    .line 179
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mAppList:Ljava/util/List;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$400(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Ljava/util/List;

    move-result-object v9

    if-eqz v9, :cond_5

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mAppList:Ljava/util/List;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$400(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    const/4 v10, 0x3

    if-ge v9, v10, :cond_5

    .line 180
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mGridView:Landroid/widget/GridView;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$300(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/widget/GridView;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mAppList:Ljava/util/List;
    invoke-static {v10}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$400(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 182
    :cond_5
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mGridView:Landroid/widget/GridView;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$300(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/widget/GridView;

    move-result-object v9

    const v10, 0x1020004

    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/GridView;->setEmptyView(Landroid/view/View;)V

    .line 183
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mGridView:Landroid/widget/GridView;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$300(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/widget/GridView;

    move-result-object v9

    invoke-virtual {v9, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 184
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mGridView:Landroid/widget/GridView;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$300(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/widget/GridView;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->mGridItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v9, v10}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 185
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mGridView:Landroid/widget/GridView;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$300(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/widget/GridView;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->mGridItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {v9, v10}, Landroid/widget/GridView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 186
    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 187
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v9, :cond_6

    .line 188
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mAppList:Ljava/util/List;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$400(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Ljava/util/List;

    move-result-object v9

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mAppList:Ljava/util/List;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$400(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    const/4 v10, 0x4

    if-ge v9, v10, :cond_6

    .line 189
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mGridView:Landroid/widget/GridView;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$300(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/widget/GridView;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mAppList:Ljava/util/List;
    invoke-static {v10}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$400(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 191
    :cond_6
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 192
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mAppList:Ljava/util/List;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$400(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Ljava/util/List;

    move-result-object v9

    if-eqz v9, :cond_7

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mAppList:Ljava/util/List;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$400(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    const/4 v10, 0x2

    if-le v9, v10, :cond_7

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mGridView:Landroid/widget/GridView;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$300(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/widget/GridView;

    move-result-object v9

    if-eqz v9, :cond_7

    .line 193
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mGridView:Landroid/widget/GridView;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$300(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/widget/GridView;

    move-result-object v9

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 194
    :cond_7
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDisableScrollBar:Z

    if-eqz v9, :cond_8

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mAppList:Ljava/util/List;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$400(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Ljava/util/List;

    move-result-object v9

    if-eqz v9, :cond_8

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mAppList:Ljava/util/List;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$400(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    const/16 v10, 0x10

    if-ge v9, v10, :cond_8

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->mCtx:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->orientation:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_8

    .line 196
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mGridView:Landroid/widget/GridView;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$300(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/widget/GridView;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/GridView;->setVerticalScrollBarEnabled(Z)V

    .line 200
    .end local v2    # "factory":Landroid/view/LayoutInflater;
    .end local v7    # "view":Landroid/view/View;
    :cond_8
    :goto_3
    iget v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->mTitleId:I

    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 201
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v10

    # setter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mChoiceDialog:Landroid/app/Dialog;
    invoke-static {v9, v10}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$602(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;Landroid/app/Dialog;)Landroid/app/Dialog;

    .line 203
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mChoiceDialog:Landroid/app/Dialog;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$600(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/app/Dialog;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFullScreen(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 204
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mChoiceDialog:Landroid/app/Dialog;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$600(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/app/Dialog;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v9

    const/16 v10, 0x400

    const/16 v11, 0x400

    invoke-virtual {v9, v10, v11}, Landroid/view/Window;->setFlags(II)V

    .line 206
    :cond_9
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v9

    if-nez v9, :cond_a

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 207
    :cond_a
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mChoiceDialog:Landroid/app/Dialog;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$600(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/app/Dialog;

    move-result-object v9

    new-instance v10, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment$1;

    invoke-direct {v10, p0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment$1;-><init>(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;)V

    invoke-virtual {v9, v10}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    goto/16 :goto_2

    .line 199
    :cond_b
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->mListClickListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment$ListClickListener;

    invoke-virtual {v0, v1, v9}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_3
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 246
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 248
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePopupMenuDialog:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->mCtx:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->mCtx:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "SHARE_DIALOG_HIDDEN"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;)V

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mOnDialogDismissListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnDialogDismissListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$900(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnDialogDismissListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 252
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mOnDialogDismissListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnDialogDismissListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$900(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnDialogDismissListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnDialogDismissListener;->onDialogDismiss()V

    .line 253
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mOnResumeListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnResumeListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$1000(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnResumeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mOnResumeListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnResumeListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->access$1000(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnResumeListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnResumeListener;->onResumeCallback()V

    .line 264
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 265
    return-void
.end method

.method public setTitle(I)V
    .locals 0
    .param p1, "titleId"    # I

    .prologue
    .line 256
    iput p1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->mTitleId:I

    .line 257
    return-void
.end method
