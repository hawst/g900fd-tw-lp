.class Lcom/sec/samsung/gallery/mapfragment/MapViewState$1;
.super Lorg/puremvc/java/patterns/mediator/Mediator;
.source "MapViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/mapfragment/MapViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/mapfragment/MapViewState;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/Object;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$1;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    invoke-direct {p0, p2, p3}, Lorg/puremvc/java/patterns/mediator/Mediator;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public handleNotification(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 4
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 153
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getName()Ljava/lang/String;

    move-result-object v1

    .line 154
    .local v1, "notiName":Ljava/lang/String;
    const-string v3, "SECRET_MODE_CHANGED"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 155
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Object;

    move-object v2, v3

    check-cast v2, [Ljava/lang/Object;

    .line 156
    .local v2, "params":[Ljava/lang/Object;
    const/4 v3, 0x0

    aget-object v0, v2, v3

    check-cast v0, Landroid/content/res/Configuration;

    .line 157
    .local v0, "configuration":Landroid/content/res/Configuration;
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$1;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # invokes: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->onConfigChanged(Landroid/content/res/Configuration;)V
    invoke-static {v3, v0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$000(Lcom/sec/samsung/gallery/mapfragment/MapViewState;Landroid/content/res/Configuration;)V

    .line 159
    .end local v0    # "configuration":Landroid/content/res/Configuration;
    .end local v2    # "params":[Ljava/lang/Object;
    :cond_0
    return-void
.end method

.method public listNotificationInterests()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 146
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "SECRET_MODE_CHANGED"

    aput-object v2, v0, v1

    return-object v0
.end method
