.class public Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;
.super Ljava/lang/Object;
.source "DetailViewScreenOrientation.java"


# instance fields
.field private mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

.field private mAutoRotationUse:Z

.field private mContext:Landroid/content/Context;

.field private mOrientation:I

.field private mOrientationListener:Landroid/view/OrientationEventListener;

.field private mRotateMenuItem:Landroid/view/MenuItem;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "actionBarView"    # Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mAutoRotationUse:Z

    .line 29
    iput v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mOrientation:I

    .line 33
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    .line 34
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)Landroid/view/MenuItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->checkAutoRotationUse()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mAutoRotationUse:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    .prologue
    .line 23
    iget v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mOrientation:I

    return v0
.end method

.method private checkAutoRotationUse()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 225
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "accelerometer_rotation"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    if-ne v2, v1, :cond_0

    :goto_0
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mAutoRotationUse:Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    :goto_1
    return-void

    .line 225
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 227
    :catch_0
    move-exception v0

    .line 228
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method private setRotationIcon()V
    .locals 2

    .prologue
    .line 53
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation$1;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 150
    return-void
.end method


# virtual methods
.method public handleScreenRotation()V
    .locals 9

    .prologue
    const/16 v8, 0x9

    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 233
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 234
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    const-string/jumbo v4, "window"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    .line 235
    .local v2, "wm":Landroid/view/WindowManager;
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 236
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mOrientation:I

    .line 238
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v3

    if-eqz v3, :cond_2

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsPortraitTablet:Z

    if-nez v3, :cond_2

    .line 239
    iget v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mOrientation:I

    packed-switch v3, :pswitch_data_0

    .line 283
    :cond_0
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryMotion()Lcom/sec/android/gallery3d/app/GalleryMotion;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v3

    sget-object v4, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_NONE:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v3, v4, :cond_1

    .line 284
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v4

    sget v5, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELAY_HIDE_BARS:I

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->notifyObservers(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 291
    :cond_1
    :goto_1
    return-void

    .line 241
    :pswitch_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3, v5}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 245
    :pswitch_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3, v8}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 249
    :pswitch_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3, v7}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 253
    :pswitch_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3, v6}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 260
    :cond_2
    iget v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mOrientation:I

    packed-switch v3, :pswitch_data_1

    goto :goto_0

    .line 262
    :pswitch_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3, v6}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 266
    :pswitch_5
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3, v5}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 270
    :pswitch_6
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsPortraitTablet:Z

    if-eqz v3, :cond_0

    .line 271
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3, v8}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 275
    :pswitch_7
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3, v7}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 288
    :catch_0
    move-exception v1

    .line 289
    .local v1, "e":Ljava/lang/NullPointerException;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mActionBarView:Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v4

    sget v5, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELAY_HIDE_BARS:I

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_1

    .line 239
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 260
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public isRotationMenuItemEnabled()Z
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v0

    .line 49
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->unregisterListener()V

    .line 214
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 215
    return-void
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/16 v7, 0x9

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 157
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-nez v2, :cond_0

    .line 158
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->setRotationIcon()V

    .line 160
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 161
    .local v1, "wm":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 162
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mOrientation:I

    .line 164
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsPortraitTablet:Z

    if-nez v2, :cond_2

    .line 165
    iget v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mOrientation:I

    packed-switch v2, :pswitch_data_0

    .line 209
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v2}, Landroid/view/OrientationEventListener;->enable()V

    .line 210
    return-void

    .line 167
    :pswitch_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2, v4}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 171
    :pswitch_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2, v7}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 175
    :pswitch_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2, v6}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 179
    :pswitch_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2, v5}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 186
    :cond_2
    iget v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mOrientation:I

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 188
    :pswitch_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2, v5}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 192
    :pswitch_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2, v4}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 196
    :pswitch_6
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsPortraitTablet:Z

    if-eqz v2, :cond_1

    .line 197
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2, v7}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 202
    :pswitch_7
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2, v6}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 165
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 186
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public refreshRotationIcon()V
    .locals 0

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->onResume()V

    .line 154
    return-void
.end method

.method public setRotationMenuItem(Landroid/view/MenuItem;)V
    .locals 2
    .param p1, "rotationMenuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;

    .line 41
    :cond_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;

    .line 42
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mRotateMenuItem:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 43
    return-void
.end method

.method public unregisterListener()V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 221
    :cond_0
    return-void
.end method
