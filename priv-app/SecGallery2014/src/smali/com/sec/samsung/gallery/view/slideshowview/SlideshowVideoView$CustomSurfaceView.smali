.class public Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;
.super Landroid/view/SurfaceView;
.source "SlideshowVideoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CustomSurfaceView"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 365
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    .line 366
    invoke-direct {p0, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 367
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 372
    const/4 v3, 0x0

    .line 373
    .local v3, "videoWidth":I
    const/4 v2, 0x0

    .line 374
    .local v2, "videoHeight":I
    const/4 v0, 0x1

    .line 375
    .local v0, "mSurfaceHeight":I
    const/4 v1, 0x1

    .line 376
    .local v1, "mSurfaceWidth":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Landroid/media/MediaPlayer;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 377
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Landroid/media/MediaPlayer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v3

    .line 378
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Landroid/media/MediaPlayer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v2

    .line 381
    :cond_0
    if-nez v3, :cond_1

    if-nez v2, :cond_1

    .line 382
    invoke-super {p0, v1, v0}, Landroid/view/SurfaceView;->setMeasuredDimension(II)V

    .line 396
    :goto_0
    return-void

    .line 385
    :cond_1
    invoke-virtual {p0, v3, p1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;->resolveAdjustedSize(II)I

    move-result v1

    .line 387
    invoke-virtual {p0, v2, p2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;->resolveAdjustedSize(II)I

    move-result v0

    .line 391
    mul-int v4, v3, v0

    mul-int v5, v2, v1

    if-lt v4, v5, :cond_2

    .line 392
    mul-int v4, v1, v2

    div-int v0, v4, v3

    .line 395
    :goto_1
    invoke-super {p0, v1, v0}, Landroid/view/SurfaceView;->setMeasuredDimension(II)V

    goto :goto_0

    .line 394
    :cond_2
    mul-int v4, v0, v3

    div-int v1, v4, v2

    goto :goto_1
.end method

.method public resolveAdjustedSize(II)I
    .locals 3
    .param p1, "desiredSize"    # I
    .param p2, "measureSpec"    # I

    .prologue
    .line 400
    move v0, p1

    .line 402
    .local v0, "result":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 403
    .local v1, "specMode":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 405
    .local v2, "specSize":I
    sparse-switch v1, :sswitch_data_0

    .line 422
    :goto_0
    return v0

    .line 407
    :sswitch_0
    move v0, p1

    .line 408
    goto :goto_0

    .line 411
    :sswitch_1
    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 412
    goto :goto_0

    .line 415
    :sswitch_2
    move v0, v2

    .line 416
    goto :goto_0

    .line 405
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method
