.class Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;
.super Landroid/os/AsyncTask;
.source "TimeViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/timeview/TimeViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateSelectionModeTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V
    .locals 0

    .prologue
    .line 3054
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Lcom/sec/samsung/gallery/view/timeview/TimeViewState$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState$1;

    .prologue
    .line 3054
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    return-void
.end method

.method private updateSelectionMode()Ljava/lang/Boolean;
    .locals 19

    .prologue
    .line 3062
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$1100(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCloneMediaList()Ljava/util/LinkedList;

    move-result-object v11

    .line 3064
    .local v11, "mediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v8, 0x0

    .line 3065
    .local v8, "isSelectedDataChanged":Z
    invoke-virtual {v11}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 3066
    .local v13, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->isCancelled()Z

    move-result v17

    if-eqz v17, :cond_3

    .line 3135
    .end local v13    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$1100(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/gallery3d/ui/SelectionManager;->updateSelectedCountMap(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 3136
    const/4 v8, 0x1

    .line 3138
    :cond_2
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v17

    return-object v17

    .line 3068
    .restart local v13    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_3
    instance-of v0, v13, Lcom/sec/android/gallery3d/data/MediaItem;

    move/from16 v17, v0

    if-eqz v17, :cond_0

    move-object v9, v13

    .line 3069
    check-cast v9, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 3070
    .local v9, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    const/16 v16, 0x0

    .line 3071
    .local v16, "updateSelection":Z
    instance-of v7, v9, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .line 3072
    .local v7, "isLocalItem":Z
    instance-of v0, v9, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    move/from16 v17, v0

    if-nez v17, :cond_4

    instance-of v0, v9, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    move/from16 v17, v0

    if-eqz v17, :cond_9

    :cond_4
    const/4 v6, 0x1

    .line 3073
    .local v6, "isCloudItem":Z
    :goto_2
    sget-boolean v17, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-nez v17, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$7700(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v17

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    if-eqz v17, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$7800(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->isPicasaDataExist(Landroid/content/Context;)Z

    move-result v17

    if-eqz v17, :cond_a

    const/4 v10, 0x1

    .line 3076
    .local v10, "mIsPicasaAccountActive":Z
    :goto_3
    const/4 v4, 0x0

    .line 3077
    .local v4, "filePathOfMediaSet":Ljava/lang/String;
    const/4 v3, 0x0

    .line 3079
    .local v3, "filePathOfItem":Ljava/lang/String;
    const/4 v12, 0x0

    .line 3080
    .local v12, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getParentSetPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v14

    .line 3081
    .local v14, "parentPath":Lcom/sec/android/gallery3d/data/Path;
    if-eqz v14, :cond_6

    .line 3082
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$7900(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v12

    .line 3083
    if-eqz v12, :cond_6

    instance-of v0, v12, Lcom/sec/android/gallery3d/data/LocalAlbum;

    move/from16 v17, v0

    if-nez v17, :cond_5

    instance-of v0, v12, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    move/from16 v17, v0

    if-eqz v17, :cond_6

    .line 3084
    :cond_5
    invoke-virtual {v12}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v4

    .line 3085
    invoke-static {v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getParentPathOnFileSystem(Lcom/sec/android/gallery3d/data/MediaItem;)Ljava/lang/String;

    move-result-object v3

    .line 3086
    const/4 v7, 0x1

    .line 3090
    :cond_6
    if-eqz v4, :cond_b

    const-string v17, ""

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_b

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_b

    .line 3092
    const/16 v16, 0x1

    .line 3114
    :cond_7
    :goto_4
    if-eqz v16, :cond_0

    if-nez v7, :cond_8

    if-eqz v6, :cond_0

    .line 3116
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$500(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v17

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/gallery3d/app/StateManager;->setReloadRequiredOnResume(Z)V

    .line 3117
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$2400(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->dismissDialogs()V

    .line 3119
    instance-of v0, v9, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    move/from16 v17, v0

    if-eqz v17, :cond_10

    .line 3120
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    move-object/from16 v17, v0

    # invokes: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->exitSelectionMode()V
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$1300(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    goto/16 :goto_1

    .line 3072
    .end local v3    # "filePathOfItem":Ljava/lang/String;
    .end local v4    # "filePathOfMediaSet":Ljava/lang/String;
    .end local v6    # "isCloudItem":Z
    .end local v10    # "mIsPicasaAccountActive":Z
    .end local v12    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v14    # "parentPath":Lcom/sec/android/gallery3d/data/Path;
    :cond_9
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 3073
    .restart local v6    # "isCloudItem":Z
    :cond_a
    const/4 v10, 0x0

    goto :goto_3

    .line 3094
    .restart local v3    # "filePathOfItem":Ljava/lang/String;
    .restart local v4    # "filePathOfMediaSet":Ljava/lang/String;
    .restart local v10    # "mIsPicasaAccountActive":Z
    .restart local v12    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .restart local v14    # "parentPath":Lcom/sec/android/gallery3d/data/Path;
    :cond_b
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v17

    if-eqz v17, :cond_f

    .line 3095
    new-instance v2, Ljava/io/File;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3096
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v17

    if-nez v17, :cond_c

    if-eqz v6, :cond_d

    :cond_c
    if-eqz v7, :cond_e

    move-object/from16 v17, v9

    check-cast v17, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->isDeleted()Z

    move-result v17

    if-eqz v17, :cond_e

    .line 3097
    :cond_d
    const/16 v16, 0x1

    .line 3099
    :cond_e
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$500(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v17

    sget-object v18, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_f

    instance-of v0, v9, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    move/from16 v17, v0

    if-eqz v17, :cond_f

    move-object/from16 v17, v9

    .line 3100
    check-cast v17, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getPersonId()I

    move-result v17

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_f

    .line 3101
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$8000(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v18

    move-object/from16 v17, v9

    check-cast v17, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getPersonId()I

    move-result v17

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/access/face/PersonList;->getUserData(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v15

    .line 3102
    .local v15, "personlist":Ljava/lang/String;
    if-nez v15, :cond_f

    .line 3103
    const/16 v16, 0x1

    .line 3107
    .end local v2    # "file":Ljava/io/File;
    .end local v15    # "personlist":Ljava/lang/String;
    :cond_f
    if-eqz v6, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$8100(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v0, v9}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;->isCloudItemDeleted(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 3109
    const/16 v16, 0x1

    goto/16 :goto_4

    .line 3123
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$5400(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Z

    move-result v17

    if-nez v17, :cond_11

    if-eqz v7, :cond_12

    .line 3124
    :cond_11
    const/4 v8, 0x1

    .line 3125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$1100(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 3126
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$1100(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v17

    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v12, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;I)V

    goto/16 :goto_0

    .line 3129
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    move-object/from16 v17, v0

    # invokes: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->exitSelectionMode()V
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$1300(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    goto/16 :goto_1
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 1
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 3058
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->updateSelectionMode()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 3054
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 3143
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 3145
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3146
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # invokes: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAllPostProcess()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$7200(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3152
    :cond_0
    :goto_0
    return-void

    .line 3148
    :catch_0
    move-exception v0

    .line 3149
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 3150
    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$900()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onPostExecute : NullPointerException!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 3054
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
