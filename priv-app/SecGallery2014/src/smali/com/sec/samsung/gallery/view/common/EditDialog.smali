.class public Lcom/sec/samsung/gallery/view/common/EditDialog;
.super Lcom/sec/samsung/gallery/view/ViewObservable;
.source "EditDialog.java"


# instance fields
.field private final mCtx:Landroid/content/Context;

.field private mDialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 0
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "t"    # I
    .param p3, "msg"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/ViewObservable;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/EditDialog;->mCtx:Landroid/content/Context;

    .line 31
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/common/EditDialog;->createDialog()V

    .line 32
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/common/EditDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/EditDialog;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/EditDialog;->startMoreService()V

    return-void
.end method

.method private startMoreService()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 78
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 79
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "samsungapps://ProductDetail/com.sec.android.mimage.photoretouching"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 80
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 81
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/EditDialog;->mCtx:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 82
    return-void
.end method


# virtual methods
.method public createDialog()V
    .locals 4

    .prologue
    .line 47
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/EditDialog;->mCtx:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 48
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/EditDialog;->mCtx:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    const v2, 0x7f0e011c

    :goto_0
    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 49
    .local v1, "editorText":Ljava/lang/String;
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 50
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/EditDialog;->mCtx:Landroid/content/Context;

    const v3, 0x7f0e019b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 51
    const v2, 0x7f0e010d

    new-instance v3, Lcom/sec/samsung/gallery/view/common/EditDialog$1;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/view/common/EditDialog$1;-><init>(Lcom/sec/samsung/gallery/view/common/EditDialog;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 62
    const v2, 0x7f0e0046

    new-instance v3, Lcom/sec/samsung/gallery/view/common/EditDialog$2;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/view/common/EditDialog$2;-><init>(Lcom/sec/samsung/gallery/view/common/EditDialog;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 70
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/EditDialog;->mDialog:Landroid/app/Dialog;

    .line 71
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 72
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/EditDialog;->mDialog:Landroid/app/Dialog;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 74
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/EditDialog;->mDialog:Landroid/app/Dialog;

    invoke-static {v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->hideStatusIcon(Ljava/lang/Object;)V

    .line 75
    return-void

    .line 48
    .end local v1    # "editorText":Ljava/lang/String;
    :cond_1
    const v2, 0x7f0e011b

    goto :goto_0
.end method

.method public dismissDialog()V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditDialog;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 44
    :cond_0
    return-void
.end method

.method public showDialog()V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditDialog;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 38
    :cond_0
    return-void
.end method
