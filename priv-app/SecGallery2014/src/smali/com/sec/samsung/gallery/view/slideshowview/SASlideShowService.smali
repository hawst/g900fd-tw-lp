.class public Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;
.super Landroid/app/Service;
.source "SASlideShowService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;,
        Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;
    }
.end annotation


# static fields
.field private static final ACTION_ADD_GLSURFACE_VIEW:Ljava/lang/String; = "ACTION_ADD_GLSURFACE_VIEW"

.field private static final ACTION_PRESENTATION_VIDEO_VIEW:Ljava/lang/String; = "ACTION_PRESENTATION_VIDEO_VIEW"

.field private static final ACTION_REMOVE_PRESENTATION_VIDEO_VIEW:Ljava/lang/String; = "ACTION_REMOVE_PRESENTATION_VIDEO_VIEW"

.field public static final COMMAND_STOP:I = 0x9

.field public static final KEY_COMMAND:Ljava/lang/String; = "key_method"

.field public static final KEY_DISPLAY_ID:Ljava/lang/String; = "key_display_id"

.field public static final NOTI_ID:I = 0x270f

.field private static TAG:Ljava/lang/String;

.field private static mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

.field private static mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;


# instance fields
.field private isSlideshowVideoPlaying:Z

.field private mFrameLayoutVideoView:Landroid/widget/FrameLayout;

.field mHandler:Landroid/os/Handler;

.field private mIgnoreBackkeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

.field private mPresentationFocuser:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;

.field private mPresentationReceiver:Landroid/content/BroadcastReceiver;

.field private mSlideshowPresentationVideoView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

.field private mUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-class v0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->isSlideshowVideoPlaying:Z

    .line 112
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentationReceiver:Landroid/content/BroadcastReceiver;

    .line 326
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$2;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 341
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$3;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mIgnoreBackkeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 352
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentationFocuser:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;

    .line 436
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$4;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$4;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mHandler:Landroid/os/Handler;

    .line 451
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mUri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideshowPresentationVideoView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Landroid/view/WindowManager$LayoutParams;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->createLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideshowPresentationVideoView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Landroid/content/DialogInterface$OnKeyListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mIgnoreBackkeyListener:Landroid/content/DialogInterface$OnKeyListener;

    return-object v0
.end method

.method static synthetic access$200()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->isSlideshowVideoPlaying:Z

    return v0
.end method

.method static synthetic access$402(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->isSlideshowVideoPlaying:Z

    return p1
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->clearSlideshowMode(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/android/gallery3d/glcore/GlRootView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;Lcom/sec/android/gallery3d/glcore/GlRootView;)Lcom/sec/android/gallery3d/glcore/GlRootView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mFrameLayoutVideoView:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;Landroid/widget/FrameLayout;)Landroid/widget/FrameLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;
    .param p1, "x1"    # Landroid/widget/FrameLayout;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mFrameLayoutVideoView:Landroid/widget/FrameLayout;

    return-object p1
.end method

.method private clearSlideshowMode(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 323
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->hideNotification()V

    .line 324
    return-void
.end method

.method private createLayoutParams()Landroid/view/WindowManager$LayoutParams;
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 293
    const/16 v3, 0x7d2

    .line 295
    .local v3, "windowType":I
    :try_start_0
    const-class v4, Landroid/view/WindowManager$LayoutParams;

    const-string v5, "TYPE_FAKE_APPLICATION"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 296
    .local v1, "field":Ljava/lang/reflect/Field;
    if-eqz v1, :cond_0

    .line 297
    invoke-virtual {v1, v1}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 302
    .end local v1    # "field":Ljava/lang/reflect/Field;
    :cond_0
    :goto_0
    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    const v4, 0x40160

    const/4 v5, 0x1

    invoke-direct {v2, v3, v4, v5}, Landroid/view/WindowManager$LayoutParams;-><init>(III)V

    .line 309
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    iput v6, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 310
    iput v6, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 312
    const/16 v4, 0x33

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 313
    const/16 v4, 0x20

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    .line 314
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 316
    return-object v2

    .line 298
    .end local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    :catch_0
    move-exception v0

    .line 299
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private createNotification()Landroid/app/Notification;
    .locals 3

    .prologue
    .line 230
    new-instance v1, Landroid/app/Notification$Builder;

    invoke-direct {v1, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0e018e

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    const v2, 0x7f0e018f

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->getStopIntent()Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    const v2, 0x7f020476

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 237
    .local v0, "notification":Landroid/app/Notification;
    return-object v0
.end method

.method private createParams()Landroid/widget/FrameLayout$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 157
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 159
    .local v0, "params":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 160
    return-object v0
.end method

.method private getStopIntent()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 241
    const/4 v0, 0x0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "key_method"

    const/16 v3, 0x9

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x8000000

    invoke-static {p0, v0, v1, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private hideNotification()V
    .locals 2

    .prologue
    .line 225
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 226
    .local v0, "notiManager":Landroid/app/NotificationManager;
    const/16 v1, 0x270f

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 227
    return-void
.end method

.method private static releaseWakeLock()V
    .locals 1

    .prologue
    .line 432
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    if-eqz v0, :cond_0

    .line 433
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->releaseWakeLock()V

    .line 434
    :cond_0
    return-void
.end method

.method private static resetContent()V
    .locals 1

    .prologue
    .line 165
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    if-eqz v0, :cond_0

    .line 166
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->destroy()V

    .line 167
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    .line 169
    :cond_0
    return-void
.end method

.method public static setSlideShowContent(Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V
    .locals 0
    .param p0, "musicPlayer"    # Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    .param p1, "slideShowViewState"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .prologue
    .line 107
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->resetContent()V

    .line 108
    sput-object p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    .line 109
    sput-object p1, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    .line 110
    return-void
.end method

.method private show(I)V
    .locals 5
    .param p1, "displayId"    # I

    .prologue
    .line 272
    sget-object v3, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "show presentation"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    const-string v3, "display"

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/display/DisplayManager;

    .line 276
    .local v2, "displayManager":Landroid/hardware/display/DisplayManager;
    invoke-virtual {v2, p1}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;

    move-result-object v0

    .line 277
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->createDisplayContext(Landroid/view/Display;)Landroid/content/Context;

    move-result-object v1

    .line 279
    .local v1, "displayContext":Landroid/content/Context;
    new-instance v3, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    invoke-direct {v3, p0, v1, v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;Landroid/content/Context;Landroid/view/Display;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    .line 281
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    if-eqz v3, :cond_0

    .line 282
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 283
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentationFocuser:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;

    invoke-virtual {v3, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->sendBroadcast(Landroid/content/Context;)V

    .line 284
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentationFocuser:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;

    invoke-virtual {v3, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->registerReceiver(Landroid/content/Context;)V

    .line 285
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->show()V

    .line 287
    :cond_0
    return-void
.end method

.method private showNotification()V
    .locals 3

    .prologue
    .line 220
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 221
    .local v0, "notiManager":Landroid/app/NotificationManager;
    const/16 v1, 0x270f

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->createNotification()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 222
    return-void
.end method

.method public static stopPresentation(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 421
    const-string v3, "display"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/display/DisplayManager;

    .line 422
    .local v1, "dm":Landroid/hardware/display/DisplayManager;
    const-string v3, "android.hardware.display.category.PRESENTATION"

    invoke-virtual {v1, v3}, Landroid/hardware/display/DisplayManager;->getDisplays(Ljava/lang/String;)[Landroid/view/Display;

    move-result-object v0

    .line 423
    .local v0, "displays":[Landroid/view/Display;
    array-length v3, v0

    if-lez v3, :cond_0

    .line 424
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 425
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 427
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->releaseWakeLock()V

    .line 429
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 173
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 248
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->TAG:Ljava/lang/String;

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideshowPresentationVideoView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideshowPresentationVideoView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->finishSlideshowPresentationVideoView()V

    .line 253
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    if-eqz v0, :cond_1

    .line 254
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setSlideShowMode(Z)V

    .line 255
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    if-eqz v0, :cond_2

    .line 256
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 257
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentationFocuser:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->unRegisterReceiver(Landroid/content/Context;)V

    .line 258
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->dismiss()V

    .line 259
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    .line 261
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentationReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_3

    .line 262
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentationReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 263
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentationReceiver:Landroid/content/BroadcastReceiver;

    .line 265
    :cond_3
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->releaseWakeLock()V

    .line 266
    invoke-direct {p0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->clearSlideshowMode(Landroid/content/Context;)V

    .line 267
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->resetContent()V

    .line 268
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 269
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 182
    sget-object v3, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->TAG:Ljava/lang/String;

    const-string v4, "onStartCommand"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    const/4 v0, 0x0

    .line 184
    .local v0, "displayId":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    if-eqz v3, :cond_0

    .line 185
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    iput-boolean v6, v3, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->mReleaseResource:Z

    .line 186
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 187
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentationFocuser:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;

    invoke-virtual {v3, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$PresentationFocuser;->unRegisterReceiver(Landroid/content/Context;)V

    .line 188
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->dismiss()V

    .line 192
    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 193
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "key_method"

    const/4 v5, -0x1

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 194
    .local v2, "method":I
    const/16 v3, 0x9

    if-ne v2, v3, :cond_1

    .line 195
    sget-object v3, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "stop"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->stopSelf()V

    .line 216
    .end local v2    # "method":I
    :goto_0
    return v7

    .line 199
    .restart local v2    # "method":I
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "key_display_id"

    invoke-virtual {v3, v4, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 203
    .end local v2    # "method":I
    :cond_2
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->showNotification()V

    .line 204
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->show(I)V

    .line 205
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 206
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v3, "ACTION_PRESENTATION_VIDEO_VIEW"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 207
    const-string v3, "ACTION_REMOVE_PRESENTATION_VIDEO_VIEW"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 208
    const-string v3, "ACTION_ADD_GLSURFACE_VIEW"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 209
    const-string v3, "ACTION_REMOVE_GLSURFACE_VIEW"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 211
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentationReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 212
    new-instance v3, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->getGlSlideShowView()Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;-><init>(Landroid/content/Context;Lcom/sec/samsung/gallery/glview/GlSlideShowView;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideshowPresentationVideoView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    goto :goto_0
.end method
