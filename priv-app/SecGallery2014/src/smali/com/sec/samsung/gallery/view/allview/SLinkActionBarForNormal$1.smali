.class Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal$1;
.super Ljava/lang/Object;
.source "SLinkActionBarForNormal.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 28
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;->access$000(Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030002

    invoke-virtual {v3, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 31
    .local v0, "customView":Landroid/view/View;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 32
    new-instance v2, Landroid/app/ActionBar$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    .line 34
    .local v2, "lp":Landroid/app/ActionBar$LayoutParams;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;->access$100(Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0042

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    iput v3, v2, Landroid/app/ActionBar$LayoutParams;->leftMargin:I

    .line 36
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;->access$200(Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v0, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 40
    .end local v2    # "lp":Landroid/app/ActionBar$LayoutParams;
    :goto_0
    const/16 v1, 0xc

    .line 42
    .local v1, "flags":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;->access$400(Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 43
    return-void

    .line 38
    .end local v1    # "flags":I
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;->access$300(Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/app/ActionBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
