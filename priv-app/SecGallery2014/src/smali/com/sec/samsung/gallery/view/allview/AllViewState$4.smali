.class Lcom/sec/samsung/gallery/view/allview/AllViewState$4;
.super Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;
.source "AllViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/allview/AllViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V
    .locals 0

    .prologue
    .line 242
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$4;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onNext()V
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$4;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1500(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$4;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1600(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/glview/GlPhotoView;

    move-result-object v0

    const/16 v1, -0x1f40

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setAirMotionImageFling(I)V

    .line 253
    :cond_0
    return-void
.end method

.method public onPrevious()V
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$4;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1500(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$4;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1600(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/glview/GlPhotoView;

    move-result-object v0

    const/16 v1, 0x1f40

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setAirMotionImageFling(I)V

    .line 247
    :cond_0
    return-void
.end method
