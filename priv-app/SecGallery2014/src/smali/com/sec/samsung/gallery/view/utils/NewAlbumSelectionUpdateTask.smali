.class public Lcom/sec/samsung/gallery/view/utils/NewAlbumSelectionUpdateTask;
.super Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;
.source "NewAlbumSelectionUpdateTask.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onProgressListener"    # Lcom/sec/android/gallery3d/data/OnProgressListener;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;)V

    .line 21
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 15
    check-cast p1, [Lcom/sec/android/gallery3d/data/MediaSet;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/utils/NewAlbumSelectionUpdateTask;->doInBackground([Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/Void;
    .locals 9
    .param p1, "arg0"    # [Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    const/4 v8, 0x0

    .line 25
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/utils/NewAlbumSelectionUpdateTask;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v6

    .line 26
    .local v6, "selectionProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/utils/NewAlbumSelectionUpdateTask;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getNewAlbumSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    .line 28
    .local v4, "newSelectionProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    aget-object v3, p1, v8

    .line 29
    .local v3, "mediaset":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v7

    invoke-virtual {v3, v8, v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v2

    .line 30
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v5, 0x0

    .line 31
    .local v5, "notCopiedCount":I
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 32
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v6, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 33
    add-int/lit8 v5, v5, 0x1

    .line 34
    invoke-virtual {v4, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_0

    .line 37
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    invoke-virtual {v4, v3, v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;I)V

    .line 38
    const/4 v7, 0x0

    return-object v7
.end method
