.class public Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;
.super Ljava/lang/Object;
.source "GallerySearchItemAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SearchHistory"
.end annotation


# instance fields
.field public mKeyword:Ljava/lang/String;

.field public mTime:J


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;->mKeyword:Ljava/lang/String;

    .line 185
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;->mTime:J

    .line 193
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;->mKeyword:Ljava/lang/String;

    .line 194
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;->mTime:J

    .line 195
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J)V
    .locals 2
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "time"    # J

    .prologue
    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;->mKeyword:Ljava/lang/String;

    .line 185
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;->mTime:J

    .line 188
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;->mKeyword:Ljava/lang/String;

    .line 189
    iput-wide p2, p0, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter$SearchHistory;->mTime:J

    .line 190
    return-void
.end method
