.class public Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;
.source "PhotoActionBarForMultiPick.java"


# instance fields
.field private final SELECT_MAX_ITEM_LIMIT:I

.field private isMWChanged:Z

.field protected mCount:I

.field private mHasConfigChanged:Z

.field private mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

.field private mIsDoneEnabled:Z

.field private mMenu:Landroid/view/Menu;

.field private mUseWidgetLimit:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 5
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 40
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 33
    sget v2, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    iput v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->SELECT_MAX_ITEM_LIMIT:I

    .line 34
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    .line 35
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->isMWChanged:Z

    .line 36
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mHasConfigChanged:Z

    .line 37
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mUseWidgetLimit:Z

    .line 41
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 42
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 44
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0f0018

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 45
    .local v0, "btn":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 47
    .local v1, "lp":Landroid/widget/LinearLayout$LayoutParams;
    if-eqz v1, :cond_0

    .line 48
    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    .line 49
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 52
    .end local v1    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Z)V
    .locals 0
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "isPicker"    # Z

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 56
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;)Landroid/view/Menu;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mMenu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;)Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;)Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->setSelectAllButtonTitle(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->setSelectAllButtonTTS(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mIsDoneEnabled:Z

    return v0
.end method


# virtual methods
.method public onConfigChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "configuration"    # Landroid/content/res/Configuration;

    .prologue
    .line 229
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->onConfigChanged(Landroid/content/res/Configuration;)V

    .line 230
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mHasConfigChanged:Z

    .line 231
    return-void
.end method

.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 11
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v10, 0x7f0f0024

    const v6, 0x7f0f0022

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x2

    .line 109
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-nez v5, :cond_1

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    .line 112
    .local v2, "inflater":Landroid/view/MenuInflater;
    const v5, 0x7f120030

    invoke-virtual {v2, v5, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 114
    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 115
    .local v1, "done":Landroid/view/MenuItem;
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 116
    .local v0, "cancel":Landroid/view/MenuItem;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 117
    invoke-static {p1, v10, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 118
    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 123
    :goto_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 124
    .local v3, "intent":Landroid/content/Intent;
    const-string v5, "help_mode"

    invoke-virtual {v3, v5, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 126
    .local v4, "isHelpMode":Z
    if-eqz v1, :cond_2

    if-eqz v4, :cond_2

    .line 127
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    if-nez v5, :cond_4

    .line 128
    new-instance v5, Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v5, v6, v7}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;-><init>(Landroid/content/Context;I)V

    iput-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    .line 129
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    invoke-virtual {v5, v9}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->show(Z)V

    .line 131
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v6, "helpPickerViewPreference"

    invoke-static {v5, v6, v7}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 140
    :cond_2
    :goto_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 141
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "photo-pick"

    invoke-virtual {v5, v6, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mUseWidgetLimit:Z

    goto :goto_0

    .line 120
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "isHelpMode":Z
    :cond_3
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->setTextOnlyButton(Landroid/view/MenuItem;)V

    .line 121
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->setTextOnlyButton(Landroid/view/MenuItem;)V

    goto :goto_1

    .line 132
    .restart local v3    # "intent":Landroid/content/Intent;
    .restart local v4    # "isHelpMode":Z
    :cond_4
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->isMWChanged:Z

    if-nez v5, :cond_5

    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mHasConfigChanged:Z

    if-eqz v5, :cond_2

    .line 133
    :cond_5
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->hide()V

    .line 134
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->initView()V

    .line 135
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    invoke-virtual {v5, v9}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->show(Z)V

    .line 136
    iput-boolean v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mHasConfigChanged:Z

    goto :goto_2
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 9
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v7, 0x1

    const/4 v6, -0x1

    const/4 v8, 0x0

    .line 172
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 203
    :goto_0
    :pswitch_0
    return-void

    .line 174
    :pswitch_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    if-eqz v4, :cond_0

    .line 175
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->hide()V

    .line 176
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 177
    .local v0, "intent":Landroid/content/Intent;
    const-string v4, "photo-pick"

    invoke-virtual {v0, v4, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "pick-from-gallery"

    invoke-virtual {v0, v4, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_2

    .line 178
    const-string v4, "pick-max-item"

    iget v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->SELECT_MAX_ITEM_LIMIT:I

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 180
    .local v1, "maxPickItems":I
    const-string v4, "photo-pick-gifmaker"

    invoke-virtual {v0, v4, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 181
    const-string v4, "select_num_key"

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 183
    :cond_1
    if-eq v1, v6, :cond_2

    iget v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mCount:I

    if-le v4, v1, :cond_2

    .line 184
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0114

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 185
    .local v3, "text":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 190
    .end local v1    # "maxPickItems":I
    .end local v3    # "text":Ljava/lang/String;
    :cond_2
    const/4 v4, 0x2

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v4, v2, v8

    const/4 v4, 0x0

    aput-object v4, v2, v7

    .line 193
    .local v2, "params":[Ljava/lang/Object;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v4

    const-string v5, "MULTIPLE_PICKER_SELECTION_COMPLETED"

    invoke-virtual {v4, v5, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 197
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v2    # "params":[Ljava/lang/Object;
    :pswitch_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    if-eqz v4, :cond_3

    .line 198
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->hide()V

    .line 199
    :cond_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v4

    const-string v5, "EXIT_SELECTION_MODE"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 200
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->finish()V

    goto/16 :goto_0

    .line 172
    :pswitch_data_0
    .packed-switch 0x7f0f0022
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 235
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->onPause()V

    .line 236
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->hide()V

    .line 238
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v7, 0x7f0f0024

    const/16 v6, 0x3e8

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 150
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mMenu:Landroid/view/Menu;

    .line 151
    const v3, 0x7f0f02cf

    invoke-interface {p1, v3, v4}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 152
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 153
    .local v1, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 154
    .local v0, "count":I
    const-string v3, "pick-min-item"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 156
    .local v2, "minPickCount":I
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mUseWidgetLimit:Z

    if-nez v3, :cond_7

    .line 157
    if-lez v2, :cond_3

    if-lt v0, v2, :cond_2

    move v3, v4

    :goto_0
    invoke-static {p1, v7, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 158
    if-lez v2, :cond_6

    if-lt v0, v2, :cond_5

    :cond_0
    :goto_1
    invoke-static {p1, v7, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemEnabled(Landroid/view/Menu;IZ)V

    .line 164
    :goto_2
    const v3, 0x7f0f0022

    invoke-static {p1, v3, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 165
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 166
    const v3, 0x7f0f0284

    invoke-static {p1, v3, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 168
    :cond_1
    return-void

    :cond_2
    move v3, v5

    .line 157
    goto :goto_0

    :cond_3
    if-lez v0, :cond_4

    move v3, v4

    goto :goto_0

    :cond_4
    move v3, v5

    goto :goto_0

    :cond_5
    move v4, v5

    .line 158
    goto :goto_1

    :cond_6
    if-gtz v0, :cond_0

    move v4, v5

    goto :goto_1

    .line 160
    :cond_7
    if-lez v2, :cond_a

    if-gt v0, v6, :cond_9

    if-lt v0, v2, :cond_9

    move v3, v4

    :goto_3
    invoke-static {p1, v7, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 161
    if-lez v2, :cond_d

    if-gt v0, v6, :cond_c

    if-lt v0, v2, :cond_c

    :cond_8
    :goto_4
    invoke-static {p1, v7, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemEnabled(Landroid/view/Menu;IZ)V

    goto :goto_2

    :cond_9
    move v3, v5

    .line 160
    goto :goto_3

    :cond_a
    if-gt v0, v6, :cond_b

    if-lez v0, :cond_b

    move v3, v4

    goto :goto_3

    :cond_b
    move v3, v5

    goto :goto_3

    :cond_c
    move v4, v5

    .line 161
    goto :goto_4

    :cond_d
    if-gt v0, v6, :cond_e

    if-gtz v0, :cond_8

    :cond_e
    move v4, v5

    goto :goto_4
.end method

.method public onResume()V
    .locals 6

    .prologue
    const v5, 0x7f0f0024

    const v4, 0x7f0f0022

    const/4 v3, 0x2

    .line 207
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    if-eqz v2, :cond_0

    .line 208
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->refreshLayout()V

    .line 210
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->setupButtons()V

    .line 211
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->setTitle(I)V

    .line 213
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mMenu:Landroid/view/Menu;

    if-eqz v2, :cond_1

    .line 214
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mMenu:Landroid/view/Menu;

    invoke-interface {v2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 215
    .local v1, "done":Landroid/view/MenuItem;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mMenu:Landroid/view/Menu;

    invoke-interface {v2, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 216
    .local v0, "cancel":Landroid/view/MenuItem;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 217
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mMenu:Landroid/view/Menu;

    invoke-static {v2, v5, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 218
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mMenu:Landroid/view/Menu;

    invoke-static {v2, v4, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 224
    .end local v0    # "cancel":Landroid/view/MenuItem;
    .end local v1    # "done":Landroid/view/MenuItem;
    :cond_1
    :goto_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->onResume()V

    .line 225
    return-void

    .line 220
    .restart local v0    # "cancel":Landroid/view/MenuItem;
    .restart local v1    # "done":Landroid/view/MenuItem;
    :cond_2
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->setTextOnlyButton(Landroid/view/MenuItem;)V

    .line 221
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->setTextOnlyButton(Landroid/view/MenuItem;)V

    goto :goto_0
.end method

.method public setMWChanged(Z)V
    .locals 0
    .param p1, "mMWChanged"    # Z

    .prologue
    .line 145
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->isMWChanged:Z

    .line 146
    return-void
.end method

.method public setTitle(I)V
    .locals 2
    .param p1, "numberOfItemsSelected"    # I

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$1;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 95
    return-void
.end method

.method protected updateDoneButton(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 98
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mIsDoneEnabled:Z

    .line 99
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick$2;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 105
    return-void
.end method
