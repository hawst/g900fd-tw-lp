.class public Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
.super Lcom/sec/android/gallery3d/glcore/GlObject;
.source "GlComposeObject.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$FadeAnimCallBack;,
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;,
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;,
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$UnDimAnimation;,
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$DimAnimation;,
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;
    }
.end annotation


# static fields
.field private static final DEF_DIM_DURATION:I = 0x1f4

.field public static final OPT_NOT_TOUCHABLE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "GlComposeObject"


# instance fields
.field private mAnimationTouch:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;

.field public mAppearAnimScaling:Z

.field public mDefRoll:F

.field private mDimAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$DimAnimation;

.field public mDimDuration:I

.field private mFadeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;

.field public mFocusBorderVisible:Z

.field public mFocused:Z

.field public mHndDim:I

.field public mHndDispMode:I

.field public mHndDispmnt:I

.field public mHoverLabelIndex:I

.field public mIndex:I

.field public mObjective:I

.field public mOption:I

.field public mPosAnimDisplacement:Z

.field public mRotation:I

.field public mSAlpha:F

.field public mSrcH:F

.field public mSrcRoll:F

.field public mSrcScale:F

.field public mSrcW:F

.field public mStartEndEffect:Z

.field public mTAlpha:F

.field public mTgtH:F

.field public mTgtRoll:F

.field public mTgtScale:F

.field public mTgtW:F

.field public mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

.field public mTransformAnimAlpha:Z

.field public mTransformAnimPos:Z

.field public mTransformAnimResizing:Z

.field public mTransformAnimRoll:Z

.field public mTransformAnimScaling:Z

.field private mUnDimAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$UnDimAnimation;

.field public mUseTouchEvent:Z

.field protected mlistenerDelete:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

.field public msx:F

.field public msy:F

.field public msz:F

.field public mtx:F

.field public mty:F

.field public mtz:F


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 5
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    const/4 v4, -0x1

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 62
    invoke-direct {p0, p1, v0, v0}, Lcom/sec/android/gallery3d/glcore/GlObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 19
    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDispmnt:I

    .line 20
    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDispMode:I

    .line 21
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDim:I

    .line 22
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mObjective:I

    .line 23
    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 25
    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSAlpha:F

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTAlpha:F

    .line 30
    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcScale:F

    .line 31
    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTgtScale:F

    .line 32
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcRoll:F

    .line 33
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTgtRoll:F

    .line 34
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mDefRoll:F

    .line 35
    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHoverLabelIndex:I

    .line 37
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAppearAnimScaling:Z

    .line 38
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFocusBorderVisible:Z

    .line 39
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFadeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;

    .line 45
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mDimDuration:I

    .line 46
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFocused:Z

    .line 47
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mUseTouchEvent:Z

    .line 48
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimPos:Z

    .line 49
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimResizing:Z

    .line 50
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimAlpha:Z

    .line 51
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimRoll:Z

    .line 52
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimScaling:Z

    .line 53
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mPosAnimDisplacement:Z

    .line 54
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mStartEndEffect:Z

    .line 585
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$1;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mlistenerDelete:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 63
    return-void
.end method

.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;I)V
    .locals 5
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "option"    # I

    .prologue
    const/4 v4, -0x1

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 66
    invoke-direct {p0, p1, v0, v0}, Lcom/sec/android/gallery3d/glcore/GlObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 19
    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDispmnt:I

    .line 20
    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDispMode:I

    .line 21
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDim:I

    .line 22
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mObjective:I

    .line 23
    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 25
    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSAlpha:F

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTAlpha:F

    .line 30
    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcScale:F

    .line 31
    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTgtScale:F

    .line 32
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcRoll:F

    .line 33
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTgtRoll:F

    .line 34
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mDefRoll:F

    .line 35
    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHoverLabelIndex:I

    .line 37
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAppearAnimScaling:Z

    .line 38
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFocusBorderVisible:Z

    .line 39
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFadeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;

    .line 45
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mDimDuration:I

    .line 46
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFocused:Z

    .line 47
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mUseTouchEvent:Z

    .line 48
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimPos:Z

    .line 49
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimResizing:Z

    .line 50
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimAlpha:Z

    .line 51
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimRoll:Z

    .line 52
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimScaling:Z

    .line 53
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mPosAnimDisplacement:Z

    .line 54
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mStartEndEffect:Z

    .line 585
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$1;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mlistenerDelete:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 67
    iput p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mOption:I

    .line 68
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mOption:I

    and-int/lit8 v0, v0, 0x1

    if-lez v0, :cond_0

    .line 69
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mUseTouchEvent:Z

    .line 71
    :cond_0
    return-void
.end method

.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;II)V
    .locals 5
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v4, -0x1

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 57
    invoke-direct {p0, p1, v0, v0}, Lcom/sec/android/gallery3d/glcore/GlObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 19
    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDispmnt:I

    .line 20
    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDispMode:I

    .line 21
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDim:I

    .line 22
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mObjective:I

    .line 23
    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 25
    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSAlpha:F

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTAlpha:F

    .line 30
    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcScale:F

    .line 31
    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTgtScale:F

    .line 32
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcRoll:F

    .line 33
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTgtRoll:F

    .line 34
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mDefRoll:F

    .line 35
    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHoverLabelIndex:I

    .line 37
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAppearAnimScaling:Z

    .line 38
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFocusBorderVisible:Z

    .line 39
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFadeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;

    .line 45
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mDimDuration:I

    .line 46
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFocused:Z

    .line 47
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mUseTouchEvent:Z

    .line 48
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimPos:Z

    .line 49
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimResizing:Z

    .line 50
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimAlpha:Z

    .line 51
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimRoll:Z

    .line 52
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimScaling:Z

    .line 53
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mPosAnimDisplacement:Z

    .line 54
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mStartEndEffect:Z

    .line 585
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$1;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mlistenerDelete:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 58
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v1, p1, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v0, v1, p2, p3}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 59
    return-void
.end method


# virtual methods
.method public applyFadeAnimation(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$FadeAnimCallBack;)V
    .locals 5
    .param p1, "callBack"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$FadeAnimCallBack;

    .prologue
    .line 133
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFadeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;

    if-eqz v1, :cond_0

    .line 142
    :goto_0
    return-void

    .line 136
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v0

    .line 137
    .local v0, "canvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->getHeight()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvasSub(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 138
    new-instance v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$FadeAnimCallBack;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFadeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;

    .line 139
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFadeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;

    const-wide/32 v2, 0x989680

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->setDuration(J)V

    .line 140
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFadeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 141
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFadeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->startAfter(J)V

    goto :goto_0
.end method

.method protected cancelAnimation()V
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;

    if-eqz v0, :cond_0

    .line 411
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->cancelAnim()V

    .line 412
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;

    .line 414
    :cond_0
    return-void
.end method

.method public dim()V
    .locals 4

    .prologue
    .line 281
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mDimAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$DimAnimation;

    if-nez v0, :cond_0

    .line 282
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$DimAnimation;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$DimAnimation;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mDimAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$DimAnimation;

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mDimAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$DimAnimation;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 285
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mDimAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$DimAnimation;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mDimDuration:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$DimAnimation;->setDuration(J)V

    .line 286
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mDimAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$DimAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$DimAnimation;->start()V

    .line 287
    return-void
.end method

.method public getFocusBorderVisible()Z
    .locals 1

    .prologue
    .line 607
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFocusBorderVisible:Z

    return v0
.end method

.method public getTransAnim()Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    return-object v0
.end method

.method public initTouchAnimation()V
    .locals 4

    .prologue
    .line 126
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;

    .line 127
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;

    const-wide/16 v2, 0x50

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->setDuration(J)V

    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;

    new-instance v1, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;-><init>()V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->setInterpolator(Lcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    .line 129
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 130
    return-void
.end method

.method public isInclude(FF)Z
    .locals 10
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/4 v6, 0x1

    .line 168
    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getWidth(Z)F

    move-result v5

    .line 169
    .local v5, "w":F
    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getHeight(Z)F

    move-result v2

    .line 170
    .local v2, "h":F
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsX()F

    move-result v7

    div-float v8, v5, v9

    sub-float v3, v7, v8

    .line 171
    .local v3, "sx":F
    add-float v0, v3, v5

    .line 172
    .local v0, "ex":F
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsY()F

    move-result v7

    div-float v8, v2, v9

    add-float v4, v7, v8

    .line 173
    .local v4, "sy":F
    sub-float v1, v4, v2

    .line 174
    .local v1, "ey":F
    cmpg-float v7, v3, p1

    if-gtz v7, :cond_0

    cmpg-float v7, p1, v0

    if-gtz v7, :cond_0

    cmpl-float v7, v4, p2

    if-ltz v7, :cond_0

    cmpl-float v7, p2, v1

    if-ltz v7, :cond_0

    .line 176
    :goto_0
    return v6

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public isSetViewToSub()Z
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFadeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;

    if-nez v0, :cond_0

    .line 160
    const/4 v0, 0x0

    .line 162
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFadeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->mFirstFade:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->access$000(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onCancelAnimation()V
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;

    if-eqz v0, :cond_0

    .line 404
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->onStop()V

    .line 405
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;

    .line 407
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 74
    invoke-super {p0}, Lcom/sec/android/gallery3d/glcore/GlObject;->onDestroy()V

    .line 75
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;

    .line 76
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;->mManualRecycle:Z

    if-nez v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->recycle()V

    .line 79
    :cond_0
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 80
    return-void
.end method

.method protected onMoved(II)Z
    .locals 2
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    const/16 v1, 0x2d

    .line 94
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mUseTouchEvent:Z

    if-nez v0, :cond_0

    .line 95
    const/4 v0, 0x0

    .line 102
    :goto_0
    return v0

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;

    if-eqz v0, :cond_2

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-gt v0, v1, :cond_1

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-le v0, v1, :cond_2

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->cancelAnim()V

    .line 102
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onPressed(II)Z
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mUseTouchEvent:Z

    if-nez v0, :cond_0

    .line 84
    const/4 v0, 0x0

    .line 90
    :goto_0
    return v0

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;

    if-nez v0, :cond_1

    .line 87
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->initTouchAnimation()V

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->startPressAnim()V

    .line 90
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onReleased(IIII)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "speedX"    # I
    .param p4, "speedY"    # I

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mUseTouchEvent:Z

    if-nez v0, :cond_0

    .line 107
    const/4 v0, 0x0

    .line 111
    :goto_0
    return v0

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;

    if-eqz v0, :cond_1

    .line 110
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->cancelAnim()V

    .line 111
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public remove()V
    .locals 0

    .prologue
    .line 273
    invoke-super {p0}, Lcom/sec/android/gallery3d/glcore/GlObject;->remove()V

    .line 274
    return-void
.end method

.method public removeFadeAnimation()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 145
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFadeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;

    if-nez v0, :cond_1

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFadeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 149
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFadeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;

    .line 150
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    if-eqz v0, :cond_2

    .line 151
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvasSub(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 153
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mGlViewSub:Lcom/sec/android/gallery3d/glcore/GlView;

    if-eqz v0, :cond_0

    .line 154
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mGlViewSub:Lcom/sec/android/gallery3d/glcore/GlView;

    goto :goto_0
.end method

.method public resetDim()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 302
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDim:I

    if-nez v0, :cond_1

    .line 305
    :cond_0
    :goto_0
    return-void

    .line 303
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDim:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getDim(I)F

    move-result v0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 304
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setDimEx(F)V

    goto :goto_0
.end method

.method public resetTransformAnimation()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 188
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimPos:Z

    .line 189
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimResizing:Z

    .line 190
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimAlpha:Z

    .line 191
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimRoll:Z

    .line 192
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimScaling:Z

    .line 194
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSAlpha:F

    .line 195
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTAlpha:F

    .line 196
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcScale:F

    .line 197
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTgtScale:F

    .line 198
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcRoll:F

    .line 199
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTgtRoll:F

    .line 200
    return-void
.end method

.method public setCurrentDeltaToSource()V
    .locals 1

    .prologue
    .line 266
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDispMode:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getDX(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->msx:F

    .line 267
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDispMode:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getDY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->msy:F

    .line 268
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDispMode:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getDZ(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->msz:F

    .line 269
    return-void
.end method

.method public setCurrentPosToSource()V
    .locals 1

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->msx:F

    .line 250
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->msy:F

    .line 251
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getZ()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->msz:F

    .line 252
    return-void
.end method

.method public setCurrentPosToTarget()V
    .locals 1

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mtx:F

    .line 256
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mty:F

    .line 257
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getZ()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mtz:F

    .line 258
    return-void
.end method

.method public setCurrentSizeToSource()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 261
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getWidth(Z)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcW:F

    .line 262
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getHeight(Z)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcH:F

    .line 263
    return-void
.end method

.method public setDimDuration(I)V
    .locals 0
    .param p1, "duration"    # I

    .prologue
    .line 382
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mDimDuration:I

    .line 383
    return-void
.end method

.method public setDimEx(F)V
    .locals 1
    .param p1, "dim"    # F

    .prologue
    .line 317
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDim:I

    if-nez v0, :cond_0

    .line 318
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getDimHnd()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDim:I

    .line 319
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDim:I

    invoke-virtual {p0, p1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setDim(FI)V

    .line 320
    return-void
.end method

.method public setEnableAlphaAnim(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 370
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimAlpha:Z

    .line 371
    return-void
.end method

.method public setEnableAnim(ZZZZZ)V
    .locals 0
    .param p1, "animPos"    # Z
    .param p2, "animResize"    # Z
    .param p3, "animAlpha"    # Z
    .param p4, "animRoll"    # Z
    .param p5, "animScale"    # Z

    .prologue
    .line 354
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimPos:Z

    .line 355
    iput-boolean p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimResizing:Z

    .line 356
    iput-boolean p3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimAlpha:Z

    .line 357
    iput-boolean p4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimRoll:Z

    .line 358
    iput-boolean p5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimScaling:Z

    .line 359
    return-void
.end method

.method public setEnablePosAnim(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 362
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimPos:Z

    .line 363
    return-void
.end method

.method public setEnableResizeAnim(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 366
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimResizing:Z

    .line 367
    return-void
.end method

.method public setEnableRollAnim(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 374
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimRoll:Z

    .line 375
    return-void
.end method

.method public setEnableScaleAnim(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 378
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimScaling:Z

    .line 379
    return-void
.end method

.method public setFocusBorderVisible(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 603
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFocusBorderVisible:Z

    .line 604
    return-void
.end method

.method public setFocused(Z)V
    .locals 1
    .param p1, "focused"    # Z

    .prologue
    .line 323
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFocused:Z

    if-ne v0, p1, :cond_0

    .line 334
    :goto_0
    return-void

    .line 325
    :cond_0
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mFocused:Z

    .line 326
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;

    if-nez v0, :cond_1

    .line 327
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->initTouchAnimation()V

    .line 329
    :cond_1
    if-eqz p1, :cond_2

    .line 330
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->startFocusAnim()V

    goto :goto_0

    .line 332
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mAnimationTouch:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->cancelAnim()V

    goto :goto_0
.end method

.method public setObjective(I)V
    .locals 0
    .param p1, "objective"    # I

    .prologue
    .line 180
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mObjective:I

    .line 181
    return-void
.end method

.method public setPosAnimMode(Z)V
    .locals 2
    .param p1, "enableDisplacement"    # Z

    .prologue
    const/4 v1, -0x1

    .line 387
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mPosAnimDisplacement:Z

    if-ne v0, p1, :cond_1

    .line 400
    :cond_0
    :goto_0
    return-void

    .line 389
    :cond_1
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mPosAnimDisplacement:Z

    .line 390
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mPosAnimDisplacement:Z

    if-eqz v0, :cond_2

    .line 391
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDispMode:I

    if-ne v0, v1, :cond_0

    .line 392
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPosIndex()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDispMode:I

    goto :goto_0

    .line 395
    :cond_2
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDispMode:I

    if-eq v0, v1, :cond_0

    .line 396
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDispMode:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->releasePosIndex(I)V

    .line 397
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDispMode:I

    goto :goto_0
.end method

.method public setSourceAlpha(F)V
    .locals 0
    .param p1, "alpha"    # F

    .prologue
    .line 225
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSAlpha:F

    .line 226
    return-void
.end method

.method public setSourcePos(FFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 213
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->msx:F

    .line 214
    iput p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->msy:F

    .line 215
    iput p3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->msz:F

    .line 216
    return-void
.end method

.method public setSourceRoll(F)V
    .locals 0
    .param p1, "roll"    # F

    .prologue
    .line 233
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcRoll:F

    .line 234
    return-void
.end method

.method public setSourceScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 241
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcScale:F

    .line 242
    return-void
.end method

.method public setSourceSize(FF)V
    .locals 0
    .param p1, "width"    # F
    .param p2, "height"    # F

    .prologue
    .line 203
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcW:F

    .line 204
    iput p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcH:F

    .line 205
    return-void
.end method

.method public setStartedEndEffect(Z)V
    .locals 0
    .param p1, "start"    # Z

    .prologue
    .line 595
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mStartEndEffect:Z

    .line 596
    return-void
.end method

.method public setTargetAlpha(F)V
    .locals 0
    .param p1, "alpha"    # F

    .prologue
    .line 229
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTAlpha:F

    .line 230
    return-void
.end method

.method public setTargetPos(FFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 219
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mtx:F

    .line 220
    iput p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mty:F

    .line 221
    iput p3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mtz:F

    .line 222
    return-void
.end method

.method public setTargetRoll(F)V
    .locals 0
    .param p1, "roll"    # F

    .prologue
    .line 237
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTgtRoll:F

    .line 238
    return-void
.end method

.method public setTargetScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 245
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTgtScale:F

    .line 246
    return-void
.end method

.method public setTargetSize(FF)V
    .locals 0
    .param p1, "width"    # F
    .param p2, "height"    # F

    .prologue
    .line 208
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTgtW:F

    .line 209
    iput p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTgtH:F

    .line 210
    return-void
.end method

.method public setUseTouchEvent(Z)V
    .locals 0
    .param p1, "use"    # Z

    .prologue
    .line 277
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mUseTouchEvent:Z

    .line 278
    return-void
.end method

.method public startTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;JJLcom/sec/android/gallery3d/glcore/GlInterpolator;)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;
    .param p2, "duration"    # J
    .param p4, "after"    # J
    .param p6, "interpolator"    # Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    .prologue
    .line 346
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 347
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 348
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    invoke-virtual {v0, p6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->setInterpolator(Lcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    .line 349
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    invoke-virtual {v0, p2, p3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->setDuration(J)V

    .line 350
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    invoke-virtual {v0, p4, p5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->startAfter(J)V

    .line 351
    return-void
.end method

.method public stopDim()V
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mDimAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$DimAnimation;

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mDimAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$DimAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$DimAnimation;->stop()V

    .line 311
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mUnDimAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$UnDimAnimation;

    if-eqz v0, :cond_1

    .line 312
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mUnDimAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$UnDimAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$UnDimAnimation;->stop()V

    .line 314
    :cond_1
    return-void
.end method

.method public stopTransAnim(Z)V
    .locals 2
    .param p1, "applyFinalRatio"    # Z

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 337
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 343
    :goto_0
    return-void

    .line 339
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->getLastRatio()F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    .line 340
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->applyTransform(F)V

    .line 342
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->stop()V

    goto :goto_0
.end method

.method public undim()V
    .locals 4

    .prologue
    .line 290
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDim:I

    if-nez v0, :cond_1

    .line 299
    :cond_0
    :goto_0
    return-void

    .line 291
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mUnDimAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$UnDimAnimation;

    if-nez v0, :cond_2

    .line 292
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$UnDimAnimation;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$UnDimAnimation;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mUnDimAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$UnDimAnimation;

    .line 294
    :cond_2
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDim:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getDim(I)F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mUnDimAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$UnDimAnimation;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 296
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mUnDimAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$UnDimAnimation;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mDimDuration:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$UnDimAnimation;->setDuration(J)V

    .line 297
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mUnDimAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$UnDimAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$UnDimAnimation;->start()V

    goto :goto_0
.end method

.method public updateCanvas(II)V
    .locals 3
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v0

    .line 116
    .local v0, "canvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->getWidth()I

    move-result v1

    if-ne v1, p1, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->getHeight()I

    move-result v1

    if-ne v1, p2, :cond_1

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v1, v2, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 120
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mGlCanvasSub:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    if-eqz v1, :cond_0

    .line 121
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v1, v2, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvasSub(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    goto :goto_0
.end method
