.class public Lcom/sec/samsung/gallery/controller/CopyToEventCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "CopyToEventCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# instance fields
.field private mAlbumListDialog:Lcom/sec/samsung/gallery/view/common/EventAlbumChoiceDialog;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method private dismissAlbumListDialog()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CopyToEventCmd;->mAlbumListDialog:Lcom/sec/samsung/gallery/view/common/EventAlbumChoiceDialog;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CopyToEventCmd;->mAlbumListDialog:Lcom/sec/samsung/gallery/view/common/EventAlbumChoiceDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/EventAlbumChoiceDialog;->dismissDialog()V

    .line 67
    :cond_0
    return-void
.end method

.method private showAlbumListDialog(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "selectionManager"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    .line 58
    const v0, 0x7f0e0446

    .line 59
    .local v0, "title":I
    new-instance v1, Lcom/sec/samsung/gallery/view/common/EventAlbumChoiceDialog;

    invoke-direct {v1, p1, p2}, Lcom/sec/samsung/gallery/view/common/EventAlbumChoiceDialog;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/CopyToEventCmd;->mAlbumListDialog:Lcom/sec/samsung/gallery/view/common/EventAlbumChoiceDialog;

    .line 60
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/CopyToEventCmd;->mAlbumListDialog:Lcom/sec/samsung/gallery/view/common/EventAlbumChoiceDialog;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/view/common/EventAlbumChoiceDialog;->setTitle(I)V

    .line 61
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/CopyToEventCmd;->mAlbumListDialog:Lcom/sec/samsung/gallery/view/common/EventAlbumChoiceDialog;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/EventAlbumChoiceDialog;->showDialog()V

    .line 62
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 10
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 28
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/Object;

    move-object v4, v8

    check-cast v4, [Ljava/lang/Object;

    .line 29
    .local v4, "params":[Ljava/lang/Object;
    const/4 v8, 0x0

    aget-object v8, v4, v8

    check-cast v8, Landroid/content/Context;

    iput-object v8, p0, Lcom/sec/samsung/gallery/controller/CopyToEventCmd;->mContext:Landroid/content/Context;

    .line 30
    const/4 v8, 0x1

    aget-object v6, v4, v8

    check-cast v6, Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 31
    .local v6, "selectionManager":Lcom/sec/android/gallery3d/ui/SelectionManager;
    const/4 v8, 0x2

    aget-object v8, v4, v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 33
    .local v7, "show":Z
    const/4 v1, 0x1

    .line 34
    .local v1, "mCheckEvent":Z
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v5

    .line 36
    .local v5, "selectedMediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v0, v2, :cond_2

    .line 37
    invoke-virtual {v5, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 38
    .local v3, "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v3, :cond_0

    instance-of v8, v3, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v8, :cond_1

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local v3    # "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 39
    :cond_0
    const/4 v1, 0x0

    .line 36
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 44
    :cond_2
    if-nez v1, :cond_3

    if-eqz v7, :cond_3

    .line 45
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/CopyToEventCmd;->mContext:Landroid/content/Context;

    const v9, 0x7f0e0107

    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 55
    :goto_1
    return-void

    .line 49
    :cond_3
    if-nez v7, :cond_4

    .line 50
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/CopyToEventCmd;->dismissAlbumListDialog()V

    goto :goto_1

    .line 54
    :cond_4
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/CopyToEventCmd;->mContext:Landroid/content/Context;

    invoke-direct {p0, v8, v6}, Lcom/sec/samsung/gallery/controller/CopyToEventCmd;->showAlbumListDialog(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    goto :goto_1
.end method
