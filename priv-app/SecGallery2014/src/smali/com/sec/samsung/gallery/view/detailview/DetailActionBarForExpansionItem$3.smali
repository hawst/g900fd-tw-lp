.class Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;
.super Ljava/lang/Object;
.source "DetailActionBarForExpansionItem.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->setTitle(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

.field final synthetic val$countOfMediaItem:I

.field final synthetic val$numberOfItemsSelected:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;II)V
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    iput p2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->val$numberOfItemsSelected:I

    iput p3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->val$countOfMediaItem:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 247
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$3500(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$3600(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->isFinishing()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 278
    :goto_0
    return-void

    .line 249
    :cond_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$3700(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 250
    .local v7, "res":Landroid/content/res/Resources;
    const/4 v4, 0x0

    .line 251
    .local v4, "numberOfSelectedItems":Ljava/lang/String;
    const/4 v1, 0x0

    .line 252
    .local v1, "contentDescription":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->getMaxCount()I

    move-result v2

    .line 253
    .local v2, "maxCount":I
    if-lez v2, :cond_4

    .line 254
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$3800(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isWQHD(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v8

    if-nez v8, :cond_1

    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionOnActionBar:Z

    if-eqz v8, :cond_3

    :cond_1
    const v5, 0x7f0e0051

    .line 256
    .local v5, "numberOfSelectedItemsStringID":I
    :goto_1
    invoke-virtual {v7, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-array v9, v13, [Ljava/lang/Object;

    iget v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->val$numberOfItemsSelected:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 257
    const v8, 0x7f0e0052

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-array v9, v13, [Ljava/lang/Object;

    iget v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->val$numberOfItemsSelected:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 268
    .end local v5    # "numberOfSelectedItemsStringID":I
    :cond_2
    :goto_2
    iget v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->val$countOfMediaItem:I

    const/4 v9, -0x1

    if-ne v8, v9, :cond_6

    .line 269
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$3900(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSourceMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v6

    .line 270
    .local v6, "oriMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    .line 271
    .local v0, "albumCount":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$4000(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v9

    invoke-virtual {v9, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSetSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v9

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->setSelectAllButtonTitle(IILjava/lang/String;)V
    invoke-static {v8, v9, v0, v4}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$4100(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;IILjava/lang/String;)V

    .line 275
    .end local v0    # "albumCount":I
    .end local v6    # "oriMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_3
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->setSelectAllButtonTTS(Ljava/lang/String;)V
    invoke-static {v8, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$4300(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;Ljava/lang/String;)V

    .line 276
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$4400(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSourceMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    .line 277
    .local v3, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->updateSelectionButton(Lcom/sec/android/gallery3d/data/MediaSet;Z)V
    invoke-static {v8, v3, v11}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;Lcom/sec/android/gallery3d/data/MediaSet;Z)V

    goto/16 :goto_0

    .line 254
    .end local v3    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_3
    const v5, 0x7f0e0050

    goto :goto_1

    .line 259
    :cond_4
    iget v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->val$numberOfItemsSelected:I

    if-ltz v8, :cond_2

    .line 260
    iget v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->val$numberOfItemsSelected:I

    if-ne v8, v12, :cond_5

    .line 261
    const v8, 0x7f0e004f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-array v9, v12, [Ljava/lang/Object;

    iget v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->val$numberOfItemsSelected:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 265
    :goto_4
    move-object v1, v4

    goto :goto_2

    .line 263
    :cond_5
    const v8, 0x7f0e004e

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-array v9, v12, [Ljava/lang/Object;

    iget v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->val$numberOfItemsSelected:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_4

    .line 273
    :cond_6
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    iget v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->val$numberOfItemsSelected:I

    iget v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$3;->val$countOfMediaItem:I

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->setSelectAllButtonTitle(IILjava/lang/String;)V
    invoke-static {v8, v9, v10, v4}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$4200(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;IILjava/lang/String;)V

    goto :goto_3
.end method
