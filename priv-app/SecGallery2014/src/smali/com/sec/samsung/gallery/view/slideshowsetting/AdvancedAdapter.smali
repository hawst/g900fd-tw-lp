.class public Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;
.super Landroid/widget/ArrayAdapter;
.source "AdvancedAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/CharSequence;",
        ">;"
    }
.end annotation


# static fields
.field public static final GROUP_LEVEL:I = 0x2710

.field private static final TAG:Ljava/lang/String;

.field private static final TAG_CHILD_VIEW:I = 0x2

.field private static final TAG_GROUP_VIEW:I = 0x1


# instance fields
.field private mAdvancedGroups:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 160
    const-class v0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0, p1, v0, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mAdvancedGroups:Ljava/util/ArrayList;

    .line 29
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mContext:Landroid/content/Context;

    .line 30
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->initGroups()V

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private initGroups()V
    .locals 4

    .prologue
    .line 34
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;

    const/4 v1, 0x4

    const v2, 0x7f0e0179

    const v3, 0x7f090095

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;-><init>(Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;III)V

    .line 36
    .local v0, "sortsGroup":Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mAdvancedGroups:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    return-void
.end method

.method private wasChildView(Landroid/view/View;)Z
    .locals 4
    .param p1, "convertView"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 82
    .local v0, "tag":Ljava/lang/Object;
    if-nez v0, :cond_1

    .line 86
    .end local v0    # "tag":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v1

    .restart local v0    # "tag":Ljava/lang/Object;
    :cond_1
    check-cast v0, Ljava/lang/Integer;

    .end local v0    # "tag":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private wasGroupNameView(Landroid/view/View;)Z
    .locals 4
    .param p1, "convertView"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 72
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 73
    .local v0, "tag":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 77
    .end local v0    # "tag":Ljava/lang/Object;
    :goto_0
    return v2

    .restart local v0    # "tag":Ljava/lang/Object;
    :cond_0
    check-cast v0, Ljava/lang/Integer;

    .end local v0    # "tag":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, v1, :cond_1

    :goto_1
    move v2, v1

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method


# virtual methods
.method public getChildPosition(I)I
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 123
    const/4 v0, 0x0

    .line 124
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mAdvancedGroups:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 125
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mAdvancedGroups:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 126
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mAdvancedGroups:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-eq v1, v2, :cond_1

    .line 127
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->getCount()I

    move-result v2

    sub-int v3, v2, v0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mAdvancedGroups:Ljava/util/ArrayList;

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    if-gt v3, v2, :cond_1

    .line 132
    :cond_0
    sub-int v2, p1, v0

    if-ltz v2, :cond_2

    .line 133
    sub-int v2, p1, v0

    add-int/lit8 v2, v2, -0x1

    .line 135
    :goto_1
    return v2

    .line 124
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 135
    :cond_2
    add-int/lit8 v2, p1, -0x1

    goto :goto_1
.end method

.method public getCount()I
    .locals 3

    .prologue
    .line 140
    const/4 v0, 0x0

    .line 141
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mAdvancedGroups:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 142
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mAdvancedGroups:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;->getCount()I

    move-result v2

    add-int/2addr v0, v2

    .line 141
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 143
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mAdvancedGroups:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/2addr v0, v2

    .line 144
    return v0
.end method

.method public getGroup(I)Ljava/lang/Object;
    .locals 1
    .param p1, "groupPosition"    # I

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mAdvancedGroups:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getGroupPosition(I)I
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 106
    const/4 v2, 0x0

    .local v2, "index":I
    const/4 v0, 0x0

    .line 107
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mAdvancedGroups:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 108
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mAdvancedGroups:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 109
    add-int/lit8 v2, v2, 0x1

    .line 110
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mAdvancedGroups:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-eq v1, v3, :cond_1

    .line 111
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->getCount()I

    move-result v3

    sub-int v4, v3, v0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mAdvancedGroups:Ljava/util/ArrayList;

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    if-gt v4, v3, :cond_1

    .line 116
    :cond_0
    sub-int v3, p1, v0

    if-ltz v3, :cond_2

    .line 119
    .end local v2    # "index":I
    :goto_1
    return v2

    .line 107
    .restart local v2    # "index":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 119
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 41
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->isGroupNameView(I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 42
    if-eqz p2, :cond_0

    invoke-direct {p0, p2}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->wasGroupNameView(Landroid/view/View;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 43
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v6, 0x7f03007d

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 46
    :cond_1
    const v4, 0x7f0f0165

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 47
    .local v3, "groupTextView":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mAdvancedGroups:Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->getGroupPosition(I)I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 49
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p2, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v2, p2

    .line 67
    .end local v3    # "groupTextView":Landroid/widget/TextView;
    .end local p2    # "convertView":Landroid/view/View;
    .local v2, "convertView":Landroid/view/View;
    :goto_0
    return-object v2

    .line 52
    .end local v2    # "convertView":Landroid/view/View;
    .restart local p2    # "convertView":Landroid/view/View;
    :cond_2
    if-eqz p2, :cond_3

    invoke-direct {p0, p2}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->wasChildView(Landroid/view/View;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 53
    :cond_3
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f0300d9

    invoke-virtual {v6, v7, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 56
    :cond_4
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mAdvancedGroups:Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->getGroupPosition(I)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;

    .line 58
    .local v0, "advancedGroup":Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;
    const v6, 0x7f0f0217

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckedTextView;

    .line 59
    .local v1, "checkedTextView":Landroid/widget/CheckedTextView;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->getChildPosition(I)I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;->get(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;->getIntSetting()I

    move-result v6

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->getChildPosition(I)I

    move-result v7

    if-ne v6, v7, :cond_5

    move v4, v5

    :cond_5
    invoke-virtual {v1, v4}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 66
    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p2, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v2, p2

    .line 67
    .end local p2    # "convertView":Landroid/view/View;
    .restart local v2    # "convertView":Landroid/view/View;
    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 149
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->isGroupNameView(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    const/4 v0, 0x0

    .line 152
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isGroupNameView(I)Z
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 90
    const/4 v0, 0x0

    .line 91
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mAdvancedGroups:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 92
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mAdvancedGroups:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 93
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mAdvancedGroups:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-eq v1, v2, :cond_1

    .line 94
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->getCount()I

    move-result v2

    sub-int v3, v2, v0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter;->mAdvancedGroups:Ljava/util/ArrayList;

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/AdvancedAdapter$AdvancedGroup;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    if-gt v3, v2, :cond_1

    .line 99
    :cond_0
    sub-int v2, p1, v0

    if-eqz v2, :cond_2

    if-eqz p1, :cond_2

    .line 100
    const/4 v2, 0x0

    .line 102
    :goto_1
    return v2

    .line 91
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 102
    :cond_2
    const/4 v2, 0x1

    goto :goto_1
.end method
