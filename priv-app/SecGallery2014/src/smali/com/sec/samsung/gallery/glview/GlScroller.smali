.class public Lcom/sec/samsung/gallery/glview/GlScroller;
.super Ljava/lang/Object;
.source "GlScroller.java"


# static fields
.field public static final MOVE_DOWN:I = 0x4

.field public static final MOVE_LEFT:I = 0x1

.field public static final MOVE_RIGHT:I = 0x2

.field public static final MOVE_UP:I = 0x3

.field protected static SUGGESTED_HW_RATIO:F

.field private static final TAG:Ljava/lang/String;

.field public static VISIBLE_BY_INIT:I

.field public static VISIBLE_BY_POS:I


# instance fields
.field public glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

.field public mInitVisible:Z

.field public mIsFocusInited:Z

.field public mIsVertical:Z

.field public mItemCount:I

.field public mItemFocused:I

.field public mItemGapH:F

.field public mItemGapW:F

.field public mItemH:F

.field public mItemHVisibleCnt:I

.field public mItemRowCount:I

.field public mItemStartX:F

.field public mItemStartY:F

.field public mItemStartZ:F

.field public mItemVVisibleCnt:I

.field public mItemVisibleCnt:I

.field public mItemW:F

.field public mScroll:F

.field protected mScrollBar:Lcom/sec/samsung/gallery/glview/GlScrollBar;

.field protected mScrollBarEnabled:Z

.field protected mScrollBarVisible:Z

.field public mScrollOffset:I

.field public mScrollRange:F

.field public mScrollStep:I

.field public mScrollVEnd:F

.field public mScrollVStart:F

.field public mScrollVisibleH:F

.field public mTagListH:F

.field public mVisibleFirst:I

.field public mVisibleLast:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/GlScroller;->TAG:Ljava/lang/String;

    .line 14
    const/4 v0, 0x1

    sput v0, Lcom/sec/samsung/gallery/glview/GlScroller;->VISIBLE_BY_POS:I

    .line 15
    const/4 v0, 0x2

    sput v0, Lcom/sec/samsung/gallery/glview/GlScroller;->VISIBLE_BY_INIT:I

    .line 17
    const/high16 v0, 0x3f400000    # 0.75f

    sput v0, Lcom/sec/samsung/gallery/glview/GlScroller;->SUGGESTED_HW_RATIO:F

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollVStart:F

    .line 28
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollVEnd:F

    .line 31
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollOffset:I

    .line 39
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 43
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mIsFocusInited:Z

    .line 44
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mTagListH:F

    return-void
.end method


# virtual methods
.method public add(ILcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 3
    .param p1, "startIndex"    # I
    .param p2, "addObj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    .line 558
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    .line 559
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlScroller;->getItemRowCount()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemRowCount:I

    .line 560
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    div-int v0, p1, v0

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollOffset:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    .line 561
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    .line 562
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    if-gez v0, :cond_0

    .line 563
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    .line 564
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    if-lt v0, v1, :cond_1

    .line 565
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    .line 567
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollVisibleH:F

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapH:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemRowCount:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 568
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapH:F

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemRowCount:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollVisibleH:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollRange:F

    .line 572
    :goto_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    div-int v0, p1, v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollStep:I

    .line 573
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollStep:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapH:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    .line 574
    return-void

    .line 570
    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollRange:F

    goto :goto_0
.end method

.method public applyThumbPosition()V
    .locals 0

    .prologue
    .line 412
    return-void
.end method

.method public calulateVisibleRange(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 150
    if-ltz p1, :cond_0

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    if-lt p1, v3, :cond_1

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    if-gt v3, p1, :cond_2

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    if-le p1, v3, :cond_0

    .line 156
    :cond_2
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    div-int/lit8 v3, v3, 0x2

    sub-int v1, p1, v3

    .line 157
    .local v1, "contentStart":I
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    add-int/2addr v3, v1

    add-int/lit8 v0, v3, -0x1

    .line 158
    .local v0, "contentEnd":I
    if-gez v1, :cond_4

    .line 159
    neg-int v2, v1

    .line 160
    .local v2, "over":I
    const/4 v1, 0x0

    .line 161
    add-int/2addr v0, v2

    .line 167
    .end local v2    # "over":I
    :cond_3
    :goto_1
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    .line 168
    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    goto :goto_0

    .line 162
    :cond_4
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    if-lt v0, v3, :cond_3

    .line 163
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    sub-int v3, v0, v3

    add-int/lit8 v2, v3, 0x1

    .line 164
    .restart local v2    # "over":I
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    add-int/lit8 v0, v3, -0x1

    .line 165
    sub-int/2addr v1, v2

    goto :goto_1
.end method

.method public firstColFocused()Z
    .locals 2

    .prologue
    .line 313
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    rem-int/2addr v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public firstRowFocused()Z
    .locals 2

    .prologue
    .line 321
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getItemRowCount()I
    .locals 2

    .prologue
    .line 680
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    div-int/2addr v0, v1

    return v0
.end method

.method public getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 436
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    if-ge v0, v2, :cond_0

    .line 437
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v2, v2

    if-lt v0, v2, :cond_1

    .line 446
    :cond_0
    const/4 v1, 0x0

    :goto_1
    return-object v1

    .line 440
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v1, v2, v0

    .line 441
    .local v1, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-nez v1, :cond_3

    .line 436
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 443
    :cond_3
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-ne v2, p1, :cond_2

    goto :goto_1
.end method

.method public getObjectForAdd()Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .locals 6

    .prologue
    .line 535
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    add-int/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    div-int v1, v4, v5

    .line 536
    .local v1, "newRow":I
    int-to-float v4, v1

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapH:F

    mul-float v3, v4, v5

    .line 537
    .local v3, "scroll":F
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollVisibleH:F

    add-float/2addr v4, v5

    cmpg-float v4, v4, v3

    if-gez v4, :cond_0

    .line 538
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollVisibleH:F

    sub-float v4, v3, v4

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/GlScroller;->onMove(F)V

    .line 539
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollVisibleH:F

    sub-float v4, v3, v4

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/GlScroller;->setScroll(F)V

    .line 541
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    if-ge v0, v4, :cond_1

    .line 542
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v4, v4

    if-lt v0, v4, :cond_2

    .line 554
    :cond_1
    const/4 v2, 0x0

    :goto_1
    return-object v2

    .line 545
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v2, v4, v0

    .line 546
    .local v2, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-nez v2, :cond_4

    .line 541
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 548
    :cond_4
    iget v4, v2, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    if-ne v4, v5, :cond_3

    .line 549
    iget v4, v2, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v5, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    or-int/2addr v4, v5

    iput v4, v2, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    .line 550
    const/4 v4, 0x1

    sget v5, Lcom/sec/samsung/gallery/glview/GlScroller;->VISIBLE_BY_POS:I

    invoke-virtual {v2, v4, v5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    goto :goto_1
.end method

.method public getScrollBar()Lcom/sec/samsung/gallery/glview/GlScrollBar;
    .locals 1

    .prologue
    .line 643
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollBar:Lcom/sec/samsung/gallery/glview/GlScrollBar;

    return-object v0
.end method

.method public getThumbByIndex(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .locals 3
    .param p1, "index"    # I

    .prologue
    const/4 v1, 0x0

    .line 467
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v2, v2

    if-gtz v2, :cond_1

    .line 479
    :cond_0
    :goto_0
    return-object v1

    .line 470
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    if-ge v0, v2, :cond_0

    .line 471
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 474
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v2, v2, v0

    if-nez v2, :cond_3

    .line 470
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 476
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v2, v2, v0

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-ne v2, p1, :cond_2

    .line 477
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v1, v1, v0

    goto :goto_0
.end method

.method public hideScrollBar()V
    .locals 4

    .prologue
    .line 657
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollBarEnabled:Z

    if-nez v0, :cond_1

    .line 664
    :cond_0
    :goto_0
    return-void

    .line 660
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollBarVisible:Z

    .line 661
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollBar:Lcom/sec/samsung/gallery/glview/GlScrollBar;

    if-eqz v0, :cond_0

    .line 662
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollBar:Lcom/sec/samsung/gallery/glview/GlScrollBar;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->fadeOut(J)V

    goto :goto_0
.end method

.method public hideScrollBarImmediately()V
    .locals 6

    .prologue
    .line 667
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollBar:Lcom/sec/samsung/gallery/glview/GlScrollBar;

    if-eqz v0, :cond_0

    .line 668
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollBar:Lcom/sec/samsung/gallery/glview/GlScrollBar;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->cancelPendingAmin()V

    .line 669
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollBar:Lcom/sec/samsung/gallery/glview/GlScrollBar;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->fadeOut(JJ)V

    .line 671
    :cond_0
    return-void
.end method

.method public jumpTo(I)Z
    .locals 16
    .param p1, "dir"    # I

    .prologue
    .line 332
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v14, v14

    if-nez v14, :cond_1

    .line 333
    :cond_0
    sget-object v14, Lcom/sec/samsung/gallery/glview/GlScroller;->TAG:Ljava/lang/String;

    const-string v15, "no globject registered"

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    const/4 v14, 0x0

    .line 386
    :goto_0
    return v14

    .line 337
    :cond_1
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/samsung/gallery/glview/GlScroller;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v1

    .line 338
    .local v1, "cur":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    const/4 v12, 0x0

    .line 340
    .local v12, "target":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-eqz v1, :cond_a

    .line 341
    const v5, 0x7f7fffff    # Float.MAX_VALUE

    .line 342
    .local v5, "distanceMax":F
    const v6, 0x3a83126f    # 0.001f

    .line 344
    .local v6, "epsilon":F
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v14, v14

    if-ge v7, v14, :cond_a

    .line 345
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v8, v14, v7

    .line 346
    .local v8, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-eqz v8, :cond_2

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getVisibility()Z

    move-result v14

    if-eqz v14, :cond_2

    iget v14, v8, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v15, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-eq v14, v15, :cond_2

    iget v14, v8, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    if-lt v14, v15, :cond_2

    iget v14, v8, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    if-le v14, v15, :cond_3

    .line 344
    :cond_2
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 351
    :cond_3
    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getX()F

    move-result v9

    .line 352
    .local v9, "objx":F
    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getY()F

    move-result v10

    .line 353
    .local v10, "objy":F
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getX()F

    move-result v2

    .line 354
    .local v2, "curx":F
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getY()F

    move-result v3

    .line 355
    .local v3, "cury":F
    const/4 v14, 0x4

    move/from16 v0, p1

    if-ne v0, v14, :cond_4

    const v14, 0x3a83126f    # 0.001f

    sub-float v14, v3, v14

    cmpg-float v14, v10, v14

    if-ltz v14, :cond_7

    :cond_4
    const/4 v14, 0x3

    move/from16 v0, p1

    if-ne v0, v14, :cond_5

    const v14, 0x3a83126f    # 0.001f

    add-float/2addr v14, v3

    cmpl-float v14, v10, v14

    if-gtz v14, :cond_7

    :cond_5
    const/4 v14, 0x1

    move/from16 v0, p1

    if-ne v0, v14, :cond_6

    const v14, 0x3a83126f    # 0.001f

    sub-float v14, v2, v14

    cmpg-float v14, v9, v14

    if-ltz v14, :cond_7

    :cond_6
    const/4 v14, 0x2

    move/from16 v0, p1

    if-ne v0, v14, :cond_2

    const v14, 0x3a83126f    # 0.001f

    add-float/2addr v14, v2

    cmpl-float v14, v9, v14

    if-lez v14, :cond_2

    .line 357
    :cond_7
    const/4 v14, 0x1

    move/from16 v0, p1

    if-eq v0, v14, :cond_8

    const/4 v14, 0x2

    move/from16 v0, p1

    if-ne v0, v14, :cond_9

    :cond_8
    const/high16 v13, 0x3f000000    # 0.5f

    .line 358
    .local v13, "weightX":F
    :goto_3
    sub-float v14, v9, v2

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v14

    mul-float/2addr v14, v13

    sub-float v15, v10, v3

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v15

    add-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 359
    .local v4, "distance":F
    const v14, 0x3a83126f    # 0.001f

    add-float/2addr v14, v4

    cmpg-float v14, v14, v5

    if-gez v14, :cond_2

    .line 360
    move-object v12, v8

    .line 361
    move v5, v4

    goto :goto_2

    .line 357
    .end local v4    # "distance":F
    .end local v13    # "weightX":F
    :cond_9
    const/high16 v13, 0x40000000    # 2.0f

    goto :goto_3

    .line 367
    .end local v2    # "curx":F
    .end local v3    # "cury":F
    .end local v5    # "distanceMax":F
    .end local v6    # "epsilon":F
    .end local v7    # "i":I
    .end local v8    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .end local v9    # "objx":F
    .end local v10    # "objy":F
    :cond_a
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    .line 368
    .local v11, "oldFocus":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    if-lt v11, v14, :cond_b

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    if-le v11, v14, :cond_d

    .line 369
    :cond_b
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/samsung/gallery/glview/GlScroller;->setScroll(F)V

    .line 370
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/glview/GlScroller;->applyThumbPosition()V

    .line 371
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    .line 372
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/samsung/gallery/glview/GlScroller;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v12

    .line 380
    :goto_4
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/samsung/gallery/glview/GlScroller;->onFocusChange(I)V

    .line 381
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/samsung/gallery/glview/GlScroller;->update(I)V

    .line 382
    if-eqz v12, :cond_c

    .line 383
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/samsung/gallery/glview/GlScroller;->update(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 384
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/samsung/gallery/glview/GlScroller;->makeVisible(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 386
    :cond_c
    const/4 v14, 0x1

    goto/16 :goto_0

    .line 373
    :cond_d
    if-eqz v12, :cond_e

    .line 374
    iget v14, v12, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    goto :goto_4

    .line 376
    :cond_e
    sget-object v14, Lcom/sec/samsung/gallery/glview/GlScroller;->TAG:Ljava/lang/String;

    const-string v15, "cannot find target"

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    const/4 v14, 0x0

    goto/16 :goto_0
.end method

.method public lastColFocused()Z
    .locals 2

    .prologue
    .line 317
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    rem-int/2addr v0, v1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lastRowFocused()Z
    .locals 3

    .prologue
    .line 325
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    add-int/lit8 v0, v2, 0x1

    .line 326
    .local v0, "idx":I
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    div-int v1, v0, v2

    .line 327
    .local v1, "rows":I
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    rem-int v2, v0, v2

    if-lez v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    .line 328
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemRowCount:I

    if-ne v1, v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public makeVisible(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 0
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    .line 390
    return-void
.end method

.method public moveTo(I)Z
    .locals 2
    .param p1, "direction"    # I

    .prologue
    .line 200
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->moveTo(IFLcom/sec/android/gallery3d/glcore/GlLayer;)Z

    move-result v0

    return v0
.end method

.method public moveTo(IF)Z
    .locals 1
    .param p1, "direction"    # I
    .param p2, "newAlbumHeaderHeight"    # F

    .prologue
    .line 204
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/samsung/gallery/glview/GlScroller;->moveTo(IFLcom/sec/android/gallery3d/glcore/GlLayer;)Z

    move-result v0

    return v0
.end method

.method public moveTo(IFLcom/sec/android/gallery3d/glcore/GlLayer;)Z
    .locals 11
    .param p1, "direction"    # I
    .param p2, "newAlbumHeaderHeight"    # F
    .param p3, "view"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 217
    iget-boolean v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mIsFocusInited:Z

    if-nez v8, :cond_1

    .line 218
    iput-boolean v7, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mIsFocusInited:Z

    .line 219
    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    if-gez v6, :cond_0

    :goto_0
    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    .line 220
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/glview/GlScroller;->onFocusChange(I)V

    .line 221
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/glview/GlScroller;->update(I)V

    move v5, v7

    .line 309
    :goto_1
    return v5

    .line 219
    :cond_0
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    goto :goto_0

    .line 224
    :cond_1
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    if-lt v8, v9, :cond_2

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    if-ge v8, v9, :cond_3

    .line 225
    :cond_2
    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/glview/GlScroller;->setScroll(F)V

    .line 226
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    .line 227
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/glview/GlScroller;->onFocusChange(I)V

    .line 228
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/glview/GlScroller;->onMove(F)V

    .line 229
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/glview/GlScroller;->update(I)V

    move v5, v7

    .line 230
    goto :goto_1

    .line 232
    :cond_3
    const/4 v2, 0x0

    .line 233
    .local v2, "newScroll":F
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    .line 234
    .local v4, "oldFocus":I
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollStep:I

    .line 235
    .local v3, "newScrollStep":I
    packed-switch p1, :pswitch_data_0

    .line 278
    :cond_4
    :goto_2
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    if-ne v4, v8, :cond_b

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    if-ne v4, v8, :cond_b

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemRowCount:I

    if-le v8, v7, :cond_b

    move v5, v7

    .line 279
    goto :goto_1

    .line 237
    :pswitch_0
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    if-le v8, v9, :cond_5

    .line 238
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    goto :goto_2

    .line 239
    :cond_5
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    if-lez v8, :cond_4

    .line 240
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    .line 241
    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    .line 245
    :pswitch_1
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    if-ge v8, v9, :cond_6

    .line 246
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    goto :goto_2

    .line 247
    :cond_6
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    add-int/lit8 v9, v9, -0x1

    if-ge v8, v9, :cond_4

    .line 248
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    .line 249
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 253
    :pswitch_2
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    add-int/2addr v9, v10

    if-lt v8, v9, :cond_7

    .line 254
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    sub-int/2addr v8, v9

    iput v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    goto :goto_2

    .line 255
    :cond_7
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    if-lt v8, v9, :cond_4

    .line 256
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    sub-int/2addr v8, v9

    iput v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    .line 257
    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    .line 261
    :pswitch_3
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    sub-int/2addr v9, v10

    if-gt v8, v9, :cond_8

    .line 262
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    add-int/2addr v8, v9

    iput v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    goto :goto_2

    .line 264
    :cond_8
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    add-int/lit8 v9, v9, -0x1

    if-ne v8, v9, :cond_9

    .line 265
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    if-eq v8, v9, :cond_4

    .line 266
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    iput v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    goto/16 :goto_2

    .line 269
    :cond_9
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    add-int/2addr v8, v9

    iput v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    .line 270
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    if-lt v8, v9, :cond_a

    .line 271
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    .line 272
    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    .line 280
    :cond_b
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    if-eq v4, v8, :cond_c

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    if-gez v8, :cond_d

    .line 281
    :cond_c
    const/4 v6, -0x1

    iput v6, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    goto/16 :goto_1

    .line 285
    :cond_d
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/glview/GlScroller;->onFocusChange(I)V

    .line 286
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    div-int v0, v5, v8

    .line 288
    .local v0, "currentRow":I
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapH:F

    add-int/lit8 v8, v0, 0x1

    int-to-float v8, v8

    mul-float v1, v5, v8

    .line 290
    .local v1, "currentScroll":F
    add-float/2addr v1, p2

    .line 291
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    sub-float v5, v1, v5

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollVStart:F

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mTagListH:F

    sub-float/2addr v8, v9

    cmpg-float v5, v5, v8

    if-gez v5, :cond_10

    .line 292
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollVStart:F

    sub-float v2, v1, v5

    .line 293
    cmpg-float v5, v2, v6

    if-ltz v5, :cond_e

    if-nez v0, :cond_f

    :cond_e
    move v2, v6

    .line 294
    :cond_f
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlScroller;->onMove(F)V

    .line 306
    :goto_3
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlScroller;->setScroll(F)V

    .line 307
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/GlScroller;->update(I)V

    .line 308
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/glview/GlScroller;->update(I)V

    move v5, v7

    .line 309
    goto/16 :goto_1

    .line 295
    :cond_10
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    sub-float v5, v1, v5

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollVEnd:F

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mTagListH:F

    add-float/2addr v6, v8

    cmpl-float v5, v5, v6

    if-lez v5, :cond_12

    .line 296
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollVEnd:F

    sub-float v2, v1, v5

    .line 297
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollRange:F

    cmpl-float v5, v2, v5

    if-lez v5, :cond_11

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollRange:F

    .line 298
    :cond_11
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlScroller;->onMove(F)V

    goto :goto_3

    .line 299
    :cond_12
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollStep:I

    if-eq v3, v5, :cond_13

    .line 300
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapH:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollStep:I

    int-to-float v6, v6

    mul-float v2, v5, v6

    .line 301
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlScroller;->onMove(F)V

    goto :goto_3

    .line 303
    :cond_13
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    .line 304
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlScroller;->onMove(F)V

    goto :goto_3

    .line 235
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public moveTo(ILcom/sec/android/gallery3d/glcore/GlLayer;)Z
    .locals 1
    .param p1, "direction"    # I
    .param p2, "view"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    .line 208
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/sec/samsung/gallery/glview/GlScroller;->moveTo(IFLcom/sec/android/gallery3d/glcore/GlLayer;)Z

    move-result v0

    return v0
.end method

.method public onAnimation(F)V
    .locals 0
    .param p1, "ratio"    # F

    .prologue
    .line 405
    return-void
.end method

.method public onFocusChange(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 393
    return-void
.end method

.method public onMove(F)V
    .locals 0
    .param p1, "delta"    # F

    .prologue
    .line 396
    return-void
.end method

.method public onScroll()V
    .locals 0

    .prologue
    .line 402
    return-void
.end method

.method public onScrollStep(I)V
    .locals 0
    .param p1, "step"    # I

    .prologue
    .line 399
    return-void
.end method

.method protected procScrollStep()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 52
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    .line 53
    const/4 v4, -0x1

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    .line 54
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v4, v4

    if-gtz v4, :cond_1

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 56
    :cond_1
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollStep:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollOffset:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    mul-int v1, v4, v5

    .line 57
    .local v1, "nScrl":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    if-ge v0, v4, :cond_2

    .line 58
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v4, v4

    if-lt v0, v4, :cond_3

    .line 93
    :cond_2
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollStep:I

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/GlScroller;->onScrollStep(I)V

    .line 94
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlScroller;->onScroll()V

    goto :goto_0

    .line 61
    :cond_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v3, v4, v0

    .line 62
    .local v3, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-nez v3, :cond_4

    .line 57
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 64
    :cond_4
    const/4 v2, 0x0

    .line 65
    .local v2, "needUpdate":Z
    :goto_3
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-ge v4, v1, :cond_5

    .line 66
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    add-int/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setIndex(I)V

    .line 67
    const/4 v2, 0x1

    goto :goto_3

    .line 69
    :cond_5
    :goto_4
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    add-int/2addr v5, v1

    if-lt v4, v5, :cond_6

    .line 70
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    sub-int/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setIndex(I)V

    .line 71
    const/4 v2, 0x1

    goto :goto_4

    .line 73
    :cond_6
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-ltz v4, :cond_b

    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    if-ge v4, v5, :cond_b

    .line 74
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    iget v5, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-ge v4, v5, :cond_7

    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    .line 75
    :cond_7
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    iget v5, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-le v4, v5, :cond_8

    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    .line 77
    :cond_8
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v5, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    or-int/2addr v4, v5

    iput v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    .line 78
    sget v4, Lcom/sec/samsung/gallery/glview/GlScroller;->VISIBLE_BY_POS:I

    invoke-virtual {v3, v6, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    .line 80
    if-eqz v2, :cond_a

    .line 81
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getVisibility()Z

    move-result v4

    if-nez v4, :cond_9

    .line 82
    sget v4, Lcom/sec/samsung/gallery/glview/GlScroller;->VISIBLE_BY_INIT:I

    invoke-virtual {v3, v6, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    .line 84
    :cond_9
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/GlScroller;->update(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 86
    :cond_a
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/GlScroller;->setThumbPosition(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    goto :goto_2

    .line 88
    :cond_b
    const/4 v4, 0x0

    sget v5, Lcom/sec/samsung/gallery/glview/GlScroller;->VISIBLE_BY_POS:I

    invoke-virtual {v3, v4, v5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    .line 89
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v5, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    xor-int/lit8 v5, v5, -0x1

    and-int/2addr v4, v5

    iput v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    goto :goto_2
.end method

.method public register([Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 0
    .param p1, "objSet"    # [Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    .line 462
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 463
    return-void
.end method

.method public remove(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 7
    .param p1, "removeObj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 580
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    .line 581
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    .line 582
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    if-lt v2, v3, :cond_0

    .line 583
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    .line 584
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlScroller;->getItemRowCount()I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemRowCount:I

    .line 585
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollVisibleH:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapH:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemRowCount:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_3

    .line 586
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapH:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemRowCount:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollVisibleH:F

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollRange:F

    .line 589
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    if-ge v0, v2, :cond_1

    .line 590
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v2, v2

    if-lt v0, v2, :cond_4

    .line 614
    :cond_1
    move-object v1, p1

    .line 615
    .local v1, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 616
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->setThumbPosition(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 617
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    if-lt v2, v3, :cond_2

    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    if-le v2, v3, :cond_a

    .line 618
    :cond_2
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    xor-int/lit8 v3, v3, -0x1

    and-int/2addr v2, v3

    iput v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    .line 619
    sget v2, Lcom/sec/samsung/gallery/glview/GlScroller;->VISIBLE_BY_POS:I

    invoke-virtual {v1, v5, v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    .line 627
    :goto_2
    return-void

    .line 588
    .end local v0    # "i":I
    .end local v1    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_3
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollRange:F

    goto :goto_0

    .line 593
    .restart local v0    # "i":I
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v1, v2, v0

    .line 594
    .restart local v1    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-nez v1, :cond_6

    .line 589
    :cond_5
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 596
    :cond_6
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getX()F

    move-result v2

    iput v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mLx:F

    .line 597
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getY()F

    move-result v2

    iput v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mLy:F

    .line 598
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v3, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-le v2, v3, :cond_5

    .line 599
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 602
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    if-lt v2, v3, :cond_7

    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    if-le v2, v3, :cond_8

    .line 603
    :cond_7
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    xor-int/lit8 v3, v3, -0x1

    and-int/2addr v2, v3

    iput v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    .line 604
    sget v2, Lcom/sec/samsung/gallery/glview/GlScroller;->VISIBLE_BY_POS:I

    invoke-virtual {v1, v5, v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    goto :goto_3

    .line 607
    :cond_8
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    and-int/2addr v2, v3

    if-nez v2, :cond_9

    .line 608
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    or-int/2addr v2, v3

    iput v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    .line 609
    sget v2, Lcom/sec/samsung/gallery/glview/GlScroller;->VISIBLE_BY_POS:I

    invoke-virtual {v1, v6, v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    .line 610
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->update(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 612
    :cond_9
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->setThumbPosition(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    goto :goto_3

    .line 621
    :cond_a
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    or-int/2addr v2, v3

    iput v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    .line 622
    sget v2, Lcom/sec/samsung/gallery/glview/GlScroller;->VISIBLE_BY_POS:I

    invoke-virtual {v1, v6, v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    .line 623
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->update(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 624
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getX()F

    move-result v2

    iput v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mLx:F

    .line 625
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getY()F

    move-result v2

    iput v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mLy:F

    goto :goto_2
.end method

.method public reset()V
    .locals 0

    .prologue
    .line 408
    return-void
.end method

.method public resetSet(IIZ)V
    .locals 8
    .param p1, "startIndex"    # I
    .param p2, "totalCount"    # I
    .param p3, "resetScroll"    # Z

    .prologue
    const/4 v7, 0x0

    .line 483
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v4, v4

    if-gtz v4, :cond_1

    .line 527
    :cond_0
    return-void

    .line 485
    :cond_1
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    .line 486
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlScroller;->getItemRowCount()I

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemRowCount:I

    .line 487
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    div-int v4, p1, v4

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollOffset:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    mul-int v3, v4, v5

    .line 488
    .local v3, "startOffset":I
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    add-int/2addr v4, v3

    add-int/lit8 v0, v4, -0x1

    .line 489
    .local v0, "endOffset":I
    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    .line 490
    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    .line 491
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    if-gez v4, :cond_2

    .line 492
    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    .line 493
    :cond_2
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    if-lt v4, v5, :cond_3

    .line 494
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    .line 495
    :cond_3
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollVisibleH:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapH:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemRowCount:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    cmpg-float v4, v4, v5

    if-gez v4, :cond_5

    .line 496
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapH:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemRowCount:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollVisibleH:F

    sub-float/2addr v4, v5

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollRange:F

    .line 499
    :goto_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlScroller;->updateScrollBar()V

    .line 500
    if-eqz p3, :cond_4

    .line 501
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    div-int v4, p1, v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollStep:I

    .line 502
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollStep:I

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapH:F

    mul-float/2addr v4, v5

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    .line 504
    :cond_4
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v4, v4

    if-ge v1, v4, :cond_0

    .line 505
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v2, v4, v1

    .line 506
    .local v2, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-nez v2, :cond_6

    .line 504
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 498
    .end local v1    # "i":I
    .end local v2    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_5
    const/4 v4, 0x0

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollRange:F

    goto :goto_0

    .line 508
    .restart local v1    # "i":I
    .restart local v2    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_6
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    if-lt v1, v4, :cond_7

    .line 510
    iget v4, v2, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v5, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_USED:I

    sget v6, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    or-int/2addr v5, v6

    xor-int/lit8 v5, v5, -0x1

    and-int/2addr v4, v5

    iput v4, v2, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    .line 511
    sget v4, Lcom/sec/samsung/gallery/glview/GlScroller;->VISIBLE_BY_POS:I

    invoke-virtual {v2, v7, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    goto :goto_2

    .line 514
    :cond_7
    iget v4, v2, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v5, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_USED:I

    or-int/2addr v4, v5

    iput v4, v2, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    .line 515
    add-int v4, v3, v1

    invoke-virtual {v2, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setIndex(I)V

    .line 516
    iget v4, v2, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-ltz v4, :cond_8

    iget v4, v2, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    if-le v4, v5, :cond_9

    .line 517
    :cond_8
    iget v4, v2, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v5, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    xor-int/lit8 v5, v5, -0x1

    and-int/2addr v4, v5

    iput v4, v2, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    .line 518
    sget v4, Lcom/sec/samsung/gallery/glview/GlScroller;->VISIBLE_BY_POS:I

    invoke-virtual {v2, v7, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    goto :goto_2

    .line 521
    :cond_9
    iget v4, v2, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v5, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    or-int/2addr v4, v5

    iput v4, v2, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    .line 522
    const/4 v4, 0x1

    sget v5, Lcom/sec/samsung/gallery/glview/GlScroller;->VISIBLE_BY_POS:I

    invoke-virtual {v2, v4, v5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    .line 523
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mInitVisible:Z

    sget v5, Lcom/sec/samsung/gallery/glview/GlScroller;->VISIBLE_BY_INIT:I

    invoke-virtual {v2, v4, v5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    .line 524
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlScroller;->update(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 525
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlScroller;->setThumbPosition(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    goto :goto_2
.end method

.method public scrollToVisible(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 174
    if-ltz p1, :cond_0

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    if-lt p1, v2, :cond_1

    .line 175
    :cond_0
    const/4 v2, 0x0

    .line 185
    :goto_0
    return-object v2

    .line 177
    :cond_1
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    if-gt v2, p1, :cond_2

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    if-gt p1, v2, :cond_2

    .line 178
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/glview/GlScroller;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v2

    goto :goto_0

    .line 180
    :cond_2
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/glview/GlScroller;->calulateVisibleRange(I)V

    .line 181
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    div-int v0, v2, v3

    .line 182
    .local v0, "newRow":I
    int-to-float v2, v0

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapH:F

    mul-float v1, v2, v3

    .line 183
    .local v1, "scroll":F
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->onMove(F)V

    .line 184
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->setScroll(F)V

    .line 185
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/glview/GlScroller;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v2

    goto :goto_0
.end method

.method public setScroll(F)V
    .locals 11
    .param p1, "scroll"    # F

    .prologue
    const/16 v10, 0x4eac

    .line 103
    const/4 v3, 0x0

    .line 105
    .local v3, "isMovingTop":Z
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    cmpl-float v7, v7, p1

    if-nez v7, :cond_0

    .line 145
    :goto_0
    return-void

    .line 107
    :cond_0
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapH:F

    div-float/2addr v7, v8

    float-to-int v0, v7

    .line 108
    .local v0, "cScrl":I
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapH:F

    div-float v7, p1, v7

    float-to-int v7, v7

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollStep:I

    .line 110
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollBar:Lcom/sec/samsung/gallery/glview/GlScrollBar;

    if-nez v7, :cond_5

    const/4 v6, 0x0

    .line 111
    .local v6, "rootView":Lcom/sec/android/gallery3d/glcore/GlRootView;
    :goto_1
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCaneExtraHapticEnabled(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 112
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    cmpg-float v7, p1, v7

    if-gez v7, :cond_1

    .line 113
    const/4 v3, 0x1

    .line 114
    :cond_1
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollVisibleH:F

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    add-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapH:F

    div-float/2addr v7, v8

    float-to-int v1, v7

    .line 115
    .local v1, "currentScrolStepBottom":I
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollVisibleH:F

    add-float/2addr v7, p1

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapH:F

    div-float/2addr v7, v8

    float-to-int v5, v7

    .line 116
    .local v5, "previousScrollStepBottom":I
    if-eq v1, v5, :cond_2

    .line 117
    if-eqz v3, :cond_2

    .line 118
    invoke-virtual {v6, v10}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    .line 121
    .end local v1    # "currentScrolStepBottom":I
    .end local v5    # "previousScrollStepBottom":I
    :cond_2
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    .line 122
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollBarEnabled:Z

    if-eqz v7, :cond_3

    .line 123
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlScroller;->showScrollBar()V

    .line 124
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlScroller;->updateScrollBar()V

    .line 126
    :cond_3
    const-string v7, "mScroll modifying (setScroll)"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mScroll="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollStep:I

    if-eq v7, v0, :cond_6

    .line 128
    if-eqz v6, :cond_4

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCaneExtraHapticEnabled(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 129
    if-nez v3, :cond_4

    .line 130
    invoke-virtual {v6, v10}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    .line 132
    :cond_4
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlScroller;->procScrollStep()V

    goto :goto_0

    .line 110
    .end local v6    # "rootView":Lcom/sec/android/gallery3d/glcore/GlRootView;
    :cond_5
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollBar:Lcom/sec/samsung/gallery/glview/GlScrollBar;

    iget-object v6, v7, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    goto :goto_1

    .line 135
    .restart local v6    # "rootView":Lcom/sec/android/gallery3d/glcore/GlRootView;
    :cond_6
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    if-ge v2, v7, :cond_7

    .line 136
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v7, v7

    if-lt v2, v7, :cond_8

    .line 144
    :cond_7
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlScroller;->onScroll()V

    goto/16 :goto_0

    .line 139
    :cond_8
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v4, v7, v2

    .line 140
    .local v4, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-eqz v4, :cond_9

    iget v7, v4, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-ltz v7, :cond_9

    iget v7, v4, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    if-ge v7, v8, :cond_9

    .line 141
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/GlScroller;->setThumbPosition(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 135
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public setScrollBarEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 639
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollBarEnabled:Z

    .line 640
    return-void
.end method

.method public setScrollBarObject(Lcom/sec/samsung/gallery/glview/GlScrollBar;Z)V
    .locals 0
    .param p1, "scrollBar"    # Lcom/sec/samsung/gallery/glview/GlScrollBar;
    .param p2, "enable"    # Z

    .prologue
    .line 634
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollBar:Lcom/sec/samsung/gallery/glview/GlScrollBar;

    .line 635
    iput-boolean p2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollBarEnabled:Z

    .line 636
    return-void
.end method

.method public setScrollStep(I)V
    .locals 2
    .param p1, "step"    # I

    .prologue
    .line 97
    int-to-float v0, p1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapH:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    .line 98
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollStep:I

    .line 99
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlScroller;->procScrollStep()V

    .line 100
    return-void
.end method

.method public setThumbPosition(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 6
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    .line 189
    if-nez p1, :cond_0

    .line 190
    sget-object v2, Lcom/sec/samsung/gallery/glview/GlScroller;->TAG:Ljava/lang/String;

    const-string v3, "setThumbPosition: The obj is null."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    :goto_0
    return-void

    .line 193
    :cond_0
    iget v2, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v3, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    div-int/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    mul-int/2addr v3, v4

    sub-int v0, v2, v3

    .line 194
    .local v0, "col":I
    iget v2, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    div-int v1, v2, v3

    .line 195
    .local v1, "row":I
    int-to-float v2, v0

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapW:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemStartX:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemStartY:F

    int-to-float v4, v1

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapH:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemStartZ:F

    invoke-virtual {p1, v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 196
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemW:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemH:F

    invoke-virtual {p1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setSize(FF)V

    goto :goto_0
.end method

.method public showScrollBar()V
    .locals 6

    .prologue
    .line 647
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollBarEnabled:Z

    if-nez v0, :cond_1

    .line 654
    :cond_0
    :goto_0
    return-void

    .line 650
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollBarVisible:Z

    .line 651
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollBar:Lcom/sec/samsung/gallery/glview/GlScrollBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollBar:Lcom/sec/samsung/gallery/glview/GlScrollBar;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->getAlpha()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 652
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollBar:Lcom/sec/samsung/gallery/glview/GlScrollBar;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->fadeIn(JJ)V

    goto :goto_0
.end method

.method public update()V
    .locals 3

    .prologue
    .line 418
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-nez v2, :cond_1

    .line 430
    :cond_0
    return-void

    .line 421
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    if-ge v0, v2, :cond_0

    .line 422
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 425
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v1, v2, v0

    .line 426
    .local v1, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-nez v1, :cond_2

    .line 421
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 428
    :cond_2
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->update(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    goto :goto_1
.end method

.method public update(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 454
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/glview/GlScroller;->getThumbByIndex(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v0

    .line 455
    .local v0, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-nez v0, :cond_0

    .line 459
    :goto_0
    return-void

    .line 458
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlScroller;->update(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    goto :goto_0
.end method

.method public update(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 0
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    .line 450
    return-void
.end method

.method protected updateScrollBar()V
    .locals 4

    .prologue
    .line 674
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollBar:Lcom/sec/samsung/gallery/glview/GlScrollBar;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollBarVisible:Z

    if-eqz v0, :cond_0

    .line 675
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollBar:Lcom/sec/samsung/gallery/glview/GlScrollBar;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollRange:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->update(FFF)V

    .line 677
    :cond_0
    return-void
.end method
