.class public Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "ShowNewAlbumDialogCmd.java"

# interfaces
.implements Ljava/util/Observer;
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mNewAlbumDialog:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

.field private mOperationType:I

.field private mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

.field private mTopSetPath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mOperationType:I

    return-void
.end method

.method private checkMediaFolder(Ljava/lang/String;)Z
    .locals 14
    .param p1, "dirPath"    # Ljava/lang/String;

    .prologue
    const/4 v13, 0x1

    const/4 v11, 0x0

    .line 172
    const/4 v9, 0x0

    .line 173
    .local v9, "result":Z
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 174
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v5, 0x2

    new-array v2, v5, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v2, v11

    const-string/jumbo v5, "substr(_data, 0, length(_data)-length(_display_name)) as dir_path"

    aput-object v5, v2, v13

    .line 178
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "dir_path = ? COLLATE NOCASE"

    .line 179
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v13, [Ljava/lang/String;

    aput-object p1, v4, v11

    .line 182
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v8, 0x0

    .local v8, "ix":I
    :goto_0
    if-gt v8, v13, :cond_3

    .line 185
    const/4 v6, 0x0

    .line 186
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v8, :cond_1

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 189
    .local v1, "uri":Landroid/net/Uri;
    :goto_1
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 190
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-lez v5, :cond_0

    .line 191
    const/4 v9, 0x1

    .line 196
    :cond_0
    if-nez v6, :cond_2

    move v5, v11

    .line 208
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :goto_2
    return v5

    .line 186
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_1
    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_1

    .line 199
    .restart local v1    # "uri":Landroid/net/Uri;
    :cond_2
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 204
    :goto_3
    if-eqz v9, :cond_6

    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_3
    move v5, v9

    .line 208
    goto :goto_2

    .line 200
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v10

    .line 201
    .local v10, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->TAG:Ljava/lang/String;

    const-string v12, "close fail"

    invoke-static {v5, v12, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 193
    .end local v10    # "t":Ljava/lang/Throwable;
    :catch_1
    move-exception v7

    .line 194
    .local v7, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 196
    if-nez v6, :cond_4

    move v5, v11

    .line 197
    goto :goto_2

    .line 199
    :cond_4
    :try_start_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_3

    .line 200
    :catch_2
    move-exception v10

    .line 201
    .restart local v10    # "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->TAG:Ljava/lang/String;

    const-string v12, "close fail"

    invoke-static {v5, v12, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 196
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v10    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v5

    if-nez v6, :cond_5

    move v5, v11

    .line 197
    goto :goto_2

    .line 199
    :cond_5
    :try_start_4
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3

    .line 202
    :goto_4
    throw v5

    .line 200
    :catch_3
    move-exception v10

    .line 201
    .restart local v10    # "t":Ljava/lang/Throwable;
    sget-object v11, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->TAG:Ljava/lang/String;

    const-string v12, "close fail"

    invoke-static {v11, v12, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 182
    .end local v10    # "t":Ljava/lang/Throwable;
    :cond_6
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method

.method private dismissDialog()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mNewAlbumDialog:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mNewAlbumDialog:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->dismissDialog()V

    .line 129
    :cond_0
    return-void
.end method

.method private downloadViaSLinkFramework(Ljava/lang/String;)V
    .locals 5
    .param p1, "dstAlbumPath"    # Ljava/lang/String;

    .prologue
    .line 104
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    iget v2, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mOperationType:I

    sget v4, Lcom/sec/samsung/gallery/core/Event;->EVENT_MOVE_FILES:I

    if-ne v2, v4, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v3, p1, v2}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->getModalDownloadIntent(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    .line 106
    .local v1, "intent":Landroid/content/Intent;
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    :goto_1
    return-void

    .line 104
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 107
    .restart local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->TAG:Ljava/lang/String;

    const-string v3, "Activity Not Found"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method private showDialog()V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mNewAlbumDialog:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mNewAlbumDialog:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->getNewAlbumDialogIsReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    :goto_0
    return-void

    .line 118
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->dismissDialog()V

    .line 119
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mNewAlbumDialog:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    .line 120
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mNewAlbumDialog:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->addObserver(Ljava/util/Observer;)V

    .line 121
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mNewAlbumDialog:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->showNewAlbumDialog()V

    .line 122
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mNewAlbumDialog:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->setCurrentName(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public checkNewAlbumExisted(Ljava/lang/String;)Z
    .locals 7
    .param p1, "albumName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 132
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 133
    :cond_0
    sget-object v4, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->TAG:Ljava/lang/String;

    const-string v5, "Album name is empty!"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mContext:Landroid/content/Context;

    const v5, 0x7f0e01ca

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 167
    :goto_0
    return v3

    .line 138
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/sec/android/gallery3d/util/GalleryUtils;->DEFAULT_NEW_ALBUM_DIR:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 140
    .local v2, "tgtPathStr":Ljava/lang/String;
    const/4 v1, 0x0

    .line 142
    .local v1, "tgtPathFile":Ljava/io/File;
    :try_start_0
    new-instance v1, Ljava/io/File;

    .end local v1    # "tgtPathFile":Ljava/io/File;
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    .restart local v1    # "tgtPathFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 151
    sget-object v4, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "File is exist already! create directory fail! ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mContext:Landroid/content/Context;

    const v5, 0x7f0e013b

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 143
    .end local v1    # "tgtPathFile":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 144
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 155
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "tgtPathFile":Ljava/io/File;
    :cond_2
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->canWrite()Z

    move-result v4

    if-nez v4, :cond_3

    .line 156
    sget-object v4, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->TAG:Ljava/lang/String;

    const-string v5, "Directory don\'t have write permission! []"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mContext:Landroid/content/Context;

    const v5, 0x7f0e01cb

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 160
    :cond_3
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 161
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->checkMediaFolder(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 162
    sget-object v4, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->TAG:Ljava/lang/String;

    const-string v5, "Media directory exist! []"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mContext:Landroid/content/Context;

    const v5, 0x7f0e0110

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 167
    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_0
.end method

.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 4
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const/4 v3, 0x3

    .line 48
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Object;

    move-object v0, v2

    check-cast v0, [Ljava/lang/Object;

    .line 49
    .local v0, "params":[Ljava/lang/Object;
    const/4 v2, 0x0

    aget-object v2, v0, v2

    check-cast v2, Landroid/content/Context;

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mContext:Landroid/content/Context;

    .line 50
    const/4 v2, 0x1

    aget-object v2, v0, v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 51
    .local v1, "show":Z
    const/4 v2, 0x2

    aget-object v2, v0, v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mOperationType:I

    .line 53
    array-length v2, v0

    if-le v2, v3, :cond_0

    .line 54
    aget-object v2, v0, v3

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mTopSetPath:Ljava/lang/String;

    .line 57
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    .line 59
    if-eqz v1, :cond_1

    .line 60
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->showDialog()V

    .line 63
    :goto_0
    return-void

    .line 62
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->dismissDialog()V

    goto :goto_0
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 10
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 67
    move-object v0, p2

    check-cast v0, Lcom/sec/samsung/gallery/core/Event;

    .line 69
    .local v0, "ev":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v6

    sget v7, Lcom/sec/samsung/gallery/core/Event;->EVENT_CREATE_NEW_ALBUM:I

    if-ne v6, v7, :cond_2

    .line 70
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getData()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 71
    .local v1, "newAlbumName":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->checkNewAlbumExisted(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 72
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mNewAlbumDialog:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    invoke-virtual {v6, v8}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->setDialogDismiss(Z)V

    .line 73
    iget v6, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mOperationType:I

    sget v7, Lcom/sec/samsung/gallery/core/Event;->EVENT_COPY_FILES:I

    if-eq v6, v7, :cond_0

    iget v6, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mOperationType:I

    sget v7, Lcom/sec/samsung/gallery/core/Event;->EVENT_MOVE_FILES:I

    if-eq v6, v7, :cond_0

    iget v6, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mOperationType:I

    sget v7, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_FROM_SECRETBOX:I

    if-ne v6, v7, :cond_4

    .line 76
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/sec/android/gallery3d/util/GalleryUtils;->DEFAULT_NEW_ALBUM_DIR:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 78
    .local v5, "tgtPathStr":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mContext:Landroid/content/Context;

    check-cast v6, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    .line 79
    .local v4, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mContext:Landroid/content/Context;

    check-cast v6, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getNewAlbumSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    .line 80
    .local v2, "newAlbumSelectionProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 81
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mTopSetPath:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->isSlinkPath(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 82
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Ljava/util/List;)V

    .line 85
    :cond_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mTopSetPath:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->isSlinkPath(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 86
    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->downloadViaSLinkFramework(Ljava/lang/String;)V

    .line 101
    .end local v1    # "newAlbumName":Ljava/lang/String;
    .end local v2    # "newAlbumSelectionProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    .end local v4    # "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    .end local v5    # "tgtPathStr":Ljava/lang/String;
    :cond_2
    :goto_0
    return-void

    .line 90
    .restart local v1    # "newAlbumName":Ljava/lang/String;
    .restart local v2    # "newAlbumSelectionProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    .restart local v4    # "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    .restart local v5    # "tgtPathStr":Ljava/lang/String;
    :cond_3
    const/4 v6, 0x4

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mContext:Landroid/content/Context;

    aput-object v6, v3, v9

    aput-object v5, v3, v8

    const/4 v6, 0x2

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v3, v6

    const/4 v6, 0x3

    iget v7, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mOperationType:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v6

    .line 93
    .local v3, "params":[Ljava/lang/Object;
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mContext:Landroid/content/Context;

    check-cast v6, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v6

    const-string v7, "SHOW_NEW_ALBUM_COPY_MOVE_DIALOG"

    invoke-virtual {v6, v7, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 95
    .end local v2    # "newAlbumSelectionProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    .end local v3    # "params":[Ljava/lang/Object;
    .end local v4    # "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    .end local v5    # "tgtPathStr":Ljava/lang/String;
    :cond_4
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mContext:Landroid/content/Context;

    check-cast v6, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v6

    const-string v7, "START_NEW_ALBUM_MODE"

    invoke-virtual {v6, v7, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 98
    :cond_5
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumDialogCmd;->mNewAlbumDialog:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    invoke-virtual {v6, v9}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->setDialogDismiss(Z)V

    goto :goto_0
.end method
