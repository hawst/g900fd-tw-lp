.class public Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam;
.super Ljava/lang/Object;
.source "ComposeViewBeam.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 27
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam;Ljava/util/List;)[Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam;->toUriPathArray(Ljava/util/List;)[Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private toUriPathArray(Ljava/util/List;)[Landroid/net/Uri;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)[",
            "Landroid/net/Uri;"
        }
    .end annotation

    .prologue
    .local p1, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 52
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    .line 53
    .local v5, "uriPathList":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    .line 55
    .local v0, "context":Landroid/content/Context;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 56
    .local v3, "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v6, v3, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v6, :cond_0

    move-object v2, v3

    .line 60
    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 61
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    .line 62
    .local v4, "uri":Landroid/net/Uri;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$AndroidBeamUtils;->isBeamSupported(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 63
    new-array v6, v7, [Landroid/net/Uri;

    invoke-static {}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$AndroidBeamUtils;->getDummyCloudUri()Landroid/net/Uri;

    move-result-object v7

    aput-object v7, v6, v8

    .line 73
    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v3    # "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v4    # "uri":Landroid/net/Uri;
    :goto_1
    return-object v6

    .line 64
    .restart local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v3    # "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    .restart local v4    # "uri":Landroid/net/Uri;
    :cond_1
    invoke-static {v4}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$AndroidBeamUtils;->isRemoteContents(Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 66
    new-array v6, v7, [Landroid/net/Uri;

    aput-object v4, v6, v8

    goto :goto_1

    .line 68
    :cond_2
    invoke-static {v0, v4}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$AndroidBeamUtils;->convertToFileUri(Landroid/content/Context;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v4

    .line 69
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 73
    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v3    # "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_3
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_4

    const/4 v6, 0x0

    goto :goto_1

    :cond_4
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Landroid/net/Uri;

    invoke-interface {v5, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Landroid/net/Uri;

    goto :goto_1
.end method


# virtual methods
.method public setBeamListener()V
    .locals 4

    .prologue
    .line 30
    new-instance v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam$1;-><init>(Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam;)V

    .line 43
    .local v0, "onGetFilePathListener":Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v0, v1, v2

    .line 47
    .local v1, "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "ANDROID_BEAM"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 49
    return-void
.end method
