.class public Lcom/sec/samsung/gallery/core/NotificationNames;
.super Ljava/lang/Object;
.source "NotificationNames.java"


# static fields
.field public static final ACTION_DELETE_CONFIRM:Ljava/lang/String; = "ACTION_DELETE_CONFIRM"

.field public static final ADD_SLIDESHOW_MUSIC:Ljava/lang/String; = "ADD_SLIDESHOW_MUSIC"

.field public static final ADD_USER_TAG:Ljava/lang/String; = "ADD_USER_TAG"

.field public static final ADJUST_SCREEN_BRIGHTNESS:Ljava/lang/String; = "ADJUST_SCREEN_BRIGHTNESS"

.field public static final ANDROID_BEAM:Ljava/lang/String; = "ANDROID_BEAM"

.field public static final BURSTSHOTLIST_UPDATED:Ljava/lang/String; = "BURSTSHOTLIST_UPDATED"

.field public static final BUSRT_SHOT_SETTING_CHANGED:Ljava/lang/String; = "BUSRT_SHOT_SETTING_CHANGED"

.field public static final CONVERT_MULTI_FORMAT_TO_MP4:Ljava/lang/String; = "CONVERT_MULTI_FORMAT_TO_MP4"

.field public static final COPY_TO_CLIPBOARD:Ljava/lang/String; = "COPY_TO_CLIPBOARD"

.field public static final COPY_TO_EVENT:Ljava/lang/String; = "COPY_TO_EVENT"

.field public static final CPU_BOOST:Ljava/lang/String; = "CPU_BOOST"

.field public static final CREATE_VIDEO_ALBUM:Ljava/lang/String; = "CREATE_VIDEO_ALBUM"

.field public static final CROP_IMAGE:Ljava/lang/String; = "CROP_IMAGE"

.field public static final DCM_INTENT_RECEIVER:Ljava/lang/String; = "DCM_INTENT_RECEIVER"

.field public static final DESTROY:Ljava/lang/String; = "DESTROY"

.field public static final DEVICE_REMOVED:Ljava/lang/String; = "DEVICE_DISCONNECTED"

.field public static final DISSMISS_SLIDE_SHOW_SETTINGS_DIALOG:Ljava/lang/String; = "DISSMISS_SLIDE_SHOW_SETTINGS_DIALOG"

.field public static final DMR_VIEW_CONTROLLER:Ljava/lang/String; = "DMR_VIEW_CONTROLLER"

.field public static final DOWNLOAD_CLOUD:Ljava/lang/String; = "DOWNLOAD_CLOUD"

.field public static final DOWNLOAD_CLOUDBY:Ljava/lang/String; = "DOWNLOAD_CLOUDBY"

.field public static final DOWNLOAD_NEARBY:Ljava/lang/String; = "DOWNLOAD_NEARBY"

.field public static final EXIT_NEW_ALBUM_MODE:Ljava/lang/String; = "EXIT_NEW_ALBUM_MODE"

.field public static final EXIT_SELECTION_MODE:Ljava/lang/String; = "EXIT_SELECTION_MODE"

.field public static final FLASH_ANNOTATE:Ljava/lang/String; = "FLASH_ANNOTATE"

.field public static final HIDE_ALBUMS:Ljava/lang/String; = "HIDE_ALBUMS"

.field public static final HIDE_ROOT_VIEW:Ljava/lang/String; = "HIDE_ROOT_VIEW"

.field public static final IMAGE_EDIT:Ljava/lang/String; = "IMAGE_EDIT"

.field public static final IMAGE_VIEWER_START:Ljava/lang/String; = "IMAGE_VIEWER_START"

.field public static final INFLATE_DRAWER:Ljava/lang/String; = "INFLATE_DRAWER"

.field public static final KNOX_MODE_RECEIVER:Ljava/lang/String; = "KNOX_MODE_RECEIVER"

.field public static final LAST_SHARE_APP:Ljava/lang/String; = "LAST_SHARE_APP"

.field public static final MAGIC_SHOT_STUDIO:Ljava/lang/String; = "MAGIC_SHOT_STUDIO"

.field public static final MAKE_MOTION_PICTURE:Ljava/lang/String; = "MAKE_MOTION_PICTURE"

.field public static final MAP_VIEW_START:Ljava/lang/String; = "MAP_VIEW_START"

.field public static final MEDIASCANNER_RECEIVER:Ljava/lang/String; = "MEDIASCANNER_RECEIVER"

.field public static final MEDIA_EJECT:Ljava/lang/String; = "MEDIA_EJECT"

.field public static final MOREINFO_EVENT:Ljava/lang/String; = "MOREINFO_EVENT"

.field public static final MOVE_TO_KNOX:Ljava/lang/String; = "MOVE_TO_KNOX"

.field public static final MOVE_TO_SECRETBOX:Ljava/lang/String; = "MOVE_TO_SECRETBOX"

.field public static final MULTIPLE_PICKER_SELECTION_COMPLETED:Ljava/lang/String; = "MULTIPLE_PICKER_SELECTION_COMPLETED"

.field public static final PHOTO_SIGNATURE:Ljava/lang/String; = "PHOTO_SIGNATURE"

.field public static final PICKER_ITEM_SELECTED:Ljava/lang/String; = "PICKER_ITEM_SELECTED"

.field public static final PICKER_START:Ljava/lang/String; = "PICKER_START"

.field public static final PLAY_3DTOUR:Ljava/lang/String; = "PLAY_3DTOUR"

.field public static final PLAY_OUTOFFOCUS_IMAGE:Ljava/lang/String; = "PLAY_OUTOFFOCUS_IMAGE"

.field public static final PLAY_SEQUENCE_IMAGE:Ljava/lang/String; = "PLAY_SEQUENCE_IMAGE"

.field public static final POPUP_DIALOG_HIDDEN:Ljava/lang/String; = "SHARE_DIALOG_HIDDEN"

.field public static final POST_GALLERY:Ljava/lang/String; = "POST_GALLERY"

.field public static final PREPARE_SLIDE_SHOW_DATA:Ljava/lang/String; = "PREPARE_SLIDE_SHOW_DATA"

.field public static final PRINT_IMAGE:Ljava/lang/String; = "PRINT_IMAGE"

.field public static final RECOMMEND_DROPBOX:Ljava/lang/String; = "RECOMMEND_DROPBOX"

.field public static final REFRESH_SELECTION:Ljava/lang/String; = "REFRESH_SELECTION"

.field public static final RENAME_ALBUM_OR_FILE:Ljava/lang/String; = "RENAME_ALBUM_OR_FILE"

.field public static final REORDER_CANCEL:Ljava/lang/String; = "REORDER_CANCEL"

.field public static final REORDER_DONE:Ljava/lang/String; = "REORDER_DONE"

.field public static final ROTATE_FILES:Ljava/lang/String; = "ROTATE_FILES"

.field public static final SECRET_MODE_CHANGED:Ljava/lang/String; = "SECRET_MODE_CHANGED"

.field public static final SECRET_MODE_RECEIVER:Ljava/lang/String; = "SECRET_MODE_RECEIVER"

.field public static final SET_DNIE_MODE:Ljava/lang/String; = "SET_DNIE_MODE"

.field public static final SHOW_BARGEIN_NOTIFICATION:Ljava/lang/String; = "SHOW_BARGEIN_NOTIFICATION"

.field public static final SHOW_BURST_SHOT_PLAY_SETTING_DIALOG:Ljava/lang/String; = "SHOW_BURST_SHOT_PLAY_SETTING_DIALOG"

.field public static final SHOW_CONTENT_TO_DISPLAY_DIALOG:Ljava/lang/String; = "SHOW_CONTENT_TO_DISPLAY_DIALOG"

.field public static final SHOW_COPY_MOVE_ALBUM_LIST_DIALOG:Ljava/lang/String; = "SHOW_COPY_MOVE_ALBUM_LIST_DIALOG"

.field public static final SHOW_DELETE_DIALOG:Ljava/lang/String; = "SHOW_DELETE_DIALOG"

.field public static final SHOW_DETAILS_DIALOG:Ljava/lang/String; = "SHOW_DETAILS_DIALOG"

.field public static final SHOW_EDIT_BUSRT_SHOT_DIALOG:Ljava/lang/String; = "SHOW_EDIT_BUSRT_SHOT_DIALOG"

.field public static final SHOW_FACES_SCAN_DIALOG:Ljava/lang/String; = "SHOW_FACES_SCAN_DIALOG"

.field public static final SHOW_IMAGE_VIDEO_CONVERSION_DIALOG:Ljava/lang/String; = "SHOW_IMAGE_VIDEO_CONVERSION_DIALOG"

.field public static final SHOW_IMAGE_VIDEO_SHARE_DIALOG:Ljava/lang/String; = "SHOW_IMAGE_VIDEO_SHARE_DIALOG"

.field public static final SHOW_IMPORT_DIALOG:Ljava/lang/String; = "SHOW_IMPORT_DIALOG"

.field public static final SHOW_NEW_ALBUM_CANCEL_DIALOG:Ljava/lang/String; = "SHOW_NEW_ALBUM_CANCEL_DIALOG"

.field public static final SHOW_NEW_ALBUM_COPY_MOVE_DIALOG:Ljava/lang/String; = "SHOW_NEW_ALBUM_COPY_MOVE_DIALOG"

.field public static final SHOW_NEW_ALBUM_DIALOG:Ljava/lang/String; = "SHOW_NEW_ALBUM_DIALOG"

.field public static final SHOW_ON_MAP:Ljava/lang/String; = "SHOW_ON_MAP"

.field public static final SHOW_RENAME_DIALOG:Ljava/lang/String; = "SHOW_RENAME_DIALOG"

.field public static final SHOW_SET_AS_DIALOG:Ljava/lang/String; = "SHOW_SET_AS_DIALOG"

.field public static final SHOW_SHARE_DIALOG:Ljava/lang/String; = "SHOW_SHARE_DIALOG"

.field public static final SHOW_SLIDESHOW_MUSIC_LIST:Ljava/lang/String; = "SHOW_SLIDESHOW_MUSIC_LIST"

.field public static final SHOW_SLIDESHOW_SETTINGS:Ljava/lang/String; = "SHOW_SLIDESHOW_SETTINGS"

.field public static final SHOW_USAGE_ALERT_DIALOG:Ljava/lang/String; = "SHOW_USAGE_ALERT_DIALOG"

.field public static final SLIDESHOW_SELECT_MUSIC:Ljava/lang/String; = "SLIDESHOW_SETUP_MUSIC"

.field public static final SLIDESHOW_SELECT_MUSIC_FROM_MP:Ljava/lang/String; = "SLIDESHOW_PICK_MUSIC"

.field public static final SLIDESHOW_SELECT_MUSIC_UPDATE:Ljava/lang/String; = "SLIDESHOW_MUSIC_UPDATE"

.field public static final SOUNDSHOT_EDITOR:Ljava/lang/String; = "SOUNDSCENE_EDITOR"

.field public static final SOUND_SCENE:Ljava/lang/String; = "SOUND_SCENE"

.field public static final START_CAMERA:Ljava/lang/String; = "START_CAMERA"

.field public static final START_CATEGORY_TAG:Ljava/lang/String; = "START_CATEGORY_TAG"

.field public static final START_COLLAGE_APP:Ljava/lang/String; = "START_COLLAGE_APP"

.field public static final START_CONTACT_PICK:Ljava/lang/String; = "START_CONTACT_PICK"

.field public static final START_FACE_SCANING:Ljava/lang/String; = "START_FACE_SCANING"

.field public static final START_FACE_TAG:Ljava/lang/String; = "START_FACE_TAG"

.field public static final START_GALLERY_VIEW:Ljava/lang/String; = "START_GALLERY_VIEW"

.field public static final START_GIF_MAKER:Ljava/lang/String; = "START_GIF_MAKER"

.field public static final START_GOOGLE_MAPS:Ljava/lang/String; = "START_GOOGLE_MAPS"

.field public static final START_HELP:Ljava/lang/String; = "START_HELP"

.field public static final START_IDLE_PROCESS:Ljava/lang/String; = "START_IDLE_PROCESS"

.field public static final START_IMAGE_NOTE:Ljava/lang/String; = "START_IMAGE_NOTE"

.field public static final START_MMS_SAVE:Ljava/lang/String; = "START_MMS_SAVE"

.field public static final START_NAVIGATION:Ljava/lang/String; = "START_NAVIGATION"

.field public static final START_NEW_ALBUM_MODE:Ljava/lang/String; = "START_NEW_ALBUM_MODE"

.field public static final START_SLIDESHOW:Ljava/lang/String; = "START_SLIDESHOW"

.field public static final START_STORY_ALBUM_APP:Ljava/lang/String; = "START_STORY_ALBUM_APP"

.field public static final S_STUDIO:Ljava/lang/String; = "S_STUDIO"

.field public static final TILT_TUTORIAL_START:Ljava/lang/String; = "TILT_TUTORIAL_START"

.field public static final UNLOCK_TOUCH:Ljava/lang/String; = "UNLOCK_TOUCH"

.field public static final UPDATE_CATEGORY:Ljava/lang/String; = "UPDATE_CATEGORY"

.field public static final UPDATE_CONFIRM_MENU:Ljava/lang/String; = "UPDATE_CONFIRM_MENU"

.field public static final USER_TAG_LOADING_FINISHED:Ljava/lang/String; = "USER_TAG_LOADING_FINISHED"

.field public static final USE_MULTI_CORE_CPU:Ljava/lang/String; = "USE_MULTI_CORE_CPU"

.field public static final VIDEO_PLAY:Ljava/lang/String; = "VIDEO_PLAY"

.field public static final VIDEO_TRIM:Ljava/lang/String; = "VIDEO_TRIM"

.field public static final VIEW_BY_TYPE_UPDATED:Ljava/lang/String; = "VIEW_BY_TYPE_UPDATED"

.field public static final VIEW_MODE_SWITCH:Ljava/lang/String; = "VIEW_MODE_SWITCH"

.field public static final VOICE_RECOGNIZER:Ljava/lang/String; = "VOICE_RECOGNIZER"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
