.class public Lcom/sec/samsung/gallery/decoder/LargeImageTile;
.super Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;
.source "LargeImageTile.java"


# static fields
.field public static final LARGE_IMAGE_TILE_POOL_SIZE:I = 0x64

.field public static final STATE_ACTIVATED:I = 0x1

.field public static final STATE_DECODED:I = 0x8

.field public static final STATE_RECYCLED:I = 0x4

.field public static final STATE_UPLOADED:I = 0x2


# instance fields
.field public mDecodedTile:Landroid/graphics/Bitmap;

.field public mNext:Lcom/sec/samsung/gallery/decoder/LargeImageTile;

.field private mTexHeight:I

.field private mTexWidth:I

.field private mTileHeight:I

.field public mTileLevel:I

.field public volatile mTileState:I

.field private mTileWidth:I

.field public mX:I

.field public mY:I


# direct methods
.method public constructor <init>(IIIIIII)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "texWidth"    # I
    .param p4, "texHeight"    # I
    .param p5, "width"    # I
    .param p6, "height"    # I
    .param p7, "level"    # I

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glrenderer/UploadedTexture;-><init>()V

    .line 25
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mTileState:I

    .line 29
    iput p1, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mX:I

    .line 30
    iput p2, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mY:I

    .line 31
    iput p3, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mTexWidth:I

    .line 32
    iput p4, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mTexHeight:I

    .line 33
    iput p5, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mTileWidth:I

    .line 34
    iput p6, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mTileHeight:I

    .line 35
    iput p7, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mTileLevel:I

    .line 36
    return-void
.end method


# virtual methods
.method public getTextureHeight()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mTexHeight:I

    return v0
.end method

.method public getTextureWidth()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mTexWidth:I

    return v0
.end method

.method protected onFreeBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 40
    invoke-static {}, Lcom/sec/android/photos/data/GalleryBitmapPool;->getInstance()Lcom/sec/android/photos/data/GalleryBitmapPool;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/photos/data/GalleryBitmapPool;->put(Landroid/graphics/Bitmap;)Z

    .line 41
    return-void
.end method

.method protected onGetBitmap()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 45
    iget v2, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mTileState:I

    const/16 v3, 0x8

    if-eq v2, v3, :cond_0

    move-object v0, v1

    .line 58
    :goto_0
    return-object v0

    .line 53
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mTileWidth:I

    iget v3, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mTileHeight:I

    invoke-virtual {p0, v2, v3}, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->setSize(II)V

    .line 55
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mDecodedTile:Landroid/graphics/Bitmap;

    .line 56
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iput-object v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mDecodedTile:Landroid/graphics/Bitmap;

    .line 57
    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mTileState:I

    goto :goto_0
.end method

.method public update(IIIIIII)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "texWidth"    # I
    .param p4, "texHeight"    # I
    .param p5, "width"    # I
    .param p6, "height"    # I
    .param p7, "level"    # I

    .prologue
    .line 76
    iput p1, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mX:I

    .line 77
    iput p2, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mY:I

    .line 78
    iput p3, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mTexWidth:I

    .line 79
    iput p4, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mTexHeight:I

    .line 80
    iput p5, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mTileWidth:I

    .line 81
    iput p6, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mTileHeight:I

    .line 82
    iput p7, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mTileLevel:I

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mNext:Lcom/sec/samsung/gallery/decoder/LargeImageTile;

    .line 84
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mTileState:I

    .line 85
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->invalidateContent()V

    .line 86
    return-void
.end method
