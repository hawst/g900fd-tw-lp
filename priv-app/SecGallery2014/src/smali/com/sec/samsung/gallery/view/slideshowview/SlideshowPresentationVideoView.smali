.class public Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;
.super Landroid/view/SurfaceView;
.source "SlideshowPresentationVideoView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mContext:Landroid/content/Context;

.field mHandler:Landroid/os/Handler;

.field private mHolder:Landroid/view/SurfaceHolder;

.field private mIsOffHookOccured:Z

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private final mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mUri:Landroid/net/Uri;

.field private mVideoHeight:I

.field private mVideoWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mUri:Landroid/net/Uri;

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mIsOffHookOccured:Z

    .line 81
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 115
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$2;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 156
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$3;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 187
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$4;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 284
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$5;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$5;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mHandler:Landroid/os/Handler;

    .line 47
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mContext:Landroid/content/Context;

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/samsung/gallery/glview/GlSlideShowView;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mUri:Landroid/net/Uri;

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mIsOffHookOccured:Z

    .line 81
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$1;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 115
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$2;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 156
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$3;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 187
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$4;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 284
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$5;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$5;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mHandler:Landroid/os/Handler;

    .line 41
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mContext:Landroid/content/Context;

    .line 42
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    .line 43
    return-void
.end method

.method private abandonAudioFocus()V
    .locals 3

    .prologue
    .line 52
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 54
    .local v0, "am":Landroid/media/AudioManager;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 55
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/telephony/TelephonyManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;Landroid/telephony/TelephonyManager;)Landroid/telephony/TelephonyManager;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;
    .param p1, "x1"    # Landroid/telephony/TelephonyManager;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;
    .param p1, "x1"    # Landroid/media/MediaPlayer;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/view/SurfaceHolder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mHolder:Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->gainAudioFocus()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mVideoHeight:I

    return v0
.end method

.method static synthetic access$402(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;
    .param p1, "x1"    # I

    .prologue
    .line 24
    iput p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mVideoHeight:I

    return p1
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mVideoWidth:I

    return v0
.end method

.method static synthetic access$502(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;
    .param p1, "x1"    # I

    .prologue
    .line 24
    iput p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mVideoWidth:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;
    .param p1, "x1"    # Z

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mIsOffHookOccured:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->abandonAudioFocus()V

    return-void
.end method

.method private gainAudioFocus()V
    .locals 6

    .prologue
    .line 139
    const/4 v1, 0x0

    .line 140
    .local v1, "audioFocusGranted":Z
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mContext:Landroid/content/Context;

    const-string v4, "audio"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 143
    .local v0, "am":Landroid/media/AudioManager;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mOnAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v4, 0x3

    const/4 v5, 0x2

    invoke-virtual {v0, v3, v4, v5}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v2

    .line 148
    .local v2, "result":I
    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 149
    const/4 v1, 0x1

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    if-nez v2, :cond_0

    .line 152
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public finishSlideshowPresentationVideoView()V
    .locals 6

    .prologue
    .line 220
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 221
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->reset()V

    .line 222
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 223
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 228
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    const-wide/16 v4, 0x64

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 229
    return-void

    .line 225
    :catch_0
    move-exception v0

    .line 226
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public initVideoView(Landroid/net/Uri;)V
    .locals 4
    .param p1, "mUri"    # Landroid/net/Uri;

    .prologue
    .line 58
    sget-object v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->TAG:Ljava/lang/String;

    const-string v2, "initVideoView()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 61
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, p1}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 62
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 72
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 73
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 74
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 76
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mContext:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 77
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v3, 0x20

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 78
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mHolder:Landroid/view/SurfaceHolder;

    .line 79
    return-void

    .line 63
    :catch_0
    move-exception v0

    .line 64
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 65
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 66
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    .line 67
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_2
    move-exception v0

    .line 68
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 69
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 70
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 234
    const/4 v3, 0x0

    .line 235
    .local v3, "videoWidth":I
    const/4 v2, 0x0

    .line 236
    .local v2, "videoHeight":I
    const/4 v0, 0x1

    .line 237
    .local v0, "mSurfaceHeight":I
    const/4 v1, 0x1

    .line 238
    .local v1, "mSurfaceWidth":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v4, :cond_0

    .line 239
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v3

    .line 240
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v2

    .line 243
    :cond_0
    if-nez v3, :cond_1

    if-nez v2, :cond_1

    .line 244
    invoke-super {p0, v1, v0}, Landroid/view/SurfaceView;->setMeasuredDimension(II)V

    .line 256
    :goto_0
    return-void

    .line 247
    :cond_1
    invoke-virtual {p0, v3, p1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->resolveAdjustedSize(II)I

    move-result v1

    .line 248
    invoke-virtual {p0, v2, p2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->resolveAdjustedSize(II)I

    move-result v0

    .line 251
    mul-int v4, v3, v0

    mul-int v5, v2, v1

    if-lt v4, v5, :cond_2

    .line 252
    mul-int v4, v1, v2

    div-int v0, v4, v3

    .line 255
    :goto_1
    invoke-super {p0, v1, v0}, Landroid/view/SurfaceView;->setMeasuredDimension(II)V

    goto :goto_0

    .line 254
    :cond_2
    mul-int v4, v0, v3

    div-int v1, v4, v2

    goto :goto_1
.end method

.method public resolveAdjustedSize(II)I
    .locals 3
    .param p1, "desiredSize"    # I
    .param p2, "measureSpec"    # I

    .prologue
    .line 259
    move v0, p1

    .line 261
    .local v0, "result":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 262
    .local v1, "specMode":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 264
    .local v2, "specSize":I
    sparse-switch v1, :sswitch_data_0

    .line 281
    :goto_0
    return v0

    .line 266
    :sswitch_0
    move v0, p1

    .line 267
    goto :goto_0

    .line 270
    :sswitch_1
    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 271
    goto :goto_0

    .line 274
    :sswitch_2
    move v0, v2

    .line 275
    goto :goto_0

    .line 264
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method

.method public setUri(Landroid/net/Uri;)V
    .locals 0
    .param p1, "mUri"    # Landroid/net/Uri;

    .prologue
    .line 308
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mUri:Landroid/net/Uri;

    .line 309
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 130
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 134
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "surfaceCreated()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mUri:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->initVideoView(Landroid/net/Uri;)V

    .line 136
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 216
    return-void
.end method
