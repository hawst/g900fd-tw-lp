.class Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;
.super Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$WindowRunnnable;
.source "AbsHListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckForKeyLongPress"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V
    .locals 1

    .prologue
    .line 2442
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$WindowRunnnable;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$1;

    .prologue
    .line 2442
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2446
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isPressed()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget v3, v3, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedPosition:I

    if-ltz v3, :cond_1

    .line 2447
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget v3, v3, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedPosition:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget v4, v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    sub-int v1, v3, v4

    .line 2448
    .local v1, "index":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-virtual {v3, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 2450
    .local v2, "v":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataChanged:Z

    if-nez v3, :cond_2

    .line 2451
    const/4 v0, 0x0

    .line 2452
    .local v0, "handled":Z
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;->sameWindow()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2453
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget v4, v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedPosition:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget-wide v6, v5, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelectedColId:J

    invoke-virtual {v3, v2, v4, v6, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->performLongPress(Landroid/view/View;IJ)Z

    move-result v0

    .line 2455
    :cond_0
    if-eqz v0, :cond_1

    .line 2456
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-virtual {v3, v8}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setPressed(Z)V

    .line 2457
    invoke-virtual {v2, v8}, Landroid/view/View;->setPressed(Z)V

    .line 2464
    .end local v0    # "handled":Z
    .end local v1    # "index":I
    .end local v2    # "v":Landroid/view/View;
    :cond_1
    :goto_0
    return-void

    .line 2460
    .restart local v1    # "index":I
    .restart local v2    # "v":Landroid/view/View;
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForKeyLongPress;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-virtual {v3, v8}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setPressed(Z)V

    .line 2461
    if-eqz v2, :cond_1

    invoke-virtual {v2, v8}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0
.end method
