.class Lcom/sec/samsung/gallery/view/detailview/DetailViewState$20;
.super Ljava/lang/Object;
.source "DetailViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/SideMirrorView$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0

    .prologue
    .line 3431
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$20;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongTap(I)V
    .locals 0
    .param p1, "slotIndex"    # I

    .prologue
    .line 3445
    return-void
.end method

.method public onSelectionChanged(Lcom/sec/android/gallery3d/data/MediaItem;Z)V
    .locals 0
    .param p1, "media"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "isSelected"    # Z

    .prologue
    .line 3449
    return-void
.end method

.method public onSelectionModeChanged(Z)V
    .locals 0
    .param p1, "isSelectionMode"    # Z

    .prologue
    .line 3453
    return-void
.end method

.method public onSlotSelected(ILcom/sec/android/gallery3d/data/MediaSet;)Z
    .locals 2
    .param p1, "slotIndex"    # I
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 3434
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$20;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    move-result-object v0

    if-eq v0, p2, :cond_0

    .line 3435
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$20;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    check-cast p2, Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    .end local p2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;
    invoke-static {v0, p2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1202(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/android/gallery3d/data/FilterDeleteSet;)Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    .line 3436
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$20;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$16600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AlbumReloader;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$20;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AlbumReloader;->changeSource(Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 3437
    const/4 v0, 0x1

    .line 3439
    :goto_0
    return v0

    .restart local p2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
