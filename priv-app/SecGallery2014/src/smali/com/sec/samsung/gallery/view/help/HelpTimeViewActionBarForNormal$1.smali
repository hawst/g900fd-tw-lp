.class Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal$1;
.super Ljava/lang/Object;
.source "HelpTimeViewActionBarForNormal.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;)V
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 25
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;->access$000(Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030002

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 28
    .local v0, "customView":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;->access$100(Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 29
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;->access$200(Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 30
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;->access$300(Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 32
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;->access$400(Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v1

    const v2, 0x7f0e0073

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setTitle(I)V

    .line 33
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;->access$500(Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f11002a

    invoke-virtual {v1, v2}, Landroid/content/Context;->setTheme(I)V

    .line 34
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;->access$600(Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 35
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;->access$700(Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/ActionBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 37
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;->access$800(Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 38
    return-void
.end method
