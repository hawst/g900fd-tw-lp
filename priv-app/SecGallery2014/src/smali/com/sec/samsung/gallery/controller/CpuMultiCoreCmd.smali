.class public Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "CpuMultiCoreCmd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;
    }
.end annotation


# static fields
.field private static final CPU_MULTICORE_DEMAND_DURATION_MS:I = 0x3e8

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCpuBooster:Landroid/os/DVFSHelper;

.field mSupportedCPUCoreTable:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    if-nez v1, :cond_0

    .line 47
    new-instance v1, Landroid/os/DVFSHelper;

    const/16 v2, 0xe

    invoke-direct {v1, p1, v2}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    .line 49
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    invoke-virtual {v1}, Landroid/os/DVFSHelper;->getSupportedCPUCoreNum()[I

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd;->mSupportedCPUCoreTable:[I

    .line 50
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd;->mSupportedCPUCoreTable:[I

    if-eqz v1, :cond_0

    .line 51
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    const-string v2, "CORE_NUM"

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd;->mSupportedCPUCoreTable:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    :cond_0
    :goto_0
    return-void

    .line 54
    :catch_0
    move-exception v0

    .line 55
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static initMultiCore(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 68
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;->INITIALIZE:Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;

    aput-object v2, v0, v1

    .line 71
    .local v0, "params":[Ljava/lang/Object;
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-static {p0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "USE_MULTI_CORE_CPU"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 72
    return-void
.end method

.method public static useMultiCore(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 75
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;->USE_MULTI_CORE_CPU:Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;

    aput-object v2, v0, v1

    .line 78
    .local v0, "params":[Ljava/lang/Object;
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-static {p0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "USE_MULTI_CORE_CPU"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 79
    return-void
.end method

.method private useMultiCoreCpu()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd;->mCpuBooster:Landroid/os/DVFSHelper;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/os/DVFSHelper;->acquire(I)V

    .line 63
    sget-object v0, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd;->TAG:Ljava/lang/String;

    const-string v1, "Use MultiCore CPU Clock"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    :cond_0
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 4
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 30
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseManulMulticore:Z

    if-nez v3, :cond_1

    .line 43
    :cond_0
    :goto_0
    return-void

    .line 34
    :cond_1
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Object;

    move-object v2, v3

    check-cast v2, [Ljava/lang/Object;

    .line 35
    .local v2, "params":[Ljava/lang/Object;
    const/4 v3, 0x0

    aget-object v1, v2, v3

    check-cast v1, Landroid/content/Context;

    .line 36
    .local v1, "context":Landroid/content/Context;
    const/4 v3, 0x1

    aget-object v0, v2, v3

    check-cast v0, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;

    .line 38
    .local v0, "cmdType":Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;
    sget-object v3, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;->INITIALIZE:Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;

    if-ne v0, v3, :cond_2

    .line 39
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd;->init(Landroid/content/Context;)V

    goto :goto_0

    .line 40
    :cond_2
    sget-object v3, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;->USE_MULTI_CORE_CPU:Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;

    if-ne v0, v3, :cond_0

    .line 41
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd;->useMultiCoreCpu()V

    goto :goto_0
.end method
