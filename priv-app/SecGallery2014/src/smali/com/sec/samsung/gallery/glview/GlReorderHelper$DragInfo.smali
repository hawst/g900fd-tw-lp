.class Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;
.super Ljava/lang/Object;
.source "GlReorderHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlReorderHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DragInfo"
.end annotation


# instance fields
.field public mActive:Z

.field private mDnAreaY:F

.field private mThumbOjbectHeight:F

.field private mUpAreaY:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)V
    .locals 1

    .prologue
    .line 582
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 583
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->mActive:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/glview/GlReorderHelper;Lcom/sec/samsung/gallery/glview/GlReorderHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/glview/GlReorderHelper;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/glview/GlReorderHelper$1;

    .prologue
    .line 582
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;-><init>(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)V

    return-void
.end method


# virtual methods
.method public getIntensity(FF)F
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 598
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$1500(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-result-object v0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeightSpace:F

    div-float/2addr v0, v2

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$1400(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v1

    sub-float/2addr v0, v1

    cmpl-float v0, p2, v0

    if-lez v0, :cond_0

    .line 602
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$1500(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-result-object v0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeightSpace:F

    div-float/2addr v0, v2

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$1400(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v1

    sub-float p2, v0, v1

    .line 608
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->mThumbOjbectHeight:F

    const/high16 v1, 0x40800000    # 4.0f

    div-float/2addr v0, v1

    sub-float/2addr p2, v0

    .line 609
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->mUpAreaY:F

    cmpl-float v0, p2, v0

    if-lez v0, :cond_1

    .line 610
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->mUpAreaY:F

    sub-float v0, p2, v0

    .line 614
    :goto_0
    return v0

    .line 612
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->mDnAreaY:F

    cmpg-float v0, p2, v0

    if-gez v0, :cond_2

    .line 613
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->mDnAreaY:F

    sub-float v0, p2, v0

    goto :goto_0

    .line 614
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reset()V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 589
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$1400(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$1300(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewThumbnailHeight(I)F

    move-result v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$1400(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mViewMode:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$1300(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNameTextboxHeight(I)F

    move-result v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->mThumbOjbectHeight:F

    .line 592
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$1500(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-result-object v0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeightSpace:F

    div-float/2addr v0, v3

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->mThumbOjbectHeight:F

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$1400(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->mUpAreaY:F

    .line 594
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$1500(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-result-object v0

    iget v0, v0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeightSpace:F

    neg-float v0, v0

    div-float/2addr v0, v3

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->mThumbOjbectHeight:F

    const/high16 v2, 0x40400000    # 3.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->mDnAreaY:F

    .line 595
    return-void
.end method
