.class Lcom/sec/samsung/gallery/view/eventview/EventViewState$ComposeViewConfig;
.super Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;
.source "EventViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/eventview/EventViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ComposeViewConfig"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1166
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$ComposeViewConfig;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;-><init>()V

    .line 1167
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$ComposeViewConfig;->mUsePenSelectInPickMode:Z

    .line 1168
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$ComposeViewConfig;->mUseEnlargeAnim:Z

    .line 1169
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$ComposeViewConfig;->mUseLayoutChange:Z

    .line 1170
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$ComposeViewConfig;->mUseGroupSelect:Z

    .line 1171
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$ComposeViewConfig;->mUseGroupTitle:Z

    .line 1172
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$ComposeViewConfig;->mUseItemSelect:Z

    .line 1173
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$ComposeViewConfig;->mHideIconMinLevel:Z

    .line 1175
    iput v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$ComposeViewConfig;->mInitialLevel:I

    .line 1176
    iput v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$ComposeViewConfig;->mMinLevel:I

    .line 1177
    iput v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$ComposeViewConfig;->mMaxLevel:I

    .line 1178
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$ComposeViewConfig;->mTopGroupTitle:Z

    .line 1179
    new-array v0, v0, [Ljava/lang/Object;

    const-class v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$ComposeViewConfig;->mPosCtrl:[Ljava/lang/Object;

    .line 1180
    return-void
.end method
