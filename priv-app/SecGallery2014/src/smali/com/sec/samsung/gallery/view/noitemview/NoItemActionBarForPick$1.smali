.class Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick$1;
.super Ljava/lang/Object;
.source "NoItemActionBarForPick.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 41
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->access$000(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030006

    invoke-virtual {v3, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 43
    .local v0, "customView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 45
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->access$100(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 47
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->access$200(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 48
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->access$300(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ActionBar;->removeAllTabs()V

    .line 49
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->access$400(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/app/ActionBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 51
    const/16 v1, 0xc

    .line 53
    .local v1, "flags":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->access$500(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 54
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->access$600(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 55
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "android.intent.action.PERSON_PICK"

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 56
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->access$700(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 57
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    const-string v4, "help_mode"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    # setter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->mIsHelpMode:Z
    invoke-static {v3, v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->access$802(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;Z)Z

    .line 58
    return-void
.end method
