.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;
.super Landroid/os/Handler;
.source "PhotoViewComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 198
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 201
    const/4 v0, -0x1

    .line 203
    .local v0, "adjustedIndex":I
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 243
    :goto_0
    return-void

    .line 205
    :pswitch_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$100(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isPickMode()Z

    move-result v2

    if-nez v2, :cond_0

    .line 206
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumPath:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 207
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$100(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumPath:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getAlbumIndexFromFilePath(Ljava/lang/String;)I

    move-result v1

    .line 208
    .local v1, "newIndex":I
    if-eq v1, v4, :cond_0

    .line 209
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iput v1, v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    .line 211
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    const/4 v3, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumPath:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$202(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Ljava/lang/String;)Ljava/lang/String;

    .line 222
    .end local v1    # "newIndex":I
    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # invokes: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->checkAlbumState(I)V
    invoke-static {v2, v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$300(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;I)V

    .line 223
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$100(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 224
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$100(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget v4, v4, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget v4, v4, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    # invokes: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateNewMarkIconInfo(Lcom/sec/android/gallery3d/data/MediaSet;I)V
    invoke-static {v2, v3, v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$400(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/android/gallery3d/data/MediaSet;I)V

    .line 226
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$500(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    goto :goto_0

    .line 214
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$100(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getAlbumIndexFromMediaSet(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v1

    .line 215
    .restart local v1    # "newIndex":I
    if-ne v1, v4, :cond_3

    .line 216
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iput v5, v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    goto :goto_1

    .line 217
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget v2, v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    if-eq v1, v2, :cond_0

    .line 218
    move v0, v1

    goto :goto_1

    .line 229
    .end local v1    # "newIndex":I
    :pswitch_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$100(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 230
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$100(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getAlbumIndexFromMediaSet(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v1

    .line 231
    .restart local v1    # "newIndex":I
    if-ne v1, v4, :cond_5

    .line 232
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iput v5, v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    .line 237
    .end local v1    # "newIndex":I
    :cond_4
    :goto_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # invokes: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->checkAlbumState(I)V
    invoke-static {v2, v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$300(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;I)V

    goto/16 :goto_0

    .line 233
    .restart local v1    # "newIndex":I
    :cond_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget v2, v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    if-eq v1, v2, :cond_4

    .line 234
    move v0, v1

    goto :goto_2

    .line 240
    .end local v1    # "newIndex":I
    :pswitch_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateAlbumNameOfActionBar()V

    goto/16 :goto_0

    .line 203
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
