.class Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$1;
.super Ljava/lang/Object;
.source "EditCategoryDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->initDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$1;->this$0:Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$1;->this$0:Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->access$000(Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$1;->this$0:Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mResult:Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$Result;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->access$200(Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;)Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$Result;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$1;->this$0:Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mItem:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->access$100(Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$Result;->done(Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$1;->this$0:Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->dimiss()V

    .line 58
    :cond_0
    return-void
.end method
