.class public Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;
.super Ljava/lang/Object;
.source "GlComposeViewAccessibility.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;
    }
.end annotation


# instance fields
.field mAccessibilityNodeInfoListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

.field private mContext:Landroid/content/Context;

.field private mView:Lcom/sec/android/gallery3d/glcore/GlRootView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlRootView;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Lcom/sec/android/gallery3d/glcore/GlRootView;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->mContext:Landroid/content/Context;

    .line 26
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 27
    return-void
.end method


# virtual methods
.method public createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;
    .locals 10
    .param p1, "virtualDescendantId"    # I

    .prologue
    const/16 v9, 0x2000

    const/16 v8, 0x1000

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 39
    const/4 v3, 0x0

    .line 40
    .local v3, "info":Landroid/view/accessibility/AccessibilityNodeInfo;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->getFocusedIndex()I

    move-result v0

    .line 41
    .local v0, "focusedIndex":I
    const/4 v5, -0x1

    if-ne p1, v5, :cond_2

    .line 42
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-static {v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain(Landroid/view/View;)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v3

    .line 43
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v5, v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 44
    invoke-virtual {v3, v6}, Landroid/view/accessibility/AccessibilityNodeInfo;->setScrollable(Z)V

    .line 45
    invoke-virtual {v3, v6}, Landroid/view/accessibility/AccessibilityNodeInfo;->setEnabled(Z)V

    .line 46
    invoke-virtual {v3, v9}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 47
    invoke-virtual {v3, v8}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 48
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->mContext:Landroid/content/Context;

    check-cast v5, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->isVisibleState()Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v4, v3

    .line 80
    .end local v3    # "info":Landroid/view/accessibility/AccessibilityNodeInfo;
    .local v4, "info":Landroid/view/accessibility/AccessibilityNodeInfo;
    :goto_0
    return-object v4

    .line 50
    .end local v4    # "info":Landroid/view/accessibility/AccessibilityNodeInfo;
    .restart local v3    # "info":Landroid/view/accessibility/AccessibilityNodeInfo;
    :cond_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->mAccessibilityNodeInfoListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

    if-eqz v5, :cond_1

    .line 51
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->mAccessibilityNodeInfoListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

    invoke-interface {v5, v3}, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;->update(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    :cond_1
    :goto_1
    move-object v4, v3

    .line 80
    .end local v3    # "info":Landroid/view/accessibility/AccessibilityNodeInfo;
    .restart local v4    # "info":Landroid/view/accessibility/AccessibilityNodeInfo;
    goto :goto_0

    .line 53
    .end local v4    # "info":Landroid/view/accessibility/AccessibilityNodeInfo;
    .restart local v3    # "info":Landroid/view/accessibility/AccessibilityNodeInfo;
    :cond_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->mAccessibilityNodeInfoListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

    if-eqz v5, :cond_1

    .line 54
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->mAccessibilityNodeInfoListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

    invoke-interface {v5, p1}, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;->getOject(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v2

    .line 55
    .local v2, "glThumbObject":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz v2, :cond_1

    .line 56
    invoke-static {}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v3

    .line 57
    const/16 v5, 0x40

    invoke-virtual {v3, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 58
    const/16 v5, 0x80

    invoke-virtual {v3, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 59
    const/4 v5, 0x4

    invoke-virtual {v3, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 60
    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 61
    const/16 v5, 0x10

    invoke-virtual {v3, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 62
    invoke-virtual {v3, v9}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 63
    invoke-virtual {v3, v8}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 64
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setPackageName(Ljava/lang/CharSequence;)V

    .line 65
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 66
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v3, v5, p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setSource(Landroid/view/View;I)V

    .line 67
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 68
    .local v1, "glObjectRect":Landroid/graphics/Rect;
    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getGlObjectRect(Landroid/graphics/Rect;)V

    .line 69
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v3, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setParent(Landroid/view/View;)V

    .line 70
    invoke-virtual {v3, v6}, Landroid/view/accessibility/AccessibilityNodeInfo;->setEnabled(Z)V

    .line 71
    iget v5, v1, Landroid/graphics/Rect;->top:I

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getActionBarSize(Landroid/content/Context;)I

    move-result v8

    if-ge v5, v8, :cond_3

    .line 72
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getActionBarSize(Landroid/content/Context;)I

    move-result v5

    iput v5, v1, Landroid/graphics/Rect;->top:I

    .line 73
    :cond_3
    invoke-virtual {v3, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBoundsInScreen(Landroid/graphics/Rect;)V

    .line 74
    if-ne p1, v0, :cond_5

    move v5, v6

    :goto_2
    invoke-virtual {v3, v5}, Landroid/view/accessibility/AccessibilityNodeInfo;->setFocused(Z)V

    .line 75
    if-ne p1, v0, :cond_4

    move v7, v6

    :cond_4
    invoke-virtual {v3, v7}, Landroid/view/accessibility/AccessibilityNodeInfo;->setAccessibilityFocused(Z)V

    .line 76
    invoke-virtual {v3, v6}, Landroid/view/accessibility/AccessibilityNodeInfo;->setVisibleToUser(Z)V

    goto :goto_1

    :cond_5
    move v5, v7

    .line 74
    goto :goto_2
.end method

.method public performAction(IILandroid/os/Bundle;)Z
    .locals 22
    .param p1, "virtualViewId"    # I
    .param p2, "action"    # I
    .param p3, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 84
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->mView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    move-result-object v18

    check-cast v18, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;

    .line 85
    .local v18, "accessiblityNodeProvider":Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;
    invoke-virtual/range {v18 .. v18}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->getFocusedIndex()I

    move-result v19

    .line 86
    .local v19, "focusedIndex":I
    sparse-switch p2, :sswitch_data_0

    .line 126
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 88
    :sswitch_0
    move/from16 v0, v19

    move/from16 v1, p1

    if-eq v0, v1, :cond_0

    .line 89
    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->setFocusedIndex(I)V

    .line 91
    :cond_0
    const/16 v20, 0x0

    .line 92
    .local v20, "glThumb":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->mAccessibilityNodeInfoListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

    if-eqz v2, :cond_1

    .line 93
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->mAccessibilityNodeInfoListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

    move/from16 v0, p1

    invoke-interface {v2, v0}, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;->getOject(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v20

    .line 95
    :cond_1
    if-nez v20, :cond_2

    .line 96
    const/4 v2, 0x0

    goto :goto_0

    .line 97
    :cond_2
    invoke-virtual/range {v20 .. v20}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getCenterX()F

    move-result v7

    .line 98
    .local v7, "glObjectCenterX":F
    invoke-virtual/range {v20 .. v20}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getCenterY()F

    move-result v8

    .line 99
    .local v8, "glObjectCenterY":F
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/16 v6, 0x3eb

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v21

    .line 102
    .local v21, "motionEvent":Landroid/view/MotionEvent;
    invoke-virtual/range {v20 .. v21}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->onTouch(Landroid/view/MotionEvent;)Z

    .line 103
    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->setCurrentSelectedItem(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 104
    const v2, 0x8000

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->sendAccessibilityEventForVirtualView(Lcom/sec/android/gallery3d/glcore/GlObject;I)Z

    .line 105
    const/4 v2, 0x1

    goto :goto_0

    .line 108
    .end local v7    # "glObjectCenterX":F
    .end local v8    # "glObjectCenterY":F
    .end local v20    # "glThumb":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .end local v21    # "motionEvent":Landroid/view/MotionEvent;
    :sswitch_1
    move/from16 v0, v19

    move/from16 v1, p1

    if-ne v0, v1, :cond_5

    .line 109
    const/4 v2, -0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->setFocusedIndex(I)V

    .line 110
    const/16 v20, 0x0

    .line 111
    .restart local v20    # "glThumb":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->mAccessibilityNodeInfoListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

    if-eqz v2, :cond_3

    .line 112
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->mAccessibilityNodeInfoListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

    move/from16 v0, p1

    invoke-interface {v2, v0}, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;->getOject(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v20

    .line 114
    :cond_3
    if-nez v20, :cond_4

    .line 115
    const/4 v2, 0x0

    goto :goto_0

    .line 116
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v12

    const/16 v14, 0x3eb

    const/high16 v15, -0x40800000    # -1.0f

    const/high16 v16, -0x40800000    # -1.0f

    const/16 v17, 0x0

    invoke-static/range {v10 .. v17}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v21

    .line 119
    .restart local v21    # "motionEvent":Landroid/view/MotionEvent;
    invoke-virtual/range {v20 .. v21}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->onTouch(Landroid/view/MotionEvent;)Z

    .line 120
    const/high16 v2, 0x10000

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/accessibility/GlAccessibilityNodeProvider;->sendAccessibilityEventForVirtualView(Lcom/sec/android/gallery3d/glcore/GlObject;I)Z

    .line 123
    .end local v20    # "glThumb":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .end local v21    # "motionEvent":Landroid/view/MotionEvent;
    :cond_5
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 86
    :sswitch_data_0
    .sparse-switch
        0x40 -> :sswitch_0
        0x80 -> :sswitch_1
    .end sparse-switch
.end method

.method public setAccessibilityNodeInfoListener(Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->mAccessibilityNodeInfoListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

    .line 36
    return-void
.end method
