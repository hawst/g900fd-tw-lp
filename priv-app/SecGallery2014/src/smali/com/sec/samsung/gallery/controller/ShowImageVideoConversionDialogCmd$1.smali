.class Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd$1;
.super Ljava/lang/Object;
.source "ShowImageVideoConversionDialogCmd.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->showDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 68
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;)Landroid/content/Context;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/sec/samsung/gallery/controller/SoundSceneCmd$SoundSceneCmdType;->STOP_PLAY_SOUND:Lcom/sec/samsung/gallery/controller/SoundSceneCmd$SoundSceneCmdType;

    aput-object v3, v1, v2

    .line 71
    .local v1, "paramsStopSound":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    .line 72
    .local v0, "galleryFacade":Lcom/sec/samsung/gallery/core/GalleryFacade;
    const-string v2, "SOUND_SCENE"

    invoke-virtual {v0, v2, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 74
    packed-switch p2, :pswitch_data_0

    .line 84
    :goto_0
    return-void

    .line 76
    :pswitch_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;

    # invokes: Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->showShareDialog()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->access$100(Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;)V

    .line 77
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;

    # invokes: Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->dismissDialog()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->access$200(Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;)V

    goto :goto_0

    .line 80
    :pswitch_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;

    # invokes: Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->convertMultiFormatToMP4()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->access$300(Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;)V

    .line 81
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;

    # invokes: Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->dismissDialog()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;->access$200(Lcom/sec/samsung/gallery/controller/ShowImageVideoConversionDialogCmd;)V

    goto :goto_0

    .line 74
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
