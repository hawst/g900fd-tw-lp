.class public Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
.super Ljava/lang/Object;
.source "PhotoViewComponent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;,
        Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;
    }
.end annotation


# static fields
.field private static final DEFAULT_MEDIASET_INDEX:I = 0x0

.field protected static final GROUP_ALL_BURST_IMAGES:I = -0x1

.field private static final MSG_ALBUM_LOADING_FINISHED:I = 0x0

.field private static final MSG_PHOTO_LOADING_FINISHED:I = 0x1

.field private static final MSG_UPDATE_ACTIONBAR:I = 0x2

.field private static final TAG:Ljava/lang/String;

.field protected static final UNGROUP_ALL_BURST_IMAGES:I = -0x2


# instance fields
.field private NWAB:Ljava/lang/String;

.field private isfromEditmode:Z

.field private mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field public mAlbumModelListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

.field public mCurrentMediaItemIndex:I

.field public mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field public mCurrentMediaSetIndex:I

.field protected mCurrentMediaSetPath:Ljava/lang/String;

.field protected mCurrentTopMediaSetPath:Ljava/lang/String;

.field private mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

.field private mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

.field public mFilteredItems:Z

.field public mFromAlbumViewState:Z

.field public mFromGalleryWidget:Z

.field public mFromGifMaker:Z

.field public mFromInsideGallery:Z

.field private mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

.field private mGroupIDList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mHandleDismissDialogsTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

.field public mIsCategoryTagType:Z

.field private mIsDeleteMode:Z

.field public mIsFromCamera:Z

.field private mIsGifMode:Z

.field public mIsNewAlbumHeaderSelected:Z

.field public mIsNoSplitMode:Z

.field private mIsPenSelect:Z

.field public mIsPickMode:Z

.field public mIsSingleAlbumMode:Z

.field private final mMainHandler:Landroid/os/Handler;

.field private mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

.field private mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

.field private mMenu:Landroid/view/Menu;

.field public mNewAlbumMode:Z

.field protected mNewAlbumName:Ljava/lang/String;

.field private mNewAlbumPath:Ljava/lang/String;

.field private mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mOnComposeAlbumClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

.field protected mOnComposeAlbumLongClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemLongClickListener;

.field private mOnComposeItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

.field private mOnComposeItemLongClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemLongClickListener;

.field private mOnComposeKeyListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnKeyListener;

.field private mOnComposeStatusChangedListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;

.field private mOnGenericMotionListenerPhoto:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;

.field private mOnGenericMotionListenerSplit:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;

.field private mOnHoverClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnHoverListener;

.field private mOnPenSelectionListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;

.field private mOnSwitchViewListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnSwitchViewListener;

.field public mPhotoModelListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

.field private mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

.field private mPhotoViewExitSelectionMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mPhotoViewMediaEjectMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mPhotoViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

.field private mRemovedCntInNewAlbumHeader:I

.field private mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mSelectedAlbumItemIndex:I

.field private mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mShowSlideshowsettingsMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

.field private mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

.field private mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field private mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

.field public mTopMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mUpdatePath:Z

.field public misFilteredForStoryAlbum:Z

.field public misFilteredForVideoAlbum:Z

.field private tempTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

.field private unmountedStorage:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 129
    const-class v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;)V
    .locals 3
    .param p1, "photoViewState"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    const-string v0, "NWAB"

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->NWAB:Ljava/lang/String;

    .line 161
    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectedAlbumItemIndex:I

    .line 162
    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mRemovedCntInNewAlbumHeader:I

    .line 171
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    .line 173
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    invoke-direct {v0, p0, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->tempTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    .line 174
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGroupIDList:Ljava/util/ArrayList;

    .line 181
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mFromAlbumViewState:Z

    .line 182
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsFromCamera:Z

    .line 184
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mFilteredItems:Z

    .line 185
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsNoSplitMode:Z

    .line 186
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumMode:Z

    .line 187
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsCategoryTagType:Z

    .line 188
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->misFilteredForStoryAlbum:Z

    .line 189
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->misFilteredForVideoAlbum:Z

    .line 190
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsNewAlbumHeaderSelected:Z

    .line 191
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isfromEditmode:Z

    .line 192
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsDeleteMode:Z

    .line 193
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mUpdatePath:Z

    .line 194
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPenSelect:Z

    .line 195
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsGifMode:Z

    .line 196
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->unmountedStorage:Ljava/lang/String;

    .line 198
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMainHandler:Landroid/os/Handler;

    .line 557
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;

    const-string v1, "PHOTO_VIEW"

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$2;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 642
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$3;

    const-string v1, "PHOTO_VIEW_EXIT_SELECTION"

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$3;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewExitSelectionMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 681
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$4;

    const-string v1, "PHOTO_VIEW_MEDIA_EJECT"

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$4;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewMediaEjectMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 707
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$5;

    const-string v1, "SLIDESHOW_SETTINGS"

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$5;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mShowSlideshowsettingsMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 2655
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$14;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$14;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mAlbumModelListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

    .line 2673
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$15;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$15;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoModelListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

    .line 2728
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$16;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$16;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mOnComposeStatusChangedListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;

    .line 2760
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$17;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$17;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mOnComposeAlbumClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    .line 2772
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$18;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$18;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mOnComposeItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    .line 2782
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$19;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$19;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mOnComposeAlbumLongClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemLongClickListener;

    .line 2799
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mOnComposeItemLongClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemLongClickListener;

    .line 2838
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$21;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$21;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mOnComposeKeyListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnKeyListener;

    .line 2861
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$22;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$22;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mOnHoverClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnHoverListener;

    .line 2869
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$23;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$23;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mOnGenericMotionListenerPhoto:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;

    .line 2896
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$24;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$24;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mOnGenericMotionListenerSplit:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;

    .line 2925
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$25;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$25;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mOnSwitchViewListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnSwitchViewListener;

    .line 2946
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mOnPenSelectionListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;

    .line 314
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    .line 315
    iget-object v0, p1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 316
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->getActionBarManager()Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 317
    iget-object v0, p1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    .line 318
    iget-object v0, p1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 319
    iget-object v0, p1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 320
    iget-object v0, p1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 321
    iget-object v0, p1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 323
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->getGalleryActivity()Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 324
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    .line 325
    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 128
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->enterNewAlbumMode(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
    .param p1, "x1"    # Z

    .prologue
    .line 128
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->addToSlideshowProxy(Z)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mHandleDismissDialogsTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;)Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mHandleDismissDialogsTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->tempTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->unmountedStorage:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->unmountedStorage:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/core/GalleryFacade;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;
    .param p2, "x2"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 128
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->switchTopMediaSet(Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;Lcom/sec/android/gallery3d/data/MediaSet;)V

    return-void
.end method

.method static synthetic access$202(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumPath:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Landroid/view/Menu;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/glcore/GlRootView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
    .param p1, "x1"    # Z

    .prologue
    .line 128
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->startSlideShow(Z)V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 128
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateActionBarSelectionMode(Lcom/sec/android/gallery3d/data/MediaSet;)V

    return-void
.end method

.method static synthetic access$2600(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->setNewMarkVisibility()V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->postUpdateAlbumNameOfActionBar()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
    .param p1, "x1"    # I

    .prologue
    .line 128
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->checkAlbumState(I)V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
    .param p1, "x1"    # I

    .prologue
    .line 128
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->showItemCopyMoveDialog(I)V

    return-void
.end method

.method static synthetic access$3200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsGifMode:Z

    return v0
.end method

.method static synthetic access$3302(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
    .param p1, "x1"    # Z

    .prologue
    .line 128
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPenSelect:Z

    return p1
.end method

.method static synthetic access$3400(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mRemovedCntInNewAlbumHeader:I

    return v0
.end method

.method static synthetic access$3402(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
    .param p1, "x1"    # I

    .prologue
    .line 128
    iput p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mRemovedCntInNewAlbumHeader:I

    return p1
.end method

.method static synthetic access$3500(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGroupIDList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/android/gallery3d/data/MediaSet;IILcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;

    .prologue
    .line 128
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->selectItemPickMode(Lcom/sec/android/gallery3d/data/MediaSet;IILcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;)V

    return-void
.end method

.method static synthetic access$3700(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "x2"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;

    .prologue
    .line 128
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->selectItemFilterMode(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;)V

    return-void
.end method

.method static synthetic access$3800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "x2"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;

    .prologue
    .line 128
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->selectItemNewAlbumMode(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;)V

    return-void
.end method

.method static synthetic access$3900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/data/DataManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/android/gallery3d/data/MediaSet;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "x2"    # I

    .prologue
    .line 128
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateNewMarkIconInfo(Lcom/sec/android/gallery3d/data/MediaSet;I)V

    return-void
.end method

.method static synthetic access$4000(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/utils/EditModeHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateHideItemsOptionMenu()V

    return-void
.end method

.method static synthetic access$4200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/StateManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
    .param p1, "x1"    # I

    .prologue
    .line 128
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->startDetailViewState(I)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->startCollageViewer()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method private addToSlideshowProxy(Z)V
    .locals 10
    .param p1, "burstPlay"    # Z

    .prologue
    .line 1654
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSlideShowSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    .line 1657
    .local v4, "slideShowProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    const/4 v0, 0x0

    .line 1658
    .local v0, "count":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1659
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 1665
    :goto_0
    :try_start_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1666
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isfromEditmode:Z

    .line 1667
    new-instance v5, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v7, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$10;

    invoke-direct {v7, p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$10;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Z)V

    const/4 v8, 0x1

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v9

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;ZI)V

    const/4 v6, 0x1

    new-array v6, v6, [Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1716
    :goto_1
    return-void

    .line 1661
    :cond_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getAllCount()I

    move-result v0

    goto :goto_0

    .line 1689
    :cond_1
    const/4 v5, 0x1

    :try_start_1
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isfromEditmode:Z

    .line 1690
    const/4 v3, 0x0

    .local v3, "position":I
    :goto_2
    if-ge v3, v0, :cond_7

    .line 1692
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1693
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5, v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1694
    .local v2, "media":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v5, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-nez v5, :cond_3

    instance-of v5, v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-eqz v5, :cond_2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v5

    const/4 v6, 0x4

    if-eq v5, v6, :cond_3

    :cond_2
    instance-of v5, v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    if-nez v5, :cond_3

    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloud:Z

    if-eqz v5, :cond_5

    instance-of v5, v2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    if-nez v5, :cond_3

    instance-of v5, v2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    if-eqz v5, :cond_5

    .line 1690
    :cond_3
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1701
    .end local v2    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_4
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    const/4 v6, 0x0

    invoke-virtual {v5, v6, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .line 1704
    .restart local v2    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_5
    instance-of v5, v2, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v5, :cond_6

    .line 1705
    check-cast v2, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    .end local v2    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;

    move-result-object v2

    .line 1708
    .restart local v2    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_6
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v5

    if-nez v5, :cond_3

    .line 1709
    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 1713
    .end local v2    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v3    # "position":I
    :catch_0
    move-exception v1

    .line 1714
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 1711
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v3    # "position":I
    :cond_7
    :try_start_2
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->startSlideShow(Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1
.end method

.method private cancelSwitchFilterTask()V
    .locals 2

    .prologue
    .line 1575
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    if-eqz v0, :cond_0

    .line 1576
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->cancel(Z)Z

    .line 1577
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    .line 1579
    :cond_0
    return-void
.end method

.method private checkAlbumState(I)V
    .locals 10
    .param p1, "adjustedIndex"    # I

    .prologue
    const/4 v9, -0x1

    const/4 v8, 0x0

    .line 247
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    if-nez v6, :cond_1

    .line 311
    :cond_0
    :goto_0
    return-void

    .line 249
    :cond_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getCount()I

    move-result v5

    .line 250
    .local v5, "size":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getCount()I

    move-result v0

    .line 251
    .local v0, "albumSize":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v6

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->IsCategoryView(Lcom/sec/samsung/gallery/core/TabTagType;Ljava/lang/Boolean;)Z

    move-result v6

    if-eqz v6, :cond_2

    if-nez v5, :cond_2

    .line 252
    const/4 v0, 0x0

    .line 255
    :cond_2
    if-nez v5, :cond_6

    if-nez v0, :cond_6

    .line 256
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 257
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v6, v8}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->setSlideshowSettingEnable(Z)V

    .line 259
    :cond_3
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v6

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->IsCategoryView(Lcom/sec/samsung/gallery/core/TabTagType;Ljava/lang/Boolean;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 260
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 261
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v6

    const-class v7, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0

    .line 262
    :cond_4
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsSingleAlbumMode:Z

    if-eqz v6, :cond_5

    .line 263
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v3

    .line 264
    .local v3, "mimeType":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->startNoItemViewState(Ljava/lang/String;)V

    goto :goto_0

    .line 267
    .end local v3    # "mimeType":Ljava/lang/String;
    :cond_5
    :try_start_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 268
    :catch_0
    move-exception v1

    .line 269
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 272
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_6
    if-nez v5, :cond_b

    if-lez v0, :cond_b

    .line 273
    iget v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    .line 274
    .local v4, "position":I
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMoveFirstAlbum:Z

    if-eqz v6, :cond_9

    .line 275
    const/4 v4, 0x0

    .line 283
    :cond_7
    :goto_1
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsNewAlbumHeaderSelected:Z

    if-eqz v6, :cond_8

    .line 284
    iput-boolean v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsNewAlbumHeaderSelected:Z

    .line 285
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iput-boolean v8, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mNotNeedToResetLayout:Z

    .line 286
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->setNewAlbumUnfocused()V

    .line 287
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateNewAlbumActionBar()V

    .line 289
    :cond_8
    invoke-virtual {p0, v4, v8}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->onAlbumClick(IZ)V

    goto/16 :goto_0

    .line 277
    :cond_9
    if-lt v4, v0, :cond_a

    .line 278
    add-int/lit8 v4, v0, -0x1

    goto :goto_1

    .line 279
    :cond_a
    if-gez v4, :cond_7

    .line 280
    const/4 v4, 0x0

    goto :goto_1

    .line 291
    .end local v4    # "position":I
    :cond_b
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->removeNoItemLayout(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 292
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mTopMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-nez v6, :cond_c

    .line 293
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentTopMediaSetPath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mTopMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 294
    :cond_c
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mTopMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v6, :cond_0

    .line 296
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mTopMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iget v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 297
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v6, :cond_e

    if-eqz v2, :cond_e

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v6

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v7

    if-eq v6, v7, :cond_e

    .line 299
    if-eq p1, v9, :cond_d

    .line 300
    iput p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    .line 301
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-virtual {v6, v7, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->selectAlbum(IZ)V

    goto/16 :goto_0

    .line 303
    :cond_d
    iget v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->onAlbumClick(I)V

    goto/16 :goto_0

    .line 304
    :cond_e
    if-nez v2, :cond_0

    .line 305
    if-eq p1, v9, :cond_0

    .line 306
    iput p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    .line 307
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-virtual {v6, v7, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->selectAlbum(IZ)V

    goto/16 :goto_0
.end method

.method private static checkDeletableMediaObject(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 4
    .param p0, "mediaObj"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 3390
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaObject;->getSupportedOperations()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkUseCreateVideoAlbum(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1006
    const/4 v1, 0x0

    .line 1008
    .local v1, "info":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "com.sec.android.app.storycam"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1015
    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    move v2, v3

    goto :goto_0

    .line 1010
    :catch_0
    move-exception v0

    .line 1015
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    if-nez v1, :cond_0

    move v2, v3

    goto :goto_0

    .line 1012
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 1013
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1015
    if-nez v1, :cond_0

    move v2, v3

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    if-nez v1, :cond_0

    move v2, v3

    goto :goto_0
.end method

.method private cloudSync()V
    .locals 3

    .prologue
    .line 2277
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCloud:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->isCloudContentSyncOn(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2279
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    if-eqz v1, :cond_0

    .line 2281
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 2282
    .local v0, "curMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v0, :cond_0

    .line 2283
    instance-of v1, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;

    if-eqz v1, :cond_1

    .line 2284
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    check-cast v0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;

    .end local v0    # "curMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;->getBucketIdString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->cloudAlbumSync(Landroid/content/Context;Ljava/lang/String;)V

    .line 2296
    :cond_0
    :goto_0
    return-void

    .line 2288
    .restart local v0    # "curMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    instance-of v1, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;

    if-eqz v1, :cond_0

    .line 2289
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    check-cast v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;

    .end local v0    # "curMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;->getBucketIdString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->cloudAlbumSync(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private editBurstShot()V
    .locals 4

    .prologue
    .line 998
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->makeBurstShotList()Ljava/util/ArrayList;

    move-result-object v0

    .line 999
    .local v0, "burstShotList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v0, v1, v2

    .line 1000
    .local v1, "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v3, "SHOW_EDIT_BUSRT_SHOT_DIALOG"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1002
    return-void
.end method

.method private enterCreateVideoAlbum()V
    .locals 3

    .prologue
    .line 1024
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    .line 1025
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "CREATE_VIDEO_ALBUM"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1027
    return-void
.end method

.method private enterNewAlbumMode(Ljava/lang/String;)V
    .locals 3
    .param p1, "albumName"    # Ljava/lang/String;

    .prologue
    .line 1380
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumName:Ljava/lang/String;

    .line 1381
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->setNewAlbumName(Ljava/lang/String;)V

    .line 1382
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->enterSelectionMode(ZZ)V

    .line 1383
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateNewAlbumActionBar()V

    .line 1385
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->NWAB:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 1386
    return-void
.end method

.method private enterSearchMode()V
    .locals 3

    .prologue
    .line 1532
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOCR:Z

    if-eqz v0, :cond_0

    .line 1533
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setSearchMode(Z)V

    .line 1534
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1535
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForSearch;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1537
    :cond_0
    return-void
.end method

.method private getItemsToAddCount(Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1847
    .local p1, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v0, 0x0

    .line 1848
    .local v0, "itemsToAdd":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1849
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1850
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaObject;

    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1851
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1853
    :cond_1
    return v0
.end method

.method private getTopSetPathByFilter()Ljava/lang/String;
    .locals 3

    .prologue
    .line 3394
    const/4 v0, 0x0

    .line 3396
    .local v0, "basePath":Ljava/lang/String;
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mTopMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mTopMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v1, v1, Lcom/sec/android/gallery3d/data/FilterTypeSet;

    if-eqz v1, :cond_0

    .line 3397
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentTopMediaSetPath:Ljava/lang/String;

    .line 3402
    :goto_0
    return-object v0

    .line 3399
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/utils/DataPath;->getViewByTopSetPath(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getUserSelectedAlbum()[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 2636
    const/4 v3, 0x3

    new-array v1, v3, [Ljava/lang/String;

    const-string v3, "-1"

    aput-object v3, v1, v5

    const/4 v3, 0x1

    const-string v4, "-1"

    aput-object v4, v1, v3

    const-string v3, "-1"

    aput-object v3, v1, v6

    .line 2637
    .local v1, "info":[Ljava/lang/String;
    invoke-virtual {v1}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 2639
    .local v2, "tempInfo":[Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "user_selected_album"

    invoke-static {v3, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadStringKey(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2641
    .local v0, "Albuminfo":Ljava/lang/String;
    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2642
    const-string v3, ";"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 2644
    :cond_0
    array-length v3, v2

    if-ne v3, v6, :cond_1

    .line 2645
    array-length v3, v2

    invoke-static {v2, v5, v1, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2649
    :goto_0
    return-object v1

    .line 2647
    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method private handleFilterPhotos()V
    .locals 6

    .prologue
    .line 937
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v0

    .line 939
    .local v0, "mediaFilterType":Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->cancelSwitchFilterTask()V

    .line 941
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentTopMediaSetPath:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/gallery3d/app/FilterUtils;->toFilterType(Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/app/FilterUtils;->switchFilterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 944
    .local v1, "mediaPath":Ljava/lang/String;
    if-nez v1, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 947
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_0
    new-instance v3, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$6;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v3, p0, v4, v0, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$6;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Landroid/content/Context;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;Lcom/sec/android/gallery3d/data/MediaSet;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    .line 953
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 954
    return-void

    .line 944
    .end local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    goto :goto_0
.end method

.method private makeBurstShotList()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 973
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 974
    .local v0, "burstShotList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v1, 0x0

    .line 975
    .local v1, "count":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 976
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    .line 980
    :goto_0
    const/4 v3, 0x0

    .local v3, "position":I
    :goto_1
    if-ge v3, v1, :cond_4

    .line 982
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 983
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4, v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 984
    .local v2, "media":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v4, v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    if-nez v4, :cond_0

    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloud:Z

    if-eqz v4, :cond_3

    instance-of v4, v2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    if-nez v4, :cond_0

    instance-of v4, v2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    if-eqz v4, :cond_3

    .line 980
    :cond_0
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 978
    .end local v2    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v3    # "position":I
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getAllCount()I

    move-result v1

    goto :goto_0

    .line 989
    .restart local v3    # "position":I
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    const/4 v5, 0x0

    invoke-virtual {v4, v5, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .line 991
    .restart local v2    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_3
    if-eqz v2, :cond_0

    .line 992
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 994
    .end local v2    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_4
    return-object v0
.end method

.method private postUpdateAlbumNameOfActionBar()V
    .locals 4

    .prologue
    const/4 v1, 0x2

    .line 1048
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1049
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMainHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x50

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1050
    return-void
.end method

.method private removeItemsAddedToNewAlbumProxy(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1839
    .local p1, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 1840
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1841
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaObject;

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1842
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 1844
    :cond_1
    return-void
.end method

.method private selectAllItemPickMode(Lcom/sec/android/gallery3d/data/MediaSet;III)V
    .locals 13
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "maxAddCount"    # I
    .param p3, "currentSelectedCount"    # I
    .param p4, "maxPickCount"    # I

    .prologue
    .line 3156
    const/4 v4, 0x0

    .line 3157
    .local v4, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v7

    .line 3158
    .local v7, "totalCount":I
    div-int v5, v7, p4

    .line 3160
    .local v5, "step":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-gt v1, v5, :cond_0

    .line 3161
    if-gtz p2, :cond_1

    .line 3187
    :cond_0
    return-void

    .line 3163
    :cond_1
    mul-int v8, v1, p4

    move/from16 v0, p4

    invoke-virtual {p1, v8, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v4

    .line 3164
    const/4 v3, 0x0

    .local v3, "ix":I
    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v3, v8, :cond_2

    .line 3165
    if-gtz p2, :cond_3

    if-lez p4, :cond_3

    .line 3166
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v8, :cond_2

    .line 3167
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e0117

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 3169
    .local v6, "text":Ljava/lang/CharSequence;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 3160
    .end local v6    # "text":Ljava/lang/CharSequence;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3173
    :cond_3
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 3174
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v8, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isAvailableDrm(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v8

    if-nez v8, :cond_6

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 3175
    :cond_4
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v9, 0x7f0e0107

    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 3164
    :cond_5
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 3178
    :cond_6
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 3180
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8, p1, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 3181
    add-int/lit8 p2, p2, -0x1

    goto :goto_2
.end method

.method private selectAllProcess(III)V
    .locals 4
    .param p1, "maxAddCount"    # I
    .param p2, "currentSelectedCount"    # I
    .param p3, "maxPickCount"    # I

    .prologue
    .line 2070
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    invoke-direct {p0, v1, p1, p2, p3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->selectItem(Lcom/sec/android/gallery3d/data/MediaSet;III)V

    .line 2071
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 2072
    .local v0, "count":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isAvailableCount(IZZ)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateDoneButton(Z)V

    .line 2073
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 2074
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->refreshCheckState()V

    .line 2075
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->updateFolderItem(I)V

    .line 2077
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateActionBarSelectionMode(Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 2078
    return-void
.end method

.method private selectAllProcessTask(II)V
    .locals 7
    .param p1, "maxAddCount"    # I
    .param p2, "currentSelectedCount"    # I

    .prologue
    .line 2081
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v3, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$12;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$12;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v6

    move-object v1, p0

    move v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;III)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2108
    return-void
.end method

.method private selectItem(Lcom/sec/android/gallery3d/data/MediaSet;III)V
    .locals 10
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "maxAddCount"    # I
    .param p3, "currentSelectedCount"    # I
    .param p4, "maxPickCount"    # I

    .prologue
    const v9, 0x7f0e0107

    const/4 v8, 0x0

    .line 3030
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    if-eqz v7, :cond_2

    .line 3031
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v7, :cond_1

    const/4 v7, -0x1

    if-eq p4, v7, :cond_1

    .line 3032
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->selectAllItemPickMode(Lcom/sec/android/gallery3d/data/MediaSet;III)V

    .line 3075
    :cond_0
    :goto_0
    return-void

    .line 3034
    :cond_1
    invoke-direct {p0, p1, p2, p3, v8}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->selectItemPickMode(Lcom/sec/android/gallery3d/data/MediaSet;IILcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;)V

    goto :goto_0

    .line 3036
    :cond_2
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->misFilteredForStoryAlbum:Z

    if-nez v7, :cond_3

    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mFilteredItems:Z

    if-eqz v7, :cond_4

    .line 3037
    :cond_3
    invoke-direct {p0, p1, v8}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->selectItemFilterMode(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;)V

    goto :goto_0

    .line 3039
    :cond_4
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumMode:Z

    if-eqz v7, :cond_5

    .line 3040
    invoke-direct {p0, p1, v8}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->selectItemNewAlbumMode(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;)V

    goto :goto_0

    .line 3043
    :cond_5
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v0

    .line 3044
    .local v0, "albumCount":I
    if-lez v0, :cond_6

    .line 3045
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_0

    .line 3046
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 3045
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3050
    .end local v1    # "i":I
    :cond_6
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inDeleteSelectionMode()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-static {p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->checkDeletableMediaObject(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 3051
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7, v9}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 3055
    :cond_7
    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v8

    invoke-virtual {p1, v7, v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v3

    .line 3056
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getNewAlbumSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v5

    .line 3057
    .local v5, "newAlbumSelectionProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    const/4 v6, 0x1

    .line 3058
    .local v6, "showUnsupportedFileToast":Z
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_8
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 3059
    .local v4, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v7, v4, Lcom/sec/android/gallery3d/data/ActionImage;

    if-nez v7, :cond_8

    .line 3061
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inExpansionMode()Z

    move-result v7

    if-eqz v7, :cond_9

    invoke-virtual {v5, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 3063
    :cond_9
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inDeleteSelectionMode()Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-static {v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->checkDeletableMediaObject(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v7

    if-nez v7, :cond_a

    .line 3064
    if-eqz v6, :cond_8

    .line 3065
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7, v9}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 3066
    const/4 v6, 0x0

    goto :goto_2

    .line 3069
    :cond_a
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 3070
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7, p1, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 3071
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_2
.end method

.method private selectItemFilterMode(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;)V
    .locals 12
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "task"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const-wide/16 v8, 0x1

    .line 3114
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v1

    .line 3115
    .local v1, "albumCount":I
    const/4 v0, 0x0

    .line 3116
    .local v0, "addCount":I
    if-lez v1, :cond_3

    .line 3117
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 3118
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->isCancelled()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 3153
    :cond_0
    :goto_1
    return-void

    .line 3121
    :cond_1
    if-eqz p2, :cond_2

    .line 3122
    invoke-virtual {p2, v8, v9, v10}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->increaseProgress(JZ)V

    .line 3123
    invoke-virtual {p2, v8, v9, v11}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->increaseProgress(JZ)V

    .line 3126
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {p1, v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 3117
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 3129
    .end local v2    # "i":I
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v6

    invoke-virtual {p1, v10, v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v3

    .line 3131
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v2, 0x0

    .restart local v2    # "i":I
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    .local v5, "n":I
    :goto_2
    if-ge v2, v5, :cond_8

    .line 3132
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->isCancelled()Z

    move-result v6

    if-nez v6, :cond_0

    .line 3135
    :cond_4
    if-eqz p2, :cond_5

    .line 3136
    invoke-virtual {p2, v8, v9, v10}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->increaseProgress(JZ)V

    .line 3137
    invoke-virtual {p2, v8, v9, v11}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->increaseProgress(JZ)V

    .line 3140
    :cond_5
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 3141
    .local v4, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 3142
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaObject;->getMediaType()I

    move-result v6

    const/4 v7, 0x4

    if-ne v6, v7, :cond_7

    .line 3131
    :cond_6
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 3145
    :cond_7
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6, p1, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 3146
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 3149
    .end local v4    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_8
    if-nez v0, :cond_0

    .line 3150
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v7, 0x7f0e0107

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_1
.end method

.method private selectItemNewAlbumMode(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;)V
    .locals 11
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "task"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;

    .prologue
    const/4 v10, 0x1

    const/4 v7, 0x0

    const-wide/16 v8, 0x1

    .line 3081
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v0

    .line 3082
    .local v0, "albumCount":I
    if-lez v0, :cond_3

    .line 3083
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 3084
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->isCancelled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 3111
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 3087
    .restart local v1    # "i":I
    :cond_1
    if-eqz p2, :cond_2

    .line 3088
    invoke-virtual {p2, v8, v9, v7}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->increaseProgress(JZ)V

    .line 3089
    invoke-virtual {p2, v8, v9, v10}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->increaseProgress(JZ)V

    .line 3092
    :cond_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 3083
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3095
    .end local v1    # "i":I
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v5

    invoke-virtual {p1, v7, v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v3

    .line 3096
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 3097
    .local v4, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz p2, :cond_5

    invoke-virtual {p2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->isCancelled()Z

    move-result v5

    if-nez v5, :cond_0

    .line 3100
    :cond_5
    if-eqz p2, :cond_6

    .line 3101
    invoke-virtual {p2, v8, v9, v7}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->increaseProgress(JZ)V

    .line 3102
    invoke-virtual {p2, v8, v9, v10}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->increaseProgress(JZ)V

    .line 3105
    :cond_6
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 3106
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5, p1, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 3107
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_1
.end method

.method private selectItemPickMode(Lcom/sec/android/gallery3d/data/MediaSet;IILcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;)V
    .locals 9
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "maxAddCount"    # I
    .param p3, "currentSelectedCount"    # I
    .param p4, "task"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;

    .prologue
    .line 3190
    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v5

    invoke-virtual {p1, v4, v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v2

    .line 3191
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-ltz p2, :cond_1

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    sub-int/2addr v4, p3

    if-le v4, p2, :cond_1

    .line 3192
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0117

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 3194
    .local v3, "text":Ljava/lang/CharSequence;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 3217
    .end local v3    # "text":Ljava/lang/CharSequence;
    :cond_0
    return-void

    .line 3197
    :cond_1
    const/4 v1, 0x0

    .local v1, "ix":I
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 3198
    if-eqz p4, :cond_2

    invoke-virtual {p4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->isCancelled()Z

    move-result v4

    if-nez v4, :cond_0

    .line 3201
    :cond_2
    if-eqz p4, :cond_3

    .line 3202
    const-wide/16 v4, 0x1

    const/4 v6, 0x0

    invoke-virtual {p4, v4, v5, v6}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->increaseProgress(JZ)V

    .line 3203
    const-wide/16 v4, 0x1

    const/4 v6, 0x1

    invoke-virtual {p4, v4, v5, v6}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$SelectionTask;->increaseProgress(JZ)V

    .line 3206
    :cond_3
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 3207
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4, v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isAvailableDrm(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 3208
    :cond_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v5, 0x7f0e0107

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 3197
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3211
    :cond_5
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4, p1, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_1
.end method

.method private selectItems(Ljava/util/List;I)I
    .locals 4
    .param p2, "maxAddCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .line 2254
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-nez p2, :cond_1

    .line 2255
    const/4 v0, 0x0

    .line 2268
    :cond_0
    :goto_0
    return v0

    .line 2257
    :cond_1
    const/4 v0, 0x0

    .line 2258
    .local v0, "addCount":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 2259
    .local v2, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2260
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 2261
    add-int/lit8 v0, v0, 0x1

    .line 2263
    :cond_3
    if-lt v0, p2, :cond_2

    goto :goto_0
.end method

.method private setLatestSelectedAlbuminfo(Lcom/sec/android/gallery3d/data/MediaSet;I)Z
    .locals 11
    .param p1, "mediaset"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "position"    # I

    .prologue
    const/4 v5, 0x0

    .line 2605
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "latest_update_album"

    const/4 v10, -0x1

    invoke-static {v8, v9, v10}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v2

    .line 2607
    .local v2, "lastBucketID":I
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v0

    .line 2609
    .local v0, "bucketId":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v8, p2, v5}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    .line 2610
    .local v4, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v4, :cond_1

    .line 2622
    :cond_0
    :goto_0
    return v5

    .line 2613
    :cond_1
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v1

    .line 2614
    .local v1, "itemId":I
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v6

    .line 2615
    .local v6, "takenTime":J
    if-ne v0, v2, :cond_0

    .line 2616
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ";"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ";"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2617
    .local v3, "lastSelectInfo":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    const-string/jumbo v8, "user_selected_album"

    invoke-static {v5, v8, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2619
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v5, v0, v1, v6, v7}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->setNewMarkID(IIJ)V

    .line 2620
    const/4 v5, 0x1

    goto :goto_0
.end method

.method private setNewMarkVisibility()V
    .locals 8

    .prologue
    .line 2626
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->getUserSelectedAlbum()[Ljava/lang/String;

    move-result-object v4

    .line 2629
    .local v4, "userSelect":[Ljava/lang/String;
    const/4 v5, 0x1

    aget-object v5, v4, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 2630
    .local v0, "itemID":I
    const/4 v5, 0x2

    aget-object v5, v4, v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2631
    .local v2, "takenTime":J
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "latest_update_album"

    const/4 v7, -0x1

    invoke-static {v5, v6, v7}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v1

    .line 2632
    .local v1, "latestUpdatedID":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v5, v1, v0, v2, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->setNewMarkID(IIJ)V

    .line 2633
    return-void
.end method

.method private showItemCopyMoveDialog(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1517
    new-array v0, v3, [Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    aput-object v2, v0, v4

    .line 1520
    .local v0, "mediaSetArray":[Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v2, 0x4

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v1, v4

    aput-object v0, v1, v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v5

    const/4 v2, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1521
    .local v1, "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v3, "SHOW_COPY_MOVE_ALBUM_LIST_DIALOG"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1522
    return-void
.end method

.method private showNewAlbumCopyMoveDialog()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1525
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/gallery3d/util/GalleryUtils;->DEFAULT_NEW_ALBUM_DIR:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumPath:Ljava/lang/String;

    .line 1527
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumPath:Ljava/lang/String;

    aput-object v1, v0, v3

    const/4 v1, 0x2

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1528
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "SHOW_NEW_ALBUM_COPY_MOVE_DIALOG"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1529
    return-void
.end method

.method private startCollageViewer()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1497
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    iget v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaItemIndex:I

    invoke-virtual {v2, v4, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    .line 1499
    .local v1, "uri":Landroid/net/Uri;
    const/4 v2, 0x3

    new-array v0, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v4

    const/4 v2, 0x1

    sget-object v3, Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;->START_COLLAGE_VIEWER:Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;

    aput-object v3, v0, v2

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 1501
    .local v0, "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v3, "START_COLLAGE_APP"

    invoke-virtual {v2, v3, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1503
    return-void
.end method

.method private startDetailView(ILandroid/net/Uri;)V
    .locals 11
    .param p1, "position"    # I
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v10, 0x0

    .line 1389
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v8, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v5

    .line 1390
    .local v5, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v8

    if-gtz v8, :cond_1

    .line 1432
    :cond_0
    :goto_0
    return-void

    .line 1393
    :cond_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v9, 0x0

    invoke-virtual {v8, p2, v9}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    .line 1395
    .local v6, "path":Lcom/sec/android/gallery3d/data/Path;
    if-nez v6, :cond_2

    .line 1396
    sget-object v8, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    const-string v9, "path is null. cannot start detail view"

    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1399
    :cond_2
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v8, v6}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1400
    .local v4, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v4, :cond_3

    .line 1401
    sget-object v8, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    const-string v9, "cannot find media item. cannot start detail view"

    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/data/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1404
    :cond_3
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v8, v10}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showNewAlbumDialog(Z)V

    .line 1405
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v7

    .line 1406
    .local v7, "srcPath":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v3

    .line 1407
    .local v3, "dstPath":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1409
    .local v2, "dstItemPath":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    .line 1410
    .local v1, "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1411
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1412
    const-string v8, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1414
    const-string v8, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1416
    const-string v8, "KEY_MEDIA_SET_POSITION"

    invoke-virtual {v0, v8, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1417
    const-string v8, "KEY_ITEM_POSITION"

    invoke-virtual {v0, v8, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1429
    :goto_1
    const-string v8, "in_camera_roll"

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->isCameraRoll()Z

    move-result v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1431
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const-class v9, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v8, v9, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 1418
    :cond_4
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v4, v3, v8}, Lcom/sec/android/gallery3d/data/DataManager;->copyItem(Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 1419
    const-string v8, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1421
    const-string v8, "KEY_ITEM_POSITION"

    invoke-virtual {v0, v8, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    .line 1423
    :cond_5
    const-string v8, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1425
    const-string v8, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v0, v8, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1426
    const-string v8, "KEY_MEDIA_SET_POSITION"

    invoke-virtual {v0, v8, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1427
    const-string v8, "KEY_ITEM_POSITION"

    invoke-virtual {v0, v8, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1
.end method

.method private startDetailViewState(I)V
    .locals 12
    .param p1, "position"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1436
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    if-nez v7, :cond_1

    .line 1437
    sget-object v7, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "startDetailViewState() : mMediaItemAdapter is null"

    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/data/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1494
    :cond_0
    :goto_0
    return-void

    .line 1440
    :cond_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v7, v9, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .line 1441
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v2, :cond_0

    .line 1443
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v7, v9}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showNewAlbumDialog(Z)V

    .line 1444
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    iput-boolean v8, v7, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mDetailViewStarted:Z

    .line 1446
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->isUseFastLargeThumbnail()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1450
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1451
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getScreenNailImage()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1452
    .local v3, "itemBitmap":Landroid/graphics/Bitmap;
    const/4 v4, 0x0

    .line 1454
    .local v4, "newBitmap":Landroid/graphics/Bitmap;
    if-nez v3, :cond_5

    const/4 v4, 0x0

    .line 1459
    :goto_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v10

    invoke-virtual {v7, v4, v10}, Lcom/sec/android/gallery3d/app/StateManager;->setPreviousBitmap(Landroid/graphics/Bitmap;I)V

    .line 1460
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    invoke-virtual {v7, v10}, Lcom/sec/android/gallery3d/app/StateManager;->setPreviousActivityState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 1461
    instance-of v7, v2, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v7, :cond_6

    move-object v7, v2

    .line 1462
    check-cast v7, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/LocalImage;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1466
    .local v6, "path":Ljava/lang/String;
    :goto_2
    const-string v7, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v0, v7, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1467
    const-string v7, "KEY_MEDIA_SET_PATH"

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetPath:Ljava/lang/String;

    invoke-virtual {v0, v7, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1468
    const-string v7, "KEY_MEDIA_SET_POSITION"

    iget v10, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-virtual {v0, v7, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1469
    const-string v7, "KEY_ITEM_POSITION"

    invoke-virtual {v0, v7, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1470
    const-string v7, "KEY_GROUP_INDEX"

    const-wide/16 v10, 0x0

    invoke-virtual {v0, v7, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1471
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v7, :cond_7

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaSet;->isCameraRoll()Z

    move-result v7

    if-eqz v7, :cond_7

    move v1, v8

    .line 1472
    .local v1, "isCameraRoll":Z
    :goto_3
    const-string v7, "in_camera_roll"

    invoke-virtual {v0, v7, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1473
    const-string v7, "is_from_camera"

    iget-boolean v10, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsFromCamera:Z

    invoke-virtual {v0, v7, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1475
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inExpansionMode()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1476
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v7, v7, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v7, :cond_3

    if-eqz v1, :cond_3

    .line 1477
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v7, v8}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->setShowAllGroup(Z)V

    .line 1478
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v7, v8}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setShowAllGroup(Z)V

    .line 1480
    :cond_3
    const-string v7, "expansion_detail_view"

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1481
    const-string v7, "in_camera_roll"

    invoke-virtual {v0, v7, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1482
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addExpMediaItem(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1484
    :cond_4
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v7, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setClickEnabled(Z)V

    .line 1485
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v8, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$9;

    invoke-direct {v8, p0, v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$9;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Landroid/os/Bundle;)V

    invoke-virtual {v7, v8}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 1454
    .end local v1    # "isCameraRoll":Z
    .end local v6    # "path":Ljava/lang/String;
    :cond_5
    :try_start_0
    invoke-static {v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto/16 :goto_1

    .line 1455
    :catch_0
    move-exception v5

    .line 1456
    .local v5, "ome":Ljava/lang/OutOfMemoryError;
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 1457
    invoke-virtual {v5}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto/16 :goto_1

    .line 1464
    .end local v5    # "ome":Ljava/lang/OutOfMemoryError;
    :cond_6
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v6

    .restart local v6    # "path":Ljava/lang/String;
    goto/16 :goto_2

    :cond_7
    move v1, v9

    .line 1471
    goto :goto_3
.end method

.method private startHiddenAlbumsViewState()V
    .locals 4

    .prologue
    .line 1506
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const-class v1, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/app/StateManager;->startStateForResult(Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 1507
    return-void
.end method

.method private startNoItemViewState(Ljava/lang/String;)V
    .locals 3
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 2440
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const-class v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/StateManager;->setPreviousViewState(Ljava/lang/Class;)V

    .line 2442
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2443
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "KEY_MEDIA_SET_PATH"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentTopMediaSetPath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2444
    const-string v1, "KEY_NOITEMSVIEW_MIME_TYPE"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2445
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$13;

    invoke-direct {v2, p0, v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$13;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2450
    return-void
.end method

.method private startSlideShow(Z)V
    .locals 4
    .param p1, "burstPlay"    # Z

    .prologue
    const/4 v3, 0x0

    .line 1511
    const/4 v1, 0x6

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object v3, v0, v1

    const/4 v1, 0x3

    aput-object v3, v0, v1

    const/4 v1, 0x4

    aput-object v3, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isfromEditmode:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1512
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "START_SLIDESHOW"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1514
    return-void
.end method

.method private switchTopMediaSet(Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 1
    .param p1, "mediaFilterType"    # Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 1562
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentMediaFilterType(Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)V

    .line 1563
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1564
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentTopMediaSetPath:Ljava/lang/String;

    .line 1572
    :cond_0
    return-void
.end method

.method private updateActionBarSelectionMode(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 11
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1918
    const/4 v5, 0x0

    .line 1919
    .local v5, "totalCheckedCount":I
    iget-boolean v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumMode:Z

    if-eqz v8, :cond_2

    .line 1920
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsNewAlbumHeaderSelected:Z

    if-eqz v6, :cond_1

    .line 1921
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v8, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    const/16 v9, 0x8

    invoke-virtual {v6, v8, v9, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 1922
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 1958
    :cond_0
    :goto_0
    return-void

    .line 1924
    :cond_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalSelectedItems()I

    move-result v5

    .line 1925
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6, v5}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 1926
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateNewAblumActionBarSelection()V

    goto :goto_0

    .line 1928
    :cond_2
    iget-boolean v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    if-eqz v8, :cond_6

    .line 1929
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v2

    .line 1930
    .local v2, "numberOfItemsInAlbum":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v0

    .line 1931
    .local v0, "checkedCount":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalSelectedItems()I

    move-result v5

    .line 1932
    if-ne v2, v0, :cond_3

    move v3, v6

    .line 1933
    .local v3, "selectAll":Z
    :goto_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v8, v5}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 1935
    iget-boolean v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsGifMode:Z

    if-eqz v8, :cond_4

    .line 1936
    const/16 v1, 0x14

    .line 1940
    .local v1, "maxPickCount":I
    :goto_2
    if-lez v1, :cond_5

    if-ne v5, v1, :cond_5

    .line 1941
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v9, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v8, v9, v7, v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    goto :goto_0

    .end local v1    # "maxPickCount":I
    .end local v3    # "selectAll":Z
    :cond_3
    move v3, v7

    .line 1932
    goto :goto_1

    .line 1938
    .restart local v3    # "selectAll":Z
    :cond_4
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "pick-max-item"

    const/4 v10, -0x1

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .restart local v1    # "maxPickCount":I
    goto :goto_2

    .line 1943
    :cond_5
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v8, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v6, v8, v7, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    goto :goto_0

    .line 1946
    .end local v0    # "checkedCount":I
    .end local v1    # "maxPickCount":I
    .end local v2    # "numberOfItemsInAlbum":I
    .end local v3    # "selectAll":Z
    :cond_6
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v8, :cond_0

    .line 1948
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v2

    .line 1949
    .restart local v2    # "numberOfItemsInAlbum":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v0

    .line 1950
    .restart local v0    # "checkedCount":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v4

    .line 1951
    .local v4, "selectedItems":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalSelectedItems()I

    move-result v5

    .line 1952
    if-ne v2, v0, :cond_7

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v8

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v9

    if-ne v8, v9, :cond_7

    move v3, v6

    .line 1954
    .restart local v3    # "selectAll":Z
    :goto_3
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    .line 1955
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6, v5}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 1956
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v8, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v6, v8, v7, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    goto/16 :goto_0

    .end local v3    # "selectAll":Z
    :cond_7
    move v3, v7

    .line 1952
    goto :goto_3
.end method

.method private updateCreateVideoAlbumOptionMenu()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const v2, 0x7f0f02cd

    .line 1269
    const-string v0, "com.sec.android.app.ve.export.ExportService"

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isVideoClipServiceRunning(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1270
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;

    invoke-static {v0, v2, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemEnabled(Landroid/view/Menu;IZ)V

    .line 1277
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->checkUseCreateVideoAlbum(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCreateVideoAlbum:Z

    if-nez v0, :cond_1

    .line 1279
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;

    invoke-static {v0, v2, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 1282
    :cond_1
    return-void

    .line 1273
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;

    const/4 v1, 0x1

    invoke-static {v0, v2, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemEnabled(Landroid/view/Menu;IZ)V

    goto :goto_0
.end method

.method private updateHideItemsOptionMenu()V
    .locals 14

    .prologue
    .line 1192
    const-wide/16 v8, 0x0

    .line 1193
    .local v8, "supportOperation":J
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v7

    if-nez v7, :cond_2

    .line 1194
    :cond_0
    sget-object v7, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    const-string/jumbo v10, "updating hide items is ignored due to null"

    invoke-static {v7, v10}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1266
    :cond_1
    :goto_0
    return-void

    .line 1198
    :cond_2
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-eqz v7, :cond_8

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v7

    if-eqz v7, :cond_8

    const/4 v2, 0x1

    .line 1201
    .local v2, "isSelectionMode":Z
    :goto_1
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableMenuHide:Z

    if-nez v7, :cond_4

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_4

    if-eqz v2, :cond_3

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v7

    if-eqz v7, :cond_4

    :cond_3
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v7

    sget-object v10, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eq v7, v10, :cond_4

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEventViewMode(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_4

    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsCategoryTagType:Z

    if-eqz v7, :cond_9

    .line 1208
    :cond_4
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;

    const v10, 0x7f0f0271

    const/4 v11, 0x0

    invoke-static {v7, v10, v11}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 1225
    :cond_5
    :goto_2
    const/4 v4, 0x0

    .line 1226
    .local v4, "mediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-eqz v7, :cond_6

    .line 1227
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCloneMediaList()Ljava/util/LinkedList;

    move-result-object v4

    .line 1229
    :cond_6
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_1

    if-eqz v4, :cond_1

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v7

    sget-object v10, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_EVENT:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eq v7, v10, :cond_1

    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsCategoryTagType:Z

    if-nez v7, :cond_1

    if-eqz v2, :cond_1

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isHideItemInSelectionMode()Z

    move-result v7

    if-nez v7, :cond_1

    .line 1233
    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_7
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 1234
    .local v6, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v7, v6, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-nez v7, :cond_c

    instance-of v7, v6, Lcom/sec/android/gallery3d/remote/cloud/CloudMediaItem;

    if-eqz v7, :cond_c

    .line 1235
    sget-object v7, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    const-string v10, "PhotoViewComponent == updateHideItemsOptionMenu-->CloudMediaItem"

    invoke-static {v7, v10}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1236
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;

    const v10, 0x7f0f0271

    const/4 v11, 0x0

    invoke-static {v7, v10, v11}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_0

    .line 1198
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "isSelectionMode":Z
    .end local v4    # "mediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    .end local v6    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 1209
    .restart local v2    # "isSelectionMode":Z
    :cond_9
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v7, :cond_5

    .line 1210
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getSupportedOperations()J

    move-result-wide v8

    .line 1211
    const-wide/high16 v10, 0x4000000000000L

    and-long/2addr v10, v8

    const-wide/16 v12, 0x0

    cmp-long v7, v10, v12

    if-nez v7, :cond_b

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isHideBlockedItemAtPhotoViewState(Lcom/sec/android/gallery3d/data/MediaSet;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 1214
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;

    const v10, 0x7f0f0271

    const/4 v11, 0x0

    invoke-static {v7, v10, v11}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 1216
    if-eqz v2, :cond_a

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isHideItemInSelectionMode()Z

    move-result v7

    if-eqz v7, :cond_a

    .line 1218
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->disablePopupMenu()V

    goto/16 :goto_2

    .line 1220
    :cond_a
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->enablePopupMenu()V

    goto/16 :goto_2

    .line 1222
    :cond_b
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->enablePopupMenu()V

    goto/16 :goto_2

    .line 1239
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v4    # "mediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    .restart local v6    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_c
    instance-of v7, v6, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v7, :cond_7

    instance-of v7, v6, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-nez v7, :cond_7

    move-object v3, v6

    .line 1240
    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1241
    .local v3, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    if-eqz v7, :cond_7

    .line 1243
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v10

    invoke-virtual {v7, v10}, Lcom/sec/android/gallery3d/data/DataManager;->getDefaultSetOf(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    .line 1244
    .local v0, "albumPath":Lcom/sec/android/gallery3d/data/Path;
    if-eqz v0, :cond_7

    .line 1246
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v7, v0}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 1248
    .local v5, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v5, :cond_7

    .line 1250
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getSupportedOperations()J

    move-result-wide v8

    .line 1251
    const-wide/high16 v10, 0x4000000000000L

    and-long/2addr v10, v8

    const-wide/16 v12, 0x0

    cmp-long v7, v10, v12

    if-nez v7, :cond_d

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isHideBlockedItemAtPhotoViewState(Lcom/sec/android/gallery3d/data/MediaSet;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 1254
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;

    const v10, 0x7f0f0271

    const/4 v11, 0x0

    invoke-static {v7, v10, v11}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_0

    .line 1258
    :cond_d
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableMenuHide:Z

    if-nez v7, :cond_7

    .line 1259
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;

    const v10, 0x7f0f0271

    const/4 v11, 0x1

    invoke-static {v7, v10, v11}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_3
.end method

.method private updateLastSharedIconAndTitle()V
    .locals 1

    .prologue
    .line 1362
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->isLastShareAppExisted(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1363
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    .line 1365
    :cond_0
    return-void
.end method

.method private updateMaximumSelectionNumberExceeded(I)V
    .locals 6
    .param p1, "maxCount"    # I

    .prologue
    .line 1961
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0117

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1964
    .local v0, "text":Ljava/lang/CharSequence;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 1965
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateActionBarSelectionMode(Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 1966
    return-void
.end method

.method private updateNearByAlbumOptionMenu()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1326
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-nez v0, :cond_1

    .line 1339
    :cond_0
    :goto_0
    return-void

    .line 1328
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    if-eqz v0, :cond_0

    .line 1329
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;

    const v1, 0x7f0f027d

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 1330
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;

    const v1, 0x7f0f027e

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 1331
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;

    const v1, 0x7f0f0271

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 1332
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;

    const v1, 0x7f0f0275

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 1334
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;

    const v1, 0x7f0f02cd

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 1336
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;

    const v1, 0x7f0f026f

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_0
.end method

.method private updateNewAblumActionBarSelection()V
    .locals 11

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1096
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    .line 1097
    .local v0, "albumTotalCount":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v4

    .line 1098
    .local v4, "newAlbumSelectedCount":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v1

    .line 1099
    .local v1, "currentSelectedCount":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalSelectedItems()I

    move-result v5

    .line 1101
    .local v5, "totalCheckedCount":I
    add-int v8, v4, v1

    if-gt v0, v8, :cond_0

    move v3, v6

    .line 1102
    .local v3, "isSelctAll":Z
    :goto_0
    if-gtz v1, :cond_1

    move v2, v6

    .line 1104
    .local v2, "isDeselectAll":Z
    :goto_1
    sget-object v8, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "albumTotalCount:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " newAlbumSelectedCount:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " currentSelectedCount:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1105
    sget-object v8, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "isSelctAll:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " isDeselectAll:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1107
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v8, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    .line 1108
    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    .line 1109
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v8, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    const/16 v9, 0x8

    invoke-virtual {v7, v8, v9, v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 1113
    :goto_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6, v5}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 1114
    return-void

    .end local v2    # "isDeselectAll":Z
    .end local v3    # "isSelctAll":Z
    :cond_0
    move v3, v7

    .line 1101
    goto :goto_0

    .restart local v3    # "isSelctAll":Z
    :cond_1
    move v2, v7

    .line 1102
    goto :goto_1

    .line 1111
    .restart local v2    # "isDeselectAll":Z
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v8, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v6, v8, v7, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    goto :goto_2
.end method

.method private updateNewAlbumActionBar()V
    .locals 2

    .prologue
    .line 1117
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 1118
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$7;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$7;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1124
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateNewAblumActionBarSelection()V

    .line 1125
    return-void
.end method

.method private updateNewAlbumOptionMenu()V
    .locals 3

    .prologue
    .line 1181
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;

    const v2, 0x7f0f02d2

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1182
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1189
    :cond_0
    :goto_0
    return-void

    .line 1186
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->hasLocalMediaSet()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1187
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private updateNewAlbumSaveButton()V
    .locals 2

    .prologue
    .line 1128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$8;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$8;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1134
    return-void
.end method

.method private updateNewMarkIconInfo(Lcom/sec/android/gallery3d/data/MediaSet;I)V
    .locals 0
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "albumIndex"    # I

    .prologue
    .line 2599
    if-eqz p1, :cond_0

    .line 2600
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->setLatestSelectedAlbuminfo(Lcom/sec/android/gallery3d/data/MediaSet;I)Z

    .line 2602
    :cond_0
    return-void
.end method

.method private updateSearchTextMenu()V
    .locals 4

    .prologue
    .line 1307
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;

    const v3, 0x7f0f02d3

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1308
    .local v1, "menuItemSearchText":Landroid/view/MenuItem;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    if-nez v2, :cond_1

    .line 1323
    :cond_0
    :goto_0
    return-void

    .line 1312
    :cond_1
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOCR:Z

    if-eqz v2, :cond_3

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoViewSearchText:Z

    if-eqz v2, :cond_3

    .line 1313
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    iget v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 1315
    .local v0, "curMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v0, :cond_2

    .line 1316
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 1318
    :cond_2
    invoke-static {v0}, Lcom/sec/android/gallery3d/data/OcrClustering;->isOCRMedia(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 1321
    .end local v0    # "curMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_3
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private updateWebAlbumMenu()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1342
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-nez v0, :cond_1

    .line 1349
    :cond_0
    :goto_0
    return-void

    .line 1344
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaMergeAlbum;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbumSet;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/sns/SNSAlbumSet;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;

    if-eqz v0, :cond_0

    .line 1346
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;

    const v1, 0x7f0f0283

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 1347
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;

    const v1, 0x7f0f026e

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_0
.end method


# virtual methods
.method protected addToNewAlbumList(I)V
    .locals 14
    .param p1, "headerIndex"    # I

    .prologue
    .line 2128
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v8

    .line 2129
    .local v8, "selectedList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-virtual {v8}, Ljava/util/LinkedList;->size()I

    move-result v10

    .line 2130
    .local v10, "size":I
    const/4 v3, 0x0

    .line 2131
    .local v3, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    const v11, 0xffff

    and-int v6, p1, v11

    .line 2132
    .local v6, "photoIndex":I
    const/4 v0, 0x0

    .line 2133
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v5, 0x0

    .line 2135
    .local v5, "newBitmap":Landroid/graphics/Bitmap;
    if-nez v10, :cond_0

    .line 2136
    sget-object v11, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    const-string v12, "addToNewAlbumList. No item is selected !!"

    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/data/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2174
    :goto_0
    return-void

    .line 2139
    :cond_0
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    const/4 v12, 0x0

    invoke-virtual {v11, v12, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    .line 2140
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    const/4 v12, 0x0

    invoke-virtual {v11, v12, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getItemImage(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2141
    if-eqz v3, :cond_1

    if-nez v0, :cond_2

    .line 2142
    :cond_1
    sget-object v11, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "addToNewAlbumList. index = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", bitmap = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", item = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/data/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2145
    :cond_2
    invoke-static {v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 2147
    invoke-virtual {v8, v3}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 2148
    invoke-virtual {v8, v3}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 2150
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v11, v5, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->updateNewAlbumImage(Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 2153
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11, v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Ljava/util/List;)V

    .line 2154
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedMediaSetMap()Ljava/util/HashMap;

    move-result-object v9

    .line 2155
    .local v9, "selectedSetMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/Integer;>;"
    invoke-virtual {v9}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 2156
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/Integer;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 2157
    .local v4, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v7

    .line 2158
    .local v7, "selectedCount":I
    sget-object v11, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Selected Album Count : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2159
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11, v4, v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;I)V

    .line 2161
    sget-object v11, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "New Album Count : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v13, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSetSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2164
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/Integer;>;"
    .end local v4    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v7    # "selectedCount":I
    :cond_3
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 2165
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->clearSelectedCount()V

    .line 2166
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v11}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->refreshCheckState()V

    .line 2167
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v11}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->refreshCheckState()V

    .line 2169
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 2170
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    .line 2172
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateNewAblumActionBarSelection()V

    .line 2173
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateNewAlbumSaveButton()V

    goto/16 :goto_0
.end method

.method public checkEnable()Z
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 530
    const/4 v0, 0x1

    .line 532
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected dismissNewAlbumDialog()V
    .locals 2

    .prologue
    .line 1352
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->hasLocalMediaSet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1353
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showNewAlbumDialog(Z)V

    .line 1354
    :cond_0
    return-void
.end method

.method protected enterSelectionMode(ZZ)V
    .locals 7
    .param p1, "newAlbumMode"    # Z
    .param p2, "useVibrator"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 2177
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumMode:Z

    .line 2178
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-eqz v3, :cond_0

    .line 2179
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3, p2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->enterSelectionMode(Z)V

    .line 2180
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsDeleteMode:Z

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setDeleteSelectMode(Z)V

    .line 2182
    :cond_0
    if-eqz p1, :cond_1

    .line 2183
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v3, v6, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->setHeaderItem(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;)V

    .line 2184
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3, p2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->enterSelectionMode(Z)V

    .line 2185
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    const/4 v4, 0x2

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setMode(IILjava/lang/Object;)V

    .line 2186
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 2187
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v4, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v4, v5}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 2188
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v4, 0x7

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v1

    .line 2189
    .local v1, "localPath":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 2190
    .local v2, "localSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->getCurrentCenterIndex()I

    move-result v0

    .line 2191
    .local v0, "centerIndex":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v3, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->setFirstIndex(I)V

    .line 2192
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v3, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->setSource(Lcom/sec/android/gallery3d/data/MediaSet;)Z

    .line 2201
    .end local v0    # "centerIndex":I
    .end local v1    # "localPath":Ljava/lang/String;
    .end local v2    # "localSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->isSelectionMode(Ljava/lang/String;)V

    .line 2202
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v3, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->refreshSelectionBarState(Z)V

    .line 2203
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v3, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->refreshSelectionBarState(Z)V

    .line 2204
    return-void

    .line 2194
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setMode(IILjava/lang/Object;)V

    .line 2195
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 2196
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsGifMode:Z

    if-nez v3, :cond_2

    .line 2197
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v4, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v4, v5}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    goto :goto_0

    .line 2199
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v4, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v4, v5}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    goto :goto_0
.end method

.method exitSearchMode()V
    .locals 5

    .prologue
    .line 1540
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOCR:Z

    if-eqz v2, :cond_0

    .line 1541
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->onPause()V

    .line 1542
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/StateManager;->setSearchMode(Z)V

    .line 1543
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    iget v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 1545
    .local v0, "curMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v0, :cond_1

    .line 1546
    sget-object v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    const-string v3, "curMediaSet is null! exitSearchMode"

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1558
    .end local v0    # "curMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    :goto_0
    return-void

    .line 1549
    .restart local v0    # "curMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1550
    .local v1, "curOrigPath":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1551
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetPath:Ljava/lang/String;

    .line 1554
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1555
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v3, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v3, v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1556
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateAlbumNameOfActionBar()V

    goto :goto_0
.end method

.method protected exitSelectionMode()V
    .locals 1

    .prologue
    .line 2250
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->exitSelectionMode(Z)V

    .line 2251
    return-void
.end method

.method protected exitSelectionMode(Z)V
    .locals 5
    .param p1, "updateView"    # Z

    .prologue
    const/4 v4, 0x0

    .line 2207
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumMode:Z

    if-eqz v1, :cond_0

    .line 2208
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->leaveSelectionMode()V

    .line 2209
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumMode:Z

    .line 2210
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsNewAlbumHeaderSelected:Z

    .line 2211
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iput-boolean v4, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mNotNeedToResetLayout:Z

    .line 2212
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->getCurrentCenterIndex()I

    move-result v0

    .line 2213
    .local v0, "centerIndex":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->setFirstIndex(I)V

    .line 2214
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mTopMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->setSource(Lcom/sec/android/gallery3d/data/MediaSet;)Z

    .line 2216
    .end local v0    # "centerIndex":I
    :cond_0
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsCategoryTagType:Z

    if-eqz v1, :cond_1

    .line 2217
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->exitSelectionMode()V

    .line 2218
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->resume()V

    .line 2221
    :cond_1
    if-eqz p1, :cond_2

    .line 2222
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    sget v2, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_NORMAL:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v4, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setMode(IILjava/lang/Object;)V

    .line 2223
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->refreshSelectionBarState(Z)V

    .line 2224
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->refreshSelectionBarState(Z)V

    .line 2227
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->leaveSelectionMode()V

    .line 2228
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 2229
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v2, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 2230
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateAlbumNameOfActionBar()V

    .line 2231
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->clearSelectedCount()V

    .line 2232
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mFilteredItems:Z

    .line 2233
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->misFilteredForVideoAlbum:Z

    .line 2234
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->misFilteredForStoryAlbum:Z

    .line 2235
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->clearShowGroupId()V

    .line 2236
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->setShowAllGroup(Z)V

    .line 2237
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->clearShowGroupId()V

    .line 2238
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setShowAllGroup(Z)V

    .line 2239
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsDeleteMode:Z

    .line 2240
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsGifMode:Z

    if-eqz v1, :cond_3

    .line 2241
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    .line 2243
    :cond_3
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsGifMode:Z

    .line 2244
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPenSelect:Z

    .line 2245
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGroupIDList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 2247
    return-void
.end method

.method public getCurrentMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1

    .prologue
    .line 2436
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method protected getFilteredPickItem(Ljava/util/ArrayList;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 957
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-nez p1, :cond_0

    .line 958
    const/4 v4, 0x0

    .line 969
    :goto_0
    return v4

    .line 960
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 961
    .local v2, "itemsClone":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 963
    .local v3, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v3, :cond_2

    .line 964
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 965
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4, v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isAvailableDrm(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 966
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 963
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 969
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    goto :goto_0
.end method

.method protected getMediaItemAdapterSize()I
    .locals 3

    .prologue
    .line 2420
    const/4 v0, 0x0

    .line 2421
    .local v0, "realCount":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    if-eqz v1, :cond_0

    .line 2425
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getCount(I)I

    move-result v0

    .line 2428
    :cond_0
    return v0
.end method

.method public getTopMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1

    .prologue
    .line 2432
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mTopMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method protected isAvailableCount(I)Z
    .locals 2
    .param p1, "selected"    # I

    .prologue
    .line 2299
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isAvailableCount(IZZ)Z

    move-result v0

    return v0
.end method

.method protected isAvailableCount(IZZ)Z
    .locals 10
    .param p1, "selected"    # I
    .param p2, "checkMin"    # Z
    .param p3, "showToast"    # Z

    .prologue
    const v9, 0x7f0e0117

    const/4 v6, -0x1

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2303
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2304
    .local v0, "intent":Landroid/content/Intent;
    const/4 v3, 0x0

    .line 2306
    .local v3, "text":Ljava/lang/CharSequence;
    const/4 v1, 0x1

    .line 2308
    .local v1, "isAvailable":Z
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mFromInsideGallery:Z

    if-eqz v4, :cond_2

    .line 2309
    const/4 v1, 0x1

    .line 2338
    :cond_0
    :goto_0
    if-nez v1, :cond_1

    if-eqz p3, :cond_1

    .line 2339
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 2341
    :cond_1
    return v1

    .line 2311
    :cond_2
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mFromGalleryWidget:Z

    if-nez v4, :cond_6

    .line 2312
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mFromGifMaker:Z

    if-eqz v4, :cond_4

    .line 2313
    const-string v4, "select_num_key"

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 2319
    .local v2, "pickCount":I
    :goto_1
    if-eq v2, v6, :cond_3

    if-le p1, v2, :cond_3

    .line 2320
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v4, v9, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2321
    const/4 v1, 0x0

    .line 2323
    :cond_3
    if-eqz p2, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isPenSelectionMode()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2324
    const-string v4, "pick-min-item"

    invoke-virtual {v0, v4, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 2325
    if-ge p1, v2, :cond_0

    if-le v2, v7, :cond_0

    .line 2326
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0116

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2327
    const/4 v1, 0x0

    goto :goto_0

    .line 2314
    .end local v2    # "pickCount":I
    :cond_4
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsGifMode:Z

    if-eqz v4, :cond_5

    .line 2315
    const/16 v2, 0x14

    .restart local v2    # "pickCount":I
    goto :goto_1

    .line 2317
    .end local v2    # "pickCount":I
    :cond_5
    const-string v4, "pick-max-item"

    sget v5, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .restart local v2    # "pickCount":I
    goto :goto_1

    .line 2331
    .end local v2    # "pickCount":I
    :cond_6
    const/16 v4, 0x3e8

    if-le p1, v4, :cond_0

    .line 2332
    const/16 v2, 0x3e8

    .line 2333
    .restart local v2    # "pickCount":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v4, v9, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2334
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method protected isAvailableSelect(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 4
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2346
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    if-nez v3, :cond_1

    .line 2359
    :cond_0
    :goto_0
    return v1

    .line 2350
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3, p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isAvailableDrm(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2351
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0e0107

    invoke-static {v1, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    move v1, v2

    .line 2352
    goto :goto_0

    .line 2355
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v3

    add-int/lit8 v0, v3, 0x1

    .line 2356
    .local v0, "count":I
    invoke-virtual {p0, v0, v2, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isAvailableCount(IZZ)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 2357
    goto :goto_0
.end method

.method protected isFromGalleryWidget()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2375
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "photo-pick"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2378
    :goto_0
    return v1

    .line 2377
    :catch_0
    move-exception v0

    .line 2378
    .local v0, "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method protected isFromGifMaker()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2384
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "photo-pick-gifmaker"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2388
    :goto_0
    return v1

    .line 2387
    :catch_0
    move-exception v0

    .line 2388
    .local v0, "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method protected isFromInsideGallery()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2394
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "pick-from-gallery"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2397
    :goto_0
    return v1

    .line 2396
    :catch_0
    move-exception v0

    .line 2397
    .local v0, "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public isPickMode()Z
    .locals 2

    .prologue
    .line 2363
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v1, :cond_1

    .line 2364
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    .line 2365
    .local v0, "currentMode":Lcom/sec/samsung/gallery/core/LaunchModeType;
    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v1, :cond_1

    .line 2367
    :cond_0
    const/4 v1, 0x1

    .line 2370
    .end local v0    # "currentMode":Lcom/sec/samsung/gallery/core/LaunchModeType;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected isSelected(I)Z
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 2410
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 2411
    .local v0, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v1

    return v1
.end method

.method protected isSelectedForNewAlbum(I)Z
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 2415
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 2416
    .local v0, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v1

    return v1
.end method

.method protected isSingleAlbumMode()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2403
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "single-album"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 2405
    :cond_0
    :goto_0
    return v1

    .line 2404
    :catch_0
    move-exception v0

    .line 2405
    .local v0, "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public isVideoClipServiceRunning(Ljava/lang/String;)Z
    .locals 6
    .param p1, "servicePackage"    # Ljava/lang/String;

    .prologue
    const v5, 0x7fffffff

    .line 1030
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v4, "activity"

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 1032
    .local v1, "manager":Landroid/app/ActivityManager;
    if-eqz v1, :cond_1

    invoke-virtual {v1, v5}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1033
    invoke-virtual {v1, v5}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 1035
    .local v2, "service":Landroid/app/ActivityManager$RunningServiceInfo;
    iget-object v3, v2, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1036
    const/4 v3, 0x1

    .line 1040
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "service":Landroid/app/ActivityManager$RunningServiceInfo;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected onAlbumClick(I)V
    .locals 5
    .param p1, "position"    # I

    .prologue
    const/4 v3, 0x0

    .line 2476
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->isDragAnimRunning()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2505
    :cond_0
    :goto_0
    return-void

    .line 2478
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    if-eqz v2, :cond_0

    .line 2480
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 2482
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v0, :cond_2

    .line 2483
    sget-object v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    const-string v3, "onAlbumClick: mediaSet is null"

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2486
    :cond_2
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsNewAlbumHeaderSelected:Z

    .line 2487
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v2, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setSource(Lcom/sec/android/gallery3d/data/MediaSet;)Z

    move-result v1

    .line 2488
    .local v1, "result":Z
    if-eqz v1, :cond_3

    .line 2489
    invoke-direct {p0, v0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->setLatestSelectedAlbuminfo(Lcom/sec/android/gallery3d/data/MediaSet;I)Z

    move-result v1

    .line 2490
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 2491
    iput p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    .line 2492
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetPath:Ljava/lang/String;

    .line 2493
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setFirstIndex(I)V

    .line 2494
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->getFirstLoadingCount()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setFirstLoadingCount(I)V

    .line 2495
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    const/4 v3, 0x1

    invoke-virtual {v2, p1, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->selectAlbum(IZ)V

    .line 2496
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->postUpdateAlbumNameOfActionBar()V

    .line 2497
    if-eqz v1, :cond_3

    .line 2498
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->refreshItem(I)V

    .line 2501
    :cond_3
    sget-object v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onAlbumClick: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-th album selected"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2503
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumMode:Z

    if-eqz v2, :cond_0

    .line 2504
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateNewAblumActionBarSelection()V

    goto :goto_0
.end method

.method protected onAlbumClick(IZ)V
    .locals 5
    .param p1, "position"    # I
    .param p2, "flagAnim"    # Z

    .prologue
    const/4 v3, 0x0

    .line 2508
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->isDragAnimRunning()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2537
    :cond_0
    :goto_0
    return-void

    .line 2510
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    if-eqz v2, :cond_0

    .line 2512
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 2514
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v0, :cond_2

    .line 2515
    sget-object v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    const-string v3, "onAlbumClick: mediaSet is null"

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2518
    :cond_2
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsNewAlbumHeaderSelected:Z

    .line 2519
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v2, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setSource(Lcom/sec/android/gallery3d/data/MediaSet;)Z

    move-result v1

    .line 2520
    .local v1, "result":Z
    if-eqz v1, :cond_3

    .line 2521
    invoke-direct {p0, v0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->setLatestSelectedAlbuminfo(Lcom/sec/android/gallery3d/data/MediaSet;I)Z

    move-result v1

    .line 2522
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 2523
    iput p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    .line 2524
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetPath:Ljava/lang/String;

    .line 2525
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setFirstIndex(I)V

    .line 2526
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->getFirstLoadingCount()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setFirstLoadingCount(I)V

    .line 2527
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v2, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->selectAlbum(IZ)V

    .line 2528
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->postUpdateAlbumNameOfActionBar()V

    .line 2529
    if-eqz v1, :cond_3

    .line 2530
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->refreshItem(I)V

    .line 2533
    :cond_3
    sget-object v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onAlbumClick: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-th album selected"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2535
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumMode:Z

    if-eqz v2, :cond_0

    .line 2536
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateNewAblumActionBarSelection()V

    goto :goto_0
.end method

.method protected onNewAlbumClick()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2539
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumMode:Z

    if-nez v0, :cond_1

    .line 2554
    :cond_0
    :goto_0
    return-void

    .line 2542
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2545
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsNewAlbumHeaderSelected:Z

    .line 2546
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setSelectionSource(Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    .line 2547
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setFirstIndex(I)V

    .line 2548
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->getFirstLoadingCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setFirstLoadingCount(I)V

    .line 2549
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->selectAlbum(IZ)V

    .line 2550
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iput-boolean v2, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mNewAlbumHeaderFocused:Z

    .line 2551
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->redrawNewAlbumImage()V

    .line 2552
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->postUpdateAlbumNameOfActionBar()V

    .line 2553
    sget-object v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    const-string v1, "onAlbumClick: new album header selected."

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onPhotoDropAlbumItemClick(ILandroid/net/Uri;)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2561
    iput p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectedAlbumItemIndex:I

    .line 2562
    iget v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectedAlbumItemIndex:I

    invoke-direct {p0, v0, p2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->startDetailView(ILandroid/net/Uri;)V

    .line 2563
    return-void
.end method

.method protected onPhotoDropItemClick(ILandroid/net/Uri;)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2557
    iget v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-direct {p0, v0, p2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->startDetailView(ILandroid/net/Uri;)V

    .line 2558
    return-void
.end method

.method protected onPhotoItemClick(I)V
    .locals 7
    .param p1, "position"    # I

    .prologue
    .line 2566
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsNewAlbumHeaderSelected:Z

    if-eqz v5, :cond_5

    .line 2567
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    if-lt p1, v5, :cond_1

    .line 2595
    :cond_0
    :goto_0
    return-void

    .line 2569
    :cond_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    monitor-enter v6

    .line 2570
    :try_start_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 2571
    .local v2, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v5, v2, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v5, :cond_4

    .line 2572
    move-object v0, v2

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    move-object v5, v0

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getParentSetPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    .line 2573
    .local v4, "parentPath":Lcom/sec/android/gallery3d/data/Path;
    if-eqz v4, :cond_3

    .line 2574
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v5, v4}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 2575
    .local v3, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v5, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getAlbumIndexFromMediaSet(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v1

    .line 2576
    .local v1, "albumIndex":I
    if-ltz v1, :cond_2

    .line 2577
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v5, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    .line 2579
    :cond_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5, v3, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 2586
    .end local v1    # "albumIndex":I
    .end local v3    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v4    # "parentPath":Lcom/sec/android/gallery3d/data/Path;
    :goto_1
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2587
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->forceReload()V

    .line 2588
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->redrawNewAlbumImage()V

    .line 2589
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPenSelect:Z

    if-eqz v5, :cond_0

    .line 2590
    iget v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mRemovedCntInNewAlbumHeader:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mRemovedCntInNewAlbumHeader:I

    goto :goto_0

    .line 2581
    .restart local v4    # "parentPath":Lcom/sec/android/gallery3d/data/Path;
    :cond_3
    :try_start_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_1

    .line 2586
    .end local v2    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v4    # "parentPath":Lcom/sec/android/gallery3d/data/Path;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 2584
    .restart local v2    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_4
    :try_start_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaObject;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 2593
    .end local v2    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_5
    iput p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaItemIndex:I

    .line 2594
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoEventHandle:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;

    invoke-virtual {v5, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->onItemClick(I)V

    goto :goto_0
.end method

.method public onPrepareOptionMenu(Landroid/view/Menu;)V
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1137
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;

    .line 1139
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateSearchTextMenu()V

    .line 1140
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateNearByAlbumOptionMenu()V

    .line 1141
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateCreateVideoAlbumOptionMenu()V

    .line 1142
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateHideItemsOptionMenu()V

    .line 1143
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateNewAlbumOptionMenu()V

    .line 1144
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateWebAlbumMenu()V

    .line 1145
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsDeleteMode:Z

    if-eqz v4, :cond_2

    .line 1146
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    .line 1147
    .local v1, "isSelected":Z
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v3

    .local v3, "n":I
    :goto_0
    if-ge v0, v3, :cond_5

    .line 1148
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 1149
    .local v2, "item":Landroid/view/MenuItem;
    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0f027b

    if-ne v4, v5, :cond_1

    .line 1150
    invoke-interface {v2, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1151
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1152
    if-eqz v1, :cond_0

    .line 1153
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1147
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1155
    :cond_0
    invoke-interface {v2, v7}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 1157
    :cond_1
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 1160
    .end local v0    # "i":I
    .end local v1    # "isSelected":Z
    .end local v2    # "item":Landroid/view/MenuItem;
    .end local v3    # "n":I
    :cond_2
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsGifMode:Z

    if-eqz v4, :cond_5

    .line 1161
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    .line 1162
    .restart local v1    # "isSelected":Z
    const/4 v0, 0x0

    .restart local v0    # "i":I
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v3

    .restart local v3    # "n":I
    :goto_2
    if-ge v0, v3, :cond_5

    .line 1163
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 1164
    .restart local v2    # "item":Landroid/view/MenuItem;
    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0f02ce

    if-ne v4, v5, :cond_4

    .line 1165
    invoke-interface {v2, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1166
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1167
    if-eqz v1, :cond_3

    .line 1168
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1162
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1170
    :cond_3
    invoke-interface {v2, v7}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_3

    .line 1172
    :cond_4
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_3

    .line 1176
    .end local v0    # "i":I
    .end local v1    # "isSelected":Z
    .end local v2    # "item":Landroid/view/MenuItem;
    .end local v3    # "n":I
    :cond_5
    return-void
.end method

.method protected prePenSelectionRemoveItem(I)Z
    .locals 8
    .param p1, "position"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 1814
    const/4 v1, 0x0

    .line 1815
    .local v1, "remove":Z
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v2, v3, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 1817
    .local v0, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v0, :cond_0

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumMode:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v3

    .line 1835
    :goto_0
    return v2

    .line 1821
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1822
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v4

    const v5, 0x7f0e029e

    new-array v6, v7, [Ljava/lang/Object;

    move-object v2, v0

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v3

    invoke-virtual {v4, v5, v6}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 1823
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCaneExtraHapticEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1824
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/16 v3, 0x4e20

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    .line 1825
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1826
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1827
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->updateFolderItem(I)V

    .line 1828
    const/4 v1, 0x1

    .line 1831
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1832
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    invoke-virtual {p0, v2, v7, v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isAvailableCount(IZZ)Z

    .line 1833
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateActionBarSelectionMode(Lcom/sec/android/gallery3d/data/MediaSet;)V

    :cond_4
    move v2, v1

    .line 1835
    goto :goto_0
.end method

.method public processEvent(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 11
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 732
    move-object v2, p2

    check-cast v2, Lcom/sec/samsung/gallery/core/Event;

    .line 733
    .local v2, "ev":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v3

    .line 735
    .local v3, "eventType":I
    sget-object v6, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "processEvent = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 736
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_DELETE_SELECTION_MODE:I

    if-ne v3, v6, :cond_1

    .line 737
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsDeleteMode:Z

    .line 738
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->scaleAnimationForSelectionMode()V

    .line 739
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->enterSelectionMode(ZZ)V

    .line 741
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 742
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateActionBarSelectionMode(Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 934
    :cond_0
    :goto_0
    return-void

    .line 744
    :cond_1
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_DELETE_DIALOG:I

    if-ne v3, v6, :cond_2

    .line 745
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->showDeleteDialog()V

    goto :goto_0

    .line 746
    :cond_2
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_SELECT_ALL:I

    if-ne v3, v6, :cond_3

    .line 747
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->selectAll()V

    goto :goto_0

    .line 748
    :cond_3
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_UNSELECT_ALL:I

    if-ne v3, v6, :cond_4

    .line 749
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->unselectAll()V

    goto :goto_0

    .line 750
    :cond_4
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_EXIT_SELECTION_MODE:I

    if-ne v3, v6, :cond_5

    .line 751
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->exitSelectionMode()V

    goto :goto_0

    .line 752
    :cond_5
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_COPY_FILES:I

    if-ne v3, v6, :cond_6

    .line 753
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v7

    const/4 v8, 0x7

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    .line 755
    .local v4, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v4, :cond_0

    .line 756
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-virtual {v6, v4, v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAlbumListDialogToCopyMove(Lcom/sec/android/gallery3d/data/MediaSet;IZI)V

    goto :goto_0

    .line 759
    .end local v4    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_6
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_MOVE_FILES:I

    if-ne v3, v6, :cond_7

    .line 760
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v7

    const/4 v8, 0x7

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    .line 762
    .restart local v4    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v4, :cond_0

    .line 763
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-virtual {v6, v4, v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAlbumListDialogToCopyMove(Lcom/sec/android/gallery3d/data/MediaSet;IZI)V

    goto :goto_0

    .line 766
    .end local v4    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_7
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_NEW_ALBUM_DONE:I

    if-ne v3, v6, :cond_8

    .line 767
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->showNewAlbumCopyMoveDialog()V

    goto :goto_0

    .line 768
    :cond_8
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_NEW_ALBUM_CANCEL:I

    if-ne v3, v6, :cond_a

    .line 769
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalSelectedItems()I

    move-result v6

    if-lez v6, :cond_9

    .line 770
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->showNewAlbumCancelDialog()V

    goto :goto_0

    .line 772
    :cond_9
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->onBackPressed()V

    goto :goto_0

    .line 774
    :cond_a
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_FROM_SECRETBOX:I

    if-ne v3, v6, :cond_b

    .line 775
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v7

    const/4 v8, 0x7

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    .line 777
    .restart local v4    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v4, :cond_0

    .line 778
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    const/4 v8, 0x1

    const/4 v9, 0x3

    invoke-virtual {v6, v4, v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAlbumListDialogToCopyMove(Lcom/sec/android/gallery3d/data/MediaSet;IZI)V

    goto/16 :goto_0

    .line 781
    .end local v4    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_b
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_FILTER_DATA:I

    if-ne v3, v6, :cond_c

    .line 782
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->handleFilterPhotos()V

    goto/16 :goto_0

    .line 783
    :cond_c
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_SLIDESHOW_SETTING:I

    if-ne v3, v6, :cond_11

    .line 784
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isIncomingCall(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 785
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v7, 0x7f0e0066

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 788
    :cond_d
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideSettingPopup:Z

    if-nez v6, :cond_e

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isExternalDisplayAvailable(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 789
    :cond_e
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 790
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 791
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v7, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v7, v8}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 792
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v7, "SLIDE_SHOW_SETTING_VIEW"

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 793
    new-instance v6, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    const-string v7, "SLIDE_SHOW_SETTING_VIEW"

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v6, v7, v8}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;-><init>(Ljava/lang/String;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    .line 794
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->setSlideshowSettingEnable(Z)V

    .line 799
    :goto_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    if-eqz v6, :cond_f

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setSlideShowMode(Z)V

    .line 800
    :cond_f
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->setSlideShowMode(Z)V

    goto/16 :goto_0

    .line 796
    :cond_10
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v7, "PREPARE_SLIDE_SHOW_DATA"

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6, v7, v8}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 801
    :cond_11
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SELECTION_MODE:I

    if-ne v3, v6, :cond_12

    .line 802
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->scaleAnimationForSelectionMode()V

    .line 803
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->enterSelectionMode(ZZ)V

    .line 804
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 805
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateActionBarSelectionMode(Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto/16 :goto_0

    .line 807
    :cond_12
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_START_NEW_ALBUM:I

    if-ne v3, v6, :cond_14

    .line 808
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->hasLocalMediaSet()Z

    move-result v6

    if-eqz v6, :cond_13

    .line 809
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showNewAlbumDialog(Z)V

    goto/16 :goto_0

    .line 811
    :cond_13
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v7, 0x7f0e02eb

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 812
    :cond_14
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SEARCH_MODE:I

    if-ne v3, v6, :cond_15

    .line 813
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->enterSearchMode()V

    goto/16 :goto_0

    .line 814
    :cond_15
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_PRESS_SEARCH:I

    if-ne v3, v6, :cond_17

    .line 815
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    iget v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 816
    .local v1, "curMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v1, :cond_16

    .line 817
    sget-object v6, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    const-string v7, "curMediaSet is null!"

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 821
    :cond_16
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/core/Event;->getData()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/data/OcrClustering;->getSearchPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetPath:Ljava/lang/String;

    goto/16 :goto_0

    .line 828
    .end local v1    # "curMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_17
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_SEARCH_FIELD_EMPTY:I

    if-ne v3, v6, :cond_18

    .line 829
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    iget v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 830
    .restart local v1    # "curMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 831
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/data/OcrClustering;->getSearchPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetPath:Ljava/lang/String;

    goto/16 :goto_0

    .line 833
    .end local v1    # "curMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_18
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_BURST_SHOT_PLAY:I

    if-ne v3, v6, :cond_19

    .line 834
    const/4 v6, 0x1

    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->addToSlideshowProxy(Z)V

    goto/16 :goto_0

    .line 835
    :cond_19
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_IMPORT:I

    if-ne v3, v6, :cond_1a

    .line 836
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v7, "SHOW_IMPORT_DIALOG"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 839
    :cond_1a
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_LAST_SHARE_APP:I

    if-ne v3, v6, :cond_1b

    .line 840
    const/4 v6, 0x1

    new-array v5, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v7, v5, v6

    .line 843
    .local v5, "params":[Ljava/lang/Object;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v7, "LAST_SHARE_APP"

    invoke-virtual {v6, v7, v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 844
    .end local v5    # "params":[Ljava/lang/Object;
    :cond_1b
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_HIDDEN_ALBUMS:I

    if-ne v3, v6, :cond_1c

    .line 845
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->startHiddenAlbumsViewState()V

    goto/16 :goto_0

    .line 846
    :cond_1c
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SELECTION_MODE_FOR_STORY_ALBUM:I

    if-ne v3, v6, :cond_1d

    .line 847
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    if-eqz v6, :cond_0

    .line 848
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->scaleAnimationForSelectionMode()V

    .line 849
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->enterSelectionMode(ZZ)V

    .line 850
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->enterEventSelectionMode()V

    .line 851
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->misFilteredForStoryAlbum:Z

    .line 852
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 853
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v7, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForStoryAlbum;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v7, v8}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForStoryAlbum;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 854
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    goto/16 :goto_0

    .line 856
    :cond_1d
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SELECTION_MODE_FOR_EDIT:I

    if-ne v3, v6, :cond_1e

    .line 857
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->scaleAnimationForSelectionMode()V

    .line 858
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->enterSelectionMode(ZZ)V

    .line 859
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->enterEventSelectionMode()V

    .line 860
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 861
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v7, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForImageEdit;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v7, v8}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForImageEdit;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 862
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    goto/16 :goto_0

    .line 863
    :cond_1e
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SELECTION_MODE_FOR_MAKE_COLLAGE:I

    if-ne v3, v6, :cond_1f

    .line 864
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    if-eqz v6, :cond_0

    .line 865
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->scaleAnimationForSelectionMode()V

    .line 866
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->enterSelectionMode(ZZ)V

    .line 867
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->enterEventSelectionMode()V

    .line 868
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mFilteredItems:Z

    .line 869
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 870
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v7, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForCollage;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v7, v8}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForCollage;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 871
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    goto/16 :goto_0

    .line 873
    :cond_1f
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_CREATE_VIDEO_ALBUM:I

    if-ne v3, v6, :cond_21

    .line 874
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v6

    if-eqz v6, :cond_20

    .line 875
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->enterCreateVideoAlbum()V

    goto/16 :goto_0

    .line 876
    :cond_20
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    if-eqz v6, :cond_0

    .line 877
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->scaleAnimationForSelectionMode()V

    .line 878
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->enterSelectionMode(ZZ)V

    .line 879
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->enterEventSelectionMode()V

    .line 880
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->misFilteredForVideoAlbum:Z

    .line 881
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 882
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v7, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForCreateVideoAlbum;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v7, v8}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForCreateVideoAlbum;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 883
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    goto/16 :goto_0

    .line 885
    :cond_21
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_HIDE_ITEMS_FOR_NORMAL_PHOTO_VIEW:I

    if-ne v3, v6, :cond_23

    .line 886
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    if-eqz v6, :cond_0

    .line 887
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->scaleAnimationForSelectionMode()V

    .line 888
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->enterSelectionMode(ZZ)V

    .line 889
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->enterHideItemInSelectionMode()V

    .line 890
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->misFilteredForVideoAlbum:Z

    .line 891
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 892
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v7, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForHideItem;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v7, v8}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForHideItem;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 893
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 894
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v6, v6, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mFilteredItems:Z

    if-nez v6, :cond_22

    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->misFilteredForVideoAlbum:Z

    if-eqz v6, :cond_0

    .line 895
    :cond_22
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v7, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    const/4 v8, 0x4

    const/4 v9, 0x1

    invoke-virtual {v6, v7, v8, v9}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    goto/16 :goto_0

    .line 897
    :cond_23
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_MAKE_BUSRTSHOT_PICTURE:I

    if-ne v3, v6, :cond_24

    .line 898
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->editBurstShot()V

    goto/16 :goto_0

    .line 899
    :cond_24
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_CATEGORY:I

    if-ne v3, v6, :cond_25

    .line 900
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->getContentsForFaceTag()Ljava/util/ArrayList;

    move-result-object v0

    .line 901
    .local v0, "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const/4 v6, 0x5

    new-array v5, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v0, v5, v6

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaItemArrayList()Ljava/util/ArrayList;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    sget-object v7, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;->REMOVE_PHOTO_TAG:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    .line 905
    .restart local v5    # "params":[Ljava/lang/Object;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v7, "START_CATEGORY_TAG"

    invoke-virtual {v6, v7, v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 906
    .end local v0    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v5    # "params":[Ljava/lang/Object;
    :cond_25
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_EDIT_CATEGORY:I

    if-ne v3, v6, :cond_26

    .line 907
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->getContentsForFaceTag()Ljava/util/ArrayList;

    move-result-object v0

    .line 908
    .restart local v0    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const/4 v6, 0x5

    new-array v5, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v0, v5, v6

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaItemArrayList()Ljava/util/ArrayList;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    sget-object v7, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;->EDIT_TAG:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    .line 912
    .restart local v5    # "params":[Ljava/lang/Object;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v7, "START_CATEGORY_TAG"

    invoke-virtual {v6, v7, v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 913
    .end local v0    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v5    # "params":[Ljava/lang/Object;
    :cond_26
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_SORT_BY_LATEST:I

    if-ne v3, v6, :cond_27

    .line 914
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string/jumbo v7, "sort_by_type_time"

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 916
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Lcom/sec/android/gallery3d/app/GalleryApp;->setSortByType(I)V

    .line 917
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->forceReload()V

    goto/16 :goto_0

    .line 919
    :cond_27
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_SORT_BY_OLDEST:I

    if-ne v3, v6, :cond_28

    .line 920
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string/jumbo v7, "sort_by_type_time"

    const/4 v8, 0x1

    invoke-static {v6, v7, v8}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 922
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v6

    const/4 v7, 0x1

    invoke-interface {v6, v7}, Lcom/sec/android/gallery3d/app/GalleryApp;->setSortByType(I)V

    .line 923
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->forceReload()V

    goto/16 :goto_0

    .line 924
    :cond_28
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_CREATE_GIF:I

    if-ne v3, v6, :cond_0

    .line 925
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsGifMode:Z

    .line 926
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    .line 927
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->scaleAnimationForSelectionMode()V

    .line 928
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->enterSelectionMode(ZZ)V

    .line 930
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 931
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateActionBarSelectionMode(Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto/16 :goto_0
.end method

.method public resetSlideshowLayout()V
    .locals 1

    .prologue
    .line 536
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_0

    .line 537
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->resetLayout()V

    .line 539
    :cond_0
    return-void
.end method

.method protected selectAlbum(I)I
    .locals 9
    .param p1, "position"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 1858
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v5, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    .line 1859
    .local v3, "selectedMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v1, 0x0

    .line 1860
    .local v1, "retVal":I
    const/4 v2, 0x0

    .line 1861
    .local v2, "selectedMSetItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz v3, :cond_2

    .line 1862
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v5

    invoke-virtual {v3, v8, v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v2

    .line 1867
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_3

    .line 1868
    :cond_0
    sget-object v4, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    const-string v5, "selectedMSetItems is null or size is 0"

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v1

    .line 1914
    :cond_1
    :goto_0
    return v4

    :cond_2
    move v4, v1

    .line 1864
    goto :goto_0

    .line 1871
    :cond_3
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumMode:Z

    if-eqz v5, :cond_4

    .line 1872
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->removeItemsAddedToNewAlbumProxy(Ljava/util/List;)V

    .line 1874
    :cond_4
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->getItemsToAddCount(Ljava/util/List;)I

    move-result v0

    .line 1878
    .local v0, "itemsToAdd":I
    iget v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    if-ne v5, p1, :cond_5

    .line 1879
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v6, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$11;

    invoke-direct {v6, p0, v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$11;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;I)V

    invoke-virtual {v5, v6}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1889
    if-nez v0, :cond_1

    .line 1890
    const/4 v4, 0x2

    goto :goto_0

    .line 1895
    :cond_5
    if-eqz v3, :cond_6

    .line 1896
    if-nez v0, :cond_8

    .line 1897
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v5

    const v6, 0x7f0e029e

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v8

    invoke-virtual {v5, v6, v4}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 1898
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4, v3, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List;)V

    .line 1899
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeShareProperty(Ljava/util/List;)V

    .line 1900
    const/4 v1, 0x2

    .line 1909
    :cond_6
    :goto_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->updateFolderItem(I)V

    .line 1911
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1912
    invoke-direct {p0, v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateActionBarSelectionMode(Lcom/sec/android/gallery3d/data/MediaSet;)V

    :cond_7
    move v4, v1

    .line 1914
    goto :goto_0

    .line 1902
    :cond_8
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v5

    const v6, 0x7f0e029d

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v8

    invoke-virtual {v5, v6, v4}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 1903
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4, v3, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List;)V

    .line 1904
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Ljava/util/List;)V

    .line 1905
    const/4 v1, 0x1

    goto :goto_1
.end method

.method protected selectAll()V
    .locals 17

    .prologue
    .line 1970
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v12}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getAllCount()I

    move-result v8

    .line 1971
    .local v8, "realCount":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v12, v12, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v12, :cond_2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/data/MediaSet;->isCameraRoll()Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1972
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v12, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    const/4 v13, 0x0

    const/4 v14, 0x1

    invoke-virtual {v12, v13, v14}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->getMediaItemCountForBurstshot(Ljava/util/ArrayList;Z)I

    move-result v8

    .line 1973
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->setShowAllGroup(Z)V

    .line 1974
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setShowAllGroup(Z)V

    .line 1983
    :goto_0
    const/4 v4, -0x1

    .line 1984
    .local v4, "maxAddCount":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v12, v13}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v2

    .line 1985
    .local v2, "currentSelectedCount":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v12}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 1986
    .local v3, "intent":Landroid/content/Intent;
    const-string v12, "pick-max-item"

    const/4 v13, -0x1

    invoke-virtual {v3, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 1988
    .local v5, "maxPickCount":I
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsGifMode:Z

    if-eqz v12, :cond_0

    .line 1989
    const/16 v12, 0x14

    if-le v8, v12, :cond_0

    .line 1990
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0e0114

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const/16 v16, 0x14

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v12, v13, v14}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 1991
    .local v9, "text":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v12, v9}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 1995
    .end local v9    # "text":Ljava/lang/String;
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    if-eqz v12, :cond_9

    .line 1996
    const/4 v12, -0x1

    if-eq v5, v12, :cond_5

    .line 1997
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v11

    .line 1998
    .local v11, "totalSelectedCount":I
    sub-int v4, v5, v11

    .line 1999
    sget-boolean v12, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v12, :cond_4

    .line 2000
    if-nez v4, :cond_9

    .line 2001
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateMaximumSelectionNumberExceeded(I)V

    .line 2067
    .end local v11    # "totalSelectedCount":I
    :cond_1
    :goto_1
    return-void

    .line 1975
    .end local v2    # "currentSelectedCount":I
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "maxAddCount":I
    .end local v5    # "maxPickCount":I
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumMode:Z

    if-eqz v12, :cond_3

    .line 1976
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v12, v13}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v6

    .line 1977
    .local v6, "newAlbumCount":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getCount(I)I

    move-result v12

    sub-int v8, v12, v6

    .line 1978
    goto/16 :goto_0

    .line 1980
    .end local v6    # "newAlbumCount":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getCount(I)I

    move-result v8

    goto/16 :goto_0

    .line 2005
    .restart local v2    # "currentSelectedCount":I
    .restart local v3    # "intent":Landroid/content/Intent;
    .restart local v4    # "maxAddCount":I
    .restart local v5    # "maxPickCount":I
    .restart local v11    # "totalSelectedCount":I
    :cond_4
    sub-int v1, v8, v2

    .line 2006
    .local v1, "currentRemainCount":I
    if-le v1, v4, :cond_9

    .line 2007
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateMaximumSelectionNumberExceeded(I)V

    goto :goto_1

    .line 2011
    .end local v1    # "currentRemainCount":I
    .end local v11    # "totalSelectedCount":I
    :cond_5
    sget-boolean v12, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v12, :cond_6

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mFromGalleryWidget:Z

    if-eqz v12, :cond_6

    .line 2012
    const/16 v5, 0x3e8

    .line 2013
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v11

    .line 2014
    .restart local v11    # "totalSelectedCount":I
    sub-int v4, v5, v11

    .line 2015
    sget-boolean v12, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v12, :cond_9

    .line 2016
    if-nez v4, :cond_9

    .line 2017
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateMaximumSelectionNumberExceeded(I)V

    goto :goto_1

    .line 2021
    .end local v11    # "totalSelectedCount":I
    :cond_6
    sget-boolean v12, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v12, :cond_7

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mFromGifMaker:Z

    if-eqz v12, :cond_7

    .line 2022
    const-string v12, "select_num_key"

    const/4 v13, -0x1

    invoke-virtual {v3, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 2023
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v11

    .line 2024
    .restart local v11    # "totalSelectedCount":I
    sub-int v4, v5, v11

    .line 2025
    sget-boolean v12, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v12, :cond_9

    .line 2026
    if-nez v4, :cond_9

    .line 2027
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateMaximumSelectionNumberExceeded(I)V

    goto :goto_1

    .line 2031
    .end local v11    # "totalSelectedCount":I
    :cond_7
    sget-boolean v12, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v12, :cond_9

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mFromInsideGallery:Z

    if-nez v12, :cond_9

    .line 2032
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsGifMode:Z

    if-eqz v12, :cond_8

    .line 2033
    const/16 v5, 0x14

    .line 2036
    :goto_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v11

    .line 2037
    .restart local v11    # "totalSelectedCount":I
    sub-int v4, v5, v11

    .line 2038
    sget-boolean v12, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v12, :cond_9

    .line 2039
    if-nez v4, :cond_9

    .line 2040
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateMaximumSelectionNumberExceeded(I)V

    goto/16 :goto_1

    .line 2035
    .end local v11    # "totalSelectedCount":I
    :cond_8
    sget v5, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    goto :goto_2

    .line 2047
    :cond_9
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v12}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v10

    .line 2048
    .local v10, "topMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    sget-boolean v12, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v12, :cond_a

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    if-eqz v12, :cond_a

    const/4 v12, -0x1

    if-eq v5, v12, :cond_a

    if-lez v4, :cond_a

    .line 2049
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v2, v5}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->selectAllProcess(III)V

    .line 2058
    :goto_3
    sget-boolean v12, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v12, :cond_1

    .line 2059
    const-string v12, "pick-max-item"

    const/4 v13, -0x1

    invoke-virtual {v3, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 2060
    .local v7, "pickItemCount":I
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    if-eqz v12, :cond_1

    const/4 v12, -0x1

    if-eq v7, v12, :cond_1

    .line 2061
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v11

    .line 2062
    .restart local v11    # "totalSelectedCount":I
    if-lt v11, v7, :cond_1

    .line 2063
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateMaximumSelectionNumberExceeded(I)V

    goto/16 :goto_1

    .line 2050
    .end local v7    # "pickItemCount":I
    .end local v11    # "totalSelectedCount":I
    :cond_a
    const/16 v12, 0x3e8

    if-le v8, v12, :cond_b

    .line 2051
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->selectAllProcessTask(II)V

    goto :goto_3

    .line 2052
    :cond_b
    if-eqz v10, :cond_c

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v12

    sget v13, Lcom/sec/android/gallery3d/util/GalleryFeature;->mMaxCountForSelectionLoadingTask:I

    if-le v12, v13, :cond_c

    .line 2053
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->selectAllProcessTask(II)V

    goto :goto_3

    .line 2055
    :cond_c
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v2, v5}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->selectAllProcess(III)V

    goto :goto_3
.end method

.method protected selectItem(I)I
    .locals 14
    .param p1, "position"    # I

    .prologue
    .line 1720
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    const/4 v10, 0x0

    invoke-virtual {v9, v10, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v6

    .line 1723
    .local v6, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v6, :cond_0

    iget-boolean v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumMode:Z

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v9, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1724
    :cond_0
    const/4 v7, 0x0

    .line 1810
    :cond_1
    :goto_0
    return v7

    .line 1727
    :cond_2
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v9, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1728
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v10

    const v11, 0x7f0e029e

    const/4 v9, 0x1

    new-array v12, v9, [Ljava/lang/Object;

    const/4 v13, 0x0

    move-object v9, v6

    check-cast v9, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v12, v13

    invoke-virtual {v10, v11, v12}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 1729
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v9}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCaneExtraHapticEnabled(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1730
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/16 v10, 0x4e20

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    .line 1731
    :cond_3
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v9, v10, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1732
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v9, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1733
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget v10, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-virtual {v9, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->updateFolderItem(I)V

    .line 1734
    const/4 v7, 0x2

    .line 1794
    .local v7, "retVal":I
    :goto_1
    iget-boolean v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsGifMode:Z

    if-eqz v9, :cond_13

    .line 1795
    const/16 v5, 0x14

    .line 1797
    .local v5, "maxCount":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v9

    if-le v9, v5, :cond_13

    .line 1798
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v9, v10, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1799
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v9, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1800
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0e0114

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v9, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 1801
    .local v8, "text":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v9, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 1802
    const/4 v7, 0x0

    goto :goto_0

    .line 1736
    .end local v5    # "maxCount":I
    .end local v7    # "retVal":I
    .end local v8    # "text":Ljava/lang/String;
    :cond_4
    instance-of v9, v6, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-nez v9, :cond_5

    instance-of v9, v6, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    if-nez v9, :cond_5

    instance-of v9, v6, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-nez v9, :cond_5

    instance-of v9, v6, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-eqz v9, :cond_9

    :cond_5
    const/4 v1, 0x1

    .line 1738
    .local v1, "isHide":Z
    :goto_2
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isHideItemInSelectionMode()Z

    move-result v9

    if-eqz v9, :cond_a

    instance-of v9, v6, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v9, :cond_6

    move-object v9, v6

    check-cast v9, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->isHideBlockedItem()Z

    move-result v9

    if-nez v9, :cond_7

    :cond_6
    if-eqz v1, :cond_a

    :cond_7
    const/4 v4, 0x1

    .local v4, "isHideBlockedItem":Z
    :goto_3
    move-object v9, v6

    .line 1741
    check-cast v9, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {p0, v9}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isAvailableSelect(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v9

    if-eqz v9, :cond_8

    if-eqz v4, :cond_b

    .line 1742
    :cond_8
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 1736
    .end local v1    # "isHide":Z
    .end local v4    # "isHideBlockedItem":Z
    :cond_9
    const/4 v1, 0x0

    goto :goto_2

    .line 1738
    .restart local v1    # "isHide":Z
    :cond_a
    const/4 v4, 0x0

    goto :goto_3

    .line 1745
    .restart local v4    # "isHideBlockedItem":Z
    :cond_b
    iget-boolean v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->misFilteredForStoryAlbum:Z

    if-eqz v9, :cond_d

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaObject;->getMediaType()I

    move-result v9

    const/4 v10, 0x4

    if-eq v9, v10, :cond_c

    move-object v9, v6

    check-cast v9, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v9

    if-nez v9, :cond_c

    move-object v9, v6

    check-cast v9, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v9

    if-nez v9, :cond_d

    move-object v9, v6

    check-cast v9, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v9

    if-eqz v9, :cond_d

    .line 1747
    :cond_c
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v10, 0x7f0e0107

    invoke-static {v9, v10}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 1748
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 1751
    :cond_d
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inDeleteSelectionMode()Z

    move-result v9

    if-eqz v9, :cond_e

    invoke-static {v6}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->checkDeletableMediaObject(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v9

    if-nez v9, :cond_e

    .line 1752
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v10, 0x7f0e0107

    invoke-static {v9, v10}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 1753
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 1756
    :cond_e
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v10

    const v11, 0x7f0e029d

    const/4 v9, 0x1

    new-array v12, v9, [Ljava/lang/Object;

    const/4 v13, 0x0

    move-object v9, v6

    check-cast v9, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v12, v13

    invoke-virtual {v10, v11, v12}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 1757
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v9}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCaneExtraHapticEnabled(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 1758
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/16 v10, 0x4e2a

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    :cond_f
    move-object v9, v6

    .line 1760
    check-cast v9, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v2

    .line 1761
    .local v2, "groupId":J
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    move-object v9, v6

    check-cast v9, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v10, v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isBurstShot(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v9

    if-eqz v9, :cond_12

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->addShowGroupId(Ljava/lang/Long;)Z

    move-result v9

    if-eqz v9, :cond_12

    .line 1764
    iget-boolean v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsGifMode:Z

    if-eqz v9, :cond_10

    .line 1765
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v9, v2, v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getBurstShotItems(J)Ljava/util/ArrayList;

    move-result-object v0

    .line 1766
    .local v0, "burstShotItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz v0, :cond_10

    .line 1767
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v10

    add-int/2addr v9, v10

    const/16 v10, 0x14

    if-le v9, v10, :cond_10

    .line 1768
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0e0114

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const/16 v13, 0x14

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v9, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 1769
    .restart local v8    # "text":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v9, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 1770
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->forceReload()V

    .line 1771
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 1776
    .end local v0    # "burstShotItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v8    # "text":Ljava/lang/String;
    :cond_10
    iget-boolean v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPenSelect:Z

    if-eqz v9, :cond_11

    .line 1777
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGroupIDList:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1784
    :goto_4
    const/4 v7, 0x3

    .line 1791
    .restart local v7    # "retVal":I
    :goto_5
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget v10, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-virtual {v9, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->updateFolderItem(I)V

    goto/16 :goto_1

    .line 1779
    .end local v7    # "retVal":I
    :cond_11
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v9, v2, v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getBurstShotItems(J)Ljava/util/ArrayList;

    move-result-object v0

    .line 1780
    .restart local v0    # "burstShotItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v9, v10, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List;)V

    .line 1781
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v9, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Ljava/util/List;)V

    .line 1782
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->forceReload()V

    goto :goto_4

    .line 1786
    .end local v0    # "burstShotItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_12
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v9, v10, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1787
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v9, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1788
    const/4 v7, 0x1

    .restart local v7    # "retVal":I
    goto :goto_5

    .line 1806
    .end local v1    # "isHide":Z
    .end local v2    # "groupId":J
    .end local v4    # "isHideBlockedItem":Z
    :cond_13
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1807
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v9

    const/4 v10, 0x1

    iget-boolean v11, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    invoke-virtual {p0, v9, v10, v11}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isAvailableCount(IZZ)Z

    .line 1808
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-direct {p0, v9}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateActionBarSelectionMode(Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto/16 :goto_0
.end method

.method public setBasicReferences()V
    .locals 1

    .prologue
    .line 522
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    .line 523
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    .line 524
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    .line 525
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    .line 526
    return-void
.end method

.method public setEnvGLViewInitialize()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 463
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mOnComposeAlbumClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->setOnItemClickListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;)V

    .line 464
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mOnComposeAlbumLongClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemLongClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->setOnItemLongClickListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemLongClickListener;)V

    .line 465
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mOnGenericMotionListenerSplit:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;

    invoke-virtual {v0, v2, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->setOnGenericMotionListener(ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;)V

    .line 467
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mOnComposeItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setOnItemClickListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;)V

    .line 468
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mOnComposeItemLongClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemLongClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setOnItemLongClickListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemLongClickListener;)V

    .line 469
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mOnComposeStatusChangedListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setOnStatusChangedListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;)V

    .line 470
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mOnComposeKeyListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnKeyListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setOnKeyListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnKeyListener;)V

    .line 471
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mOnHoverClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnHoverListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setOnHoverListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnHoverListener;)V

    .line 472
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mOnGenericMotionListenerPhoto:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;

    invoke-virtual {v0, v2, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setOnGenericMotionListener(ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;)V

    .line 473
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mOnSwitchViewListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnSwitchViewListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setOnSwitchViewListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnSwitchViewListener;)V

    .line 474
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mOnPenSelectionListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setOnPenSelectionListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;)V

    .line 475
    return-void
.end method

.method public setEnvOnBackPressed()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 487
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 488
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->setSlideshowSettingEnable(Z)V

    .line 489
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    if-eqz v0, :cond_0

    .line 490
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setSlideShowMode(Z)V

    .line 491
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    if-eqz v0, :cond_1

    .line 492
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->setSlideShowMode(Z)V

    .line 494
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 495
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 496
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForEdit;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 497
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateActionBarSelectionMode(Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 498
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateCountOnActionBar()V

    .line 505
    :goto_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsCategoryTagType:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-nez v0, :cond_2

    .line 506
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 507
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    .line 509
    :cond_3
    return-void

    .line 501
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 502
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 503
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateAlbumNameOfActionBar()V

    goto :goto_0
.end method

.method public setEnvOnCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 328
    if-eqz p1, :cond_4

    const-string v2, "KEY_ITEM_POSITION"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    :goto_0
    iput v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaItemIndex:I

    .line 329
    if-eqz p1, :cond_5

    const-string v2, "KEY_NO_SPLIT_MODE"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    :goto_1
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsNoSplitMode:Z

    .line 331
    if-eqz p1, :cond_6

    const-string v2, "KEY_MEDIA_SET_PATH"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentTopMediaSetPath:Ljava/lang/String;

    .line 332
    if-eqz p1, :cond_0

    const-string v2, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_0
    iput-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetPath:Ljava/lang/String;

    .line 333
    if-eqz p1, :cond_7

    const-string v2, "KEY_MEDIA_SET_POSITION"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    :goto_3
    iput v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    .line 334
    if-eqz p1, :cond_8

    const-string v2, "KEY_FROM_ALBUM_VIEW_STATE"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    :goto_4
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mFromAlbumViewState:Z

    .line 335
    if-eqz p1, :cond_9

    const-string v2, "is_from_camera"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    :goto_5
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsFromCamera:Z

    .line 336
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isFromGalleryWidget()Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mFromGalleryWidget:Z

    .line 337
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isFromInsideGallery()Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mFromInsideGallery:Z

    .line 338
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isSingleAlbumMode()Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsSingleAlbumMode:Z

    .line 339
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isFromGifMaker()Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mFromGifMaker:Z

    .line 341
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentTopMediaSetPath:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mTopMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 342
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetPath:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 343
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-nez v2, :cond_2

    .line 344
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mTopMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v2, :cond_1

    .line 345
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mTopMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 346
    :cond_1
    iput v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    .line 348
    :cond_2
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mUpdatePath:Z

    .line 349
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isPickMode()Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    .line 350
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    if-eqz v2, :cond_3

    .line 351
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 352
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "pick-max-item"

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 353
    .local v1, "pickMaxCount":I
    if-gtz v1, :cond_3

    .line 354
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mFromGalleryWidget:Z

    if-eqz v2, :cond_a

    .line 355
    const-string v2, "pick-max-item"

    const/16 v3, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 360
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "pickMaxCount":I
    :cond_3
    :goto_6
    return-void

    :cond_4
    move v2, v3

    .line 328
    goto/16 :goto_0

    :cond_5
    move v2, v3

    .line 329
    goto/16 :goto_1

    :cond_6
    move-object v2, v4

    .line 331
    goto/16 :goto_2

    :cond_7
    move v2, v3

    .line 333
    goto/16 :goto_3

    :cond_8
    move v2, v3

    .line 334
    goto :goto_4

    :cond_9
    move v2, v3

    .line 335
    goto :goto_5

    .line 357
    .restart local v0    # "intent":Landroid/content/Intent;
    .restart local v1    # "pickMaxCount":I
    :cond_a
    const-string v2, "pick-max-item"

    sget v3, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_6
.end method

.method public setEnvOnDestroy()V
    .locals 2

    .prologue
    .line 512
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mHandleDismissDialogsTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    if-eqz v0, :cond_0

    .line 513
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mHandleDismissDialogsTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->cancel(Z)Z

    .line 514
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mHandleDismissDialogsTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    .line 516
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v1, "PHOTO_VIEW_EXIT_SELECTION"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 517
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v1, "PHOTO_VIEW_MEDIA_EJECT"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 518
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showNewAlbumDialog(Z)V

    .line 519
    return-void
.end method

.method public setEnvOnPause()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 478
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMainHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 479
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v1, "PHOTO_VIEW"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 480
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->setGenericMotionFocus(I)V

    .line 482
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    if-eqz v0, :cond_1

    .line 483
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setGenericMotionFocus(I)V

    .line 484
    :cond_1
    return-void
.end method

.method public setEnvOnResume()V
    .locals 12

    .prologue
    const/4 v11, -0x1

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 363
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v8, :cond_9

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v8

    if-eqz v8, :cond_9

    move v2, v6

    .line 365
    .local v2, "isSlideShowSettingEnabled":Z
    :goto_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v8

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->IsCategoryView(Lcom/sec/samsung/gallery/core/TabTagType;Ljava/lang/Boolean;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsCategoryTagType:Z

    .line 366
    iget-boolean v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsCategoryTagType:Z

    if-eqz v8, :cond_a

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v8

    if-nez v8, :cond_a

    .line 367
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->resume()V

    .line 370
    :goto_1
    iget-boolean v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mUpdatePath:Z

    if-eqz v8, :cond_c

    .line 371
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "mtpMode"

    invoke-static {v8, v9, v7}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    .line 372
    .local v1, "isMtpMode":Z
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->getTopSetPathByFilter()Ljava/lang/String;

    move-result-object v4

    .line 373
    .local v4, "path":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentTopMediaSetPath:Ljava/lang/String;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentTopMediaSetPath:Ljava/lang/String;

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    :cond_0
    if-eqz v1, :cond_3

    .line 374
    :cond_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentTopMediaSetPath:Ljava/lang/String;

    if-eqz v8, :cond_2

    if-eqz v1, :cond_2

    .line 375
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentTopMediaSetPath:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/data/DataManager;->getAddMtpTopPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentTopMediaSetPath:Ljava/lang/String;

    .line 377
    :cond_2
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentTopMediaSetPath:Ljava/lang/String;

    if-nez v8, :cond_b

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v8

    :goto_2
    iput-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mTopMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 379
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->reloadData()V

    .line 384
    .end local v1    # "isMtpMode":Z
    .end local v4    # "path":Ljava/lang/String;
    :cond_3
    :goto_3
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 385
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewExitSelectionMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 386
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewMediaEjectMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 387
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v9, "SLIDESHOW_SETTINGS"

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 388
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mShowSlideshowsettingsMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 390
    if-eqz v2, :cond_4

    .line 391
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v8, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->setSlideShowMode(Z)V

    .line 392
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v8, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setSlideShowMode(Z)V

    .line 394
    :cond_4
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    iput-boolean v7, v8, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mDetailViewStarted:Z

    .line 395
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v8

    if-eqz v8, :cond_13

    .line 396
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v8

    if-lez v8, :cond_5

    .line 399
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->tempTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    monitor-enter v8

    .line 400
    :try_start_0
    new-instance v9, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    const/4 v10, 0x0

    invoke-direct {v9, p0, v10}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;)V

    iput-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mHandleDismissDialogsTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    .line 401
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mHandleDismissDialogsTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Void;

    invoke-virtual {v9, v10}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 402
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 404
    :cond_5
    if-eqz v2, :cond_d

    .line 405
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 406
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v7, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v7, v8}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 431
    :goto_4
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumMode:Z

    if-eqz v6, :cond_6

    .line 432
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 433
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v7, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v7, v8}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 448
    :cond_6
    :goto_5
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateAlbumNameOfActionBar()V

    .line 449
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->cloudSync()V

    .line 451
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateLastSharedIconAndTitle()V

    .line 453
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEventViewMode(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 454
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseStoryAlbum(Landroid/content/pm/PackageManager;)V

    .line 456
    :cond_7
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 457
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    iget v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    shl-int/lit8 v7, v7, 0x10

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->setGenericMotionFocus(I)V

    .line 458
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-virtual {v6, v11, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->updateBorder(II)V

    .line 460
    :cond_8
    return-void

    .end local v2    # "isSlideShowSettingEnabled":Z
    :cond_9
    move v2, v7

    .line 363
    goto/16 :goto_0

    .line 369
    .restart local v2    # "isSlideShowSettingEnabled":Z
    :cond_a
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v8

    invoke-virtual {v8, v7}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    goto/16 :goto_1

    .line 377
    .restart local v1    # "isMtpMode":Z
    .restart local v4    # "path":Ljava/lang/String;
    :cond_b
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentTopMediaSetPath:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v8

    goto/16 :goto_2

    .line 382
    .end local v1    # "isMtpMode":Z
    .end local v4    # "path":Ljava/lang/String;
    :cond_c
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mUpdatePath:Z

    goto/16 :goto_3

    .line 402
    :catchall_0
    move-exception v6

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6

    .line 408
    :cond_d
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v8

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->getMediaItemAdapterSize()I

    move-result v9

    if-ne v8, v9, :cond_10

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->getMediaItemAdapterSize()I

    move-result v8

    if-lez v8, :cond_10

    move v5, v6

    .line 410
    .local v5, "selectedAll":Z
    :goto_6
    if-nez v5, :cond_e

    .line 411
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 412
    .local v0, "intent":Landroid/content/Intent;
    const-string v8, "pick-max-item"

    invoke-virtual {v0, v8, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 413
    .local v3, "maxPickCount":I
    if-eq v3, v11, :cond_e

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v8

    if-ne v8, v3, :cond_e

    .line 414
    const/4 v5, 0x1

    .line 417
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v3    # "maxPickCount":I
    :cond_e
    iget-boolean v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    if-eqz v8, :cond_f

    .line 418
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v8, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->setShowAllGroup(Z)V

    .line 419
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v8, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setShowAllGroup(Z)V

    .line 421
    :cond_f
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumMode:Z

    if-eqz v6, :cond_11

    .line 422
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateNewAblumActionBarSelection()V

    goto/16 :goto_4

    .end local v5    # "selectedAll":Z
    :cond_10
    move v5, v7

    .line 408
    goto :goto_6

    .line 423
    .restart local v5    # "selectedAll":Z
    :cond_11
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->misFilteredForStoryAlbum:Z

    if-eqz v6, :cond_12

    .line 424
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 425
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v7, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForStoryAlbum;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v7, v8}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForStoryAlbum;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 426
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    goto/16 :goto_4

    .line 428
    :cond_12
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v8, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v6, v8, v7, v5}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    goto/16 :goto_4

    .line 435
    .end local v5    # "selectedAll":Z
    :cond_13
    if-eqz v2, :cond_14

    .line 436
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 437
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v7, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v7, v8}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    goto/16 :goto_5

    .line 438
    :cond_14
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    if-eqz v7, :cond_15

    .line 439
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v7, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->setShowAllGroup(Z)V

    .line 440
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v7, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setShowAllGroup(Z)V

    goto/16 :goto_5

    .line 443
    :cond_15
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsCategoryTagType:Z

    if-eqz v7, :cond_16

    .line 444
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 445
    :cond_16
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 446
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v7, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v7, v8}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    goto/16 :goto_5
.end method

.method public setFilteredMimeType()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2453
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2454
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_1

    .line 2455
    const/4 v0, 0x0

    .line 2456
    .local v0, "filterMimeType":Ljava/lang/String;
    const-string v4, "onlyJpg"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 2457
    .local v3, "onlyJpg":Z
    const-string v4, "only3gp"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 2458
    .local v2, "only3gp":Z
    if-eqz v3, :cond_2

    .line 2459
    const-string v0, "image/jpeg"

    .line 2463
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v4, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setFilterMimeType(Ljava/lang/String;)V

    .line 2465
    .end local v0    # "filterMimeType":Ljava/lang/String;
    .end local v2    # "only3gp":Z
    .end local v3    # "onlyJpg":Z
    :cond_1
    return-void

    .line 2460
    .restart local v0    # "filterMimeType":Ljava/lang/String;
    .restart local v2    # "only3gp":Z
    .restart local v3    # "onlyJpg":Z
    :cond_2
    if-eqz v2, :cond_0

    .line 2461
    const-string/jumbo v0, "video/3gp"

    goto :goto_0
.end method

.method public setOnSaveState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 542
    sget-object v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;

    const-string v1, "setOnSaveState"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    const-string v0, "KEY_ITEM_POSITION"

    iget v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaItemIndex:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 544
    const-string v0, "KEY_NO_SPLIT_MODE"

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsNoSplitMode:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 546
    const-string v0, "KEY_MEDIA_SET_PATH"

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentTopMediaSetPath:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    const-string v0, "KEY_MEDIA_ITEM_PATH"

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetPath:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    const-string v0, "KEY_MEDIA_SET_POSITION"

    iget v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 549
    const-string v0, "KEY_FROM_ALBUM_VIEW_STATE"

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mFromAlbumViewState:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 550
    const-string v0, "is_from_camera"

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsFromCamera:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 551
    return-void
.end method

.method public showDeleteDialog()V
    .locals 3

    .prologue
    .line 1358
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showDeleteDialog(ZZ)V

    .line 1359
    return-void
.end method

.method public showNewAlbumCancelDialog()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1368
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v3

    .line 1372
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "SHOW_NEW_ALBUM_CANCEL_DIALOG"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1374
    return-void
.end method

.method public startDrag(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    const/4 v4, 0x0

    .line 2468
    new-instance v0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;-><init>(Landroid/content/Context;)V

    .line 2469
    .local v0, "drag":Lcom/sec/samsung/gallery/view/common/DragAndDrop;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v1, v4, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v1, v4, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getItemImage(II)Landroid/graphics/Bitmap;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->startPhotosDrag(ILcom/sec/android/gallery3d/glcore/GlObject;Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    .line 2473
    return-void
.end method

.method protected unselectAll()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2111
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAllItemInMediaSet(Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 2112
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;I)V

    .line 2113
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->updateFolderItem(I)V

    .line 2115
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 2116
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    .line 2117
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateActionBarSelectionMode(Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 2118
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->refreshCheckState()V

    .line 2119
    return-void
.end method

.method protected updateAlbumNameOfActionBar()V
    .locals 8

    .prologue
    const v7, 0x7f0e0069

    const/4 v6, 0x1

    .line 1053
    iget v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    .line 1054
    .local v3, "position":I
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    if-ne v5, v6, :cond_1

    .line 1056
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    const-string v6, ""

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(Ljava/lang/String;)V

    .line 1093
    :cond_0
    :goto_0
    return-void

    .line 1059
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mTopMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v5, :cond_0

    .line 1061
    if-ltz v3, :cond_0

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mTopMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v5

    if-ge v3, v5, :cond_0

    .line 1063
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mTopMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v5, v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 1064
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v1, :cond_0

    .line 1067
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1069
    .local v0, "albumName":Ljava/lang/String;
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v5, :cond_2

    instance-of v5, v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    if-nez v5, :cond_2

    instance-of v5, v1, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    if-nez v5, :cond_2

    if-eqz v0, :cond_2

    .line 1072
    const-string v5, "/"

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1073
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1074
    .local v4, "values":[Ljava/lang/String;
    aget-object v0, v4, v6

    .line 1078
    .end local v4    # "values":[Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEventViewMode(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5, v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    if-eqz v0, :cond_4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1080
    :cond_3
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getEventAlbumTimeInfo()Ljava/lang/String;

    move-result-object v0

    .line 1083
    :cond_4
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v2

    .line 1084
    .local v2, "numberOfItemsInAlbum":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1085
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateActionBarSelectionMode(Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_0

    .line 1086
    :cond_5
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsCategoryTagType:Z

    if-eqz v5, :cond_6

    .line 1087
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v5, v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1088
    :cond_6
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v5

    instance-of v5, v5, Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    if-eqz v5, :cond_7

    .line 1089
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    const v6, 0x7f0e0074

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    goto/16 :goto_0

    .line 1091
    :cond_7
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v5, v0, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(Ljava/lang/String;I)V

    goto/16 :goto_0
.end method

.method public updateConfirmMenu()V
    .locals 6

    .prologue
    .line 1285
    const/4 v3, 0x0

    .line 1286
    .local v3, "name":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v1

    .line 1287
    .local v1, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 1288
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCurrentIndex()I

    move-result v4

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 1291
    .local v0, "mediaItem":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v0, :cond_2

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local v0    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemType()I

    move-result v4

    if-nez v4, :cond_2

    .line 1293
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    iget v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 1295
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_0

    .line 1296
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1298
    :cond_0
    if-eqz v3, :cond_1

    .line 1299
    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 1301
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v4, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateConfirm(Ljava/lang/String;)V

    .line 1304
    .end local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_2
    return-void
.end method

.method public updateCountOnActionBar()V
    .locals 3

    .prologue
    .line 2122
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 2123
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v0

    .line 2124
    .local v0, "checkedCount":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    .line 2125
    return-void
.end method
