.class Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$7;
.super Ljava/lang/Object;
.source "AbstractActionBarViewForSelection.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->setupPopupMenu(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)V
    .locals 0

    .prologue
    .line 449
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$7;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 453
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$7;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenu:Landroid/widget/ListPopupWindow;

    if-eqz v1, :cond_1

    .line 454
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$7;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1}, Landroid/widget/ListPopupWindow;->isShowing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 455
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$7;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 457
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$7;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1}, Landroid/widget/ListPopupWindow;->show()V

    .line 459
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$7;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELAY_HIDE_BARS:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->notifyObservers(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 463
    :goto_0
    return-void

    .line 460
    :catch_0
    move-exception v0

    .line 461
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
