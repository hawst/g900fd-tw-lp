.class public Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "StartIdleProcessCmd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd$1;,
        Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd$IdleProcess;
    }
.end annotation


# instance fields
.field private mActivity:Lcom/sec/android/gallery3d/app/GalleryActivity;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 38
    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;)Lcom/sec/android/gallery3d/app/GalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;->mActivity:Lcom/sec/android/gallery3d/app/GalleryActivity;

    return-object v0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 2
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 22
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    .line 23
    .local v0, "params":[Ljava/lang/Object;
    const/4 v1, 0x0

    aget-object v1, v0, v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryActivity;

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;->mActivity:Lcom/sec/android/gallery3d/app/GalleryActivity;

    .line 25
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;->startIdleProcess()V

    .line 26
    return-void
.end method

.method public startIdleProcess()V
    .locals 8

    .prologue
    .line 29
    new-instance v4, Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd$IdleProcess;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd$IdleProcess;-><init>(Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd$1;)V

    .line 31
    .local v4, "idleProcess":Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd$IdleProcess;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 32
    .local v0, "currentTime":J
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;->mActivity:Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getActivityCreateTime()J

    move-result-wide v6

    sub-long v2, v0, v6

    .line 33
    .local v2, "elapsedTime":J
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;->mActivity:Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/app/StateManager;->setStartIdleState(Z)V

    .line 34
    const-string v5, "Gallery_Performance"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Elapsed time to complete decoding = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/StartIdleProcessCmd;->mActivity:Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getGlRootView()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/sec/android/gallery3d/glcore/GlRootView;->addOnGLIdleListener(Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;)V

    .line 36
    return-void
.end method
