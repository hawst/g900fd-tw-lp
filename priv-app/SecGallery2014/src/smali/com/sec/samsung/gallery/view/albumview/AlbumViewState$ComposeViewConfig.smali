.class Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;
.super Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;
.source "AlbumViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ComposeViewConfig"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2156
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;-><init>()V

    .line 2157
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->mUsePenSelectInPickMode:Z

    .line 2158
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->mUseEnlargeAnim:Z

    .line 2159
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->mUseGroupSelect:Z

    .line 2160
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->mUseGroupTitle:Z

    .line 2161
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->mUseItemSelect:Z

    .line 2162
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->mHideIconMinLevel:Z

    .line 2163
    iput v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->mInitialLevel:I

    .line 2164
    iput v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->mMinLevel:I

    .line 2165
    iput v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->mMaxLevel:I

    .line 2166
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->mTopGroupTitle:Z

    .line 2167
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const-class v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;

    aput-object v1, v0, v2

    const-class v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->mPosCtrl:[Ljava/lang/Object;

    .line 2168
    return-void
.end method

.method static synthetic access$5300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;ZI)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;
    .param p1, "x1"    # Z
    .param p2, "x2"    # I

    .prologue
    .line 2154
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->setInitalLevel(ZI)V

    return-void
.end method

.method private setInitalLevel(ZI)V
    .locals 4
    .param p1, "isEasyMode"    # Z
    .param p2, "level"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2171
    if-eqz p1, :cond_0

    .line 2172
    iput v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->mInitialLevel:I

    .line 2173
    iput v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->mMinLevel:I

    .line 2174
    iput v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->mMaxLevel:I

    .line 2180
    :goto_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->mMinLevel:I

    iget v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->mMaxLevel:I

    if-eq v2, v3, :cond_1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->mUseLayoutChange:Z

    .line 2181
    return-void

    .line 2176
    :cond_0
    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->mInitialLevel:I

    .line 2177
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$6400(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0078

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->mMinLevel:I

    .line 2178
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$6500(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0079

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->mMaxLevel:I

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2180
    goto :goto_1
.end method
