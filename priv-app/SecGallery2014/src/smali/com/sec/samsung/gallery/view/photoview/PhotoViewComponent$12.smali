.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$12;
.super Ljava/lang/Object;
.source "PhotoViewComponent.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/OnProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->selectAllProcessTask(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V
    .locals 0

    .prologue
    .line 2081
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$12;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 1
    .param p1, "object"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 2100
    const/4 v0, 0x0

    return v0
.end method

.method public handleOperation(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2105
    .local p1, "objectList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public onCompleted(Z)V
    .locals 5
    .param p1, "result"    # Z

    .prologue
    .line 2089
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$12;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 2090
    .local v0, "count":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$12;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$500(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$12;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$12;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v4, v4, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    invoke-virtual {v2, v0, v3, v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isAvailableCount(IZZ)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateDoneButton(Z)V

    .line 2091
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$12;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$500(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 2092
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$12;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->refreshCheckState()V

    .line 2093
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$12;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$12;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget v2, v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->updateFolderItem(I)V

    .line 2095
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$12;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$12;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    # invokes: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateActionBarSelectionMode(Lcom/sec/android/gallery3d/data/MediaSet;)V
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2500(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 2096
    return-void
.end method

.method public onProgress(II)V
    .locals 0
    .param p1, "progress"    # I
    .param p2, "total"    # I

    .prologue
    .line 2084
    return-void
.end method
