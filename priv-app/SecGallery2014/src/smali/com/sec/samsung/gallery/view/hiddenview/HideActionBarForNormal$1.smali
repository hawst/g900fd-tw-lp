.class Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal$1;
.super Ljava/lang/Object;
.source "HideActionBarForNormal.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private resetCustumView()V
    .locals 3

    .prologue
    .line 47
    const/16 v1, 0x10

    .line 48
    .local v1, "flags":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->access$600(Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActionBar;->getDisplayOptions()I

    move-result v2

    and-int/lit8 v0, v2, -0x11

    .line 49
    .local v0, "change":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->access$700(Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 50
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 30
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 31
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->access$000(Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030002

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 33
    .local v0, "customView":Landroid/view/View;
    const v2, 0x7f0f000f

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 34
    .local v1, "ll":Landroid/widget/LinearLayout;
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setShowDividers(I)V

    .line 35
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->access$100(Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 42
    .end local v0    # "customView":Landroid/view/View;
    .end local v1    # "ll":Landroid/widget/LinearLayout;
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->access$400(Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 43
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->access$500(Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActionBar;->removeAllTabs()V

    .line 44
    return-void

    .line 37
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->access$200(Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 38
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->access$300(Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 39
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal$1;->resetCustumView()V

    goto :goto_0
.end method
