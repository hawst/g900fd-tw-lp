.class public Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;
.super Ljava/lang/Object;
.source "SPenSDKUtils.java"


# static fields
.field private static final MAX_DISPLAY_PIXEL_COUNT:I = 0x7e900

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 200
    const-class v0, Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decodeImageFile(Ljava/lang/String;Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "strImagePath"    # Ljava/lang/String;
    .param p1, "checkOrientation"    # Z

    .prologue
    .line 186
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;->decodeImageFile(Ljava/lang/String;ZI)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static decodeImageFile(Ljava/lang/String;ZI)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "strImagePath"    # Ljava/lang/String;
    .param p1, "checkOrientation"    # Z
    .param p2, "maxPixel"    # I

    .prologue
    .line 190
    if-nez p1, :cond_0

    .line 191
    invoke-static {p0, p2}, Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;->loadSmallBitmap(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 195
    :goto_0
    return-object v2

    .line 193
    :cond_0
    invoke-static {p0, p2}, Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;->loadSmallBitmap(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 194
    .local v0, "bm":Landroid/graphics/Bitmap;
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getExifOrientation(Ljava/lang/String;)I

    move-result v1

    .line 195
    .local v1, "degree":I
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;->getRotatedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0
.end method

.method public static fileNameRemoveExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 335
    if-nez p0, :cond_1

    .line 336
    const/4 p0, 0x0

    .line 344
    .end local p0    # "fileName":Ljava/lang/String;
    .local v0, "idx":I
    :cond_0
    :goto_0
    return-object p0

    .line 338
    .end local v0    # "idx":I
    .restart local p0    # "fileName":Ljava/lang/String;
    :cond_1
    const-string v1, "."

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 340
    .restart local v0    # "idx":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 344
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static getBitmapSize(Ljava/lang/String;)Landroid/graphics/BitmapFactory$Options;
    .locals 2
    .param p0, "strImagePath"    # Ljava/lang/String;

    .prologue
    .line 240
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 241
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 243
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 245
    return-object v0
.end method

.method public static getBitmapSize(Ljava/lang/String;Z)Landroid/graphics/BitmapFactory$Options;
    .locals 4
    .param p0, "strImagePath"    # Ljava/lang/String;
    .param p1, "checkOrientation"    # Z

    .prologue
    .line 253
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 254
    .local v1, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x1

    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 256
    invoke-static {p0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 258
    if-eqz p1, :cond_1

    .line 259
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getExifOrientation(Ljava/lang/String;)I

    move-result v0

    .line 260
    .local v0, "degree":I
    const/16 v3, 0x5a

    if-eq v0, v3, :cond_0

    const/16 v3, 0x10e

    if-ne v0, v3, :cond_1

    .line 261
    :cond_0
    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 262
    .local v2, "temp":I
    iget v3, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput v3, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 263
    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 267
    .end local v0    # "degree":I
    .end local v2    # "temp":I
    :cond_1
    return-object v1
.end method

.method public static getFileExtension(Ljava/io/File;)Ljava/lang/String;
    .locals 3
    .param p0, "f"    # Ljava/io/File;

    .prologue
    .line 327
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 328
    .local v0, "idx":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 329
    const-string v1, ""

    .line 331
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getImageSize(Ljava/lang/String;)Landroid/graphics/Rect;
    .locals 5
    .param p0, "strImagePath"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 445
    if-eqz p0, :cond_0

    .line 446
    const/4 v1, 0x1

    invoke-static {p0, v1}, Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;->getBitmapSize(Ljava/lang/String;Z)Landroid/graphics/BitmapFactory$Options;

    move-result-object v0

    .line 447
    .local v0, "opts":Landroid/graphics/BitmapFactory$Options;
    new-instance v1, Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 449
    .end local v0    # "opts":Landroid/graphics/BitmapFactory$Options;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getRealPathFromURI(Landroid/app/Activity;Landroid/net/Uri;)Ljava/lang/String;
    .locals 13
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "contentUri"    # Landroid/net/Uri;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 38
    sget-object v10, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 40
    .local v10, "releaseNumber":Ljava/lang/String;
    if-eqz v10, :cond_5

    .line 42
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {v10, v5}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x34

    if-ne v1, v2, :cond_2

    .line 43
    new-array v3, v6, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v3, v5

    .line 44
    .local v3, "proj":[Ljava/lang/String;
    const-string v11, ""

    .line 45
    .local v11, "strFileName":Ljava/lang/String;
    new-instance v0, Landroid/content/CursorLoader;

    move-object v1, p0

    move-object v2, p1

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .local v0, "cursorLoader":Landroid/content/CursorLoader;
    if-eqz v0, :cond_1

    .line 48
    invoke-virtual {v0}, Landroid/content/CursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v8

    .line 49
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    .line 51
    const-string v1, "_data"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 52
    .local v7, "column_index":I
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 53
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 54
    invoke-interface {v8, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 56
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .end local v7    # "column_index":I
    .end local v8    # "cursor":Landroid/database/Cursor;
    :cond_1
    move-object v12, v11

    .line 107
    .end local v0    # "cursorLoader":Landroid/content/CursorLoader;
    .end local v11    # "strFileName":Ljava/lang/String;
    .local v12, "strFileName":Ljava/lang/String;
    :goto_0
    return-object v12

    .line 62
    .end local v3    # "proj":[Ljava/lang/String;
    .end local v12    # "strFileName":Ljava/lang/String;
    :cond_2
    const-string v1, "2.3"

    invoke-virtual {v10, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 63
    new-array v3, v6, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v3, v5

    .line 64
    .restart local v3    # "proj":[Ljava/lang/String;
    const-string v11, ""

    .restart local v11    # "strFileName":Ljava/lang/String;
    move-object v1, p0

    move-object v2, p1

    move-object v5, v4

    move-object v6, v4

    .line 65
    invoke-virtual/range {v1 .. v6}, Landroid/app/Activity;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 66
    .restart local v8    # "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_4

    .line 68
    const-string v1, "_data"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 70
    .restart local v7    # "column_index":I
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 71
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_3

    .line 72
    invoke-interface {v8, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 74
    :cond_3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .end local v7    # "column_index":I
    :cond_4
    move-object v12, v11

    .line 76
    .end local v11    # "strFileName":Ljava/lang/String;
    .restart local v12    # "strFileName":Ljava/lang/String;
    goto :goto_0

    .line 84
    .end local v3    # "proj":[Ljava/lang/String;
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v12    # "strFileName":Ljava/lang/String;
    :cond_5
    new-array v3, v6, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v3, v5

    .line 85
    .restart local v3    # "proj":[Ljava/lang/String;
    const-string v11, ""

    .restart local v11    # "strFileName":Ljava/lang/String;
    move-object v1, p0

    move-object v2, p1

    move-object v5, v4

    move-object v6, v4

    .line 86
    invoke-virtual/range {v1 .. v6}, Landroid/app/Activity;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 88
    .restart local v8    # "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_7

    .line 89
    :try_start_0
    const-string v1, "_data"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 92
    .restart local v7    # "column_index":I
    invoke-virtual {p0, v8}, Landroid/app/Activity;->startManagingCursor(Landroid/database/Cursor;)V

    .line 94
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 95
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_6

    .line 96
    invoke-interface {v8, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 99
    :cond_6
    invoke-virtual {p0, v8}, Landroid/app/Activity;->stopManagingCursor(Landroid/database/Cursor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    .end local v7    # "column_index":I
    :cond_7
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :goto_1
    move-object v12, v11

    .line 107
    .end local v11    # "strFileName":Ljava/lang/String;
    .restart local v12    # "strFileName":Ljava/lang/String;
    goto :goto_0

    .line 101
    .end local v12    # "strFileName":Ljava/lang/String;
    .restart local v11    # "strFileName":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 102
    .local v9, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1
.end method

.method public static getRotatedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "degrees"    # I

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 310
    if-eqz p1, :cond_0

    if-eqz p0, :cond_0

    .line 311
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 312
    .local v5, "m":Landroid/graphics/Matrix;
    int-to-float v0, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-virtual {v5, v0, v1, v2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 314
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 315
    .local v7, "b2":Landroid/graphics/Bitmap;
    if-eq p0, v7, :cond_0

    .line 316
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 317
    move-object p0, v7

    .line 323
    .end local v5    # "m":Landroid/graphics/Matrix;
    .end local v7    # "b2":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-object p0

    .line 320
    .restart local v5    # "m":Landroid/graphics/Matrix;
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static getSafeResizingBitmap(Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "strImagePath"    # Ljava/lang/String;
    .param p1, "nMaxResizedWidth"    # I
    .param p2, "nMaxResizedHeight"    # I
    .param p3, "checkOrientation"    # Z

    .prologue
    const/4 v6, 0x0

    .line 147
    invoke-static {p0}, Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;->getBitmapSize(Ljava/lang/String;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v2

    .line 149
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    if-nez v2, :cond_1

    .line 150
    const/4 v3, 0x0

    .line 182
    :cond_0
    :goto_0
    return-object v3

    .line 156
    :cond_1
    const/4 v0, 0x0

    .line 157
    .local v0, "degree":I
    if-eqz p3, :cond_2

    .line 158
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getExifOrientation(Ljava/lang/String;)I

    move-result v0

    .line 161
    :cond_2
    const/16 v4, 0x5a

    if-eq v0, v4, :cond_3

    const/16 v4, 0x10e

    if-ne v0, v4, :cond_4

    .line 162
    :cond_3
    iget v4, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iget v5, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v4, v5, p1, p2}, Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;->getSafeResizingSampleSize(IIII)I

    move-result v1

    .line 172
    .local v1, "nSampleSize":I
    :goto_1
    iput-boolean v6, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 173
    iput v1, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 174
    iput-boolean v6, v2, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 175
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v4, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 176
    const/4 v4, 0x1

    iput-boolean v4, v2, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 178
    invoke-static {p0, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 179
    .local v3, "photo":Landroid/graphics/Bitmap;
    if-eqz p3, :cond_0

    .line 180
    invoke-static {v3, v0}, Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;->getRotatedBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v3

    goto :goto_0

    .line 165
    .end local v1    # "nSampleSize":I
    .end local v3    # "photo":Landroid/graphics/Bitmap;
    :cond_4
    iget v4, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v5, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v4, v5, p1, p2}, Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;->getSafeResizingSampleSize(IIII)I

    move-result v1

    .restart local v1    # "nSampleSize":I
    goto :goto_1
.end method

.method public static getSafeResizingSampleSize(IIII)I
    .locals 6
    .param p0, "nOrgWidth"    # I
    .param p1, "nOrgHeight"    # I
    .param p2, "nMaxWidth"    # I
    .param p3, "nMaxHeight"    # I

    .prologue
    .line 288
    const/4 v3, 0x1

    .line 290
    .local v3, "size":I
    const/4 v1, 0x0

    .line 291
    .local v1, "fWidthScale":F
    const/4 v0, 0x0

    .line 293
    .local v0, "fHeightScale":F
    if-gt p0, p2, :cond_0

    if-le p1, p3, :cond_3

    .line 295
    :cond_0
    if-le p0, p2, :cond_1

    .line 296
    int-to-float v4, p0

    int-to-float v5, p2

    div-float v1, v4, v5

    .line 297
    :cond_1
    if-le p1, p3, :cond_2

    .line 298
    int-to-float v4, p1

    int-to-float v5, p3

    div-float v0, v4, v5

    .line 300
    :cond_2
    cmpl-float v4, v1, v0

    if-ltz v4, :cond_4

    move v2, v1

    .line 303
    .local v2, "fsize":F
    :goto_0
    float-to-int v3, v2

    .line 306
    .end local v2    # "fsize":F
    :cond_3
    return v3

    .line 301
    :cond_4
    move v2, v0

    .restart local v2    # "fsize":F
    goto :goto_0
.end method

.method public static getUniqueFilename(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "folder"    # Ljava/io/File;
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "ext"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 364
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 365
    :cond_0
    const/4 v1, 0x0

    .line 381
    :cond_1
    return-object v1

    .line 370
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x14

    if-le v4, v5, :cond_3

    .line 371
    const/16 v4, 0x13

    invoke-virtual {p1, v8, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 374
    :cond_3
    invoke-static {p1}, Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;->stringCheck(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 376
    const/4 v2, 0x1

    .line 378
    .local v2, "i":I
    :goto_0
    const-string v4, "%s_%02d.%s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p1, v5, v8

    const/4 v6, 0x1

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "i":I
    .local v3, "i":I
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    aput-object p2, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 379
    .local v1, "curFileName":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 380
    .local v0, "curFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    move v2, v3

    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_0
.end method

.method public static isValidImagePath(Ljava/lang/String;)Z
    .locals 4
    .param p0, "strImagePath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 115
    if-nez p0, :cond_0

    .line 122
    :goto_0
    return v2

    .line 118
    :cond_0
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 119
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 120
    invoke-static {p0, v0}, Lcom/sec/samsung/gallery/decoder/DecoderInterface;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 122
    iget-object v3, v0, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    if-eqz v3, :cond_1

    :goto_1
    move v2, v1

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private static loadSmallBitmap(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "maxPixel"    # I

    .prologue
    const/4 v4, 0x0

    .line 203
    const/4 v0, 0x0

    .line 204
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 205
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v4, v2, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 206
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v3, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 207
    iput-boolean v4, v2, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 208
    const/4 v3, 0x1

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 210
    :try_start_0
    invoke-static {p0, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 211
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 212
    iget v4, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v5, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    const/4 v6, -0x1

    if-lez p1, :cond_0

    mul-int v3, p1, p1

    :goto_0
    invoke-static {v4, v5, v6, v3}, Lcom/sec/android/gallery3d/common/BitmapUtils;->computeSampleSize(IIII)I

    move-result v3

    iput v3, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 216
    const/4 v3, 0x0

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 217
    invoke-static {p0, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 223
    :goto_1
    return-object v0

    .line 212
    :cond_0
    const v3, 0x7e900

    goto :goto_0

    .line 218
    :catch_0
    move-exception v1

    .line 219
    .local v1, "oom":Ljava/lang/OutOfMemoryError;
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 220
    sget-object v3, Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;->TAG:Ljava/lang/String;

    const-string v4, "OOM"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public static readBytedata(Ljava/lang/String;)[B
    .locals 10
    .param p0, "aFilename"    # Ljava/lang/String;

    .prologue
    .line 385
    const/4 v6, 0x0

    .line 387
    .local v6, "imgBuffer":[B
    const/4 v4, 0x0

    .line 389
    .local v4, "fileInputStream":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 390
    .local v3, "file":Ljava/io/File;
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    .end local v4    # "fileInputStream":Ljava/io/FileInputStream;
    .local v5, "fileInputStream":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v8

    long-to-int v0, v8

    .line 392
    .local v0, "byteSize":I
    new-array v6, v0, [B

    .line 393
    invoke-virtual {v5, v6}, Ljava/io/FileInputStream;->read([B)I

    move-result v7

    const/4 v8, -0x1

    if-ne v7, v8, :cond_0

    .line 394
    sget-object v7, Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;->TAG:Ljava/lang/String;

    const-string v8, "readBytedata :: read(imgBuffer) = -1 "

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    :cond_0
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 402
    if-eqz v5, :cond_3

    .line 404
    :try_start_2
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v4, v5

    .line 411
    .end local v0    # "byteSize":I
    .end local v3    # "file":Ljava/io/File;
    .end local v5    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "fileInputStream":Ljava/io/FileInputStream;
    :cond_1
    :goto_0
    return-object v6

    .line 405
    .end local v4    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v0    # "byteSize":I
    .restart local v3    # "file":Ljava/io/File;
    .restart local v5    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_0
    move-exception v1

    .line 406
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    move-object v4, v5

    .line 407
    .end local v5    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_0

    .line 397
    .end local v0    # "byteSize":I
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "file":Ljava/io/File;
    :catch_1
    move-exception v1

    .line 398
    .local v1, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 402
    if-eqz v4, :cond_1

    .line 404
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 405
    :catch_2
    move-exception v1

    .line 406
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 399
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 400
    .local v2, "e2":Ljava/io/IOException;
    :goto_2
    :try_start_5
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 402
    if-eqz v4, :cond_1

    .line 404
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 405
    :catch_4
    move-exception v1

    .line 406
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 402
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "e2":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_3
    if-eqz v4, :cond_2

    .line 404
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 407
    :cond_2
    :goto_4
    throw v7

    .line 405
    :catch_5
    move-exception v1

    .line 406
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 402
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v5    # "fileInputStream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v7

    move-object v4, v5

    .end local v5    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_3

    .line 399
    .end local v4    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v5    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_6
    move-exception v2

    move-object v4, v5

    .end local v5    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_2

    .line 397
    .end local v4    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v5    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_7
    move-exception v1

    move-object v4, v5

    .end local v5    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_1

    .end local v4    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v0    # "byteSize":I
    .restart local v5    # "fileInputStream":Ljava/io/FileInputStream;
    :cond_3
    move-object v4, v5

    .end local v5    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_0
.end method

.method public static stringCheck(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 348
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 350
    .local v3, "strbuilder":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 351
    .local v2, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_2

    .line 352
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 353
    .local v0, "curChar":C
    const/16 v4, 0x5c

    if-eq v0, v4, :cond_0

    const/16 v4, 0x2f

    if-eq v0, v4, :cond_0

    const/16 v4, 0x3a

    if-eq v0, v4, :cond_0

    const/16 v4, 0x2a

    if-eq v0, v4, :cond_0

    const/16 v4, 0x3f

    if-eq v0, v4, :cond_0

    const/16 v4, 0x22

    if-eq v0, v4, :cond_0

    const/16 v4, 0x3c

    if-eq v0, v4, :cond_0

    const/16 v4, 0x3e

    if-eq v0, v4, :cond_0

    const/16 v4, 0x7c

    if-ne v0, v4, :cond_1

    .line 356
    :cond_0
    const/16 v4, 0x5f

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 351
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 358
    :cond_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 360
    .end local v0    # "curChar":C
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static writeBytedata(Ljava/lang/String;[B)Z
    .locals 7
    .param p0, "aFilename"    # Ljava/lang/String;
    .param p1, "imgBuffer"    # [B

    .prologue
    .line 416
    const/4 v3, 0x0

    .line 417
    .local v3, "fileOutputStream":Ljava/io/FileOutputStream;
    const/4 v5, 0x1

    .line 420
    .local v5, "result":Z
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 421
    .local v2, "file":Ljava/io/File;
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 422
    .end local v3    # "fileOutputStream":Ljava/io/FileOutputStream;
    .local v4, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v4, p1}, Ljava/io/FileOutputStream;->write([B)V

    .line 423
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 431
    if-eqz v4, :cond_2

    .line 433
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v3, v4

    .line 441
    .end local v2    # "file":Ljava/io/File;
    .end local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v3    # "fileOutputStream":Ljava/io/FileOutputStream;
    :cond_0
    :goto_0
    return v5

    .line 434
    .end local v3    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 435
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 436
    const/4 v5, 0x0

    move-object v3, v4

    .line 437
    .end local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v3    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 424
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "file":Ljava/io/File;
    :catch_1
    move-exception v0

    .line 425
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 426
    const/4 v5, 0x0

    .line 431
    if-eqz v3, :cond_0

    .line 433
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 434
    :catch_2
    move-exception v0

    .line 435
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 436
    const/4 v5, 0x0

    .line 437
    goto :goto_0

    .line 427
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 428
    .local v1, "e2":Ljava/io/IOException;
    :goto_2
    :try_start_5
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 429
    const/4 v5, 0x0

    .line 431
    if-eqz v3, :cond_0

    .line 433
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 434
    :catch_4
    move-exception v0

    .line 435
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 436
    const/4 v5, 0x0

    .line 437
    goto :goto_0

    .line 431
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "e2":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_3
    if-eqz v3, :cond_1

    .line 433
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 437
    :cond_1
    :goto_4
    throw v6

    .line 434
    :catch_5
    move-exception v0

    .line 435
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 436
    const/4 v5, 0x0

    goto :goto_4

    .line 431
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v3    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 427
    .end local v3    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v1

    move-object v3, v4

    .end local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v3    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 424
    .end local v3    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v0

    move-object v3, v4

    .end local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v3    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_1

    .end local v3    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    :cond_2
    move-object v3, v4

    .end local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v3    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_0
.end method
