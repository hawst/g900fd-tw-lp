.class Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$2;
.super Ljava/lang/Object;
.source "GlComposeAlbumView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 5
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mStartIndex:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->access$300(Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragIndex:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->access$400(Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 89
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mThis:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->getParam()I

    move-result v3

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;->onStatusChange(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;III)V

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    # invokes: Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->resetReorderData()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->access$500(Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;)V

    .line 92
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseReOrderAlbumsSlectionMode:Z

    if-eqz v0, :cond_1

    .line 93
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mOnExtendListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnExtendListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnExtendListener;->onExtendRequest(I)V

    .line 95
    :cond_1
    return-void
.end method

.method public onAnimationRepeat(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 0
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 99
    return-void
.end method

.method public onAnimationStart(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 0
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 84
    return-void
.end method
