.class public Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;
.source "HideActionBarForEdit.java"


# instance fields
.field private mCurrentViewState:Lcom/sec/android/gallery3d/app/ActivityState;

.field private mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

.field private mMenu:Landroid/view/Menu;

.field protected mQuantitySelectedAlbum:I

.field protected mQuantitySelectedItem:I


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/view/utils/EditModeHelper;)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "editModeHelper"    # Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .prologue
    const/4 v0, 0x1

    .line 35
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 29
    iput v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mQuantitySelectedAlbum:I

    .line 30
    iput v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mQuantitySelectedItem:I

    .line 36
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 37
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mCurrentViewState:Lcom/sec/android/gallery3d/app/ActivityState;

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;)Landroid/view/Menu;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mMenu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->setSelectAllButtonTitle(ILjava/lang/String;)V

    return-void
.end method

.method private isLastShareSupported(Landroid/view/Menu;)Z
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 192
    const/4 v2, 0x0

    .line 193
    .local v2, "showLastShare":Z
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-wide/16 v4, 0x4

    invoke-static {v3, p1, v4, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkLastShareAppSupport(Landroid/content/Context;Landroid/view/Menu;J)J

    move-result-wide v0

    .line 195
    .local v0, "lastShareSupported":J
    const-wide v4, 0x80000000000L

    and-long/2addr v4, v0

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    .line 196
    :goto_0
    return v2

    .line 195
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public isValidSelection()Z
    .locals 7

    .prologue
    .line 177
    const/4 v0, 0x1

    .line 179
    .local v0, "isValid":Z
    iget v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mQuantitySelectedAlbum:I

    if-nez v2, :cond_1

    .line 180
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0e0113

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 181
    const/4 v0, 0x0

    .line 189
    :cond_0
    :goto_0
    return v0

    .line 182
    :cond_1
    iget v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mQuantitySelectedItem:I

    sget v3, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    if-le v2, v3, :cond_0

    .line 183
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0114

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget v6, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 185
    .local v1, "text":Ljava/lang/CharSequence;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 186
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 71
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 72
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f120025

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 73
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 150
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 152
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->isValidSelection()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->chooseShareDialog()V

    goto :goto_0

    .line 157
    :sswitch_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v0, v2, v1, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->hideAlbums(ZZZ)V

    goto :goto_0

    .line 161
    :sswitch_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v0, v2, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showDeleteDialog(ZZ)V

    goto :goto_0

    .line 164
    :sswitch_3
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->isValidSelection()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAdvancedSoundSceneShare:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isASoundSceneFileSelected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/golf/GolfMgr;->isGolfFileSelected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/Play3DTourCmd;->isSelected3DTour(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 167
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v0, v1, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showImageVideoConversionShareDialog(ZZ)V

    goto :goto_0

    .line 169
    :cond_3
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    sget v1, Lcom/sec/samsung/gallery/core/Event;->EVENT_LAST_SHARE_APP:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 150
    :sswitch_data_0
    .sparse-switch
        0x7f0f026c -> :sswitch_0
        0x7f0f026d -> :sswitch_3
        0x7f0f026e -> :sswitch_2
        0x7f0f027b -> :sswitch_2
        0x7f0f02a2 -> :sswitch_1
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 14
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v9, 0x1

    const v13, 0x7f0f026d

    const v12, 0x7f0f026c

    const v11, 0x7f0f02a2

    const/4 v5, 0x0

    .line 77
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mMenu:Landroid/view/Menu;

    .line 78
    const v8, 0x7f0f02c5

    invoke-interface {p1, v8, v9}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 81
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mCurrentViewState:Lcom/sec/android/gallery3d/app/ActivityState;

    if-eqz v8, :cond_7

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mCurrentViewState:Lcom/sec/android/gallery3d/app/ActivityState;

    instance-of v8, v8, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    if-eqz v8, :cond_7

    .line 82
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->getSupportedOperationForSelectedAlbumItem()J

    move-result-wide v6

    .line 87
    .local v6, "supportOperation":J
    :goto_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v8, p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkLastShareAppSupport(Landroid/content/Context;Landroid/view/Menu;J)J

    move-result-wide v6

    .line 88
    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->updateMenuOperation(Landroid/view/Menu;J)V

    .line 90
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->getSelectedItemsCount()I

    move-result v8

    if-nez v8, :cond_8

    .line 91
    invoke-static {p1, v12, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 92
    invoke-static {p1, v11, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 93
    const v8, 0x7f0f026e

    invoke-static {p1, v8, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 94
    invoke-static {p1, v13, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 101
    :goto_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->orientation:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_a

    .line 102
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v8

    if-nez v8, :cond_0

    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-nez v8, :cond_0

    .line 103
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mMenu:Landroid/view/Menu;

    const/4 v9, 0x2

    invoke-static {v8, v12, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 106
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0002

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    if-eqz v8, :cond_9

    const/4 v4, 0x4

    .line 109
    .local v4, "showText":I
    :goto_2
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v9, 0x7f0f026e

    or-int/lit8 v10, v4, 0x2

    invoke-static {v8, v9, v10}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 111
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mMenu:Landroid/view/Menu;

    or-int/lit8 v9, v4, 0x2

    invoke-static {v8, v11, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 127
    .end local v4    # "showText":I
    :cond_0
    :goto_3
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v3

    .line 128
    .local v3, "selectedItems":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 129
    .local v2, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v8, v2, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v8, :cond_1

    move-object v1, v2

    .line 130
    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 131
    .local v1, "m":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 132
    invoke-static {p1, v12, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 133
    invoke-static {p1, v13, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 139
    .end local v1    # "m":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v2    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAdvancedSoundSceneShare:Z

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v8}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isASoundSceneFileSelected(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_4

    :cond_3
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v8}, Lcom/sec/android/gallery3d/golf/GolfMgr;->isGolfFileSelected(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 141
    :cond_4
    invoke-static {p1, v13, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 143
    :cond_5
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLastShare:Z

    if-nez v8, :cond_6

    .line 144
    invoke-static {p1, v13, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 146
    :cond_6
    const v8, 0x7f0f027b

    invoke-static {p1, v8, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 147
    return-void

    .line 84
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v3    # "selectedItems":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    .end local v6    # "supportOperation":J
    :cond_7
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->getSupportedOperationForSelectedItem()J

    move-result-wide v6

    .restart local v6    # "supportOperation":J
    goto/16 :goto_0

    .line 97
    :cond_8
    invoke-static {p1, v11, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 98
    const v8, 0x7f0f026e

    invoke-static {p1, v8, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_1

    :cond_9
    move v4, v5

    .line 106
    goto :goto_2

    .line 115
    :cond_a
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v8

    if-eqz v8, :cond_b

    .line 116
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mMenu:Landroid/view/Menu;

    invoke-static {v8, v11, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 117
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mMenu:Landroid/view/Menu;

    invoke-static {v8, v12, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 118
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v9, 0x7f020032

    invoke-static {v8, v11, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIcon(Landroid/view/Menu;II)V

    .line 119
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v9, 0x7f020030

    invoke-static {v8, v12, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIcon(Landroid/view/Menu;II)V

    goto/16 :goto_3

    .line 120
    :cond_b
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v8

    if-nez v8, :cond_0

    .line 121
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->isLastShareSupported(Landroid/view/Menu;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 122
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mMenu:Landroid/view/Menu;

    invoke-static {v8, v11, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    goto/16 :goto_3
.end method

.method protected setSelectedItemCount(I)V
    .locals 0
    .param p1, "numberOfItemsSelected"    # I

    .prologue
    .line 66
    iput p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mQuantitySelectedItem:I

    .line 67
    return-void
.end method

.method public setTitle(I)V
    .locals 2
    .param p1, "numberOfItemsSelected"    # I

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit$1;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 62
    return-void
.end method
