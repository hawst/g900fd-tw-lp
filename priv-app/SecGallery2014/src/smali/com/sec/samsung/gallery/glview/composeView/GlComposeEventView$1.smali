.class Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView$1;
.super Ljava/lang/Object;
.source "GlComposeEventView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;)V
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Lcom/sec/android/gallery3d/glcore/GlObject;)Z
    .locals 7
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/4 v6, 0x1

    .line 203
    iget-object v3, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    check-cast v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v1, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 206
    .local v1, "index":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mClickEnabled:Z

    if-nez v3, :cond_1

    .line 222
    :cond_0
    :goto_0
    return v6

    .line 207
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    if-eqz v3, :cond_0

    .line 211
    shr-int/lit8 v0, v1, 0x10

    .line 212
    .local v0, "albumIndex":I
    const/4 v2, -0x1

    .line 215
    .local v2, "photoIndex":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mMode:I

    if-nez v3, :cond_2

    .line 216
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v3

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 221
    :goto_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mThis:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v5

    invoke-interface {v3, v4, v5, v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;->onItemClick(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/android/gallery3d/glcore/GlView;II)Z

    goto :goto_0

    .line 218
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    goto :goto_1
.end method
