.class Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;
.super Ljava/lang/Object;
.source "DetailActionBarForExpansionItem.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13
    .param p1, "checkBoxView"    # Landroid/view/View;

    .prologue
    const/16 v9, 0x3e8

    const v10, 0x7f0e0117

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 83
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSourceMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    .line 84
    .local v3, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$100(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getExpMediaItem()Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v2

    .line 87
    .local v2, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$200(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 88
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$300(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v7

    invoke-virtual {v7, v3, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 89
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 132
    .end local p1    # "checkBoxView":Landroid/view/View;
    :goto_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->getMinCount()I
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$2700(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)I

    move-result v4

    .line 133
    .local v4, "minCount":I
    if-le v4, v12, :cond_0

    .line 134
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$2800(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v5

    .line 135
    .local v5, "pickCount":I
    if-ge v5, v4, :cond_0

    .line 136
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$2900(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0e0116

    new-array v9, v12, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 137
    .local v6, "text":Ljava/lang/CharSequence;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$3000(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v7

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 140
    .end local v5    # "pickCount":I
    .end local v6    # "text":Ljava/lang/CharSequence;
    :cond_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->updateSelectionButton(Lcom/sec/android/gallery3d/data/MediaSet;Z)V
    invoke-static {v7, v3, v12}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;Lcom/sec/android/gallery3d/data/MediaSet;Z)V

    .line 141
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v8

    sget v9, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELAY_HIDE_BARS:I

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->notifyObservers(Ljava/lang/Object;)V

    .line 142
    .end local v4    # "minCount":I
    :goto_1
    return-void

    .line 93
    .restart local p1    # "checkBoxView":Landroid/view/View;
    :cond_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->isMultiPickMode()Z
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$500(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 94
    instance-of v7, v2, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v7, :cond_3

    move-object v0, v2

    .line 95
    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 96
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$600(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v7

    invoke-static {v7, v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isAvailableDrm(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 97
    :cond_2
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$700(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v7

    const v8, 0x7f0e0107

    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 98
    check-cast p1, Landroid/widget/CheckBox;

    .end local p1    # "checkBoxView":Landroid/view/View;
    invoke-virtual {p1, v11}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    .line 103
    .end local v0    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local p1    # "checkBoxView":Landroid/view/View;
    :cond_3
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->maxPickCount:I
    invoke-static {v7, v9}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$802(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;I)I

    .line 104
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$900(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 105
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$900(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v8

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->totalSelectedCount:I
    invoke-static {v7, v8}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$1002(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;I)I

    .line 107
    :cond_4
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->maxPickCount:I
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$800(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->totalSelectedCount:I
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)I

    move-result v8

    sub-int v1, v7, v8

    .line 108
    .local v1, "maxAddCount":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->isMultiPickMode()Z
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$1100(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Z

    move-result v7

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->updatePopupMenuButtonState()Z
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$1200(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Z

    move-result v7

    if-nez v7, :cond_5

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->getMaxCount()I

    move-result v7

    if-lez v7, :cond_5

    .line 109
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$1300(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    new-array v8, v12, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->getMaxCount()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-virtual {v7, v10, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 112
    .restart local v6    # "text":Ljava/lang/CharSequence;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$1400(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v7

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 113
    check-cast p1, Landroid/widget/CheckBox;

    .end local p1    # "checkBoxView":Landroid/view/View;
    invoke-virtual {p1, v11}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    .line 114
    .end local v6    # "text":Ljava/lang/CharSequence;
    .restart local p1    # "checkBoxView":Landroid/view/View;
    :cond_5
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->isMultiPickMode()Z
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$1500(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Z

    move-result v7

    if-eqz v7, :cond_6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->updatePopupMenuButtonState()Z
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$1600(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Z

    move-result v7

    if-nez v7, :cond_6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$1700(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFromGalleryWidget(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_6

    if-gtz v1, :cond_6

    .line 115
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$1800(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    new-array v8, v12, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-virtual {v7, v10, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 118
    .restart local v6    # "text":Ljava/lang/CharSequence;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$1900(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v7

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 119
    check-cast p1, Landroid/widget/CheckBox;

    .end local p1    # "checkBoxView":Landroid/view/View;
    invoke-virtual {p1, v11}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    .line 120
    .end local v6    # "text":Ljava/lang/CharSequence;
    .restart local p1    # "checkBoxView":Landroid/view/View;
    :cond_6
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->isMultiPickMode()Z
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$2000(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Z

    move-result v7

    if-eqz v7, :cond_7

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->updatePopupMenuButtonState()Z
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$2100(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Z

    move-result v7

    if-nez v7, :cond_7

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$2200(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFromInsideGallery(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 121
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$2300(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    new-array v8, v12, [Ljava/lang/Object;

    sget v9, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-virtual {v7, v10, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 124
    .restart local v6    # "text":Ljava/lang/CharSequence;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$2400(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v7

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 125
    check-cast p1, Landroid/widget/CheckBox;

    .end local p1    # "checkBoxView":Landroid/view/View;
    invoke-virtual {p1, v11}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    .line 127
    .end local v6    # "text":Ljava/lang/CharSequence;
    .restart local p1    # "checkBoxView":Landroid/view/View;
    :cond_7
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$2500(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v7

    invoke-virtual {v7, v3, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 128
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;->access$2600(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForExpansionItem;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto/16 :goto_0
.end method
