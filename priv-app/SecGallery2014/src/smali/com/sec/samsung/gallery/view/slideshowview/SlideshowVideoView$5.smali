.class Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$5;
.super Ljava/lang/Object;
.source "SlideshowVideoView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 4
    .param p1, "arg0"    # Landroid/media/MediaPlayer;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I

    .prologue
    .line 210
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$600(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v0

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMultiWindow:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$702(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;Lcom/sec/android/gallery3d/app/GalleryMultiWindow;)Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    .line 211
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMultiWindow:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$700(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMultiWindow:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$700(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->finishSlideshowVideoView()V

    .line 213
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 215
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
