.class public Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlComposeObject.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ThumbTouchAnimation"
.end annotation


# static fields
.field private static final FOCUS_SCALE:F = 1.05f

.field private static final PRESS_SCALE:F = 0.95f


# instance fields
.field private mFromScale:F

.field private mLScale:F

.field private mToScale:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V
    .locals 1

    .prologue
    .line 416
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 421
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->mLScale:F

    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 8
    .param p1, "ratio"    # F

    .prologue
    .line 457
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->mFromScale:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->mToScale:F

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->mFromScale:F

    sub-float/2addr v6, v7

    mul-float/2addr v6, p1

    add-float v1, v5, v6

    .line 458
    .local v1, "dScale":F
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->mLScale:F

    invoke-static {v5, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v5

    if-eqz v5, :cond_2

    .line 459
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v5, v1, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->setScale(FF)V

    .line 460
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/glcore/GlObject;->getAllChildObject()[Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 461
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/glcore/GlObject;->getAllChildObject()[Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/android/gallery3d/glcore/GlObject;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v2, v0, v3

    .line 462
    .local v2, "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    if-nez v2, :cond_0

    .line 461
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 465
    :cond_0
    invoke-virtual {v2, v1, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->setScale(FF)V

    goto :goto_1

    .line 468
    .end local v0    # "arr$":[Lcom/sec/android/gallery3d/glcore/GlObject;
    .end local v2    # "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_1
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->mLScale:F

    .line 470
    :cond_2
    return-void
.end method

.method public cancelAnim()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 438
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->isIdle()Z

    move-result v0

    if-nez v0, :cond_0

    .line 439
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->stop()V

    .line 441
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->mLScale:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    .line 442
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->mLScale:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->mFromScale:F

    .line 443
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->mToScale:F

    .line 444
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->start()V

    .line 446
    :cond_1
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mStartEndEffect:Z

    if-eqz v0, :cond_0

    .line 450
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 452
    :cond_0
    return-void
.end method

.method public startFocusAnim()V
    .locals 1

    .prologue
    .line 431
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->stop()V

    .line 432
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->mLScale:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->mFromScale:F

    .line 433
    const v0, 0x3f866666    # 1.05f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->mToScale:F

    .line 434
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->start()V

    .line 435
    return-void
.end method

.method public startPressAnim()V
    .locals 1

    .prologue
    .line 424
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->stop()V

    .line 425
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->mLScale:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->mFromScale:F

    .line 426
    const v0, 0x3f733333    # 0.95f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->mToScale:F

    .line 427
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbTouchAnimation;->start()V

    .line 428
    return-void
.end method
