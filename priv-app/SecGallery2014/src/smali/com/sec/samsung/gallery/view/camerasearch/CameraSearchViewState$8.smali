.class Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$8;
.super Ljava/lang/Object;
.source "CameraSearchViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)V
    .locals 0

    .prologue
    .line 240
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$8;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V
    .locals 2
    .param p1, "parentView"    # Lcom/sec/samsung/gallery/glview/GlAbsListView;
    .param p2, "position"    # I
    .param p3, "extra"    # I

    .prologue
    .line 242
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$8;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->access$600(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$8;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->access$600(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getKeyWordCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 250
    :cond_0
    :goto_0
    return-void

    .line 244
    :cond_1
    if-nez p3, :cond_2

    const/4 v0, 0x1

    .line 245
    .local v0, "focusedForKeyword":Z
    :goto_1
    if-eqz v0, :cond_3

    .line 246
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$8;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    invoke-virtual {v1, p2}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->setKeywordFocus(I)V

    goto :goto_0

    .line 244
    .end local v0    # "focusedForKeyword":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 248
    .restart local v0    # "focusedForKeyword":Z
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$8;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    invoke-virtual {v1, p2}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->setItemFocus(I)V

    goto :goto_0
.end method
