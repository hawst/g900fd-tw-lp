.class public Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarView;
.source "AlbumActionBarForNormal.java"


# instance fields
.field private mIsNoItemMode:Z

.field private mMenu:Landroid/view/Menu;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v0, 0x1

    .line 38
    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;I)V

    .line 36
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mIsNoItemMode:Z

    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal$1;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 66
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->isReadyToEnableHomeButton()Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method private getOptionMenuId()I
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEmergencyMode(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 316
    const v0, 0x7f120020

    .line 325
    :goto_0
    return v0

    .line 317
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 318
    const v0, 0x7f12000d

    goto :goto_0

    .line 319
    :cond_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 320
    const v0, 0x7f120010

    goto :goto_0

    .line 322
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 323
    const v0, 0x7f12000e

    goto :goto_0

    .line 325
    :cond_3
    const v0, 0x7f12000f

    goto :goto_0
.end method


# virtual methods
.method public onConfChanged()V
    .locals 0

    .prologue
    .line 366
    return-void
.end method

.method public onConfigChanged(Landroid/content/res/Configuration;)V
    .locals 6
    .param p1, "configuration"    # Landroid/content/res/Configuration;

    .prologue
    const v5, 0x7f0f0251

    const v4, 0x7f0203be

    const v3, 0x7f0f028d

    const/4 v2, 0x2

    .line 335
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 336
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mMenu:Landroid/view/Menu;

    invoke-static {v0, v5, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 339
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 340
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mMenu:Landroid/view/Menu;

    invoke-static {v0, v3, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 342
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mMenu:Landroid/view/Menu;

    invoke-static {v0, v3, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIcon(Landroid/view/Menu;II)V

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 347
    :cond_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 348
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mMenu:Landroid/view/Menu;

    invoke-static {v0, v3, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 350
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mMenu:Landroid/view/Menu;

    invoke-static {v0, v3, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIcon(Landroid/view/Menu;II)V

    goto :goto_0

    .line 353
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 354
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mMenu:Landroid/view/Menu;

    invoke-static {v0, v3, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 357
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mMenu:Landroid/view/Menu;

    invoke-static {v0, v5, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    goto :goto_0
.end method

.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mMenu:Landroid/view/Menu;

    .line 71
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 72
    .local v0, "inflater":Landroid/view/MenuInflater;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->getOptionMenuId()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 73
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 74
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 239
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 309
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 310
    :goto_0
    :sswitch_0
    return-void

    .line 241
    :sswitch_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    const-class v3, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0

    .line 244
    :sswitch_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "SHOW_CONTENT_TO_DISPLAY_DIALOG"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 249
    :sswitch_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ABCO"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2, v3, v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 250
    invoke-interface {p1, v5}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 251
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_START_NEW_ALBUM:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 254
    :sswitch_4
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_SLIDESHOW_SETTING:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 257
    :sswitch_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "AMSL"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2, v3, v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 258
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SELECTION_MODE:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 261
    :sswitch_6
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "AMDL"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2, v3, v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 262
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_DELETE_SELECTION_MODE:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 265
    :sswitch_7
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_HIDDEN_ALBUMS:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 268
    :sswitch_8
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v2, :cond_0

    .line 269
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_GALLERY_SEARCH_MODE:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 271
    :cond_0
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SEARCH_MODE:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 276
    :sswitch_9
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v3, v1, v2

    aput-object v4, v1, v5

    .line 277
    .local v1, "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "S_STUDIO"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 280
    .end local v1    # "params":[Ljava/lang/Object;
    :sswitch_a
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/gallery3d/app/StateManager;->setDownloadMode(Z)V

    .line 281
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    const-class v3, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 285
    :sswitch_b
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "START_CAMERA"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 289
    :sswitch_c
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-class v3, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 290
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 293
    .end local v0    # "intent":Landroid/content/Intent;
    :sswitch_d
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->goToHelpGallery(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 297
    :sswitch_e
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_REORDER_ALBUMS:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 306
    :sswitch_f
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v3, "com.vcast.mediamanager.ACTION_PICTURES"

    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/utils/VZCloudUtils;->launchVZCloud(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 239
    :sswitch_data_0
    .sparse-switch
        0x7f0f0251 -> :sswitch_b
        0x7f0f0253 -> :sswitch_5
        0x7f0f0257 -> :sswitch_c
        0x7f0f026e -> :sswitch_6
        0x7f0f026f -> :sswitch_4
        0x7f0f0283 -> :sswitch_9
        0x7f0f0287 -> :sswitch_7
        0x7f0f0289 -> :sswitch_2
        0x7f0f028a -> :sswitch_d
        0x7f0f028c -> :sswitch_0
        0x7f0f028d -> :sswitch_3
        0x7f0f028e -> :sswitch_8
        0x7f0f028f -> :sswitch_a
        0x7f0f0292 -> :sswitch_e
        0x7f0f0293 -> :sswitch_1
        0x7f0f0294 -> :sswitch_f
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 12
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v11, 0x2

    const v10, 0x7f0f028d

    const/4 v9, 0x1

    const v8, 0x7f0f0293

    const/4 v7, 0x0

    .line 78
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    .line 79
    .local v1, "currentTabTagType":Lcom/sec/samsung/gallery/core/TabTagType;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/gallery3d/data/HiddenSource;->hasHiddenMedias(Lcom/sec/android/gallery3d/app/GalleryApp;)Z

    move-result v2

    .line 80
    .local v2, "isContainsHiddenItems":Z
    const v5, 0x7f0f028b

    invoke-interface {p1, v5, v9}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 81
    const v5, 0x7f0f028b

    invoke-interface {p1, v5, v9}, Landroid/view/Menu;->setGroupEnabled(IZ)V

    .line 83
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-nez v5, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    if-ne v5, v11, :cond_1

    .line 84
    :cond_0
    const v5, 0x7f0f0251

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 87
    :cond_1
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v5, :cond_2

    .line 88
    const v5, 0x7f0f026f

    invoke-static {p1, v5, v11}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 90
    :cond_2
    const v5, 0x7f0f0275

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 92
    const v5, 0x7f0f028c

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 93
    .local v0, "buaVUXMenuItem":Landroid/view/MenuItem;
    if-eqz v0, :cond_3

    .line 97
    const v5, 0x7f0f028c

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 102
    :cond_3
    const v5, 0x7f0f0294

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 103
    invoke-interface {p1}, Landroid/view/Menu;->hasVisibleItems()Z

    move-result v5

    if-nez v5, :cond_5

    .line 235
    :cond_4
    :goto_0
    return-void

    .line 107
    :cond_5
    sget-object v5, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v1, v5, :cond_6

    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSocialTag20Chn:Z

    if-ne v5, v9, :cond_7

    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSocialTag20ChnWithWeChat:Z

    if-nez v5, :cond_7

    .line 109
    :cond_6
    const v5, 0x7f0f0289

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 112
    :cond_7
    const v5, 0x7f0f0257

    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSettings:Z

    invoke-static {p1, v5, v6}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 114
    const v5, 0x7f0f0290

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 115
    const v5, 0x7f0f0291

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 117
    sget-object v5, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_LOCATION:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v1, v5, :cond_9

    .line 118
    const v5, 0x7f0f026f

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 119
    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 120
    .local v3, "layoutModelItem":Landroid/view/MenuItem;
    if-eqz v3, :cond_8

    .line 121
    invoke-static {p1, v8, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 123
    :cond_8
    invoke-static {p1, v10, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 124
    const v5, 0x7f0f0253

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 125
    const v5, 0x7f0f028e

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 126
    const v5, 0x7f0f0287

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 127
    const v5, 0x7f0f028f

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 129
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    if-ne v5, v9, :cond_4

    .line 130
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v5

    if-nez v5, :cond_4

    .line 131
    const v5, 0x7f0f0251

    invoke-static {p1, v5, v11}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    goto :goto_0

    .line 136
    .end local v3    # "layoutModelItem":Landroid/view/MenuItem;
    :cond_9
    sget-object v5, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v1, v5, :cond_15

    .line 137
    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 138
    .restart local v3    # "layoutModelItem":Landroid/view/MenuItem;
    if-eqz v3, :cond_a

    .line 139
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUse3DViewMode:Z

    if-eqz v5, :cond_14

    .line 140
    const v5, 0x7f020036

    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 142
    const v5, 0x7f0e01b1

    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 153
    :cond_a
    :goto_1
    sget-object v5, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v1, v5, :cond_17

    .line 154
    const v5, 0x7f0f026f

    invoke-static {p1, v5, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 155
    invoke-static {p1, v10, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 156
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v5, :cond_16

    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mIsNoItemMode:Z

    if-nez v5, :cond_16

    .line 157
    const v5, 0x7f0f028e

    invoke-static {p1, v5, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 161
    :goto_2
    const v5, 0x7f0f0287

    invoke-static {p1, v5, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 167
    const v5, 0x7f0f028f

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 169
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v4

    .line 170
    .local v4, "viewType":I
    sget-object v5, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v5, v4}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v5

    if-nez v5, :cond_c

    sget-object v5, Lcom/sec/samsung/gallery/core/ViewByFilterType;->CLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v5, v4}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v5

    if-nez v5, :cond_b

    sget-object v5, Lcom/sec/samsung/gallery/core/ViewByFilterType;->PICASA:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v5, v4}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v5

    if-nez v5, :cond_b

    sget-object v5, Lcom/sec/samsung/gallery/core/ViewByFilterType;->FACEBOOK:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v5, v4}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 174
    :cond_b
    sget-object v5, Lcom/sec/samsung/gallery/core/ViewByFilterType;->LOCAL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v5, v4}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v5

    if-nez v5, :cond_c

    .line 175
    invoke-static {p1, v10, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 178
    :cond_c
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v6, 0x7f0f0294

    invoke-static {v5, p1, v6}, Lcom/sec/samsung/gallery/view/utils/VZCloudUtils;->prepareOptionsMenu(Landroid/content/Context;Landroid/view/Menu;I)V

    .line 186
    .end local v4    # "viewType":I
    :goto_3
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->fontScale:F

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v5, v5, v6

    if-lez v5, :cond_18

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    if-ne v5, v9, :cond_18

    .line 188
    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 189
    if-eqz v3, :cond_d

    .line 190
    invoke-static {p1, v8, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 200
    :cond_d
    :goto_4
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->isHelpMenuAvailable(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_e

    .line 201
    const v5, 0x7f0f028a

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 204
    :cond_e
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 205
    invoke-static {p1, v10, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 206
    const v5, 0x7f0f0287

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 207
    const v5, 0x7f0f0289

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 208
    const v5, 0x7f0f0257

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 209
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEasymodeHelp:Z

    if-nez v5, :cond_f

    .line 210
    const v5, 0x7f0f028a

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 213
    :cond_f
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    if-ne v5, v9, :cond_19

    .line 214
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v5

    if-nez v5, :cond_10

    .line 215
    const v5, 0x7f0f0251

    invoke-static {p1, v5, v11}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 216
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v5

    if-nez v5, :cond_10

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_10

    .line 218
    const v5, 0x7f0203be

    invoke-static {p1, v10, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIcon(Landroid/view/Menu;II)V

    .line 226
    :cond_10
    :goto_5
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 227
    const v5, 0x7f0f0289

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 229
    :cond_11
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseReOrderAlbums:Z

    if-eqz v5, :cond_12

    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseReOrderAlbumsSlectionMode:Z

    if-eqz v5, :cond_13

    .line 230
    :cond_12
    const v5, 0x7f0f0292

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 232
    :cond_13
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/sec/samsung/gallery/controller/SStudioCmd;->isSStudioAvailable(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 233
    const v5, 0x7f0f0283

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_0

    .line 144
    :cond_14
    invoke-static {p1, v8, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_1

    .line 148
    .end local v3    # "layoutModelItem":Landroid/view/MenuItem;
    :cond_15
    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 149
    .restart local v3    # "layoutModelItem":Landroid/view/MenuItem;
    if-eqz v3, :cond_a

    .line 150
    invoke-static {p1, v8, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_1

    .line 159
    :cond_16
    const v5, 0x7f0f028e

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_2

    .line 180
    :cond_17
    invoke-static {p1, v10, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 181
    const v5, 0x7f0f028e

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 182
    const v5, 0x7f0f0287

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 183
    const v5, 0x7f0f028f

    invoke-static {p1, v5, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_3

    .line 193
    :cond_18
    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 194
    if-eqz v3, :cond_d

    .line 195
    invoke-static {p1, v8, v11}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    goto/16 :goto_4

    .line 221
    :cond_19
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v5

    if-eqz v5, :cond_10

    .line 222
    invoke-static {p1, v10, v11}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 223
    const v5, 0x7f0203be

    invoke-static {p1, v10, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIcon(Landroid/view/Menu;II)V

    goto :goto_5
.end method

.method public setNoItemMode(Z)V
    .locals 0
    .param p1, "isNoItemMode"    # Z

    .prologue
    .line 369
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->mIsNoItemMode:Z

    .line 370
    return-void
.end method
