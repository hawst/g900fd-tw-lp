.class public Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarView;
.source "MapActionBarForNormal.java"


# instance fields
.field private mAlbumName:Ljava/lang/String;

.field private mAlbumPath:Ljava/lang/String;

.field private mAlbumPosition:I

.field private mItemPath:Ljava/lang/String;

.field private mMenu:Landroid/view/Menu;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "albumPath"    # Ljava/lang/String;
    .param p3, "currentItemPath"    # Ljava/lang/String;
    .param p4, "albumPosition"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 47
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mAlbumPath:Ljava/lang/String;

    .line 48
    iput-object p3, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mItemPath:Ljava/lang/String;

    .line 49
    iput p4, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mAlbumPosition:I

    .line 52
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal$1;-><init>(Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mItemPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mAlbumName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mAlbumName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method


# virtual methods
.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mMenu:Landroid/view/Menu;

    .line 80
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 81
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f120028

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 82
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 83
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 102
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 117
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 118
    :goto_0
    return-void

    .line 104
    :sswitch_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 105
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "KEY_MEDIA_SET_PATH"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mAlbumPath:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const-string v2, "KEY_MEDIA_ITEM_PATH"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mItemPath:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    const-string v2, "KEY_MEDIA_SET_POSITION"

    iget v3, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mAlbumPosition:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 108
    const-string v2, "KEY_NO_SPLIT_MODE"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 109
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    const-class v3, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0

    .line 113
    .end local v0    # "bundle":Landroid/os/Bundle;
    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-class v3, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 114
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 102
    :sswitch_data_0
    .sparse-switch
        0x7f0f0257 -> :sswitch_1
        0x7f0f02c8 -> :sswitch_0
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v4, 0x7f0f02c8

    const v0, 0x7f0f02c7

    const v3, 0x7f0f0257

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 87
    invoke-interface {p1, v0, v1}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 88
    invoke-interface {p1, v0, v1}, Landroid/view/Menu;->setGroupEnabled(IZ)V

    .line 90
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mAlbumPath:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 91
    invoke-static {p1, v4, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 92
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSettings:Z

    invoke-static {p1, v3, v0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 98
    :goto_0
    return-void

    .line 95
    :cond_0
    invoke-static {p1, v4, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 96
    invoke-static {p1, v3, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_0
.end method

.method protected setTitle(Ljava/lang/String;)V
    .locals 2
    .param p1, "albumName"    # Ljava/lang/String;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal$2;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal$2;-><init>(Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 132
    return-void
.end method
