.class public Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;
.super Lcom/sec/samsung/gallery/drawer/AbstractDrawerGroup;
.source "DrawerGroupRemote.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContentResolver:Landroid/content/ContentResolver;

.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mDeviceIconDownloadHandler:Landroid/os/Handler;

.field private mNearbyDevicesObserver:Landroid/database/ContentObserver;

.field private mNearbyGroup:Lcom/sec/samsung/gallery/drawer/Group;

.field private mRegisteredDevicesObserver:Landroid/database/ContentObserver;

.field private mSlinkGroup:Lcom/sec/samsung/gallery/drawer/Group;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/drawer/IDrawerCallback;)V
    .locals 3
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "callback"    # Lcom/sec/samsung/gallery/drawer/IDrawerCallback;

    .prologue
    const/4 v0, 0x0

    .line 64
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/drawer/AbstractDrawerGroup;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/drawer/IDrawerCallback;)V

    .line 37
    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mSlinkGroup:Lcom/sec/samsung/gallery/drawer/Group;

    .line 38
    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mNearbyGroup:Lcom/sec/samsung/gallery/drawer/Group;

    .line 40
    new-instance v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote$1;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote$1;-><init>(Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mRegisteredDevicesObserver:Landroid/database/ContentObserver;

    .line 48
    new-instance v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote$2;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote$2;-><init>(Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mNearbyDevicesObserver:Landroid/database/ContentObserver;

    .line 56
    new-instance v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote$3;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote$3;-><init>(Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mDeviceIconDownloadHandler:Landroid/os/Handler;

    .line 65
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mContentResolver:Landroid/content/ContentResolver;

    .line 67
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 70
    invoke-direct {p0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->isPickerMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    invoke-direct {p0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->addRegisteredDevicesGroup()V

    .line 72
    invoke-direct {p0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->addNearbyDevicesGroup()V

    .line 74
    :cond_0
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private addNearbyDevicesGroup()V
    .locals 5

    .prologue
    .line 136
    const/4 v0, 0x0

    .line 137
    .local v0, "nearbySource":Lcom/sec/android/gallery3d/remote/nearby/NearbySource;
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNearby:Z

    if-eqz v1, :cond_0

    .line 138
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const-string v2, "nearby"

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getSource(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSource;

    move-result-object v0

    .end local v0    # "nearbySource":Lcom/sec/android/gallery3d/remote/nearby/NearbySource;
    check-cast v0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    .line 141
    .restart local v0    # "nearbySource":Lcom/sec/android/gallery3d/remote/nearby/NearbySource;
    :cond_0
    new-instance v1, Lcom/sec/samsung/gallery/drawer/Group;

    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0e04c9

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/samsung/gallery/drawer/Group;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;II)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mNearbyGroup:Lcom/sec/samsung/gallery/drawer/Group;

    .line 143
    if-eqz v0, :cond_1

    .line 144
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->updateNearbyItems(Lcom/sec/android/gallery3d/remote/nearby/NearbySource;)V

    .line 146
    :cond_1
    return-void
.end method

.method private addRegisteredDevicesGroup()V
    .locals 6

    .prologue
    .line 121
    const/4 v0, 0x0

    .line 122
    .local v0, "sLinkClient":Lcom/sec/android/gallery3d/remote/slink/SLinkClient;
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSamsungLinkApi:Z

    if-eqz v2, :cond_0

    .line 123
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const-string/jumbo v3, "slink"

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/data/DataManager;->getSource(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSource;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/remote/slink/SLinkSource;

    .line 124
    .local v1, "sLinkSource":Lcom/sec/android/gallery3d/remote/slink/SLinkSource;
    if-eqz v1, :cond_0

    .line 125
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkSource;->getSLinkClient()Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    move-result-object v0

    .line 128
    .end local v1    # "sLinkSource":Lcom/sec/android/gallery3d/remote/slink/SLinkSource;
    :cond_0
    new-instance v2, Lcom/sec/samsung/gallery/drawer/Group;

    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v4, 0x7f0e04ce

    const/4 v5, 0x1

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/samsung/gallery/drawer/Group;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;II)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mSlinkGroup:Lcom/sec/samsung/gallery/drawer/Group;

    .line 130
    if-eqz v0, :cond_1

    .line 131
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->updateSLinkItems(Lcom/sec/android/gallery3d/remote/slink/SLinkClient;)V

    .line 133
    :cond_1
    return-void
.end method

.method private isPickerMode()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 149
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    instance-of v2, v2, Landroid/app/Activity;

    if-eqz v2, :cond_1

    .line 150
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 151
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 152
    const-string v2, "android.intent.action.GET_CONTENT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "android.intent.action.PICK"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "android.intent.action.PERSON_PICK"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "android.intent.action.MULTIPLE_PICK"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 157
    .end local v0    # "action":Ljava/lang/String;
    :cond_1
    return v1
.end method

.method private updateNearbyItems(Lcom/sec/android/gallery3d/remote/nearby/NearbySource;)V
    .locals 10
    .param p1, "nearbySource"    # Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    .prologue
    .line 222
    sget-object v7, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "updateNearbyItems"

    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    if-nez p1, :cond_1

    .line 251
    :cond_0
    return-void

    .line 227
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->getNearbyDeviceSet()Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;

    move-result-object v6

    .line 228
    .local v6, "nearbyDeviceSet":Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->getNearbyContext()Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->getNearbyClient()Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v5

    .line 230
    .local v5, "nearbyClient":Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    sget-object v7, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v5, v7}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->getCheckedDeviceList(Lcom/samsung/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;

    move-result-object v1

    .line 231
    .local v1, "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 232
    .local v3, "length":I
    sget-object v7, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "updateNearbyItems :  provider device list = [ "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/app/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 234
    const/4 v0, 0x0

    .line 235
    .local v0, "bIsDuplicated":Z
    const v8, 0x7f0e04ce

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/allshare/Device;

    iget-object v9, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mDeviceIconDownloadHandler:Landroid/os/Handler;

    invoke-static {v8, v6, v7, v9}, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;->createNearByItems(ILcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;Lcom/samsung/android/allshare/Device;Landroid/os/Handler;)Lcom/sec/samsung/gallery/drawer/DrawerItem;

    move-result-object v4

    .line 247
    .local v4, "nearByItem":Lcom/sec/samsung/gallery/drawer/DrawerItem;
    if-nez v0, :cond_2

    .line 248
    iget-object v7, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mNearbyGroup:Lcom/sec/samsung/gallery/drawer/Group;

    invoke-virtual {v7, v4}, Lcom/sec/samsung/gallery/drawer/Group;->addChild(Lcom/sec/samsung/gallery/drawer/DrawerItem;)V

    .line 233
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getGroupForDevices()Lcom/sec/samsung/gallery/drawer/Group;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mSlinkGroup:Lcom/sec/samsung/gallery/drawer/Group;

    return-object v0
.end method

.method public getGroupForNearby()Lcom/sec/samsung/gallery/drawer/Group;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mNearbyGroup:Lcom/sec/samsung/gallery/drawer/Group;

    return-object v0
.end method

.method public initialize()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mCallback:Lcom/sec/samsung/gallery/drawer/IDrawerCallback;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/drawer/IDrawerCallback;->onUpdate()V

    .line 79
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 110
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 99
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSamsungLinkApi:Z

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mRegisteredDevicesObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 103
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNearby:Z

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mNearbyDevicesObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 106
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 83
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNearby:Z

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mContentResolver:Landroid/content/ContentResolver;

    const-string v1, "nearby://nearby"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mNearbyDevicesObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 87
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSamsungLinkApi:Z

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mRegisteredDevicesObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 92
    :cond_1
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->updateRegisteredDevicesGroup(Z)V

    .line 93
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->updateNearbyDevicesGroup(Z)V

    .line 95
    return-void
.end method

.method public updateNearbyDevicesGroup(Z)V
    .locals 3
    .param p1, "notifyChange"    # Z

    .prologue
    .line 179
    sget-object v1, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "update connected devices group"

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mNearbyGroup:Lcom/sec/samsung/gallery/drawer/Group;

    if-nez v1, :cond_0

    .line 191
    :goto_0
    return-void

    .line 183
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mNearbyGroup:Lcom/sec/samsung/gallery/drawer/Group;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/Group;->removeChildren()V

    .line 185
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNearby:Z

    if-eqz v1, :cond_1

    .line 186
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const-string v2, "nearby"

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getSource(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSource;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    .line 187
    .local v0, "nearbySource":Lcom/sec/android/gallery3d/remote/nearby/NearbySource;
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->updateNearbyItems(Lcom/sec/android/gallery3d/remote/nearby/NearbySource;)V

    .line 190
    .end local v0    # "nearbySource":Lcom/sec/android/gallery3d/remote/nearby/NearbySource;
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mCallback:Lcom/sec/samsung/gallery/drawer/IDrawerCallback;

    invoke-interface {v1}, Lcom/sec/samsung/gallery/drawer/IDrawerCallback;->onUpdate()V

    goto :goto_0
.end method

.method public updateRegisteredDevicesGroup(Z)V
    .locals 4
    .param p1, "notifyChange"    # Z

    .prologue
    .line 161
    sget-object v2, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "update connected devices group"

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mSlinkGroup:Lcom/sec/samsung/gallery/drawer/Group;

    if-nez v2, :cond_0

    .line 176
    :goto_0
    return-void

    .line 165
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mSlinkGroup:Lcom/sec/samsung/gallery/drawer/Group;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/drawer/Group;->removeChildren()V

    .line 167
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSamsungLinkApi:Z

    if-eqz v2, :cond_1

    .line 168
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const-string/jumbo v3, "slink"

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/data/DataManager;->getSource(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSource;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/remote/slink/SLinkSource;

    .line 169
    .local v1, "sLinkSource":Lcom/sec/android/gallery3d/remote/slink/SLinkSource;
    if-eqz v1, :cond_1

    .line 170
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkSource;->getSLinkClient()Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    move-result-object v0

    .line 171
    .local v0, "sLinkClient":Lcom/sec/android/gallery3d/remote/slink/SLinkClient;
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->updateSLinkItems(Lcom/sec/android/gallery3d/remote/slink/SLinkClient;)V

    .line 175
    .end local v0    # "sLinkClient":Lcom/sec/android/gallery3d/remote/slink/SLinkClient;
    .end local v1    # "sLinkSource":Lcom/sec/android/gallery3d/remote/slink/SLinkSource;
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mCallback:Lcom/sec/samsung/gallery/drawer/IDrawerCallback;

    invoke-interface {v2}, Lcom/sec/samsung/gallery/drawer/IDrawerCallback;->onUpdate()V

    goto :goto_0
.end method

.method public updateSLinkItems(Lcom/sec/android/gallery3d/remote/slink/SLinkClient;)V
    .locals 10
    .param p1, "sLinkClient"    # Lcom/sec/android/gallery3d/remote/slink/SLinkClient;

    .prologue
    .line 194
    sget-object v4, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "updateSLinkItems"

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/app/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    if-nez p1, :cond_1

    .line 219
    :cond_0
    return-void

    .line 199
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->getStorageCount()I

    move-result v2

    .line 200
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 201
    const/4 v0, 0x0

    .line 202
    .local v0, "bIsDuplicated":Z
    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->getStoragePath(I)Ljava/lang/String;

    move-result-object v3

    .line 213
    .local v3, "storagePath":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 214
    iget-object v4, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->mSlinkGroup:Lcom/sec/samsung/gallery/drawer/Group;

    const v5, 0x7f0e04ce

    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->getDeviceName(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->getStoragePath(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->getStorageId(I)I

    move-result v8

    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->getNetworkMode(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v5, v6, v7, v8, v9}, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->createSamsungLinkItems(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/sec/samsung/gallery/drawer/DrawerItem;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/drawer/Group;->addChild(Lcom/sec/samsung/gallery/drawer/DrawerItem;)V

    .line 200
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
