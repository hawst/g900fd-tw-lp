.class public Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlPhotoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GlFolderChgAnim"
.end annotation


# instance fields
.field private final HIDE_DISTANCE:F

.field private final HIDE_DURATION:F

.field private mDoComplete:Z

.field private mDoneShow:Z

.field private mFromZ:F

.field private mLastAnimStep:I

.field private mShowStep:I

.field private mThumbHide:[Lcom/sec/android/gallery3d/glcore/GlObject;

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 1

    .prologue
    .line 2427
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .line 2428
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 2417
    const v0, 0x3df5c28f    # 0.12f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->HIDE_DURATION:F

    .line 2418
    const/high16 v0, 0x42c80000    # 100.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->HIDE_DISTANCE:F

    .line 2420
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mThumbHide:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 2429
    return-void
.end method

.method private procHiding(F)V
    .locals 7
    .param p1, "hideRatio"    # F

    .prologue
    const/4 v6, 0x0

    .line 2467
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mFromZ:F

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v5, p1

    sub-float v0, v4, v5

    .line 2468
    .local v0, "dz":F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mThumbHide:[Lcom/sec/android/gallery3d/glcore/GlObject;

    array-length v4, v4

    if-ge v1, v4, :cond_1

    .line 2469
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mThumbHide:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v3, v4, v1

    .line 2470
    .local v3, "thumb":Lcom/sec/android/gallery3d/glcore/GlObject;
    if-nez v3, :cond_0

    .line 2468
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2471
    :cond_0
    const/high16 v4, 0x3f800000    # 1.0f

    sub-float v2, v4, p1

    .line 2472
    .local v2, "ratio":F
    mul-float v4, v2, v2

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/glcore/GlObject;->setAlpha(F)V

    .line 2473
    const/4 v4, 0x4

    invoke-virtual {v3, v6, v6, v0, v4}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPos(FFFI)V

    goto :goto_1

    .line 2475
    .end local v2    # "ratio":F
    .end local v3    # "thumb":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_1
    return-void
.end method

.method private procShow(Z)V
    .locals 8
    .param p1, "doAnim"    # Z

    .prologue
    const/4 v7, 0x1

    .line 2501
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mShowStep:I

    .line 2502
    .local v1, "curStep":I
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mShowStep:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mShowStep:I

    .line 2504
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mLastAnimStep:I

    if-ne v1, v5, :cond_1

    .line 2520
    :cond_0
    :goto_0
    return-void

    .line 2505
    :cond_1
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mLastAnimStep:I

    .line 2506
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mLastAnimStep:I

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemVVisibleCnt:I

    div-int v0, v5, v6

    .line 2507
    .local v0, "col":I
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mLastAnimStep:I

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemVVisibleCnt:I

    rem-int v3, v5, v6

    .line 2508
    .local v3, "row":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemHVisibleCnt:I

    mul-int/2addr v5, v3

    add-int v2, v5, v0

    .line 2509
    .local v2, "index":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mVisibleFirst:I

    add-int/2addr v6, v2

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->getThumbByIndex(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 2510
    .local v4, "thumb":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    if-eqz v4, :cond_2

    .line 2511
    sget v5, Lcom/sec/samsung/gallery/glview/GlScroller;->VISIBLE_BY_INIT:I

    invoke-virtual {v4, v7, v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setVisibility(ZI)V

    .line 2512
    if-eqz p1, :cond_2

    .line 2513
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->startShowUpAnim(I)V

    .line 2516
    :cond_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemVisibleCnt:I

    add-int/lit8 v5, v5, -0x1

    if-lt v2, v5, :cond_0

    .line 2517
    iput-boolean v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mDoneShow:Z

    .line 2518
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->stop()V

    goto :goto_0
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 3
    .param p1, "ratio"    # F

    .prologue
    const v2, 0x3df5c28f    # 0.12f

    .line 2525
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mThumbHide:[Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v1, :cond_0

    .line 2526
    cmpg-float v1, p1, v2

    if-gez v1, :cond_1

    .line 2527
    div-float v0, p1, v2

    .line 2531
    .local v0, "hideRatio":F
    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->procHiding(F)V

    .line 2532
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v0, v1

    if-nez v1, :cond_0

    .line 2533
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->procDeleteLast()V

    .line 2536
    .end local v0    # "hideRatio":F
    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->procShow(Z)V

    .line 2537
    return-void

    .line 2529
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    .restart local v0    # "hideRatio":F
    goto :goto_0
.end method

.method protected onCancel()V
    .locals 2

    .prologue
    .line 2552
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_TYPE_NONE:I

    iput v1, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimType:I

    .line 2553
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_STATUS_IDLE:I
    invoke-static {}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3100()I

    move-result v1

    iput v1, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimStatus:I

    .line 2554
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2540
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->procDeleteLast()V

    .line 2541
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mDoComplete:Z

    if-eqz v0, :cond_1

    .line 2542
    :goto_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mDoneShow:Z

    if-nez v0, :cond_0

    .line 2543
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->procShow(Z)V

    goto :goto_0

    .line 2545
    :cond_0
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mDoComplete:Z

    .line 2547
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_TYPE_NONE:I

    iput v1, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimType:I

    .line 2548
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_STATUS_IDLE:I
    invoke-static {}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3100()I

    move-result v1

    iput v1, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAnimStatus:I

    .line 2549
    return-void
.end method

.method public procDeleteLast()V
    .locals 4

    .prologue
    .line 2481
    const-string v2, "GlPhotoView"

    const-string v3, "GlFolderChgAnim procDeleteLast"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2483
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mThumbHide:[Lcom/sec/android/gallery3d/glcore/GlObject;

    if-nez v2, :cond_0

    .line 2493
    :goto_0
    return-void

    .line 2486
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mThumbHide:[Lcom/sec/android/gallery3d/glcore/GlObject;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 2487
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mThumbHide:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v2, v0

    .line 2488
    .local v1, "thumb":Lcom/sec/android/gallery3d/glcore/GlObject;
    if-nez v1, :cond_1

    .line 2486
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2489
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->remove()V

    .line 2490
    const/4 v1, 0x0

    goto :goto_2

    .line 2492
    .end local v1    # "thumb":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_2
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mThumbHide:[Lcom/sec/android/gallery3d/glcore/GlObject;

    goto :goto_0
.end method

.method public setThumbs([Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 2
    .param p1, "thumbSet"    # [Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 2434
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mThumbHide:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 2435
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mThumbHide:[Lcom/sec/android/gallery3d/glcore/GlObject;

    if-nez v1, :cond_1

    .line 2444
    :cond_0
    return-void

    .line 2438
    :cond_1
    sget v1, Lcom/sec/samsung/gallery/glview/GlAbsListView;->DEF_DISTANCE:F

    neg-float v1, v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mFromZ:F

    .line 2439
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mThumbHide:[Lcom/sec/android/gallery3d/glcore/GlObject;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 2440
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mThumbHide:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v1, v0

    if-eqz v1, :cond_2

    .line 2441
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mThumbHide:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->removeAllListeners()V

    .line 2439
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public startChange()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2447
    const-string v0, "GlPhotoView"

    const-string v1, "GlFolderChgAnim start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2449
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mDoneShow:Z

    .line 2450
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mDoComplete:Z

    .line 2451
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mLastAnimStep:I

    .line 2452
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mShowStep:I

    .line 2453
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->start()V

    .line 2454
    return-void
.end method

.method public stop(Z)V
    .locals 0
    .param p1, "complete"    # Z

    .prologue
    .line 2457
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFolderChgAnim;->mDoComplete:Z

    .line 2458
    invoke-super {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->stop()V

    .line 2459
    return-void
.end method
