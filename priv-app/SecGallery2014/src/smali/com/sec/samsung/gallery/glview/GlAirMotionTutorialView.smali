.class public Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;
.super Lcom/sec/samsung/gallery/glview/GlAbsListView;
.source "GlAirMotionTutorialView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$GlBoundaryEffectAnim;,
        Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "GlAirMotionTutorialView"

.field static mHelpPluginResources:Landroid/content/res/Resources;


# instance fields
.field private final INDEX_CENTER:I

.field private final INDEX_LEFT:I

.field private final INDEX_RIGHT:I

.field private mAnimationC:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

.field private mAnimationL:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

.field private mAnimationR:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

.field private mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

.field private mContext:Landroid/content/Context;

.field private mIgnoreMoveIamge:Z

.field private mIndex:I

.field private mSampleImageC:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

.field private mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

.field private mSampleImageR:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mHelpPluginResources:Landroid/content/res/Resources;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x0

    .line 42
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;-><init>(Landroid/content/Context;)V

    .line 30
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationL:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    .line 31
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationC:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    .line 32
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationR:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mIgnoreMoveIamge:Z

    .line 34
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->INDEX_LEFT:I

    .line 35
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->INDEX_CENTER:I

    .line 36
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->INDEX_RIGHT:I

    .line 37
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mIndex:I

    .line 43
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mContext:Landroid/content/Context;

    .line 44
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;)Lcom/sec/samsung/gallery/glview/GlBlurObject;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    return-object v0
.end method

.method private showBoundaryEffect(Z)V
    .locals 2
    .param p1, "isLeft"    # Z

    .prologue
    .line 208
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$GlBoundaryEffectAnim;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$GlBoundaryEffectAnim;-><init>(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;)V

    .line 209
    .local v0, "ani":Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$GlBoundaryEffectAnim;
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v1, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mRootLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v0, v1, p1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$GlBoundaryEffectAnim;->startAnimation(Lcom/sec/android/gallery3d/glcore/GlLayer;Z)V

    .line 210
    return-void
.end method

.method private translateImage(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;FF)Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;
    .locals 9
    .param p1, "image"    # Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;
    .param p2, "x"    # F
    .param p3, "toX"    # F

    .prologue
    .line 213
    const/16 v8, 0x15e

    .line 214
    .local v8, "duration":I
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->getY()F

    move-result v4

    .line 215
    .local v4, "y":F
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->getZ()F

    move-result v6

    .line 216
    .local v6, "z":F
    move-object v1, p1

    .line 219
    .local v1, "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    move v2, p2

    move v3, p3

    move v5, v4

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;-><init>(Lcom/sec/android/gallery3d/glcore/GlObject;FFFFFF)V

    .line 220
    .local v0, "ani":Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 221
    int-to-long v2, v8

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->setDuration(J)V

    .line 222
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setVisibility(Z)V

    .line 223
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->start()V

    .line 224
    return-object v0
.end method


# virtual methods
.method public getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1, "resourceName"    # Ljava/lang/String;

    .prologue
    .line 353
    sget-object v0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mHelpPluginResources:Landroid/content/res/Resources;

    if-nez v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/GalleryHelp;->getHelpResources(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mHelpPluginResources:Landroid/content/res/Resources;

    .line 356
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mHelpPluginResources:Landroid/content/res/Resources;

    if-eqz v0, :cond_1

    .line 357
    sget-object v0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mHelpPluginResources:Landroid/content/res/Resources;

    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/view/help/GalleryHelp;->getHelpDrawable(Landroid/content/res/Resources;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 360
    :goto_0
    return-object v0

    .line 359
    :cond_1
    const-string v0, "GlAirMotionTutorialView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getHelpDrawable : not find resource!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveImage(Z)V
    .locals 6
    .param p1, "isLeft"    # Z

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x3

    const/4 v1, 0x1

    const v3, 0x3f8ccccd    # 1.1f

    const/4 v2, 0x0

    .line 159
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageC:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageR:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    if-nez v0, :cond_1

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 162
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationL:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationL:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationC:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationC:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 168
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationR:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationR:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    :cond_4
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mIgnoreMoveIamge:Z

    if-nez v0, :cond_0

    .line 174
    if-eqz p1, :cond_7

    .line 175
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mIndex:I

    .line 176
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mIndex:I

    if-ge v0, v1, :cond_5

    .line 177
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mIndex:I

    .line 178
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->showBoundaryEffect(Z)V

    goto :goto_0

    .line 182
    :cond_5
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mIndex:I

    if-ne v0, v1, :cond_6

    .line 183
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidthSpace:F

    neg-float v1, v1

    mul-float/2addr v1, v3

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->translateImage(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;FF)Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationL:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    .line 184
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageC:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidthSpace:F

    mul-float/2addr v1, v3

    invoke-direct {p0, v0, v2, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->translateImage(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;FF)Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationC:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    goto :goto_0

    .line 185
    :cond_6
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mIndex:I

    if-ne v0, v5, :cond_0

    .line 186
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageC:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidthSpace:F

    neg-float v1, v1

    mul-float/2addr v1, v3

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->translateImage(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;FF)Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationC:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    .line 187
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageR:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidthSpace:F

    mul-float/2addr v1, v3

    invoke-direct {p0, v0, v2, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->translateImage(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;FF)Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationR:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    goto :goto_0

    .line 190
    :cond_7
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mIndex:I

    .line 191
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mIndex:I

    if-le v0, v4, :cond_8

    .line 192
    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mIndex:I

    .line 193
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->showBoundaryEffect(Z)V

    goto/16 :goto_0

    .line 197
    :cond_8
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mIndex:I

    if-ne v0, v5, :cond_9

    .line 198
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidthSpace:F

    neg-float v1, v1

    mul-float/2addr v1, v3

    invoke-direct {p0, v0, v2, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->translateImage(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;FF)Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationL:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    .line 199
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageC:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidthSpace:F

    mul-float/2addr v1, v3

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->translateImage(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;FF)Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationC:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    goto/16 :goto_0

    .line 200
    :cond_9
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mIndex:I

    if-ne v0, v4, :cond_0

    .line 201
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageC:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidthSpace:F

    neg-float v1, v1

    mul-float/2addr v1, v3

    invoke-direct {p0, v0, v2, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->translateImage(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;FF)Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationC:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    .line 202
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageR:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidthSpace:F

    mul-float/2addr v1, v3

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->translateImage(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;FF)Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationR:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    goto/16 :goto_0
.end method

.method public onConfigChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "configuration"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v1, 0x2

    .line 325
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageC:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageR:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    if-nez v0, :cond_1

    .line 350
    :cond_0
    :goto_0
    return-void

    .line 328
    :cond_1
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v0, :cond_3

    .line 329
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_2

    .line 330
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const-string v1, "gallery_air_browse_try01_h"

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 331
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageC:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const-string v1, "gallery_air_browse_try02_h"

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 332
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageR:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const-string v1, "gallery_air_browse_try03_h"

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 334
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const-string v1, "gallery_air_browse_try01"

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 335
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageC:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const-string v1, "gallery_air_browse_try02"

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 336
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageR:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const-string v1, "gallery_air_browse_try03"

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 339
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_4

    .line 340
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const v1, 0x7f02003f

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setImage(I)V

    .line 341
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageC:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const v1, 0x7f020041

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setImage(I)V

    .line 342
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageR:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const v1, 0x7f020043

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setImage(I)V

    goto :goto_0

    .line 344
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const v1, 0x7f02003e

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setImage(I)V

    .line 345
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageC:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const v1, 0x7f020040

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setImage(I)V

    .line 346
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageR:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const v1, 0x7f020042

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setImage(I)V

    goto/16 :goto_0
.end method

.method protected onCreate()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 47
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->setClearByColor(Z)V

    .line 48
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->resetLayout()V

    .line 50
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getLcdRect(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object v0

    .line 51
    .local v0, "lcdRect":Landroid/graphics/Rect;
    iget v2, v0, Landroid/graphics/Rect;->right:I

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    if-ge v2, v3, :cond_0

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    div-int/lit8 v1, v2, 0x2

    .line 53
    .local v1, "size":I
    :goto_0
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v2, :cond_2

    .line 54
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v5, :cond_1

    .line 55
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const-string v3, "gallery_air_browse_try01_h"

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-direct {v2, p0, p0, v3, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;-><init>(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;Lcom/sec/android/gallery3d/glcore/GlLayer;Landroid/graphics/drawable/Drawable;I)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    .line 56
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const-string v3, "gallery_air_browse_try02_h"

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-direct {v2, p0, p0, v3, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;-><init>(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;Lcom/sec/android/gallery3d/glcore/GlLayer;Landroid/graphics/drawable/Drawable;I)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageC:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    .line 57
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const-string v3, "gallery_air_browse_try03_h"

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-direct {v2, p0, p0, v3, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;-><init>(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;Lcom/sec/android/gallery3d/glcore/GlLayer;Landroid/graphics/drawable/Drawable;I)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageR:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    .line 75
    :goto_1
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlBlurObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mContext:Landroid/content/Context;

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mUpdateUpperBorder:Z

    invoke-direct {v2, v3, p0, v5, v4}, Lcom/sec/samsung/gallery/glview/GlBlurObject;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlLayer;IZ)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    .line 76
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    const v3, 0x7f0203fa

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->setResourceImage(I)V

    .line 78
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->getGlRoot()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestRender()V

    .line 79
    return-void

    .line 51
    .end local v1    # "size":I
    :cond_0
    iget v2, v0, Landroid/graphics/Rect;->right:I

    div-int/lit8 v1, v2, 0x2

    goto :goto_0

    .line 59
    .restart local v1    # "size":I
    :cond_1
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const-string v3, "gallery_air_browse_try01"

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-direct {v2, p0, p0, v3, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;-><init>(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;Lcom/sec/android/gallery3d/glcore/GlLayer;Landroid/graphics/drawable/Drawable;I)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    .line 60
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const-string v3, "gallery_air_browse_try02"

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-direct {v2, p0, p0, v3, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;-><init>(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;Lcom/sec/android/gallery3d/glcore/GlLayer;Landroid/graphics/drawable/Drawable;I)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageC:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    .line 61
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const-string v3, "gallery_air_browse_try03"

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-direct {v2, p0, p0, v3, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;-><init>(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;Lcom/sec/android/gallery3d/glcore/GlLayer;Landroid/graphics/drawable/Drawable;I)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageR:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    goto :goto_1

    .line 64
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v5, :cond_3

    .line 65
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const v3, 0x7f02003f

    invoke-direct {v2, p0, p0, v3, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;-><init>(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;Lcom/sec/android/gallery3d/glcore/GlLayer;II)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    .line 66
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const v3, 0x7f020041

    invoke-direct {v2, p0, p0, v3, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;-><init>(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;Lcom/sec/android/gallery3d/glcore/GlLayer;II)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageC:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    .line 67
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const v3, 0x7f020043

    invoke-direct {v2, p0, p0, v3, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;-><init>(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;Lcom/sec/android/gallery3d/glcore/GlLayer;II)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageR:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    goto :goto_1

    .line 69
    :cond_3
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const v3, 0x7f02003e

    invoke-direct {v2, p0, p0, v3, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;-><init>(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;Lcom/sec/android/gallery3d/glcore/GlLayer;II)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    .line 70
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const v3, 0x7f020040

    invoke-direct {v2, p0, p0, v3, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;-><init>(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;Lcom/sec/android/gallery3d/glcore/GlLayer;II)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageC:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    .line 71
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    const v3, 0x7f020042

    invoke-direct {v2, p0, p0, v3, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;-><init>(Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;Lcom/sec/android/gallery3d/glcore/GlLayer;II)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageR:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->destroy()V

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->remove()V

    .line 87
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageC:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->remove()V

    .line 88
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageR:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->remove()V

    .line 89
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 321
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->onPause()V

    .line 322
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 315
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->onResume()V

    .line 316
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mIgnoreMoveIamge:Z

    .line 317
    return-void
.end method

.method public onSurfaceChanged(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->stopMoveAnimation()V

    .line 142
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->onSurfaceChanged(II)V

    .line 143
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->resetLayout()V

    .line 144
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mIgnoreMoveIamge:Z

    .line 145
    return-void
.end method

.method public resetLayout()V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const v12, 0x40066666    # 2.1f

    const v11, 0x3f8ccccd    # 1.1f

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 92
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageC:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageR:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    if-nez v4, :cond_1

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->resetLayout()V

    .line 99
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWideMode:Z

    if-eqz v4, :cond_2

    .line 100
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->getImageWidth()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mHeightSpace:F

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->getImageHeight()I

    move-result v5

    int-to-float v5, v5

    div-float v2, v4, v5

    .line 101
    .local v2, "x":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mHeightSpace:F

    invoke-virtual {v4, v2, v5}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setScale(FF)V

    .line 102
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageC:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mHeightSpace:F

    invoke-virtual {v4, v2, v5}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setScale(FF)V

    .line 103
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageR:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mHeightSpace:F

    invoke-virtual {v4, v2, v5}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setScale(FF)V

    .line 105
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidth:I

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->getImageWidth()I

    move-result v6

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mHeight:I

    mul-int/2addr v6, v7

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->getImageHeight()I

    move-result v7

    div-int/2addr v6, v7

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidth:I

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->getImageWidth()I

    move-result v7

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mHeight:I

    mul-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->getImageHeight()I

    move-result v8

    div-int/2addr v7, v8

    sub-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    invoke-virtual {v4, v5, v6, v10, v10}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->setPadding(IIII)V

    .line 108
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mHeight:I

    .line 109
    .local v0, "blurH":I
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidth:I

    div-int/lit8 v1, v4, 0x5

    .line 121
    .end local v2    # "x":F
    .local v1, "blurW":I
    :goto_1
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mIndex:I

    if-ne v4, v13, :cond_3

    .line 122
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->getDistance()F

    move-result v5

    neg-float v5, v5

    invoke-virtual {v4, v9, v9, v5}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setPos(FFF)V

    .line 123
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageC:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidthSpace:F

    mul-float/2addr v5, v11

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->getDistance()F

    move-result v6

    neg-float v6, v6

    invoke-virtual {v4, v5, v9, v6}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setPos(FFF)V

    .line 124
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageR:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidthSpace:F

    mul-float/2addr v5, v12

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->getDistance()F

    move-result v6

    neg-float v6, v6

    invoke-virtual {v4, v5, v9, v6}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setPos(FFF)V

    .line 135
    :goto_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    const/16 v5, 0xd1

    invoke-virtual {v4, v10, v5, v1, v0}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->setAlignAndScale(IIII)V

    .line 136
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    const/16 v5, 0x53

    invoke-virtual {v4, v13, v5, v1, v0}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->setAlignAndScale(IIII)V

    .line 137
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    invoke-virtual {v4, v10, v9}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->show(IF)V

    goto/16 :goto_0

    .line 111
    .end local v0    # "blurH":I
    .end local v1    # "blurW":I
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->getImageHeight()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidthSpace:F

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->getImageWidth()I

    move-result v5

    int-to-float v5, v5

    div-float v3, v4, v5

    .line 112
    .local v3, "y":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidthSpace:F

    invoke-virtual {v4, v5, v3}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setScale(FF)V

    .line 113
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageC:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidthSpace:F

    invoke-virtual {v4, v5, v3}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setScale(FF)V

    .line 114
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageR:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidthSpace:F

    invoke-virtual {v4, v5, v3}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setScale(FF)V

    .line 116
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    invoke-virtual {v4, v10, v10, v10, v10}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->setPadding(IIII)V

    .line 117
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->getImageHeight()I

    move-result v4

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidth:I

    mul-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->getImageWidth()I

    move-result v5

    div-int v0, v4, v5

    .line 118
    .restart local v0    # "blurH":I
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidth:I

    div-int/lit8 v1, v4, 0x5

    .restart local v1    # "blurW":I
    goto :goto_1

    .line 125
    .end local v3    # "y":F
    :cond_3
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mIndex:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_4

    .line 126
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidthSpace:F

    neg-float v5, v5

    mul-float/2addr v5, v11

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->getDistance()F

    move-result v6

    neg-float v6, v6

    invoke-virtual {v4, v5, v9, v6}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setPos(FFF)V

    .line 127
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageC:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->getDistance()F

    move-result v5

    neg-float v5, v5

    invoke-virtual {v4, v9, v9, v5}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setPos(FFF)V

    .line 128
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageR:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidthSpace:F

    mul-float/2addr v5, v11

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->getDistance()F

    move-result v6

    neg-float v6, v6

    invoke-virtual {v4, v5, v9, v6}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setPos(FFF)V

    goto/16 :goto_2

    .line 130
    :cond_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageL:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidthSpace:F

    neg-float v5, v5

    mul-float/2addr v5, v12

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->getDistance()F

    move-result v6

    neg-float v6, v6

    invoke-virtual {v4, v5, v9, v6}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setPos(FFF)V

    .line 131
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageC:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mWidthSpace:F

    neg-float v5, v5

    mul-float/2addr v5, v11

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->getDistance()F

    move-result v6

    neg-float v6, v6

    invoke-virtual {v4, v5, v9, v6}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setPos(FFF)V

    .line 132
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mSampleImageR:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->getDistance()F

    move-result v5

    neg-float v5, v5

    invoke-virtual {v4, v9, v9, v5}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView$SampleImage;->setPos(FFF)V

    goto/16 :goto_2
.end method

.method public stopMoveAnimation()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationL:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationL:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->stop()V

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationC:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    if-eqz v0, :cond_1

    .line 152
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationC:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->stop()V

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationR:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    if-eqz v0, :cond_2

    .line 155
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->mAnimationR:Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlTransAnimation;->stop()V

    .line 156
    :cond_2
    return-void
.end method
