.class Lcom/sec/samsung/gallery/glview/GlPhotoView$19;
.super Ljava/lang/Object;
.source "GlPhotoView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 0

    .prologue
    .line 2189
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$19;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Lcom/sec/android/gallery3d/glcore/GlObject;)Z
    .locals 6
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2191
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$19;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsResizing:Z
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$800(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz p1, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$19;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$500(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    move-result-object v4

    if-eqz v4, :cond_2

    :cond_0
    move v2, v3

    .line 2215
    .end local p1    # "object":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_1
    :goto_0
    return v2

    .line 2194
    .restart local p1    # "object":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$19;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mLClickable:Z
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$2600(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2196
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$19;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCaneExtraHapticEnabled(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2197
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$19;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/16 v5, 0x5320

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    .line 2198
    :cond_3
    check-cast p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .end local p1    # "object":Lcom/sec/android/gallery3d/glcore/GlObject;
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getMainObject()Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/glview/GlSplitObject;

    .line 2199
    .local v1, "obj":Lcom/sec/samsung/gallery/glview/GlSplitObject;
    iget v0, v1, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mIndex:I

    .line 2201
    .local v0, "index":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$19;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    if-eq v4, v0, :cond_1

    .line 2203
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$19;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderObjecSel:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v4, :cond_4

    .line 2204
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$19;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderObjecSel:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v4, v3, v3}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setSelected(ZZ)V

    .line 2207
    :cond_4
    iput-boolean v2, v1, Lcom/sec/samsung/gallery/glview/GlSplitObject;->mSelected:Z

    .line 2208
    invoke-virtual {v1, v3, v3}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->onPressed(II)Z

    .line 2209
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$19;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnConvAnim:Z
    invoke-static {v4, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$402(Lcom/sec/samsung/gallery/glview/GlPhotoView;Z)Z

    .line 2210
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$19;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    sget v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_SWITCH_ALBUM:I

    invoke-virtual {v4, v5, v0, v3, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    .line 2211
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$19;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mEventListener:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlPhotoViewEventListener;
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3000(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$GlPhotoViewEventListener;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2212
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$19;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mEventListener:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlPhotoViewEventListener;
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3000(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$GlPhotoViewEventListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlPhotoViewEventListener;->onAlbumSwitchStart()V

    goto :goto_0
.end method
