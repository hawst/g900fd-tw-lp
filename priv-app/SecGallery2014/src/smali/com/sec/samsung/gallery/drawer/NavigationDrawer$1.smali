.class Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;
.super Landroid/os/Handler;
.source "NavigationDrawer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/drawer/NavigationDrawer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 106
    iget v3, p1, Landroid/os/Message;->what:I

    .line 107
    .local v3, "what":I
    const/4 v2, 0x0

    .line 108
    .local v2, "position":I
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v4, :cond_0

    .line 109
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 112
    :cond_0
    packed-switch v3, :pswitch_data_0

    .line 147
    :goto_0
    return-void

    .line 114
    :pswitch_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # invokes: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->selectItem(I)V
    invoke-static {v4, v2}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$000(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;I)V

    goto :goto_0

    .line 117
    :pswitch_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mDrawerAdapter:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;
    invoke-static {v4}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$100(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 120
    :pswitch_2
    const-string v4, "Gallery_Performance"

    const-string v7, "NavigationDrawer : mHandler : MSG_NOTIFY_INFLATE_DRAWER"

    invoke-static {v4, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    iget-object v4, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # setter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsNeedtoPostInflate:Z
    invoke-static {v4, v6}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$202(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;Z)Z

    .line 122
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDrawerIconNotSetAlpha:Z

    if-nez v4, :cond_1

    .line 123
    iget-object v4, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    const/16 v7, 0xff

    invoke-virtual {v4, v7}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setDrawerIconWithAlpha(I)V

    .line 125
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$300(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Landroid/app/ActionBar;->setHomeAsUpIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 127
    iget-object v4, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # invokes: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->isDrawerHideMode()Z
    invoke-static {v4}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$400(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Z

    move-result v0

    .line 128
    .local v0, "bHideMode":Z
    iget-object v4, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$300(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v7

    if-nez v0, :cond_3

    move v4, v5

    :goto_1
    invoke-virtual {v7, v4}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 130
    iget-object v4, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onResume()V

    .line 132
    iget-object v4, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$500(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v1

    .line 133
    .local v1, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    iget-object v4, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsOnlyAlbumModePick:Z
    invoke-static {v4}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$600(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Z

    move-result v4

    if-nez v4, :cond_2

    instance-of v4, v1, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    if-nez v4, :cond_2

    instance-of v4, v1, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-nez v4, :cond_2

    instance-of v4, v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$500(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v4

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->IsCategoryView(Lcom/sec/samsung/gallery/core/TabTagType;Ljava/lang/Boolean;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 137
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v4, v6}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 141
    :goto_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # invokes: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->adjustLayout()V
    invoke-static {v4}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$700(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)V

    goto/16 :goto_0

    .end local v1    # "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_3
    move v4, v6

    .line 128
    goto :goto_1

    .line 139
    .restart local v1    # "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$1;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    goto :goto_2

    .line 112
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
