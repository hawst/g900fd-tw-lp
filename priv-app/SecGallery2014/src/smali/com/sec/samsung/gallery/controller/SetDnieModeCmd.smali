.class public Lcom/sec/samsung/gallery/controller/SetDnieModeCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "SetDnieModeCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field public static final DISABLE:Z = false

.field public static final ENABLE:Z = true

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/samsung/gallery/controller/SetDnieModeCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/SetDnieModeCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/sec/samsung/gallery/controller/SetDnieModeCmd;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 5
    .param p1, "note"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 22
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Object;

    move-object v2, v3

    check-cast v2, [Ljava/lang/Object;

    .line 23
    .local v2, "params":[Ljava/lang/Object;
    const/4 v3, 0x0

    aget-object v3, v2, v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 24
    .local v0, "isEnable":Z
    const/4 v3, 0x1

    aget-object v1, v2, v3

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 25
    .local v1, "mActivity":Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/sec/samsung/gallery/controller/SetDnieModeCmd$1;

    invoke-direct {v4, p0, v0, v1}, Lcom/sec/samsung/gallery/controller/SetDnieModeCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/SetDnieModeCmd;ZLcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 43
    return-void
.end method
