.class Lcom/sec/samsung/gallery/view/allview/AllViewState$5;
.super Landroid/content/BroadcastReceiver;
.source "AllViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/allview/AllViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$5;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 259
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$5;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1700(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$5;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1500(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->hasWindowFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 260
    const-string v1, "isEnable"

    const/4 v2, 0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 261
    .local v0, "isEnable":Z
    if-eqz v0, :cond_1

    .line 262
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$5;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1700(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$5;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1800(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    .line 266
    .end local v0    # "isEnable":Z
    :cond_0
    :goto_0
    return-void

    .line 264
    .restart local v0    # "isEnable":Z
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$5;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1700(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    goto :goto_0
.end method
