.class Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;
.super Ljava/lang/Object;
.source "DetailViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/OnProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->addItemsToSlideshowProxy(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

.field final synthetic val$isBurst:Z

.field final synthetic val$slideShowProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;ZLcom/sec/android/gallery3d/ui/SelectionManager;)V
    .locals 0

    .prologue
    .line 4301
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    iput-boolean p2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;->val$isBurst:Z

    iput-object p3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;->val$slideShowProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 1
    .param p1, "object"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 4353
    const/4 v0, 0x0

    return v0
.end method

.method public handleOperation(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 4358
    .local p1, "objectList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public onCompleted(Z)V
    .locals 11
    .param p1, "result"    # Z

    .prologue
    .line 4308
    iget-boolean v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;->val$isBurst:Z

    if-eqz v8, :cond_5

    .line 4309
    const/4 v5, 0x0

    .line 4311
    .local v5, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mBurstShotGrouping:Z

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getBaseSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v8

    instance-of v8, v8, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v8, :cond_2

    .line 4312
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getBaseSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v8

    instance-of v8, v8, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v8, :cond_0

    .line 4313
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v2

    .line 4314
    .local v2, "groupId":J
    new-instance v5, Ljava/util/ArrayList;

    .end local v5    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getBaseSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    invoke-virtual {v8, v2, v3}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->getBurstShotItems(J)Ljava/util/ArrayList;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 4340
    .end local v2    # "groupId":J
    .restart local v5    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :cond_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->startBurstPlay()V
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$17500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    .line 4349
    .end local v5    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :cond_1
    :goto_0
    return-void

    .line 4317
    .restart local v5    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :cond_2
    const/4 v4, 0x0

    .line 4318
    .local v4, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v8

    instance-of v8, v8, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v8, :cond_3

    .line 4319
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .line 4320
    .local v7, "localItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getGroupId()J

    move-result-wide v2

    .line 4321
    .restart local v2    # "groupId":J
    iget v0, v7, Lcom/sec/android/gallery3d/data/LocalMediaItem;->bucketId:I

    .line 4322
    .local v0, "bucketId":I
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/sec/android/gallery3d/data/EventAlbumSet;->PATH_IMAGE:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 4323
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mApplication:Lcom/sec/android/gallery3d/app/GalleryApp;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$14200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/EventAlbumManager;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getBurstshotItems(Lcom/sec/android/gallery3d/data/MediaItem;)Ljava/util/ArrayList;

    move-result-object v4

    .line 4333
    .end local v0    # "bucketId":I
    .end local v2    # "groupId":J
    .end local v7    # "localItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :cond_3
    :goto_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;->val$slideShowProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 4334
    if-eqz v4, :cond_0

    .line 4335
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v1, v8, :cond_0

    .line 4336
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;->val$slideShowProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/data/MediaObject;

    invoke-virtual {v9, v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 4335
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4325
    .end local v1    # "i":I
    .restart local v0    # "bucketId":I
    .restart local v2    # "groupId":J
    .restart local v7    # "localItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :cond_4
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "/local/image/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/LocalAlbum;

    .line 4327
    .local v6, "localAlbum":Lcom/sec/android/gallery3d/data/LocalAlbum;
    if-eqz v6, :cond_3

    .line 4328
    const/4 v8, 0x0

    const v9, 0x7fffffff

    invoke-virtual {v6, v8, v9, v2, v3}, Lcom/sec/android/gallery3d/data/LocalAlbum;->getMediaItemByGroupId(IIJ)Ljava/util/ArrayList;

    move-result-object v4

    goto :goto_1

    .line 4341
    .end local v0    # "bucketId":I
    .end local v2    # "groupId":J
    .end local v4    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v5    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    .end local v6    # "localAlbum":Lcom/sec/android/gallery3d/data/LocalAlbum;
    .end local v7    # "localItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :cond_5
    if-eqz p1, :cond_1

    .line 4342
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$6200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    move-result-object v8

    if-nez v8, :cond_6

    .line 4343
    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$5000()Ljava/lang/String;

    move-result-object v8

    const-string v9, "SlideshowSelectionTask : onCompleted() - mModel is null."

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 4346
    :cond_6
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;->val$slideShowProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$6200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    move-result-object v9

    invoke-interface {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentIndex()I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setCurrentIndex(I)V

    .line 4347
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$23;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->startSlideShow()V
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$17600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    goto/16 :goto_0
.end method

.method public onProgress(II)V
    .locals 0
    .param p1, "progress"    # I
    .param p2, "total"    # I

    .prologue
    .line 4304
    return-void
.end method
