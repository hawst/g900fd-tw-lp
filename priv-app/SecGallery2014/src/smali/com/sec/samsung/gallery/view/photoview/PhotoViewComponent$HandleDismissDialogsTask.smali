.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;
.super Landroid/os/AsyncTask;
.source "PhotoViewComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HandleDismissDialogsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V
    .locals 0

    .prologue
    .line 3253
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;

    .prologue
    .line 3253
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V

    return-void
.end method

.method private updateSelectionMode()Ljava/lang/Boolean;
    .locals 20

    .prologue
    .line 3268
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCloneMediaList()Ljava/util/LinkedList;

    move-result-object v12

    .line 3270
    .local v12, "mediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v9, 0x0

    .line 3271
    .local v9, "isSelectedDataChanged":Z
    invoke-virtual {v12}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 3272
    .local v15, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->isCancelled()Z

    move-result v17

    if-eqz v17, :cond_3

    .line 3343
    .end local v15    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v17

    if-eqz v17, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v17

    sget-object v18, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_EVENT:Lcom/sec/samsung/gallery/core/TabTagType;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_2

    .line 3344
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v14

    .line 3345
    .local v14, "mediaset":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v14, :cond_2

    .line 3346
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v17

    const/16 v18, 0x0

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v14, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->update(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/ArrayList;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 3347
    const/4 v9, 0x1

    .line 3352
    .end local v14    # "mediaset":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_2
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v17

    return-object v17

    .line 3274
    .restart local v15    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_3
    instance-of v0, v15, Lcom/sec/android/gallery3d/data/MediaItem;

    move/from16 v17, v0

    if-eqz v17, :cond_0

    move-object v10, v15

    .line 3275
    check-cast v10, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 3276
    .local v10, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    const/16 v16, 0x0

    .line 3277
    .local v16, "updateSelection":Z
    instance-of v8, v10, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .line 3278
    .local v8, "isLocalItem":Z
    instance-of v0, v10, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    move/from16 v17, v0

    if-nez v17, :cond_4

    instance-of v0, v10, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    move/from16 v17, v0

    if-eqz v17, :cond_e

    :cond_4
    const/4 v7, 0x1

    .line 3279
    .local v7, "isCloudItem":Z
    :goto_2
    sget-boolean v17, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-nez v17, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v17

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    if-eqz v17, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->isPicasaDataExist(Landroid/content/Context;)Z

    move-result v17

    if-eqz v17, :cond_f

    const/4 v11, 0x1

    .line 3282
    .local v11, "mIsPicasaAccountActive":Z
    :goto_3
    const/4 v5, 0x0

    .line 3283
    .local v5, "filePathOfMediaSet":Ljava/lang/String;
    const/4 v4, 0x0

    .line 3284
    .local v4, "filePathOfItem":Ljava/lang/String;
    const/4 v2, 0x0

    .line 3285
    .local v2, "albumPath":Lcom/sec/android/gallery3d/data/Path;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    move/from16 v17, v0

    if-eqz v17, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v17

    if-eqz v17, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->name()Ljava/lang/String;

    move-result-object v17

    const-string v18, "IMAGE"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->name()Ljava/lang/String;

    move-result-object v17

    const-string v18, "VIDEO"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_6

    .line 3286
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$3900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v17

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/gallery3d/data/DataManager;->getParentSetOf(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    .line 3288
    :cond_6
    if-nez v2, :cond_7

    .line 3289
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$3900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v17

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/gallery3d/data/DataManager;->getDefaultSetOf(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    .line 3290
    :cond_7
    const/4 v13, 0x0

    .line 3291
    .local v13, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_8

    .line 3292
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$3900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v13

    .end local v13    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    check-cast v13, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 3293
    .restart local v13    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_8
    if-eqz v13, :cond_a

    instance-of v0, v13, Lcom/sec/android/gallery3d/data/LocalAlbum;

    move/from16 v17, v0

    if-nez v17, :cond_9

    instance-of v0, v13, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    move/from16 v17, v0

    if-eqz v17, :cond_a

    .line 3294
    :cond_9
    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v5

    .line 3295
    invoke-static {v10}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getParentPathOnFileSystem(Lcom/sec/android/gallery3d/data/MediaItem;)Ljava/lang/String;

    move-result-object v4

    .line 3297
    const/4 v8, 0x1

    .line 3300
    :cond_a
    if-eqz v5, :cond_10

    const-string v17, ""

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_10

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_10

    .line 3302
    const/16 v16, 0x1

    .line 3318
    :cond_b
    :goto_4
    if-eqz v16, :cond_0

    if-nez v8, :cond_c

    if-eqz v7, :cond_0

    .line 3320
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v17

    if-eqz v17, :cond_d

    .line 3321
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v17

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/gallery3d/app/StateManager;->setReloadRequiredOnResume(Z)V

    .line 3323
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$4000(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->dismissDialogs()V

    .line 3325
    instance-of v0, v10, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    move/from16 v17, v0

    if-eqz v17, :cond_14

    .line 3326
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->exitSelectionMode()V

    goto/16 :goto_1

    .line 3278
    .end local v2    # "albumPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v4    # "filePathOfItem":Ljava/lang/String;
    .end local v5    # "filePathOfMediaSet":Ljava/lang/String;
    .end local v7    # "isCloudItem":Z
    .end local v11    # "mIsPicasaAccountActive":Z
    .end local v13    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_e
    const/4 v7, 0x0

    goto/16 :goto_2

    .line 3279
    .restart local v7    # "isCloudItem":Z
    :cond_f
    const/4 v11, 0x0

    goto/16 :goto_3

    .line 3304
    .restart local v2    # "albumPath":Lcom/sec/android/gallery3d/data/Path;
    .restart local v4    # "filePathOfItem":Ljava/lang/String;
    .restart local v5    # "filePathOfMediaSet":Ljava/lang/String;
    .restart local v11    # "mIsPicasaAccountActive":Z
    .restart local v13    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_10
    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v17

    if-eqz v17, :cond_13

    .line 3305
    new-instance v3, Ljava/io/File;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3306
    .local v3, "file":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->unmountedStorage:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1500(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Ljava/lang/String;

    move-result-object v17

    if-eqz v17, :cond_11

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->unmountedStorage:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1500(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_12

    :cond_11
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v17

    if-nez v17, :cond_13

    if-nez v7, :cond_13

    .line 3307
    :cond_12
    const/16 v16, 0x1

    .line 3311
    .end local v3    # "file":Ljava/io/File;
    :cond_13
    if-eqz v7, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v0, v10}, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;->isCloudItemDeleted(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 3313
    const/16 v16, 0x1

    goto/16 :goto_4

    .line 3329
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v17

    if-eqz v17, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v17

    sget-object v18, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_EVENT:Lcom/sec/samsung/gallery/core/TabTagType;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    .line 3331
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    move/from16 v17, v0

    if-nez v17, :cond_16

    if-nez v8, :cond_16

    if-eqz v7, :cond_17

    .line 3332
    :cond_16
    const/4 v9, 0x1

    .line 3333
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 3334
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v17

    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v13, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;I)V

    goto/16 :goto_0

    .line 3337
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->exitSelectionMode()V

    goto/16 :goto_1
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 1
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 3264
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->updateSelectionMode()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 3253
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 9
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 3357
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 3359
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->isCancelled()Z

    move-result v4

    if-nez v4, :cond_1

    .line 3360
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$500(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v6

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v8, v8, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isAvailableCount(IZZ)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateDoneButton(Z)V

    .line 3362
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v2

    .line 3364
    .local v2, "selectedMediaSetItems":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->getMediaItemAdapterSize()I

    move-result v4

    if-ne v2, v4, :cond_2

    .line 3365
    .local v1, "selectedAll":Z
    :goto_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$500(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 3367
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$500(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    .line 3369
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v3, v3, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mFilteredItems:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->misFilteredForVideoAlbum:Z

    if-eqz v3, :cond_3

    .line 3371
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$500(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v3

    sget-object v4, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    const/4 v5, 0x4

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 3381
    .end local v1    # "selectedAll":Z
    .end local v2    # "selectedMediaSetItems":I
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # invokes: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateHideItemsOptionMenu()V
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$4100(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V

    .line 3386
    :goto_2
    return-void

    .restart local v2    # "selectedMediaSetItems":I
    :cond_2
    move v1, v3

    .line 3364
    goto :goto_0

    .line 3375
    .restart local v1    # "selectedAll":Z
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$500(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v3

    sget-object v4, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 3382
    .end local v1    # "selectedAll":Z
    .end local v2    # "selectedMediaSetItems":I
    :catch_0
    move-exception v0

    .line 3383
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 3384
    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$4200()Ljava/lang/String;

    move-result-object v3

    const-string v4, "onPostExecute : NullPointerException!!"

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/data/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 3253
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 3259
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 3260
    return-void
.end method
