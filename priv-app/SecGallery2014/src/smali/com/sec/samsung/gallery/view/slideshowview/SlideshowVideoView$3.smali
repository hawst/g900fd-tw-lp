.class Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;
.super Ljava/lang/Object;
.source "SlideshowVideoView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "arg0"    # Landroid/media/MediaPlayer;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$600(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3$1;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 174
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mCustomSurfaceView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mCustomSurfaceView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$902(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    .line 176
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$900(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mCustomSurfaceView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 180
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mCustomSurfaceView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;

    move-result-object v0

    new-instance v1, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3$2;-><init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$3;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$CustomSurfaceView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 191
    :cond_1
    return-void
.end method
