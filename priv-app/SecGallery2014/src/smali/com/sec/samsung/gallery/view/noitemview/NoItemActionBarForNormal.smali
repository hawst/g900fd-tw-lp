.class public Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarView;
.source "NoItemActionBarForNormal.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mIsFocusInited:Z

.field private mMenu:Landroid/view/Menu;

.field private mTopSetPath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->TAG:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 54
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;I)V

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mIsFocusInited:Z

    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal$1;-><init>(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 92
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->isReadyToEnableHomeButton()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method private getPreviousViewState()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 277
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getPreviousViewState()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method private handleHiddenAlbumsLaunch()V
    .locals 3

    .prologue
    .line 327
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    const-class v1, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 328
    return-void
.end method

.method private isContactPick()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 294
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "caller_id_pick"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 296
    :goto_0
    return v1

    .line 295
    :catch_0
    move-exception v0

    .line 296
    .local v0, "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method private isNearBySet()Z
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mTopSetPath:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 367
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mTopSetPath:Ljava/lang/String;

    const-string v1, "/nearby"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    .line 369
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPersonPick()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 282
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v2

    sget-object v3, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v2, v3, :cond_0

    .line 283
    const/4 v1, 0x1

    .line 288
    :cond_0
    :goto_0
    return v1

    .line 287
    :catch_0
    move-exception v0

    .line 288
    .local v0, "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method private isWidgetPick()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 302
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "photo-pick"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 304
    :goto_0
    return v1

    .line 303
    :catch_0
    move-exception v0

    .line 304
    .local v0, "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method private setCameraButtonVisibility(Landroid/view/Menu;)V
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v3, 0x7f0f0251

    const/4 v2, 0x0

    .line 265
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    .line 267
    .local v0, "hideCameraButton":Z
    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->isContactPick()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->isPersonPick()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->isConnectedDevice()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 268
    :cond_0
    invoke-static {p1, v3, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 270
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->isPick()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isWilcoxds3gzmEnabled()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isBaffinRDEnabled()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isMs013gzmEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 272
    :cond_2
    invoke-static {p1, v3, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 274
    :cond_3
    return-void
.end method

.method private setShowHiddenItemsVisibility(Landroid/view/Menu;Ljava/lang/Class;)V
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "previousViewState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    const/4 v4, 0x0

    .line 331
    const-class v3, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-ne p2, v3, :cond_1

    .line 332
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    .line 333
    .local v1, "currentTabTagType":Lcom/sec/samsung/gallery/core/TabTagType;
    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v1, v3, :cond_1

    .line 334
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/data/HiddenSource;->hasHiddenMedias(Lcom/sec/android/gallery3d/app/GalleryApp;)Z

    move-result v2

    .line 335
    .local v2, "isContainsHiddenItems":Z
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->isPick()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 336
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 337
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 338
    const-string v3, "person-pick"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 339
    const/4 v2, 0x0

    .line 349
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_0
    :goto_0
    const v3, 0x7f0f0287

    invoke-static {p1, v3, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 352
    .end local v1    # "currentTabTagType":Lcom/sec/samsung/gallery/core/TabTagType;
    .end local v2    # "isContainsHiddenItems":Z
    :cond_1
    return-void

    .line 340
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .restart local v1    # "currentTabTagType":Lcom/sec/samsung/gallery/core/TabTagType;
    .restart local v2    # "isContainsHiddenItems":Z
    :cond_2
    const-string v3, "caller_id_pick"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 341
    const/4 v2, 0x0

    goto :goto_0

    .line 342
    :cond_3
    const-string v3, "android.intent.extra.LOCAL_ONLY"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 343
    const/4 v2, 0x0

    goto :goto_0

    .line 344
    :cond_4
    const-string v3, "photo-pick"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 345
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private setTextOnlyButton(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 260
    const v1, 0x7f0f0288

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 261
    .local v0, "cancel":Landroid/view/MenuItem;
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->setTextOnlyButton(Landroid/view/MenuItem;)V

    .line 262
    return-void
.end method

.method private startAllView()V
    .locals 3

    .prologue
    .line 355
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 356
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "KEY_MEDIA_SET_PATH"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mTopSetPath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    const-string v1, "is_refresh"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 358
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    const-class v2, Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 359
    return-void
.end method


# virtual methods
.method public isConnectedDevice()Z
    .locals 2

    .prologue
    .line 323
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->getPreviousViewState()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/sec/samsung/gallery/view/allview/AllViewState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPick()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 310
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v2

    sget-object v3, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v2

    sget-object v3, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v2

    sget-object v3, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v2, v3, :cond_1

    .line 313
    :cond_0
    const/4 v1, 0x1

    .line 318
    :cond_1
    :goto_0
    return v1

    .line 317
    :catch_0
    move-exception v0

    .line 318
    .local v0, "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMenu:Landroid/view/Menu;

    .line 123
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 125
    .local v0, "inflater":Landroid/view/MenuInflater;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->getPreviousViewState()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    if-ne v1, v2, :cond_1

    .line 126
    const v1, 0x7f12002a

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 127
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->setTextOnlyButton(Landroid/view/Menu;)V

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->isFestivalMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 134
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 135
    const v1, 0x7f12002b

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 139
    :goto_1
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->setTextOnlyButton(Landroid/view/Menu;)V

    goto :goto_0

    .line 137
    :cond_2
    const v1, 0x7f120029

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_1
.end method

.method protected onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 217
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 254
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 255
    :goto_0
    :sswitch_0
    return-void

    .line 219
    :sswitch_1
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 220
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_NEW_EVENT_ALBUM:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 223
    :sswitch_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "START_CAMERA"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 226
    :sswitch_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "SHOW_CONTENT_TO_DISPLAY_DIALOG"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 229
    :sswitch_4
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->handleHiddenAlbumsLaunch()V

    goto :goto_0

    .line 232
    :sswitch_5
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->startAllView()V

    goto :goto_0

    .line 235
    :sswitch_6
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-class v2, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 236
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 239
    .end local v0    # "intent":Landroid/content/Intent;
    :sswitch_7
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->goToHelpGallery(Landroid/content/Context;)V

    goto :goto_0

    .line 242
    :sswitch_8
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->setResult(I)V

    .line 243
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->finish()V

    goto :goto_0

    .line 251
    :sswitch_9
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v2, "com.vcast.mediamanager.ACTION_PICTURES"

    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/utils/VZCloudUtils;->launchVZCloud(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 217
    :sswitch_data_0
    .sparse-switch
        0x7f0f0251 -> :sswitch_2
        0x7f0f0257 -> :sswitch_6
        0x7f0f0287 -> :sswitch_4
        0x7f0f0288 -> :sswitch_8
        0x7f0f0289 -> :sswitch_3
        0x7f0f028a -> :sswitch_7
        0x7f0f028c -> :sswitch_0
        0x7f0f0294 -> :sswitch_9
        0x7f0f02be -> :sswitch_1
        0x7f0f02c9 -> :sswitch_5
    .end sparse-switch
.end method

.method protected onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 11
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v10, 0x7f0f0289

    const v9, 0x7f0f0257

    const/4 v8, 0x1

    const v7, 0x7f0f028a

    const/4 v6, 0x0

    .line 144
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->getPreviousViewState()Ljava/lang/Class;

    move-result-object v3

    .line 146
    .local v3, "previousViewState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    const-class v4, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    if-ne v3, v4, :cond_1

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSettings:Z

    invoke-static {p1, v9, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 151
    invoke-direct {p0, p1, v3}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->setShowHiddenItemsVisibility(Landroid/view/Menu;Ljava/lang/Class;)V

    .line 153
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->isPick()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 154
    invoke-static {p1, v9, v6}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 155
    invoke-static {p1, v7, v6}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 156
    const v4, 0x7f0f0287

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkShowHiddenMenu(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v5

    invoke-static {p1, v4, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 159
    :cond_2
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->isPick()Z

    move-result v4

    if-nez v4, :cond_4

    .line 160
    :cond_3
    const v4, 0x7f0f0288

    invoke-static {p1, v4, v6}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 161
    :cond_4
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->setCameraButtonVisibility(Landroid/view/Menu;)V

    .line 163
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v8, :cond_5

    .line 164
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v4

    if-nez v4, :cond_5

    .line 165
    const v4, 0x7f0f0251

    const/4 v5, 0x2

    invoke-static {p1, v4, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 170
    :cond_5
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mIsFocusInited:Z

    if-nez v4, :cond_6

    .line 171
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 172
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_f

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    .line 173
    .local v1, "actionView":Landroid/view/View;
    :goto_1
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 174
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mIsFocusInited:Z

    .line 178
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    .end local v1    # "actionView":Landroid/view/View;
    :cond_6
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_7

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->isNearBySet()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 179
    :cond_7
    invoke-static {p1, v10, v6}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 182
    :cond_8
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEmergencyMode(Landroid/app/Activity;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 183
    invoke-static {p1, v9, v6}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 184
    invoke-static {p1, v7, v6}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 185
    invoke-static {p1, v10, v6}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 188
    :cond_9
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->isHelpMenuAvailable(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_a

    .line 189
    invoke-static {p1, v7, v6}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 192
    :cond_a
    const v4, 0x7f0f028c

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 193
    .local v2, "buaVUXMenuItem":Landroid/view/MenuItem;
    if-eqz v2, :cond_b

    .line 197
    const v4, 0x7f0f028c

    invoke-static {p1, v4, v6}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 201
    :cond_b
    const-class v4, Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    if-ne v3, v4, :cond_c

    .line 202
    const v4, 0x7f0f02be

    invoke-static {p1, v4, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 205
    :cond_c
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->isConnectedDevice()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 206
    const v4, 0x7f0f02c9

    invoke-static {p1, v4, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 207
    invoke-static {p1, v7, v6}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 210
    :cond_d
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->isPick()Z

    move-result v4

    if-nez v4, :cond_0

    const-class v4, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-eq v3, v4, :cond_e

    const-class v4, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-ne v3, v4, :cond_0

    .line 211
    :cond_e
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v5, 0x7f0f0294

    invoke-static {v4, p1, v5}, Lcom/sec/samsung/gallery/view/utils/VZCloudUtils;->prepareOptionsMenu(Landroid/content/Context;Landroid/view/Menu;I)V

    goto/16 :goto_0

    .line 172
    .end local v2    # "buaVUXMenuItem":Landroid/view/MenuItem;
    .restart local v0    # "actionBar":Landroid/app/ActionBar;
    :cond_f
    const/4 v1, 0x0

    goto/16 :goto_1
.end method

.method protected setTitle(Ljava/lang/String;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 96
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0f0010

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 97
    .local v0, "titleView":Landroid/widget/TextView;
    if-nez v0, :cond_0

    .line 98
    sget-object v1, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->TAG:Ljava/lang/String;

    const-string v2, "cannot set title because textview resource is not be"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    :goto_0
    return-void

    .line 102
    :cond_0
    if-nez p1, :cond_1

    .line 103
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 104
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 106
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 107
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setTopSetPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 362
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->mTopSetPath:Ljava/lang/String;

    .line 363
    return-void
.end method

.method protected setupButtons()V
    .locals 2

    .prologue
    .line 113
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->setupButtons()V

    .line 114
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->isPick()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->getPreviousViewState()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    if-ne v0, v1, :cond_1

    .line 116
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->showCameraButton(Z)V

    .line 117
    :cond_1
    return-void
.end method
