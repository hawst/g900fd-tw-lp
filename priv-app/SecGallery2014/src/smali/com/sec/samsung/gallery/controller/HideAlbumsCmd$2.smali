.class Lcom/sec/samsung/gallery/controller/HideAlbumsCmd$2;
.super Ljava/lang/Object;
.source "HideAlbumsCmd.java"

# interfaces
.implements Lcom/sec/samsung/gallery/util/HiddenOperations$OnHiddenListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->handleOperation(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;

.field final synthetic val$operationId:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;I)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;

    iput p2, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd$2;->val$operationId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 3
    .param p1, "mediaObject"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 105
    const/4 v0, 0x0

    .line 107
    .local v0, "result":Z
    iget v1, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd$2;->val$operationId:I

    if-nez v1, :cond_1

    .line 108
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/data/MediaObject;->hide(Ljava/lang/String;)Z

    move-result v0

    .line 115
    :cond_0
    :goto_0
    return v0

    .line 109
    :cond_1
    iget v1, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd$2;->val$operationId:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 110
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaObject;->show()Z

    move-result v0

    goto :goto_0

    .line 111
    :cond_2
    iget v1, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd$2;->val$operationId:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 112
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaObject;->delete()V

    .line 113
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onCancelled()V
    .locals 4

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->access$200(Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 121
    return-void
.end method

.method public onCompleted(I)V
    .locals 4
    .param p1, "failedCount"    # I

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;

    # invokes: Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->dismissDialog()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->access$100(Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;)V

    .line 99
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->access$200(Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 100
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;

    iget v1, p0, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd$2;->val$operationId:I

    # invokes: Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->showToast(II)V
    invoke-static {v0, v1, p1}, Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;->access$300(Lcom/sec/samsung/gallery/controller/HideAlbumsCmd;II)V

    .line 101
    return-void
.end method
