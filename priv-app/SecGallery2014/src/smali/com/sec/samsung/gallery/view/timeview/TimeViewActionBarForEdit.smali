.class public Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;
.source "TimeViewActionBarForEdit.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isNeedScanLastShareAPPSupport:Z

.field private mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

.field private mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

.field private mIsSelectAll:Z

.field private mMenu:Landroid/view/Menu;

.field private mPopupMenu:Landroid/widget/ListPopupWindow;

.field protected mQuantitySelectedAlbum:I

.field protected mQuantitySelectedItem:I

.field private mSelectionButton:Landroid/widget/Button;

.field private mSelectionCount:Landroid/widget/TextView;

.field private needScanLastShareAPPSupportOneMoreTime:Z

.field private outOfShareMaxItemLimit:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 57
    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;I)V

    .line 41
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mIsSelectAll:Z

    .line 47
    iput v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mQuantitySelectedAlbum:I

    .line 48
    iput v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mQuantitySelectedItem:I

    .line 52
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->isNeedScanLastShareAPPSupport:Z

    .line 53
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->needScanLastShareAPPSupportOneMoreTime:Z

    .line 54
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->outOfShareMaxItemLimit:Z

    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    .line 59
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->setSelectAllButtonTitle(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;ZI)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;
    .param p1, "x1"    # Z
    .param p2, "x2"    # I

    .prologue
    .line 38
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->updatePopupMenuItemsVisibility(ZI)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;)Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;)Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;)Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;)Landroid/view/Menu;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mMenu:Landroid/view/Menu;

    return-object v0
.end method

.method private checkNeedScanLastShareAppSupport()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 392
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberItemOfMarkedAsSelected()I

    move-result v0

    .line 393
    .local v0, "numberMarkItem":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v3

    if-eqz v3, :cond_2

    if-le v0, v1, :cond_2

    sget v3, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    if-ge v0, v3, :cond_2

    .line 395
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->outOfShareMaxItemLimit:Z

    if-eqz v3, :cond_1

    .line 396
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->isNeedScanLastShareAPPSupport:Z

    .line 397
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->outOfShareMaxItemLimit:Z

    .line 408
    :goto_0
    sget v3, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    if-le v0, v3, :cond_0

    .line 409
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->outOfShareMaxItemLimit:Z

    .line 412
    :cond_0
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->isNeedScanLastShareAPPSupport:Z

    if-eqz v3, :cond_5

    .line 418
    :goto_1
    return v1

    .line 399
    :cond_1
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->isNeedScanLastShareAPPSupport:Z

    goto :goto_0

    .line 401
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v3

    if-eqz v3, :cond_4

    if-eqz v0, :cond_3

    if-ne v0, v1, :cond_4

    .line 402
    :cond_3
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->needScanLastShareAPPSupportOneMoreTime:Z

    .line 403
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->isNeedScanLastShareAPPSupport:Z

    goto :goto_0

    .line 405
    :cond_4
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->isNeedScanLastShareAPPSupport:Z

    goto :goto_0

    .line 414
    :cond_5
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->needScanLastShareAPPSupportOneMoreTime:Z

    if-eqz v3, :cond_6

    .line 415
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->needScanLastShareAPPSupportOneMoreTime:Z

    goto :goto_1

    :cond_6
    move v1, v2

    .line 418
    goto :goto_1
.end method

.method private getOptionMenuId()I
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEmergencyMode(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    const v0, 0x7f120020

    .line 366
    :goto_0
    return v0

    .line 359
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 360
    const v0, 0x7f120009

    goto :goto_0

    .line 361
    :cond_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 362
    const v0, 0x7f12003c

    goto :goto_0

    .line 363
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->isFestivalMode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 364
    const v0, 0x7f120023

    goto :goto_0

    .line 366
    :cond_3
    const v0, 0x7f12003b

    goto :goto_0
.end method

.method private isValidSelection()Z
    .locals 7

    .prologue
    .line 371
    const/4 v0, 0x1

    .line 373
    .local v0, "isValid":Z
    iget v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mQuantitySelectedAlbum:I

    if-nez v2, :cond_1

    .line 374
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0e0113

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 375
    const/4 v0, 0x0

    .line 383
    :cond_0
    :goto_0
    return v0

    .line 376
    :cond_1
    iget v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mQuantitySelectedItem:I

    sget v3, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    if-le v2, v3, :cond_0

    .line 377
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0114

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget v6, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 379
    .local v1, "text":Ljava/lang/CharSequence;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 380
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getPopUpMenu()Landroid/widget/ListPopupWindow;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mPopupMenu:Landroid/widget/ListPopupWindow;

    return-object v0
.end method

.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 69
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 70
    .local v0, "inflater":Landroid/view/MenuInflater;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->getOptionMenuId()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 76
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 196
    sget-object v1, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->TAG:Ljava/lang/String;

    const-string v2, "onOptionsItemSelected start"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 284
    :cond_0
    :goto_0
    return-void

    .line 199
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->isValidSelection()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 200
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->chooseShareDialog()V

    goto :goto_0

    .line 204
    :sswitch_1
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_IMPORT:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 207
    :sswitch_2
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_SLIDESHOW_SETTING:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 211
    :sswitch_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TMSD"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1, v2, v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 212
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_DELETE_DIALOG:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 215
    :sswitch_4
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_COPY_FILES:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 218
    :sswitch_5
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_MOVE_FILES:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 221
    :sswitch_6
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showRenameDialog(Z)V

    goto :goto_0

    .line 224
    :sswitch_7
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showDetailsDialog(Z)V

    goto :goto_0

    .line 227
    :sswitch_8
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3, v4, v4}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->hideAlbums(ZZZ)V

    goto :goto_0

    .line 230
    :sswitch_9
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->CopyToEvent(Z)V

    goto :goto_0

    .line 233
    :sswitch_a
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "START_STORY_ALBUM_APP"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 236
    :sswitch_b
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v1, v0, v4

    const/4 v1, 0x0

    aput-object v1, v0, v3

    .line 237
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "S_STUDIO"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 240
    .end local v0    # "params":[Ljava/lang/Object;
    :sswitch_c
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3, v4}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->moveToSecretbox(ZZ)V

    goto/16 :goto_0

    .line 243
    :sswitch_d
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_FROM_SECRETBOX:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 246
    :sswitch_e
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    sget-object v2, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;->CONFIRM_TAG:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    invoke-virtual {v1, v3, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showRemoveConfirmTagDialog(ZLcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;)V

    goto/16 :goto_0

    .line 249
    :sswitch_f
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/16 v2, 0x904

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->startContactActivity(I)V

    goto/16 :goto_0

    .line 252
    :sswitch_10
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    sget-object v2, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;->REMOVE_TAG:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    invoke-virtual {v1, v3, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showRemoveConfirmTagDialog(ZLcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;)V

    goto/16 :goto_0

    .line 255
    :sswitch_11
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->isValidSelection()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 256
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_LAST_SHARE_APP:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 260
    :sswitch_12
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->download()V

    goto/16 :goto_0

    .line 263
    :sswitch_13
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/16 v2, -0x5a

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->rotate(I)V

    goto/16 :goto_0

    .line 266
    :sswitch_14
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/16 v2, 0x5a

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->rotate(I)V

    goto/16 :goto_0

    .line 269
    :sswitch_15
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3, v4}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->moveToKNOX(ZZ)V

    goto/16 :goto_0

    .line 272
    :sswitch_16
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v4, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->moveToKNOX(ZZ)V

    goto/16 :goto_0

    .line 275
    :sswitch_17
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->printImage()V

    goto/16 :goto_0

    .line 278
    :sswitch_18
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/16 v2, 0x905

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAddUserTagDialog(IZ)V

    goto/16 :goto_0

    .line 281
    :sswitch_19
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->startGifMaker()V

    goto/16 :goto_0

    .line 197
    :sswitch_data_0
    .sparse-switch
        0x7f0f026c -> :sswitch_0
        0x7f0f026d -> :sswitch_11
        0x7f0f026e -> :sswitch_3
        0x7f0f026f -> :sswitch_2
        0x7f0f0270 -> :sswitch_6
        0x7f0f0271 -> :sswitch_8
        0x7f0f0272 -> :sswitch_e
        0x7f0f0273 -> :sswitch_f
        0x7f0f0274 -> :sswitch_10
        0x7f0f0275 -> :sswitch_a
        0x7f0f0276 -> :sswitch_12
        0x7f0f0277 -> :sswitch_c
        0x7f0f0278 -> :sswitch_d
        0x7f0f0279 -> :sswitch_15
        0x7f0f027a -> :sswitch_16
        0x7f0f027b -> :sswitch_3
        0x7f0f027c -> :sswitch_1
        0x7f0f027d -> :sswitch_4
        0x7f0f027e -> :sswitch_5
        0x7f0f027f -> :sswitch_9
        0x7f0f0280 -> :sswitch_13
        0x7f0f0281 -> :sswitch_14
        0x7f0f0282 -> :sswitch_18
        0x7f0f0283 -> :sswitch_b
        0x7f0f0285 -> :sswitch_17
        0x7f0f029b -> :sswitch_7
        0x7f0f02ce -> :sswitch_19
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 13
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v12, 0x7f0f0279

    const v11, 0x7f0f0278

    const v10, 0x7f0f0277

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 80
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mMenu:Landroid/view/Menu;

    .line 81
    const v1, 0x7f0f02dc

    invoke-interface {p1, v1, v9}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 82
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->getSupportedOperationForSelectedItem()J

    move-result-wide v2

    .line 83
    .local v2, "supportOperation":J
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->checkNeedScanLastShareAppSupport()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 84
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mMenu:Landroid/view/Menu;

    invoke-static {v1, v4, v2, v3, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkLastShareAppSupport(Landroid/content/Context;Landroid/view/Menu;JZ)J

    move-result-wide v2

    .line 88
    :goto_0
    invoke-static {p1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->updateMenuOperation(Landroid/view/Menu;J)V

    .line 90
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->isFestivalMode()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 91
    const v1, 0x7f0f0253

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 192
    :cond_0
    :goto_1
    return-void

    .line 86
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mMenu:Landroid/view/Menu;

    invoke-static {v1, v4, v2, v3, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkLastShareAppSupport(Landroid/content/Context;Landroid/view/Menu;JZ)J

    move-result-wide v2

    goto :goto_0

    .line 95
    :cond_2
    const v1, 0x7f0f0275

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 96
    const v1, 0x7f0f0270

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 97
    const v1, 0x7f0f0284

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 98
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mNoUseMovetoAlbumInTimeview:Z

    if-eqz v1, :cond_3

    .line 99
    const v1, 0x7f0f027e

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 101
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    sget-object v4, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v1, v4, :cond_d

    .line 102
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v4, "UPDATE_CONFIRM_MENU"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 103
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v4, 0x7f0f027f

    invoke-static {v1, v4, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 107
    :goto_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v9, :cond_e

    .line 108
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    if-nez v1, :cond_4

    .line 109
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v4, 0x7f0f026e

    const/4 v5, 0x2

    invoke-static {v1, v4, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 111
    :cond_4
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 112
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v4, 0x7f0f026c

    invoke-static {v1, v4, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 113
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v4, 0x7f0f026c

    const v5, 0x7f020030

    invoke-static {v1, v4, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIcon(Landroid/view/Menu;II)V

    .line 120
    :cond_5
    :goto_3
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v1, :cond_11

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecretModeOn()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/samsung/android/privatemode/PrivateModeManager;->isPrivateStorageMounted(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_7

    :cond_6
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isReadyPrivateMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 122
    :cond_7
    const-wide/32 v4, 0x40000000

    and-long/2addr v4, v2

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_f

    .line 123
    invoke-static {p1, v10, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 127
    :goto_4
    const-wide/32 v4, 0x20000000

    and-long/2addr v4, v2

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_10

    .line 128
    invoke-static {p1, v11, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 137
    :goto_5
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableMenuHide:Z

    if-eqz v1, :cond_8

    .line 138
    const v1, 0x7f0f0271

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 141
    :cond_8
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCS03lteEnabled()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 142
    invoke-static {p1, v10, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 143
    invoke-static {p1, v11, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 146
    :cond_9
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsGuestMode:Z

    if-nez v1, :cond_15

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOXInstallMode(Landroid/content/Context;)Z

    move-result v1

    if-ne v1, v9, :cond_15

    .line 147
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 148
    invoke-static {p1, v12, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 149
    const-wide/32 v4, 0x1000000

    and-long/2addr v4, v2

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_12

    .line 150
    const v1, 0x7f0f027a

    invoke-static {p1, v1, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 167
    :goto_6
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/SStudioCmd;->isSStudioAvailable(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_16

    .line 168
    const v1, 0x7f0f0283

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 175
    :cond_a
    :goto_7
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 176
    const v1, 0x7f0f0280

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 177
    const v1, 0x7f0f0281

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 178
    const v1, 0x7f0f029b

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 179
    const v1, 0x7f0f0283

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 181
    :cond_b
    const v1, 0x7f0f027b

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 182
    const v1, 0x7f0f02ce

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 184
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-nez v1, :cond_c

    .line 185
    const v1, 0x7f0f0282

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 188
    :cond_c
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOXInstallMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 189
    const v1, 0x7f0f0280

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 190
    const v1, 0x7f0f0281

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_1

    .line 105
    :cond_d
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v4, 0x7f0f0272

    invoke-static {v1, v4, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_2

    .line 116
    :cond_e
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v1

    if-nez v1, :cond_5

    .line 117
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v4, 0x7f0f026e

    const/4 v5, 0x2

    invoke-static {v1, v4, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    goto/16 :goto_3

    .line 125
    :cond_f
    invoke-static {p1, v10, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_4

    .line 130
    :cond_10
    invoke-static {p1, v11, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_5

    .line 133
    :cond_11
    invoke-static {p1, v10, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 134
    invoke-static {p1, v11, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_5

    .line 152
    :cond_12
    const v1, 0x7f0f027a

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_6

    .line 155
    :cond_13
    const v1, 0x7f0f027a

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 156
    const-wide/32 v4, 0x2000000

    and-long/2addr v4, v2

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_14

    .line 157
    invoke-static {p1, v12, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_6

    .line 159
    :cond_14
    invoke-static {p1, v12, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_6

    .line 163
    :cond_15
    invoke-static {p1, v12, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 164
    const v1, 0x7f0f027a

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_6

    .line 169
    :cond_16
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableStudioShowAsAction:Z

    if-eqz v1, :cond_a

    .line 170
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 171
    .local v0, "locale":Ljava/lang/String;
    const-string/jumbo v1, "vi"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 172
    const v1, 0x7f0f0283

    invoke-static {p1, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    goto/16 :goto_7
.end method

.method public resetSelectAll()V
    .locals 2

    .prologue
    .line 387
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mIsSelectAll:Z

    .line 388
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mIsSelectAll:Z

    iget v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mQuantitySelectedAlbum:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->updatePopupMenuItemsVisibility(ZI)V

    .line 389
    return-void
.end method

.method protected setSelectedItemCount(I)V
    .locals 0
    .param p1, "numberOfItemsSelected"    # I

    .prologue
    .line 315
    iput p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mQuantitySelectedItem:I

    .line 316
    return-void
.end method

.method protected setTitle(I)V
    .locals 2
    .param p1, "numberOfItemsSelected"    # I

    .prologue
    .line 288
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$1;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 311
    return-void
.end method

.method protected updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V
    .locals 2
    .param p1, "button"    # Lcom/sec/samsung/gallery/util/Consts$ButtonType;
    .param p2, "visible"    # I
    .param p3, "toggleImage"    # Z

    .prologue
    .line 320
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$2;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$2;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;Lcom/sec/samsung/gallery/util/Consts$ButtonType;ZI)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 343
    return-void
.end method

.method public updateConfirm(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "format"    # Ljava/lang/String;

    .prologue
    .line 347
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mMenu:Landroid/view/Menu;

    if-eqz v1, :cond_0

    .line 348
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v2, 0x7f0f0272

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 350
    .local v0, "confirmAsMenu":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 351
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 354
    .end local v0    # "confirmAsMenu":Landroid/view/MenuItem;
    :cond_0
    return-void
.end method
