.class public Lcom/sec/samsung/gallery/controller/PlaySequenceCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "PlaySequenceCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field private static final GOLFSHOT_COMPONENT_NAME:Ljava/lang/String; = "com.stri.golfviewer.MainActivity"

.field private static final GOLFSHOT_PACKAGE_NAME:Ljava/lang/String; = "com.stri.golfviewer"

.field private static final SEQUENCESHOT_COMPONENT_NAME:Ljava/lang/String; = "com.sec.android.app.sequenceviewer.SequenceviewerMainActivity"

.field private static final SEQUENCESHOT_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.camera.shootingmode.sequence"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method private startGolfActivity(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 7
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 62
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v2

    .line 63
    .local v2, "filePath":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 64
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 65
    .local v3, "uri":Landroid/net/Uri;
    new-instance v0, Landroid/content/ComponentName;

    const-string v5, "com.stri.golfviewer"

    const-string v6, "com.stri.golfviewer.MainActivity"

    invoke-direct {v0, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    .local v0, "cm":Landroid/content/ComponentName;
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 67
    .local v4, "videoPlayIntent":Landroid/content/Intent;
    invoke-virtual {v4, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 68
    invoke-virtual {v4, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 70
    :try_start_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/PlaySequenceCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    .end local v0    # "cm":Landroid/content/ComponentName;
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v4    # "videoPlayIntent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 71
    .restart local v0    # "cm":Landroid/content/ComponentName;
    .restart local v3    # "uri":Landroid/net/Uri;
    .restart local v4    # "videoPlayIntent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 72
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/PlaySequenceCmd;->mContext:Landroid/content/Context;

    const v6, 0x7f0e0062

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 73
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private startSequenceViewer(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 6
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 44
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v2

    .line 45
    .local v2, "filePath":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 59
    :goto_0
    return-void

    .line 48
    :cond_0
    :try_start_0
    const-string v4, "PlaySequenceCmd"

    const-string v5, "play sequence"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 50
    .local v3, "intent":Landroid/content/Intent;
    const-string v4, "android.intent.category.LAUNCHER"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    new-instance v0, Landroid/content/ComponentName;

    const-string v4, "com.sec.android.app.camera.shootingmode.sequence"

    const-string v5, "com.sec.android.app.sequenceviewer.SequenceviewerMainActivity"

    invoke-direct {v0, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .local v0, "component":Landroid/content/ComponentName;
    invoke-virtual {v3, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 53
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 54
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/PlaySequenceCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 55
    .end local v0    # "component":Landroid/content/ComponentName;
    .end local v3    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 56
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/PlaySequenceCmd;->mContext:Landroid/content/Context;

    const v5, 0x7f0e0062

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 57
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 6
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 29
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Object;

    move-object v2, v3

    check-cast v2, [Ljava/lang/Object;

    .line 30
    .local v2, "params":[Ljava/lang/Object;
    const/4 v3, 0x0

    aget-object v0, v2, v3

    check-cast v0, Landroid/app/Activity;

    .line 31
    .local v0, "activity":Landroid/app/Activity;
    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/PlaySequenceCmd;->mContext:Landroid/content/Context;

    .line 32
    const/4 v3, 0x1

    aget-object v1, v2, v3

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 34
    .local v1, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v1, :cond_1

    .line 41
    :cond_0
    :goto_0
    return-void

    .line 36
    :cond_1
    const-wide/32 v4, 0x40000

    invoke-virtual {v1, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 37
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/PlaySequenceCmd;->startSequenceViewer(Lcom/sec/android/gallery3d/data/MediaItem;)V

    goto :goto_0

    .line 38
    :cond_2
    const-wide/16 v4, 0x80

    invoke-virtual {v1, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 39
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/PlaySequenceCmd;->startGolfActivity(Lcom/sec/android/gallery3d/data/MediaItem;)V

    goto :goto_0
.end method
