.class public Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;
.super Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;
.source "HiddenMediaAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$1;,
        Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;,
        Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$GetUpdateInfo;,
        Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "HiddenMediaAdapter"

.field private static mMergedItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

.field protected final mContext:Landroid/content/Context;

.field private final mCoverData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;"
        }
    .end annotation
.end field

.field private mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

.field private mMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

.field private mMediaSelectionCallback:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;

.field private mMergeSources:Z

.field private final mSource:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mSourceVersion:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 503
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMergedItems:Ljava/util/List;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;ZLcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "pick"    # Z
    .param p4, "mediaFilterType"    # Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    .prologue
    const/4 v0, 0x0

    .line 74
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;-><init>(Landroid/content/Context;)V

    .line 60
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    .line 61
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mData:Ljava/util/List;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mCoverData:Ljava/util/List;

    .line 67
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mSourceVersion:J

    .line 76
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 77
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mContext:Landroid/content/Context;

    .line 78
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 80
    iput-boolean p3, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMergeSources:Z

    .line 81
    iput-object p4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    .line 82
    return-void
.end method

.method private GetMediaTypeForAblumSet(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Ljava/util/List;
    .locals 6
    .param p1, "album"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "filterType"    # Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 587
    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v5

    invoke-virtual {p1, v4, v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v3

    .line 588
    .local v3, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    sget-object v4, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-ne p2, v4, :cond_1

    .line 589
    const/4 v0, 0x2

    .line 593
    .local v0, "MediaDeleteType":I
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 594
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 595
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v4

    if-ne v4, v0, :cond_0

    .line 596
    invoke-interface {v3, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 597
    add-int/lit8 v1, v1, -0x1

    .line 593
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 591
    .end local v0    # "MediaDeleteType":I
    .end local v1    # "i":I
    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    const/4 v0, 0x4

    .restart local v0    # "MediaDeleteType":I
    goto :goto_0

    .line 601
    .restart local v1    # "i":I
    :cond_2
    return-object v3
.end method

.method private GetMediaTypeForList(Ljava/util/List;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Ljava/util/List;
    .locals 4
    .param p2, "filterType"    # Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;",
            "Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 606
    .local p1, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    sget-object v3, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-ne p2, v3, :cond_1

    .line 607
    const/4 v0, 0x2

    .line 611
    .local v0, "MediaDeleteType":I
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 612
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 613
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v3

    if-ne v3, v0, :cond_0

    .line 614
    invoke-interface {p1, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 615
    add-int/lit8 v1, v1, -0x1

    .line 611
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 609
    .end local v0    # "MediaDeleteType":I
    .end local v1    # "i":I
    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    const/4 v0, 0x4

    .restart local v0    # "MediaDeleteType":I
    goto :goto_0

    .line 618
    .restart local v1    # "i":I
    :cond_2
    return-object p1
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mSourceVersion:J

    return-wide v0
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;
    .param p1, "x1"    # J

    .prologue
    .line 57
    iput-wide p1, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mSourceVersion:J

    return-wide p1
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mData:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mCoverData:Ljava/util/List;

    return-object v0
.end method

.method private drawFocusBox(Lcom/sec/android/gallery3d/glcore/GlImageView;IIZ)V
    .locals 3
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I
    .param p3, "itemCount"    # I
    .param p4, "show"    # Z

    .prologue
    const/4 v2, 0x5

    .line 200
    invoke-virtual {p1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 201
    .local v0, "focusBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p4, :cond_2

    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mGenericMotionFocus:I

    if-ne v1, p2, :cond_2

    .line 202
    if-nez v0, :cond_0

    .line 203
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "focusBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 204
    .restart local v0    # "focusBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const v1, 0x7f020291

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 205
    invoke-virtual {p1, v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 208
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 212
    :cond_1
    :goto_0
    return-void

    .line 209
    :cond_2
    if-eqz v0, :cond_1

    .line 210
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private drawFrame(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V
    .locals 5
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "itemCount"    # I

    .prologue
    const/4 v2, 0x1

    .line 263
    invoke-virtual {p1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 265
    .local v0, "frameView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-nez v0, :cond_0

    .line 266
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "frameView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 267
    .restart local v0    # "frameView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {p1, v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 270
    :cond_0
    if-gt p2, v2, :cond_1

    .line 271
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframe_left_padding:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframe_top_padding:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v3, v3, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframe_right_padding:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v4, v4, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframe_bottom_padding:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setPaddings(IIII)V

    .line 273
    const v1, 0x7f0201cc

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 285
    :goto_0
    return-void

    .line 275
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v1, 0xf

    if-ge p2, v1, :cond_2

    .line 276
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_2_left_padding:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_2_top_padding:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v3, v3, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_2_right_padding:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v4, v4, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_2_bottom_padding:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setPaddings(IIII)V

    .line 278
    const v1, 0x7f0201cd

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    goto :goto_0

    .line 281
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_3_left_padding:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_3_top_padding:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v3, v3, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_3_right_padding:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v4, v4, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_3_bottom_padding:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setPaddings(IIII)V

    .line 283
    const v1, 0x7f0201ce

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private drawLabel(Lcom/sec/android/gallery3d/glcore/GlImageView;Ljava/lang/String;I)V
    .locals 14
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "albumName"    # Ljava/lang/String;
    .param p3, "itemCount"    # I

    .prologue
    .line 215
    const/4 v10, 0x2

    invoke-virtual {p1, v10}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 216
    .local v8, "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    const-string v1, ""

    .line 217
    .local v1, "albumLabel":Ljava/lang/String;
    const-string v2, ""

    .line 218
    .local v2, "albumNumber":Ljava/lang/String;
    if-eqz p3, :cond_0

    .line 219
    if-eqz p2, :cond_1

    move-object/from16 v1, p2

    .line 220
    :goto_0
    if-eqz p2, :cond_2

    move/from16 v0, p3

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->getAlbumNumber(I)Ljava/lang/String;

    move-result-object v2

    .line 222
    :cond_0
    :goto_1
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v3, v10, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawlabel_bottom_margin:I

    .line 223
    .local v3, "bottomMargin":I
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v11, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mContext:Landroid/content/Context;

    const-string v12, "albumViewMode"

    const/4 v13, 0x0

    invoke-static {v11, v12, v13}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNameTextboxWidthPixel(I)I

    move-result v10

    int-to-float v9, v10

    .line 224
    .local v9, "textW":F
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewAlbumFontSize(I)I

    move-result v10

    int-to-float v7, v10

    .line 226
    .local v7, "textSize":F
    invoke-static {v7}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getDefaultPaintForLabel(F)Landroid/text/TextPaint;

    move-result-object v5

    .line 227
    .local v5, "paint":Landroid/text/TextPaint;
    move-object/from16 v0, p2

    invoke-static {v0, v2, v9, v5}, Lcom/sec/android/gallery3d/glcore/GlTextView;->ellipsizeString(Ljava/lang/String;Ljava/lang/String;FLandroid/text/TextPaint;)Ljava/lang/String;

    move-result-object v4

    .line 228
    .local v4, "label":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v10}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewAlbumFontColor()I

    move-result v6

    .line 230
    .local v6, "textColor":I
    if-nez v8, :cond_3

    .line 231
    invoke-static {v4, v7, v6}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v8

    .line 232
    const/4 v10, 0x2

    invoke-virtual {p1, v8, v10}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 233
    const/4 v10, 0x2

    const/4 v11, 0x2

    invoke-virtual {v8, v10, v11}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 239
    :goto_2
    return-void

    .line 219
    .end local v3    # "bottomMargin":I
    .end local v4    # "label":Ljava/lang/String;
    .end local v5    # "paint":Landroid/text/TextPaint;
    .end local v6    # "textColor":I
    .end local v7    # "textSize":F
    .end local v9    # "textW":F
    :cond_1
    const-string v1, ""

    goto :goto_0

    .line 220
    :cond_2
    const-string v2, ""

    goto :goto_1

    .line 235
    .restart local v3    # "bottomMargin":I
    .restart local v4    # "label":Ljava/lang/String;
    .restart local v5    # "paint":Landroid/text/TextPaint;
    .restart local v6    # "textColor":I
    .restart local v7    # "textSize":F
    .restart local v9    # "textW":F
    :cond_3
    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v8, v10, v11, v12, v13}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 236
    invoke-virtual {v8, v7}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setTextSize(F)V

    .line 237
    invoke-virtual {v8, v4}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setText(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private drawMediaTypeIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;I)V
    .locals 7
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p4, "itemCount"    # I

    .prologue
    const/16 v6, 0xe

    const/4 v5, 0x1

    const/4 v3, -0x1

    .line 379
    invoke-virtual {p1, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 380
    .local v2, "view":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_2

    invoke-direct {p0, p2, p3}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->getImageResourceId(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v1

    .line 381
    .local v1, "resId":I
    :goto_0
    if-eq v1, v3, :cond_3

    .line 382
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 383
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    if-nez v2, :cond_0

    .line 384
    new-instance v2, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v2    # "view":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 385
    .restart local v2    # "view":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const/4 v3, 0x3

    invoke-virtual {v2, v5, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 386
    invoke-virtual {p1, v2, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 388
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v4, v4, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->typeicon_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v5, v5, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->typeicon_scale:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 398
    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 399
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 403
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    :goto_1
    return-void

    .end local v1    # "resId":I
    :cond_2
    move v1, v3

    .line 380
    goto :goto_0

    .line 400
    .restart local v1    # "resId":I
    :cond_3
    if-eqz v2, :cond_1

    .line 401
    invoke-virtual {v2, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method private drawThumbnailImage(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 12
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I
    .param p3, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/16 v2, 0x140

    .line 331
    const/4 v1, 0x0

    .line 333
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v0, p2}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 337
    :cond_0
    if-nez v1, :cond_1

    .line 338
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->readyDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 349
    .end local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :goto_0
    return-void

    .line 340
    .restart local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    invoke-virtual {p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v4

    .line 341
    .local v4, "rotation":I
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceThumbnail:Z

    if-eqz v0, :cond_2

    instance-of v0, p3, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v0, :cond_2

    invoke-static {p3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 343
    check-cast p3, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {p3}, Lcom/sec/android/gallery3d/data/LocalImage;->getRectOfAllFaces()Landroid/graphics/RectF;

    move-result-object v5

    .local v5, "faceRect":Landroid/graphics/RectF;
    move-object v0, p1

    move v3, v2

    .line 344
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setCroppedBitmap(Landroid/graphics/Bitmap;IIILandroid/graphics/RectF;)V

    goto :goto_0

    .line 346
    .end local v5    # "faceRect":Landroid/graphics/RectF;
    .restart local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_2
    const/4 v11, 0x0

    move-object v6, p1

    move-object v7, v1

    move v8, v2

    move v9, v2

    move v10, v4

    invoke-virtual/range {v6 .. v11}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setCroppedBitmap(Landroid/graphics/Bitmap;IIILandroid/graphics/RectF;)V

    goto :goto_0
.end method

.method private drawUnconfirmIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 6
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/16 v5, 0x30

    const/4 v4, 0x7

    const/4 v3, 0x1

    const/4 v2, 0x3

    .line 310
    invoke-virtual {p1, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 311
    .local v0, "unknowIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemType()I

    move-result v1

    if-nez v1, :cond_3

    .line 312
    if-nez v0, :cond_0

    .line 313
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "unknowIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 314
    .restart local v0    # "unknowIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const v1, 0x7f0200de

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 315
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 316
    invoke-virtual {v0, v2, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 320
    :goto_0
    invoke-virtual {v0, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 321
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 322
    invoke-virtual {p1, v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 324
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 328
    :cond_1
    :goto_1
    return-void

    .line 318
    :cond_2
    invoke-virtual {v0, v3, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    goto :goto_0

    .line 325
    :cond_3
    if-eqz v0, :cond_1

    .line 326
    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method private drawVideoPlayIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;I)V
    .locals 9
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p3, "itemCount"    # I

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 288
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v5

    const/4 v6, 0x4

    if-ne v5, v6, :cond_2

    instance-of v5, p2, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-nez v5, :cond_2

    move v1, v3

    .line 290
    .local v1, "isVideo":Z
    :goto_0
    invoke-virtual {p1, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 291
    .local v2, "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v1, :cond_3

    .line 292
    if-nez v2, :cond_0

    .line 293
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v3

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020308

    invoke-virtual {v3, v5, v6}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 294
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v2, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v2    # "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 295
    .restart local v2    # "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 296
    invoke-virtual {v2, v7, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 297
    invoke-virtual {v2, v4, v4, v4, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 298
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    invoke-virtual {v2, v3, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 299
    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 300
    invoke-virtual {p1, v2, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 302
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    if-eqz v2, :cond_1

    .line 303
    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 307
    :cond_1
    :goto_1
    return-void

    .end local v1    # "isVideo":Z
    .end local v2    # "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_2
    move v1, v4

    .line 288
    goto :goto_0

    .line 304
    .restart local v1    # "isVideo":Z
    .restart local v2    # "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_3
    if-eqz v2, :cond_1

    .line 305
    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method private getAlbumNumber(I)Ljava/lang/String;
    .locals 5
    .param p1, "itemCount"    # I

    .prologue
    .line 257
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 258
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0e01c0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getChildImages(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 4
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 505
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v1

    if-eqz v1, :cond_0

    .line 506
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 507
    invoke-virtual {p1, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->getChildImages(Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 506
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 509
    .end local v0    # "j":I
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    sget-object v2, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->IMAGE:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    sget-object v2, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-ne v1, v2, :cond_2

    .line 510
    :cond_1
    sget-object v1, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMergedItems:Ljava/util/List;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    invoke-direct {p0, p1, v2}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->GetMediaTypeForAblumSet(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 514
    :goto_1
    return-void

    .line 512
    :cond_2
    sget-object v1, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMergedItems:Ljava/util/List;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method private getImageResourceId(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)I
    .locals 1
    .param p1, "media"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 352
    instance-of v0, p1, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    if-eqz v0, :cond_1

    .line 353
    :cond_0
    const v0, 0x7f0200da

    .line 375
    .end local p2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :goto_0
    return v0

    .line 354
    .restart local p2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    instance-of v0, p1, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;

    if-nez v0, :cond_2

    instance-of v0, p1, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;

    if-nez v0, :cond_2

    instance-of v0, p2, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-nez v0, :cond_2

    instance-of v0, p2, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-eqz v0, :cond_4

    .line 355
    :cond_2
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaidu:Z

    if-nez v0, :cond_3

    .line 356
    const v0, 0x7f0200d9

    goto :goto_0

    .line 358
    :cond_3
    const v0, 0x7f0200d8

    goto :goto_0

    .line 359
    :cond_4
    instance-of v0, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    if-nez v0, :cond_5

    instance-of v0, p2, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-eqz v0, :cond_6

    .line 360
    :cond_5
    const v0, 0x7f0201e2

    goto :goto_0

    .line 361
    :cond_6
    instance-of v0, p1, Lcom/sec/android/gallery3d/data/LocalAlbum;

    if-nez v0, :cond_7

    instance-of v0, p1, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-nez v0, :cond_7

    instance-of v0, p2, Lcom/sec/android/gallery3d/data/LocalImage;

    if-nez v0, :cond_7

    instance-of v0, p2, Lcom/sec/android/gallery3d/data/LocalVideo;

    if-eqz v0, :cond_d

    .line 362
    :cond_7
    instance-of v0, p1, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v0, :cond_8

    move-object v0, p1

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->isCameraAlbum()Z

    move-result v0

    if-nez v0, :cond_9

    :cond_8
    if-eqz p2, :cond_b

    check-cast p2, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .end local p2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->isCameraItem()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 364
    :cond_9
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardCameraPath(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    const v0, 0x7f020103

    goto :goto_0

    :cond_a
    const v0, 0x7f020102

    goto :goto_0

    .line 366
    :cond_b
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardPath(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 367
    const v0, 0x7f02010d

    goto :goto_0

    .line 369
    :cond_c
    const v0, 0x7f02010c

    goto :goto_0

    .line 371
    .restart local p2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_d
    instance-of v0, p1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;

    if-nez v0, :cond_e

    instance-of v0, p1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;

    if-nez v0, :cond_e

    instance-of v0, p2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    if-nez v0, :cond_e

    instance-of v0, p2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    if-eqz v0, :cond_f

    .line 372
    :cond_e
    const v0, 0x7f0200dd

    goto/16 :goto_0

    .line 375
    :cond_f
    const/4 v0, -0x1

    goto/16 :goto_0
.end method

.method private setImageViewPadding(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V
    .locals 4
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "itemCount"    # I

    .prologue
    .line 183
    const/4 v0, 0x1

    if-gt p2, v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v0, v0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_left_margin:I

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_top_margin:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_right_margin:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v3, v3, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_bottom_margin:I

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setPaddings(IIII)V

    .line 195
    :goto_0
    return-void

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v0, 0xf

    if-ge p2, v0, :cond_1

    .line 188
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v0, v0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_2_left_margin:I

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_2_top_margin:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_2_right_margin:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v3, v3, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_2_bottom_margin:I

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setPaddings(IIII)V

    goto :goto_0

    .line 192
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v0, v0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_3_left_margin:I

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v1, v1, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_3_top_margin:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v2, v2, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_3_right_margin:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    iget v3, v3, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_3_bottom_margin:I

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setPaddings(IIII)V

    goto :goto_0
.end method


# virtual methods
.method protected addContentListener()V
    .locals 2

    .prologue
    .line 518
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mSourceListener:Lcom/sec/android/gallery3d/data/ContentListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 519
    return-void
.end method

.method public drawCheckboxAndMark(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V
    .locals 8
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I

    .prologue
    const/4 v5, 0x4

    const/4 v7, 0x1

    .line 406
    if-nez p1, :cond_1

    .line 441
    :cond_0
    :goto_0
    return-void

    .line 409
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v3

    .line 410
    .local v3, "selectionManager":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->getMedia(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v2

    .line 411
    .local v2, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p1, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 414
    .local v0, "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 416
    if-nez v0, :cond_2

    .line 417
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 418
    .restart local v0    # "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {p1, v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 420
    :cond_2
    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 421
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v5

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v4, :cond_3

    const v4, 0x7f020122

    :goto_1
    invoke-virtual {v5, v6, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 429
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    :goto_2
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 430
    invoke-virtual {v0, v7, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 431
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 434
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 435
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0

    .line 421
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_3
    const v4, 0x7f0204ad

    goto :goto_1

    .line 425
    :cond_4
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v5

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v4, :cond_5

    const v4, 0x7f020121

    :goto_3
    invoke-virtual {v5, v6, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .restart local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    goto :goto_2

    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_5
    const v4, 0x7f0204a6

    goto :goto_3

    .line 437
    :cond_6
    if-eqz v0, :cond_0

    .line 438
    invoke-virtual {v0, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public getAlbumLabel(I)Ljava/lang/String;
    .locals 12
    .param p1, "position"    # I

    .prologue
    const/4 v8, 0x0

    .line 242
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->getMedia(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v4

    .line 243
    .local v4, "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v9, v4, Lcom/sec/android/gallery3d/data/MediaSet;

    if-nez v9, :cond_0

    .line 244
    const/4 v8, 0x0

    .line 253
    :goto_0
    return-object v8

    :cond_0
    move-object v3, v4

    .line 245
    check-cast v3, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 246
    .local v3, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v2

    .line 247
    .local v2, "itemCount":I
    :goto_1
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mContext:Landroid/content/Context;

    const-string v11, "albumViewMode"

    invoke-static {v10, v11, v8}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v8

    invoke-virtual {v9, v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNameTextboxWidthPixel(I)I

    move-result v8

    int-to-float v7, v8

    .line 249
    .local v7, "textW":F
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v0

    .line 250
    .local v0, "albumName":Ljava/lang/String;
    :goto_2
    if-eqz v3, :cond_3

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->getAlbumNumber(I)Ljava/lang/String;

    move-result-object v1

    .line 251
    .local v1, "albumNumber":Ljava/lang/String;
    :goto_3
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewAlbumFontSize(I)I

    move-result v8

    int-to-float v6, v8

    .line 252
    .local v6, "textSize":F
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewAlbumFontColor()I

    move-result v8

    invoke-static {v6, v8}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getDefaultPaint(FI)Landroid/text/TextPaint;

    move-result-object v5

    .line 253
    .local v5, "paint":Landroid/text/TextPaint;
    invoke-static {v0, v1, v7, v5}, Lcom/sec/android/gallery3d/glcore/GlTextView;->ellipsizeString(Ljava/lang/String;Ljava/lang/String;FLandroid/text/TextPaint;)Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .end local v0    # "albumName":Ljava/lang/String;
    .end local v1    # "albumNumber":Ljava/lang/String;
    .end local v2    # "itemCount":I
    .end local v5    # "paint":Landroid/text/TextPaint;
    .end local v6    # "textSize":F
    .end local v7    # "textW":F
    :cond_1
    move v2, v8

    .line 246
    goto :goto_1

    .line 249
    .restart local v2    # "itemCount":I
    .restart local v7    # "textW":F
    :cond_2
    const-string v0, ""

    goto :goto_2

    .line 250
    .restart local v0    # "albumName":Ljava/lang/String;
    :cond_3
    const-string v1, ""

    goto :goto_3
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 91
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 92
    :cond_0
    const/4 v0, 0x0

    .line 94
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mCoverData:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    return-object v0
.end method

.method protected getItemCount(I)I
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 113
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->getMedia(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    .line 115
    .local v1, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v1, :cond_2

    instance-of v2, v1, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v2, :cond_2

    .line 116
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    sget-object v3, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->IMAGE:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    sget-object v3, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-ne v2, v3, :cond_1

    .line 117
    :cond_0
    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local v1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->GetMediaTypeForAblumSet(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Ljava/util/List;

    move-result-object v0

    .line 118
    .local v0, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 123
    .end local v0    # "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :goto_0
    return v2

    .line 120
    .restart local v1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local v1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v2

    goto :goto_0

    .line 123
    .restart local v1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getItemImage(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "position"    # I
    .param p2, "type"    # I

    .prologue
    .line 106
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    if-nez v0, :cond_1

    .line 107
    :cond_0
    const/4 v0, 0x0

    .line 109
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v0, p1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public getMedia(I)Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 98
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 99
    :cond_0
    const/4 v0, 0x0

    .line 101
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mData:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaObject;

    goto :goto_0
.end method

.method public getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    .line 129
    const/4 v0, 0x0

    return-object v0
.end method

.method public getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p4, "ext"    # I
    .param p5, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v6, 0x0

    .line 137
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->getCount()I

    move-result v5

    if-lt p1, v5, :cond_0

    .line 138
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getView position = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", size = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->getCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 141
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    .line 142
    .local v3, "mediaItems":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->getItemCount(I)I

    move-result v1

    .line 143
    .local v1, "itemCount":I
    const/4 v4, 0x0

    .line 144
    .local v4, "name":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->getMedia(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v2

    .line 145
    .local v2, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v2, :cond_1

    .line 146
    instance-of v5, v2, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v5, :cond_4

    move-object v5, v2

    .line 147
    check-cast v5, Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v4

    .line 153
    :cond_1
    :goto_0
    if-nez p2, :cond_5

    .line 154
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 160
    .local v0, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :goto_1
    invoke-virtual {v0, v6, v6, v6, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setPaddings(IIII)V

    .line 162
    instance-of v5, p5, Lcom/sec/samsung/gallery/glview/GlTextObject;

    if-eqz v5, :cond_2

    .line 163
    invoke-direct {p0, v0, v4, v1}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->drawLabel(Lcom/sec/android/gallery3d/glcore/GlImageView;Ljava/lang/String;I)V

    .line 164
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mFocusOnText:Z

    invoke-direct {p0, v0, p1, v1, v5}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->drawFocusBox(Lcom/sec/android/gallery3d/glcore/GlImageView;IIZ)V

    .line 167
    :cond_2
    instance-of v5, p5, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-eqz v5, :cond_3

    .line 168
    invoke-direct {p0, v0, v3, v1}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->drawVideoPlayIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;I)V

    .line 169
    invoke-direct {p0, v0, p1, v3}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->drawThumbnailImage(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/android/gallery3d/data/MediaItem;)V

    .line 170
    invoke-direct {p0, v0, v3}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->drawUnconfirmIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 171
    instance-of v5, v2, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v5, :cond_6

    .line 172
    check-cast v2, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local v2    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-direct {p0, v0, v2, v3, v1}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->drawMediaTypeIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;I)V

    .line 176
    :goto_2
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mFocusOnText:Z

    if-nez v5, :cond_7

    const/4 v5, 0x1

    :goto_3
    invoke-direct {p0, v0, p1, v1, v5}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->drawFocusBox(Lcom/sec/android/gallery3d/glcore/GlImageView;IIZ)V

    .line 177
    invoke-virtual {p0, v0, p1}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->drawCheckboxAndMark(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V

    .line 179
    :cond_3
    return-object v0

    .end local v0    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .restart local v2    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_4
    move-object v5, v2

    .line 149
    check-cast v5, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_5
    move-object v0, p2

    .line 156
    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v0    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_1

    .line 174
    :cond_6
    const/4 v5, 0x0

    invoke-direct {p0, v0, v5, v3, v1}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->drawMediaTypeIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;I)V

    goto :goto_2

    .end local v2    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_7
    move v5, v6

    .line 176
    goto :goto_3
.end method

.method protected loadData()Z
    .locals 15

    .prologue
    .line 449
    const-wide/16 v10, -0x1

    .line 451
    .local v10, "version":J
    sget-object v12, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v12

    .line 452
    :try_start_0
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    move-result-wide v10

    .line 453
    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 454
    new-instance v9, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$GetUpdateInfo;

    invoke-direct {v9, p0, v10, v11}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$GetUpdateInfo;-><init>(Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;J)V

    invoke-virtual {p0, v9}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->executeAndWait(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;

    .line 455
    .local v2, "info":Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;
    if-eqz v2, :cond_a

    .line 456
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, v2, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;->data:Ljava/util/List;

    .line 457
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, v2, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;->covers:Ljava/util/List;

    .line 458
    sget-object v12, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v12

    .line 459
    const/4 v0, 0x0

    .local v0, "i":I
    :try_start_1
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v6

    .local v6, "n":I
    :goto_0
    if-ge v0, v6, :cond_4

    .line 460
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v9, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v8

    .line 461
    .local v8, "subAlbum":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    .line 462
    iget-boolean v9, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMergeSources:Z

    if-nez v9, :cond_3

    .line 463
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    sget-object v13, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->IMAGE:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-eq v9, v13, :cond_0

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    sget-object v13, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-ne v9, v13, :cond_2

    .line 464
    :cond_0
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    invoke-direct {p0, v8, v9}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->GetMediaTypeForAblumSet(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Ljava/util/List;

    move-result-object v4

    .line 465
    .local v4, "itemListForSet":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_1

    .line 466
    iget-object v9, v2, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;->data:Ljava/util/List;

    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 467
    iget-object v9, v2, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;->covers:Ljava/util/List;

    const/4 v13, 0x0

    invoke-interface {v4, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    invoke-interface {v9, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 459
    .end local v4    # "itemListForSet":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 453
    .end local v0    # "i":I
    .end local v2    # "info":Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;
    .end local v6    # "n":I
    .end local v8    # "subAlbum":Lcom/sec/android/gallery3d/data/MediaSet;
    :catchall_0
    move-exception v9

    :try_start_2
    monitor-exit v12
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v9

    .line 470
    .restart local v0    # "i":I
    .restart local v2    # "info":Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;
    .restart local v6    # "n":I
    .restart local v8    # "subAlbum":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_2
    :try_start_3
    iget-object v9, v2, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;->data:Ljava/util/List;

    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 471
    iget-object v9, v2, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;->covers:Ljava/util/List;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getCoverMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v13

    invoke-interface {v9, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 487
    .end local v6    # "n":I
    .end local v8    # "subAlbum":Lcom/sec/android/gallery3d/data/MediaSet;
    :catchall_1
    move-exception v9

    monitor-exit v12
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v9

    .line 474
    .restart local v6    # "n":I
    .restart local v8    # "subAlbum":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_3
    :try_start_4
    invoke-direct {p0, v8}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->getChildImages(Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_1

    .line 478
    .end local v8    # "subAlbum":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_4
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    sget-object v13, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->IMAGE:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-eq v9, v13, :cond_5

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    sget-object v13, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-ne v9, v13, :cond_7

    .line 479
    :cond_5
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v13, 0x0

    iget-object v14, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v14

    invoke-virtual {v9, v13, v14}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v9

    iget-object v13, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    invoke-direct {p0, v9, v13}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->GetMediaTypeForList(Ljava/util/List;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Ljava/util/List;

    move-result-object v3

    .line 483
    .local v3, "itemListForItem":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :goto_2
    sget-object v9, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMergedItems:Ljava/util/List;

    invoke-interface {v9, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 484
    iget-object v9, v2, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;->data:Ljava/util/List;

    sget-object v13, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMergedItems:Ljava/util/List;

    invoke-interface {v9, v13}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 485
    iget-object v9, v2, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;->covers:Ljava/util/List;

    sget-object v13, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMergedItems:Ljava/util/List;

    invoke-interface {v9, v13}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 486
    sget-object v9, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMergedItems:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->clear()V

    .line 487
    monitor-exit v12
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 489
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMediaSelectionCallback:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;

    if-eqz v9, :cond_9

    .line 490
    new-instance v7, Ljava/util/LinkedHashSet;

    invoke-direct {v7}, Ljava/util/LinkedHashSet;-><init>()V

    .line 491
    .local v7, "pathList":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/gallery3d/data/Path;>;"
    iget-object v9, v2, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;->data:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_6
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 492
    .local v5, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v5, :cond_6

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v9

    invoke-interface {v7, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 481
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "itemListForItem":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v5    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v7    # "pathList":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/gallery3d/data/Path;>;"
    :cond_7
    :try_start_5
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v13, 0x0

    iget-object v14, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v14

    invoke-virtual {v9, v13, v14}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v3

    .restart local v3    # "itemListForItem":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    goto :goto_2

    .line 494
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v7    # "pathList":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/gallery3d/data/Path;>;"
    :cond_8
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMediaSelectionCallback:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;

    invoke-interface {v9, v7}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;->handleMediaList(Ljava/util/Set;)V

    .line 497
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v7    # "pathList":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/gallery3d/data/Path;>;"
    :cond_9
    new-instance v9, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;

    invoke-direct {v9, p0, v2}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;-><init>(Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;)V

    invoke-virtual {p0, v9}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->executeAndWait(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    .line 500
    .end local v0    # "i":I
    .end local v3    # "itemListForItem":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v6    # "n":I
    :cond_a
    const/4 v9, 0x1

    return v9
.end method

.method protected removeContentListener()V
    .locals 2

    .prologue
    .line 523
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mSourceListener:Lcom/sec/android/gallery3d/data/ContentListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->removeContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 524
    return-void
.end method

.method public setMediaSelectionCallback(Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;

    .prologue
    .line 444
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mMediaSelectionCallback:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;

    .line 445
    return-void
.end method
