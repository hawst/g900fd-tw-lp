.class public Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;
.super Lcom/sec/android/gallery3d/app/ActivityState;
.source "NoItemViewState.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAdapter:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;

.field private mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

.field private mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

.field private mEAMReceiver:Landroid/content/BroadcastReceiver;

.field private mEmptySetDrawer:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

.field private mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

.field private mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

.field private mIsAlbumPick:Z

.field private mMenu:Landroid/view/Menu;

.field private mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

.field private mNeedsCrop:Z

.field private mNoItemActionBarForNormal:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

.field private mNoItemViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field private mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

.field private mTopSetPath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const-class v0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 69
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ActivityState;-><init>()V

    .line 79
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    .line 80
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mIsAlbumPick:Z

    .line 81
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mNeedsCrop:Z

    .line 82
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mTopSetPath:Ljava/lang/String;

    .line 86
    new-instance v0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$1;

    const-string v1, "NO_ITEM_VIEW"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$1;-><init>(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mNoItemViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 122
    new-instance v0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$2;-><init>(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    .line 573
    new-instance v0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$5;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$5;-><init>(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->onTabChanged()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->handleFilter()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->startPreviousViewState()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Ljava/lang/Class;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->getPreviousViewState()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/samsung/gallery/core/GalleryFacade;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/android/gallery3d/ui/EmptySetDrawer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mEmptySetDrawer:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mNoItemActionBarForNormal:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/android/gallery3d/app/StateManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/android/gallery3d/data/DataManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mTopSetPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mTopSetPath:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mNeedsCrop:Z

    return v0
.end method

.method static synthetic access$802(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mNeedsCrop:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;

    return-object v0
.end method

.method private cancelSwitchFilterTask()V
    .locals 2

    .prologue
    .line 517
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    if-eqz v0, :cond_0

    .line 518
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->cancel(Z)Z

    .line 519
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    .line 521
    :cond_0
    return-void
.end method

.method private createAdapter(Ljava/lang/String;)V
    .locals 7
    .param p1, "topSetPath"    # Ljava/lang/String;

    .prologue
    .line 396
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->needMediaSetAdapter(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v6

    .line 398
    .local v6, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    new-instance v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1, v6}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;

    .line 402
    .end local v6    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_0
    return-void

    .line 400
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;

    goto :goto_0
.end method

.method private finishCurrentViewState()V
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 406
    return-void
.end method

.method private getCurrentTopSetPath(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 418
    if-nez p1, :cond_0

    .line 419
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 421
    :cond_0
    const-string v1, "KEY_MEDIA_SET_PATH"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 423
    .local v0, "path":Ljava/lang/String;
    return-object v0
.end method

.method private getMimeType(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 427
    const/4 v0, 0x0

    .line 428
    .local v0, "mimeType":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 429
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 431
    :cond_0
    const-string v1, "KEY_NOITEMSVIEW_MIME_TYPE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 433
    return-object v0
.end method

.method private getPreviousViewState()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 409
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getPreviousViewState()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method private getTopFilterMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 4
    .param p1, "viewByType"    # I

    .prologue
    .line 547
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    .line 548
    .local v0, "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    .line 549
    .local v1, "tabType":Lcom/sec/samsung/gallery/core/TabTagType;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3, p1}, Lcom/sec/samsung/gallery/view/utils/DataPath;->getViewByTopSetPath(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 550
    .local v2, "topPath":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/FilterUtils;->toClusterType(Lcom/sec/samsung/gallery/core/TabTagType;)I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/app/FilterUtils;->switchClusterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 551
    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    return-object v3
.end method

.method private handleFilter()V
    .locals 5

    .prologue
    .line 524
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->cancelSwitchFilterTask()V

    .line 525
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v0

    .line 526
    .local v0, "viewByType":I
    new-instance v1, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$4;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, p0, v2}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$4;-><init>(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    .line 543
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v3, 0x0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->getTopFilterMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 544
    return-void
.end method

.method private handleHiddenAlbumsLaunch()V
    .locals 3

    .prologue
    .line 570
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    const-class v1, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 571
    return-void
.end method

.method private handleNewEventAlbum()V
    .locals 3

    .prologue
    .line 562
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-class v2, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 563
    .local v0, "request":Landroid/content/Intent;
    const-string v1, "android.intent.action.MULTIPLE_PICK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 564
    const-string v1, "*/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 565
    const-string v1, "android.intent.extra.LOCAL_ONLY"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 566
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/16 v2, 0x701

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 567
    return-void
.end method

.method private handleResultCameraLaunchForSinglePick(Landroid/content/Intent;I)V
    .locals 12
    .param p1, "data"    # Landroid/content/Intent;
    .param p2, "request"    # I

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 460
    if-nez p1, :cond_1

    .line 497
    :cond_0
    :goto_0
    return-void

    .line 464
    :cond_1
    const/16 v6, 0x902

    if-ne p2, v6, :cond_2

    .line 465
    const-string/jumbo v6, "uri-data"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 466
    .local v0, "fileString":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 468
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 473
    .end local v0    # "fileString":Ljava/lang/String;
    .local v5, "uri":Landroid/net/Uri;
    :goto_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v6, v5, v11}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    .line 474
    .local v3, "path":Lcom/sec/android/gallery3d/data/Path;
    if-nez v3, :cond_3

    .line 475
    sget-object v6, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "cannot find file : path is null : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 470
    .end local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    .restart local v5    # "uri":Landroid/net/Uri;
    goto :goto_1

    .line 479
    .restart local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_3
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v6, v3}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 480
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_0

    .line 483
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v6

    sget-object v7, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v6, v7, :cond_4

    .line 484
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    .line 485
    .local v4, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v4, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 487
    new-array v2, v10, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v6, v2, v8

    aput-object v11, v2, v9

    .line 490
    .local v2, "params":[Ljava/lang/Object;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v6

    const-string v7, "MULTIPLE_PICKER_SELECTION_COMPLETED"

    invoke-virtual {v6, v7, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 492
    .end local v2    # "params":[Ljava/lang/Object;
    .end local v4    # "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    :cond_4
    new-array v2, v10, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v6, v2, v8

    aput-object v1, v2, v9

    .line 495
    .restart local v2    # "params":[Ljava/lang/Object;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v6

    const-string v7, "PICKER_ITEM_SELECTED"

    invoke-virtual {v6, v7, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private handleResultWallpaper(Landroid/content/Intent;I)V
    .locals 7
    .param p1, "data"    # Landroid/content/Intent;
    .param p2, "result"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, -0x1

    const/4 v3, 0x0

    .line 446
    if-eq p2, v6, :cond_0

    if-nez p2, :cond_1

    if-eqz p1, :cond_1

    const-string v5, "is_pressed_cancel"

    invoke-virtual {p1, v5, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    move v0, v4

    .line 448
    .local v0, "finishActivity":Z
    :goto_0
    if-eqz v0, :cond_3

    .line 449
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-ne p2, v6, :cond_2

    .end local p1    # "data":Landroid/content/Intent;
    :goto_1
    invoke-virtual {v3, p2, p1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 450
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 457
    :goto_2
    return-void

    .end local v0    # "finishActivity":Z
    .restart local p1    # "data":Landroid/content/Intent;
    :cond_1
    move v0, v3

    .line 446
    goto :goto_0

    .restart local v0    # "finishActivity":Z
    :cond_2
    move-object p1, v2

    .line 449
    goto :goto_1

    .line 452
    :cond_3
    const/4 v5, 0x2

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v5, v1, v3

    aput-object v2, v1, v4

    .line 455
    .local v1, "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "PICKER_ITEM_SELECTED"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2
.end method

.method private needMediaSetAdapter(Ljava/lang/String;)Z
    .locals 1
    .param p1, "topSetPath"    # Ljava/lang/String;

    .prologue
    .line 437
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 438
    const-string v0, "/allinone"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/nearby"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 439
    :cond_0
    const/4 v0, 0x0

    .line 441
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private onTabChanged()V
    .locals 0

    .prologue
    .line 559
    return-void
.end method

.method private setModelListener()V
    .locals 2

    .prologue
    .line 413
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->setModelListener(Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;)V

    .line 414
    return-void
.end method

.method private startPreviousViewState()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 182
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->getPreviousViewState()Ljava/lang/Class;

    move-result-object v1

    .line 183
    .local v1, "previousViewState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 185
    .local v0, "bundle":Landroid/os/Bundle;
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mIsAlbumPick:Z

    if-eqz v2, :cond_0

    .line 186
    const-string v2, "album-pick"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 188
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mTopSetPath:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 189
    const-string v2, "KEY_MEDIA_SET_PATH"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mTopSetPath:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v3, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$3;

    invoke-direct {v3, p0, v1, v0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$3;-><init>(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;Ljava/lang/Class;Landroid/os/Bundle;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 201
    return-void

    .line 191
    :cond_1
    const-string v2, "KEY_VIEW_REDRAW"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method


# virtual methods
.method protected getNearbyClient()Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    .locals 5

    .prologue
    .line 204
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    .line 205
    .local v1, "manager":Lcom/sec/android/gallery3d/data/DataManager;
    const-string v3, "nearby"

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/data/DataManager;->getSource(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSource;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    .line 206
    .local v2, "nearbySource":Lcom/sec/android/gallery3d/remote/nearby/NearbySource;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->getNearbyContext()Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 208
    :try_start_0
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->getNearbyContext()Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->getNearbyClient()Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 213
    :goto_0
    return-object v3

    .line 209
    :catch_0
    move-exception v0

    .line 210
    .local v0, "e":Ljava/lang/NullPointerException;
    sget-object v3, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->TAG:Ljava/lang/String;

    const-string v4, "NullPointerException at nearbySource.getNearbyContext().getNearbyClient()"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected isDrawableHideMode()Z
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 585
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 586
    .local v1, "intent":Landroid/content/Intent;
    if-nez v1, :cond_1

    .line 603
    :cond_0
    :goto_0
    return v6

    .line 588
    :cond_1
    const-string v7, "album-pick"

    invoke-virtual {v1, v7, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 589
    .local v2, "isAlbumPick":Z
    const-string v7, "onlyMagic"

    invoke-virtual {v1, v7, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 590
    .local v4, "isMagicPick":Z
    const-string v7, "android.intent.action.PERSON_PICK"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 591
    .local v5, "isPersonPick":Z
    const-string v7, "caller_id_pick"

    invoke-virtual {v1, v7, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 593
    .local v3, "isContactPickerMode":Z
    const/4 v0, 0x0

    .line 594
    .local v0, "clusterType":I
    if-eqz v3, :cond_4

    .line 595
    const/16 v0, 0x4000

    .line 603
    :cond_2
    :goto_1
    if-nez v2, :cond_3

    if-nez v4, :cond_3

    if-nez v5, :cond_3

    if-eqz v0, :cond_0

    :cond_3
    const/4 v6, 0x1

    goto :goto_0

    .line 596
    :cond_4
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v7

    sget-object v8, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v7, v8, :cond_2

    .line 597
    const-string v7, "include-recommend"

    invoke-virtual {v1, v7, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 598
    const/high16 v0, 0x10000

    goto :goto_1

    .line 600
    :cond_5
    const v0, 0x8000

    goto :goto_1
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 338
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->finishCurrentViewState()V

    .line 339
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    const v3, 0x7f0f0288

    .line 501
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 502
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 503
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mMenu:Landroid/view/Menu;

    const/4 v2, 0x0

    invoke-static {v1, v3, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIcon(Landroid/view/Menu;II)V

    .line 507
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    if-eqz v1, :cond_0

    .line 508
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    .line 510
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 511
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    .line 512
    .local v0, "size":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mEmptySetDrawer:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->setScreenSize(Landroid/graphics/Point;)V

    .line 513
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mEmptySetDrawer:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->setLayout()V

    .line 514
    return-void

    .line 505
    .end local v0    # "size":Landroid/graphics/Point;
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mMenu:Landroid/view/Menu;

    const v2, 0x7f0203b5

    invoke-static {v1, v3, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIcon(Landroid/view/Menu;II)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 218
    const-string v0, "Gallery_Performance"

    const-string v1, "NoItemViewState onCreate Start"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    new-instance v1, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-direct {v1, v0}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;-><init>(Lcom/sec/android/gallery3d/app/GalleryActivity;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    .line 221
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    .line 222
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 223
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    .line 224
    new-instance v0, Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1, p0}, Lcom/sec/samsung/gallery/view/ActionBarManager;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/app/ActivityState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 225
    new-instance v0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mNoItemActionBarForNormal:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    .line 226
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .line 227
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setStatusProxy(Lcom/sec/android/gallery3d/app/StateManager;)V

    .line 228
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setTitle()V

    .line 229
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setPostInflateStatus(Z)V

    .line 230
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mNoItemViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 232
    if-eqz p1, :cond_0

    .line 233
    const-string v0, "album-pick"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mIsAlbumPick:Z

    .line 236
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->getCurrentTopSetPath(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mTopSetPath:Ljava/lang/String;

    .line 237
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mTopSetPath:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->createAdapter(Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mNoItemActionBarForNormal:Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 239
    new-instance v0, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->getMimeType(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mEmptySetDrawer:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    .line 241
    const-string v0, "Gallery_Performance"

    const-string v1, "NoItemViewState onCreate End"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 304
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mMenu:Landroid/view/Menu;

    .line 305
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 306
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v1, "NO_ITEM_VIEW"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 334
    return-void
.end method

.method public onDirty()V
    .locals 2

    .prologue
    .line 356
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;

    instance-of v1, v1, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    if-eqz v1, :cond_0

    .line 357
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;

    check-cast v1, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 358
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 359
    check-cast v1, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->isNeedUpdateCluster()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 360
    check-cast v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    .end local v0    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->updateMediaSet()V

    .line 363
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 325
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setOptionsItemSelected(Landroid/view/MenuItem;)V

    .line 326
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 329
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 295
    const-string v0, "Gallery_Performance"

    const-string v1, "NoItemViewState onPause Start"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 297
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->onPause()V

    .line 298
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->unregisterEAM(Landroid/content/BroadcastReceiver;)V

    .line 299
    const-string v0, "Gallery_Performance"

    const-string v1, "NoItemViewState onPause End"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    return-void
.end method

.method public onPrepareOptionMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 310
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 311
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-nez v0, :cond_1

    .line 321
    :cond_0
    :goto_0
    return-void

    .line 313
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 314
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mEmptySetDrawer:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->isDrawerVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 316
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mEmptySetDrawer:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->setPopupTextVisible(Z)V

    goto :goto_0

    .line 318
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mEmptySetDrawer:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->setPopupTextVisible(Z)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 246
    const-string v3, "Gallery_Performance"

    const-string v4, "NoItemViewState onResume Start"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->setModelListener()V

    .line 249
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->onResume()V

    .line 251
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->getCount()I

    move-result v3

    if-eqz v3, :cond_1

    .line 252
    const/4 v2, 0x1

    .line 253
    .local v2, "toPreviousView":Z
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->isFestivalMode()Z

    move-result v3

    if-ne v3, v5, :cond_0

    .line 254
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;

    instance-of v3, v3, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    if-eqz v3, :cond_0

    .line 255
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;

    check-cast v3, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 256
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v1, :cond_0

    .line 257
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v3

    if-gtz v3, :cond_0

    .line 258
    const/4 v2, 0x0

    .line 263
    .end local v1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    if-ne v2, v5, :cond_1

    .line 264
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->startPreviousViewState()V

    .line 268
    .end local v2    # "toPreviousView":Z
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onResume()V

    .line 270
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v3

    sget-object v4, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v3, v4, :cond_2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v3

    sget-object v4, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v3, v4, :cond_2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v3

    sget-object v4, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v3, v4, :cond_5

    .line 273
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v4, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v4, v5}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 279
    :goto_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mEmptySetDrawer:Lcom/sec/android/gallery3d/ui/EmptySetDrawer;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->setLayout()V

    .line 280
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v3, :cond_4

    .line 281
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    const v4, 0x7f0e0174

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->resume(I)V

    .line 282
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v3

    sget-object v4, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v3, v4, :cond_3

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v3

    sget-object v4, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v3, v4, :cond_4

    .line 284
    :cond_3
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->isDrawableHideMode()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getActionBarView()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v3

    instance-of v3, v3, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    if-eqz v3, :cond_4

    .line 285
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getActionBarView()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForPick;->hideDrawer()V

    .line 289
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->registerEAM(Landroid/content/BroadcastReceiver;)V

    .line 290
    const-string v3, "Gallery_Performance"

    const-string v4, "NoItemViewState onResume End"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    return-void

    .line 275
    :cond_5
    new-instance v0, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v3}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 276
    .local v0, "actionbar":Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3, v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 277
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mTopSetPath:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/noitemview/NoItemActionBarForNormal;->setTopSetPath(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStateResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "request"    # I
    .param p2, "result"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 367
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 368
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    .line 369
    const-string v6, "crop"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    const/4 v6, 0x1

    :goto_0
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mNeedsCrop:Z

    .line 370
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 391
    :cond_1
    :goto_1
    return-void

    .line 369
    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    .line 373
    :sswitch_0
    invoke-direct {p0, p3, p1}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->handleResultCameraLaunchForSinglePick(Landroid/content/Intent;I)V

    goto :goto_1

    .line 377
    :sswitch_1
    invoke-direct {p0, p3, p2}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->handleResultWallpaper(Landroid/content/Intent;I)V

    goto :goto_1

    .line 380
    :sswitch_2
    if-eqz p3, :cond_1

    .line 381
    const-string v6, "selectedItems"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    .line 382
    .local v5, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v2

    .line 383
    .local v2, "mDataManager":Lcom/sec/android/gallery3d/data/DataManager;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v3

    .line 384
    .local v3, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    .line 385
    .local v4, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    invoke-virtual {v2, v4, v6}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_2

    .line 387
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_3
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/EventAlbumManager;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6, v7, v3}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->prepareCreateEventDialog(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    goto :goto_1

    .line 370
    :sswitch_data_0
    .sparse-switch
        0x403 -> :sswitch_1
        0x701 -> :sswitch_2
        0x902 -> :sswitch_0
        0x903 -> :sswitch_0
    .end sparse-switch
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 5
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 343
    move-object v0, p2

    check-cast v0, Lcom/sec/samsung/gallery/core/Event;

    .line 344
    .local v0, "ev":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v1

    .line 345
    .local v1, "eventType":I
    sget-object v2, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Event update ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_NEW_EVENT_ALBUM:I

    if-ne v1, v2, :cond_1

    .line 348
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->handleNewEventAlbum()V

    .line 352
    :cond_0
    :goto_0
    return-void

    .line 349
    :cond_1
    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_HIDDEN_ALBUMS:I

    if-ne v1, v2, :cond_0

    .line 350
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->handleHiddenAlbumsLaunch()V

    goto :goto_0
.end method
