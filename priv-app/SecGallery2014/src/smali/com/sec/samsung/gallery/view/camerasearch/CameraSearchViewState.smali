.class public Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;
.super Lcom/sec/android/gallery3d/app/ActivityState;
.source "CameraSearchViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;


# static fields
.field private static final THUMB_PHOTO_CACHE_SIZE:I = 0x40


# instance fields
.field private mBgImageIndex:I

.field private mCurrentTopSetPath:Ljava/lang/String;

.field private mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

.field protected mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

.field private mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

.field private mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

.field private mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mScrolledPosition:I

.field private mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

.field private mSearchView:Lcom/sec/samsung/gallery/glview/GlSearchView;

.field private mShowProgressDialog:Z

.field private mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ActivityState;-><init>()V

    .line 49
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    .line 51
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlSearchView;

    .line 53
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mCurrentTopSetPath:Ljava/lang/String;

    .line 59
    iput v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mBgImageIndex:I

    .line 61
    iput v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mScrolledPosition:I

    .line 67
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mShowProgressDialog:Z

    .line 95
    new-instance v0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$1;-><init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

    .line 240
    new-instance v0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$8;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$8;-><init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mShowProgressDialog:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mShowProgressDialog:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;)Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;
    .param p1, "x1"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->startDetailViewState(I)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mBgImageIndex:I

    return v0
.end method

.method static synthetic access$502(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mBgImageIndex:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/samsung/gallery/glview/GlSearchView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlSearchView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method private getTopSetPathByFilter()Ljava/lang/String;
    .locals 3

    .prologue
    .line 377
    const-string v0, ""

    .line 378
    .local v0, "basePath":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    .line 379
    .local v1, "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 380
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    .line 382
    :cond_0
    const/16 v2, 0x800

    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/app/FilterUtils;->newClusterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private startDetailViewState(I)V
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 386
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 387
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    invoke-virtual {v3, p1}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    .line 389
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v1, :cond_0

    .line 406
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :goto_0
    return-void

    .line 392
    .restart local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    instance-of v3, v1, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v3, :cond_1

    .line 394
    check-cast v1, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/LocalImage;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    .line 396
    .local v2, "path":Ljava/lang/String;
    const-string v3, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    .end local v2    # "path":Ljava/lang/String;
    :goto_1
    const-string v3, "KEY_MEDIA_SET_PATH"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getAlbumPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    const-string v3, "KEY_ITEM_POSITION"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 405
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    const-class v4, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0

    .line 399
    .restart local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    const-string v3, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method checkSearchResult()V
    .locals 2

    .prologue
    .line 409
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 410
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0e006d

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 412
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 327
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 328
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    .line 329
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    const/16 v3, 0x40

    .line 118
    new-instance v1, Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2, p0}, Lcom/sec/samsung/gallery/view/ActionBarManager;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/app/ActivityState;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 119
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 120
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getPhotoThumbnailProxy()Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    .line 121
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->getCacheSize()I

    move-result v1

    if-eq v1, v3, :cond_0

    .line 122
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setCacheSize(I)V

    .line 123
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resetContentWindow()V

    .line 124
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0f00bf

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 125
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->getTopSetPathByFilter()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mCurrentTopSetPath:Ljava/lang/String;

    .line 126
    new-instance v1, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mCurrentTopSetPath:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    .line 127
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getSuggestionNames()Ljava/util/List;

    move-result-object v0

    .line 128
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 129
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mShowProgressDialog:Z

    .line 131
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->setLoadingListener(Lcom/sec/android/gallery3d/app/LoadingListener;)V

    .line 132
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 305
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 306
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setSearchText(Ljava/lang/String;)V

    .line 301
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 315
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_1

    .line 316
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 317
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 318
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    .line 322
    :cond_0
    :goto_0
    return-void

    .line 319
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0f0284

    if-ne v0, v1, :cond_0

    .line 320
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->goToHome(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->pause()V

    .line 292
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->onPause()V

    .line 293
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlSearchView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->getScrollPosition()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mScrolledPosition:I

    .line 294
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlSearchView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->pause()V

    .line 295
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 296
    return-void
.end method

.method public onPrepareOptionMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 310
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 311
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 277
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V

    .line 278
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resume()V

    .line 279
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setVisibility(I)V

    .line 280
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 282
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlSearchView;

    .line 283
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mScrolledPosition:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setScrollPosition(I)V

    .line 284
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlSearchView;

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->attachLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;)V

    .line 286
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->onResume()V

    .line 287
    return-void
.end method

.method public onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 6
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    const/4 v5, 0x2

    .line 136
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V

    .line 137
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlSearchView;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v3, "searchColWide"

    const/4 v4, 0x4

    invoke-static {v2, v3, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setAttrs(II)V

    .line 139
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v2, "searchColPort"

    const/4 v3, 0x3

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v5, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setAttrs(II)V

    .line 141
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setOnFocusChangedListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;)V

    .line 142
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlSearchView;

    new-instance v1, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$2;-><init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setOnScrollListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;)V

    .line 165
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlSearchView;

    new-instance v1, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$3;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$3;-><init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setOnItemClickListenerEx(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListenerEx;)V

    .line 183
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlSearchView;

    new-instance v1, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$4;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$4;-><init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setOnItemClickListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;)V

    .line 190
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlSearchView;

    const/4 v1, 0x0

    new-instance v2, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$5;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$5;-><init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setOnGenericMotionListener(ILcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;)V

    .line 207
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlSearchView;

    new-instance v1, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$6;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$6;-><init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)V

    invoke-virtual {v0, v5, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setOnGenericMotionListener(ILcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;)V

    .line 223
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlSearchView;

    new-instance v1, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$7;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$7;-><init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setOnStatusChangedListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;)V

    .line 238
    return-void
.end method

.method public setItemFocus(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x0

    .line 254
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getGenericMotionFocus()I

    move-result v0

    .line 255
    .local v0, "genericMotionFocus":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->setGenericMotionFocus(I)V

    .line 256
    if-ltz v0, :cond_0

    .line 257
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    invoke-virtual {v1, v0, v2}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->notifyDataSetChanged(II)V

    .line 259
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    invoke-virtual {v1, p1, v2}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->notifyDataSetChanged(II)V

    .line 260
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->setGenericMotionKeywordFocus(I)V

    .line 261
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->notifyDataSetChanged(II)V

    .line 262
    return-void
.end method

.method public setKeywordFocus(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x1

    .line 265
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getGenericMotionKeywordFocus()I

    move-result v0

    .line 266
    .local v0, "genericMotionFocus":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->setGenericMotionKeywordFocus(I)V

    .line 267
    if-ltz v0, :cond_0

    .line 268
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    invoke-virtual {v1, v0, v2}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->notifyDataSetChanged(II)V

    .line 270
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    invoke-virtual {v1, p1, v2}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->notifyDataSetChanged(II)V

    .line 271
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->setGenericMotionFocus(I)V

    .line 272
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->notifyDataSetChanged(II)V

    .line 273
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 13
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 333
    move-object v1, p2

    check-cast v1, Lcom/sec/samsung/gallery/core/Event;

    .line 334
    .local v1, "ev":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v2

    .line 335
    .local v2, "eventType":I
    sget v9, Lcom/sec/samsung/gallery/core/Event;->EVENT_PRESS_SEARCH:I

    if-ne v2, v9, :cond_1

    .line 336
    iput-boolean v11, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mShowProgressDialog:Z

    .line 337
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/core/Event;->getData()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 338
    .local v5, "key":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    invoke-virtual {v9, v5}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->addKeyword(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 339
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->checkSearchResult()V

    .line 361
    .end local v5    # "key":Ljava/lang/String;
    :cond_0
    return-void

    .line 341
    :cond_1
    sget v9, Lcom/sec/samsung/gallery/core/Event;->EVENT_ADD_SUGGESTION:I

    if-ne v2, v9, :cond_0

    .line 342
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/core/Event;->getData()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/Object;

    move-object v6, v9

    check-cast v6, [Ljava/lang/Object;

    .line 343
    .local v6, "params":[Ljava/lang/Object;
    aget-object v0, v6, v11

    check-cast v0, Landroid/database/MatrixCursor;

    .line 344
    .local v0, "cursor":Landroid/database/MatrixCursor;
    aget-object v3, v6, v12

    check-cast v3, Ljava/lang/String;

    .line 346
    .local v3, "filterText":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getSuggestionNames()Ljava/util/List;

    move-result-object v8

    .line 347
    .local v8, "suggestionNameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    if-ge v4, v9, :cond_0

    .line 348
    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 349
    .local v7, "suggestionName":Ljava/lang/String;
    if-eqz v3, :cond_2

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 351
    :cond_2
    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    aput-object v7, v9, v12

    invoke-virtual {v0, v9}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 347
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method
