.class Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;
.super Ljava/lang/Object;
.source "GlComposeView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlPenSelectListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 0

    .prologue
    .line 1309
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isReachedBoundary()I
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, -0x1

    .line 1366
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->getReachedBoundary()I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 1372
    :goto_0
    return v0

    .line 1369
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->getReachedBoundary()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    .line 1370
    goto :goto_0

    .line 1372
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPenEnter(Landroid/graphics/PointF;)Z
    .locals 4
    .param p1, "point"    # Landroid/graphics/PointF;

    .prologue
    .line 1313
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;

    if-eqz v0, :cond_1

    .line 1314
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->initAttribute()V

    .line 1316
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v0, :cond_0

    .line 1317
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->setAdapter(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;)V

    .line 1320
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnPenSelectionListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->setPenSelectionListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;)V

    .line 1321
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->setposCtrl(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;)V

    .line 1323
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->setStartPoint(Landroid/graphics/PointF;)V

    .line 1324
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getFirstVisiblePosition()I

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getLastVisiblePosition()I

    move-result v3

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->searchObject(Landroid/graphics/PointF;Landroid/graphics/Rect;II)Z

    .line 1326
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public onPenMove(Landroid/graphics/Rect;Z)Z
    .locals 3
    .param p1, "r"    # Landroid/graphics/Rect;
    .param p2, "listScrollActive"    # Z

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1352
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPickMode(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiPickMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mUsePenSelectInPickMode:Z

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 1361
    :goto_0
    return v0

    .line 1355
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mIsDisplayedPenSelectionBox:Z
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$1500(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1356
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$700(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setExpansionMode(Z)V

    .line 1357
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # setter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mIsDisplayedPenSelectionBox:Z
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$1502(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;Z)Z

    .line 1358
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->requestStart(Landroid/graphics/Rect;)V

    .line 1360
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectionBox:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectionBox;->changeBackgroundSize(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public onPenSelect(Landroid/graphics/PointF;Landroid/graphics/Rect;)Z
    .locals 4
    .param p1, "point"    # Landroid/graphics/PointF;
    .param p2, "r"    # Landroid/graphics/Rect;

    .prologue
    const/16 v3, 0x8

    const/4 v0, 0x0

    .line 1331
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiPickMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mUsePenSelectInPickMode:Z

    if-nez v1, :cond_0

    .line 1347
    :goto_0
    return v0

    .line 1334
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/glcore/GlHandler;->hasMessage(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1335
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/glcore/GlHandler;->removeMessage(I)V

    .line 1337
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->isEnabledQuickScroll()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1338
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v2, 0x3

    invoke-virtual {v1, v3, v2, v0, v0}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 1341
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;

    if-eqz v1, :cond_3

    .line 1342
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->setEndPoint(Landroid/graphics/PointF;)V

    .line 1343
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->setDirection()V

    .line 1344
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPenSelectCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getFirstVisiblePosition()I

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getLastVisiblePosition()I

    move-result v3

    invoke-virtual {v1, p1, p2, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->searchObject(Landroid/graphics/PointF;Landroid/graphics/Rect;II)Z

    .line 1346
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$14;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/16 v2, 0xf

    invoke-virtual {v1, v2, v0, v0, v0}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 1347
    const/4 v0, 0x1

    goto :goto_0
.end method
