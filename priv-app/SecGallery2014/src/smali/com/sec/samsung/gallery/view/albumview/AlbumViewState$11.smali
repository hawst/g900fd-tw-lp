.class Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$11;
.super Ljava/lang/Object;
.source "AlbumViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V
    .locals 0

    .prologue
    .line 647
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$11;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHoverClick(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;IILjava/lang/Object;)Z
    .locals 2
    .param p1, "parentView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;
    .param p2, "albumIndex"    # I
    .param p3, "itemIndex"    # I
    .param p4, "media"    # Ljava/lang/Object;

    .prologue
    .line 650
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$11;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$2700(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    .line 651
    .local v0, "launchMode":Lcom/sec/samsung/gallery/core/LaunchModeType;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$11;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->latestSelectedAlbuminfo(I)V
    invoke-static {v1, p2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$3300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;I)V

    .line 652
    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_NORMAL:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_0

    .line 653
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$11;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mAlbumEventHandle:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$3100(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    move-result-object v1

    check-cast p4, Lcom/sec/android/gallery3d/data/MediaObject;

    .end local p4    # "media":Ljava/lang/Object;
    invoke-virtual {v1, p4}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->onHoverClick(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 657
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 655
    .restart local p4    # "media":Ljava/lang/Object;
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$11;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    check-cast p4, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local p4    # "media":Ljava/lang/Object;
    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->startDetailView(IILcom/sec/android/gallery3d/data/MediaItem;)V
    invoke-static {v1, p2, p3, p4}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$5200(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;IILcom/sec/android/gallery3d/data/MediaItem;)V

    goto :goto_0
.end method
