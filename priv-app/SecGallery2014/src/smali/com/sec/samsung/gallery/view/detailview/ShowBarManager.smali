.class public Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;
.super Ljava/lang/Object;
.source "ShowBarManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;,
        Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;,
        Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowbarUpdateListener;
    }
.end annotation


# static fields
.field private static final AUTO:I = 0x0

.field private static final OFF:I = 0x2

.field private static final ON:I = 0x1

.field private static final SHOWBAR_INFO:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mInfo:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;

.field private mInitialFilmRender:Z

.field private mListener:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowbarUpdateListener;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x2

    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->SHOWBAR_INFO:Ljava/util/HashMap;

    .line 46
    sget-object v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->SHOWBAR_INFO:Ljava/util/HashMap;

    sget v1, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_DEFAULT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;

    invoke-direct {v2, v5, v5, v5}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;-><init>(IIZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->SHOWBAR_INFO:Ljava/util/HashMap;

    sget v1, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_COVER:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;

    invoke-direct {v2, v3, v3}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->SHOWBAR_INFO:Ljava/util/HashMap;

    sget v1, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_MOTION_HELP:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;

    invoke-direct {v2, v3, v3}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->SHOWBAR_INFO:Ljava/util/HashMap;

    sget v1, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_MOTION_PREVIEW:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;

    invoke-direct {v2, v4, v3}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->SHOWBAR_INFO:Ljava/util/HashMap;

    sget v1, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_MOTION_TUTORIAL:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;

    invoke-direct {v2, v4, v3}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->SHOWBAR_INFO:Ljava/util/HashMap;

    sget v1, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_MOREINFO:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;

    invoke-direct {v2, v4, v3}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->SHOWBAR_INFO:Ljava/util/HashMap;

    sget v1, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_SLIDESHOW_SETTING:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;

    invoke-direct {v2, v4, v3}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    return-void
.end method

.method public constructor <init>(Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowbarUpdateListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowbarUpdateListener;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->mInitialFilmRender:Z

    .line 60
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->mListener:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowbarUpdateListener;

    .line 61
    sget-object v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->SHOWBAR_INFO:Ljava/util/HashMap;

    sget v1, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_DEFAULT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->mInfo:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;

    .line 62
    return-void
.end method

.method private updateShowBar()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->mListener:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowbarUpdateListener;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->mListener:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowbarUpdateListener;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowbarUpdateListener;->updateShowBar()V

    .line 108
    :cond_0
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->mListener:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowbarUpdateListener;

    .line 102
    return-void
.end method

.method public enableActioBarShow()Z
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->mInfo:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;

    iget v0, v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;->showActionBar:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public enableFilmShow()Z
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->mInfo:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;

    iget v0, v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;->showFilm:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->mInitialFilmRender:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public enableShowBar()Z
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->enableActioBarShow()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->enableFilmShow()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActionBarShowAuto()Z
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->mInfo:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;

    iget v0, v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;->showActionBar:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActionBarShowOn()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 93
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->mInfo:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;

    iget v1, v1, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;->showActionBar:I

    if-ne v1, v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->isDisabledAutoHide()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDisabledAutoHide()Z
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->mInfo:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;->disableAutoHide:Z

    return v0
.end method

.method public isFilmShowAuto()Z
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->mInfo:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;

    iget v0, v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;->showFilm:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFilmShowOn()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 97
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->mInfo:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;

    iget v1, v1, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;->showFilm:I

    if-ne v1, v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->isDisabledAutoHide()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setInitialFilmRender(Z)V
    .locals 0
    .param p1, "show"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->mInitialFilmRender:Z

    .line 66
    return-void
.end method

.method public setShowBarMode(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 111
    sget-object v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->SHOWBAR_INFO:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->mInfo:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;

    .line 112
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->mInfo:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;

    if-nez v0, :cond_0

    .line 113
    sget-object v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->SHOWBAR_INFO:Ljava/util/HashMap;

    sget v1, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_DEFAULT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->mInfo:Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$ShowBarInfo;

    .line 115
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;->updateShowBar()V

    .line 116
    return-void
.end method
