.class public Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;
.super Landroid/widget/FrameLayout;
.source "CheckableLayout.java"

# interfaces
.implements Landroid/widget/Checkable;


# static fields
.field private static final CHECKED_STATE_SET:[I


# instance fields
.field private mChecked:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;->CHECKED_STATE_SET:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;->mChecked:Z

    .line 33
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;->init()V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;->mChecked:Z

    .line 38
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;->init()V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;->mChecked:Z

    .line 43
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;->init()V

    .line 44
    return-void
.end method

.method private init()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 47
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;->setClickable(Z)V

    .line 48
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;->setFocusable(Z)V

    .line 49
    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;->mChecked:Z

    return v0
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2
    .param p1, "extraSpace"    # I

    .prologue
    .line 53
    add-int/lit8 v1, p1, 0x1

    invoke-super {p0, v1}, Landroid/widget/FrameLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 55
    .local v0, "drawableState":[I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    sget-object v1, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;->CHECKED_STATE_SET:[I

    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;->mergeDrawableStates([I[I)[I

    .line 58
    :cond_0
    return-object v0
.end method

.method public setChecked(Z)V
    .locals 0
    .param p1, "checked"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;->mChecked:Z

    .line 69
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;->refreshDrawableState()V

    .line 70
    return-void
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;->mChecked:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;->mChecked:Z

    .line 75
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/CheckableLayout;->refreshDrawableState()V

    .line 76
    return-void

    .line 74
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
