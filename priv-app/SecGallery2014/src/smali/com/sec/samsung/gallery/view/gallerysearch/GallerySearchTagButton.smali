.class public Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;
.super Landroid/widget/TextView;
.source "GallerySearchTagButton.java"


# instance fields
.field private mData:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

.field private mPadding:I

.field private mRightMargin:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->mData:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "tagData"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->mData:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 37
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->mData:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 38
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->init()V

    .line 39
    return-void
.end method

.method public static getName(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "DCM_type"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 144
    const-string v3, "profile/Me"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 145
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e00c0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 158
    .end local p0    # "DCM_type":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 147
    .restart local p0    # "DCM_type":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    .line 148
    .local v1, "resId":Ljava/lang/Integer;
    sget-object v3, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->CATEGORY_STRING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, p0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 149
    .local v0, "mainResID":Ljava/lang/Integer;
    sget-object v3, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->SUB_CATEGORY_STRING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, p0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 150
    .local v2, "subResID":Ljava/lang/Integer;
    if-nez v0, :cond_2

    if-eqz v2, :cond_0

    .line 153
    :cond_2
    if-nez v0, :cond_3

    .line 154
    move-object v1, v2

    .line 158
    :goto_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 156
    :cond_3
    move-object v1, v0

    goto :goto_1
.end method

.method private init()V
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 43
    const/4 v1, 0x0

    .line 46
    .local v1, "isSelected":Z
    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setClickable(Z)V

    .line 47
    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setFocusable(Z)V

    .line 49
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->mData:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    if-eqz v4, :cond_0

    .line 50
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->mData:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isSelected()Z

    move-result v1

    .line 51
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->mData:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->getName(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setText(Ljava/lang/CharSequence;)V

    .line 53
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setSelected(Z)V

    .line 54
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setSingleLine()V

    .line 55
    const/16 v4, 0x11

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setGravity(I)V

    .line 56
    const/high16 v4, 0x41a00000    # 20.0f

    invoke-virtual {p0, v7, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setTextSize(IF)V

    .line 57
    invoke-static {}, Lcom/sec/samsung/gallery/util/FontUtil;->getRobotoLightFont()Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setTypeface(Landroid/graphics/Typeface;)V

    .line 59
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGallerySearchUISeperatedMode:Z

    if-eqz v4, :cond_1

    .line 60
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b007a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 61
    const v4, 0x7f020470

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setBackgroundResource(I)V

    .line 67
    :goto_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0079

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->mPadding:I

    .line 68
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d007c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 69
    .local v3, "verticalPadding":I
    iget v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->mPadding:I

    iget v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->mPadding:I

    invoke-virtual {p0, v4, v3, v5, v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setPadding(IIII)V

    .line 71
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d007a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->mRightMargin:I

    .line 72
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d007b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 73
    .local v0, "buttonHeight":I
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x2

    invoke-direct {v4, v5, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-direct {v2, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/widget/LinearLayout$LayoutParams;)V

    .line 74
    .local v2, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->mRightMargin:I

    invoke-virtual {v2, v6, v6, v4, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 75
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 76
    return-void

    .line 63
    .end local v0    # "buttonHeight":I
    .end local v2    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v3    # "verticalPadding":I
    :cond_1
    const/4 v4, -0x1

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setTextColor(I)V

    .line 64
    const v4, 0x7f02046f

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setBackgroundResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public getExpectedWidth()I
    .locals 3

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->getWidth()I

    move-result v0

    .line 88
    .local v0, "ExpectedWidth":I
    if-nez v0, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    .line 91
    :cond_0
    iget v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->mPadding:I

    mul-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->mRightMargin:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 92
    return v0
.end method

.method public getTagData()Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->mData:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    return-object v0
.end method

.method public playSelectAnimation(Z)V
    .locals 6
    .param p1, "isSeleted"    # Z

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0039

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 108
    .local v1, "colorFrom":Ljava/lang/Integer;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b003a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 109
    .local v2, "colorTo":Ljava/lang/Integer;
    new-instance v3, Landroid/animation/ArgbEvaluator;

    invoke-direct {v3}, Landroid/animation/ArgbEvaluator;-><init>()V

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v5, 0x1

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Landroid/animation/ValueAnimator;->ofObject(Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 110
    .local v0, "bgAnimator":Landroid/animation/ValueAnimator;
    new-instance v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton$1;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton$1;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;)V

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 117
    new-instance v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton$2;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton$2;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;)V

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 139
    const-wide/16 v4, 0x1f4

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 140
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 141
    return-void
.end method

.method public setButtonEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    const/4 v1, 0x0

    .line 96
    if-eqz p1, :cond_0

    .line 97
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setEnabled(Z)V

    .line 98
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setSelected(Z)V

    .line 103
    :goto_0
    return-void

    .line 100
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setEnabled(Z)V

    .line 101
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setSelected(Z)V

    goto :goto_0
.end method

.method public setTagData(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)V
    .locals 0
    .param p1, "data"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->mData:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 84
    return-void
.end method
