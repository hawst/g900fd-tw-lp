.class Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$2;
.super Ljava/lang/Object;
.source "HelpInfoPreviewState.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 194
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    iget v6, v6, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHelpMode:I

    const/4 v7, 0x3

    if-eq v6, v7, :cond_1

    .line 238
    :cond_0
    :goto_0
    :pswitch_0
    return v4

    .line 197
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 198
    .local v0, "action":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->access$100(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getPenHovering(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p2, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    if-eq v6, v8, :cond_3

    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->access$200(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getFingerAirView(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {p2, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    if-ne v6, v4, :cond_0

    .line 200
    :cond_3
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->access$300(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getPenHoveringInformationPreview(Landroid/content/Context;)Z

    move-result v3

    .line 202
    .local v3, "isPenHoverInfoPreviewOn":Z
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->access$400(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getFingerAirViewInformationPreview(Landroid/content/Context;)Z

    move-result v2

    .line 205
    .local v2, "isFingerAirViewInformationPreviewOn":Z
    if-nez v3, :cond_4

    invoke-virtual {p2, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    if-ne v6, v8, :cond_4

    move v4, v5

    .line 206
    goto :goto_0

    .line 207
    :cond_4
    if-nez v2, :cond_5

    invoke-virtual {p2, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    if-ne v6, v4, :cond_5

    move v4, v5

    .line 208
    goto :goto_0

    .line 210
    :cond_5
    packed-switch v0, :pswitch_data_0

    :pswitch_1
    goto :goto_0

    .line 217
    :pswitch_2
    :try_start_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    iget-boolean v5, v5, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverModeOn:Z

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    iget v5, v5, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mViewMode:I

    const/4 v6, 0x7

    if-ne v5, v6, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    iget-wide v8, v5, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverEnterTime:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x190

    cmp-long v5, v6, v8

    if-ltz v5, :cond_0

    .line 219
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->soundAndVibrator()V

    .line 220
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    const/16 v6, 0x8

    iput v6, v5, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mViewMode:I

    .line 221
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->setHelpView()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 234
    :catch_0
    move-exception v1

    .line 235
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 212
    .end local v1    # "e":Landroid/os/RemoteException;
    :pswitch_3
    :try_start_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, v5, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverEnterTime:J

    .line 213
    const/16 v5, 0xa

    const/4 v6, -0x1

    invoke-static {v5, v6}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V

    .line 214
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    const/4 v6, 0x1

    iput-boolean v6, v5, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverModeOn:Z

    goto/16 :goto_0

    .line 225
    :pswitch_4
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    const/4 v6, 0x0

    iput-boolean v6, v5, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mHoverModeOn:Z

    .line 226
    const/4 v5, 0x1

    const/4 v6, -0x1

    invoke-static {v5, v6}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 210
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
