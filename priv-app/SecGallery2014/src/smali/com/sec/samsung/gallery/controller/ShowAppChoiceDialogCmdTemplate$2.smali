.class Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$2;
.super Ljava/lang/Object;
.source "ShowAppChoiceDialogCmdTemplate.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->handleMessageOnUiThread(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;

.field final synthetic val$msg:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;I)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$2;->this$0:Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;

    iput p2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$2;->val$msg:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 91
    iget v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$2;->val$msg:I

    packed-switch v2, :pswitch_data_0

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 93
    :pswitch_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$2;->this$0:Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->access$100(Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$2;->this$0:Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->access$100(Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->isShowing()Z

    move-result v2

    if-nez v2, :cond_0

    .line 95
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$2;->this$0:Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;

    iget-object v2, v2, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SHRE"

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$2;->this$0:Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;

    iget-object v4, v4, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mContext:Landroid/content/Context;

    invoke-static {v2, v3, v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 96
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$2;->this$0:Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->createAppList()Ljava/util/List;

    move-result-object v0

    .line 97
    .local v0, "shareAppList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$2;->this$0:Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;

    # invokes: Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->showAppChoiceDialog(Ljava/util/List;)V
    invoke-static {v2, v0}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->access$200(Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;Ljava/util/List;)V

    goto :goto_0

    .line 100
    .end local v0    # "shareAppList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :pswitch_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$2;->this$0:Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->access$100(Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 101
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$2;->this$0:Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->access$100(Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->dismissDialog()V

    goto :goto_0

    .line 105
    :pswitch_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$2;->this$0:Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->access$100(Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 106
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$2;->this$0:Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->createAppList()Ljava/util/List;

    move-result-object v1

    .line 107
    .local v1, "shareAppLst":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$2;->this$0:Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->access$100(Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->setAppList(Ljava/util/List;)V

    .line 108
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$2;->this$0:Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->access$100(Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->notifyDataSetChanged()V

    goto :goto_0

    .line 91
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
