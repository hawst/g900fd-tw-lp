.class public Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;
.super Lcom/sec/samsung/gallery/dialog/GalleryDialog;
.source "ContentToDisplayDialogForCloudAlbum.java"


# instance fields
.field contentAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private contentPopupListLayout:Landroid/view/View;

.field formatAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mContentListView:Landroid/widget/ListView;

.field private mContext:Landroid/content/Context;

.field private mCurrentViewType:I

.field private mFormatListView:Landroid/widget/ListView;

.field private mIsCloudDisplayFormat:Z

.field private mIsDropBoxAccountActive:Z

.field private mIsPicasaAccountActive:Z

.field private mIsSNSAccountActive:Z

.field private mIsTCloudAccountActive:Z

.field private mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field private mValidContentOptions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/core/ViewByFilterType;",
            ">;"
        }
    .end annotation
.end field

.field private mValidFormatOptions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/core/ViewByFilterType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 71
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/dialog/GalleryDialog;-><init>(Landroid/content/Context;)V

    .line 44
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mIsDropBoxAccountActive:Z

    .line 46
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mIsSNSAccountActive:Z

    .line 48
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mIsPicasaAccountActive:Z

    .line 50
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mIsTCloudAccountActive:Z

    .line 52
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mIsCloudDisplayFormat:Z

    .line 54
    iput v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I

    .line 64
    iput-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContentListView:Landroid/widget/ListView;

    .line 65
    iput-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mFormatListView:Landroid/widget/ListView;

    .line 72
    iput-object p1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;

    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 74
    invoke-direct {p0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->UpdateDisplayTypes()V

    .line 75
    return-void
.end method

.method private UpdateDisplayTypes()V
    .locals 2

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->updateActivedViewByTypes()V

    .line 120
    invoke-static {}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getValidOptions()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidContentOptions:Ljava/util/ArrayList;

    .line 121
    invoke-static {}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getValidFormatOptions()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidFormatOptions:Ljava/util/ArrayList;

    .line 122
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->isCloudSupported(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidContentOptions:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->CLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 125
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSettings:Z

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidContentOptions:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->FACEBOOK:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 127
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidContentOptions:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->PICASA:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 129
    :cond_1
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloudMandatory:Z

    if-nez v0, :cond_2

    .line 130
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidContentOptions:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->TCLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 132
    :cond_2
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_3

    .line 133
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidContentOptions:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->FACEBOOK:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 135
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I

    .line 137
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I

    return v0
.end method

.method static synthetic access$072(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I

    and-int/2addr v0, p1

    iput v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I

    return v0
.end method

.method static synthetic access$076(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Lcom/sec/android/gallery3d/app/StateManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidContentOptions:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;
    .param p1, "x1"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->isViewByOptionEnabled(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContentListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->updateSaveButton()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mFormatListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidFormatOptions:Ljava/util/ArrayList;

    return-object v0
.end method

.method private createViewtypeDialog()V
    .locals 14

    .prologue
    const v3, 0x7f0300ca

    const v4, 0x1020014

    .line 173
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v13

    .line 174
    .local v13, "inflater":Landroid/view/LayoutInflater;
    const v0, 0x7f03003a

    const/4 v1, 0x0

    invoke-virtual {v13, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    .line 175
    .local v12, "contentPopupList":Landroid/view/View;
    const v0, 0x7f0f009d

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContentListView:Landroid/widget/ListView;

    .line 176
    const v0, 0x7f0f009f

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mFormatListView:Landroid/widget/ListView;

    .line 178
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidContentOptions:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getViewByDisplayOptions(Landroid/content/Context;Ljava/util/ArrayList;)[Ljava/lang/String;

    move-result-object v5

    .line 179
    .local v5, "contentOptions":[Ljava/lang/String;
    new-instance v0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;-><init>(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;Landroid/content/Context;II[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->contentAdapter:Landroid/widget/ArrayAdapter;

    .line 300
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidFormatOptions:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getViewByDisplayOptions(Landroid/content/Context;Ljava/util/ArrayList;)[Ljava/lang/String;

    move-result-object v11

    .line 302
    .local v11, "formatOptions":[Ljava/lang/String;
    new-instance v6, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;

    iget-object v8, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;

    move-object v7, p0

    move v9, v3

    move v10, v4

    invoke-direct/range {v6 .. v11}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$3;-><init>(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;Landroid/content/Context;II[Ljava/lang/String;)V

    iput-object v6, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->formatAdapter:Landroid/widget/ArrayAdapter;

    .line 373
    const v0, 0x7f0f009b

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->contentPopupListLayout:Landroid/view/View;

    .line 374
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContentListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->contentAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 375
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mFormatListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->formatAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 376
    return-void
.end method

.method private getDialogHeight()I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 379
    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->contentAdapter:Landroid/widget/ArrayAdapter;

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContentListView:Landroid/widget/ListView;

    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 380
    .local v0, "childView":Landroid/view/View;
    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 382
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidContentOptions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    mul-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidFormatOptions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    mul-int/2addr v1, v2

    return v1
.end method

.method private isViewByOptionEnabled(I)Z
    .locals 2
    .param p1, "viewType"    # I

    .prologue
    const/4 v0, 0x1

    .line 101
    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->LOCAL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 115
    :cond_0
    :goto_0
    return v0

    .line 104
    :cond_1
    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->CLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 105
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mIsDropBoxAccountActive:Z

    goto :goto_0

    .line 106
    :cond_2
    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->FACEBOOK:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 107
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mIsSNSAccountActive:Z

    goto :goto_0

    .line 108
    :cond_3
    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->PICASA:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 109
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mIsPicasaAccountActive:Z

    goto :goto_0

    .line 110
    :cond_4
    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->TCLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 111
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mIsTCloudAccountActive:Z

    goto :goto_0

    .line 112
    :cond_5
    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->CLOUDALBUM_FORMAT:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 113
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mIsCloudDisplayFormat:Z

    goto :goto_0
.end method

.method private setupListViewForContentOption(Landroid/widget/ListView;)V
    .locals 1
    .param p1, "listView"    # Landroid/widget/ListView;

    .prologue
    .line 386
    if-nez p1, :cond_0

    .line 463
    :goto_0
    return-void

    .line 389
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$4;

    invoke-direct {v0, p0, p1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$4;-><init>(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;Landroid/widget/ListView;)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto :goto_0
.end method

.method private setupListViewForFormatOption(Landroid/widget/ListView;)V
    .locals 1
    .param p1, "listView"    # Landroid/widget/ListView;

    .prologue
    .line 466
    if-nez p1, :cond_0

    .line 543
    :goto_0
    return-void

    .line 469
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;

    invoke-direct {v0, p0, p1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$5;-><init>(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;Landroid/widget/ListView;)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto :goto_0
.end method

.method private updateActivedViewByTypes()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 82
    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->isCloudSupported(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/cloud/CloudRefer;->isCloudContentSyncOn(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v1

    sget-object v4, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-eq v1, v4, :cond_2

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mIsDropBoxAccountActive:Z

    .line 85
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->isSNSAccountsActive(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/sns/SNSSource;->isSNSDataExist(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v1

    sget-object v4, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-eq v1, v4, :cond_3

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mIsSNSAccountActive:Z

    .line 88
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaApi;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v1, v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->isPicasaDataExist(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v2

    :goto_2
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mIsPicasaAccountActive:Z

    .line 91
    new-instance v0, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;-><init>()V

    .line 92
    .local v0, "refer":Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloud:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;->isTCloudAccoutActivated(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v1

    sget-object v4, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    if-eq v1, v4, :cond_5

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v1

    sget-object v4, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_NORMAL:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v1, v4, :cond_5

    move v1, v2

    :goto_3
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mIsTCloudAccountActive:Z

    .line 97
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mIsDropBoxAccountActive:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mIsSNSAccountActive:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mIsPicasaAccountActive:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mIsTCloudAccountActive:Z

    if-eqz v1, :cond_1

    :cond_0
    move v3, v2

    :cond_1
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mIsCloudDisplayFormat:Z

    .line 98
    return-void

    .end local v0    # "refer":Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    :cond_2
    move v1, v3

    .line 82
    goto :goto_0

    :cond_3
    move v1, v3

    .line 85
    goto :goto_1

    :cond_4
    move v1, v3

    .line 88
    goto :goto_2

    .restart local v0    # "refer":Lcom/sec/android/gallery3d/remote/tCloud/TCloudRefer;
    :cond_5
    move v1, v3

    .line 92
    goto :goto_3
.end method

.method private updateSaveButton()V
    .locals 3

    .prologue
    .line 78
    const/4 v1, -0x1

    iget v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I

    const/16 v2, 0x40

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->setEnableButton(IZ)V

    .line 79
    return-void

    .line 78
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 141
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/dialog/GalleryDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    .line 142
    invoke-direct {p0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->createViewtypeDialog()V

    .line 143
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->contentPopupListLayout:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0e014f

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0e00db

    new-instance v4, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$1;

    invoke-direct {v4, p0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$1;-><init>(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0e0046

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 161
    .local v0, "dialog":Landroid/app/AlertDialog;
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContentListView:Landroid/widget/ListView;

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->setupListViewForContentOption(Landroid/widget/ListView;)V

    .line 162
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mFormatListView:Landroid/widget/ListView;

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->setupListViewForFormatOption(Landroid/widget/ListView;)V

    .line 165
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {p0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->getDialogHeight()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 166
    .local v1, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    add-int/lit8 v2, v2, 0xc

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 167
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContentListView:Landroid/widget/ListView;

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 168
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContentListView:Landroid/widget/ListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setVerticalScrollBarEnabled(Z)V

    .line 169
    return-object v0
.end method
