.class public Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
.super Lcom/sec/samsung/gallery/mapfragment/clustering/BasePoint;
.source "InputPoint.java"


# instance fields
.field info:Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;

.field private tag:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;)V
    .locals 0
    .param p1, "mapPosition"    # Lcom/google/android/gms/maps/model/LatLng;
    .param p2, "info"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/BasePoint;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;->mapPosition:Lcom/google/android/gms/maps/model/LatLng;

    .line 26
    iput-object p2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;->info:Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;

    .line 27
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/maps/model/LatLng;Ljava/lang/Object;)V
    .locals 0
    .param p1, "mapPosition"    # Lcom/google/android/gms/maps/model/LatLng;
    .param p2, "tag"    # Ljava/lang/Object;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/BasePoint;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;->mapPosition:Lcom/google/android/gms/maps/model/LatLng;

    .line 38
    iput-object p2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;->tag:Ljava/lang/Object;

    .line 39
    return-void
.end method


# virtual methods
.method public getInfo()Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;->info:Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;

    return-object v0
.end method

.method public bridge synthetic getMapPosition()Lcom/google/android/gms/maps/model/LatLng;
    .locals 1

    .prologue
    .line 11
    invoke-super {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/BasePoint;->getMapPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    return-object v0
.end method

.method public getTag()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;->tag:Ljava/lang/Object;

    return-object v0
.end method

.method public setInfo(Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;)V
    .locals 0
    .param p1, "info"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;->info:Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;

    .line 54
    return-void
.end method

.method public setTag(Ljava/lang/Object;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/Object;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;->tag:Ljava/lang/Object;

    .line 62
    return-void
.end method
