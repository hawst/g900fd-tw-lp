.class public final Lcom/sec/samsung/gallery/access/dcm/DCMInterface$ImageColumns;
.super Ljava/lang/Object;
.source "DCMInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/access/dcm/DCMInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ImageColumns"
.end annotation


# static fields
.field public static final CALENDAR_EVENT:Ljava/lang/String; = "calendar_event"

.field public static final CREATION_DATE:Ljava/lang/String; = "date"

.field public static final FACE_COUNT:Ljava/lang/String; = "facecount"

.field public static final MIMETYPE:Ljava/lang/String; = "mimetype"

.field public static final NAMED_LOCATION:Ljava/lang/String; = "named_location"

.field public static final PATH:Ljava/lang/String; = "path"

.field public static final PERSON_NAMES:Ljava/lang/String; = "person_names"

.field public static final SCENE_TYPE:Ljava/lang/String; = "scene_type"

.field public static final SUB_CATEGORY_TYPE:Ljava/lang/String; = "subscene_type"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final URI:Ljava/lang/String; = "uri"

.field public static final USER_TAGS:Ljava/lang/String; = "user_tags"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
