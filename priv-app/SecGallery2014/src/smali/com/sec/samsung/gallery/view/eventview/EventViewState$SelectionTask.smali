.class Lcom/sec/samsung/gallery/view/eventview/EventViewState$SelectionTask;
.super Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;
.source "EventViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/eventview/EventViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SelectionTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "onProgressListener"    # Lcom/sec/android/gallery3d/data/OnProgressListener;
    .param p4, "maxCount"    # I

    .prologue
    .line 1226
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$SelectionTask;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .line 1227
    invoke-direct {p0, p2, p3, p4}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;I)V

    .line 1229
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 1225
    check-cast p1, [Lcom/sec/android/gallery3d/data/MediaSet;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$SelectionTask;->doInBackground([Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/Void;
    .locals 3
    .param p1, "arg0"    # [Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 1233
    const/4 v1, 0x0

    aget-object v0, p1, v1

    .line 1234
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$SelectionTask;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$SelectionTask;->mContext:Landroid/content/Context;

    # invokes: Lcom/sec/samsung/gallery/view/eventview/EventViewState;->selectAllProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/eventview/EventViewState$SelectionTask;)V
    invoke-static {v1, v2, v0, p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->access$4200(Lcom/sec/samsung/gallery/view/eventview/EventViewState;Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/eventview/EventViewState$SelectionTask;)V

    .line 1235
    const/4 v1, 0x0

    return-object v1
.end method
