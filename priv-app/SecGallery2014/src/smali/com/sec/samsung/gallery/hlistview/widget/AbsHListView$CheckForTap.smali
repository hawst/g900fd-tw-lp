.class final Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;
.super Ljava/lang/Object;
.source "AbsHListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "CheckForTap"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V
    .locals 0

    .prologue
    .line 2621
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 2625
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget v4, v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    if-nez v4, :cond_2

    .line 2626
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iput v7, v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    .line 2627
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget v5, v5, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionPosition:I

    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget v6, v6, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    sub-int/2addr v5, v6

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2628
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->hasFocusable()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2629
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    const/4 v5, 0x0

    iput v5, v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mLayoutMode:I

    .line 2631
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget-boolean v4, v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataChanged:Z

    if-nez v4, :cond_5

    .line 2632
    invoke-virtual {v0, v7}, Landroid/view/View;->setPressed(Z)V

    .line 2633
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-virtual {v4, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setPressed(Z)V

    .line 2634
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->layoutChildren()V

    .line 2635
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget v5, v5, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mMotionPosition:I

    invoke-virtual {v4, v5, v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->positionSelector(ILandroid/view/View;)V

    .line 2636
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->refreshDrawableState()V

    .line 2638
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v3

    .line 2639
    .local v3, "longPressTimeout":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isLongClickable()Z

    move-result v2

    .line 2641
    .local v2, "longClickable":Z
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_0

    .line 2642
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2643
    .local v1, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_0

    instance-of v4, v1, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v4, :cond_0

    .line 2644
    if-eqz v2, :cond_3

    .line 2645
    check-cast v1, Landroid/graphics/drawable/TransitionDrawable;

    .end local v1    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1, v3}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 2652
    :cond_0
    :goto_0
    if-eqz v2, :cond_4

    .line 2653
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    # getter for: Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingCheckForLongPress:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;
    invoke-static {v4}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->access$500(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;

    move-result-object v4

    if-nez v4, :cond_1

    .line 2654
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    new-instance v5, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;

    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$1;)V

    # setter for: Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingCheckForLongPress:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;
    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->access$502(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;)Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;

    .line 2656
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    # getter for: Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingCheckForLongPress:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;
    invoke-static {v4}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->access$500(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;->rememberWindowAttachCount()V

    .line 2657
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    # getter for: Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mPendingCheckForLongPress:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;
    invoke-static {v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->access$500(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForLongPress;

    move-result-object v5

    int-to-long v6, v3

    invoke-virtual {v4, v5, v6, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2666
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "longClickable":Z
    .end local v3    # "longPressTimeout":I
    :cond_2
    :goto_1
    return-void

    .line 2647
    .restart local v0    # "child":Landroid/view/View;
    .restart local v1    # "d":Landroid/graphics/drawable/Drawable;
    .restart local v2    # "longClickable":Z
    .restart local v3    # "longPressTimeout":I
    :cond_3
    check-cast v1, Landroid/graphics/drawable/TransitionDrawable;

    .end local v1    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    goto :goto_0

    .line 2659
    :cond_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iput v8, v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    goto :goto_1

    .line 2662
    .end local v2    # "longClickable":Z
    .end local v3    # "longPressTimeout":I
    :cond_5
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$CheckForTap;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iput v8, v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mTouchMode:I

    goto :goto_1
.end method
