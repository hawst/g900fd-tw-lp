.class Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionSettings;
.super Ljava/lang/Object;
.source "GlAirMotionDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AirMotionSettings"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMotionType:I

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;Landroid/content/Context;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "motionType"    # I

    .prologue
    .line 187
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionSettings;->this$0:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionSettings;->mContext:Landroid/content/Context;

    .line 189
    iput p3, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionSettings;->mMotionType:I

    .line 190
    return-void
.end method


# virtual methods
.method public checkAirMotionEnabled()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 193
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionSettings;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getAirMotionEngine(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 206
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 196
    :cond_1
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionSettings;->mMotionType:I

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 206
    goto :goto_0

    .line 198
    :pswitch_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionSettings;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getAirMotionScroll(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionSettings;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getAirMotionScrollAlbumAndPhoto(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 201
    :pswitch_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionSettings;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getAirMotionTurn(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionSettings;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getAirMotionTurnSinglePhotoView(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    .line 196
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
