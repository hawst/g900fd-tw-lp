.class public Lcom/sec/samsung/gallery/util/Panorama3DUtil;
.super Ljava/lang/Object;
.source "Panorama3DUtil.java"


# static fields
.field private static final ACTION_VIEW_360_PHOTO:Ljava/lang/String; = "com.samsung.android.intent.action.VIEW_360_PHOTO"

.field public static final COLUMN_SPHERICAL_MOSAIC:Ljava/lang/String; = "spherical_mosaic"

.field private static final CONTENT_URI_360_PHOTO:Ljava/lang/String; = "com.samsung.android.intent.CONTENT_URI_360_PHOTO"

.field private static final MAX_READ_LEN:I = 0x400

.field private static final MORPHO_PANORAMA_DATA_KEY_STR:Ljava/lang/String; = "MPPA"

.field private static final MORPHO_PANORAMA_DATA_KEY_STR_LEN:I = 0x4

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/samsung/gallery/util/Panorama3DUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/util/Panorama3DUtil;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static check3DPanorama(Ljava/io/RandomAccessFile;)Z
    .locals 20
    .param p0, "file"    # Ljava/io/RandomAccessFile;

    .prologue
    .line 63
    const/4 v12, 0x0

    .line 65
    .local v12, "result":Z
    sget-boolean v17, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUse3DPanorama:Z

    if-nez v17, :cond_0

    move v13, v12

    .line 117
    .end local v12    # "result":Z
    .local v13, "result":I
    :goto_0
    return v13

    .line 70
    .end local v13    # "result":I
    .restart local v12    # "result":Z
    :cond_0
    const/16 v17, 0x400

    :try_start_0
    move/from16 v0, v17

    new-array v5, v0, [B

    .line 72
    .local v5, "data":[B
    const-wide/16 v18, 0x2

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 73
    const/4 v11, 0x0

    .line 74
    .local v11, "pos":I
    const/4 v4, 0x0

    .line 75
    .local v4, "app_data_size":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Ljava/io/RandomAccessFile;->read([B)I

    move-result v14

    .line 76
    .local v14, "size":I
    :goto_1
    if-ge v11, v14, :cond_5

    .line 77
    aget-byte v17, v5, v11

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    .line 78
    add-int/lit8 v11, v11, 0x1

    .line 79
    aget-byte v17, v5, v11

    const/16 v18, -0x17

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_4

    .line 81
    add-int/lit8 v11, v11, 0x1

    .line 82
    aget-byte v17, v5, v11

    move/from16 v0, v17

    and-int/lit16 v15, v0, 0xff

    .line 83
    .local v15, "val1":I
    add-int/lit8 v17, v11, 0x1

    aget-byte v17, v5, v17

    move/from16 v0, v17

    and-int/lit16 v0, v0, 0xff

    move/from16 v16, v0

    .line 84
    .local v16, "val2":I
    shl-int/lit8 v17, v15, 0x8

    or-int v4, v17, v16

    .line 85
    add-int/lit8 v11, v11, 0x2

    .line 86
    add-int v17, v11, v4

    add-int/lit8 v17, v17, -0x2

    move/from16 v0, v17

    if-ge v0, v14, :cond_2

    .line 88
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_2
    add-int/lit8 v17, v4, -0x4

    move/from16 v0, v17

    if-ge v7, v0, :cond_2

    .line 89
    const/16 v17, 0x4

    move/from16 v0, v17

    new-array v9, v0, [B

    .line 90
    .local v9, "key_data":[B
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_3
    const/16 v17, 0x4

    move/from16 v0, v17

    if-ge v8, v0, :cond_1

    .line 91
    add-int v17, v11, v8

    aget-byte v17, v5, v17

    aput-byte v17, v9, v8

    .line 90
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 93
    :cond_1
    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v9}, Ljava/lang/String;-><init>([B)V

    .line 94
    .local v10, "key_str":Ljava/lang/String;
    const-string v17, "MPPA"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 95
    const/4 v12, 0x1

    .line 112
    .end local v7    # "i":I
    .end local v8    # "j":I
    .end local v9    # "key_data":[B
    .end local v10    # "key_str":Ljava/lang/String;
    .end local v15    # "val1":I
    .end local v16    # "val2":I
    :cond_2
    :goto_4
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 88
    .restart local v7    # "i":I
    .restart local v8    # "j":I
    .restart local v9    # "key_data":[B
    .restart local v10    # "key_str":Ljava/lang/String;
    .restart local v15    # "val1":I
    .restart local v16    # "val2":I
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 100
    .end local v7    # "i":I
    .end local v8    # "j":I
    .end local v9    # "key_data":[B
    .end local v10    # "key_str":Ljava/lang/String;
    .end local v15    # "val1":I
    .end local v16    # "val2":I
    :cond_4
    aget-byte v17, v5, v11

    const/16 v18, -0x20

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_5

    aget-byte v17, v5, v11

    const/16 v18, -0x11

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_6

    .end local v4    # "app_data_size":I
    .end local v5    # "data":[B
    .end local v11    # "pos":I
    .end local v14    # "size":I
    :cond_5
    :goto_5
    move v13, v12

    .line 117
    .restart local v13    # "result":I
    goto/16 :goto_0

    .line 105
    .end local v13    # "result":I
    .restart local v4    # "app_data_size":I
    .restart local v5    # "data":[B
    .restart local v11    # "pos":I
    .restart local v14    # "size":I
    :cond_6
    add-int/lit8 v11, v11, 0x1

    .line 106
    aget-byte v17, v5, v11

    move/from16 v0, v17

    and-int/lit16 v15, v0, 0xff

    .line 107
    .restart local v15    # "val1":I
    add-int/lit8 v17, v11, 0x1

    aget-byte v17, v5, v17
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move/from16 v0, v17

    and-int/lit16 v0, v0, 0xff

    move/from16 v16, v0

    .line 108
    .restart local v16    # "val2":I
    shl-int/lit8 v17, v15, 0x8

    or-int v4, v17, v16

    .line 109
    add-int/lit8 v17, v4, -0x2

    add-int v11, v11, v17

    goto :goto_4

    .line 114
    .end local v4    # "app_data_size":I
    .end local v5    # "data":[B
    .end local v11    # "pos":I
    .end local v14    # "size":I
    .end local v15    # "val1":I
    .end local v16    # "val2":I
    :catch_0
    move-exception v6

    .line 115
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5
.end method

.method public static check3DPanorama(Ljava/lang/String;)Z
    .locals 6
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 122
    const/4 v3, 0x0

    .line 123
    .local v3, "result":Z
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUse3DPanorama:Z

    if-nez v5, :cond_0

    move v4, v3

    .line 142
    .end local v3    # "result":Z
    .local v4, "result":I
    :goto_0
    return v4

    .line 128
    .end local v4    # "result":I
    .restart local v3    # "result":Z
    :cond_0
    const/4 v1, 0x0

    .line 131
    .local v1, "file":Ljava/io/RandomAccessFile;
    :try_start_0
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v5, "r"

    invoke-direct {v2, p0, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    .end local v1    # "file":Ljava/io/RandomAccessFile;
    .local v2, "file":Ljava/io/RandomAccessFile;
    :try_start_1
    invoke-static {v2}, Lcom/sec/samsung/gallery/util/Panorama3DUtil;->check3DPanorama(Ljava/io/RandomAccessFile;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    .line 136
    invoke-static {v2}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object v1, v2

    .end local v2    # "file":Ljava/io/RandomAccessFile;
    .restart local v1    # "file":Ljava/io/RandomAccessFile;
    :goto_1
    move v4, v3

    .line 142
    .restart local v4    # "result":I
    goto :goto_0

    .line 133
    .end local v4    # "result":I
    :catch_0
    move-exception v0

    .line 134
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 136
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_1

    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v5

    :goto_3
    invoke-static {v1}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v5

    .end local v1    # "file":Ljava/io/RandomAccessFile;
    .restart local v2    # "file":Ljava/io/RandomAccessFile;
    :catchall_1
    move-exception v5

    move-object v1, v2

    .end local v2    # "file":Ljava/io/RandomAccessFile;
    .restart local v1    # "file":Ljava/io/RandomAccessFile;
    goto :goto_3

    .line 133
    .end local v1    # "file":Ljava/io/RandomAccessFile;
    .restart local v2    # "file":Ljava/io/RandomAccessFile;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "file":Ljava/io/RandomAccessFile;
    .restart local v1    # "file":Ljava/io/RandomAccessFile;
    goto :goto_2
.end method

.method public static is3DPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 6
    .param p0, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v0, 0x0

    .line 55
    if-nez p0, :cond_1

    .line 58
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v2

    const-wide v4, 0x4000000000L

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static launch3DPanorama(Landroid/app/Activity;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 34
    sget-object v1, Lcom/sec/samsung/gallery/util/Panorama3DUtil;->TAG:Ljava/lang/String;

    const-string v2, "launch 3d panorama!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 36
    :cond_0
    sget-object v1, Lcom/sec/samsung/gallery/util/Panorama3DUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "no arguments : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-static {v1, v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 52
    :goto_0
    return-void

    .line 40
    :cond_1
    invoke-static {p1}, Lcom/sec/samsung/gallery/util/Panorama3DUtil;->is3DPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->is3DPanorama()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    instance-of v1, p1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-nez v1, :cond_4

    instance-of v1, p1, Lcom/sec/android/gallery3d/data/UriImage;

    if-nez v1, :cond_4

    .line 42
    :cond_3
    sget-object v1, Lcom/sec/samsung/gallery/util/Panorama3DUtil;->TAG:Ljava/lang/String;

    const-string v2, "not 3d panorama item"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 46
    :cond_4
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.intent.action.VIEW_360_PHOTO"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 47
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 50
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    const-string v2, "image/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
