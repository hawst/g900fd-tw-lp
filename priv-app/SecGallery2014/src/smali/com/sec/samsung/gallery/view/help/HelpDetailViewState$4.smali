.class Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$4;
.super Ljava/lang/Object;
.source "HelpDetailViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/UserInteractionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)V
    .locals 0

    .prologue
    .line 363
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUserDobuleTap()V
    .locals 2

    .prologue
    .line 388
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 389
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 390
    :cond_0
    return-void
.end method

.method public onUserFlingUp()V
    .locals 0

    .prologue
    .line 384
    return-void
.end method

.method public onUserInteraction()V
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->showBarsForAShortTime()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$2000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)V

    .line 380
    return-void
.end method

.method public onUserInteractionBegin()V
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    const/4 v1, 0x1

    # setter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mIsInteracting:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$2402(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;Z)Z

    .line 367
    return-void
.end method

.method public onUserInteractionEnd()V
    .locals 4

    .prologue
    .line 371
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    const/4 v1, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mIsInteracting:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$2402(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;Z)Z

    .line 372
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mShowBars:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$2500(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 373
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x8

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->DOUBLE_TAP_TIMEOUT:I
    invoke-static {}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$2600()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 375
    :cond_0
    return-void
.end method
