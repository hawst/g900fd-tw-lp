.class public Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
.super Ljava/lang/Object;
.source "ContactMediaInfo.java"


# instance fields
.field private mDisplayName:Ljava/lang/String;

.field private mDisplayNameAlter:Ljava/lang/String;

.field private mId:Ljava/lang/Long;

.field private mJoinedLookupKey:Ljava/lang/String;

.field private mLookupKey:Ljava/lang/String;

.field private mPhotoId:Ljava/lang/Long;

.field private mPhotoThumbnailUri:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayNameAlter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->mDisplayNameAlter:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->mId:Ljava/lang/Long;

    return-object v0
.end method

.method public getJoinedLookupKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->mJoinedLookupKey:Ljava/lang/String;

    return-object v0
.end method

.method public getLookupKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->mJoinedLookupKey:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->mJoinedLookupKey:Ljava/lang/String;

    .line 71
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->mLookupKey:Ljava/lang/String;

    goto :goto_0
.end method

.method public getPhotoId()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->mPhotoId:Ljava/lang/Long;

    return-object v0
.end method

.method public getPhotoThumbnailUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->mPhotoThumbnailUri:Ljava/lang/String;

    return-object v0
.end method

.method public getSingleLookupKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->mLookupKey:Ljava/lang/String;

    return-object v0
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1, "displayName"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->mDisplayName:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public setDisplayNameAlter(Ljava/lang/String;)V
    .locals 0
    .param p1, "displayNameAlter"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->mDisplayNameAlter:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public setId(Ljava/lang/Long;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/Long;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->mId:Ljava/lang/Long;

    .line 34
    return-void
.end method

.method public setJoinedLookupKey(Ljava/lang/String;)V
    .locals 0
    .param p1, "joinedLookupKey"    # Ljava/lang/String;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->mJoinedLookupKey:Ljava/lang/String;

    .line 88
    return-void
.end method

.method public setPhotoId(Ljava/lang/Long;)V
    .locals 0
    .param p1, "photoId"    # Ljava/lang/Long;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->mPhotoId:Ljava/lang/Long;

    .line 58
    return-void
.end method

.method public setPhotoThumbnailUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "photoThumbnailUri"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->mPhotoThumbnailUri:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public setSingleLookupKey(Ljava/lang/String;)V
    .locals 0
    .param p1, "lookupKey"    # Ljava/lang/String;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->mLookupKey:Ljava/lang/String;

    .line 84
    return-void
.end method
