.class public Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlComposeObject.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TranformAnimation"
.end annotation


# instance fields
.field mRatio:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V
    .locals 0

    .prologue
    .line 539
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    return-void
.end method


# virtual methods
.method public applyTransform(F)V
    .locals 8
    .param p1, "ratio"    # F

    .prologue
    .line 543
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->mRatio:F

    .line 544
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimPos:Z

    if-eqz v1, :cond_0

    .line 545
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mPosAnimDisplacement:Z

    if-eqz v1, :cond_5

    .line 546
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDispMode:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->msx:F

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mtx:F

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->msx:F

    sub-float/2addr v4, v5

    mul-float/2addr v4, p1

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->msy:F

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mty:F

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->msy:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, p1

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->msz:F

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mtz:F

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->msz:F

    sub-float/2addr v6, v7

    mul-float/2addr v6, p1

    add-float/2addr v5, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(IFFF)V

    .line 550
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimResizing:Z

    if-eqz v1, :cond_1

    .line 551
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcW:F

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTgtW:F

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcW:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, p1

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcH:F

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTgtH:F

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcH:F

    sub-float/2addr v4, v5

    mul-float/2addr v4, p1

    add-float/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    .line 552
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimAlpha:Z

    if-eqz v1, :cond_2

    .line 553
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSAlpha:F

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTAlpha:F

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSAlpha:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, p1

    add-float/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setAlpha(F)V

    .line 554
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimRoll:Z

    if-eqz v1, :cond_3

    .line 555
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mDefRoll:F

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcRoll:F

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTgtRoll:F

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcRoll:F

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->mRatio:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setRoll(F)V

    .line 556
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimScaling:Z

    if-eqz v1, :cond_4

    .line 557
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcScale:F

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTgtScale:F

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcScale:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, p1

    add-float v0, v1, v2

    .line 558
    .local v0, "scale":F
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v1, v0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setScale(FF)V

    .line 560
    .end local v0    # "scale":F
    :cond_4
    return-void

    .line 548
    :cond_5
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->msx:F

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mtx:F

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->msx:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, p1

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->msy:F

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mty:F

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->msy:F

    sub-float/2addr v4, v5

    mul-float/2addr v4, p1

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->msz:F

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mtz:F

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->msz:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, p1

    add-float/2addr v4, v5

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    goto/16 :goto_0
.end method

.method protected onStop()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 563
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimPos:Z

    if-eqz v0, :cond_0

    .line 564
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mPosAnimDisplacement:Z

    if-eqz v0, :cond_5

    .line 565
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHndDispMode:I

    invoke-virtual {v0, v1, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->addPos(II)V

    .line 569
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimResizing:Z

    if-eqz v0, :cond_1

    .line 570
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTgtW:F

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTgtH:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    .line 571
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimAlpha:Z

    if-eqz v0, :cond_2

    .line 572
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTAlpha:F

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setAlpha(F)V

    .line 574
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimRoll:Z

    if-eqz v0, :cond_3

    .line 575
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTgtRoll:F

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setRoll(F)V

    .line 577
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransformAnimScaling:Z

    if-eqz v0, :cond_4

    .line 578
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTgtScale:F

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTgtScale:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setScale(FF)V

    .line 580
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPosAnimMode(Z)V

    .line 581
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 582
    return-void

    .line 567
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mtx:F

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mty:F

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mtz:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    goto :goto_0
.end method
