.class public Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlBoundaryAnim.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;
    }
.end annotation


# static fields
.field private static DECEL_UNIT:F = 0.0f

.field private static ELASTIC_RATIO:F = 0.0f

.field private static final TAG:Ljava/lang/String; = "GlBoundaryAnim"


# instance fields
.field private final REACHED_BOUNDARY_REACHED_MAX:I

.field private final REACHED_BOUNDARY_REACHED_MIN:I

.field private final REACHED_BOUNDARY_WITHIN_RANGE:I

.field private mAnimSx:F

.field private mBoundDec:F

.field protected mBoundaryAnimListener:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

.field private mDec:F

.field private mDecFactor:F

.field private mDispDx:F

.field private mDx:F

.field private mElastic:F

.field private mFlingDuration:J

.field private mFlingTimer:Landroid/os/CountDownTimer;

.field private mLastDelta:F

.field private mMaxX:F

.field private mMinX:F

.field private mReachedBoundary:I

.field private mSpeed:F

.field private mSpeedMax:F

.field private mSpeedRatio:F

.field private mSx:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/high16 v0, 0x42c80000    # 100.0f

    sput v0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->DECEL_UNIT:F

    .line 18
    const v0, 0x3c23d70a    # 0.01f

    sput v0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->ELASTIC_RATIO:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 43
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 19
    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->REACHED_BOUNDARY_WITHIN_RANGE:I

    .line 20
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->REACHED_BOUNDARY_REACHED_MIN:I

    .line 21
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->REACHED_BOUNDARY_REACHED_MAX:I

    .line 24
    const/high16 v1, 0x42c80000    # 100.0f

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mBoundDec:F

    .line 25
    const v1, 0x3f19999a    # 0.6f

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeedRatio:F

    .line 26
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDecFactor:F

    .line 27
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    .line 28
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mAnimSx:F

    .line 34
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mLastDelta:F

    .line 36
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeedMax:F

    .line 37
    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mReachedBoundary:I

    .line 38
    const-wide/16 v2, 0xbb8

    iput-wide v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mFlingDuration:J

    .line 39
    iput-object v4, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mFlingTimer:Landroid/os/CountDownTimer;

    .line 41
    iput-object v4, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mBoundaryAnimListener:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 45
    .local v0, "r":Landroid/content/res/Resources;
    const v1, 0x7f0e001d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeedRatio:F

    .line 46
    const v1, 0x7f0e001e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDecFactor:F

    .line 47
    return-void
.end method

.method private setMovementInter(FZ)V
    .locals 11
    .param p1, "delta"    # F
    .param p2, "byUser"    # Z

    .prologue
    const/4 v10, 0x1

    const/4 v9, -0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 168
    cmpl-float v5, p1, v7

    if-nez v5, :cond_1

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 171
    :cond_1
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mLastDelta:F

    sub-float v0, p1, v5

    .line 172
    .local v0, "deltaDisp":F
    move v1, v0

    .line 174
    .local v1, "deltaDispR":F
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDx:F

    add-float/2addr v5, v1

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDx:F

    .line 175
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDx:F

    add-float v4, v5, v6

    .line 176
    .local v4, "sum":F
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mMinX:F

    cmpg-float v5, v4, v5

    if-gtz v5, :cond_7

    .line 177
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCaneExtraHapticEnabled(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 178
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/16 v6, 0x4ea2

    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    .line 179
    :cond_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mFlingTimer:Landroid/os/CountDownTimer;

    if-eqz v5, :cond_3

    .line 180
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mFlingTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v5}, Landroid/os/CountDownTimer;->cancel()V

    .line 182
    :cond_3
    cmpl-float v5, v0, v7

    if-lez v5, :cond_6

    if-eqz p2, :cond_6

    .line 183
    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    .line 184
    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDx:F

    .line 185
    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDispDx:F

    .line 186
    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mElastic:F

    .line 187
    iput v8, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mReachedBoundary:I

    .line 224
    :goto_1
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mLastDelta:F

    .line 225
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mBoundaryAnimListener:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

    if-eqz v5, :cond_0

    .line 226
    const/high16 v3, 0x3f800000    # 1.0f

    .line 227
    .local v3, "maxElasticR":F
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 228
    const v3, 0x3e4ccccd    # 0.2f

    .line 229
    :cond_4
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mElastic:F

    sget v6, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->ELASTIC_RATIO:F

    mul-float v2, v5, v6

    .line 230
    .local v2, "elasticR":F
    cmpl-float v5, v2, v3

    if-lez v5, :cond_d

    .line 231
    move v2, v3

    .line 234
    :cond_5
    :goto_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mBoundaryAnimListener:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDispDx:F

    add-float/2addr v6, v7

    invoke-interface {v5, v6, v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;->onFlingProcess(FF)V

    goto :goto_0

    .line 189
    .end local v2    # "elasticR":F
    .end local v3    # "maxElasticR":F
    :cond_6
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    neg-float v5, v5

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDispDx:F

    .line 190
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mMinX:F

    sub-float v5, v4, v5

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mElastic:F

    .line 191
    iput v9, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mReachedBoundary:I

    goto :goto_1

    .line 193
    :cond_7
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mMaxX:F

    cmpl-float v5, v4, v5

    if-ltz v5, :cond_a

    .line 194
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mFlingTimer:Landroid/os/CountDownTimer;

    if-eqz v5, :cond_8

    .line 195
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mFlingTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v5}, Landroid/os/CountDownTimer;->cancel()V

    .line 197
    :cond_8
    cmpg-float v5, v0, v7

    if-gez v5, :cond_9

    if-eqz p2, :cond_9

    .line 198
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mMaxX:F

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    .line 199
    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDx:F

    .line 200
    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDispDx:F

    .line 201
    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mElastic:F

    .line 202
    iput v8, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mReachedBoundary:I

    goto :goto_1

    .line 204
    :cond_9
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mMaxX:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    sub-float/2addr v5, v6

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDispDx:F

    .line 205
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mMaxX:F

    sub-float v5, v4, v5

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mElastic:F

    .line 206
    iput v10, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mReachedBoundary:I

    goto :goto_1

    .line 209
    :cond_a
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mReachedBoundary:I

    if-ne v5, v9, :cond_b

    .line 210
    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    .line 211
    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDx:F

    .line 212
    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDispDx:F

    .line 220
    :goto_3
    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mElastic:F

    .line 221
    iput v8, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mReachedBoundary:I

    goto :goto_1

    .line 213
    :cond_b
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mReachedBoundary:I

    if-ne v5, v10, :cond_c

    .line 214
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mMaxX:F

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    .line 215
    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDx:F

    .line 216
    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDispDx:F

    goto :goto_3

    .line 218
    :cond_c
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDx:F

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDispDx:F

    goto :goto_3

    .line 232
    .restart local v2    # "elasticR":F
    .restart local v3    # "maxElasticR":F
    :cond_d
    neg-float v5, v3

    cmpg-float v5, v2, v5

    if-gez v5, :cond_5

    .line 233
    neg-float v2, v3

    goto :goto_2
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 9
    .param p1, "ratio"    # F

    .prologue
    const/high16 v8, 0x447a0000    # 1000.0f

    const/high16 v7, -0x3b860000    # -1000.0f

    const/4 v6, 0x0

    .line 262
    const/4 v2, 0x0

    .line 264
    .local v2, "reachAccZero":Z
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mReachedBoundary:I

    .line 265
    .local v0, "oldBoundary":I
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mReachedBoundary:I

    if-nez v3, :cond_5

    .line 266
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeed:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDec:F

    add-float/2addr v3, v4

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeed:F

    .line 272
    :goto_0
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mAnimSx:F

    .line 273
    .local v1, "oldSx":F
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mAnimSx:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeed:F

    const/high16 v5, 0x42c80000    # 100.0f

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mAnimSx:F

    .line 274
    cmpl-float v3, v1, v6

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mAnimSx:F

    mul-float/2addr v3, v1

    cmpg-float v3, v3, v6

    if-gtz v3, :cond_0

    .line 275
    iput v6, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mAnimSx:F

    .line 276
    const/4 v2, 0x1

    .line 278
    :cond_0
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mAnimSx:F

    const/4 v4, 0x0

    invoke-direct {p0, v3, v4}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setMovementInter(FZ)V

    .line 279
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeed:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    sget v4, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->DECEL_UNIT:F

    cmpg-float v3, v3, v4

    if-ltz v3, :cond_1

    if-eqz v2, :cond_2

    .line 280
    :cond_1
    const-string v3, "GlBoundaryAnim"

    const-string v4, "applyTransform stopped by low speed"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 283
    :cond_2
    if-eqz v0, :cond_3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mReachedBoundary:I

    if-eq v3, v0, :cond_3

    .line 284
    const-string v3, "GlBoundaryAnim"

    const-string v4, "applyTransform stopped by end of boundeffect"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 287
    :cond_3
    if-nez v0, :cond_4

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mReachedBoundary:I

    if-eqz v3, :cond_4

    .line 288
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeed:F

    cmpl-float v3, v3, v8

    if-lez v3, :cond_7

    .line 289
    iput v8, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeed:F

    .line 293
    :cond_4
    :goto_1
    return-void

    .line 267
    .end local v1    # "oldSx":F
    :cond_5
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mReachedBoundary:I

    if-lez v3, :cond_6

    .line 268
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeed:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mBoundDec:F

    sub-float/2addr v3, v4

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeed:F

    goto :goto_0

    .line 270
    :cond_6
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeed:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mBoundDec:F

    add-float/2addr v3, v4

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeed:F

    goto :goto_0

    .line 290
    .restart local v1    # "oldSx":F
    :cond_7
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeed:F

    cmpg-float v3, v3, v7

    if-gez v3, :cond_4

    .line 291
    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeed:F

    goto :goto_1
.end method

.method public getReachedBoundary()I
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mReachedBoundary:I

    return v0
.end method

.method public getScroll()F
    .locals 1

    .prologue
    .line 131
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    return v0
.end method

.method public getSpeed()F
    .locals 1

    .prologue
    .line 246
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeed:F

    return v0
.end method

.method public getSpeedRatio()F
    .locals 1

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mReachedBoundary:I

    if-nez v0, :cond_0

    .line 240
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeed:F

    .line 242
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onStop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 297
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDispDx:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    .line 298
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDx:F

    .line 299
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDispDx:F

    .line 300
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mLastDelta:F

    .line 301
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mAnimSx:F

    .line 302
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mBoundaryAnimListener:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mBoundaryAnimListener:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;->onFlingEnd(F)V

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mFlingTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_1

    .line 306
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mFlingTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 307
    :cond_1
    return-void
.end method

.method public setBoundaryAnimationListener(Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

    .prologue
    .line 255
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mBoundaryAnimListener:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

    .line 256
    return-void
.end method

.method public setDuration(J)V
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 311
    iput-wide p1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mFlingDuration:J

    .line 312
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->setDuration(J)V

    .line 313
    return-void
.end method

.method public setFactors(FF)V
    .locals 0
    .param p1, "speedRatio"    # F
    .param p2, "decFactor"    # F

    .prologue
    .line 250
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeedRatio:F

    .line 251
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDecFactor:F

    .line 252
    return-void
.end method

.method public setInitMovement(F)V
    .locals 1
    .param p1, "movX"    # F

    .prologue
    const/4 v0, 0x0

    .line 144
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    .line 145
    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDx:F

    .line 146
    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDispDx:F

    .line 147
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mReachedBoundary:I

    .line 148
    return-void
.end method

.method public setMaxSpeed(F)V
    .locals 1
    .param p1, "speed"    # F

    .prologue
    .line 151
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeedMax:F

    .line 152
    return-void
.end method

.method public setMoveToward(F)V
    .locals 3
    .param p1, "speed"    # F

    .prologue
    const/4 v2, 0x0

    .line 109
    cmpl-float v0, p1, v2

    if-nez v0, :cond_1

    .line 110
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mAnimState:I

    if-eqz v0, :cond_0

    .line 112
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 127
    :cond_0
    :goto_0
    const-string v0, "GlBoundaryAnim"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMoveToward = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeed:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    return-void

    .line 115
    :cond_1
    const-wide/16 v0, 0x2710

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setDuration(J)V

    .line 116
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeed:F

    .line 117
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDec:F

    .line 118
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->isIdle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDx:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    .line 120
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDx:F

    .line 121
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDispDx:F

    .line 122
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mLastDelta:F

    .line 123
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mAnimSx:F

    .line 124
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->start()V

    goto :goto_0
.end method

.method public setMovement(F)V
    .locals 1
    .param p1, "delta"    # F

    .prologue
    .line 155
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setMovementInter(FZ)V

    .line 156
    return-void
.end method

.method public setRange(FF)V
    .locals 1
    .param p1, "minX"    # F
    .param p2, "maxX"    # F

    .prologue
    .line 135
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mMinX:F

    .line 136
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mMaxX:F

    .line 137
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_0

    .line 138
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    .line 139
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    cmpl-float v0, v0, p2

    if-lez v0, :cond_1

    .line 140
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    .line 141
    :cond_1
    return-void
.end method

.method public startFling(F)V
    .locals 3
    .param p1, "speed"    # F

    .prologue
    const/4 v2, 0x0

    .line 65
    cmpl-float v0, p1, v2

    if-nez v0, :cond_1

    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mReachedBoundary:I

    if-nez v0, :cond_1

    .line 66
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDispDx:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    .line 67
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDx:F

    .line 68
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDispDx:F

    .line 69
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mLastDelta:F

    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mBoundaryAnimListener:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mBoundaryAnimListener:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;->onFlingEnd(F)V

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeedMax:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_2

    .line 77
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeedMax:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_3

    .line 78
    iget p1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeedMax:F

    .line 82
    :cond_2
    :goto_1
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mReachedBoundary:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 83
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDx:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mAnimSx:F

    .line 84
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mAnimSx:F

    neg-float v0, v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeed:F

    .line 85
    sget v0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->DECEL_UNIT:F

    neg-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDecFactor:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDec:F

    .line 103
    :goto_2
    const-string v0, "GlBoundaryAnim"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "startFling = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeed:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mDec = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDec:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    iget-wide v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mFlingDuration:J

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setDuration(J)V

    .line 105
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->start()V

    goto :goto_0

    .line 79
    :cond_3
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeedMax:F

    neg-float v0, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_2

    .line 80
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeedMax:F

    neg-float p1, v0

    goto :goto_1

    .line 86
    :cond_4
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mReachedBoundary:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_5

    .line 87
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDx:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mAnimSx:F

    .line 88
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mAnimSx:F

    neg-float v0, v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeed:F

    .line 89
    sget v0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->DECEL_UNIT:F

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDecFactor:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDec:F

    goto :goto_2

    .line 91
    :cond_5
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDx:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSx:F

    .line 92
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDx:F

    .line 93
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDispDx:F

    .line 94
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mLastDelta:F

    .line 95
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mAnimSx:F

    .line 96
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeedRatio:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mSpeed:F

    .line 97
    cmpl-float v0, p1, v2

    if-lez v0, :cond_6

    .line 98
    sget v0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->DECEL_UNIT:F

    neg-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDecFactor:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDec:F

    goto :goto_2

    .line 100
    :cond_6
    sget v0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->DECEL_UNIT:F

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDecFactor:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mDec:F

    goto :goto_2
.end method

.method public startFlingtoEnd(F)V
    .locals 7
    .param p1, "speed"    # F

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mFlingTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mFlingTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 53
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$1;

    const-wide/32 v2, 0xf4240

    const-wide/16 v4, 0xc8

    move-object v1, p0

    move v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$1;-><init>(Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;JJF)V

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$1;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->mFlingTimer:Landroid/os/CountDownTimer;

    .line 63
    return-void
.end method
