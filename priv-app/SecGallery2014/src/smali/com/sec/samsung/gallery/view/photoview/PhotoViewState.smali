.class public Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;
.super Lcom/sec/android/gallery3d/app/ActivityState;
.source "PhotoViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLoaderConfig;,
        Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLoaderConfig;,
        Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;,
        Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;
    }
.end annotation


# static fields
.field public static final KEY_AUTO_SELECT_ALL:Ljava/lang/String; = "auto-select-all"

.field public static final KEY_EMPTY_ALBUM:Ljava/lang/String; = "empty-album"

.field public static final KEY_MEDIA_PATH:Ljava/lang/String; = "media-path"

.field public static final KEY_PARENT_MEDIA_PATH:Ljava/lang/String; = "parent-media-path"

.field public static final KEY_RESUME_ANIMATION:Ljava/lang/String; = "resume_animation"

.field public static final KEY_SET_CENTER:Ljava/lang/String; = "set-center"

.field public static final KEY_SHOW_CLUSTER_MENU:Ljava/lang/String; = "cluster-menu"

.field public static NO_USE_SPLITVIEW:Z = false

.field public static final REQUEST_ADD_TAG:I = 0x205

.field public static final REQUEST_ASSIGN_NAME:I = 0x204

.field public static final REQUEST_NEW_ALBUM_CREATION:I = 0x200

.field public static SPLIT_RATIO_LAND:F = 0.0f

.field public static SPLIT_RATIO_PORT:F = 0.0f

.field private static final TAG:Ljava/lang/String;

.field public static final VIEW_MODE_SWITCH_TO_COLLAGE_VIEW:I = 0x203

.field public static final VIEW_MODE_SWITCH_TO_DETAIL_VIEW:I = 0x201

.field public static final VIEW_MODE_SWITCH_TO_DETAIL_VIEW_FROM_CAMERA:I = 0x202


# instance fields
.field private mAlbumAdapterCfg:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLoaderConfig;

.field private mAlbumLayoutConfig:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;

.field protected mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

.field public mCurrentLaunchMode:Lcom/sec/samsung/gallery/core/LaunchModeType;

.field public mCurrentLayout:I

.field private mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mCurrentMultiZone:I

.field public mDetailViewStarted:Z

.field protected mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

.field public mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

.field protected mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

.field private mIsEasyMode:Z

.field protected mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

.field protected mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

.field protected mMenu:Landroid/view/Menu;

.field private mMultiWindow:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

.field protected mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mPhotoAdapterCfg:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLoaderConfig;

.field public mPhotoEventHandle:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;

.field private mPhotoLayoutConfig:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;

.field protected mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

.field protected mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field protected mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mShrinkOption:I

.field protected mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

.field protected mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const-class v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->TAG:Ljava/lang/String;

    .line 72
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->NO_USE_SPLITVIEW:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 69
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ActivityState;-><init>()V

    .line 103
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    .line 104
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    .line 106
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 107
    iput v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mShrinkOption:I

    .line 116
    iput v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentMultiZone:I

    .line 118
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mDetailViewStarted:Z

    .line 836
    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method private checkAndSetValidMediaItemSource()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 600
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 621
    :goto_0
    return-void

    .line 602
    :cond_0
    sget-object v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->TAG:Ljava/lang/String;

    const-string v3, "checkMediaItemSourceValid : source in mediaItemAdapter is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mTopMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-nez v2, :cond_2

    .line 605
    :cond_1
    sget-object v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->TAG:Ljava/lang/String;

    const-string v3, "checkMediaItemSourceValid : mComponent or mTopMediaSet is null, finish state !!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 606
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v2, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    goto :goto_0

    .line 610
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mTopMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 611
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v1, :cond_3

    .line 612
    sget-object v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->TAG:Ljava/lang/String;

    const-string v3, "checkMediaItemSourceValid : 1st SubMediaSet is null, finish state !!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v2, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    goto :goto_0

    .line 616
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iput v4, v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    .line 617
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iput-object v1, v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 618
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setSource(Lcom/sec/android/gallery3d/data/MediaSet;)Z

    .line 619
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget v2, v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    shl-int/lit8 v0, v2, 0x10

    .line 620
    .local v0, "initCode":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v2, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->setInitialCode(I)V

    goto :goto_0
.end method

.method private getResource(I)F
    .locals 2
    .param p1, "resID"    # I

    .prologue
    .line 735
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 736
    .local v0, "str":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    return v1
.end method

.method private setMediaSetFromAlbumViewState()V
    .locals 3

    .prologue
    .line 740
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/StateManager;->popPreviousInfo(I)Ljava/lang/Object;

    move-result-object v0

    .line 741
    .local v0, "info":Ljava/lang/Object;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;

    if-nez v1, :cond_1

    .line 745
    .end local v0    # "info":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-void

    .line 744
    .restart local v0    # "info":Ljava/lang/Object;
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    check-cast v0, Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;

    .end local v0    # "info":Ljava/lang/Object;
    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->applyMediaSetInfo(Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;)V

    goto :goto_0
.end method


# virtual methods
.method public SetExpandSplitView()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 851
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->isOriginExpanded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 852
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v0, v1, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setExpanded(ZZ)V

    .line 853
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setOriginExpanded(Z)V

    .line 855
    :cond_0
    return-void
.end method

.method public getContentsForDetailsDialog()Ljava/util/ArrayList;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 548
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 549
    .local v2, "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-nez v11, :cond_0

    .line 550
    sget-object v11, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->TAG:Ljava/lang/String;

    const-string v16, "mSelectionModeProxy is null"

    move-object/from16 v0, v16

    invoke-static {v11, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 595
    .end local v2    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local v3, "contentList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    return-object v3

    .line 554
    .end local v3    # "contentList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v2    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v10

    .line 555
    .local v10, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    if-nez v10, :cond_1

    .line 556
    sget-object v11, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->TAG:Ljava/lang/String;

    const-string v16, "mediaList is null"

    move-object/from16 v0, v16

    invoke-static {v11, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 557
    .restart local v3    # "contentList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    goto :goto_0

    .line 560
    .end local v3    # "contentList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v9

    .line 563
    .local v9, "mediaCount":I
    const/4 v11, 0x1

    if-ne v9, v11, :cond_3

    .line 564
    const/4 v11, 0x0

    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 565
    .local v6, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v6, :cond_2

    .line 566
    new-instance v11, Lcom/sec/samsung/gallery/controller/DetailModel;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaObject;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v11, v0, v1}, Lcom/sec/samsung/gallery/controller/DetailModel;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaDetails;)V

    invoke-virtual {v11}, Lcom/sec/samsung/gallery/controller/DetailModel;->toStringList()Ljava/util/ArrayList;

    move-result-object v2

    .end local v6    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    :goto_1
    move-object v3, v2

    .line 595
    .restart local v3    # "contentList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    goto :goto_0

    .line 569
    .end local v3    # "contentList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_3
    const-string v7, ""

    .line 570
    .local v7, "line1":Ljava/lang/String;
    const-string v8, ""

    .line 571
    .local v8, "line2":Ljava/lang/String;
    const-wide/16 v14, 0x0

    .line 572
    .local v14, "sizeSummary":J
    const-wide/16 v12, 0x0

    .line 575
    .local v12, "size":D
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    if-ge v5, v9, :cond_5

    .line 576
    :try_start_0
    invoke-interface {v10, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 577
    .restart local v6    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v6, :cond_4

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaObject;->getSize()J

    move-result-wide v16

    const-wide/16 v18, 0x0

    cmp-long v11, v16, v18

    if-lez v11, :cond_4

    .line 578
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaObject;->getSize()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v16

    add-long v14, v14, v16

    .line 575
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 581
    .end local v6    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :catch_0
    move-exception v4

    .line 582
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 585
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_5
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Long;->doubleValue()D

    move-result-wide v12

    .line 586
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v16, 0x7f0e01d0

    move/from16 v0, v16

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    move-object/from16 v0, v16

    invoke-static {v11, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 588
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0e01d1

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v16, ": "

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v0, v12, v13}, Lcom/sec/android/gallery3d/util/GalleryUtils;->formatItemsSelectedSize(Landroid/content/Context;D)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 591
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 592
    const-wide/16 v16, 0x0

    cmpl-double v11, v12, v16

    if-lez v11, :cond_2

    .line 593
    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method

.method public getContentsForFaceTag()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 626
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    if-nez v1, :cond_1

    .line 627
    :cond_0
    const/4 v0, 0x0

    .line 632
    :goto_0
    return-object v0

    .line 628
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 630
    .local v0, "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 631
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getCurrentMediaItemIndex()I
    .locals 1

    .prologue
    .line 703
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaItemIndex:I

    return v0
.end method

.method public getCurrentMediaSetIndex()I
    .locals 1

    .prologue
    .line 707
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    return v0
.end method

.method public getCurrentMediaSetPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 699
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetPath:Ljava/lang/String;

    return-object v0
.end method

.method public getFirstLoadingCount()I
    .locals 2

    .prologue
    .line 714
    iget v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentLayout:I

    packed-switch v1, :pswitch_data_0

    .line 725
    const/16 v0, 0xc

    .line 727
    .local v0, "loadingCount":I
    :goto_0
    return v0

    .line 716
    .end local v0    # "loadingCount":I
    :pswitch_0
    const/16 v0, 0x80

    .line 717
    .restart local v0    # "loadingCount":I
    goto :goto_0

    .line 719
    .end local v0    # "loadingCount":I
    :pswitch_1
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLargeLoadingCount:Z

    if-eqz v1, :cond_0

    const/16 v0, 0x42

    .line 720
    .restart local v0    # "loadingCount":I
    :goto_1
    goto :goto_0

    .line 719
    .end local v0    # "loadingCount":I
    :cond_0
    const/16 v0, 0x20

    goto :goto_1

    .line 722
    :pswitch_2
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLargeLoadingCount:Z

    if-eqz v1, :cond_1

    const/16 v0, 0x1c

    .line 723
    .restart local v0    # "loadingCount":I
    :goto_2
    goto :goto_0

    .line 722
    .end local v0    # "loadingCount":I
    :cond_1
    const/16 v0, 0x12

    goto :goto_2

    .line 714
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getGalleryActivity()Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1

    .prologue
    .line 731
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method public getMediaForDetails()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 3

    .prologue
    .line 536
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 537
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 538
    .local v0, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v1, v0, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v1, :cond_0

    .line 539
    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 543
    .end local v0    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 507
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mDetailViewStarted:Z

    if-eqz v0, :cond_0

    .line 514
    :goto_0
    return-void

    .line 509
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalSelectedItems()I

    move-result v0

    if-lez v0, :cond_1

    .line 510
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->showNewAlbumCancelDialog()V

    goto :goto_0

    .line 512
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoEventHandle:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->onBackPressed()V

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v3, 0x0

    .line 127
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 129
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMenu:Landroid/view/Menu;

    const v2, 0x7f0f0287

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIcon(Landroid/view/Menu;II)V

    .line 130
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMenu:Landroid/view/Menu;

    const v2, 0x7f0f0024

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIcon(Landroid/view/Menu;II)V

    .line 131
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMenu:Landroid/view/Menu;

    const v2, 0x7f0f0288

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIcon(Landroid/view/Menu;II)V

    .line 140
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 141
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->stop()V

    .line 142
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->refreshSelectionBarState(Z)V

    .line 145
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->IsCategoryView(Lcom/sec/samsung/gallery/core/TabTagType;Ljava/lang/Boolean;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 146
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v1, :cond_1

    .line 147
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 152
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    if-eqz v1, :cond_2

    .line 153
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onConfigChanged(Landroid/content/res/Configuration;)V

    .line 154
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoEventHandle:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;

    if-eqz v1, :cond_3

    .line 160
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoEventHandle:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->resetSlideshowLayout()V

    .line 162
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v1, :cond_4

    .line 163
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 164
    :cond_4
    return-void

    .line 156
    :catch_0
    move-exception v0

    .line 157
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 8
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 168
    const-string v1, "Gallery_Performance"

    const-string v4, "PhotoViewState onCreate Start"

    invoke-static {v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v4, 0x7f0f00bf

    invoke-virtual {v1, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 171
    const v1, 0x7f0e0017

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->getResource(I)F

    move-result v1

    sput v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->SPLIT_RATIO_PORT:F

    .line 172
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTabletPickerMode(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 173
    const v1, 0x7f0e0019

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->getResource(I)F

    move-result v1

    sput v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->SPLIT_RATIO_LAND:F

    .line 176
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->hidePreDisplayScreen()V

    .line 177
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 178
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .line 179
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setStatusProxy(Lcom/sec/android/gallery3d/app/StateManager;)V

    .line 180
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setTitle()V

    .line 181
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    .line 182
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 183
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getNewAlbumSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 184
    new-instance v1, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v4}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 185
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->isUpButtonVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 186
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentViewMode(Ljava/lang/Class;)V

    .line 188
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentLaunchMode:Lcom/sec/samsung/gallery/core/LaunchModeType;

    .line 189
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEmergencyMode(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_1
    move v1, v3

    :goto_1
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mIsEasyMode:Z

    .line 191
    new-instance v1, Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v4, p0}, Lcom/sec/samsung/gallery/view/ActionBarManager;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/app/ActivityState;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 192
    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v4, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoEventHandle:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;

    .line 193
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoEventHandle:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentLaunchMode:Lcom/sec/samsung/gallery/core/LaunchModeType;

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->setMode(Lcom/sec/samsung/gallery/core/LaunchModeType;)V

    .line 195
    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .line 196
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->setEnvOnCreate(Landroid/os/Bundle;)V

    .line 198
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->IsCategoryView(Lcom/sec/samsung/gallery/core/TabTagType;Ljava/lang/Boolean;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 199
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string/jumbo v4, "timeViewColCnt"

    invoke-static {v1, v4, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentLayout:I

    .line 221
    :cond_2
    :goto_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMultiWindow:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    .line 222
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v1, :cond_3

    .line 223
    iput v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentMultiZone:I

    .line 225
    :cond_3
    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoLayoutConfig:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;

    .line 226
    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v1, p0, v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;Landroid/content/res/Resources;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mAlbumLayoutConfig:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;

    .line 227
    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLoaderConfig;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLoaderConfig;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoAdapterCfg:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLoaderConfig;

    .line 228
    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLoaderConfig;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLoaderConfig;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mAlbumAdapterCfg:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLoaderConfig;

    .line 230
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsNoSplitMode:Z

    if-eqz v1, :cond_b

    .line 231
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoLayoutConfig:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;

    iput-boolean v3, v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mIsSplitViewExpanded:Z

    .line 240
    :goto_3
    new-instance v1, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mTopMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mAlbumAdapterCfg:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLoaderConfig;

    invoke-direct {v1, v4, v5, v6, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;I)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    .line 242
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mAlbumModelListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->setModelListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;)V

    .line 244
    new-instance v1, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoAdapterCfg:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLoaderConfig;

    invoke-direct {v1, v3, v4, v5, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;I)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    .line 245
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoModelListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setModelListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;)V

    .line 246
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mIsEasyMode:Z

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setEasyMode(Z)V

    .line 247
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->setMediaSetFromAlbumViewState()V

    .line 248
    const-string v1, "Gallery_Performance"

    const-string v2, "PhotoViewState onCreate End"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    return-void

    .line 175
    :cond_4
    const v1, 0x7f0e0018

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->getResource(I)F

    move-result v1

    sput v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->SPLIT_RATIO_LAND:F

    goto/16 :goto_0

    :cond_5
    move v1, v2

    .line 189
    goto/16 :goto_1

    .line 201
    :cond_6
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsNoSplitMode:Z

    if-eqz v1, :cond_7

    .line 202
    iput v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentLayout:I

    goto/16 :goto_2

    .line 203
    :cond_7
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mIsEasyMode:Z

    if-eqz v1, :cond_8

    .line 204
    iput v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentLayout:I

    .line 205
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v4, "photoViewColCnt"

    iget v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentLayout:I

    invoke-static {v1, v4, v5}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    goto/16 :goto_2

    .line 206
    :cond_8
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isPickMode()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 207
    iput v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentLayout:I

    goto/16 :goto_2

    .line 209
    :cond_9
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v4, "photoViewColCnt"

    invoke-static {v1, v4, v5}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentLayout:I

    .line 211
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mIsEasyMode:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentLayout:I

    if-ne v1, v7, :cond_2

    .line 212
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v4, "is_split_view_expanded"

    invoke-static {v1, v4, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    .line 214
    .local v0, "expanded":Z
    if-eqz v0, :cond_a

    .line 215
    iput v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentLayout:I

    goto/16 :goto_2

    .line 217
    :cond_a
    iput v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentLayout:I

    goto/16 :goto_2

    .line 233
    .end local v0    # "expanded":Z
    :cond_b
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPickMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_c

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiPickMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 234
    :cond_c
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoLayoutConfig:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;

    iput-boolean v2, v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mIsSplitViewExpanded:Z

    goto/16 :goto_3

    .line 236
    :cond_d
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoLayoutConfig:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v5, "is_split_view_expanded"

    invoke-static {v4, v5, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mIsSplitViewExpanded:Z

    goto/16 :goto_3
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 456
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 457
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMenu:Landroid/view/Menu;

    .line 458
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 392
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->onDestroy()V

    .line 393
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    if-eqz v0, :cond_0

    .line 394
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setOnStatusChangedListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;)V

    .line 396
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->setEnvOnDestroy()V

    .line 397
    return-void
.end method

.method public onDirty()V
    .locals 2

    .prologue
    .line 689
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    if-eqz v1, :cond_0

    .line 690
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 691
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 692
    check-cast v1, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->isNeedUpdateCluster()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 693
    check-cast v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    .end local v0    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->updateMediaSet()V

    .line 696
    :cond_0
    return-void
.end method

.method public onMWLayoutChanged()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 665
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-nez v5, :cond_1

    .line 679
    :cond_0
    :goto_0
    return-void

    .line 667
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 668
    .local v1, "intent":Landroid/content/Intent;
    const-string v5, "help_mode"

    invoke-virtual {v1, v5, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 669
    .local v2, "isHelpMode":Z
    if-eqz v2, :cond_0

    .line 670
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getActionBarView()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;

    .line 671
    .local v3, "mMultiPick":Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMultiWindow:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v5

    if-eqz v5, :cond_2

    move v0, v4

    .line 672
    .local v0, "curZone":I
    :goto_1
    iget v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentMultiZone:I

    if-eq v5, v0, :cond_3

    .line 673
    iput v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentMultiZone:I

    .line 674
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->setMWChanged(Z)V

    .line 675
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->onResume()V

    goto :goto_0

    .line 671
    .end local v0    # "curZone":I
    :cond_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMultiWindow:Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->getZone()I

    move-result v0

    goto :goto_1

    .line 677
    .restart local v0    # "curZone":I
    :cond_3
    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->setMWChanged(Z)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 471
    const/4 v0, 0x0

    .line 472
    .local v0, "isCategoryType":Z
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v1, :cond_0

    .line 473
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->IsCategoryView(Lcom/sec/samsung/gallery/core/TabTagType;Ljava/lang/Boolean;)Z

    move-result v0

    .line 475
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 476
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 479
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_7

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->getDrawerEnable()Z

    move-result v1

    if-nez v1, :cond_7

    .line 480
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->isUpButtonVisible()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v1

    if-nez v1, :cond_3

    .line 482
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onBackPressed()V

    .line 503
    :cond_2
    :goto_0
    return-void

    .line 484
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->isUpButtonVisible()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 486
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getStateCount()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 487
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->onBackPressed()V

    goto :goto_0

    .line 489
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->isUpButtonVisible()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoEventHandle:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->isSlideshowcheck()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 491
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->onBackPressed()V

    goto :goto_0

    .line 493
    :cond_5
    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoEventHandle:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->isSlideshowcheck()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 494
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onBackPressed()V

    goto :goto_0

    .line 496
    :cond_6
    if-eqz v0, :cond_8

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 497
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->onBackPressed()V

    .line 502
    :cond_7
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onOptionsItemSelected(Landroid/view/MenuItem;)V

    goto :goto_0

    .line 498
    :cond_8
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryActivity;

    iget-boolean v1, v1, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsSingleAlbumForPick:Z

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsSingleAlbumMode:Z

    if-eqz v1, :cond_7

    .line 499
    :cond_9
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->onBackPressed()V

    goto :goto_1
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 362
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->onPause()V

    .line 363
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getActionBarView()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v0

    .line 364
    .local v0, "actionbarView":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    instance-of v1, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    if-eqz v1, :cond_3

    .line 365
    check-cast v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    .end local v0    # "actionbarView":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->onPause(Z)V

    .line 369
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    if-eqz v1, :cond_0

    .line 370
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->saveCurrentScrollInfo()V

    .line 371
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->pause()V

    .line 373
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    .line 374
    iput v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mShrinkOption:I

    .line 375
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->setEnvOnPause()V

    .line 377
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    if-eqz v1, :cond_1

    .line 378
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->onPause()V

    .line 380
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    if-eqz v1, :cond_2

    .line 381
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->onPause()V

    .line 388
    :cond_2
    return-void

    .line 367
    .restart local v0    # "actionbarView":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    goto :goto_0
.end method

.method public onPrepareOptionMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 462
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 463
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->onPrepareOptionMenu(Landroid/view/Menu;)V

    .line 464
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v0, :cond_0

    .line 465
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 467
    :cond_0
    return-void
.end method

.method public onRefresh()V
    .locals 0

    .prologue
    .line 685
    return-void
.end method

.method public onResume()V
    .locals 15

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x4

    const/4 v1, 0x1

    const/4 v14, 0x0

    .line 273
    const-string v0, "Gallery_Performance"

    const-string v5, "PhotoViewState onResume Start"

    invoke-static {v0, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->onResume()V

    .line 275
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/gallery3d/app/FilterUtils;->toClusterType(Lcom/sec/samsung/gallery/core/TabTagType;)I

    move-result v5

    invoke-interface {v0, v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->setCurrentClusterType(I)V

    .line 276
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumMode:Z

    if-eqz v0, :cond_9

    .line 277
    const/4 v9, 0x2

    .line 282
    .local v9, "option":I
    :goto_0
    if-eqz v9, :cond_0

    .line 283
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->isSelectionMode(Ljava/lang/String;)V

    .line 285
    :cond_0
    move v4, v9

    .line 286
    .local v4, "splitOption":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    shl-int/lit8 v2, v0, 0x10

    .line 287
    .local v2, "initialAlbum":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v0, :cond_1

    .line 288
    iget v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mShrinkOption:I

    or-int/2addr v9, v0

    .line 291
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEmergencyMode(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_2
    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mIsEasyMode:Z

    .line 292
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mIsEasyMode:Z

    if-eqz v0, :cond_d

    .line 293
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsNoSplitMode:Z

    if-eqz v0, :cond_c

    .line 294
    or-int/lit16 v9, v9, 0x300

    .line 299
    :goto_2
    iput v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentLayout:I

    .line 303
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSetIndex:I

    shl-int/lit8 v2, v0, 0x10

    .line 304
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mIsEasyMode:Z

    if-nez v0, :cond_4

    iget v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentLayout:I

    if-ne v0, v6, :cond_4

    .line 305
    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentLayout:I

    .line 306
    :cond_4
    sget-boolean v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->NO_USE_SPLITVIEW:Z

    if-eqz v0, :cond_5

    .line 307
    or-int/lit16 v9, v9, 0x200

    .line 308
    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentLayout:I

    .line 310
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->isLocaleRTL()Z

    move-result v12

    .line 311
    .local v12, "isRTL":Z
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mAlbumLayoutConfig:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;

    iput-boolean v12, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;->mIsRTL:Z

    .line 312
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v0, v12}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->setRTL(Z)V

    .line 313
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->setFirstLoadingCount(I)V

    .line 314
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->setFirstIndex(I)V

    .line 316
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoLayoutConfig:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;

    iget v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentLayout:I

    # invokes: Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->setModeLevel(I)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->access$000(Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;I)V

    .line 317
    or-int/lit16 v9, v9, 0x80

    .line 318
    iget v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentLayout:I

    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->getThumbSizeType(I)I

    move-result v13

    .line 319
    .local v13, "type":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v0, v13}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setInitThumbType(I)V

    .line 320
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->getFirstLoadingCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setFirstLoadingCount(I)V

    .line 321
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mIsEasyMode:Z

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setEasyMode(Z)V

    .line 323
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string/jumbo v1, "sort_by_type_time"

    invoke-static {v0, v1, v14}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v13

    .line 326
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v11

    .line 327
    .local v11, "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    if-eqz v11, :cond_6

    .line 328
    invoke-interface {v11, v13}, Lcom/sec/android/gallery3d/app/GalleryApp;->setSortByType(I)V

    .line 330
    :cond_6
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mAlbumLayoutConfig:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$AlbumLayoutConfig;

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;-><init>(Landroid/content/Context;ILcom/sec/android/gallery3d/data/MediaItem;ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    .line 331
    new-instance v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/4 v7, -0x1

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoLayoutConfig:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;

    invoke-direct/range {v5 .. v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;-><init>(Landroid/content/Context;ILcom/sec/android/gallery3d/data/MediaItem;ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;)V

    iput-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    .line 332
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->setBasicReferences()V

    .line 333
    sget-boolean v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->NO_USE_SPLITVIEW:Z

    if-eqz v0, :cond_e

    .line 334
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->attachLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;)V

    .line 339
    :goto_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoEventHandle:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->initializeView()V

    .line 340
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->checkAndSetValidMediaItemSource()V

    .line 341
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->setEnvOnResume()V

    .line 343
    sget-boolean v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->NO_USE_SPLITVIEW:Z

    if-nez v0, :cond_7

    .line 344
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->onResume()V

    .line 346
    :cond_7
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->onResume()V

    .line 348
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inExpansionMode()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    if-eqz v0, :cond_8

    .line 349
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->refreshCheckState()V

    .line 351
    :cond_8
    iput-boolean v14, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mDetailViewStarted:Z

    .line 352
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onResume()V

    .line 353
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;->setBeamListener()V

    .line 355
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-static {v0, v1, p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->checkViewStateByFilterBySetting(Landroid/content/Context;Lcom/sec/android/gallery3d/app/StateManager;Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 357
    const-string v0, "Gallery_Performance"

    const-string v1, "PhotoViewState onResume End"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    return-void

    .line 278
    .end local v2    # "initialAlbum":I
    .end local v4    # "splitOption":I
    .end local v9    # "option":I
    .end local v11    # "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    .end local v12    # "isRTL":Z
    .end local v13    # "type":I
    :cond_9
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 279
    const/4 v9, 0x1

    .restart local v9    # "option":I
    goto/16 :goto_0

    .line 281
    .end local v9    # "option":I
    :cond_a
    const/4 v9, 0x0

    .restart local v9    # "option":I
    goto/16 :goto_0

    .restart local v2    # "initialAlbum":I
    .restart local v4    # "splitOption":I
    :cond_b
    move v0, v14

    .line 291
    goto/16 :goto_1

    .line 297
    :cond_c
    or-int/lit16 v9, v9, 0x100

    goto/16 :goto_2

    .line 300
    :cond_d
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsNoSplitMode:Z

    if-eqz v0, :cond_3

    .line 301
    or-int/lit16 v9, v9, 0x200

    goto/16 :goto_3

    .line 336
    .restart local v11    # "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    .restart local v12    # "isRTL":Z
    .restart local v13    # "type":I
    :cond_e
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->addLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    .line 337
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->attachLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;)V

    goto :goto_4
.end method

.method protected onSaveState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 401
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->setOnSaveState(Landroid/os/Bundle;)V

    .line 402
    return-void
.end method

.method public onStateResult(IILandroid/content/Intent;)V
    .locals 9
    .param p1, "request"    # I
    .param p2, "result"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v4, 0x1

    const/4 v6, -0x1

    const/4 v1, 0x0

    .line 406
    sparse-switch p1, :sswitch_data_0

    .line 452
    .end local p3    # "data":Landroid/content/Intent;
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 409
    .restart local p3    # "data":Landroid/content/Intent;
    :sswitch_1
    if-eq p2, v6, :cond_1

    if-nez p2, :cond_2

    if-eqz p3, :cond_2

    const-string v5, "is_pressed_cancel"

    invoke-virtual {p3, v5, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_1
    move v1, v4

    .line 411
    .local v1, "finishActivity":Z
    :cond_2
    if-eqz v1, :cond_0

    .line 412
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-ne p2, v6, :cond_3

    .end local p3    # "data":Landroid/content/Intent;
    :goto_1
    invoke-virtual {v4, p2, p3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 413
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 412
    .restart local p3    # "data":Landroid/content/Intent;
    :cond_3
    const/4 p3, 0x0

    goto :goto_1

    .line 428
    .end local v1    # "finishActivity":Z
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->getContentsForFaceTag()Ljava/util/ArrayList;

    move-result-object v0

    .line 429
    .local v0, "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {p0, p3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->setFaceTagParam(Landroid/content/Intent;)V

    .line 430
    if-eqz p2, :cond_4

    .line 431
    const/4 v5, 0x4

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v5, v3, v1

    aput-object v0, v3, v4

    sget-object v4, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;->ASSIGN_TAG:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    aput-object v4, v3, v7

    aput-object p3, v3, v8

    .line 434
    .local v3, "sendParams":[Ljava/lang/Object;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v5, "START_FACE_TAG"

    invoke-virtual {v4, v5, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 437
    .end local v3    # "sendParams":[Ljava/lang/Object;
    :cond_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v5, "EXIT_SELECTION_MODE"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 440
    .end local v0    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :sswitch_3
    if-ne p2, v6, :cond_0

    .line 441
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/16 v5, 0x205

    invoke-virtual {v4, v5, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAddUserTagDialog(IZ)V

    .line 442
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v5, "EXIT_SELECTION_MODE"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 446
    :sswitch_4
    new-array v2, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v5, v2, v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v4

    aput-object p3, v2, v7

    .line 449
    .local v2, "params":[Ljava/lang/Object;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v5, "ADD_SLIDESHOW_MUSIC"

    invoke-virtual {v4, v5, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 406
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_4
        0x203 -> :sswitch_0
        0x204 -> :sswitch_2
        0x205 -> :sswitch_3
        0x403 -> :sswitch_1
    .end sparse-switch
.end method

.method public onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 2
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    .line 253
    sget-object v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->TAG:Ljava/lang/String;

    const-string v1, "onViewInitialize"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    if-nez v0, :cond_1

    .line 264
    :cond_0
    :goto_0
    return-void

    .line 258
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mSplitView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->setAdapter(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;)V

    .line 259
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setAdapter(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;)V

    .line 261
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->setEnvGLViewInitialize()V

    goto :goto_0
.end method

.method public setFaceTagParam(Landroid/content/Intent;)V
    .locals 7
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 638
    if-eqz p1, :cond_0

    .line 639
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 640
    .local v1, "uri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 641
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 643
    .local v6, "c":Landroid/database/Cursor;
    const/4 v2, 0x2

    :try_start_0
    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "lookup"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "display_name"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 652
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 658
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_0
    return-void

    .line 652
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v6    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v2

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2
.end method

.method public setPromptRequest(IILjava/lang/Object;)V
    .locals 4
    .param p1, "code"    # I
    .param p2, "parm"    # I
    .param p3, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 523
    check-cast p3, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local p3    # "data":Ljava/lang/Object;
    iput-object p3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 524
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-nez v1, :cond_0

    .line 525
    iput v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mShrinkOption:I

    .line 532
    :goto_0
    return-void

    .line 528
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->requestScreenNail(Lcom/sec/android/gallery3d/data/MediaItem;I)Z

    .line 529
    const/4 v1, 0x1

    if-ne p2, v1, :cond_2

    const/16 v0, 0x20

    :cond_1
    :goto_1
    iput v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mShrinkOption:I

    goto :goto_0

    :cond_2
    const/4 v1, 0x2

    if-ne p2, v1, :cond_1

    const/16 v0, 0x10

    goto :goto_1
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 1
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 518
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->processEvent(Ljava/util/Observable;Ljava/lang/Object;)V

    .line 519
    return-void
.end method
