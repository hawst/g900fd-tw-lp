.class public final enum Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;
.super Ljava/lang/Enum;
.source "KnoxModeReceiverCmd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CmdType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;

.field public static final enum REGISTER_RECEIVER:Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;

.field public static final enum UNREGISTER_RECEIVER:Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 26
    new-instance v0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;

    const-string v1, "REGISTER_RECEIVER"

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;->REGISTER_RECEIVER:Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;

    new-instance v0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;

    const-string v1, "UNREGISTER_RECEIVER"

    invoke-direct {v0, v1, v3}, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;->UNREGISTER_RECEIVER:Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;

    .line 25
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;

    sget-object v1, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;->REGISTER_RECEIVER:Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;->UNREGISTER_RECEIVER:Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;->$VALUES:[Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 25
    const-class v0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;

    return-object v0
.end method

.method public static values()[Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;->$VALUES:[Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;

    invoke-virtual {v0}, [Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;

    return-object v0
.end method
