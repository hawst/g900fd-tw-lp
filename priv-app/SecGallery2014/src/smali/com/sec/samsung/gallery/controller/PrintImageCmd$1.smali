.class Lcom/sec/samsung/gallery/controller/PrintImageCmd$1;
.super Landroid/print/PrintDocumentAdapter;
.source "PrintImageCmd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/PrintImageCmd;->doPrint()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mAttributes:Landroid/print/PrintAttributes;

.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/PrintImageCmd;

.field final synthetic val$jobName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/PrintImageCmd;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/PrintImageCmd;

    iput-object p2, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd$1;->val$jobName:Ljava/lang/String;

    invoke-direct {p0}, Landroid/print/PrintDocumentAdapter;-><init>()V

    return-void
.end method

.method private getMatrix(IILandroid/graphics/RectF;I)Landroid/graphics/Matrix;
    .locals 7
    .param p1, "imageWidth"    # I
    .param p2, "imageHeight"    # I
    .param p3, "content"    # Landroid/graphics/RectF;
    .param p4, "fittingMode"    # I

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 208
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 211
    .local v0, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v4

    int-to-float v5, p1

    div-float v1, v4, v5

    .line 212
    .local v1, "scale":F
    const/4 v4, 0x2

    if-ne p4, v4, :cond_0

    .line 213
    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v4

    int-to-float v5, p2

    div-float/2addr v4, v5

    invoke-static {v1, v4}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 217
    :goto_0
    invoke-virtual {v0, v1, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 220
    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v4

    int-to-float v5, p1

    mul-float/2addr v5, v1

    sub-float/2addr v4, v5

    div-float v2, v4, v6

    .line 221
    .local v2, "translateX":F
    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v4

    int-to-float v5, p2

    mul-float/2addr v5, v1

    sub-float/2addr v4, v5

    div-float v3, v4, v6

    .line 222
    .local v3, "translateY":F
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 223
    return-object v0

    .line 215
    .end local v2    # "translateX":F
    .end local v3    # "translateY":F
    :cond_0
    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v4

    int-to-float v5, p2

    div-float/2addr v4, v5

    invoke-static {v1, v4}, Ljava/lang/Math;->min(FF)F

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method public onLayout(Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$LayoutResultCallback;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "oldAttributes"    # Landroid/print/PrintAttributes;
    .param p2, "newAttributes"    # Landroid/print/PrintAttributes;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .param p4, "callback"    # Landroid/print/PrintDocumentAdapter$LayoutResultCallback;
    .param p5, "metadata"    # Landroid/os/Bundle;

    .prologue
    const/4 v0, 0x1

    .line 141
    iput-object p2, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd$1;->mAttributes:Landroid/print/PrintAttributes;

    .line 142
    invoke-virtual {p3}, Landroid/os/CancellationSignal;->isCanceled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 143
    invoke-virtual {p4}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutCancelled()V

    .line 154
    :goto_0
    return-void

    .line 146
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/PrintImageCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/PrintImageCmd;->mMediaList:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/PrintImageCmd;->access$000(Lcom/sec/samsung/gallery/controller/PrintImageCmd;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 147
    new-instance v2, Landroid/print/PrintDocumentInfo$Builder;

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd$1;->val$jobName:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/print/PrintDocumentInfo$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Landroid/print/PrintDocumentInfo$Builder;->setContentType(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/PrintImageCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/PrintImageCmd;->mMediaList:Ljava/util/List;
    invoke-static {v3}, Lcom/sec/samsung/gallery/controller/PrintImageCmd;->access$000(Lcom/sec/samsung/gallery/controller/PrintImageCmd;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/print/PrintDocumentInfo$Builder;->setPageCount(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/print/PrintDocumentInfo$Builder;->build()Landroid/print/PrintDocumentInfo;

    move-result-object v1

    .line 149
    .local v1, "info":Landroid/print/PrintDocumentInfo;
    invoke-virtual {p2, p1}, Landroid/print/PrintAttributes;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 150
    .local v0, "changed":Z
    :goto_1
    invoke-virtual {p4, v1, v0}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFinished(Landroid/print/PrintDocumentInfo;Z)V

    goto :goto_0

    .line 149
    .end local v0    # "changed":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 152
    .end local v1    # "info":Landroid/print/PrintDocumentInfo;
    :cond_2
    const-string v2, "Page count calculation failed."

    invoke-virtual {p4, v2}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFailed(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onWrite([Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V
    .locals 11
    .param p1, "pageRanges"    # [Landroid/print/PageRange;
    .param p2, "destination"    # Landroid/os/ParcelFileDescriptor;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .param p4, "callback"    # Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    .prologue
    .line 159
    new-instance v5, Landroid/print/pdf/PrintedPdfDocument;

    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/PrintImageCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/PrintImageCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v8}, Lcom/sec/samsung/gallery/controller/PrintImageCmd;->access$100(Lcom/sec/samsung/gallery/controller/PrintImageCmd;)Landroid/app/Activity;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd$1;->mAttributes:Landroid/print/PrintAttributes;

    invoke-direct {v5, v8, v9}, Landroid/print/pdf/PrintedPdfDocument;-><init>(Landroid/content/Context;Landroid/print/PrintAttributes;)V

    .line 160
    .local v5, "mPdfDocument":Landroid/print/pdf/PrintedPdfDocument;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/PrintImageCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/PrintImageCmd;->mMediaList:Ljava/util/List;
    invoke-static {v8}, Lcom/sec/samsung/gallery/controller/PrintImageCmd;->access$000(Lcom/sec/samsung/gallery/controller/PrintImageCmd;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v3, v8, :cond_5

    .line 161
    if-eqz p1, :cond_2

    .line 162
    const/4 v0, 0x0

    .line 164
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/PrintImageCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/PrintImageCmd;->mMediaList:Ljava/util/List;
    invoke-static {v8}, Lcom/sec/samsung/gallery/controller/PrintImageCmd;->access$000(Lcom/sec/samsung/gallery/controller/PrintImageCmd;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 165
    .local v4, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v8, v4, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v8, :cond_0

    .line 166
    check-cast v4, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    .end local v4    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;

    move-result-object v4

    .line 168
    .restart local v4    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    if-eqz v4, :cond_1

    .line 169
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 171
    :cond_1
    if-nez v0, :cond_3

    .line 160
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v4    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_2
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 174
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v4    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_3
    invoke-virtual {p3}, Landroid/os/CancellationSignal;->isCanceled()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 175
    invoke-virtual {p4}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteCancelled()V

    .line 176
    invoke-virtual {v5}, Landroid/print/pdf/PrintedPdfDocument;->close()V

    .line 177
    const/4 v5, 0x0

    .line 205
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v4    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :goto_2
    return-void

    .line 181
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v4    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_4
    invoke-virtual {v5, v3}, Landroid/print/pdf/PrintedPdfDocument;->startPage(I)Landroid/graphics/pdf/PdfDocument$Page;

    move-result-object v7

    .line 182
    .local v7, "page":Landroid/graphics/pdf/PdfDocument$Page;
    new-instance v1, Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/pdf/PdfDocument$Page;->getInfo()Landroid/graphics/pdf/PdfDocument$PageInfo;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/pdf/PdfDocument$PageInfo;->getContentRect()Landroid/graphics/Rect;

    move-result-object v8

    invoke-direct {v1, v8}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 183
    .local v1, "content":Landroid/graphics/RectF;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    const/4 v10, 0x1

    invoke-direct {p0, v8, v9, v1, v10}, Lcom/sec/samsung/gallery/controller/PrintImageCmd$1;->getMatrix(IILandroid/graphics/RectF;I)Landroid/graphics/Matrix;

    move-result-object v6

    .line 184
    .local v6, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v7}, Landroid/graphics/pdf/PdfDocument$Page;->getCanvas()Landroid/graphics/Canvas;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v0, v6, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 185
    invoke-virtual {v5, v7}, Landroid/print/pdf/PrintedPdfDocument;->finishPage(Landroid/graphics/pdf/PdfDocument$Page;)V

    .line 186
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v8

    if-nez v8, :cond_2

    .line 187
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1

    .line 195
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "content":Landroid/graphics/RectF;
    .end local v4    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v6    # "matrix":Landroid/graphics/Matrix;
    .end local v7    # "page":Landroid/graphics/pdf/PdfDocument$Page;
    :cond_5
    :try_start_0
    new-instance v8, Ljava/io/FileOutputStream;

    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    invoke-virtual {v5, v8}, Landroid/print/pdf/PrintedPdfDocument;->writeTo(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    invoke-virtual {v5}, Landroid/print/pdf/PrintedPdfDocument;->close()V

    .line 201
    const/4 v5, 0x0

    .line 203
    invoke-virtual {p4, p1}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteFinished([Landroid/print/PageRange;)V

    goto :goto_2

    .line 196
    :catch_0
    move-exception v2

    .line 197
    .local v2, "e":Ljava/io/IOException;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p4, v8}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteFailed(Ljava/lang/CharSequence;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 200
    invoke-virtual {v5}, Landroid/print/pdf/PrintedPdfDocument;->close()V

    .line 201
    const/4 v5, 0x0

    goto :goto_2

    .line 200
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    invoke-virtual {v5}, Landroid/print/pdf/PrintedPdfDocument;->close()V

    .line 201
    const/4 v5, 0x0

    throw v8
.end method
