.class Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$1;
.super Ljava/lang/Object;
.source "StartCategoryTagCmd.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCategoryPickerDialog$Result;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->requestEditCategory(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;Ljava/lang/String;Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

.field final synthetic val$clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field final synthetic val$editType:Ljava/lang/String;

.field final synthetic val$parentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field final synthetic val$selectedPaths:Ljava/util/ArrayList;

.field final synthetic val$uriList:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;Ljava/lang/String;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    iput-object p2, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$1;->val$editType:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$1;->val$clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iput-object p4, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$1;->val$parentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iput-object p5, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$1;->val$selectedPaths:Ljava/util/ArrayList;

    iput-object p6, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$1;->val$uriList:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 0

    .prologue
    .line 243
    return-void
.end method

.method public done(Ljava/util/ArrayList;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v8, 0x0

    .line 230
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$1;->val$editType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->access$000(Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "EXIT_SELECTION_MODE"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 238
    return-void

    .line 233
    :cond_0
    invoke-static {p1}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->makeStringForType(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v3

    .line 234
    .local v3, "newType":Ljava/lang/String;
    new-instance v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$1;->val$editType:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$1;->val$clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$1;->val$parentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$1;->val$selectedPaths:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$1;->val$uriList:Ljava/util/ArrayList;

    invoke-direct/range {v0 .. v7}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;-><init>(Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    new-array v1, v8, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
