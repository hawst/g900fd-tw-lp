.class Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar$1;
.super Ljava/lang/Object;
.source "MoreInfoActionBar.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;->access$000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;)Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 33
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;)Landroid/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0e0468

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;->access$300(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;)Landroid/app/ActionBar;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;->access$200(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x106000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoActionBar;->initDisplayOptions()V

    .line 37
    return-void
.end method
