.class Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;
.super Ljava/lang/Object;
.source "QuickScrollHelpView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->addView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 64
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    const/4 v3, 0x1

    # setter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mIsShowingHelpView:Z
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$002(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;Z)Z

    .line 65
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mQuickScrollHelpView:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$100(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Landroid/widget/RelativeLayout;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 92
    :goto_0
    return-void

    .line 67
    :cond_0
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewOverlayHelp:Z

    if-eqz v2, :cond_1

    .line 68
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$300(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v4, 0x7f0300c2

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mMainView:Landroid/view/ViewGroup;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$200(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Landroid/view/ViewGroup;

    move-result-object v5

    invoke-virtual {v2, v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    # setter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mQuickScrollHelpView:Landroid/widget/RelativeLayout;
    invoke-static {v3, v2}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$102(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;

    .line 71
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mQuickScrollHelpView:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$100(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Landroid/widget/RelativeLayout;

    move-result-object v2

    const v3, 0x7f0f00e3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 72
    .local v1, "mTextView":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mQuickScrollHelpView:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$100(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Landroid/widget/RelativeLayout;

    move-result-object v2

    const v3, 0x7f0f00e2

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 73
    .local v0, "imageview":Landroid/widget/ImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    # invokes: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->isRTL()Z
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$400(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 75
    const v2, 0x7f0202dc

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 81
    :goto_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$500(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v2

    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v2, v3, :cond_3

    .line 82
    const v2, 0x7f0e04a6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 85
    :goto_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mMainView:Landroid/view/ViewGroup;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$200(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Landroid/view/ViewGroup;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mQuickScrollHelpView:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$100(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 86
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    # invokes: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->initializeView()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$600(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)V

    .line 87
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    # invokes: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->setHelpState(Z)V
    invoke-static {v2, v6}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$700(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;Z)V

    .line 88
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    # invokes: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->bringToFront()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$800(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)V

    .line 89
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$300(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 90
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$300(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->getDrawerEnable()Z

    move-result v2

    # setter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mIsDrawerEnabled:Z
    invoke-static {v3, v2}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$902(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;Z)Z

    .line 91
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$300(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setDrawerEnable(Z)V

    goto/16 :goto_0

    .line 70
    .end local v0    # "imageview":Landroid/widget/ImageView;
    .end local v1    # "mTextView":Landroid/widget/TextView;
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$300(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v4, 0x7f0300c1

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mMainView:Landroid/view/ViewGroup;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$200(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Landroid/view/ViewGroup;

    move-result-object v5

    invoke-virtual {v2, v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    # setter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mQuickScrollHelpView:Landroid/widget/RelativeLayout;
    invoke-static {v3, v2}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$102(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;

    goto/16 :goto_1

    .line 79
    .restart local v0    # "imageview":Landroid/widget/ImageView;
    .restart local v1    # "mTextView":Landroid/widget/TextView;
    :cond_2
    const v2, 0x7f0202db

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    .line 84
    :cond_3
    const v2, 0x7f0e04a7

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_3
.end method
