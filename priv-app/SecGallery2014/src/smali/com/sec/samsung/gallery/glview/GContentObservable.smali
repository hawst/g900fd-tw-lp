.class public Lcom/sec/samsung/gallery/glview/GContentObservable;
.super Landroid/database/Observable;
.source "GContentObservable.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/database/Observable",
        "<",
        "Lcom/sec/samsung/gallery/glview/GContentObserver;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Landroid/database/Observable;-><init>()V

    return-void
.end method


# virtual methods
.method public notifyDataSetChanged()V
    .locals 4

    .prologue
    .line 35
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GContentObservable;->mObservers:Ljava/util/ArrayList;

    monitor-enter v3

    .line 36
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GContentObservable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/glview/GContentObserver;

    .line 37
    .local v1, "observer":Lcom/sec/samsung/gallery/glview/GContentObserver;
    invoke-interface {v1}, Lcom/sec/samsung/gallery/glview/GContentObserver;->onChanged()V

    goto :goto_0

    .line 39
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "observer":Lcom/sec/samsung/gallery/glview/GContentObserver;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 40
    return-void
.end method

.method public notifyDataSetChanged(II)V
    .locals 4
    .param p1, "Index"    # I
    .param p2, "parm"    # I

    .prologue
    .line 24
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GContentObservable;->mObservers:Ljava/util/ArrayList;

    monitor-enter v3

    .line 25
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GContentObservable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/glview/GContentObserver;

    .line 26
    .local v1, "observer":Lcom/sec/samsung/gallery/glview/GContentObserver;
    invoke-interface {v1, p1, p2}, Lcom/sec/samsung/gallery/glview/GContentObserver;->onChanged(II)V

    goto :goto_0

    .line 28
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "observer":Lcom/sec/samsung/gallery/glview/GContentObserver;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 29
    return-void
.end method

.method public registerObserver(Lcom/sec/samsung/gallery/glview/GContentObserver;)V
    .locals 1
    .param p1, "observer"    # Lcom/sec/samsung/gallery/glview/GContentObserver;

    .prologue
    .line 13
    :try_start_0
    invoke-super {p0, p1}, Landroid/database/Observable;->registerObserver(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 17
    :goto_0
    return-void

    .line 14
    :catch_0
    move-exception v0

    .line 15
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method public bridge synthetic registerObserver(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 9
    check-cast p1, Lcom/sec/samsung/gallery/glview/GContentObserver;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/glview/GContentObservable;->registerObserver(Lcom/sec/samsung/gallery/glview/GContentObserver;)V

    return-void
.end method
