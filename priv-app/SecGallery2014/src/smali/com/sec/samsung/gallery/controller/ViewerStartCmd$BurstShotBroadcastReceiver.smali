.class Lcom/sec/samsung/gallery/controller/ViewerStartCmd$BurstShotBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ViewerStartCmd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/ViewerStartCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BurstShotBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/ViewerStartCmd;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/controller/ViewerStartCmd;)V
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd$BurstShotBroadcastReceiver;->this$0:Lcom/sec/samsung/gallery/controller/ViewerStartCmd;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/controller/ViewerStartCmd;Lcom/sec/samsung/gallery/controller/ViewerStartCmd$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/controller/ViewerStartCmd;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/controller/ViewerStartCmd$1;

    .prologue
    .line 319
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/ViewerStartCmd$BurstShotBroadcastReceiver;-><init>(Lcom/sec/samsung/gallery/controller/ViewerStartCmd;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 323
    :try_start_0
    const-string v2, "BurstShotImageProcess"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 325
    .local v0, "burstShotImagesSaveInProgress":Z
    if-nez v0, :cond_0

    .line 326
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd$BurstShotBroadcastReceiver;->this$0:Lcom/sec/samsung/gallery/controller/ViewerStartCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->access$000(Lcom/sec/samsung/gallery/controller/ViewerStartCmd;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/data/DataManager;->setChangeNotifierActive(Z)V

    .line 327
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd$BurstShotBroadcastReceiver;->this$0:Lcom/sec/samsung/gallery/controller/ViewerStartCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->access$000(Lcom/sec/samsung/gallery/controller/ViewerStartCmd;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd$BurstShotBroadcastReceiver;->this$0:Lcom/sec/samsung/gallery/controller/ViewerStartCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mBurstShotBroadcastReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v3}, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->access$200(Lcom/sec/samsung/gallery/controller/ViewerStartCmd;)Landroid/content/BroadcastReceiver;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 328
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd$BurstShotBroadcastReceiver;->this$0:Lcom/sec/samsung/gallery/controller/ViewerStartCmd;

    const/4 v3, 0x0

    # setter for: Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mBurstShotBroadcastReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->access$202(Lcom/sec/samsung/gallery/controller/ViewerStartCmd;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 333
    .end local v0    # "burstShotImagesSaveInProgress":Z
    :cond_0
    :goto_0
    return-void

    .line 330
    :catch_0
    move-exception v1

    .line 331
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method
