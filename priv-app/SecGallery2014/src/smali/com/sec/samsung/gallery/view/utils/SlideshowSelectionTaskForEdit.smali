.class public Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;
.super Landroid/os/AsyncTask;
.source "SlideshowSelectionTaskForEdit.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/sec/android/gallery3d/ui/SelectionManager;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SlideshowSelectionTaskForEdit"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMaxCount:I

.field private mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field private mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

.field private mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

.field private mTaskRunning:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onProgressListener"    # Lcom/sec/android/gallery3d/data/OnProgressListener;
    .param p3, "maxCount"    # I

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mTaskRunning:Z

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mMaxCount:I

    .line 32
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit$1;-><init>(Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 44
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mContext:Landroid/content/Context;

    .line 45
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    .line 46
    iput p3, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mMaxCount:I

    .line 47
    return-void
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;
    .param p1, "x1"    # Z

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mTaskRunning:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;)Lcom/sec/android/gallery3d/data/OnProgressListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    return-object v0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 24
    check-cast p1, [Lcom/sec/android/gallery3d/ui/SelectionManager;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->doInBackground([Lcom/sec/android/gallery3d/ui/SelectionManager;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/sec/android/gallery3d/ui/SelectionManager;)Ljava/lang/Void;
    .locals 16
    .param p1, "arg"    # [Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    const-wide/16 v14, 0x1

    const/4 v13, 0x0

    .line 51
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mContext:Landroid/content/Context;

    check-cast v11, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSlideShowSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v10

    .line 53
    .local v10, "selectionProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v10}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 54
    aget-object v2, p1, v13

    .line 56
    .local v2, "albumSelectionProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v3

    .line 58
    .local v3, "count":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v3, :cond_1

    .line 59
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->isCancelled()Z

    move-result v11

    if-nez v11, :cond_0

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mTaskRunning:Z

    if-nez v11, :cond_2

    .line 60
    :cond_0
    invoke-virtual {v10}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 99
    :cond_1
    :goto_1
    const/4 v11, 0x0

    return-object v11

    .line 63
    :cond_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15, v13}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->increaseProgress(JZ)V

    .line 64
    const/4 v11, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15, v11}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->increaseProgress(JZ)V

    .line 66
    const/4 v9, 0x0

    .line 68
    .local v9, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :try_start_0
    invoke-virtual {v2, v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v8

    .line 69
    .local v8, "mediaObj":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v11, v8, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v11, :cond_3

    .line 70
    move-object v0, v8

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    move-object v9, v0
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    if-eqz v9, :cond_3

    instance-of v11, v9, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    if-nez v11, :cond_3

    sget-boolean v11, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloud:Z

    if-eqz v11, :cond_4

    instance-of v11, v9, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;

    if-eqz v11, :cond_4

    .line 58
    :cond_3
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 74
    .end local v8    # "mediaObj":Lcom/sec/android/gallery3d/data/MediaObject;
    :catch_0
    move-exception v4

    .line 75
    .local v4, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v11, "SlideshowSelectionTaskForEdit"

    const-string v12, "IndexOutOfBoundsException, some items are not added"

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 83
    .end local v4    # "e":Ljava/lang/IndexOutOfBoundsException;
    .restart local v8    # "mediaObj":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_4
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v11

    invoke-virtual {v9, v13, v11}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 84
    .local v7, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->isCancelled()Z

    move-result v11

    if-nez v11, :cond_6

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mTaskRunning:Z

    if-nez v11, :cond_7

    .line 85
    :cond_6
    invoke-virtual {v10}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    goto :goto_2

    .line 89
    :cond_7
    instance-of v11, v7, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v11, :cond_8

    .line 90
    check-cast v7, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    .end local v7    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;

    move-result-object v7

    .line 93
    .restart local v7    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_8
    if-eqz v7, :cond_5

    instance-of v11, v7, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-nez v11, :cond_5

    instance-of v11, v7, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-eqz v11, :cond_9

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v11

    const/4 v12, 0x4

    if-eq v11, v12, :cond_5

    .line 96
    :cond_9
    invoke-virtual {v10, v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_3
.end method

.method public increaseProgress(JZ)V
    .locals 3
    .param p1, "step"    # J
    .param p3, "isFileSize"    # Z

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->increaseProgress(JZZ)V

    .line 134
    return-void
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->closeCustomProgressDialog()V

    .line 105
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 106
    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 24
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->onCancelled(Ljava/lang/Void;)V

    return-void
.end method

.method protected onCancelled(Ljava/lang/Void;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->closeCustomProgressDialog()V

    .line 111
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onCancelled(Ljava/lang/Object;)V

    .line 112
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 24
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/data/OnProgressListener;->onCompleted(Z)V

    .line 117
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->closeCustomProgressDialog(I)V

    .line 118
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 119
    return-void
.end method

.method protected onPreExecute()V
    .locals 5

    .prologue
    .line 123
    new-instance v0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    .line 124
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mMaxCount:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalCount(I)V

    .line 126
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mMaxCount:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalSize(J)V

    .line 127
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mContext:Landroid/content/Context;

    const v2, 0x7f0e0032

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->showProgressDialog(Ljava/lang/String;Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 129
    :cond_0
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 130
    return-void
.end method
