.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;
.super Ljava/lang/Object;
.source "PhotoViewComponent.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V
    .locals 0

    .prologue
    .line 2946
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isCheckAvailable()V
    .locals 4

    .prologue
    .line 2994
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    if-nez v1, :cond_0

    .line 3003
    :goto_0
    return-void

    .line 2997
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isPenSelectionMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2998
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setPenSelectionMode(Z)V

    .line 3000
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 3001
    .local v0, "selected":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPickMode:Z

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isAvailableCount(IZZ)Z

    .line 3002
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    # invokes: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateActionBarSelectionMode(Lcom/sec/android/gallery3d/data/MediaSet;)V
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2500(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_0
.end method

.method public onPenSelection(IIIZ)Z
    .locals 8
    .param p1, "albumIdex"    # I
    .param p2, "itemIndex"    # I
    .param p3, "selectedCnt"    # I
    .param p4, "isLast"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2950
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v4

    sget-object v5, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v4, v5, :cond_0

    .line 2989
    :goto_0
    return v2

    .line 2953
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isPenSelectionMode()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2954
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setPenSelectionMode(Z)V

    .line 2957
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v4, v4, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumMode:Z

    if-eqz v4, :cond_6

    .line 2958
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    move-result-object v5

    invoke-virtual {v5, v2, p2}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2959
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v4, v3, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->enterSelectionMode(ZZ)V

    .line 2963
    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # setter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPenSelect:Z
    invoke-static {v4, v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$3302(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Z)Z

    .line 2964
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v4, v4, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsNewAlbumHeaderSelected:Z

    if-eqz v4, :cond_3

    .line 2965
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mRemovedCntInNewAlbumHeader:I
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$3400(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)I

    move-result v4

    sub-int/2addr p2, v4

    .line 2966
    :cond_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v4, p2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->onPhotoItemClick(I)V

    .line 2968
    if-eqz p4, :cond_8

    .line 2969
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # setter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsPenSelect:Z
    invoke-static {v4, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$3302(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Z)Z

    .line 2970
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # setter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mRemovedCntInNewAlbumHeader:I
    invoke-static {v4, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$3402(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;I)I

    .line 2971
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGroupIDList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$3500(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_7

    .line 2972
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-object v4, v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGroupIDList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$3500(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getBurstShotItems(J)Ljava/util/ArrayList;

    move-result-object v0

    .line 2973
    .local v0, "burstShotItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 2974
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v2, v4, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List;)V

    .line 2975
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Ljava/util/List;)V

    .line 2978
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mGroupIDList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$3500(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_5

    .line 2979
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->forceReload()V

    .line 2971
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2961
    .end local v0    # "burstShotItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v1    # "i":I
    :cond_6
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v4, v2, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->enterSelectionMode(ZZ)V

    goto/16 :goto_1

    .line 2982
    .restart local v1    # "i":I
    :cond_7
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsNewAlbumHeaderSelected:Z

    if-eqz v2, :cond_8

    .line 2983
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-gtz v2, :cond_8

    .line 2984
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    move-result-object v2

    iput-boolean v3, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mNotNeedToResetLayout:Z

    .end local v1    # "i":I
    :cond_8
    move v2, v3

    .line 2989
    goto/16 :goto_0
.end method

.method public prePenSelectionCheck(III)Z
    .locals 5
    .param p1, "albumIndex"    # I
    .param p2, "itemIndex"    # I
    .param p3, "selectedCnt"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 3008
    const/4 v0, 0x0

    .line 3009
    .local v0, "remove":Z
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v2

    sget-object v3, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v2, v3, :cond_0

    .line 3025
    :goto_0
    return v1

    .line 3012
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isPenSelectionMode()Z

    move-result v2

    if-nez v2, :cond_1

    .line 3013
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setPenSelectionMode(Z)V

    .line 3016
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumMode:Z

    if-eqz v2, :cond_4

    .line 3017
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    move-result-object v3

    invoke-virtual {v3, v1, p2}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 3018
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v2, v4, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->enterSelectionMode(ZZ)V

    .line 3022
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v1, p2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->prePenSelectionRemoveItem(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3023
    const/4 v0, 0x1

    :cond_3
    move v1, v0

    .line 3025
    goto :goto_0

    .line 3020
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$26;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v2, v1, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->enterSelectionMode(ZZ)V

    goto :goto_1
.end method
