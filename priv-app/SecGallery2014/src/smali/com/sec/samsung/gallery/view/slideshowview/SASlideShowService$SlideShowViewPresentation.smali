.class public Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;
.super Landroid/app/Presentation;
.source "SASlideShowService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SlideShowViewPresentation"
.end annotation


# instance fields
.field public mReleaseResource:Z

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;Landroid/content/Context;Landroid/view/Display;)V
    .locals 1
    .param p2, "outerContext"    # Landroid/content/Context;
    .param p3, "display"    # Landroid/view/Display;

    .prologue
    .line 455
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    .line 456
    invoke-direct {p0, p2, p3}, Landroid/app/Presentation;-><init>(Landroid/content/Context;Landroid/view/Display;)V

    .line 453
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->mReleaseResource:Z

    .line 457
    return-void
.end method


# virtual methods
.method public addCustomView(Landroid/view/View;Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/widget/FrameLayout$LayoutParams;

    .prologue
    .line 479
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    const v0, 0x7f0f00f9

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mFrameLayoutVideoView:Landroid/widget/FrameLayout;
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$902(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;Landroid/widget/FrameLayout;)Landroid/widget/FrameLayout;

    .line 480
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    const/4 v1, 0x1

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->isSlideshowVideoPlaying:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$402(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;Z)Z

    .line 481
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$200()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 482
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$200()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->pauseSlideshowMusic()V

    .line 483
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mFrameLayoutVideoView:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$900(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Landroid/widget/FrameLayout;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 484
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mFrameLayoutVideoView:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$900(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 485
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x4b0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 487
    :cond_1
    return-void
.end method

.method public dismiss()V
    .locals 2

    .prologue
    .line 491
    invoke-super {p0}, Landroid/app/Presentation;->dismiss()V

    .line 492
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$200()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->getSlideShowAdapter()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->mReleaseResource:Z

    if-eqz v0, :cond_0

    .line 493
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$200()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->getSlideShowAdapter()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->onPause()V

    .line 494
    :cond_0
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$200()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->getGlSlideShowView()Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 495
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$200()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->getGlSlideShowView()Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->pause()V

    .line 497
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$800(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 498
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$800(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onPause()V

    .line 499
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$800(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAllObject()V

    .line 500
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$802(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;Lcom/sec/android/gallery3d/glcore/GlRootView;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 503
    :cond_2
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$200()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onResume()V

    .line 504
    return-void
.end method

.method protected initializeGLSurfaceView()V
    .locals 3

    .prologue
    .line 507
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    const v0, 0x7f0f00bf

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlRootView;

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$802(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;Lcom/sec/android/gallery3d/glcore/GlRootView;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 508
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$800(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 509
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$800(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onResume()V

    .line 510
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$800(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$200()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->getGlSlideShowView()Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v1

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$200()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->attachLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;)V

    .line 513
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 461
    invoke-super {p0, p1}, Landroid/app/Presentation;->onCreate(Landroid/os/Bundle;)V

    .line 462
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # invokes: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->createLayoutParams()Landroid/view/WindowManager$LayoutParams;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$1000(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 463
    .local v0, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mIgnoreBackkeyListener:Landroid/content/DialogInterface$OnKeyListener;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$1100(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Landroid/content/DialogInterface$OnKeyListener;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 464
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 465
    .local v2, "window":Landroid/view/Window;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/Window;->requestFeature(I)Z

    .line 466
    const v3, 0x106000d

    invoke-virtual {v2, v3}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 467
    invoke-virtual {v2, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 469
    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->addKeepScreenOnFlag(Landroid/view/Window;)V

    .line 471
    const v3, 0x7f03005c

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->setContentView(I)V

    .line 472
    const v3, 0x7f0f00f8

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 473
    .local v1, "textView":Landroid/widget/TextView;
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 474
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->initializeGLSurfaceView()V

    .line 475
    return-void
.end method

.method protected reInitializeGLSurfaceView()V
    .locals 4

    .prologue
    .line 515
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$800(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setVisibility(I)V

    .line 516
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$800(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->onResume()V

    .line 517
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mNewGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$800(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$200()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->getGlSlideShowView()Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v1

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$200()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->attachLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;)V

    .line 518
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 519
    return-void
.end method
