.class public Lcom/sec/samsung/gallery/glview/GlGallerySearchView;
.super Lcom/sec/samsung/gallery/glview/GlAbsListView;
.source "GlGallerySearchView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;,
        Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;,
        Lcom/sec/samsung/gallery/glview/GlGallerySearchView$EstimationInfo;,
        Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GlInterpolatorSearch;
    }
.end annotation


# static fields
.field private static ALBUM_ITEM_MAX:I

.field private static CMD_APPLY_POS:I

.field public static final LOCK:Ljava/lang/Object;

.field private static final TAG:Ljava/lang/String;

.field public static final WIDE_THUMB_COUNT:[I


# instance fields
.field public final PORT_THUMB_COUNT:I

.field private mAnimDropUpDown:Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;

.field private mAnimStep:I

.field private mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

.field private mConvSetListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

.field private mDeleteAnim:Lcom/sec/samsung/gallery/glview/GlDeleteAnim;

.field private mDeletingIndex:I

.field public mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

.field private mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

.field protected mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

.field private mInitAnimListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

.field private mListenerDelete:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

.field private mListenerItem:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

.field private mListenerItemClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

.field private mListenerItemLongClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

.field private mListenerItemMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

.field private mNeedResize:Z

.field private mObjEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

.field private mObjEstimatorEx:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

.field private mPitchFact:F

.field private mReposAnim:Lcom/sec/samsung/gallery/glview/GlRepositionAnim;

.field protected mResource:Landroid/content/res/Resources;

.field private mResultText:Ljava/lang/String;

.field public mSPKeyString:Ljava/lang/String;

.field private mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

.field protected mTitleGlObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

.field protected mTitleUnderlineGlObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

.field protected mTitleUnderlineView:Lcom/sec/android/gallery3d/glcore/GlImageView;

.field private mUsePitchBoundary:Z

.field private mViewMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->TAG:Ljava/lang/String;

    .line 34
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->WIDE_THUMB_COUNT:[I

    .line 36
    const/16 v0, 0x40

    sput v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->ALBUM_ITEM_MAX:I

    .line 37
    const/16 v0, 0x69

    sput v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->CMD_APPLY_POS:I

    .line 60
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->LOCK:Ljava/lang/Object;

    return-void

    .line 34
    :array_0
    .array-data 4
        0x8
        0x6
        0x4
        0x2
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 63
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;-><init>(Landroid/content/Context;)V

    .line 35
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->PORT_THUMB_COUNT:I

    .line 38
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    .line 39
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    .line 41
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlDeleteAnim;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/glview/GlDeleteAnim;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDeleteAnim:Lcom/sec/samsung/gallery/glview/GlDeleteAnim;

    .line 42
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mReposAnim:Lcom/sec/samsung/gallery/glview/GlRepositionAnim;

    .line 43
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAnimDropUpDown:Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;

    .line 50
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mUsePitchBoundary:Z

    .line 51
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mNeedResize:Z

    .line 52
    const-string v0, "searchViewMode"

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mSPKeyString:Ljava/lang/String;

    .line 358
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$3;-><init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mInitAnimListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 407
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$4;-><init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mListenerItem:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

    .line 444
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$5;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$5;-><init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mListenerItemMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    .line 458
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$6;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$6;-><init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mListenerItemClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .line 483
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$7;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$7;-><init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mListenerItemLongClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

    .line 503
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$8;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$8;-><init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mListenerDelete:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 605
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$9;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$9;-><init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mConvSetListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

    .line 745
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$10;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$10;-><init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mObjEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    .line 759
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$11;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$11;-><init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mObjEstimatorEx:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    .line 64
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mResource:Landroid/content/res/Resources;

    .line 65
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mColCnt:I

    .line 66
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mViewMode:I

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGallerySearchView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mResultText:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1200()I
    .locals 1

    .prologue
    .line 30
    sget v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->CMD_APPLY_POS:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mUsePitchBoundary:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mPitchFact:F

    return v0
.end method

.method static synthetic access$302(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGallerySearchView;
    .param p1, "x1"    # F

    .prologue
    .line 30
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mPitchFact:F

    return p1
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)Lcom/sec/samsung/gallery/glview/GlBlurObject;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)Lcom/sec/samsung/gallery/view/utils/SoundUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDeletingIndex:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->getHorizontalCount()V

    return-void
.end method

.method private convAnimResize()V
    .locals 11

    .prologue
    .line 580
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v5, v7

    .line 581
    .local v5, "objCount":I
    new-array v1, v5, [Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 582
    .local v1, "glObject":[Lcom/sec/android/gallery3d/glcore/GlObject;
    new-array v4, v5, [I

    .line 583
    .local v4, "indexes":[I
    const/4 v6, 0x0

    .line 584
    .local v6, "validCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v5, :cond_0

    .line 585
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v2, v7, v3

    check-cast v2, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 586
    .local v2, "glObjectCur":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    if-lt v3, v7, :cond_1

    .line 596
    .end local v2    # "glObjectCur":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mObjEstimatorEx:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    invoke-interface {v7}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;->saveState()V

    .line 597
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v7}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;-><init>(Landroid/content/Context;)V

    .line 598
    .local v0, "convSetAnim":Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mIndexs:[I

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget v10, v10, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCount:I

    invoke-virtual {v0, v7, v8, v9, v10}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->setSrc([Lcom/sec/android/gallery3d/glcore/GlObject;[ILcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;I)V

    .line 599
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mObjEstimatorEx:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    invoke-virtual {v0, v1, v4, v7, v6}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->setDst([Lcom/sec/android/gallery3d/glcore/GlObject;[ILcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;I)V

    .line 600
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mConvSetListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

    invoke-virtual {v0, v7}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->setAnimationDoneListener(Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;)V

    .line 601
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->start()V

    .line 602
    const/4 v7, 0x0

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAnimType:I

    .line 603
    return-void

    .line 588
    .end local v0    # "convSetAnim":Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;
    .restart local v2    # "glObjectCur":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_1
    iget v7, v2, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    if-ge v7, v8, :cond_2

    .line 589
    const/4 v7, 0x1

    invoke-virtual {v2, v7}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setVisibility(Z)V

    .line 590
    aput-object v2, v1, v6

    .line 591
    iget v7, v2, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    aput v7, v4, v6

    .line 592
    add-int/lit8 v6, v6, 0x1

    .line 584
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private createRectangles()[Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .locals 8

    .prologue
    const/16 v4, 0x140

    const/high16 v2, 0x3f800000    # 1.0f

    .line 268
    sget v1, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->ALBUM_ITEM_MAX:I

    new-array v6, v1, [Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 270
    .local v6, "glThumbSet":[Lcom/sec/samsung/gallery/glview/GlThumbObject;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    sget v1, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->ALBUM_ITEM_MAX:I

    if-ge v7, v1, :cond_0

    .line 271
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    move v3, v2

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFII)V

    .line 272
    .local v0, "glThumb":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    aput-object v0, v6, v7

    .line 273
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mListenerItemMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V

    .line 274
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mListenerItemClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)V

    .line 275
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mListenerItemLongClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setLongClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;)V

    .line 276
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mListenerGenericMotion:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setGenericMotionListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;)V

    .line 270
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 278
    .end local v0    # "glThumb":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_0
    return-object v6
.end method

.method private freeRectangles()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 210
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-nez v1, :cond_0

    .line 219
    :goto_0
    return-void

    .line 214
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 215
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->remove()V

    .line 216
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aput-object v2, v1, v0

    .line 214
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 218
    :cond_1
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    goto :goto_0
.end method

.method private getHorizontalCount()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const-wide v8, 0x3fe3333333333333L    # 0.6

    const-wide v6, 0x3fd999999999999aL    # 0.4

    .line 652
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWideMode:Z

    if-eqz v1, :cond_4

    .line 653
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 654
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 655
    .local v0, "fullScreenWidth":I
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWidth:I

    int-to-double v2, v1

    int-to-double v4, v0

    mul-double/2addr v4, v6

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_0

    .line 656
    sget-object v1, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->WIDE_THUMB_COUNT:[I

    const/4 v2, 0x3

    aget v1, v1, v2

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mColCnt:I

    .line 670
    .end local v0    # "fullScreenWidth":I
    :goto_0
    return-void

    .line 657
    .restart local v0    # "fullScreenWidth":I
    :cond_0
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWidth:I

    int-to-double v2, v1

    int-to-double v4, v0

    mul-double/2addr v4, v6

    cmpl-double v1, v2, v4

    if-lez v1, :cond_1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWidth:I

    int-to-double v2, v1

    int-to-double v4, v0

    mul-double/2addr v4, v8

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_1

    .line 658
    sget-object v1, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->WIDE_THUMB_COUNT:[I

    const/4 v2, 0x2

    aget v1, v1, v2

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mColCnt:I

    goto :goto_0

    .line 659
    :cond_1
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWidth:I

    int-to-double v2, v1

    int-to-double v4, v0

    mul-double/2addr v4, v8

    cmpl-double v1, v2, v4

    if-lez v1, :cond_2

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWidth:I

    int-to-double v2, v1

    int-to-double v4, v0

    const-wide v6, 0x3feb333333333333L    # 0.85

    mul-double/2addr v4, v6

    cmpg-double v1, v2, v4

    if-gez v1, :cond_2

    .line 660
    sget-object v1, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->WIDE_THUMB_COUNT:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mColCnt:I

    goto :goto_0

    .line 662
    :cond_2
    sget-object v1, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->WIDE_THUMB_COUNT:[I

    aget v1, v1, v10

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mColCnt:I

    goto :goto_0

    .line 665
    .end local v0    # "fullScreenWidth":I
    :cond_3
    sget-object v1, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->WIDE_THUMB_COUNT:[I

    aget v1, v1, v10

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mColCnt:I

    goto :goto_0

    .line 668
    :cond_4
    const/4 v1, 0x4

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mColCnt:I

    goto :goto_0
.end method

.method private getObjectIndex(I)Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 705
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    if-ge v1, v2, :cond_1

    .line 706
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v0, v2, v1

    check-cast v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 707
    .local v0, "glObject":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    if-eqz v0, :cond_0

    iget v2, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    if-ne v2, p1, :cond_0

    .line 710
    .end local v0    # "glObject":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :goto_1
    return-object v0

    .line 705
    .restart local v0    # "glObject":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 710
    .end local v0    # "glObject":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private initGlHandler()V
    .locals 2

    .prologue
    .line 132
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$1;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$1;-><init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    .line 139
    return-void
.end method

.method private loadItems(Z)V
    .locals 4
    .param p1, "resizing"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 289
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v0

    .line 290
    .local v0, "count":I
    :goto_0
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAnimType:I

    if-eqz v2, :cond_2

    .line 291
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iput-boolean v1, v2, Lcom/sec/samsung/gallery/glview/GlScroller;->mInitVisible:Z

    .line 294
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->reset()V

    .line 296
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFocused:I

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/samsung/gallery/glview/GlScroller;->resetSet(IIZ)V

    .line 297
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollRange:F

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setRange(FF)V

    .line 299
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 300
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->applyThumbPosition()V

    .line 301
    if-lez v0, :cond_0

    .line 302
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->getFirstVisiblePosition()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->getLastVisiblePosition()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->updateVisiableRange(II)V

    .line 305
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->getScrollBar()Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->moveToLast()V

    .line 306
    return-void

    .end local v0    # "count":I
    :cond_1
    move v0, v1

    .line 289
    goto :goto_0

    .line 293
    .restart local v0    # "count":I
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iput-boolean v3, v1, Lcom/sec/samsung/gallery/glview/GlScroller;->mInitVisible:Z

    goto :goto_1
.end method

.method private pushCurrentListInfo()V
    .locals 5

    .prologue
    .line 677
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    sub-int/2addr v3, v4

    add-int/lit8 v2, v3, 0x1

    .line 678
    .local v2, "visibleCount":I
    if-gtz v2, :cond_0

    .line 699
    :goto_0
    return-void

    .line 681
    :cond_0
    new-instance v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    invoke-direct {v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;-><init>()V

    .line 682
    .local v1, "listInfo":Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    iput v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mFirst:I

    .line 683
    iput v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCount:I

    .line 684
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    iput v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mSelected:I

    .line 685
    new-array v3, v2, [I

    iput-object v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mIndexs:[I

    .line 686
    new-array v3, v2, [Lcom/sec/android/gallery3d/glcore/GlObject;

    iput-object v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 687
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_1

    .line 688
    iget-object v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mIndexs:[I

    iget v4, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mFirst:I

    add-int/2addr v4, v0

    aput v4, v3, v0

    .line 689
    iget-object v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v4, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mFirst:I

    add-int/2addr v4, v0

    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v4

    aput-object v4, v3, v0

    .line 687
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 691
    :cond_1
    const/4 v0, 0x0

    :goto_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 692
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v4, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    and-int/2addr v3, v4

    if-nez v3, :cond_2

    .line 693
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->remove()V

    .line 691
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 695
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mObjEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    iput-object v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    .line 696
    iget-object v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    invoke-interface {v3}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;->saveState()V

    .line 697
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v4, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/glcore/GlRootView;->addObjectNotToDetach([Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 698
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->saveStatus(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private reload()V
    .locals 1

    .prologue
    .line 529
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->loadItems(Z)V

    .line 530
    return-void
.end method

.method private setBoundaryEffect()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 309
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mUsePitchBoundary:Z

    if-eqz v2, :cond_0

    .line 325
    :goto_0
    return-void

    .line 312
    :cond_0
    const/16 v0, 0x64

    .line 314
    .local v0, "boundThick":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    if-nez v2, :cond_1

    .line 315
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlBlurObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v5, 0x2

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/samsung/gallery/glview/GlBlurObject;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlLayer;I)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    .line 316
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    const v3, 0x7f0203fa

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->setResourceImage(I)V

    .line 318
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeightPixel()I

    move-result v1

    .line 319
    .local v1, "paddingTop":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    invoke-virtual {v2, v6, v6, v1, v6}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->setPadding(IIII)V

    .line 321
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    const/16 v3, 0x88

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWidth:I

    invoke-virtual {v2, v6, v3, v4, v0}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->setAlignAndScale(IIII)V

    .line 322
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    const/4 v3, 0x1

    const/16 v4, 0x18

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWidth:I

    invoke-virtual {v2, v3, v4, v5, v0}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->setAlignAndScale(IIII)V

    .line 324
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    const/4 v3, 0x0

    invoke-virtual {v2, v6, v3}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->show(IF)V

    goto :goto_0
.end method

.method private setScaleFactor(ZI)V
    .locals 0
    .param p1, "wide"    # Z
    .param p2, "colCount"    # I

    .prologue
    .line 545
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->getHorizontalCount()V

    .line 546
    return-void
.end method

.method private switchKeyboardFocus(Z)V
    .locals 5
    .param p1, "toKeyword"    # Z

    .prologue
    const/4 v4, 0x0

    .line 782
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 783
    .local v0, "focusedIndex":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iput v0, v2, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    .line 784
    const/4 v1, 0x0

    .line 785
    .local v1, "focusedPart":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    if-eqz v2, :cond_0

    .line 786
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-interface {v2, v3, v0, v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;->onFocusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V

    .line 788
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlScroller;->update(I)V

    .line 789
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setFocusable(Z)V

    .line 790
    return-void
.end method


# virtual methods
.method public attachScrollBar(Lcom/sec/samsung/gallery/glview/GlScroller;)V
    .locals 6
    .param p1, "scroller"    # Lcom/sec/samsung/gallery/glview/GlScroller;

    .prologue
    const/4 v5, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    .line 143
    if-nez p1, :cond_0

    .line 148
    :goto_0
    return-void

    .line 146
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;

    move-object v1, p0

    move-object v2, p0

    move v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;-><init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;Lcom/sec/android/gallery3d/glcore/GlLayer;FFI)V

    .line 147
    .local v0, "scrollBar":Lcom/sec/samsung/gallery/glview/GlScrollBar;
    invoke-virtual {p1, v0, v5}, Lcom/sec/samsung/gallery/glview/GlScroller;->setScrollBarObject(Lcom/sec/samsung/gallery/glview/GlScrollBar;Z)V

    goto :goto_0
.end method

.method public convAnimGrid(Z)V
    .locals 7
    .param p1, "isShow"    # Z

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    .line 388
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAnimDropUpDown:Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;

    if-nez v0, :cond_0

    .line 389
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->getGlRoot()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Lcom/sec/samsung/gallery/glview/GlScroller;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAnimDropUpDown:Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;

    .line 390
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAnimDropUpDown:Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->setDuration(J)V

    .line 391
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAnimDropUpDown:Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mInitAnimListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 392
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAnimDropUpDown:Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;

    new-instance v1, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GlInterpolatorSearch;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GlInterpolatorSearch;-><init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->setInterpolator(Lcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    .line 394
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAnimDropUpDown:Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    .line 395
    if-eqz p1, :cond_2

    .line 396
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAnimDropUpDown:Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v2, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    check-cast v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTagListAbsHeight:F
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->access$100(Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;)F

    move-result v0

    sub-float/2addr v2, v0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v3, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    invoke-virtual/range {v1 .. v6}, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->startTranlateScrollAnimation(FFJZ)V

    .line 405
    :cond_1
    :goto_0
    return-void

    .line 400
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAnimDropUpDown:Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v2, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    check-cast v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTagListAbsHeight:F
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->access$100(Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;)F

    move-result v0

    add-float/2addr v2, v0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v3, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    invoke-virtual/range {v1 .. v6}, Lcom/sec/samsung/gallery/glview/GlScrollerAnimation;->startTranlateScrollAnimation(FFJZ)V

    goto :goto_0
.end method

.method protected doResize()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 549
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 550
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->pushCurrentListInfo()V

    .line 551
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->restoreStatus()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    .line 553
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    if-nez v0, :cond_0

    .line 571
    :goto_0
    return-void

    .line 556
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;

    if-eqz v0, :cond_1

    .line 557
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWideMode:Z

    if-eqz v0, :cond_2

    .line 558
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mColCnt:I

    invoke-interface {v0, v1, v4, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;->onStatusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V

    .line 563
    :cond_1
    :goto_1
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAnimType:I

    .line 564
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mLockPos:Z

    .line 565
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->createRectangles()[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 566
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlScroller;->reset()V

    .line 567
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->register([Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 569
    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->loadItems(Z)V

    .line 570
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->convAnimResize()V

    goto :goto_0

    .line 560
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v2, 0x2

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mColCnt:I

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;->onStatusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V

    goto :goto_1
.end method

.method public getColumnCount()I
    .locals 1

    .prologue
    .line 1380
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mColCnt:I

    return v0
.end method

.method public getResultText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mResultText:Ljava/lang/String;

    return-object v0
.end method

.method public getScrollBar(Lcom/sec/samsung/gallery/glview/GlScroller;)Lcom/sec/samsung/gallery/glview/GlScrollBar;
    .locals 1
    .param p1, "scroller"    # Lcom/sec/samsung/gallery/glview/GlScroller;

    .prologue
    .line 157
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->getScrollBar(Lcom/sec/samsung/gallery/glview/GlScroller;)Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v0

    return-object v0
.end method

.method protected isScreenLocked()Z
    .locals 1

    .prologue
    .line 775
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAnimStep:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 776
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->isScreenLocked()Z

    move-result v0

    goto :goto_0
.end method

.method protected loadItems()V
    .locals 1

    .prologue
    .line 283
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->loadItems(Z)V

    .line 284
    return-void
.end method

.method protected onCommand(ILjava/lang/Object;III)V
    .locals 2
    .param p1, "cmd"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I
    .param p5, "parm3"    # I

    .prologue
    const/4 v1, 0x1

    .line 514
    if-ne p1, v1, :cond_1

    .line 515
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->reload()V

    .line 526
    :cond_0
    :goto_0
    return-void

    .line 516
    :cond_1
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    .line 517
    if-eq p4, v1, :cond_0

    .line 519
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v0, p3}, Lcom/sec/samsung/gallery/glview/GlScroller;->update(I)V

    goto :goto_0

    .line 521
    :cond_2
    sget v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->CMD_APPLY_POS:I

    if-ne p1, v0, :cond_3

    .line 522
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlScroller;->applyThumbPosition()V

    goto :goto_0

    .line 524
    :cond_3
    invoke-super/range {p0 .. p5}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->onCommand(ILjava/lang/Object;III)V

    goto :goto_0
.end method

.method protected onCreate()V
    .locals 14

    .prologue
    const/16 v4, 0x4b0

    const/4 v9, 0x3

    const/4 v6, 0x0

    const/4 v1, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    .line 70
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setClearByColor(Z)V

    .line 71
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mResource:Landroid/content/res/Resources;

    const v3, 0x7f0b0060

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->intColorToFloatARGBArray(I)[F

    move-result-object v12

    .line 73
    .local v12, "color":[F
    aget v0, v12, v1

    const/4 v3, 0x2

    aget v3, v12, v3

    aget v7, v12, v9

    aget v8, v12, v6

    invoke-virtual {p0, v0, v3, v7, v8}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setGlBackgroundColor(FFFF)V

    .line 75
    .end local v12    # "color":[F
    :cond_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->resetLayout()V

    .line 77
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v13

    .line 78
    .local v13, "orientation":I
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-nez v0, :cond_3

    if-eq v13, v1, :cond_1

    if-ne v13, v9, :cond_3

    :cond_1
    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWideMode:Z

    .line 80
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0d0263

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 81
    .local v5, "photoLabelSize":I
    add-int/lit8 v5, v5, 0x10

    .line 83
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 84
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWidthSpace:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mHeightSpace:F

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->getHeight()I

    move-result v8

    invoke-virtual {v0, v1, v3, v7, v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->setDimensionUtil(FFII)V

    .line 86
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mSPKeyString:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewDefaultMode()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v0, v1, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setThumbnailViewMode(I)V

    .line 87
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mViewMode:I

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewShowSplitModeCount()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setThumbnailViewMode(I)V

    .line 88
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->getHorizontalCount()V

    .line 90
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->freeRectangles()V

    .line 91
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;

    invoke-direct {v0, p0, p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;-><init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;Lcom/sec/samsung/gallery/glview/GlAbsListView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    .line 92
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->attachScrollBar(Lcom/sec/samsung/gallery/glview/GlScroller;)V

    .line 93
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->createRectangles()[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 94
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFII)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleGlObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 95
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleGlObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    check-cast v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v0, v6}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setTouchAnimEnable(Z)V

    .line 96
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleGlObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mListenerGenericMotionEx:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setGenericMotionListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;)V

    .line 98
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->register([Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 100
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGallerySearchUISeperatedMode:Z

    if-eqz v0, :cond_2

    .line 101
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleUnderlineView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 102
    new-instance v6, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/16 v11, 0xa

    move v8, v2

    move v9, v2

    move v10, v4

    invoke-direct/range {v6 .. v11}, Lcom/sec/samsung/gallery/glview/GlBaseObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFII)V

    iput-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleUnderlineGlObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 103
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleUnderlineView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    const v1, 0x7f020228

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 104
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleUnderlineGlObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleUnderlineView:Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 107
    :cond_2
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    .line 108
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    const v1, 0x3e99999a    # 0.3f

    const v2, 0x3f333333    # 0.7f

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setFactors(FF)V

    .line 109
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setDuration(J)V

    .line 110
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    const/high16 v1, 0x467a0000    # 16000.0f

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setMaxSpeed(F)V

    .line 111
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mListenerItem:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setBoundaryAnimationListener(Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;)V

    .line 112
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDeleteAnim:Lcom/sec/samsung/gallery/glview/GlDeleteAnim;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mListenerDelete:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlDeleteAnim;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 115
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDeleteAnim:Lcom/sec/samsung/gallery/glview/GlDeleteAnim;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 117
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mReposAnim:Lcom/sec/samsung/gallery/glview/GlRepositionAnim;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mListenerDelete:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 118
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mReposAnim:Lcom/sec/samsung/gallery/glview/GlRepositionAnim;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 120
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setBoundaryEffect()V

    .line 122
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    .line 123
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->initGlHandler()V

    .line 124
    return-void

    .end local v5    # "photoLabelSize":I
    :cond_3
    move v0, v6

    .line 78
    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 162
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v1, v1

    if-nez v1, :cond_1

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->freeRectangles()V

    .line 167
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDeleteAnim:Lcom/sec/samsung/gallery/glview/GlDeleteAnim;

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 168
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mReposAnim:Lcom/sec/samsung/gallery/glview/GlRepositionAnim;

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 169
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    if-eqz v1, :cond_2

    .line 170
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->destroy()V

    .line 172
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->getScrollBar(Lcom/sec/samsung/gallery/glview/GlScroller;)Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v0

    .line 173
    .local v0, "sb":Lcom/sec/samsung/gallery/glview/GlScrollBar;
    if-eqz v0, :cond_3

    .line 174
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->remove()V

    .line 175
    :cond_3
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V

    goto :goto_0
.end method

.method protected onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v4, 0x42

    const/16 v3, 0x17

    const/4 v2, 0x3

    const/4 v0, 0x1

    .line 808
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->isScreenLocked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 853
    :cond_0
    :goto_0
    return v0

    .line 812
    :cond_1
    const/16 v1, 0x15

    if-eq p1, v1, :cond_2

    const/16 v1, 0x16

    if-eq p1, v1, :cond_2

    const/16 v1, 0x13

    if-eq p1, v1, :cond_2

    const/16 v1, 0x14

    if-eq p1, v1, :cond_2

    if-eq p1, v3, :cond_2

    if-eq p1, v4, :cond_2

    const/16 v1, 0x3d

    if-ne p1, v1, :cond_3

    .line 815
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 817
    :cond_3
    if-eq p1, v4, :cond_4

    if-ne p1, v3, :cond_5

    .line 818
    :cond_4
    invoke-virtual {p2}, Landroid/view/KeyEvent;->startTracking()V

    goto :goto_0

    .line 821
    :cond_5
    packed-switch p1, :pswitch_data_0

    .line 853
    const/4 v0, 0x0

    goto :goto_0

    .line 823
    :pswitch_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 824
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    if-nez v1, :cond_6

    .line 825
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->switchKeyboardFocus(Z)V

    .line 826
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/GlScroller;->moveTo(I)Z

    goto :goto_0

    .line 828
    :cond_6
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/GlScroller;->moveTo(I)Z

    move-result v0

    goto :goto_0

    .line 831
    :pswitch_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 832
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    add-int/lit8 v2, v2, -0x1

    if-eq v1, v2, :cond_0

    .line 833
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->moveTo(I)Z

    move-result v0

    goto :goto_0

    .line 837
    :pswitch_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 838
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->firstRowFocused()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 839
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->switchKeyboardFocus(Z)V

    .line 840
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlScroller;->moveTo(I)Z

    goto :goto_0

    .line 842
    :cond_7
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/GlScroller;->moveTo(I)Z

    move-result v0

    goto :goto_0

    .line 845
    :pswitch_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 846
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    if-lez v1, :cond_0

    .line 847
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->lastRowFocused()Z

    move-result v1

    if-nez v1, :cond_0

    .line 848
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->moveTo(I)Z

    move-result v0

    goto/16 :goto_0

    .line 821
    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 794
    const/16 v0, 0xa8

    if-eq p1, v0, :cond_0

    const/16 v0, 0xa9

    if-ne p1, v0, :cond_1

    .line 795
    :cond_0
    const/4 v0, 0x1

    .line 803
    :goto_0
    return v0

    .line 798
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->isScreenLocked()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 800
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mOnKeyListenner:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;

    if-eqz v0, :cond_4

    .line 801
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mOnKeyListenner:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;

    const/16 v1, 0x80

    invoke-interface {v0, p1, v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;->onKeyEvent(II)Z

    move-result v0

    goto :goto_0

    .line 803
    :cond_4
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 858
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->isSlideShowMode:Z

    if-eqz v3, :cond_1

    .line 876
    :cond_0
    :goto_0
    return v1

    .line 860
    :cond_1
    const/16 v3, 0xa8

    if-eq p1, v3, :cond_2

    const/16 v3, 0xa9

    if-eq p1, v3, :cond_2

    const/16 v3, 0x118

    if-eq p1, v3, :cond_2

    const/16 v3, 0x117

    if-ne p1, v3, :cond_3

    :cond_2
    move v1, v2

    .line 862
    goto :goto_0

    .line 864
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->isScreenLocked()Z

    move-result v3

    if-nez v3, :cond_0

    .line 866
    const/16 v3, 0x42

    if-eq p1, v3, :cond_4

    const/16 v3, 0x17

    if-ne p1, v3, :cond_6

    .line 867
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 868
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFocused:I

    .line 869
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFocused:I

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/GlScroller;->getThumbByIndex(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v0

    .line 870
    .local v0, "glThumbObject":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;

    if-eqz v3, :cond_5

    if-eqz v0, :cond_5

    .line 871
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v5

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFocused:I

    invoke-interface {v3, v4, v5, v6}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;->onItemClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 872
    const/4 v3, 0x4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFocused:I

    invoke-virtual {p0, v3, v4, v1, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setCommand(IIII)V

    :cond_5
    move v1, v2

    .line 874
    goto :goto_0

    .line 876
    .end local v0    # "glThumbObject":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_6
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method protected onMoved(II)Z
    .locals 4
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    const/4 v3, 0x1

    .line 339
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->isScreenLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342
    :goto_0
    return v3

    .line 341
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    neg-int v2, p2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWorldCoordinateValue(F)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setMovement(F)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setHoverScrollFlexibleHeightMargin(F)V

    .line 129
    return-void
.end method

.method protected onPressed(II)Z
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v1, 0x1

    .line 329
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->isScreenLocked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 334
    :cond_0
    :goto_0
    return v1

    .line 331
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    goto :goto_0
.end method

.method protected onReleased(IIII)Z
    .locals 3
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "speedX"    # I
    .param p4, "speedY"    # I

    .prologue
    const/4 v2, 0x1

    .line 352
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->isScreenLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355
    :goto_0
    return v2

    .line 354
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    neg-int v1, p4

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->startFling(F)V

    goto :goto_0
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)V
    .locals 4
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v3, 0x0

    .line 623
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mLockPos:Z

    if-eqz v2, :cond_1

    .line 641
    :cond_0
    :goto_0
    return-void

    .line 626
    :cond_1
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    .line 627
    .local v1, "scaleFactor":F
    const v2, 0x3f8147ae    # 1.01f

    cmpl-float v2, v1, v2

    if-lez v2, :cond_2

    .line 628
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mViewMode:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setThumbnailViewMode(I)V

    .line 635
    :goto_1
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mColCnt:I

    .line 636
    .local v0, "oldColCount":I
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->getHorizontalCount()V

    .line 637
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mColCnt:I

    if-eq v2, v0, :cond_0

    .line 638
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFocused:I

    .line 639
    const/4 v2, 0x6

    invoke-virtual {p0, v2, v3, v3, v3}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setCommand(IIII)V

    goto :goto_0

    .line 629
    .end local v0    # "oldColCount":I
    :cond_2
    const v2, 0x3f7d70a4    # 0.99f

    cmpg-float v2, v1, v2

    if-gez v2, :cond_0

    .line 630
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mViewMode:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setThumbnailViewMode(I)V

    goto :goto_1
.end method

.method protected onScrolled(IIII)Z
    .locals 2
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "x"    # I
    .param p4, "y"    # I

    .prologue
    const/4 v1, 0x0

    .line 347
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->onMoved(II)Z

    move-result v0

    invoke-virtual {p0, v1, v1, v1, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->onReleased(IIII)Z

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method public playSound(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 614
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    if-eqz v0, :cond_0

    .line 615
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    .line 617
    :cond_0
    return-void
.end method

.method public redrawLayout(Z)V
    .locals 3
    .param p1, "needResize"    # Z

    .prologue
    const/4 v2, 0x0

    .line 246
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/16 v1, 0x9

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 248
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mNeedResize:Z

    .line 250
    :cond_0
    return-void
.end method

.method protected resetLayout()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 227
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->resetLayout()V

    .line 228
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleGlObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-nez v1, :cond_0

    .line 243
    :goto_0
    return-void

    .line 231
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 232
    .local v0, "orientation":I
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    if-nez v1, :cond_3

    if-eq v0, v2, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_1
    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWideMode:Z

    .line 234
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWidthSpace:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mHeightSpace:F

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->getHeight()I

    move-result v6

    invoke-virtual {v1, v3, v4, v5, v6}, Lcom/sec/samsung/gallery/util/DimensionUtil;->setDimensionUtil(FFII)V

    .line 235
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->getHorizontalCount()V

    .line 236
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFocused:I

    .line 237
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mMode:I

    sget v3, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->LIST_MODE_SELECT:I

    if-ne v1, v3, :cond_2

    .line 238
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mNeedResize:Z

    .line 240
    :cond_2
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mNeedResize:Z

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->loadItems(Z)V

    .line 241
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setBoundaryEffect()V

    .line 242
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mNeedResize:Z

    goto :goto_0

    .line 232
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public resetPositionByTaglistOnOff()V
    .locals 5

    .prologue
    .line 253
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->resetLayout()V

    .line 254
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    if-nez v0, :cond_1

    .line 261
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWidthSpace:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mHeightSpace:F

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->getHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->setDimensionUtil(FFII)V

    .line 257
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->getHorizontalCount()V

    .line 258
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFocused:I

    .line 259
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->loadItems(Z)V

    .line 260
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setBoundaryEffect()V

    goto :goto_0
.end method

.method public setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->unregisterObserver(Ljava/lang/Object;)V

    .line 182
    :cond_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .line 183
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v0, :cond_1

    .line 184
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$2;-><init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    .line 204
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->registerObserver(Lcom/sec/samsung/gallery/glview/GContentObserver;)V

    .line 207
    :cond_1
    return-void
.end method

.method public setAttrs(II)V
    .locals 1
    .param p1, "attr"    # I
    .param p2, "parm"    # I

    .prologue
    const/4 v0, 0x1

    .line 538
    if-ne p1, v0, :cond_1

    .line 539
    invoke-direct {p0, v0, p2}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setScaleFactor(ZI)V

    .line 542
    :cond_0
    :goto_0
    return-void

    .line 540
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 541
    const/4 v0, 0x0

    invoke-direct {p0, v0, p2}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setScaleFactor(ZI)V

    goto :goto_0
.end method

.method public setScrollBarMaxHeight(Lcom/sec/samsung/gallery/glview/GlScroller;F)V
    .locals 0
    .param p1, "scroller"    # Lcom/sec/samsung/gallery/glview/GlScroller;
    .param p2, "max"    # F

    .prologue
    .line 152
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->setScrollBarMaxHeight(Lcom/sec/samsung/gallery/glview/GlScroller;F)V

    .line 153
    return-void
.end method

.method public setThumbnailPitchRate(FLcom/sec/samsung/gallery/glview/GlScroller;)V
    .locals 6
    .param p1, "rate"    # F
    .param p2, "scroller"    # Lcom/sec/samsung/gallery/glview/GlScroller;

    .prologue
    .line 1384
    iget-object v4, p2, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-nez v4, :cond_1

    .line 1402
    :cond_0
    return-void

    .line 1388
    :cond_1
    iget v4, p2, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    iget-object v5, p2, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v5, v5

    if-le v4, v5, :cond_2

    iget-object v4, p2, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v0, v4

    .line 1389
    .local v0, "count":I
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_0

    .line 1390
    iget-object v4, p2, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v2, v4, v1

    .line 1391
    .local v2, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-nez v2, :cond_3

    .line 1389
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1388
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_2
    iget v0, p2, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    goto :goto_0

    .line 1394
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    .restart local v2    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_3
    iget v4, v2, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v5, p2, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    div-int v3, v4, v5

    .line 1396
    .local v3, "row":I
    if-eqz v3, :cond_4

    iget v4, p2, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    iget v5, p2, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    div-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x1

    sub-int/2addr v4, v3

    if-nez v4, :cond_5

    .line 1397
    :cond_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewEndAffordanceRowDegree(I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, p1

    invoke-virtual {v2, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPitch(F)V

    goto :goto_2

    .line 1399
    :cond_5
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewEndAffordanceRowDegree(I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, p1

    invoke-virtual {v2, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPitch(F)V

    goto :goto_2
.end method

.method public setThumbnailViewMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 644
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewShowSplitModeCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewModeCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le p1, v0, :cond_1

    .line 649
    :cond_0
    :goto_0
    return-void

    .line 647
    :cond_1
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mViewMode:I

    .line 648
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mSPKeyString:Ljava/lang/String;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mViewMode:I

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public updateItems()V
    .locals 1

    .prologue
    .line 533
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    if-eqz v0, :cond_0

    .line 534
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlScroller;->update()V

    .line 535
    :cond_0
    return-void
.end method
